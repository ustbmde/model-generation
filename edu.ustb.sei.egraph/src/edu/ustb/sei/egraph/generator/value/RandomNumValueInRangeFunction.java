package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public abstract class RandomNumValueInRangeFunction<T> extends RandomValueInRangeFunction<T> {
	public RandomNumValueInRangeFunction(T min, T max) {
		super();
		this.min = min;
		this.max = max;
	}

	protected T min;
	protected T max;
	
	@Override
	public T getValue(RandomService t) {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString() {
		return "["+min+".."+(max==null?"*":max)+"]";
	}

}
