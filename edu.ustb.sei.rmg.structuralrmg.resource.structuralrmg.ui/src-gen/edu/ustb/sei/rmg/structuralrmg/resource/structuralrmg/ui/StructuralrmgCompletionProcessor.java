/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgCompletionProcessor implements org.eclipse.jface.text.contentassist.IContentAssistProcessor {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourceProvider resourceProvider;
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.IStructuralrmgBracketHandlerProvider bracketHandlerProvider;
	
	public StructuralrmgCompletionProcessor(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourceProvider resourceProvider, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.IStructuralrmgBracketHandlerProvider bracketHandlerProvider) {
		this.resourceProvider = resourceProvider;
		this.bracketHandlerProvider = bracketHandlerProvider;
	}
	
	public org.eclipse.jface.text.contentassist.ICompletionProposal[] computeCompletionProposals(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource textResource = resourceProvider.getResource();
		if (textResource == null) {
			return new org.eclipse.jface.text.contentassist.ICompletionProposal[0];
		}
		String content = viewer.getDocument().get();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCodeCompletionHelper helper = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCodeCompletionHelper();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal[] computedProposals = helper.computeCompletionProposals(textResource, content, offset);
		
		// call completion proposal post processor to allow for customizing the proposals
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgProposalPostProcessor proposalPostProcessor = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgProposalPostProcessor();
		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal> computedProposalList = java.util.Arrays.asList(computedProposals);
		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal> extendedProposalList = proposalPostProcessor.process(computedProposalList);
		if (extendedProposalList == null) {
			extendedProposalList = java.util.Collections.emptyList();
		}
		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal> finalProposalList = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal>();
		for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal proposal : extendedProposalList) {
			if (proposal.getMatchesPrefix()) {
				finalProposalList.add(proposal);
			}
		}
		org.eclipse.jface.text.contentassist.ICompletionProposal[] result = new org.eclipse.jface.text.contentassist.ICompletionProposal[finalProposalList.size()];
		int i = 0;
		for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal proposal : finalProposalList) {
			String proposalString = proposal.getInsertString();
			String displayString = proposal.getDisplayString();
			String prefix = proposal.getPrefix();
			org.eclipse.swt.graphics.Image image = proposal.getImage();
			org.eclipse.jface.text.contentassist.IContextInformation info;
			info = new org.eclipse.jface.text.contentassist.ContextInformation(image, proposalString, proposalString);
			int begin = offset - prefix.length();
			int replacementLength = prefix.length();
			// if a closing bracket was automatically inserted right before, we enlarge the
			// replacement length in order to overwrite the bracket.
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.IStructuralrmgBracketHandler bracketHandler = bracketHandlerProvider.getBracketHandler();
			String closingBracket = bracketHandler.getClosingBracket();
			if (bracketHandler.addedClosingBracket() && proposalString.endsWith(closingBracket)) {
				replacementLength += closingBracket.length();
			}
			result[i++] = new org.eclipse.jface.text.contentassist.CompletionProposal(proposalString, begin, replacementLength, proposalString.length(), image, displayString, info, proposalString);
		}
		return result;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformation[] computeContextInformation(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		return null;
	}
	
	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}
	
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	
	public String getErrorMessage() {
		return null;
	}
}
