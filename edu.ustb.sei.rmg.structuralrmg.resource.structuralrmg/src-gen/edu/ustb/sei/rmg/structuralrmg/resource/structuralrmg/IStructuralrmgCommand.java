/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IStructuralrmgCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
