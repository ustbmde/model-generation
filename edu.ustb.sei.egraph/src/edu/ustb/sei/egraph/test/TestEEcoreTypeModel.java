package edu.ustb.sei.egraph.test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.ustb.sei.egraph.bridge.CustomizedResourceSet;
import edu.ustb.sei.egraph.bridge.EMFExporter;
import edu.ustb.sei.egraph.generator.graph.GraphGenerator;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfig;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfigUtil;
import edu.ustb.sei.egraph.generator.graph.service.GraphGenerationService;
import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.typemodel.EEcoreTypeModel;

public class TestEEcoreTypeModel {
	
	private EEcoreTypeModel model;
	private CustomizedResourceSet resSet;
	private GraphConfig r;

	@Before
	public void setUp() throws Exception {
		resSet = new CustomizedResourceSet();
		resSet.setDefaultPath("file:/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.egraph/src/edu/ustb/sei/egraph/test");
		Resource ecore = resSet.getResource(URI.createURI("test.ecore"), true);
		model = new EEcoreTypeModel((EPackage)ecore.getContents().get(0));
		
		GraphConfigUtil g = new GraphConfigUtil();
		String config = "global\n"
					  + "    define name : ['a'..'z']@4,\n"
					  + "	 root : manual,\n"
					  + "    save schema_location : true,\n"
					  + "    save xmi_id : true\n"
					  + "node A \n"
					  + "  instance a {name=\"root\"}\n"
					  + "node C \n"
					  + "  instance c {fullName=\"c1\"},{fullName=\"c2\"}\n"
					  + "node D\n"
					  + "  instance d {fullName=\"d1\"},{fullName=\"d2\"}\n"
					  + "node EDataType\n"
					  + "  instance primitive external <http://www.eclipse.org/emf/2002/Ecore#//EString>\n"
					  + "edge elements:A\n"
					  + "  instance a-(elements:A)->d[0]\n"
					  + "edge type:A\n"
					  + "  {bound=1}\n"
					  + "edge elements:C\n"
					  + "  instance c[0]-(elements:C)->d[1]\n"
					  + "edge elements:D\n"
					  + "  instance d[0]-(elements:D)->c[0]\n"
					  + "           d[1]-(elements:D)->c[1]"
					  ;
		
		r = g.parse(config).root;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1() {
		for(EClass c : model.allNodeTypes()) {
			for(EClass t : model.allNodeTypes()) {
				System.out.println(c.getName()+","+t.getName()+"=>"+model.hasCommonSubTypes(c, t));
			}
		}
	}

	@Test
	public void test2() {
		for(EClass c : model.allNodeTypes()) {
			Assert.assertTrue(model.collectAllSuperTypes(c).contains(c));
			Assert.assertTrue(model.collectAllSuperTypes(c).containsAll(c.getEAllSuperTypes()));
		}
	}
	
	@Test
	public void test3() {
		try {
			EGraph<EClass,EReference,EAttribute,String> graph = new EGraph<EClass, EReference, EAttribute, String>();
			graph.setTypeModel(model);
			
			GraphGenerationService service = new GraphGenerationService(100, graph,r);
			
			GraphGenerator<EClass, EReference, EAttribute, Void> generator = new GraphGenerator<EClass, EReference, EAttribute, Void>(service);
			
			generator.randomGenerateModel();
			
			for(ENode<?,?,?> n : graph.getNodes()) {
				System.out.println(n);
			}
			for(EEdge<?,?,?> n : graph.getEdges()) {
				System.out.println(n);
			}
			EMFExporter exporter = new EMFExporter();
			
			Resource output = resSet.createResource(URI.createURI("out.xmi"));
			
			exporter.exportGraph(output, generator.getHostGraph());
			
			exporter.save(output, generator.getSaveOptions());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
