/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgOutlinePageLinkWithEditorAction extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.AbstractStructuralrmgOutlinePageAction {
	
	public StructuralrmgOutlinePageLinkWithEditorAction(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Link with Editor", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/link_with_editor_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setLinkWithEditor(on);
	}
	
}
