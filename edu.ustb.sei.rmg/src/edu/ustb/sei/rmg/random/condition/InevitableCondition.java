package edu.ustb.sei.rmg.random.condition;

public class InevitableCondition<T> extends AtomicCondition<T> {
	public InevitableCondition() {
		possibility = 0.0;
	}
}
