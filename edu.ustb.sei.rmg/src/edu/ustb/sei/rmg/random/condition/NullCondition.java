package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class NullCondition extends Condition<Void> {
	
	static final public NullCondition instance = new NullCondition();

	@Override
	public Void getValue(RandomUtil r, Object... extra) {
		// TODO Auto-generated method stub
		return null;
	}

}
