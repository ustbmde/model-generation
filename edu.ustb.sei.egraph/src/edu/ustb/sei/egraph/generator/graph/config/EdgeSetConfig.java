package edu.ustb.sei.egraph.generator.graph.config;

import java.util.ArrayList;
import java.util.List;

import edu.ustb.sei.egraph.generator.util.QualifiedName;

public class EdgeSetConfig {
	public EdgeSetConfig(QualifiedName[] edges, boolean idem, boolean refl, boolean ord, boolean sourceUnique,
			boolean sourceRequired, boolean targetUnique, boolean targetRequired) {
		super();
		this.edges = edges;
		this.idem = idem;
		this.refl = refl;
		this.ord = ord;
		this.sourceUnique = sourceUnique;
		this.sourceRequired = sourceRequired;
		this.targetUnique = targetUnique;
		this.targetRequired = targetRequired;
		this.generationConfigs = new ArrayList<EdgeSetGenerationConfig>();
		this.instanceConfigs = new ArrayList<EdgeInstanceConfig>();
	}
	private QualifiedName[] edges;
	private boolean idem; // idempotent
	private boolean refl; // reflectivity
	private boolean ord; // ordered
	private boolean sourceUnique;
	private boolean sourceRequired;
	private boolean targetUnique;
	private boolean targetRequired;
	
	private List<EdgeSetGenerationConfig> generationConfigs;
	private List<EdgeInstanceConfig> instanceConfigs;
	
	public QualifiedName[] getEdges() {
		return edges;
	}
	public boolean isIdem() {
		return idem;
	}
	public boolean isRefl() {
		return refl;
	}
	public boolean isOrd() {
		return ord;
	}
	public boolean isSourceUnique() {
		return sourceUnique;
	}
	public boolean isSourceRequired() {
		return sourceRequired;
	}
	public boolean isTargetUnique() {
		return targetUnique;
	}
	public boolean isTargetRequired() {
		return targetRequired;
	}
	public List<EdgeSetGenerationConfig> getGenerationConfigs() {
		return generationConfigs;
	}
	public List<EdgeInstanceConfig> getInstanceConfigs() {
		return instanceConfigs;
	}
	public boolean addInstanceConfig(EdgeInstanceConfig config) {
		QualifiedName n = config.getType();
		for(QualifiedName t : edges) {
			if(t.name.equals(n.name) && t.type.equals(n.type)) {
				this.instanceConfigs.add(config);
				return true;
			}
		}
		return false;
	}
	
	

}
