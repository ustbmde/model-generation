package edu.ustb.sei.mde.mt4mt.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.modeling.ui.ConsoleUtil;
import edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite;
import edu.ustb.sei.mde.mt4mt.CustomizableTestCase;
import edu.ustb.sei.mde.mt4mt.GetPutTestCase;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.PutGetTestCase;
import edu.ustb.sei.mde.mt4mt.TestCase;
import edu.ustb.sei.mde.mt4mt.TestSuite;
import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;
import edu.ustb.sei.mt4mt.base.test.AutoExecutorTestRunner;
import edu.ustb.sei.mt4mt.base.test.CustomizableTransformationTestCase;
import edu.ustb.sei.mt4mt.base.test.TestGetPut;
import edu.ustb.sei.mt4mt.base.test.TestPutGet;
import edu.ustb.sei.mt4mt.base.test.TestRunner;

public class RunTest extends AbstractEMFAction {

	public RunTest() {
		super("Run Test");
	}
	
	private List<TestCase> selectedTests = new UniqueEList<TestCase>();
	private TestSuite selectedTestSuite;
	private String title = null;
	
	public String getTitle() {
		if(title==null) {
			if(selectedTests.size()>1) return "Testing Task for "+selectedTests.size()+" Tests";
			else return "Testing Task for 1 Test";
		} else
			return title;
	}

	@Override
	protected boolean updateSelection(IStructuredSelection selection) {
		selectedTests.clear();
		selectedTestSuite = null;
		
		@SuppressWarnings("unchecked")
		Iterator<? extends Object> it = selection.iterator();
		while(it.hasNext()) {
			Object o = it.next();
			if(o instanceof TestCase) {
				selectedTests.add((TestCase)o);
			} else if(o instanceof TestSuite){
				if(selection.size()>1) {
					selectedTests.clear();
					break;
				} else {
					selectedTestSuite = (TestSuite) o;
					for(TestCase c : ((TestSuite) o).getTestCases()) {
						selectedTests.add(c);
					}
					title = ((TestSuite) o).getName();
					title = title==null? "Unnamed Test Suite" : title;
				}
			}
		}
		
		return selectedTests.size()!=0;
	}
	
	@Override
	public void run() {
		IFile file = null;
		if(selectedTestSuite!=null) {
			String uri = selectedTestSuite.eResource().getURI().toPlatformString(true);
			uri = uri.substring(0,uri.lastIndexOf(".mt4mt"))+".log";
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			file = root.getFile(new Path(uri));
			if(file.exists()) {
				try {
					file.delete(true, null);
				} catch (CoreException e) {
				}
			}
		}
		
		TestRunner runner = null;
		
		if(selectedTestSuite==null || selectedTestSuite.eClass()==MT4MTPackage.Literals.TEST_SUITE) {
			runner = new TestRunner(ConsoleUtil.getMessageWriter("MT4MT"), file, getTitle());
		} else {
			EList<String> executors = ((AutoExecutorTestSuite)selectedTestSuite).getExecutors();
			EList<String> executables = ((AutoExecutorTestSuite)selectedTestSuite).getExecutables();
			runner = new AutoExecutorTestRunner(ConsoleUtil.getMessageWriter("MT4MT"), file, getTitle(), 
					executors.toArray(new String[executors.size()]), 
					executables.toArray(new String[executables.size()]), 
					((AutoExecutorTestSuite)selectedTestSuite).getExecutorAttributeName());
		}
		
		for(TestCase tc : selectedTests) {
			try {
				AbstractTransformationTestCase test = tc.build();
				runner.addTest(test);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			new ProgressMonitorDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell()).run(true, true, 
					new TestRunnable(runner));
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	class TestRunnable implements IRunnableWithProgress {
		private TestRunner runner;
		public TestRunnable(TestRunner runner) {
			this.runner = runner;
		}
		@Override
		public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
			if(runner instanceof AutoExecutorTestRunner) {
				monitor.beginTask("Testing", runner.getTaskCount());
			} else {
				
			}
			
			runner.run(monitor);
		}
		
	}
}
