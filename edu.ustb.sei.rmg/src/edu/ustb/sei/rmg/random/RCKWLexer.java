package edu.ustb.sei.rmg.random;

import lpg.runtime.*;

public class RCKWLexer extends RCKWLexerprs
{
    private char[] inputChars;
    private final int keywordKind[] = new int[13 + 1];

    public int[] getKeywordKinds() { return keywordKind; }

    public int lexer(int curtok, int lasttok)
    {
        int current_kind = getKind(inputChars[curtok]),
            act;

        for (act = tAction(START_STATE, current_kind);
             act > NUM_RULES && act < ACCEPT_ACTION;
             act = tAction(act, current_kind))
        {
            curtok++;
            current_kind = (curtok > lasttok
                                   ? Char_EOF
                                   : getKind(inputChars[curtok]));
        }

        if (act > ERROR_ACTION)
        {
            curtok++;
            act -= ERROR_ACTION;
        }

        return keywordKind[act == ERROR_ACTION  || curtok <= lasttok ? 0 : act];
    }

    public void setInputChars(char[] inputChars) { this.inputChars = inputChars; }


    final static int tokenKind[] = new int[128];
    static
    {
        tokenKind['$'] = RCKWLexersym.Char_DollarSign;
        tokenKind['%'] = RCKWLexersym.Char_Percent;
        tokenKind['_'] = RCKWLexersym.Char__;

        tokenKind['a'] = RCKWLexersym.Char_a;
        tokenKind['b'] = RCKWLexersym.Char_b;
        tokenKind['c'] = RCKWLexersym.Char_c;
        tokenKind['d'] = RCKWLexersym.Char_d;
        tokenKind['e'] = RCKWLexersym.Char_e;
        tokenKind['f'] = RCKWLexersym.Char_f;
        tokenKind['g'] = RCKWLexersym.Char_g;
        tokenKind['h'] = RCKWLexersym.Char_h;
        tokenKind['i'] = RCKWLexersym.Char_i;
        tokenKind['j'] = RCKWLexersym.Char_j;
        tokenKind['k'] = RCKWLexersym.Char_k;
        tokenKind['l'] = RCKWLexersym.Char_l;
        tokenKind['m'] = RCKWLexersym.Char_m;
        tokenKind['n'] = RCKWLexersym.Char_n;
        tokenKind['o'] = RCKWLexersym.Char_o;
        tokenKind['p'] = RCKWLexersym.Char_p;
        tokenKind['q'] = RCKWLexersym.Char_q;
        tokenKind['r'] = RCKWLexersym.Char_r;
        tokenKind['s'] = RCKWLexersym.Char_s;
        tokenKind['t'] = RCKWLexersym.Char_t;
        tokenKind['u'] = RCKWLexersym.Char_u;
        tokenKind['v'] = RCKWLexersym.Char_v;
        tokenKind['w'] = RCKWLexersym.Char_w;
        tokenKind['x'] = RCKWLexersym.Char_x;
        tokenKind['y'] = RCKWLexersym.Char_y;
        tokenKind['z'] = RCKWLexersym.Char_z;

        tokenKind['A'] = RCKWLexersym.Char_A;
        tokenKind['B'] = RCKWLexersym.Char_B;
        tokenKind['C'] = RCKWLexersym.Char_C;
        tokenKind['D'] = RCKWLexersym.Char_D;
        tokenKind['E'] = RCKWLexersym.Char_E;
        tokenKind['F'] = RCKWLexersym.Char_F;
        tokenKind['G'] = RCKWLexersym.Char_G;
        tokenKind['H'] = RCKWLexersym.Char_H;
        tokenKind['I'] = RCKWLexersym.Char_I;
        tokenKind['J'] = RCKWLexersym.Char_J;
        tokenKind['K'] = RCKWLexersym.Char_K;
        tokenKind['L'] = RCKWLexersym.Char_L;
        tokenKind['M'] = RCKWLexersym.Char_M;
        tokenKind['N'] = RCKWLexersym.Char_N;
        tokenKind['O'] = RCKWLexersym.Char_O;
        tokenKind['P'] = RCKWLexersym.Char_P;
        tokenKind['Q'] = RCKWLexersym.Char_Q;
        tokenKind['R'] = RCKWLexersym.Char_R;
        tokenKind['S'] = RCKWLexersym.Char_S;
        tokenKind['T'] = RCKWLexersym.Char_T;
        tokenKind['U'] = RCKWLexersym.Char_U;
        tokenKind['V'] = RCKWLexersym.Char_V;
        tokenKind['W'] = RCKWLexersym.Char_W;
        tokenKind['X'] = RCKWLexersym.Char_X;
        tokenKind['Y'] = RCKWLexersym.Char_Y;
        tokenKind['Z'] = RCKWLexersym.Char_Z;
    };

    final int getKind(char c)
    {
        return (((c & 0xFFFFFF80) == 0) /* 0 <= c < 128? */ ? tokenKind[c] : 0);
    }


    public RCKWLexer(char[] inputChars, int identifierKind)
    {
        this.inputChars = inputChars;
        keywordKind[0] = identifierKind;

        //
        // Rule 1:  KeyWord ::= l i s t
        //
        
		keywordKind[1] = (RCParsersym.TK_list);
	  
	
        //
        // Rule 2:  KeyWord ::= s e p a r a t e
        //
        
		keywordKind[2] = (RCParsersym.TK_separate);
	  
	
        //
        // Rule 3:  KeyWord ::= n o r m
        //
        
		keywordKind[3] = (RCParsersym.TK_norm);
	  
	
        //
        // Rule 4:  KeyWord ::= b e r n
        //
        
		keywordKind[4] = (RCParsersym.TK_bern);
	  
	
        //
        // Rule 5:  KeyWord ::= g e o m
        //
        
		keywordKind[5] = (RCParsersym.TK_geom);
	  
	
        //
        // Rule 6:  KeyWord ::= p o i s
        //
        
		keywordKind[6] = (RCParsersym.TK_pois);
	  
	
        //
        // Rule 7:  KeyWord ::= p a r e
        //
        
		keywordKind[7] = (RCParsersym.TK_pare);
	  
	
        //
        // Rule 8:  KeyWord ::= c a u c
        //
        
		keywordKind[8] = (RCParsersym.TK_cauc);
	  
	
        //
        // Rule 9:  KeyWord ::= t r u e
        //
        
		keywordKind[9] = (RCParsersym.TK_true);
	  
	
        //
        // Rule 10:  KeyWord ::= f a l s e
        //
        
		keywordKind[10] = (RCParsersym.TK_false);
	  
	
        //
        // Rule 11:  KeyWord ::= u n i q u e
        //
        
		keywordKind[11] = (RCParsersym.TK_unique);
	  
	
        //
        // Rule 12:  KeyWord ::= f o r
        //
        
		keywordKind[12] = (RCParsersym.TK_for);
	  
	
        //
        // Rule 13:  KeyWord ::= a l l
        //
        
		keywordKind[13] = (RCParsersym.TK_all);
	  
	
        for (int i = 0; i < keywordKind.length; i++)
        {
            if (keywordKind[i] == 0)
                keywordKind[i] = identifierKind;
        }
    }
}

