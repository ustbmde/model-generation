/**
 */
package edu.ustb.sei.rmg.structuralrmg.impl;

import edu.ustb.sei.rmg.structuralrmg.BindingMapping;
import edu.ustb.sei.rmg.structuralrmg.EndpointBinding;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Endpoint Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl#getEdge <em>Edge</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl#getSourceMappings <em>Source Mappings</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl#getTargetMappings <em>Target Mappings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EndpointBindingImpl extends MinimalEObjectImpl.Container implements EndpointBinding {
	/**
	 * The cached value of the '{@link #getSourceMappings() <em>Source Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<BindingMapping> sourceMappings;

	/**
	 * The cached value of the '{@link #getTargetMappings() <em>Target Mappings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetMappings()
	 * @generated
	 * @ordered
	 */
	protected EList<BindingMapping> targetMappings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EndpointBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructuralRmgPackage.Literals.ENDPOINT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateEdge getEdge() {
		if (eContainerFeatureID() != StructuralRmgPackage.ENDPOINT_BINDING__EDGE) return null;
		return (TemplateEdge)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEdge(TemplateEdge newEdge, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newEdge, StructuralRmgPackage.ENDPOINT_BINDING__EDGE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEdge(TemplateEdge newEdge) {
		if (newEdge != eInternalContainer() || (eContainerFeatureID() != StructuralRmgPackage.ENDPOINT_BINDING__EDGE && newEdge != null)) {
			if (EcoreUtil.isAncestor(this, newEdge))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newEdge != null)
				msgs = ((InternalEObject)newEdge).eInverseAdd(this, StructuralRmgPackage.TEMPLATE_EDGE__BINDING, TemplateEdge.class, msgs);
			msgs = basicSetEdge(newEdge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.ENDPOINT_BINDING__EDGE, newEdge, newEdge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BindingMapping> getSourceMappings() {
		if (sourceMappings == null) {
			sourceMappings = new EObjectContainmentEList<BindingMapping>(BindingMapping.class, this, StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS);
		}
		return sourceMappings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BindingMapping> getTargetMappings() {
		if (targetMappings == null) {
			targetMappings = new EObjectContainmentEList<BindingMapping>(BindingMapping.class, this, StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS);
		}
		return targetMappings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetEdge((TemplateEdge)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				return basicSetEdge(null, msgs);
			case StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS:
				return ((InternalEList<?>)getSourceMappings()).basicRemove(otherEnd, msgs);
			case StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS:
				return ((InternalEList<?>)getTargetMappings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				return eInternalContainer().eInverseRemove(this, StructuralRmgPackage.TEMPLATE_EDGE__BINDING, TemplateEdge.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				return getEdge();
			case StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS:
				return getSourceMappings();
			case StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS:
				return getTargetMappings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				setEdge((TemplateEdge)newValue);
				return;
			case StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS:
				getSourceMappings().clear();
				getSourceMappings().addAll((Collection<? extends BindingMapping>)newValue);
				return;
			case StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS:
				getTargetMappings().clear();
				getTargetMappings().addAll((Collection<? extends BindingMapping>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				setEdge((TemplateEdge)null);
				return;
			case StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS:
				getSourceMappings().clear();
				return;
			case StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS:
				getTargetMappings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.ENDPOINT_BINDING__EDGE:
				return getEdge() != null;
			case StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS:
				return sourceMappings != null && !sourceMappings.isEmpty();
			case StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS:
				return targetMappings != null && !targetMappings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EndpointBindingImpl
