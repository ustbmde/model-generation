/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * The StructuralrmgTaskItemBuilder is used to find task items in text documents.
 * The current implementation uses the generated lexer and the TaskItemDetector to
 * detect task items. This class is called by the BuilderAdapter, which runs both
 * this builder and the default builder that is intended to be customized.
 */
public class StructuralrmgTaskItemBuilder {
	
	public void build(org.eclipse.core.resources.IFile resource, org.eclipse.emf.ecore.resource.ResourceSet resourceSet, org.eclipse.core.runtime.IProgressMonitor monitor) {
		monitor.setTaskName("Searching for task items");
		new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgMarkerHelper().removeAllMarkers(resource, org.eclipse.core.resources.IMarker.TASK);
		if (isInBinFolder(resource)) {
			return;
		}
		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTaskItem> taskItems = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTaskItem>();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTaskItemDetector taskItemDetector = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTaskItemDetector();
		try {
			java.io.InputStream inputStream = resource.getContents();
			String content = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStreamUtil.getContent(inputStream);
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextScanner lexer = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgMetaInformation().createLexer();
			lexer.setText(content);
			
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextToken nextToken = lexer.getNextToken();
			while (nextToken != null) {
				String text = nextToken.getText();
				taskItems.addAll(taskItemDetector.findTaskItems(text, nextToken.getLine(), nextToken.getOffset()));
				nextToken = lexer.getNextToken();
			}
		} catch (java.io.IOException e) {
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgPlugin.logError("Exception while searching for task items", e);
		} catch (org.eclipse.core.runtime.CoreException e) {
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgPlugin.logError("Exception while searching for task items", e);
		}
		
		for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTaskItem taskItem : taskItems) {
			java.util.Map<String, Object> markerAttributes = new java.util.LinkedHashMap<String, Object>();
			markerAttributes.put(org.eclipse.core.resources.IMarker.USER_EDITABLE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.DONE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.LINE_NUMBER, taskItem.getLine());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_START, taskItem.getCharStart());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_END, taskItem.getCharEnd());
			markerAttributes.put(org.eclipse.core.resources.IMarker.MESSAGE, taskItem.getMessage());
			new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgMarkerHelper().createMarker(resource, org.eclipse.core.resources.IMarker.TASK, markerAttributes);
		}
	}
	
	public String getBuilderMarkerId() {
		return org.eclipse.core.resources.IMarker.TASK;
	}
	
	public boolean isInBinFolder(org.eclipse.core.resources.IFile resource) {
		org.eclipse.core.resources.IContainer parent = resource.getParent();
		while (parent != null) {
			if ("bin".equals(parent.getName())) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
	
}
