package edu.ustb.sei.egraph.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class ConcurrentArrayList<E> extends ArrayList<E> {
	public ConcurrentArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	private static final long serialVersionUID = -197854316712662825L;
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	
	@Override
	public boolean add(E e) {
		boolean ret = false;
		
		lock.writeLock().lock();
		ret = super.add(e);
		lock.writeLock().unlock();
		
		return ret;
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean ret = false;
		
		lock.writeLock().lock();
		try{
			ret = super.addAll(c);
		}catch(Exception e) {
			ret = false;
		}
		lock.writeLock().unlock();
		
		return ret;
	}
	
	@Override
	public void clear() {
		lock.writeLock().lock();
		super.clear();
		lock.writeLock().unlock();
	}
	
	@Override
	public boolean contains(Object o) {
		boolean ret = false;
		
		lock.readLock().lock();
		ret = super.contains(o);
		lock.readLock().unlock();
		
		return ret;
	}
	
	@Override
	public void add(int index, E element) {
		lock.writeLock().lock();
		try {
			super.add(index, element);
		} catch(Exception e) {
		}
		lock.writeLock().unlock();
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		boolean ret = false;
		
		lock.writeLock().lock();
		try {
			ret = super.addAll(index, c);
		} catch(Exception e) {
			ret = false;
		}
		lock.writeLock().unlock();
		
		return ret;
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		boolean ret = false;
		
		lock.readLock().lock();
		try {
			ret = super.containsAll(c);
		} catch(Exception e) {
			ret = false;
		}
		lock.readLock().unlock();
		
		return ret;
	}
	
	@Override
	public void forEach(Consumer<? super E> action) {
		lock.readLock().lock();
		try {
			super.forEach(action);
		} catch(Exception e) {}
		lock.readLock().unlock();
	}
	
	@Override
	public E get(int index) {
		E e = null;
		
		lock.readLock().lock();
		
		try {
		e = super.get(index);
		} catch(Exception ee) {
			e = null;
		}
		
		lock.readLock().unlock();
		
		return e;
	}
	
	@Override
	public int indexOf(Object o) {
		int i = -1;
		lock.readLock().lock();
		i = super.indexOf(o);
		lock.readLock().unlock();
		return i;
	}
	
	@Override
	public boolean isEmpty() {
		boolean b = false;
		lock.readLock().lock();
		b = super.isEmpty();
		lock.readLock().unlock();
		return b;
	}
	
	@Override
	public E remove(int index) {
		E e = null;
		lock.writeLock().lock();
		try {
			e = super.remove(index);
		} catch(Exception ee) {
			e = null;
		}
		lock.writeLock().unlock();
		return e;
	}
	
	@Override
	public boolean remove(Object o) {
		boolean b = false;
		
		lock.writeLock().lock();
		try {
			b = super.remove(o);
		} catch(Exception e) {
		}
		lock.writeLock().unlock();
		
		return b;
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean b = false;
		lock.writeLock().lock();
		try {
			b = super.removeAll(c);
		} catch(Exception e) {
		}
		lock.writeLock().unlock();
		
		return b;
	}
	
	@Override
	public int lastIndexOf(Object o) {
		int i = -1;
		lock.readLock().lock();
		i = super.lastIndexOf(o);
		lock.readLock().unlock();
		return i;
	}
	
	@Override
	protected void removeRange(int fromIndex, int toIndex) {
		lock.writeLock().lock();
		try {
			super.removeRange(fromIndex, toIndex);
		} catch(Exception e) {
		}
		lock.writeLock().unlock();
	}
	
	@Override
	public boolean removeIf(Predicate<? super E> filter) {
		boolean b = false;
		lock.writeLock().lock();
		b = super.removeIf(filter);
		lock.writeLock().unlock();
		return b;
	}
	
	@Override
	public void replaceAll(UnaryOperator<E> operator) {
		lock.writeLock().lock();
		super.replaceAll(operator);
		lock.writeLock().unlock();
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		boolean b = false;
		lock.writeLock().lock();
		b = super.retainAll(c);
		lock.writeLock().unlock();
		return b;
	}
	
	@Override
	public E set(int index, E element) {
		E e= null;
		lock.writeLock().lock();
		try {
			e = super.set(index, element);
		} catch(Exception ee){}
		lock.writeLock().unlock();
		return e;
	}
	
	@Override
	public int size() {
		int i = -1;
		lock.readLock().lock();
		i =  super.size();
		lock.readLock().unlock();
		return i;
	}
	
	@Override
	public void sort(Comparator<? super E> c) {
		lock.writeLock().lock();
		super.sort(c);
		lock.writeLock().unlock();
	}
	
	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		List<E> list = null;
		lock.readLock().lock();
		try {
			list = super.subList(fromIndex, toIndex);
		} catch(Exception e){}
		lock.readLock().unlock();
		return list;
	}
	@Override
	public Iterator<E> iterator() {
		return super.iterator();
	}
}
