/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgOutlinePageActionProvider {
	
	public java.util.List<org.eclipse.jface.action.IAction> getActions(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		java.util.List<org.eclipse.jface.action.IAction> defaultActions = new java.util.ArrayList<org.eclipse.jface.action.IAction>();
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
