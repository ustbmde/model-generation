/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Reference Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceUnique <em>Source Unique</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceRequired <em>Source Required</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetUnique <em>Target Unique</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetRequired <em>Target Required</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTransitivity <em>Transitivity</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isRing <em>Ring</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isCircle <em>Circle</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#getExternalObjects <em>External Objects</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption()
 * @model abstract="true"
 * @generated
 */
public interface AbstractReferenceOption extends ScalableCondition {
	/**
	 * Returns the value of the '<em><b>Source Unique</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Unique</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Unique</em>' attribute.
	 * @see #setSourceUnique(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_SourceUnique()
	 * @model default="false"
	 * @generated
	 */
	boolean isSourceUnique();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceUnique <em>Source Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Unique</em>' attribute.
	 * @see #isSourceUnique()
	 * @generated
	 */
	void setSourceUnique(boolean value);

	/**
	 * Returns the value of the '<em><b>Source Required</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Required</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Required</em>' attribute.
	 * @see #setSourceRequired(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_SourceRequired()
	 * @model default="false"
	 * @generated
	 */
	boolean isSourceRequired();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceRequired <em>Source Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Required</em>' attribute.
	 * @see #isSourceRequired()
	 * @generated
	 */
	void setSourceRequired(boolean value);

	/**
	 * Returns the value of the '<em><b>Target Unique</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Unique</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Unique</em>' attribute.
	 * @see #setTargetUnique(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_TargetUnique()
	 * @model default="false"
	 * @generated
	 */
	boolean isTargetUnique();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetUnique <em>Target Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Unique</em>' attribute.
	 * @see #isTargetUnique()
	 * @generated
	 */
	void setTargetUnique(boolean value);

	/**
	 * Returns the value of the '<em><b>Target Required</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Required</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Required</em>' attribute.
	 * @see #setTargetRequired(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_TargetRequired()
	 * @model default="false"
	 * @generated
	 */
	boolean isTargetRequired();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetRequired <em>Target Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Required</em>' attribute.
	 * @see #isTargetRequired()
	 * @generated
	 */
	void setTargetRequired(boolean value);

	/**
	 * Returns the value of the '<em><b>Transitivity</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitivity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitivity</em>' attribute.
	 * @see #setTransitivity(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_Transitivity()
	 * @model default="false"
	 * @generated
	 */
	boolean isTransitivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTransitivity <em>Transitivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transitivity</em>' attribute.
	 * @see #isTransitivity()
	 * @generated
	 */
	void setTransitivity(boolean value);

	/**
	 * Returns the value of the '<em><b>Ring</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ring</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ring</em>' attribute.
	 * @see #setRing(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_Ring()
	 * @model default="true"
	 * @generated
	 */
	boolean isRing();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isRing <em>Ring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ring</em>' attribute.
	 * @see #isRing()
	 * @generated
	 */
	void setRing(boolean value);

	/**
	 * Returns the value of the '<em><b>Circle</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Circle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Circle</em>' attribute.
	 * @see #setCircle(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_Circle()
	 * @model default="true"
	 * @generated
	 */
	boolean isCircle();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isCircle <em>Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Circle</em>' attribute.
	 * @see #isCircle()
	 * @generated
	 */
	void setCircle(boolean value);

	/**
	 * Returns the value of the '<em><b>External Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Objects</em>' reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAbstractReferenceOption_ExternalObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getExternalObjects();

} // AbstractReferenceOption
