package edu.ustb.sei.rmg.random.condition;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

public class ValueLazyGenerationRequest extends RuntimeException {
	private static final long serialVersionUID = 3927024122625416733L;
	private Condition<?> requester = null;
	private EObject object = null;
	private EAttribute attribute = null;
	private int[] pair = null; 
	
	public int[] getPair() {
		return pair;
	}

	public void setPair(int[] pair) {
		this.pair = pair;
	}

	public Condition<?> getRequester() {
		return requester;
	}

	public void setRequester(Condition<?> requester) {
		this.requester = requester;
	}

	public EObject getObject() {
		return object;
	}

	public void setObject(EObject object) {
		this.object = object;
	}

	public EAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(EAttribute attribute) {
		this.attribute = attribute;
	}
	
	public ValueLazyGenerationRequest(Condition<?> requester, EObject object,
			EAttribute attribute) {
		super();
		this.requester = requester;
		this.object = object;
		this.attribute = attribute;
	}

	public ValueLazyGenerationRequest(ValueLazyGenerationRequest parent, Condition<?> requester, EObject object,
			EAttribute attribute) {
		super(parent);
		this.requester = requester;
		this.object = object;
		this.attribute = attribute;
	}
	
	
}
