package edu.ustb.sei.egraph.generator.tree.function;

public abstract class AssignmentFunction<NT,ET,NAT,EAT> {
	protected String featureName;
	protected int lower;
	protected int upper;
	
	public AssignmentFunction(String feature) {
		this.featureName = feature;
		lower = 1;
		upper = 1;
	}
	
	public AssignmentFunction(String feature,int lower) {
		this.featureName = feature;
		this.lower = lower;
		this.upper = lower;
	}
	
	public AssignmentFunction(String feature,int lower,int upper) {
		this.featureName = feature;
		this.lower = lower;
		this.upper = upper;
	}
	
	public boolean isLazy() {
		return false;
	}
	
	public abstract Integer apply(TreeGenerationService<NT,ET,NAT,EAT> service);
}
