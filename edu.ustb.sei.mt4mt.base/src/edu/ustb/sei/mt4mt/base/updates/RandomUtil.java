package edu.ustb.sei.mt4mt.base.updates;

import java.util.List;
import java.util.Random;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;

public class RandomUtil {
	private Random random = new Random();
	
	/**
	 * @param threshold must be in [0.0, 1.0) 
	 */
	public boolean decision(double threshold) {
		return random.nextDouble() < threshold;
	}
	
	public int integer() {
		return random.nextInt();
	}
	
	public int unsignedInteger() {
		return Math.abs(integer());
	}
	
	public int unsignedInteger(int bound) {
		return random.nextInt(bound);
	}
	
	public boolean bool() {
		return decision(0.5);
	}
	
	final static char[] charSet = new char[]{
		'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
	};
	
	public String identifier(int length) {
		
		char[] buf = new char[length];
		
		for(int i=0;i<length;i++) {
			buf[i] = charSet[unsignedInteger(charSet.length)];
		}
		
		return new String(buf);
	}
	
	public EEnumLiteral enumLiteral(EEnum e) {
		List<EEnumLiteral> literals = e.getELiterals();
		return element(literals);
	}
	
	public <T> T element(T[] arr) {
		if(arr.length==0) return null;
		return arr[unsignedInteger(arr.length)];
	}
	
	public <T> T element(List<? extends T> list) {
		if(list.size()==0) return null;
		return list.get(unsignedInteger(list.size()));
	}
}
