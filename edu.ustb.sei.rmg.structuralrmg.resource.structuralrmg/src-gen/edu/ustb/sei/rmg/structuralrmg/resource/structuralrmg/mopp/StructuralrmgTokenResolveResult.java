/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A basic implementation of the ITokenResolveResult interface.
 */
public class StructuralrmgTokenResolveResult implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult {
	
	private String errorMessage;
	private Object resolvedToken;
	
	public StructuralrmgTokenResolveResult() {
		super();
		clear();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Object getResolvedToken() {
		return resolvedToken;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void setResolvedToken(Object resolvedToken) {
		this.resolvedToken = resolvedToken;
	}
	
	public void clear() {
		errorMessage = "Can't resolve token.";
		resolvedToken = null;
	}
	
}
