/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.structuralrmg.*;

public class TemplateEdgeReferenceReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.TemplateEdge container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<org.eclipse.emf.ecore.EReference> result) {
		if(identifier ==null || container==null || !identifier.contains("::"))
			return;
		
		String[] path = identifier.split("::");
		
		TemplateModel m = ResolverUtil.getModel(container);
		
		if(m==null) return;
		
		EClass cls = ResolverUtil.searchClass(path[0],m.getPackages());
		
		if(cls==null) return;
		
		for(EReference ref : cls.getEAllReferences()) {
			if(ResolverUtil.matchName(path[1], ref.getName())) {
				result.addMapping(identifier, ref);
			}
		}
	}
	
	public String deResolve(org.eclipse.emf.ecore.EReference element, edu.ustb.sei.rmg.structuralrmg.TemplateEdge container, org.eclipse.emf.ecore.EReference reference) {
		return ((EClass)element.eContainer()).getName()+"::"+element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
