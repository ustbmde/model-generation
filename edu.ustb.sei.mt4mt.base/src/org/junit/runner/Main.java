package org.junit.runner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.internal.JUnitSystem;
import org.junit.internal.RealSystem;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCommandLineParseResult;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

import edu.ustb.sei.mt4mt.base.test.Test;
import junit.framework.TestCase;
import junit.runner.Version;

public class Main {

	public static void main(String[] args) throws Exception {
//		UITestRunListener
//		Test t = new Test();
//		t.setUp();
//		t.test();
		
		Class<?> cls = new ClassLoader() {
			public java.lang.Class<?> loadClass(String name) throws ClassNotFoundException {
				try {
					Class<?> c = super.loadClass(name);
					return c;
				} catch(Exception e) {}
				
				String clazzName = name.substring(name.lastIndexOf('/')+1,name.lastIndexOf(".class"));
				String dir = name.substring(0,name.lastIndexOf('/')+1);
				String fileName = dir+clazzName.replace('.', '/').concat(".class");
				
				
				Class<?> c = findLoadedClass(clazzName);
				
				if(c==null) {
					try {
						if(getParent()!=null) {
							c = getParent().loadClass(clazzName);
						}
					}catch(ClassNotFoundException e) {
						
					}
					
					if(c==null) {
						try {
							InputStream is = new FileInputStream(fileName);
							byte[] b = new byte[is.available()];
							is.read(b);
							is.close();
							return defineClass(clazzName, b, 0, b.length);
						} catch(IOException e) {
							throw new ClassNotFoundException(clazzName);
						}
					}
				}
				return c;
			}
		}.loadClass("/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.model/bin/edu.ustb.sei.mde.mt4mt.MyTest.class");
		cls.getMethod("echo").invoke(cls.newInstance());
		
//		JUnitCore core = new JUnitCore().runMain(new RealSystem(), Test.class.getName());
	}

}
