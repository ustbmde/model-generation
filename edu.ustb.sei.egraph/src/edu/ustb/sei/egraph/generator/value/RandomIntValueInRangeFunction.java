package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomIntValueInRangeFunction extends RandomNumValueInRangeFunction<Integer> {
	
	public RandomIntValueInRangeFunction(Integer min, Integer max) {
		super(min,max);
	}

	@Override
	public Integer getValue(RandomService t) {
		if(max!=null)
			return t.randomInt(min, max+1);
		else
			return Math.abs(t.randomInt())+min;
	}

}
