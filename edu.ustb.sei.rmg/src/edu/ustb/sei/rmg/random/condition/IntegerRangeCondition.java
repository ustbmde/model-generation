package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class IntegerRangeCondition extends RangeCondition<Integer> {

	@Override
	protected Integer randomLU(RandomUtil r) {
		return r.randomInt(lower, upper);
	}

	@Override
	protected Integer randomL(RandomUtil r) {
		// TODO Auto-generated method stub
		return r.randomIntL(lower);
	}

	@Override
	protected Integer randomU(RandomUtil r) {
		// TODO Auto-generated method stub
		return r.randomIntU(upper);
	}


}
