package edu.ustb.sei.mt4mt.base.actions;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

abstract public class AbstractExecutor implements IAction{
	abstract public void beforeRun(Context context);
	abstract public void actualRun(Context context);
	abstract public void afterRun(Context context);
	
	@Override
	public void run(Context context) {
		beforeRun(context);
		actualRun(context);
		afterRun(context);
	}
}
