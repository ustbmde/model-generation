package edu.ustb.sei.mt4mt.base.updates;

import java.util.Map;

import edu.ustb.sei.mt4mt.base.Context;

public interface IExternalAction {
	void execute(Map<String,String> parameters, String result, Context context) throws Exception;
}
