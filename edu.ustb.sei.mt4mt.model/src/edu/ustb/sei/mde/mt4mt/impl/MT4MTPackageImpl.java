/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction;
import edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction;
import edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite;
import edu.ustb.sei.mde.mt4mt.ClearAction;
import edu.ustb.sei.mde.mt4mt.CompareModelAction;
import edu.ustb.sei.mde.mt4mt.CopyModelAction;
import edu.ustb.sei.mde.mt4mt.CustomizableTestCase;
import edu.ustb.sei.mde.mt4mt.DiffKind;
import edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction;
import edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction;
import edu.ustb.sei.mde.mt4mt.ExternalAction;
import edu.ustb.sei.mde.mt4mt.ExternalActionParameter;
import edu.ustb.sei.mde.mt4mt.GetPutTestCase;
import edu.ustb.sei.mde.mt4mt.LoadModelAction;
import edu.ustb.sei.mde.mt4mt.MT4MTFactory;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase;
import edu.ustb.sei.mde.mt4mt.PredefinedTestCase;
import edu.ustb.sei.mde.mt4mt.PutGetTestCase;
import edu.ustb.sei.mde.mt4mt.SaveModelAction;
import edu.ustb.sei.mde.mt4mt.SequenceAction;
import edu.ustb.sei.mde.mt4mt.SetVariableAction;
import edu.ustb.sei.mde.mt4mt.TestAction;
import edu.ustb.sei.mde.mt4mt.TestCase;
import edu.ustb.sei.mde.mt4mt.TestSuite;

import edu.ustb.sei.mde.mt4mt.UpdateModelAction;
import edu.ustb.sei.mde.mt4mt.UserAction;
import edu.ustb.sei.mde.mt4mt.WrapperTestCase;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MT4MTPackageImpl extends EPackageImpl implements MT4MTPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testSuiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customizableTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass predefinedTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getPutTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass putGetTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass predefinedBXTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loadModelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass saveModelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executeLauncherActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass evaluateOCLQueryActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyModelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compareModelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertComparisonResultActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertOCLInvariantActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setVariableActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass updateModelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wrapperTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass autoExecutorTestSuiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clearActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalActionParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum diffKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iActionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType abstractTransformationTestCaseEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iExternalActionEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MT4MTPackageImpl() {
		super(eNS_URI, MT4MTFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MT4MTPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MT4MTPackage init() {
		if (isInited) return (MT4MTPackage)EPackage.Registry.INSTANCE.getEPackage(MT4MTPackage.eNS_URI);

		// Obtain or create and register package
		MT4MTPackageImpl theMT4MTPackage = (MT4MTPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MT4MTPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MT4MTPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theMT4MTPackage.createPackageContents();

		// Initialize created meta-data
		theMT4MTPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMT4MTPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MT4MTPackage.eNS_URI, theMT4MTPackage);
		return theMT4MTPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestSuite() {
		return testSuiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestSuite_Name() {
		return (EAttribute)testSuiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestSuite_TestCases() {
		return (EReference)testSuiteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestSuite_DefaultWaitingTime() {
		return (EAttribute)testSuiteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCase() {
		return testCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCase_Id() {
		return (EAttribute)testCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCase_Metamodels() {
		return (EAttribute)testCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTestCase__Build() {
		return testCaseEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTestCase__GetDefaultWaitingTime() {
		return testCaseEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomizableTestCase() {
		return customizableTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomizableTestCase_Action() {
		return (EReference)customizableTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPredefinedTestCase() {
		return predefinedTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedTestCase_Inputs() {
		return (EAttribute)predefinedTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetPutTestCase() {
		return getPutTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPutGetTestCase() {
		return putGetTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestAction() {
		return testActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTestAction__Build() {
		return testActionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPredefinedBXTestCase() {
		return predefinedBXTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_FwdLauncher() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_FwdSource() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_FwdView() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_BwdLauncher() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_BwdSource() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_BwdView() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPredefinedBXTestCase_BwdUpdatedSource() {
		return (EAttribute)predefinedBXTestCaseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequenceAction() {
		return sequenceActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequenceAction_Actions() {
		return (EReference)sequenceActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoadModelAction() {
		return loadModelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoadModelAction_ModelURI() {
		return (EAttribute)loadModelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLoadModelAction_Key() {
		return (EAttribute)loadModelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSaveModelAction() {
		return saveModelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSaveModelAction_ModelURI() {
		return (EAttribute)saveModelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSaveModelAction_Key() {
		return (EAttribute)saveModelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecuteLauncherAction() {
		return executeLauncherActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecuteLauncherAction_Launcher() {
		return (EAttribute)executeLauncherActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecuteLauncherAction_MaxWait() {
		return (EAttribute)executeLauncherActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvaluateOCLQueryAction() {
		return evaluateOCLQueryActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluateOCLQueryAction_ModelKey() {
		return (EAttribute)evaluateOCLQueryActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluateOCLQueryAction_Query() {
		return (EAttribute)evaluateOCLQueryActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluateOCLQueryAction_ResultKey() {
		return (EAttribute)evaluateOCLQueryActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvaluateOCLQueryAction_ImportContextVariables() {
		return (EAttribute)evaluateOCLQueryActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyModelAction() {
		return copyModelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyModelAction_FromURI() {
		return (EAttribute)copyModelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyModelAction_ToURI() {
		return (EAttribute)copyModelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompareModelAction() {
		return compareModelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompareModelAction_LeftURI() {
		return (EAttribute)compareModelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompareModelAction_RightURI() {
		return (EAttribute)compareModelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompareModelAction_DiffKey() {
		return (EAttribute)compareModelActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertComparisonResultAction() {
		return assertComparisonResultActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertComparisonResultAction_DiffKey() {
		return (EAttribute)assertComparisonResultActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertComparisonResultAction_DifferentCount() {
		return (EAttribute)assertComparisonResultActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertComparisonResultAction_Kind() {
		return (EAttribute)assertComparisonResultActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertOCLInvariantAction() {
		return assertOCLInvariantActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertOCLInvariantAction_ModelKey() {
		return (EAttribute)assertOCLInvariantActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertOCLInvariantAction_Invariant() {
		return (EAttribute)assertOCLInvariantActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertOCLInvariantAction_ImportContextVariables() {
		return (EAttribute)assertOCLInvariantActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserAction() {
		return userActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserAction_ClassFileURI() {
		return (EAttribute)userActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetVariableAction() {
		return setVariableActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSetVariableAction_Key() {
		return (EAttribute)setVariableActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSetVariableAction_Value() {
		return (EAttribute)setVariableActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUpdateModelAction() {
		return updateModelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUpdateModelAction_ModelKey() {
		return (EAttribute)updateModelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUpdateModelAction_UpdateOperations() {
		return (EAttribute)updateModelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWrapperTestCase() {
		return wrapperTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWrapperTestCase_BeforeTest() {
		return (EReference)wrapperTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWrapperTestCase_AfterTest() {
		return (EReference)wrapperTestCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWrapperTestCase_CoreTest() {
		return (EReference)wrapperTestCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWrapperTestCase_Repeat() {
		return (EAttribute)wrapperTestCaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAutoExecutorTestSuite() {
		return autoExecutorTestSuiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAutoExecutorTestSuite_Executors() {
		return (EAttribute)autoExecutorTestSuiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAutoExecutorTestSuite_Executables() {
		return (EAttribute)autoExecutorTestSuiteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAutoExecutorTestSuite_ExecutorAttributeName() {
		return (EAttribute)autoExecutorTestSuiteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClearAction() {
		return clearActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClearAction_Files() {
		return (EAttribute)clearActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalAction() {
		return externalActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalAction_Name() {
		return (EAttribute)externalActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalAction_ResultName() {
		return (EAttribute)externalActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExternalAction_Parameters() {
		return (EReference)externalActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalAction_Action() {
		return (EAttribute)externalActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalActionParameter() {
		return externalActionParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalActionParameter_Name() {
		return (EAttribute)externalActionParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExternalActionParameter_Value() {
		return (EAttribute)externalActionParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDiffKind() {
		return diffKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIAction() {
		return iActionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAbstractTransformationTestCase() {
		return abstractTransformationTestCaseEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIExternalAction() {
		return iExternalActionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTFactory getMT4MTFactory() {
		return (MT4MTFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		testSuiteEClass = createEClass(TEST_SUITE);
		createEAttribute(testSuiteEClass, TEST_SUITE__NAME);
		createEReference(testSuiteEClass, TEST_SUITE__TEST_CASES);
		createEAttribute(testSuiteEClass, TEST_SUITE__DEFAULT_WAITING_TIME);

		testCaseEClass = createEClass(TEST_CASE);
		createEAttribute(testCaseEClass, TEST_CASE__ID);
		createEAttribute(testCaseEClass, TEST_CASE__METAMODELS);
		createEOperation(testCaseEClass, TEST_CASE___BUILD);
		createEOperation(testCaseEClass, TEST_CASE___GET_DEFAULT_WAITING_TIME);

		customizableTestCaseEClass = createEClass(CUSTOMIZABLE_TEST_CASE);
		createEReference(customizableTestCaseEClass, CUSTOMIZABLE_TEST_CASE__ACTION);

		predefinedTestCaseEClass = createEClass(PREDEFINED_TEST_CASE);
		createEAttribute(predefinedTestCaseEClass, PREDEFINED_TEST_CASE__INPUTS);

		getPutTestCaseEClass = createEClass(GET_PUT_TEST_CASE);

		putGetTestCaseEClass = createEClass(PUT_GET_TEST_CASE);

		testActionEClass = createEClass(TEST_ACTION);
		createEOperation(testActionEClass, TEST_ACTION___BUILD);

		predefinedBXTestCaseEClass = createEClass(PREDEFINED_BX_TEST_CASE);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__FWD_SOURCE);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__FWD_VIEW);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__BWD_SOURCE);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__BWD_VIEW);
		createEAttribute(predefinedBXTestCaseEClass, PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE);

		sequenceActionEClass = createEClass(SEQUENCE_ACTION);
		createEReference(sequenceActionEClass, SEQUENCE_ACTION__ACTIONS);

		loadModelActionEClass = createEClass(LOAD_MODEL_ACTION);
		createEAttribute(loadModelActionEClass, LOAD_MODEL_ACTION__MODEL_URI);
		createEAttribute(loadModelActionEClass, LOAD_MODEL_ACTION__KEY);

		saveModelActionEClass = createEClass(SAVE_MODEL_ACTION);
		createEAttribute(saveModelActionEClass, SAVE_MODEL_ACTION__MODEL_URI);
		createEAttribute(saveModelActionEClass, SAVE_MODEL_ACTION__KEY);

		executeLauncherActionEClass = createEClass(EXECUTE_LAUNCHER_ACTION);
		createEAttribute(executeLauncherActionEClass, EXECUTE_LAUNCHER_ACTION__LAUNCHER);
		createEAttribute(executeLauncherActionEClass, EXECUTE_LAUNCHER_ACTION__MAX_WAIT);

		evaluateOCLQueryActionEClass = createEClass(EVALUATE_OCL_QUERY_ACTION);
		createEAttribute(evaluateOCLQueryActionEClass, EVALUATE_OCL_QUERY_ACTION__MODEL_KEY);
		createEAttribute(evaluateOCLQueryActionEClass, EVALUATE_OCL_QUERY_ACTION__QUERY);
		createEAttribute(evaluateOCLQueryActionEClass, EVALUATE_OCL_QUERY_ACTION__RESULT_KEY);
		createEAttribute(evaluateOCLQueryActionEClass, EVALUATE_OCL_QUERY_ACTION__IMPORT_CONTEXT_VARIABLES);

		copyModelActionEClass = createEClass(COPY_MODEL_ACTION);
		createEAttribute(copyModelActionEClass, COPY_MODEL_ACTION__FROM_URI);
		createEAttribute(copyModelActionEClass, COPY_MODEL_ACTION__TO_URI);

		compareModelActionEClass = createEClass(COMPARE_MODEL_ACTION);
		createEAttribute(compareModelActionEClass, COMPARE_MODEL_ACTION__LEFT_URI);
		createEAttribute(compareModelActionEClass, COMPARE_MODEL_ACTION__RIGHT_URI);
		createEAttribute(compareModelActionEClass, COMPARE_MODEL_ACTION__DIFF_KEY);

		assertComparisonResultActionEClass = createEClass(ASSERT_COMPARISON_RESULT_ACTION);
		createEAttribute(assertComparisonResultActionEClass, ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY);
		createEAttribute(assertComparisonResultActionEClass, ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT);
		createEAttribute(assertComparisonResultActionEClass, ASSERT_COMPARISON_RESULT_ACTION__KIND);

		assertOCLInvariantActionEClass = createEClass(ASSERT_OCL_INVARIANT_ACTION);
		createEAttribute(assertOCLInvariantActionEClass, ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY);
		createEAttribute(assertOCLInvariantActionEClass, ASSERT_OCL_INVARIANT_ACTION__INVARIANT);
		createEAttribute(assertOCLInvariantActionEClass, ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES);

		userActionEClass = createEClass(USER_ACTION);
		createEAttribute(userActionEClass, USER_ACTION__CLASS_FILE_URI);

		setVariableActionEClass = createEClass(SET_VARIABLE_ACTION);
		createEAttribute(setVariableActionEClass, SET_VARIABLE_ACTION__KEY);
		createEAttribute(setVariableActionEClass, SET_VARIABLE_ACTION__VALUE);

		updateModelActionEClass = createEClass(UPDATE_MODEL_ACTION);
		createEAttribute(updateModelActionEClass, UPDATE_MODEL_ACTION__MODEL_KEY);
		createEAttribute(updateModelActionEClass, UPDATE_MODEL_ACTION__UPDATE_OPERATIONS);

		wrapperTestCaseEClass = createEClass(WRAPPER_TEST_CASE);
		createEReference(wrapperTestCaseEClass, WRAPPER_TEST_CASE__BEFORE_TEST);
		createEReference(wrapperTestCaseEClass, WRAPPER_TEST_CASE__AFTER_TEST);
		createEReference(wrapperTestCaseEClass, WRAPPER_TEST_CASE__CORE_TEST);
		createEAttribute(wrapperTestCaseEClass, WRAPPER_TEST_CASE__REPEAT);

		autoExecutorTestSuiteEClass = createEClass(AUTO_EXECUTOR_TEST_SUITE);
		createEAttribute(autoExecutorTestSuiteEClass, AUTO_EXECUTOR_TEST_SUITE__EXECUTORS);
		createEAttribute(autoExecutorTestSuiteEClass, AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES);
		createEAttribute(autoExecutorTestSuiteEClass, AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME);

		clearActionEClass = createEClass(CLEAR_ACTION);
		createEAttribute(clearActionEClass, CLEAR_ACTION__FILES);

		externalActionEClass = createEClass(EXTERNAL_ACTION);
		createEAttribute(externalActionEClass, EXTERNAL_ACTION__NAME);
		createEAttribute(externalActionEClass, EXTERNAL_ACTION__RESULT_NAME);
		createEReference(externalActionEClass, EXTERNAL_ACTION__PARAMETERS);
		createEAttribute(externalActionEClass, EXTERNAL_ACTION__ACTION);

		externalActionParameterEClass = createEClass(EXTERNAL_ACTION_PARAMETER);
		createEAttribute(externalActionParameterEClass, EXTERNAL_ACTION_PARAMETER__NAME);
		createEAttribute(externalActionParameterEClass, EXTERNAL_ACTION_PARAMETER__VALUE);

		// Create enums
		diffKindEEnum = createEEnum(DIFF_KIND);

		// Create data types
		iActionEDataType = createEDataType(IACTION);
		abstractTransformationTestCaseEDataType = createEDataType(ABSTRACT_TRANSFORMATION_TEST_CASE);
		iExternalActionEDataType = createEDataType(IEXTERNAL_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		customizableTestCaseEClass.getESuperTypes().add(this.getTestCase());
		predefinedTestCaseEClass.getESuperTypes().add(this.getTestCase());
		getPutTestCaseEClass.getESuperTypes().add(this.getPredefinedBXTestCase());
		putGetTestCaseEClass.getESuperTypes().add(this.getPredefinedBXTestCase());
		predefinedBXTestCaseEClass.getESuperTypes().add(this.getPredefinedTestCase());
		sequenceActionEClass.getESuperTypes().add(this.getTestAction());
		loadModelActionEClass.getESuperTypes().add(this.getTestAction());
		saveModelActionEClass.getESuperTypes().add(this.getTestAction());
		executeLauncherActionEClass.getESuperTypes().add(this.getTestAction());
		evaluateOCLQueryActionEClass.getESuperTypes().add(this.getTestAction());
		copyModelActionEClass.getESuperTypes().add(this.getTestAction());
		compareModelActionEClass.getESuperTypes().add(this.getTestAction());
		assertComparisonResultActionEClass.getESuperTypes().add(this.getTestAction());
		assertOCLInvariantActionEClass.getESuperTypes().add(this.getTestAction());
		userActionEClass.getESuperTypes().add(this.getTestAction());
		setVariableActionEClass.getESuperTypes().add(this.getTestAction());
		updateModelActionEClass.getESuperTypes().add(this.getTestAction());
		wrapperTestCaseEClass.getESuperTypes().add(this.getTestCase());
		autoExecutorTestSuiteEClass.getESuperTypes().add(this.getTestSuite());
		clearActionEClass.getESuperTypes().add(this.getTestAction());
		externalActionEClass.getESuperTypes().add(this.getTestAction());

		// Initialize classes, features, and operations; add parameters
		initEClass(testSuiteEClass, TestSuite.class, "TestSuite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestSuite_Name(), ecorePackage.getEString(), "name", null, 0, 1, TestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestSuite_TestCases(), this.getTestCase(), null, "testCases", null, 0, -1, TestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestSuite_DefaultWaitingTime(), ecorePackage.getELong(), "defaultWaitingTime", "10000", 0, 1, TestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseEClass, TestCase.class, "TestCase", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestCase_Id(), ecorePackage.getEInt(), "id", null, 0, 1, TestCase.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestCase_Metamodels(), ecorePackage.getEString(), "metamodels", null, 0, -1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTestCase__Build(), this.getAbstractTransformationTestCase(), "build", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTestCase__GetDefaultWaitingTime(), ecorePackage.getELong(), "getDefaultWaitingTime", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(customizableTestCaseEClass, CustomizableTestCase.class, "CustomizableTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomizableTestCase_Action(), this.getTestAction(), null, "action", null, 1, 1, CustomizableTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(predefinedTestCaseEClass, PredefinedTestCase.class, "PredefinedTestCase", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPredefinedTestCase_Inputs(), ecorePackage.getEString(), "inputs", null, 0, -1, PredefinedTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(getPutTestCaseEClass, GetPutTestCase.class, "GetPutTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(putGetTestCaseEClass, PutGetTestCase.class, "PutGetTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(testActionEClass, TestAction.class, "TestAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getTestAction__Build(), this.getIAction(), "build", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(predefinedBXTestCaseEClass, PredefinedBXTestCase.class, "PredefinedBXTestCase", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPredefinedBXTestCase_FwdLauncher(), ecorePackage.getEString(), "fwdLauncher", null, 0, 1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_FwdSource(), ecorePackage.getEString(), "fwdSource", null, 0, -1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_FwdView(), ecorePackage.getEString(), "fwdView", null, 0, -1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_BwdLauncher(), ecorePackage.getEString(), "bwdLauncher", null, 0, 1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_BwdSource(), ecorePackage.getEString(), "bwdSource", null, 0, -1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_BwdView(), ecorePackage.getEString(), "bwdView", null, 0, -1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPredefinedBXTestCase_BwdUpdatedSource(), ecorePackage.getEString(), "bwdUpdatedSource", null, 0, -1, PredefinedBXTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceActionEClass, SequenceAction.class, "SequenceAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequenceAction_Actions(), this.getTestAction(), null, "actions", null, 0, -1, SequenceAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(loadModelActionEClass, LoadModelAction.class, "LoadModelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLoadModelAction_ModelURI(), ecorePackage.getEString(), "modelURI", null, 0, 1, LoadModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadModelAction_Key(), ecorePackage.getEString(), "key", null, 0, 1, LoadModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(saveModelActionEClass, SaveModelAction.class, "SaveModelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSaveModelAction_ModelURI(), ecorePackage.getEString(), "modelURI", null, 0, 1, SaveModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSaveModelAction_Key(), ecorePackage.getEString(), "key", null, 0, 1, SaveModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executeLauncherActionEClass, ExecuteLauncherAction.class, "ExecuteLauncherAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecuteLauncherAction_Launcher(), ecorePackage.getEString(), "launcher", null, 0, 1, ExecuteLauncherAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecuteLauncherAction_MaxWait(), ecorePackage.getELong(), "maxWait", "10000", 0, 1, ExecuteLauncherAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(evaluateOCLQueryActionEClass, EvaluateOCLQueryAction.class, "EvaluateOCLQueryAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEvaluateOCLQueryAction_ModelKey(), ecorePackage.getEString(), "modelKey", null, 0, 1, EvaluateOCLQueryAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvaluateOCLQueryAction_Query(), ecorePackage.getEString(), "query", null, 0, 1, EvaluateOCLQueryAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvaluateOCLQueryAction_ResultKey(), ecorePackage.getEString(), "resultKey", null, 0, 1, EvaluateOCLQueryAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvaluateOCLQueryAction_ImportContextVariables(), ecorePackage.getEString(), "importContextVariables", null, 0, -1, EvaluateOCLQueryAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(copyModelActionEClass, CopyModelAction.class, "CopyModelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCopyModelAction_FromURI(), ecorePackage.getEString(), "fromURI", null, 0, 1, CopyModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCopyModelAction_ToURI(), ecorePackage.getEString(), "toURI", null, 0, 1, CopyModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compareModelActionEClass, CompareModelAction.class, "CompareModelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCompareModelAction_LeftURI(), ecorePackage.getEString(), "leftURI", null, 0, 1, CompareModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCompareModelAction_RightURI(), ecorePackage.getEString(), "rightURI", null, 0, 1, CompareModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCompareModelAction_DiffKey(), ecorePackage.getEString(), "diffKey", null, 0, 1, CompareModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertComparisonResultActionEClass, AssertComparisonResultAction.class, "AssertComparisonResultAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssertComparisonResultAction_DiffKey(), ecorePackage.getEString(), "diffKey", null, 0, 1, AssertComparisonResultAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertComparisonResultAction_DifferentCount(), ecorePackage.getEInt(), "differentCount", null, 0, 1, AssertComparisonResultAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertComparisonResultAction_Kind(), this.getDiffKind(), "kind", null, 0, 1, AssertComparisonResultAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertOCLInvariantActionEClass, AssertOCLInvariantAction.class, "AssertOCLInvariantAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssertOCLInvariantAction_ModelKey(), ecorePackage.getEString(), "modelKey", null, 0, 1, AssertOCLInvariantAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertOCLInvariantAction_Invariant(), ecorePackage.getEString(), "invariant", null, 0, 1, AssertOCLInvariantAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssertOCLInvariantAction_ImportContextVariables(), ecorePackage.getEString(), "importContextVariables", null, 0, -1, AssertOCLInvariantAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userActionEClass, UserAction.class, "UserAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserAction_ClassFileURI(), ecorePackage.getEString(), "classFileURI", null, 0, 1, UserAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(setVariableActionEClass, SetVariableAction.class, "SetVariableAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSetVariableAction_Key(), ecorePackage.getEString(), "key", null, 0, 1, SetVariableAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSetVariableAction_Value(), ecorePackage.getEString(), "value", null, 0, 1, SetVariableAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(updateModelActionEClass, UpdateModelAction.class, "UpdateModelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUpdateModelAction_ModelKey(), ecorePackage.getEString(), "modelKey", null, 0, 1, UpdateModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUpdateModelAction_UpdateOperations(), ecorePackage.getEString(), "updateOperations", null, 0, -1, UpdateModelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wrapperTestCaseEClass, WrapperTestCase.class, "WrapperTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWrapperTestCase_BeforeTest(), this.getTestAction(), null, "beforeTest", null, 0, 1, WrapperTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWrapperTestCase_AfterTest(), this.getTestAction(), null, "afterTest", null, 0, 1, WrapperTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWrapperTestCase_CoreTest(), this.getTestCase(), null, "coreTest", null, 1, 1, WrapperTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWrapperTestCase_Repeat(), ecorePackage.getEInt(), "repeat", "1", 1, 1, WrapperTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(autoExecutorTestSuiteEClass, AutoExecutorTestSuite.class, "AutoExecutorTestSuite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAutoExecutorTestSuite_Executors(), ecorePackage.getEString(), "executors", null, 0, -1, AutoExecutorTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAutoExecutorTestSuite_Executables(), ecorePackage.getEString(), "executables", null, 0, -1, AutoExecutorTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAutoExecutorTestSuite_ExecutorAttributeName(), ecorePackage.getEString(), "executorAttributeName", null, 0, 1, AutoExecutorTestSuite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clearActionEClass, ClearAction.class, "ClearAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClearAction_Files(), ecorePackage.getEString(), "files", null, 0, -1, ClearAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalActionEClass, ExternalAction.class, "ExternalAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExternalAction_Name(), ecorePackage.getEString(), "name", null, 0, 1, ExternalAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExternalAction_ResultName(), ecorePackage.getEString(), "resultName", null, 0, 1, ExternalAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExternalAction_Parameters(), this.getExternalActionParameter(), null, "parameters", null, 0, -1, ExternalAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExternalAction_Action(), ecorePackage.getEString(), "action", null, 0, 1, ExternalAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalActionParameterEClass, ExternalActionParameter.class, "ExternalActionParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExternalActionParameter_Name(), ecorePackage.getEString(), "name", null, 0, 1, ExternalActionParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExternalActionParameter_Value(), ecorePackage.getEString(), "value", null, 0, 1, ExternalActionParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(diffKindEEnum, DiffKind.class, "DiffKind");
		addEEnumLiteral(diffKindEEnum, DiffKind.EQUAL);
		addEEnumLiteral(diffKindEEnum, DiffKind.LESS);
		addEEnumLiteral(diffKindEEnum, DiffKind.GREATER);

		// Initialize data types
		initEDataType(iActionEDataType, IAction.class, "IAction", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(abstractTransformationTestCaseEDataType, AbstractTransformationTestCase.class, "AbstractTransformationTestCase", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iExternalActionEDataType, edu.ustb.sei.mt4mt.base.updates.IExternalAction.class, "IExternalAction", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //MT4MTPackageImpl
