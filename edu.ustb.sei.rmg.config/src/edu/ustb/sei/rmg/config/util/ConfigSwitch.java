/**
 */
package edu.ustb.sei.rmg.config.util;

import edu.ustb.sei.rmg.config.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.config.ConfigPackage
 * @generated
 */
public class ConfigSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ConfigPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigSwitch() {
		if (modelPackage == null) {
			modelPackage = ConfigPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ConfigPackage.OPTION_MODEL: {
				OptionModel optionModel = (OptionModel)theEObject;
				T result = caseOptionModel(optionModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.CLASS_OPTION: {
				ClassOption classOption = (ClassOption)theEObject;
				T result = caseClassOption(classOption);
				if (result == null) result = caseScalableCondition(classOption);
				if (result == null) result = caseCondition(classOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.REFERENCE_OPTION: {
				ReferenceOption referenceOption = (ReferenceOption)theEObject;
				T result = caseReferenceOption(referenceOption);
				if (result == null) result = caseAbstractReferenceOption(referenceOption);
				if (result == null) result = caseScalableCondition(referenceOption);
				if (result == null) result = caseCondition(referenceOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.REFERENCE_GROUP_OPTION: {
				ReferenceGroupOption referenceGroupOption = (ReferenceGroupOption)theEObject;
				T result = caseReferenceGroupOption(referenceGroupOption);
				if (result == null) result = caseAbstractReferenceOption(referenceGroupOption);
				if (result == null) result = caseScalableCondition(referenceGroupOption);
				if (result == null) result = caseCondition(referenceGroupOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION: {
				AbstractReferenceOption abstractReferenceOption = (AbstractReferenceOption)theEObject;
				T result = caseAbstractReferenceOption(abstractReferenceOption);
				if (result == null) result = caseScalableCondition(abstractReferenceOption);
				if (result == null) result = caseCondition(abstractReferenceOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.ATTRIBUTE_OPTION: {
				AttributeOption attributeOption = (AttributeOption)theEObject;
				T result = caseAttributeOption(attributeOption);
				if (result == null) result = caseCondition(attributeOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.GLOBAL_OPTION: {
				GlobalOption globalOption = (GlobalOption)theEObject;
				T result = caseGlobalOption(globalOption);
				if (result == null) result = caseCondition(globalOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.CONDITION: {
				Condition condition = (Condition)theEObject;
				T result = caseCondition(condition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.SCALABLE_CONDITION: {
				ScalableCondition scalableCondition = (ScalableCondition)theEObject;
				T result = caseScalableCondition(scalableCondition);
				if (result == null) result = caseCondition(scalableCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.FILTER: {
				Filter filter = (Filter)theEObject;
				T result = caseFilter(filter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.BASIC_FILTER: {
				BasicFilter basicFilter = (BasicFilter)theEObject;
				T result = caseBasicFilter(basicFilter);
				if (result == null) result = caseFilter(basicFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.COMBINATIONAL_FILTER: {
				CombinationalFilter combinationalFilter = (CombinationalFilter)theEObject;
				T result = caseCombinationalFilter(combinationalFilter);
				if (result == null) result = caseFilter(combinationalFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ConfigPackage.REFERENCE_FILTER_OPTION: {
				ReferenceFilterOption referenceFilterOption = (ReferenceFilterOption)theEObject;
				T result = caseReferenceFilterOption(referenceFilterOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Option Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Option Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOptionModel(OptionModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassOption(ClassOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceOption(ReferenceOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Group Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Group Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceGroupOption(ReferenceGroupOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Reference Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Reference Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractReferenceOption(AbstractReferenceOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeOption(AttributeOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobalOption(GlobalOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondition(Condition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scalable Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scalable Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScalableCondition(ScalableCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFilter(Filter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicFilter(BasicFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinational Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinational Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinationalFilter(CombinationalFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Filter Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Filter Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceFilterOption(ReferenceFilterOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ConfigSwitch
