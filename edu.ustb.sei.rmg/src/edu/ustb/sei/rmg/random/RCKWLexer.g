%options slr
%options fp=RCKWLexer,prefix=Char_
%options noserialize
%options package=edu.ustb.sei.rmg.random
%options template=./KeywordTemplateF.gi
%options export_terminals=("RCParsersym.java", "TK_")
%options include_directory="./"

%Import
	KWLexerMapF.gi
%End

%Define

	--
	-- Definition of macros used in the template
	--
	$action_class /.$file_prefix./
	$eof_char /.Char_EOF./
	$copyright_contributions /.*./

%End

%Export
	norm
	bern
	geom
	pois
	pare
	cauc
	true
	false
	separate
	list
	unique
	for
	all
%End

%Start
	KeyWord
%End

%Rules
    KeyWord ::= l i s t
		/.$BeginAction
			$setResult($_list);
		  $EndAction
		./
		
	KeyWord ::= s e p a r a t e
		/.$BeginAction
			$setResult($_separate);
		  $EndAction
		./
		
	KeyWord ::= n o r m
		/.$BeginAction
			$setResult($_norm);
		  $EndAction
		./

	KeyWord ::= b e r n
		/.$BeginAction
			$setResult($_bern);
		  $EndAction
		./


	KeyWord ::= g e o m
		/.$BeginAction
			$setResult($_geom);
		  $EndAction
		./

	KeyWord ::= p o i s
		/.$BeginAction
			$setResult($_pois);
		  $EndAction
		./
		
	KeyWord ::= p a r e
		/.$BeginAction
			$setResult($_pare);
		  $EndAction
		./
		
	KeyWord ::= c a u c
		/.$BeginAction
			$setResult($_cauc);
		  $EndAction
		./
		
	KeyWord ::= t r u e
		/.$BeginAction
			$setResult($_true);
		  $EndAction
		./
		
	KeyWord ::= f a l s e
		/.$BeginAction
			$setResult($_false);
		  $EndAction
		./
	KeyWord ::= u n i q u e
		/.$BeginAction
			$setResult($_unique);
		  $EndAction
		./
	KeyWord ::= f o r
		/.$BeginAction
			$setResult($_for);
		  $EndAction
		./
	KeyWord ::= a l l
		/.$BeginAction
			$setResult($_all);
		  $EndAction
		./

%End
