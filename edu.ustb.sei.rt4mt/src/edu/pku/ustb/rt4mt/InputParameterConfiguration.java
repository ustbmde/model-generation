/**
 */
package edu.pku.ustb.rt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Parameter Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.InputParameterConfiguration#getRmgModelURI <em>Rmg Model URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getInputParameterConfiguration()
 * @model
 * @generated
 */
public interface InputParameterConfiguration extends ParameterConfiguration {
	/**
	 * Returns the value of the '<em><b>Rmg Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rmg Model URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rmg Model URI</em>' attribute.
	 * @see #setRmgModelURI(String)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getInputParameterConfiguration_RmgModelURI()
	 * @model
	 * @generated
	 */
	String getRmgModelURI();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.InputParameterConfiguration#getRmgModelURI <em>Rmg Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rmg Model URI</em>' attribute.
	 * @see #getRmgModelURI()
	 * @generated
	 */
	void setRmgModelURI(String value);

} // InputParameterConfiguration
