/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgOutlinePageExpandAllAction extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.AbstractStructuralrmgOutlinePageAction {
	
	public StructuralrmgOutlinePageExpandAllAction(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Expand all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/expand_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().expandAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
