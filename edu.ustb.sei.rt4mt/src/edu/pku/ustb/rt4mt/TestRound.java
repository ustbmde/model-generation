/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Round</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.TestRound#getRoundNumber <em>Round Number</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.TestRound#getTestExecutions <em>Test Executions</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestRound()
 * @model
 * @generated
 */
public interface TestRound extends EObject {
	/**
	 * Returns the value of the '<em><b>Round Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Number</em>' attribute.
	 * @see #setRoundNumber(int)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestRound_RoundNumber()
	 * @model
	 * @generated
	 */
	int getRoundNumber();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.TestRound#getRoundNumber <em>Round Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round Number</em>' attribute.
	 * @see #getRoundNumber()
	 * @generated
	 */
	void setRoundNumber(int value);

	/**
	 * Returns the value of the '<em><b>Test Executions</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.TestExecution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Executions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Executions</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestRound_TestExecutions()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestExecution> getTestExecutions();

} // TestRound
