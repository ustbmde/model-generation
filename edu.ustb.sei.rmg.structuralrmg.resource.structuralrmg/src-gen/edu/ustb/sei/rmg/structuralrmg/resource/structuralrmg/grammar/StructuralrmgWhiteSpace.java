/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public class StructuralrmgWhiteSpace extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFormattingElement {
	
	private final int amount;
	
	public StructuralrmgWhiteSpace(int amount, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
