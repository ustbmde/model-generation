/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.config.ConfigPackage
 * @generated
 */
public interface ConfigFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigFactory eINSTANCE = edu.ustb.sei.rmg.config.impl.ConfigFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Option Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Option Model</em>'.
	 * @generated
	 */
	OptionModel createOptionModel();

	/**
	 * Returns a new object of class '<em>Class Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Option</em>'.
	 * @generated
	 */
	ClassOption createClassOption();

	/**
	 * Returns a new object of class '<em>Reference Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Option</em>'.
	 * @generated
	 */
	ReferenceOption createReferenceOption();

	/**
	 * Returns a new object of class '<em>Reference Group Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Group Option</em>'.
	 * @generated
	 */
	ReferenceGroupOption createReferenceGroupOption();

	/**
	 * Returns a new object of class '<em>Attribute Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Option</em>'.
	 * @generated
	 */
	AttributeOption createAttributeOption();

	/**
	 * Returns a new object of class '<em>Global Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Option</em>'.
	 * @generated
	 */
	GlobalOption createGlobalOption();

	/**
	 * Returns a new object of class '<em>Basic Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Filter</em>'.
	 * @generated
	 */
	BasicFilter createBasicFilter();

	/**
	 * Returns a new object of class '<em>Combinational Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Combinational Filter</em>'.
	 * @generated
	 */
	CombinationalFilter createCombinationalFilter();

	/**
	 * Returns a new object of class '<em>Reference Filter Option</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Filter Option</em>'.
	 * @generated
	 */
	ReferenceFilterOption createReferenceFilterOption();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ConfigPackage getConfigPackage();

} //ConfigFactory
