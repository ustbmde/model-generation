package edu.ustb.sei.mt4mt.base.test;

import static edu.ustb.sei.mt4mt.base.Actions.compareAndAssert;
import static edu.ustb.sei.mt4mt.base.Actions.copy;
import static edu.ustb.sei.mt4mt.base.Actions.launch;
import static edu.ustb.sei.mt4mt.base.Actions.sequence;

import java.util.Arrays;

import edu.ustb.sei.mt4mt.base.IAction;

public class TestPutGet extends AbstractBXTransformationTestCase {
	public TestPutGet(String[] inputData, String fwdName, String[] fwdSource, String[] fwdView,
			String bwdName, String[] bwdSource, String[] bwdView, String[] bwdUpdatedSource, long defaultWaitingtime) {
		super(inputData, fwdName, fwdSource, fwdView, bwdName, bwdSource, bwdView, bwdUpdatedSource);
		
		String[] originalViews = Arrays.copyOfRange(inputData, inputData.length - fwdView.length, inputData.length);
		
		IAction action = sequence(
				copy(inputData, bwdSource, bwdView),
				launch(bwdName,defaultWaitingtime),
				copy(bwdUpdatedSource,fwdSource),
				launch(fwdName,defaultWaitingtime),
				compareAndAssert(originalViews,fwdView,"#PutGet")
				);
		
		this.action = action;
	}
}
