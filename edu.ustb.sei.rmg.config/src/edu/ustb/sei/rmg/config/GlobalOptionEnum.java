/**
 */
package edu.ustb.sei.rmg.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Global Option Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getGlobalOptionEnum()
 * @model
 * @generated
 */
public enum GlobalOptionEnum implements Enumerator {
	/**
	 * The '<em><b>Global object bound</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GLOBAL_OBJECT_BOUND_VALUE
	 * @generated
	 * @ordered
	 */
	GLOBAL_OBJECT_BOUND(0, "global_object_bound", "global_object_bound"),

	/**
	 * The '<em><b>Default object bound</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_OBJECT_BOUND_VALUE
	 * @generated
	 * @ordered
	 */
	DEFAULT_OBJECT_BOUND(1, "default_object_bound", "default_object_bound"),

	/**
	 * The '<em><b>Default link bound</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_LINK_BOUND_VALUE
	 * @generated
	 * @ordered
	 */
	DEFAULT_LINK_BOUND(2, "default_link_bound", "default_link_bound"),

	/**
	 * The '<em><b>Default string condition</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_STRING_CONDITION_VALUE
	 * @generated
	 * @ordered
	 */
	DEFAULT_STRING_CONDITION(3, "default_string_condition", "default_string_condition"),

	/**
	 * The '<em><b>Default integer range</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_INTEGER_RANGE_VALUE
	 * @generated
	 * @ordered
	 */
	DEFAULT_INTEGER_RANGE(4, "default_integer_range", "default_integer_range"), /**
	 * The '<em><b>Size factor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIZE_FACTOR_VALUE
	 * @generated
	 * @ordered
	 */
	SIZE_FACTOR(5, "size_factor", "size_factor");

	/**
	 * The '<em><b>Global object bound</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Global object bound</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GLOBAL_OBJECT_BOUND
	 * @model name="global_object_bound"
	 * @generated
	 * @ordered
	 */
	public static final int GLOBAL_OBJECT_BOUND_VALUE = 0;

	/**
	 * The '<em><b>Default object bound</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Default object bound</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_OBJECT_BOUND
	 * @model name="default_object_bound"
	 * @generated
	 * @ordered
	 */
	public static final int DEFAULT_OBJECT_BOUND_VALUE = 1;

	/**
	 * The '<em><b>Default link bound</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Default link bound</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_LINK_BOUND
	 * @model name="default_link_bound"
	 * @generated
	 * @ordered
	 */
	public static final int DEFAULT_LINK_BOUND_VALUE = 2;

	/**
	 * The '<em><b>Default string condition</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Default string condition</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_STRING_CONDITION
	 * @model name="default_string_condition"
	 * @generated
	 * @ordered
	 */
	public static final int DEFAULT_STRING_CONDITION_VALUE = 3;

	/**
	 * The '<em><b>Default integer range</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Default integer range</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_INTEGER_RANGE
	 * @model name="default_integer_range"
	 * @generated
	 * @ordered
	 */
	public static final int DEFAULT_INTEGER_RANGE_VALUE = 4;

	/**
	 * The '<em><b>Size factor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Size factor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIZE_FACTOR
	 * @model name="size_factor"
	 * @generated
	 * @ordered
	 */
	public static final int SIZE_FACTOR_VALUE = 5;

	/**
	 * An array of all the '<em><b>Global Option Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final GlobalOptionEnum[] VALUES_ARRAY =
		new GlobalOptionEnum[] {
			GLOBAL_OBJECT_BOUND,
			DEFAULT_OBJECT_BOUND,
			DEFAULT_LINK_BOUND,
			DEFAULT_STRING_CONDITION,
			DEFAULT_INTEGER_RANGE,
			SIZE_FACTOR,
		};

	/**
	 * A public read-only list of all the '<em><b>Global Option Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<GlobalOptionEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Global Option Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlobalOptionEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GlobalOptionEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Global Option Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlobalOptionEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GlobalOptionEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Global Option Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GlobalOptionEnum get(int value) {
		switch (value) {
			case GLOBAL_OBJECT_BOUND_VALUE: return GLOBAL_OBJECT_BOUND;
			case DEFAULT_OBJECT_BOUND_VALUE: return DEFAULT_OBJECT_BOUND;
			case DEFAULT_LINK_BOUND_VALUE: return DEFAULT_LINK_BOUND;
			case DEFAULT_STRING_CONDITION_VALUE: return DEFAULT_STRING_CONDITION;
			case DEFAULT_INTEGER_RANGE_VALUE: return DEFAULT_INTEGER_RANGE;
			case SIZE_FACTOR_VALUE: return SIZE_FACTOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private GlobalOptionEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //GlobalOptionEnum
