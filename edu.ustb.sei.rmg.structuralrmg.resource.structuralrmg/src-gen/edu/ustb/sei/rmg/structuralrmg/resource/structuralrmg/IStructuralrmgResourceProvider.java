/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IStructuralrmgResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource getResource();
	
}
