/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory
 * @model kind="package"
 * @generated
 */
public interface StructuralRmgPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "structuralrmg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/structuralrmg";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "structuralrmg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StructuralRmgPackage eINSTANCE = edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateModelImpl <em>Template Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateModelImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateModel()
	 * @generated
	 */
	int TEMPLATE_MODEL = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.NamedElementImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_MODEL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_MODEL__TEMPLATES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_MODEL__PACKAGES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Template Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Template Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_MODEL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl <em>Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplate()
	 * @generated
	 */
	int TEMPLATE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__ROLES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__NODES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__EDGES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__ABSTRACT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Super Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__SUPER_TEMPLATE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get All Roles</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE___GET_ALL_ROLES = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get All Nodes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE___GET_ALL_NODES = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get All Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE___GET_ALL_EDGES = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Super Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE___IS_SUPER_OF__TEMPLATE = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateNodeImpl <em>Template Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateNodeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateNode()
	 * @generated
	 */
	int TEMPLATE_NODE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE__ROLE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bound</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE__BOUND = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE___IS_SET = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Max</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE___GET_MAX = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Min</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE___GET_MIN = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_NODE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.BoundImpl <em>Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.BoundImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getBound()
	 * @generated
	 */
	int BOUND = 4;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOUND__MIN = 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOUND__MAX = 1;

	/**
	 * The number of structural features of the '<em>Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOUND_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOUND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateEdgeImpl <em>Template Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateEdgeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateEdge()
	 * @generated
	 */
	int TEMPLATE_EDGE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_EDGE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_EDGE__REFERENCE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_EDGE__BINDING = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_EDGE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_EDGE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl <em>Primitive Template Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getPrimitiveTemplateEdge()
	 * @generated
	 */
	int PRIMITIVE_TEMPLATE_EDGE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__NAME = TEMPLATE_EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__REFERENCE = TEMPLATE_EDGE__REFERENCE;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__BINDING = TEMPLATE_EDGE__BINDING;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__SOURCE = TEMPLATE_EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__TARGET = TEMPLATE_EDGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Pos</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS = TEMPLATE_EDGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target Pos</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE__TARGET_POS = TEMPLATE_EDGE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Primitive Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE_FEATURE_COUNT = TEMPLATE_EDGE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Primitive Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_EDGE_OPERATION_COUNT = TEMPLATE_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.ChainTemplateEdgeImpl <em>Chain Template Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.ChainTemplateEdgeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getChainTemplateEdge()
	 * @generated
	 */
	int CHAIN_TEMPLATE_EDGE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE__NAME = TEMPLATE_EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE__REFERENCE = TEMPLATE_EDGE__REFERENCE;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE__BINDING = TEMPLATE_EDGE__BINDING;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE__NODE = TEMPLATE_EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Round</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE__ROUND = TEMPLATE_EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Chain Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE_FEATURE_COUNT = TEMPLATE_EDGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Chain Template Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAIN_TEMPLATE_EDGE_OPERATION_COUNT = TEMPLATE_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateNodeImpl <em>Primitive Template Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateNodeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getPrimitiveTemplateNode()
	 * @generated
	 */
	int PRIMITIVE_TEMPLATE_NODE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE__NAME = TEMPLATE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE__ROLE = TEMPLATE_NODE__ROLE;

	/**
	 * The feature id for the '<em><b>Bound</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE__BOUND = TEMPLATE_NODE__BOUND;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE__TYPE = TEMPLATE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES = TEMPLATE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Primitive Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE_FEATURE_COUNT = TEMPLATE_NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE___IS_SET = TEMPLATE_NODE___IS_SET;

	/**
	 * The operation id for the '<em>Get Max</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE___GET_MAX = TEMPLATE_NODE___GET_MAX;

	/**
	 * The operation id for the '<em>Get Min</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE___GET_MIN = TEMPLATE_NODE___GET_MIN;

	/**
	 * The number of operations of the '<em>Primitive Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_TEMPLATE_NODE_OPERATION_COUNT = TEMPLATE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.AttributeBindingImpl <em>Attribute Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.AttributeBindingImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getAttributeBinding()
	 * @generated
	 */
	int ATTRIBUTE_BINDING = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_BINDING__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_BINDING__ATTRIBUTE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_BINDING_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attribute Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_BINDING_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.NestedTemplateNodeImpl <em>Nested Template Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.NestedTemplateNodeImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getNestedTemplateNode()
	 * @generated
	 */
	int NESTED_TEMPLATE_NODE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE__NAME = TEMPLATE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE__ROLE = TEMPLATE_NODE__ROLE;

	/**
	 * The feature id for the '<em><b>Bound</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE__BOUND = TEMPLATE_NODE__BOUND;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE__TEMPLATES = TEMPLATE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sort</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE__SORT = TEMPLATE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Nested Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE_FEATURE_COUNT = TEMPLATE_NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE___IS_SET = TEMPLATE_NODE___IS_SET;

	/**
	 * The operation id for the '<em>Get Max</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE___GET_MAX = TEMPLATE_NODE___GET_MAX;

	/**
	 * The operation id for the '<em>Get Min</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE___GET_MIN = TEMPLATE_NODE___GET_MIN;

	/**
	 * The number of operations of the '<em>Nested Template Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NESTED_TEMPLATE_NODE_OPERATION_COUNT = TEMPLATE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.RoleImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;


	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.BindingMappingImpl <em>Binding Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.BindingMappingImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getBindingMapping()
	 * @generated
	 */
	int BINDING_MAPPING = 12;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_MAPPING__ROLES = 0;

	/**
	 * The feature id for the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_MAPPING__TEMPLATE = 1;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_MAPPING__POSITION = 2;

	/**
	 * The number of structural features of the '<em>Binding Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_MAPPING_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Binding Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_MAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl <em>Endpoint Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getEndpointBinding()
	 * @generated
	 */
	int ENDPOINT_BINDING = 13;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_BINDING__EDGE = 0;

	/**
	 * The feature id for the '<em><b>Source Mappings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_BINDING__SOURCE_MAPPINGS = 1;

	/**
	 * The feature id for the '<em><b>Target Mappings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_BINDING__TARGET_MAPPINGS = 2;

	/**
	 * The number of structural features of the '<em>Endpoint Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_BINDING_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Endpoint Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_BINDING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.structuralrmg.CompositionSort <em>Composition Sort</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.structuralrmg.CompositionSort
	 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getCompositionSort()
	 * @generated
	 */
	int COMPOSITION_SORT = 14;

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.TemplateModel <em>Template Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Model</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateModel
	 * @generated
	 */
	EClass getTemplateModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.TemplateModel#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Templates</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateModel#getTemplates()
	 * @see #getTemplateModel()
	 * @generated
	 */
	EReference getTemplateModel_Templates();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.structuralrmg.TemplateModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Packages</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateModel#getPackages()
	 * @see #getTemplateModel()
	 * @generated
	 */
	EReference getTemplateModel_Packages();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.Template <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template
	 * @generated
	 */
	EClass getTemplate();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.Template#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getRoles()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_Roles();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.Template#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getNodes()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.Template#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getEdges()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_Edges();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.Template#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#isAbstract()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Abstract();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.Template#getSuperTemplate <em>Super Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Template</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getSuperTemplate()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_SuperTemplate();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.Template#getAllRoles() <em>Get All Roles</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Roles</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getAllRoles()
	 * @generated
	 */
	EOperation getTemplate__GetAllRoles();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.Template#getAllNodes() <em>Get All Nodes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Nodes</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getAllNodes()
	 * @generated
	 */
	EOperation getTemplate__GetAllNodes();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.Template#getAllEdges() <em>Get All Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Edges</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#getAllEdges()
	 * @generated
	 */
	EOperation getTemplate__GetAllEdges();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.Template#isSuperOf(edu.ustb.sei.rmg.structuralrmg.Template) <em>Is Super Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Super Of</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.Template#isSuperOf(edu.ustb.sei.rmg.structuralrmg.Template)
	 * @generated
	 */
	EOperation getTemplate__IsSuperOf__Template();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode <em>Template Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Node</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode
	 * @generated
	 */
	EClass getTemplateNode();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode#getRole()
	 * @see #getTemplateNode()
	 * @generated
	 */
	EReference getTemplateNode_Role();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getBound <em>Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Bound</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode#getBound()
	 * @see #getTemplateNode()
	 * @generated
	 */
	EReference getTemplateNode_Bound();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#isSet() <em>Is Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Set</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode#isSet()
	 * @generated
	 */
	EOperation getTemplateNode__IsSet();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getMax() <em>Get Max</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Max</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode#getMax()
	 * @generated
	 */
	EOperation getTemplateNode__GetMax();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getMin() <em>Get Min</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Min</em>' operation.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateNode#getMin()
	 * @generated
	 */
	EOperation getTemplateNode__GetMin();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.Bound <em>Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bound</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Bound
	 * @generated
	 */
	EClass getBound();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.Bound#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Bound#getMin()
	 * @see #getBound()
	 * @generated
	 */
	EAttribute getBound_Min();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.Bound#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Bound#getMax()
	 * @see #getBound()
	 * @generated
	 */
	EAttribute getBound_Max();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge <em>Template Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Edge</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateEdge
	 * @generated
	 */
	EClass getTemplateEdge();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getReference()
	 * @see #getTemplateEdge()
	 * @generated
	 */
	EReference getTemplateEdge_Reference();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Binding</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding()
	 * @see #getTemplateEdge()
	 * @generated
	 */
	EReference getTemplateEdge_Binding();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge <em>Primitive Template Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Template Edge</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge
	 * @generated
	 */
	EClass getPrimitiveTemplateEdge();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSource()
	 * @see #getPrimitiveTemplateEdge()
	 * @generated
	 */
	EReference getPrimitiveTemplateEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTarget()
	 * @see #getPrimitiveTemplateEdge()
	 * @generated
	 */
	EReference getPrimitiveTemplateEdge_Target();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSourcePos <em>Source Pos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Pos</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSourcePos()
	 * @see #getPrimitiveTemplateEdge()
	 * @generated
	 */
	EAttribute getPrimitiveTemplateEdge_SourcePos();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTargetPos <em>Target Pos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Pos</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTargetPos()
	 * @see #getPrimitiveTemplateEdge()
	 * @generated
	 */
	EAttribute getPrimitiveTemplateEdge_TargetPos();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge <em>Chain Template Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chain Template Edge</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge
	 * @generated
	 */
	EClass getChainTemplateEdge();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Node</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#getNode()
	 * @see #getChainTemplateEdge()
	 * @generated
	 */
	EReference getChainTemplateEdge_Node();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#isRound <em>Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Round</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#isRound()
	 * @see #getChainTemplateEdge()
	 * @generated
	 */
	EAttribute getChainTemplateEdge_Round();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode <em>Primitive Template Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Template Node</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode
	 * @generated
	 */
	EClass getPrimitiveTemplateNode();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getType()
	 * @see #getPrimitiveTemplateNode()
	 * @generated
	 */
	EReference getPrimitiveTemplateNode_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getAttributes()
	 * @see #getPrimitiveTemplateNode()
	 * @generated
	 */
	EReference getPrimitiveTemplateNode_Attributes();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.AttributeBinding <em>Attribute Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Binding</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.AttributeBinding
	 * @generated
	 */
	EClass getAttributeBinding();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.AttributeBinding#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.AttributeBinding#getAttribute()
	 * @see #getAttributeBinding()
	 * @generated
	 */
	EReference getAttributeBinding_Attribute();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode <em>Nested Template Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nested Template Node</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode
	 * @generated
	 */
	EClass getNestedTemplateNode();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Templates</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getTemplates()
	 * @see #getNestedTemplateNode()
	 * @generated
	 */
	EReference getNestedTemplateNode_Templates();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getSort <em>Sort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sort</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getSort()
	 * @see #getNestedTemplateNode()
	 * @generated
	 */
	EAttribute getNestedTemplateNode_Sort();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.BindingMapping <em>Binding Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding Mapping</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.BindingMapping
	 * @generated
	 */
	EClass getBindingMapping();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.structuralrmg.BindingMapping#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Roles</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.BindingMapping#getRoles()
	 * @see #getBindingMapping()
	 * @generated
	 */
	EReference getBindingMapping_Roles();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.structuralrmg.BindingMapping#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Template</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.BindingMapping#getTemplate()
	 * @see #getBindingMapping()
	 * @generated
	 */
	EReference getBindingMapping_Template();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.structuralrmg.BindingMapping#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.BindingMapping#getPosition()
	 * @see #getBindingMapping()
	 * @generated
	 */
	EAttribute getBindingMapping_Position();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding <em>Endpoint Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Endpoint Binding</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.EndpointBinding
	 * @generated
	 */
	EClass getEndpointBinding();

	/**
	 * Returns the meta object for the container reference '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Edge</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge()
	 * @see #getEndpointBinding()
	 * @generated
	 */
	EReference getEndpointBinding_Edge();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getSourceMappings <em>Source Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Source Mappings</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getSourceMappings()
	 * @see #getEndpointBinding()
	 * @generated
	 */
	EReference getEndpointBinding_SourceMappings();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getTargetMappings <em>Target Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Target Mappings</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getTargetMappings()
	 * @see #getEndpointBinding()
	 * @generated
	 */
	EReference getEndpointBinding_TargetMappings();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.rmg.structuralrmg.CompositionSort <em>Composition Sort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Composition Sort</em>'.
	 * @see edu.ustb.sei.rmg.structuralrmg.CompositionSort
	 * @generated
	 */
	EEnum getCompositionSort();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StructuralRmgFactory getStructuralRmgFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateModelImpl <em>Template Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateModelImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateModel()
		 * @generated
		 */
		EClass TEMPLATE_MODEL = eINSTANCE.getTemplateModel();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_MODEL__TEMPLATES = eINSTANCE.getTemplateModel_Templates();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_MODEL__PACKAGES = eINSTANCE.getTemplateModel_Packages();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.NamedElementImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl <em>Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplate()
		 * @generated
		 */
		EClass TEMPLATE = eINSTANCE.getTemplate();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__ROLES = eINSTANCE.getTemplate_Roles();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__NODES = eINSTANCE.getTemplate_Nodes();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__EDGES = eINSTANCE.getTemplate_Edges();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__ABSTRACT = eINSTANCE.getTemplate_Abstract();

		/**
		 * The meta object literal for the '<em><b>Super Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__SUPER_TEMPLATE = eINSTANCE.getTemplate_SuperTemplate();

		/**
		 * The meta object literal for the '<em><b>Get All Roles</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE___GET_ALL_ROLES = eINSTANCE.getTemplate__GetAllRoles();

		/**
		 * The meta object literal for the '<em><b>Get All Nodes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE___GET_ALL_NODES = eINSTANCE.getTemplate__GetAllNodes();

		/**
		 * The meta object literal for the '<em><b>Get All Edges</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE___GET_ALL_EDGES = eINSTANCE.getTemplate__GetAllEdges();

		/**
		 * The meta object literal for the '<em><b>Is Super Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE___IS_SUPER_OF__TEMPLATE = eINSTANCE.getTemplate__IsSuperOf__Template();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateNodeImpl <em>Template Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateNodeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateNode()
		 * @generated
		 */
		EClass TEMPLATE_NODE = eINSTANCE.getTemplateNode();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_NODE__ROLE = eINSTANCE.getTemplateNode_Role();

		/**
		 * The meta object literal for the '<em><b>Bound</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_NODE__BOUND = eINSTANCE.getTemplateNode_Bound();

		/**
		 * The meta object literal for the '<em><b>Is Set</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE_NODE___IS_SET = eINSTANCE.getTemplateNode__IsSet();

		/**
		 * The meta object literal for the '<em><b>Get Max</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE_NODE___GET_MAX = eINSTANCE.getTemplateNode__GetMax();

		/**
		 * The meta object literal for the '<em><b>Get Min</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEMPLATE_NODE___GET_MIN = eINSTANCE.getTemplateNode__GetMin();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.BoundImpl <em>Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.BoundImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getBound()
		 * @generated
		 */
		EClass BOUND = eINSTANCE.getBound();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOUND__MIN = eINSTANCE.getBound_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOUND__MAX = eINSTANCE.getBound_Max();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateEdgeImpl <em>Template Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.TemplateEdgeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getTemplateEdge()
		 * @generated
		 */
		EClass TEMPLATE_EDGE = eINSTANCE.getTemplateEdge();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_EDGE__REFERENCE = eINSTANCE.getTemplateEdge_Reference();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_EDGE__BINDING = eINSTANCE.getTemplateEdge_Binding();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl <em>Primitive Template Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getPrimitiveTemplateEdge()
		 * @generated
		 */
		EClass PRIMITIVE_TEMPLATE_EDGE = eINSTANCE.getPrimitiveTemplateEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_TEMPLATE_EDGE__SOURCE = eINSTANCE.getPrimitiveTemplateEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_TEMPLATE_EDGE__TARGET = eINSTANCE.getPrimitiveTemplateEdge_Target();

		/**
		 * The meta object literal for the '<em><b>Source Pos</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS = eINSTANCE.getPrimitiveTemplateEdge_SourcePos();

		/**
		 * The meta object literal for the '<em><b>Target Pos</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMITIVE_TEMPLATE_EDGE__TARGET_POS = eINSTANCE.getPrimitiveTemplateEdge_TargetPos();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.ChainTemplateEdgeImpl <em>Chain Template Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.ChainTemplateEdgeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getChainTemplateEdge()
		 * @generated
		 */
		EClass CHAIN_TEMPLATE_EDGE = eINSTANCE.getChainTemplateEdge();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHAIN_TEMPLATE_EDGE__NODE = eINSTANCE.getChainTemplateEdge_Node();

		/**
		 * The meta object literal for the '<em><b>Round</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHAIN_TEMPLATE_EDGE__ROUND = eINSTANCE.getChainTemplateEdge_Round();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateNodeImpl <em>Primitive Template Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateNodeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getPrimitiveTemplateNode()
		 * @generated
		 */
		EClass PRIMITIVE_TEMPLATE_NODE = eINSTANCE.getPrimitiveTemplateNode();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_TEMPLATE_NODE__TYPE = eINSTANCE.getPrimitiveTemplateNode_Type();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES = eINSTANCE.getPrimitiveTemplateNode_Attributes();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.AttributeBindingImpl <em>Attribute Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.AttributeBindingImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getAttributeBinding()
		 * @generated
		 */
		EClass ATTRIBUTE_BINDING = eINSTANCE.getAttributeBinding();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_BINDING__ATTRIBUTE = eINSTANCE.getAttributeBinding_Attribute();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.NestedTemplateNodeImpl <em>Nested Template Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.NestedTemplateNodeImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getNestedTemplateNode()
		 * @generated
		 */
		EClass NESTED_TEMPLATE_NODE = eINSTANCE.getNestedTemplateNode();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NESTED_TEMPLATE_NODE__TEMPLATES = eINSTANCE.getNestedTemplateNode_Templates();

		/**
		 * The meta object literal for the '<em><b>Sort</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NESTED_TEMPLATE_NODE__SORT = eINSTANCE.getNestedTemplateNode_Sort();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.RoleImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.BindingMappingImpl <em>Binding Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.BindingMappingImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getBindingMapping()
		 * @generated
		 */
		EClass BINDING_MAPPING = eINSTANCE.getBindingMapping();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_MAPPING__ROLES = eINSTANCE.getBindingMapping_Roles();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING_MAPPING__TEMPLATE = eINSTANCE.getBindingMapping_Template();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING_MAPPING__POSITION = eINSTANCE.getBindingMapping_Position();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl <em>Endpoint Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.EndpointBindingImpl
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getEndpointBinding()
		 * @generated
		 */
		EClass ENDPOINT_BINDING = eINSTANCE.getEndpointBinding();

		/**
		 * The meta object literal for the '<em><b>Edge</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENDPOINT_BINDING__EDGE = eINSTANCE.getEndpointBinding_Edge();

		/**
		 * The meta object literal for the '<em><b>Source Mappings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENDPOINT_BINDING__SOURCE_MAPPINGS = eINSTANCE.getEndpointBinding_SourceMappings();

		/**
		 * The meta object literal for the '<em><b>Target Mappings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENDPOINT_BINDING__TARGET_MAPPINGS = eINSTANCE.getEndpointBinding_TargetMappings();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.structuralrmg.CompositionSort <em>Composition Sort</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.structuralrmg.CompositionSort
		 * @see edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgPackageImpl#getCompositionSort()
		 * @generated
		 */
		EEnum COMPOSITION_SORT = eINSTANCE.getCompositionSort();

	}

} //StructuralRmgPackage
