/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.Filter;
import edu.ustb.sei.rmg.config.ReferenceFilterOption;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Filter Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl#getSourceFilter <em>Source Filter</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl#getTargetFilter <em>Target Filter</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl#getReferenceRef <em>Reference Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceFilterOptionImpl extends EObjectImpl implements ReferenceFilterOption {
	/**
	 * The cached value of the '{@link #getSourceFilter() <em>Source Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFilter()
	 * @generated
	 * @ordered
	 */
	protected Filter sourceFilter;

	/**
	 * The cached value of the '{@link #getTargetFilter() <em>Target Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetFilter()
	 * @generated
	 * @ordered
	 */
	protected Filter targetFilter;

	/**
	 * The cached value of the '{@link #getReferenceRef() <em>Reference Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceRef()
	 * @generated
	 * @ordered
	 */
	protected EReference referenceRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceFilterOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.REFERENCE_FILTER_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Filter getSourceFilter() {
		return sourceFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceFilter(Filter newSourceFilter, NotificationChain msgs) {
		Filter oldSourceFilter = sourceFilter;
		sourceFilter = newSourceFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER, oldSourceFilter, newSourceFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceFilter(Filter newSourceFilter) {
		if (newSourceFilter != sourceFilter) {
			NotificationChain msgs = null;
			if (sourceFilter != null)
				msgs = ((InternalEObject)sourceFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER, null, msgs);
			if (newSourceFilter != null)
				msgs = ((InternalEObject)newSourceFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER, null, msgs);
			msgs = basicSetSourceFilter(newSourceFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER, newSourceFilter, newSourceFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Filter getTargetFilter() {
		return targetFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetFilter(Filter newTargetFilter, NotificationChain msgs) {
		Filter oldTargetFilter = targetFilter;
		targetFilter = newTargetFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER, oldTargetFilter, newTargetFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetFilter(Filter newTargetFilter) {
		if (newTargetFilter != targetFilter) {
			NotificationChain msgs = null;
			if (targetFilter != null)
				msgs = ((InternalEObject)targetFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER, null, msgs);
			if (newTargetFilter != null)
				msgs = ((InternalEObject)newTargetFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER, null, msgs);
			msgs = basicSetTargetFilter(newTargetFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER, newTargetFilter, newTargetFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceRef() {
		if (referenceRef != null && referenceRef.eIsProxy()) {
			InternalEObject oldReferenceRef = (InternalEObject)referenceRef;
			referenceRef = (EReference)eResolveProxy(oldReferenceRef);
			if (referenceRef != oldReferenceRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF, oldReferenceRef, referenceRef));
			}
		}
		return referenceRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetReferenceRef() {
		return referenceRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceRef(EReference newReferenceRef) {
		EReference oldReferenceRef = referenceRef;
		referenceRef = newReferenceRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF, oldReferenceRef, referenceRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER:
				return basicSetSourceFilter(null, msgs);
			case ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER:
				return basicSetTargetFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER:
				return getSourceFilter();
			case ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER:
				return getTargetFilter();
			case ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF:
				if (resolve) return getReferenceRef();
				return basicGetReferenceRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER:
				setSourceFilter((Filter)newValue);
				return;
			case ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER:
				setTargetFilter((Filter)newValue);
				return;
			case ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF:
				setReferenceRef((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER:
				setSourceFilter((Filter)null);
				return;
			case ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER:
				setTargetFilter((Filter)null);
				return;
			case ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF:
				setReferenceRef((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_FILTER_OPTION__SOURCE_FILTER:
				return sourceFilter != null;
			case ConfigPackage.REFERENCE_FILTER_OPTION__TARGET_FILTER:
				return targetFilter != null;
			case ConfigPackage.REFERENCE_FILTER_OPTION__REFERENCE_REF:
				return referenceRef != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferenceFilterOptionImpl
