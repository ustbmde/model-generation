/**
 */
package edu.pku.ustb.rt4mt.util;

import edu.pku.ustb.rt4mt.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage
 * @generated
 */
public class Rt4mtSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Rt4mtPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rt4mtSwitch() {
		if (modelPackage == null) {
			modelPackage = Rt4mtPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL: {
				RegressionTestModel regressionTestModel = (RegressionTestModel)theEObject;
				T result = caseRegressionTestModel(regressionTestModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_CASE: {
				TestCase testCase = (TestCase)theEObject;
				T result = caseTestCase(testCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_ROUND: {
				TestRound testRound = (TestRound)theEObject;
				T result = caseTestRound(testRound);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_EXECUTION: {
				TestExecution testExecution = (TestExecution)theEObject;
				T result = caseTestExecution(testExecution);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_CASE_GROUP: {
				TestCaseGroup testCaseGroup = (TestCaseGroup)theEObject;
				T result = caseTestCaseGroup(testCaseGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_ROUND_GROUP: {
				TestRoundGroup testRoundGroup = (TestRoundGroup)theEObject;
				T result = caseTestRoundGroup(testRoundGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION_GROUP: {
				InputParameterConfigurationGroup inputParameterConfigurationGroup = (InputParameterConfigurationGroup)theEObject;
				T result = caseInputParameterConfigurationGroup(inputParameterConfigurationGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.OUTPUT_PARAMETER_CONFIGURATION_GROUP: {
				OutputParameterConfigurationGroup outputParameterConfigurationGroup = (OutputParameterConfigurationGroup)theEObject;
				T result = caseOutputParameterConfigurationGroup(outputParameterConfigurationGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.PARAMETER_CONFIGURATION: {
				ParameterConfiguration parameterConfiguration = (ParameterConfiguration)theEObject;
				T result = caseParameterConfiguration(parameterConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION: {
				InputParameterConfiguration inputParameterConfiguration = (InputParameterConfiguration)theEObject;
				T result = caseInputParameterConfiguration(inputParameterConfiguration);
				if (result == null) result = caseParameterConfiguration(inputParameterConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.OUTPUT_PARAMETER_CONFIGURATION: {
				OutputParameterConfiguration outputParameterConfiguration = (OutputParameterConfiguration)theEObject;
				T result = caseOutputParameterConfiguration(outputParameterConfiguration);
				if (result == null) result = caseParameterConfiguration(outputParameterConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.TEST_DATA: {
				TestData testData = (TestData)theEObject;
				T result = caseTestData(testData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Rt4mtPackage.EXPECTED_OUTPUT: {
				ExpectedOutput expectedOutput = (ExpectedOutput)theEObject;
				T result = caseExpectedOutput(expectedOutput);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regression Test Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regression Test Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegressionTestModel(RegressionTestModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestCase(TestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Round</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Round</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestRound(TestRound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Execution</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Execution</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestExecution(TestExecution object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Case Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Case Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestCaseGroup(TestCaseGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Round Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Round Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestRoundGroup(TestRoundGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Parameter Configuration Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input Parameter Configuration Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputParameterConfigurationGroup(InputParameterConfigurationGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Parameter Configuration Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Parameter Configuration Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputParameterConfigurationGroup(OutputParameterConfigurationGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterConfiguration(ParameterConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Parameter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input Parameter Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputParameterConfiguration(InputParameterConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Parameter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Parameter Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputParameterConfiguration(OutputParameterConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestData(TestData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expected Output</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expected Output</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpectedOutput(ExpectedOutput object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Rt4mtSwitch
