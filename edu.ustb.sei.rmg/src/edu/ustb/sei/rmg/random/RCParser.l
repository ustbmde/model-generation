
Options in effect for RCParser.g:

    ACTION-BLOCK=("RCParser.java","/.","./")

    AST-DIRECTORY=""  AST-TYPE="Condition"  NOATTRIBUTES  NOAUTOMATIC-AST  
    NOBACKTRACK  BYTE  CONFLICTS  DAT-DIRECTORY=""  DAT-FILE="RCParserdcl.data"
    DCL-FILE="RCParserdcl.java"  NODEBUG  DEF-FILE="RCParserdef.java"  
    DIRECTORY-PREFIX=""  NOEDIT  ERROR-MAPS  ESCAPE='$'  
    EXPORT-TERMINALS=("RCParserexp.java","","")  EXTENDS-PARSETABLE  
    FACTORY="new "  FILE-PREFIX="RCParser"  NOFIRST  NOFOLLOW  NOGLR  
    NOGOTO-DEFAULT  GRM-FILE="RCParser.g"  IMP-FILE="RCParserimp.java"  
    IMPORT-TERMINALS="RCLexer.g"  INCLUDE-DIRECTORY="./"  LALR=2  LEGACY  NOLIST
    MARGIN=4  MAX-CASES=1024  NAMES=OPTIMIZED  NONT-CHECK  OR_MARKER='|'  
    OUT-DIRECTORY=""  PACKAGE="edu.ustb.sei.rmg.random"  NOPARENT-SAVE  
    PARSETABLE-INTERFACES="lpg.runtime.ParseTable"  PREFIX="TK_"  PRIORITY  
    PROGRAMMING_LANGUAGE=JAVA  PRS-FILE="RCParserprs.java"  NOQUIET  READ-REDUCE
    REMAP-TERMINALS  RULE_CLASSNAMES=SEQUENTIAL  SCOPES  NOSERIALIZE  
    NOSHIFT-DEFAULT  NOSINGLE-PRODUCTIONS  NOSOFT-KEYWORDS  NOSTATES  SUFFIX=""
    SYM-FILE="RCParsersym.java"  TAB-FILE="RCParser.t"  TABLE  
    TEMPLATE="./dtParserTemplateF.gi"  TRACE=CONFLICTS  NOVARIABLES  NOVERBOSE
    NOVISITOR  VISITOR-TYPE="Visitor"  WARNINGS  NOXREF  

RCParser.g:101:5:101:11:2192:2198: Informative: The terminal PERCENT is useless.
RCParser.g:105:5:105:12:2263:2270: Informative: The terminal MULTIPLY is useless.
RCParser.g:106:5:106:10:2285:2290: Informative: The terminal DIVIDE is useless.
RCParser.g:110:5:110:9:2363:2367: Informative: The terminal EQUAL is useless.
RCParser.g:111:5:111:17:2390:2402: Informative: The terminal GREATER_EQUAL is useless.
RCParser.g:112:5:112:14:2418:2427: Informative: The terminal LESS_EQUAL is useless.
RCParser.g:122:5:122:9:2610:2614: Informative: The terminal ARROW is useless.
RCParser.g:123:5:123:7:2635:2637: Informative: The terminal BAR is useless.
RCParser.g:126:5:126:14:2707:2716: Informative: The terminal COLONCOLON is useless.
RCParser.g:127:5:127:13:2732:2740: Informative: The terminal SEMICOLON is useless.
RCParser.g:75:9:75:12:1916:1919: Informative: The terminal bern is useless.


RCParser.g:135:9:135:12:2868:2871: Informative: Grammar is  LALR(1).

Number of Terminals: 49
Number of Nonterminals: 72
Number of Productions: 158
Number of Items: 488
Number of Scopes: 17
Number of States: 167
Number of Shift actions: 247
Number of Goto actions: 269
Number of Shift/Reduce actions: 280
Number of Goto/Reduce actions: 377
Number of Reduce actions: 119
Number of Shift-Reduce conflicts: 0
Number of Reduce-Reduce conflicts: 0
Number of Keyword/Identifier Shift conflicts: 0
Number of Keyword/Identifier Shift-Reduce conflicts: 0
Number of Keyword/Identifier Reduce-Reduce conflicts: 0

Number of entries in base Action Table: 813
Additional space required for compaction of Action Table: 0.1%

Number of unique terminal states: 132
Number of Shift actions saved by merging: 156
Number of Conflict points saved by merging: 0
Number of Reduce actions saved by merging: 1
Number of Reduce saved by default: 115

Number of entries in Terminal Action Table: 506
Additional space required for compaction of Terminal Table: 4.1%

Actions in Compressed Tables:
     Number of Shifts: 178
     Number of Shift/Reduces: 193
     Number of Gotos: 269
     Number of Goto/Reduces: 377
     Number of Reduces: 3
     Number of Defaults: 25

Parsing Tables storage:
    Storage required for BASE_CHECK: 1944 Bytes
    Storage required for BASE_ACTION: 1946 Bytes
    Storage required for TERM_CHECK: 558 Bytes
    Storage required for TERM_ACTION: 1056 Bytes

Error maps storage:
    Storage required for ACTION_SYMBOLS_BASE map: 336 Bytes
    Storage required for ACTION_SYMBOLS_RANGE map: 172 Bytes
    Storage required for NACTION_SYMBOLS_BASE map: 168 Bytes
    Storage required for NACTION_SYMBOLS_RANGE map: 68 Bytes
    Storage required for TERMINAL_INDEX map: 50 Bytes
    Storage required for NON_TERMINAL_INDEX map: 74 Bytes

    Storage required for SCOPE_PREFIX map: 17 Bytes
    Storage required for SCOPE_SUFFIX map: 17 Bytes
    Storage required for SCOPE_LHS_SYMBOL map: 17 Bytes
    Storage required for SCOPE_LOOK_AHEAD map: 17 Bytes
    Storage required for SCOPE_STATE_SET map: 17 Bytes
    Storage required for SCOPE_RIGHT_SIDE map: 61 Bytes
    Storage required for SCOPE_STATE map: 196 Bytes
    Storage required for IN_SYMB map: 168 Bytes

    Number of names: 104
    Number of characters in name: 1244
