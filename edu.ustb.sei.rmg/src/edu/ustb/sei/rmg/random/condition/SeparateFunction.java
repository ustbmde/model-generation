package edu.ustb.sei.rmg.random.condition;

import java.util.Arrays;

import edu.ustb.sei.rmg.random.RandomUtil;

public class SeparateFunction extends Condition<Integer[]> {
	public Condition<Integer> sumCondition;
	public Condition<Integer> countCondition;
	

	@Override
	public Integer[] getValue(RandomUtil r, Object... extra){
		int sum = sumCondition.getValue(r);
		int count = countCondition.getValue(r);
		
		int[] div = new int[count+1];
		div[0] = 0;
		div[1] = sum;
		
		Integer[] res = new Integer[count];
		int i=0;
		for(i=0;i<div.length;i++){
			div[i] = r.randomInt(0, sum);
		}
		Arrays.sort(div);
		
		for(i=0;i<count;i++){
			res[i] = div[i+1]-div[i];
		}
		return res;
	}

}
