/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mt4mt.MT4MTFactory
 * @model kind="package"
 * @generated
 */
public interface MT4MTPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mt4mt";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/mt4mt";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mt4mt";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MT4MTPackage eINSTANCE = edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl <em>Test Suite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestSuite()
	 * @generated
	 */
	int TEST_SUITE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SUITE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Test Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SUITE__TEST_CASES = 1;

	/**
	 * The feature id for the '<em><b>Default Waiting Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SUITE__DEFAULT_WAITING_TIME = 2;

	/**
	 * The number of structural features of the '<em>Test Suite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SUITE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Test Suite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SUITE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestCaseImpl <em>Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.TestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestCase()
	 * @generated
	 */
	int TEST_CASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__ID = 0;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__METAMODELS = 1;

	/**
	 * The number of structural features of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE___BUILD = 0;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE___GET_DEFAULT_WAITING_TIME = 1;

	/**
	 * The number of operations of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.CustomizableTestCaseImpl <em>Customizable Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.CustomizableTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCustomizableTestCase()
	 * @generated
	 */
	int CUSTOMIZABLE_TEST_CASE = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE__ID = TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE__METAMODELS = TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE__ACTION = TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Customizable Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE_FEATURE_COUNT = TEST_CASE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE___BUILD = TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE___GET_DEFAULT_WAITING_TIME = TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Customizable Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMIZABLE_TEST_CASE_OPERATION_COUNT = TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedTestCaseImpl <em>Predefined Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.PredefinedTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPredefinedTestCase()
	 * @generated
	 */
	int PREDEFINED_TEST_CASE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE__ID = TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE__METAMODELS = TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE__INPUTS = TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Predefined Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE_FEATURE_COUNT = TEST_CASE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE___BUILD = TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE___GET_DEFAULT_WAITING_TIME = TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Predefined Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_TEST_CASE_OPERATION_COUNT = TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl <em>Predefined BX Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPredefinedBXTestCase()
	 * @generated
	 */
	int PREDEFINED_BX_TEST_CASE = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__ID = PREDEFINED_TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__METAMODELS = PREDEFINED_TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__INPUTS = PREDEFINED_TEST_CASE__INPUTS;

	/**
	 * The feature id for the '<em><b>Fwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER = PREDEFINED_TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__FWD_SOURCE = PREDEFINED_TEST_CASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Fwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__FWD_VIEW = PREDEFINED_TEST_CASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Bwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER = PREDEFINED_TEST_CASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Bwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__BWD_SOURCE = PREDEFINED_TEST_CASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Bwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__BWD_VIEW = PREDEFINED_TEST_CASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Bwd Updated Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE = PREDEFINED_TEST_CASE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Predefined BX Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE_FEATURE_COUNT = PREDEFINED_TEST_CASE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE___BUILD = PREDEFINED_TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE___GET_DEFAULT_WAITING_TIME = PREDEFINED_TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Predefined BX Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREDEFINED_BX_TEST_CASE_OPERATION_COUNT = PREDEFINED_TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.GetPutTestCaseImpl <em>Get Put Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.GetPutTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getGetPutTestCase()
	 * @generated
	 */
	int GET_PUT_TEST_CASE = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__ID = PREDEFINED_BX_TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__METAMODELS = PREDEFINED_BX_TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__INPUTS = PREDEFINED_BX_TEST_CASE__INPUTS;

	/**
	 * The feature id for the '<em><b>Fwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__FWD_LAUNCHER = PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER;

	/**
	 * The feature id for the '<em><b>Fwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__FWD_SOURCE = PREDEFINED_BX_TEST_CASE__FWD_SOURCE;

	/**
	 * The feature id for the '<em><b>Fwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__FWD_VIEW = PREDEFINED_BX_TEST_CASE__FWD_VIEW;

	/**
	 * The feature id for the '<em><b>Bwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__BWD_LAUNCHER = PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER;

	/**
	 * The feature id for the '<em><b>Bwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__BWD_SOURCE = PREDEFINED_BX_TEST_CASE__BWD_SOURCE;

	/**
	 * The feature id for the '<em><b>Bwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__BWD_VIEW = PREDEFINED_BX_TEST_CASE__BWD_VIEW;

	/**
	 * The feature id for the '<em><b>Bwd Updated Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE__BWD_UPDATED_SOURCE = PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE;

	/**
	 * The number of structural features of the '<em>Get Put Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE_FEATURE_COUNT = PREDEFINED_BX_TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE___BUILD = PREDEFINED_BX_TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE___GET_DEFAULT_WAITING_TIME = PREDEFINED_BX_TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Get Put Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GET_PUT_TEST_CASE_OPERATION_COUNT = PREDEFINED_BX_TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.PutGetTestCaseImpl <em>Put Get Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.PutGetTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPutGetTestCase()
	 * @generated
	 */
	int PUT_GET_TEST_CASE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__ID = PREDEFINED_BX_TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__METAMODELS = PREDEFINED_BX_TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__INPUTS = PREDEFINED_BX_TEST_CASE__INPUTS;

	/**
	 * The feature id for the '<em><b>Fwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__FWD_LAUNCHER = PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER;

	/**
	 * The feature id for the '<em><b>Fwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__FWD_SOURCE = PREDEFINED_BX_TEST_CASE__FWD_SOURCE;

	/**
	 * The feature id for the '<em><b>Fwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__FWD_VIEW = PREDEFINED_BX_TEST_CASE__FWD_VIEW;

	/**
	 * The feature id for the '<em><b>Bwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__BWD_LAUNCHER = PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER;

	/**
	 * The feature id for the '<em><b>Bwd Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__BWD_SOURCE = PREDEFINED_BX_TEST_CASE__BWD_SOURCE;

	/**
	 * The feature id for the '<em><b>Bwd View</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__BWD_VIEW = PREDEFINED_BX_TEST_CASE__BWD_VIEW;

	/**
	 * The feature id for the '<em><b>Bwd Updated Source</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE__BWD_UPDATED_SOURCE = PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE;

	/**
	 * The number of structural features of the '<em>Put Get Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE_FEATURE_COUNT = PREDEFINED_BX_TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE___BUILD = PREDEFINED_BX_TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE___GET_DEFAULT_WAITING_TIME = PREDEFINED_BX_TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Put Get Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUT_GET_TEST_CASE_OPERATION_COUNT = PREDEFINED_BX_TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestActionImpl <em>Test Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.TestActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestAction()
	 * @generated
	 */
	int TEST_ACTION = 6;

	/**
	 * The number of structural features of the '<em>Test Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ACTION_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ACTION___BUILD = 0;

	/**
	 * The number of operations of the '<em>Test Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ACTION_OPERATION_COUNT = 1;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.SequenceActionImpl <em>Sequence Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.SequenceActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSequenceAction()
	 * @generated
	 */
	int SEQUENCE_ACTION = 8;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION__ACTIONS = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequence Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Sequence Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl <em>Load Model Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getLoadModelAction()
	 * @generated
	 */
	int LOAD_MODEL_ACTION = 9;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_MODEL_ACTION__MODEL_URI = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_MODEL_ACTION__KEY = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Load Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_MODEL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_MODEL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Load Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_MODEL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.SaveModelActionImpl <em>Save Model Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.SaveModelActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSaveModelAction()
	 * @generated
	 */
	int SAVE_MODEL_ACTION = 10;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_MODEL_ACTION__MODEL_URI = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_MODEL_ACTION__KEY = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Save Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_MODEL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_MODEL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Save Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVE_MODEL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl <em>Execute Launcher Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExecuteLauncherAction()
	 * @generated
	 */
	int EXECUTE_LAUNCHER_ACTION = 11;

	/**
	 * The feature id for the '<em><b>Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_LAUNCHER_ACTION__LAUNCHER = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_LAUNCHER_ACTION__MAX_WAIT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Execute Launcher Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_LAUNCHER_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_LAUNCHER_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Execute Launcher Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_LAUNCHER_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.EvaluateOCLQueryActionImpl <em>Evaluate OCL Query Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.EvaluateOCLQueryActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getEvaluateOCLQueryAction()
	 * @generated
	 */
	int EVALUATE_OCL_QUERY_ACTION = 12;

	/**
	 * The feature id for the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION__MODEL_KEY = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Query</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION__QUERY = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Result Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION__RESULT_KEY = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Import Context Variables</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION__IMPORT_CONTEXT_VARIABLES = TEST_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Evaluate OCL Query Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Evaluate OCL Query Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATE_OCL_QUERY_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl <em>Copy Model Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCopyModelAction()
	 * @generated
	 */
	int COPY_MODEL_ACTION = 13;

	/**
	 * The feature id for the '<em><b>From URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_MODEL_ACTION__FROM_URI = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_MODEL_ACTION__TO_URI = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Copy Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_MODEL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_MODEL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Copy Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_MODEL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl <em>Compare Model Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCompareModelAction()
	 * @generated
	 */
	int COMPARE_MODEL_ACTION = 14;

	/**
	 * The feature id for the '<em><b>Left URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION__LEFT_URI = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION__RIGHT_URI = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Diff Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION__DIFF_KEY = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Compare Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Compare Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_MODEL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl <em>Assert Comparison Result Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAssertComparisonResultAction()
	 * @generated
	 */
	int ASSERT_COMPARISON_RESULT_ACTION = 15;

	/**
	 * The feature id for the '<em><b>Diff Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Different Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION__KIND = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Assert Comparison Result Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Assert Comparison Result Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_COMPARISON_RESULT_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl <em>Assert OCL Invariant Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAssertOCLInvariantAction()
	 * @generated
	 */
	int ASSERT_OCL_INVARIANT_ACTION = 16;

	/**
	 * The feature id for the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Invariant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION__INVARIANT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Import Context Variables</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Assert OCL Invariant Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Assert OCL Invariant Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERT_OCL_INVARIANT_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.UserActionImpl <em>User Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.UserActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getUserAction()
	 * @generated
	 */
	int USER_ACTION = 17;

	/**
	 * The feature id for the '<em><b>Class File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACTION__CLASS_FILE_URI = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>User Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.SetVariableActionImpl <em>Set Variable Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.SetVariableActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSetVariableAction()
	 * @generated
	 */
	int SET_VARIABLE_ACTION = 18;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_VARIABLE_ACTION__KEY = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_VARIABLE_ACTION__VALUE = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Set Variable Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_VARIABLE_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_VARIABLE_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Set Variable Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_VARIABLE_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl <em>Update Model Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getUpdateModelAction()
	 * @generated
	 */
	int UPDATE_MODEL_ACTION = 19;

	/**
	 * The feature id for the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_MODEL_ACTION__MODEL_KEY = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Update Operations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_MODEL_ACTION__UPDATE_OPERATIONS = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Update Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_MODEL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_MODEL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Update Model Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATE_MODEL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl <em>Wrapper Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getWrapperTestCase()
	 * @generated
	 */
	int WRAPPER_TEST_CASE = 20;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__ID = TEST_CASE__ID;

	/**
	 * The feature id for the '<em><b>Metamodels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__METAMODELS = TEST_CASE__METAMODELS;

	/**
	 * The feature id for the '<em><b>Before Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__BEFORE_TEST = TEST_CASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>After Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__AFTER_TEST = TEST_CASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Core Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__CORE_TEST = TEST_CASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE__REPEAT = TEST_CASE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Wrapper Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE_FEATURE_COUNT = TEST_CASE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE___BUILD = TEST_CASE___BUILD;

	/**
	 * The operation id for the '<em>Get Default Waiting Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE___GET_DEFAULT_WAITING_TIME = TEST_CASE___GET_DEFAULT_WAITING_TIME;

	/**
	 * The number of operations of the '<em>Wrapper Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRAPPER_TEST_CASE_OPERATION_COUNT = TEST_CASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl <em>Auto Executor Test Suite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAutoExecutorTestSuite()
	 * @generated
	 */
	int AUTO_EXECUTOR_TEST_SUITE = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__NAME = TEST_SUITE__NAME;

	/**
	 * The feature id for the '<em><b>Test Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__TEST_CASES = TEST_SUITE__TEST_CASES;

	/**
	 * The feature id for the '<em><b>Default Waiting Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__DEFAULT_WAITING_TIME = TEST_SUITE__DEFAULT_WAITING_TIME;

	/**
	 * The feature id for the '<em><b>Executors</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__EXECUTORS = TEST_SUITE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Executables</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES = TEST_SUITE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Executor Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME = TEST_SUITE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Auto Executor Test Suite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE_FEATURE_COUNT = TEST_SUITE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Auto Executor Test Suite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTO_EXECUTOR_TEST_SUITE_OPERATION_COUNT = TEST_SUITE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.ClearActionImpl <em>Clear Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.ClearActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getClearAction()
	 * @generated
	 */
	int CLEAR_ACTION = 22;

	/**
	 * The feature id for the '<em><b>Files</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ACTION__FILES = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Clear Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>Clear Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl <em>External Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExternalAction()
	 * @generated
	 */
	int EXTERNAL_ACTION = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION__NAME = TEST_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Result Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION__RESULT_NAME = TEST_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION__PARAMETERS = TEST_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION__ACTION = TEST_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>External Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_FEATURE_COUNT = TEST_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Build</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION___BUILD = TEST_ACTION___BUILD;

	/**
	 * The number of operations of the '<em>External Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_OPERATION_COUNT = TEST_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionParameterImpl <em>External Action Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.impl.ExternalActionParameterImpl
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExternalActionParameter()
	 * @generated
	 */
	int EXTERNAL_ACTION_PARAMETER = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_PARAMETER__VALUE = 1;

	/**
	 * The number of structural features of the '<em>External Action Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>External Action Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_ACTION_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mt4mt.DiffKind <em>Diff Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mt4mt.DiffKind
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getDiffKind()
	 * @generated
	 */
	int DIFF_KIND = 25;

	/**
	 * The meta object id for the '<em>IAction</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mt4mt.base.IAction
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getIAction()
	 * @generated
	 */
	int IACTION = 26;


	/**
	 * The meta object id for the '<em>Abstract Transformation Test Case</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAbstractTransformationTestCase()
	 * @generated
	 */
	int ABSTRACT_TRANSFORMATION_TEST_CASE = 27;


	/**
	 * The meta object id for the '<em>IExternal Action</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mt4mt.base.updates.IExternalAction
	 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getIExternalAction()
	 * @generated
	 */
	int IEXTERNAL_ACTION = 28;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.TestSuite <em>Test Suite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Suite</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestSuite
	 * @generated
	 */
	EClass getTestSuite();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.TestSuite#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestSuite#getName()
	 * @see #getTestSuite()
	 * @generated
	 */
	EAttribute getTestSuite_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mt4mt.TestSuite#getTestCases <em>Test Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Cases</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestSuite#getTestCases()
	 * @see #getTestSuite()
	 * @generated
	 */
	EReference getTestSuite_TestCases();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.TestSuite#getDefaultWaitingTime <em>Default Waiting Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Waiting Time</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestSuite#getDefaultWaitingTime()
	 * @see #getTestSuite()
	 * @generated
	 */
	EAttribute getTestSuite_DefaultWaitingTime();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase
	 * @generated
	 */
	EClass getTestCase();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.TestCase#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase#getId()
	 * @see #getTestCase()
	 * @generated
	 */
	EAttribute getTestCase_Id();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.TestCase#getMetamodels <em>Metamodels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Metamodels</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase#getMetamodels()
	 * @see #getTestCase()
	 * @generated
	 */
	EAttribute getTestCase_Metamodels();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.mt4mt.TestCase#build() <em>Build</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Build</em>' operation.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase#build()
	 * @generated
	 */
	EOperation getTestCase__Build();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.mt4mt.TestCase#getDefaultWaitingTime() <em>Get Default Waiting Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Default Waiting Time</em>' operation.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase#getDefaultWaitingTime()
	 * @generated
	 */
	EOperation getTestCase__GetDefaultWaitingTime();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase <em>Customizable Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customizable Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CustomizableTestCase
	 * @generated
	 */
	EClass getCustomizableTestCase();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CustomizableTestCase#getAction()
	 * @see #getCustomizableTestCase()
	 * @generated
	 */
	EReference getCustomizableTestCase_Action();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.PredefinedTestCase <em>Predefined Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Predefined Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedTestCase
	 * @generated
	 */
	EClass getPredefinedTestCase();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedTestCase#getInputs <em>Inputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Inputs</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedTestCase#getInputs()
	 * @see #getPredefinedTestCase()
	 * @generated
	 */
	EAttribute getPredefinedTestCase_Inputs();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.GetPutTestCase <em>Get Put Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Get Put Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.GetPutTestCase
	 * @generated
	 */
	EClass getGetPutTestCase();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.PutGetTestCase <em>Put Get Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Put Get Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PutGetTestCase
	 * @generated
	 */
	EClass getPutGetTestCase();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.TestAction <em>Test Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.TestAction
	 * @generated
	 */
	EClass getTestAction();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.mt4mt.TestAction#build() <em>Build</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Build</em>' operation.
	 * @see edu.ustb.sei.mde.mt4mt.TestAction#build()
	 * @generated
	 */
	EOperation getTestAction__Build();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase <em>Predefined BX Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Predefined BX Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase
	 * @generated
	 */
	EClass getPredefinedBXTestCase();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdLauncher <em>Fwd Launcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fwd Launcher</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdLauncher()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_FwdLauncher();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdSource <em>Fwd Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Fwd Source</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdSource()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_FwdSource();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdView <em>Fwd View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Fwd View</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdView()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_FwdView();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdLauncher <em>Bwd Launcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bwd Launcher</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdLauncher()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_BwdLauncher();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdSource <em>Bwd Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Bwd Source</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdSource()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_BwdSource();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdView <em>Bwd View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Bwd View</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdView()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_BwdView();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdUpdatedSource <em>Bwd Updated Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Bwd Updated Source</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdUpdatedSource()
	 * @see #getPredefinedBXTestCase()
	 * @generated
	 */
	EAttribute getPredefinedBXTestCase_BwdUpdatedSource();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.SequenceAction <em>Sequence Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SequenceAction
	 * @generated
	 */
	EClass getSequenceAction();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mt4mt.SequenceAction#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SequenceAction#getActions()
	 * @see #getSequenceAction()
	 * @generated
	 */
	EReference getSequenceAction_Actions();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction <em>Load Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Load Model Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.LoadModelAction
	 * @generated
	 */
	EClass getLoadModelAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getModelURI <em>Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.LoadModelAction#getModelURI()
	 * @see #getLoadModelAction()
	 * @generated
	 */
	EAttribute getLoadModelAction_ModelURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.LoadModelAction#getKey()
	 * @see #getLoadModelAction()
	 * @generated
	 */
	EAttribute getLoadModelAction_Key();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.SaveModelAction <em>Save Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Save Model Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SaveModelAction
	 * @generated
	 */
	EClass getSaveModelAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.SaveModelAction#getModelURI <em>Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SaveModelAction#getModelURI()
	 * @see #getSaveModelAction()
	 * @generated
	 */
	EAttribute getSaveModelAction_ModelURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.SaveModelAction#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SaveModelAction#getKey()
	 * @see #getSaveModelAction()
	 * @generated
	 */
	EAttribute getSaveModelAction_Key();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction <em>Execute Launcher Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execute Launcher Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction
	 * @generated
	 */
	EClass getExecuteLauncherAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getLauncher <em>Launcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Launcher</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getLauncher()
	 * @see #getExecuteLauncherAction()
	 * @generated
	 */
	EAttribute getExecuteLauncherAction_Launcher();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getMaxWait <em>Max Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Wait</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getMaxWait()
	 * @see #getExecuteLauncherAction()
	 * @generated
	 */
	EAttribute getExecuteLauncherAction_MaxWait();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction <em>Evaluate OCL Query Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluate OCL Query Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction
	 * @generated
	 */
	EClass getEvaluateOCLQueryAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getModelKey <em>Model Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getModelKey()
	 * @see #getEvaluateOCLQueryAction()
	 * @generated
	 */
	EAttribute getEvaluateOCLQueryAction_ModelKey();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getQuery <em>Query</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Query</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getQuery()
	 * @see #getEvaluateOCLQueryAction()
	 * @generated
	 */
	EAttribute getEvaluateOCLQueryAction_Query();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getResultKey <em>Result Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getResultKey()
	 * @see #getEvaluateOCLQueryAction()
	 * @generated
	 */
	EAttribute getEvaluateOCLQueryAction_ResultKey();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getImportContextVariables <em>Import Context Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Import Context Variables</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getImportContextVariables()
	 * @see #getEvaluateOCLQueryAction()
	 * @generated
	 */
	EAttribute getEvaluateOCLQueryAction_ImportContextVariables();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction <em>Copy Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Model Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CopyModelAction
	 * @generated
	 */
	EClass getCopyModelAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getFromURI <em>From URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CopyModelAction#getFromURI()
	 * @see #getCopyModelAction()
	 * @generated
	 */
	EAttribute getCopyModelAction_FromURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getToURI <em>To URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CopyModelAction#getToURI()
	 * @see #getCopyModelAction()
	 * @generated
	 */
	EAttribute getCopyModelAction_ToURI();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction <em>Compare Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compare Model Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CompareModelAction
	 * @generated
	 */
	EClass getCompareModelAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getLeftURI <em>Left URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CompareModelAction#getLeftURI()
	 * @see #getCompareModelAction()
	 * @generated
	 */
	EAttribute getCompareModelAction_LeftURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getRightURI <em>Right URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CompareModelAction#getRightURI()
	 * @see #getCompareModelAction()
	 * @generated
	 */
	EAttribute getCompareModelAction_RightURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getDiffKey <em>Diff Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diff Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.CompareModelAction#getDiffKey()
	 * @see #getCompareModelAction()
	 * @generated
	 */
	EAttribute getCompareModelAction_DiffKey();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction <em>Assert Comparison Result Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assert Comparison Result Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction
	 * @generated
	 */
	EClass getAssertComparisonResultAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDiffKey <em>Diff Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diff Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDiffKey()
	 * @see #getAssertComparisonResultAction()
	 * @generated
	 */
	EAttribute getAssertComparisonResultAction_DiffKey();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDifferentCount <em>Different Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Different Count</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDifferentCount()
	 * @see #getAssertComparisonResultAction()
	 * @generated
	 */
	EAttribute getAssertComparisonResultAction_DifferentCount();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getKind()
	 * @see #getAssertComparisonResultAction()
	 * @generated
	 */
	EAttribute getAssertComparisonResultAction_Kind();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction <em>Assert OCL Invariant Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assert OCL Invariant Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction
	 * @generated
	 */
	EClass getAssertOCLInvariantAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getModelKey <em>Model Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getModelKey()
	 * @see #getAssertOCLInvariantAction()
	 * @generated
	 */
	EAttribute getAssertOCLInvariantAction_ModelKey();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getInvariant <em>Invariant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Invariant</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getInvariant()
	 * @see #getAssertOCLInvariantAction()
	 * @generated
	 */
	EAttribute getAssertOCLInvariantAction_Invariant();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getImportContextVariables <em>Import Context Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Import Context Variables</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getImportContextVariables()
	 * @see #getAssertOCLInvariantAction()
	 * @generated
	 */
	EAttribute getAssertOCLInvariantAction_ImportContextVariables();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.UserAction <em>User Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.UserAction
	 * @generated
	 */
	EClass getUserAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.UserAction#getClassFileURI <em>Class File URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class File URI</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.UserAction#getClassFileURI()
	 * @see #getUserAction()
	 * @generated
	 */
	EAttribute getUserAction_ClassFileURI();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.SetVariableAction <em>Set Variable Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Variable Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SetVariableAction
	 * @generated
	 */
	EClass getSetVariableAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.SetVariableAction#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SetVariableAction#getKey()
	 * @see #getSetVariableAction()
	 * @generated
	 */
	EAttribute getSetVariableAction_Key();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.SetVariableAction#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.SetVariableAction#getValue()
	 * @see #getSetVariableAction()
	 * @generated
	 */
	EAttribute getSetVariableAction_Value();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction <em>Update Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Update Model Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.UpdateModelAction
	 * @generated
	 */
	EClass getUpdateModelAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction#getModelKey <em>Model Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Key</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.UpdateModelAction#getModelKey()
	 * @see #getUpdateModelAction()
	 * @generated
	 */
	EAttribute getUpdateModelAction_ModelKey();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction#getUpdateOperations <em>Update Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Update Operations</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.UpdateModelAction#getUpdateOperations()
	 * @see #getUpdateModelAction()
	 * @generated
	 */
	EAttribute getUpdateModelAction_UpdateOperations();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase <em>Wrapper Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wrapper Test Case</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase
	 * @generated
	 */
	EClass getWrapperTestCase();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getBeforeTest <em>Before Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Before Test</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase#getBeforeTest()
	 * @see #getWrapperTestCase()
	 * @generated
	 */
	EReference getWrapperTestCase_BeforeTest();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getAfterTest <em>After Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>After Test</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase#getAfterTest()
	 * @see #getWrapperTestCase()
	 * @generated
	 */
	EReference getWrapperTestCase_AfterTest();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getCoreTest <em>Core Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Core Test</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase#getCoreTest()
	 * @see #getWrapperTestCase()
	 * @generated
	 */
	EReference getWrapperTestCase_CoreTest();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repeat</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase#getRepeat()
	 * @see #getWrapperTestCase()
	 * @generated
	 */
	EAttribute getWrapperTestCase_Repeat();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite <em>Auto Executor Test Suite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Auto Executor Test Suite</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite
	 * @generated
	 */
	EClass getAutoExecutorTestSuite();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutors <em>Executors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Executors</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutors()
	 * @see #getAutoExecutorTestSuite()
	 * @generated
	 */
	EAttribute getAutoExecutorTestSuite_Executors();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutables <em>Executables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Executables</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutables()
	 * @see #getAutoExecutorTestSuite()
	 * @generated
	 */
	EAttribute getAutoExecutorTestSuite_Executables();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutorAttributeName <em>Executor Attribute Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Executor Attribute Name</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutorAttributeName()
	 * @see #getAutoExecutorTestSuite()
	 * @generated
	 */
	EAttribute getAutoExecutorTestSuite_ExecutorAttributeName();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.ClearAction <em>Clear Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ClearAction
	 * @generated
	 */
	EClass getClearAction();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mt4mt.ClearAction#getFiles <em>Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Files</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ClearAction#getFiles()
	 * @see #getClearAction()
	 * @generated
	 */
	EAttribute getClearAction_Files();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.ExternalAction <em>External Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction
	 * @generated
	 */
	EClass getExternalAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExternalAction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction#getName()
	 * @see #getExternalAction()
	 * @generated
	 */
	EAttribute getExternalAction_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExternalAction#getResultName <em>Result Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Name</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction#getResultName()
	 * @see #getExternalAction()
	 * @generated
	 */
	EAttribute getExternalAction_ResultName();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mt4mt.ExternalAction#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction#getParameters()
	 * @see #getExternalAction()
	 * @generated
	 */
	EReference getExternalAction_Parameters();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExternalAction#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction#getAction()
	 * @see #getExternalAction()
	 * @generated
	 */
	EAttribute getExternalAction_Action();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter <em>External Action Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Action Parameter</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalActionParameter
	 * @generated
	 */
	EClass getExternalActionParameter();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalActionParameter#getName()
	 * @see #getExternalActionParameter()
	 * @generated
	 */
	EAttribute getExternalActionParameter_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalActionParameter#getValue()
	 * @see #getExternalActionParameter()
	 * @generated
	 */
	EAttribute getExternalActionParameter_Value();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.mt4mt.DiffKind <em>Diff Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Diff Kind</em>'.
	 * @see edu.ustb.sei.mde.mt4mt.DiffKind
	 * @generated
	 */
	EEnum getDiffKind();

	/**
	 * Returns the meta object for data type '{@link edu.ustb.sei.mt4mt.base.IAction <em>IAction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IAction</em>'.
	 * @see edu.ustb.sei.mt4mt.base.IAction
	 * @model instanceClass="edu.ustb.sei.mt4mt.base.IAction"
	 * @generated
	 */
	EDataType getIAction();

	/**
	 * Returns the meta object for data type '{@link edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase <em>Abstract Transformation Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Abstract Transformation Test Case</em>'.
	 * @see edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase
	 * @model instanceClass="edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase"
	 * @generated
	 */
	EDataType getAbstractTransformationTestCase();

	/**
	 * Returns the meta object for data type '{@link edu.ustb.sei.mt4mt.base.updates.IExternalAction <em>IExternal Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IExternal Action</em>'.
	 * @see edu.ustb.sei.mt4mt.base.updates.IExternalAction
	 * @model instanceClass="edu.ustb.sei.mt4mt.base.updates.IExternalAction"
	 * @generated
	 */
	EDataType getIExternalAction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MT4MTFactory getMT4MTFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl <em>Test Suite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestSuite()
		 * @generated
		 */
		EClass TEST_SUITE = eINSTANCE.getTestSuite();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_SUITE__NAME = eINSTANCE.getTestSuite_Name();

		/**
		 * The meta object literal for the '<em><b>Test Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_SUITE__TEST_CASES = eINSTANCE.getTestSuite_TestCases();

		/**
		 * The meta object literal for the '<em><b>Default Waiting Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_SUITE__DEFAULT_WAITING_TIME = eINSTANCE.getTestSuite_DefaultWaitingTime();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestCaseImpl <em>Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.TestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestCase()
		 * @generated
		 */
		EClass TEST_CASE = eINSTANCE.getTestCase();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE__ID = eINSTANCE.getTestCase_Id();

		/**
		 * The meta object literal for the '<em><b>Metamodels</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE__METAMODELS = eINSTANCE.getTestCase_Metamodels();

		/**
		 * The meta object literal for the '<em><b>Build</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEST_CASE___BUILD = eINSTANCE.getTestCase__Build();

		/**
		 * The meta object literal for the '<em><b>Get Default Waiting Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEST_CASE___GET_DEFAULT_WAITING_TIME = eINSTANCE.getTestCase__GetDefaultWaitingTime();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.CustomizableTestCaseImpl <em>Customizable Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.CustomizableTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCustomizableTestCase()
		 * @generated
		 */
		EClass CUSTOMIZABLE_TEST_CASE = eINSTANCE.getCustomizableTestCase();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOMIZABLE_TEST_CASE__ACTION = eINSTANCE.getCustomizableTestCase_Action();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedTestCaseImpl <em>Predefined Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.PredefinedTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPredefinedTestCase()
		 * @generated
		 */
		EClass PREDEFINED_TEST_CASE = eINSTANCE.getPredefinedTestCase();

		/**
		 * The meta object literal for the '<em><b>Inputs</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_TEST_CASE__INPUTS = eINSTANCE.getPredefinedTestCase_Inputs();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.GetPutTestCaseImpl <em>Get Put Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.GetPutTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getGetPutTestCase()
		 * @generated
		 */
		EClass GET_PUT_TEST_CASE = eINSTANCE.getGetPutTestCase();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.PutGetTestCaseImpl <em>Put Get Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.PutGetTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPutGetTestCase()
		 * @generated
		 */
		EClass PUT_GET_TEST_CASE = eINSTANCE.getPutGetTestCase();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.TestActionImpl <em>Test Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.TestActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getTestAction()
		 * @generated
		 */
		EClass TEST_ACTION = eINSTANCE.getTestAction();

		/**
		 * The meta object literal for the '<em><b>Build</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TEST_ACTION___BUILD = eINSTANCE.getTestAction__Build();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl <em>Predefined BX Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getPredefinedBXTestCase()
		 * @generated
		 */
		EClass PREDEFINED_BX_TEST_CASE = eINSTANCE.getPredefinedBXTestCase();

		/**
		 * The meta object literal for the '<em><b>Fwd Launcher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER = eINSTANCE.getPredefinedBXTestCase_FwdLauncher();

		/**
		 * The meta object literal for the '<em><b>Fwd Source</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__FWD_SOURCE = eINSTANCE.getPredefinedBXTestCase_FwdSource();

		/**
		 * The meta object literal for the '<em><b>Fwd View</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__FWD_VIEW = eINSTANCE.getPredefinedBXTestCase_FwdView();

		/**
		 * The meta object literal for the '<em><b>Bwd Launcher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER = eINSTANCE.getPredefinedBXTestCase_BwdLauncher();

		/**
		 * The meta object literal for the '<em><b>Bwd Source</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__BWD_SOURCE = eINSTANCE.getPredefinedBXTestCase_BwdSource();

		/**
		 * The meta object literal for the '<em><b>Bwd View</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__BWD_VIEW = eINSTANCE.getPredefinedBXTestCase_BwdView();

		/**
		 * The meta object literal for the '<em><b>Bwd Updated Source</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE = eINSTANCE.getPredefinedBXTestCase_BwdUpdatedSource();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.SequenceActionImpl <em>Sequence Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.SequenceActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSequenceAction()
		 * @generated
		 */
		EClass SEQUENCE_ACTION = eINSTANCE.getSequenceAction();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_ACTION__ACTIONS = eINSTANCE.getSequenceAction_Actions();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl <em>Load Model Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getLoadModelAction()
		 * @generated
		 */
		EClass LOAD_MODEL_ACTION = eINSTANCE.getLoadModelAction();

		/**
		 * The meta object literal for the '<em><b>Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOAD_MODEL_ACTION__MODEL_URI = eINSTANCE.getLoadModelAction_ModelURI();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOAD_MODEL_ACTION__KEY = eINSTANCE.getLoadModelAction_Key();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.SaveModelActionImpl <em>Save Model Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.SaveModelActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSaveModelAction()
		 * @generated
		 */
		EClass SAVE_MODEL_ACTION = eINSTANCE.getSaveModelAction();

		/**
		 * The meta object literal for the '<em><b>Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAVE_MODEL_ACTION__MODEL_URI = eINSTANCE.getSaveModelAction_ModelURI();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAVE_MODEL_ACTION__KEY = eINSTANCE.getSaveModelAction_Key();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl <em>Execute Launcher Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExecuteLauncherAction()
		 * @generated
		 */
		EClass EXECUTE_LAUNCHER_ACTION = eINSTANCE.getExecuteLauncherAction();

		/**
		 * The meta object literal for the '<em><b>Launcher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTE_LAUNCHER_ACTION__LAUNCHER = eINSTANCE.getExecuteLauncherAction_Launcher();

		/**
		 * The meta object literal for the '<em><b>Max Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTE_LAUNCHER_ACTION__MAX_WAIT = eINSTANCE.getExecuteLauncherAction_MaxWait();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.EvaluateOCLQueryActionImpl <em>Evaluate OCL Query Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.EvaluateOCLQueryActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getEvaluateOCLQueryAction()
		 * @generated
		 */
		EClass EVALUATE_OCL_QUERY_ACTION = eINSTANCE.getEvaluateOCLQueryAction();

		/**
		 * The meta object literal for the '<em><b>Model Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATE_OCL_QUERY_ACTION__MODEL_KEY = eINSTANCE.getEvaluateOCLQueryAction_ModelKey();

		/**
		 * The meta object literal for the '<em><b>Query</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATE_OCL_QUERY_ACTION__QUERY = eINSTANCE.getEvaluateOCLQueryAction_Query();

		/**
		 * The meta object literal for the '<em><b>Result Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATE_OCL_QUERY_ACTION__RESULT_KEY = eINSTANCE.getEvaluateOCLQueryAction_ResultKey();

		/**
		 * The meta object literal for the '<em><b>Import Context Variables</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATE_OCL_QUERY_ACTION__IMPORT_CONTEXT_VARIABLES = eINSTANCE.getEvaluateOCLQueryAction_ImportContextVariables();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl <em>Copy Model Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCopyModelAction()
		 * @generated
		 */
		EClass COPY_MODEL_ACTION = eINSTANCE.getCopyModelAction();

		/**
		 * The meta object literal for the '<em><b>From URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_MODEL_ACTION__FROM_URI = eINSTANCE.getCopyModelAction_FromURI();

		/**
		 * The meta object literal for the '<em><b>To URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_MODEL_ACTION__TO_URI = eINSTANCE.getCopyModelAction_ToURI();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl <em>Compare Model Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getCompareModelAction()
		 * @generated
		 */
		EClass COMPARE_MODEL_ACTION = eINSTANCE.getCompareModelAction();

		/**
		 * The meta object literal for the '<em><b>Left URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARE_MODEL_ACTION__LEFT_URI = eINSTANCE.getCompareModelAction_LeftURI();

		/**
		 * The meta object literal for the '<em><b>Right URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARE_MODEL_ACTION__RIGHT_URI = eINSTANCE.getCompareModelAction_RightURI();

		/**
		 * The meta object literal for the '<em><b>Diff Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARE_MODEL_ACTION__DIFF_KEY = eINSTANCE.getCompareModelAction_DiffKey();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl <em>Assert Comparison Result Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAssertComparisonResultAction()
		 * @generated
		 */
		EClass ASSERT_COMPARISON_RESULT_ACTION = eINSTANCE.getAssertComparisonResultAction();

		/**
		 * The meta object literal for the '<em><b>Diff Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY = eINSTANCE.getAssertComparisonResultAction_DiffKey();

		/**
		 * The meta object literal for the '<em><b>Different Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT = eINSTANCE.getAssertComparisonResultAction_DifferentCount();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_COMPARISON_RESULT_ACTION__KIND = eINSTANCE.getAssertComparisonResultAction_Kind();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl <em>Assert OCL Invariant Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAssertOCLInvariantAction()
		 * @generated
		 */
		EClass ASSERT_OCL_INVARIANT_ACTION = eINSTANCE.getAssertOCLInvariantAction();

		/**
		 * The meta object literal for the '<em><b>Model Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY = eINSTANCE.getAssertOCLInvariantAction_ModelKey();

		/**
		 * The meta object literal for the '<em><b>Invariant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_OCL_INVARIANT_ACTION__INVARIANT = eINSTANCE.getAssertOCLInvariantAction_Invariant();

		/**
		 * The meta object literal for the '<em><b>Import Context Variables</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES = eINSTANCE.getAssertOCLInvariantAction_ImportContextVariables();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.UserActionImpl <em>User Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.UserActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getUserAction()
		 * @generated
		 */
		EClass USER_ACTION = eINSTANCE.getUserAction();

		/**
		 * The meta object literal for the '<em><b>Class File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_ACTION__CLASS_FILE_URI = eINSTANCE.getUserAction_ClassFileURI();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.SetVariableActionImpl <em>Set Variable Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.SetVariableActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getSetVariableAction()
		 * @generated
		 */
		EClass SET_VARIABLE_ACTION = eINSTANCE.getSetVariableAction();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SET_VARIABLE_ACTION__KEY = eINSTANCE.getSetVariableAction_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SET_VARIABLE_ACTION__VALUE = eINSTANCE.getSetVariableAction_Value();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl <em>Update Model Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getUpdateModelAction()
		 * @generated
		 */
		EClass UPDATE_MODEL_ACTION = eINSTANCE.getUpdateModelAction();

		/**
		 * The meta object literal for the '<em><b>Model Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UPDATE_MODEL_ACTION__MODEL_KEY = eINSTANCE.getUpdateModelAction_ModelKey();

		/**
		 * The meta object literal for the '<em><b>Update Operations</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UPDATE_MODEL_ACTION__UPDATE_OPERATIONS = eINSTANCE.getUpdateModelAction_UpdateOperations();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl <em>Wrapper Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getWrapperTestCase()
		 * @generated
		 */
		EClass WRAPPER_TEST_CASE = eINSTANCE.getWrapperTestCase();

		/**
		 * The meta object literal for the '<em><b>Before Test</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WRAPPER_TEST_CASE__BEFORE_TEST = eINSTANCE.getWrapperTestCase_BeforeTest();

		/**
		 * The meta object literal for the '<em><b>After Test</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WRAPPER_TEST_CASE__AFTER_TEST = eINSTANCE.getWrapperTestCase_AfterTest();

		/**
		 * The meta object literal for the '<em><b>Core Test</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WRAPPER_TEST_CASE__CORE_TEST = eINSTANCE.getWrapperTestCase_CoreTest();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WRAPPER_TEST_CASE__REPEAT = eINSTANCE.getWrapperTestCase_Repeat();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl <em>Auto Executor Test Suite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAutoExecutorTestSuite()
		 * @generated
		 */
		EClass AUTO_EXECUTOR_TEST_SUITE = eINSTANCE.getAutoExecutorTestSuite();

		/**
		 * The meta object literal for the '<em><b>Executors</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUTO_EXECUTOR_TEST_SUITE__EXECUTORS = eINSTANCE.getAutoExecutorTestSuite_Executors();

		/**
		 * The meta object literal for the '<em><b>Executables</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES = eINSTANCE.getAutoExecutorTestSuite_Executables();

		/**
		 * The meta object literal for the '<em><b>Executor Attribute Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME = eINSTANCE.getAutoExecutorTestSuite_ExecutorAttributeName();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.ClearActionImpl <em>Clear Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.ClearActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getClearAction()
		 * @generated
		 */
		EClass CLEAR_ACTION = eINSTANCE.getClearAction();

		/**
		 * The meta object literal for the '<em><b>Files</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_ACTION__FILES = eINSTANCE.getClearAction_Files();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl <em>External Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExternalAction()
		 * @generated
		 */
		EClass EXTERNAL_ACTION = eINSTANCE.getExternalAction();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_ACTION__NAME = eINSTANCE.getExternalAction_Name();

		/**
		 * The meta object literal for the '<em><b>Result Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_ACTION__RESULT_NAME = eINSTANCE.getExternalAction_ResultName();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTERNAL_ACTION__PARAMETERS = eINSTANCE.getExternalAction_Parameters();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_ACTION__ACTION = eINSTANCE.getExternalAction_Action();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionParameterImpl <em>External Action Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.impl.ExternalActionParameterImpl
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getExternalActionParameter()
		 * @generated
		 */
		EClass EXTERNAL_ACTION_PARAMETER = eINSTANCE.getExternalActionParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_ACTION_PARAMETER__NAME = eINSTANCE.getExternalActionParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_ACTION_PARAMETER__VALUE = eINSTANCE.getExternalActionParameter_Value();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mt4mt.DiffKind <em>Diff Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mt4mt.DiffKind
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getDiffKind()
		 * @generated
		 */
		EEnum DIFF_KIND = eINSTANCE.getDiffKind();

		/**
		 * The meta object literal for the '<em>IAction</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mt4mt.base.IAction
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getIAction()
		 * @generated
		 */
		EDataType IACTION = eINSTANCE.getIAction();

		/**
		 * The meta object literal for the '<em>Abstract Transformation Test Case</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getAbstractTransformationTestCase()
		 * @generated
		 */
		EDataType ABSTRACT_TRANSFORMATION_TEST_CASE = eINSTANCE.getAbstractTransformationTestCase();

		/**
		 * The meta object literal for the '<em>IExternal Action</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mt4mt.base.updates.IExternalAction
		 * @see edu.ustb.sei.mde.mt4mt.impl.MT4MTPackageImpl#getIExternalAction()
		 * @generated
		 */
		EDataType IEXTERNAL_ACTION = eINSTANCE.getIExternalAction();

	}

} //MT4MTPackage
