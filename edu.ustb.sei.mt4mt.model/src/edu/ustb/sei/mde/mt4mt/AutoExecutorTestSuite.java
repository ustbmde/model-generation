/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Auto Executor Test Suite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutors <em>Executors</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutables <em>Executables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutorAttributeName <em>Executor Attribute Name</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAutoExecutorTestSuite()
 * @model
 * @generated
 */
public interface AutoExecutorTestSuite extends TestSuite {
	/**
	 * Returns the value of the '<em><b>Executors</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executors</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executors</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAutoExecutorTestSuite_Executors()
	 * @model
	 * @generated
	 */
	EList<String> getExecutors();

	/**
	 * Returns the value of the '<em><b>Executables</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executables</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executables</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAutoExecutorTestSuite_Executables()
	 * @model
	 * @generated
	 */
	EList<String> getExecutables();

	/**
	 * Returns the value of the '<em><b>Executor Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executor Attribute Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executor Attribute Name</em>' attribute.
	 * @see #setExecutorAttributeName(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAutoExecutorTestSuite_ExecutorAttributeName()
	 * @model
	 * @generated
	 */
	String getExecutorAttributeName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite#getExecutorAttributeName <em>Executor Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executor Attribute Name</em>' attribute.
	 * @see #getExecutorAttributeName()
	 * @generated
	 */
	void setExecutorAttributeName(String value);

} // AutoExecutorTestSuite
