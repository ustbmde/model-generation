grammar GraphConfig;
import ValueSpec;

@header {
	import edu.ustb.sei.egraph.generator.value.*;
	import edu.ustb.sei.commonutil.util.Triple;
}

configRoot returns [GraphConfig root]:
('global' (global+=globalOption) (',' global+=globalOption)*)?
('node' nodes+=nodeConfig)*
('attribute' attributes+=attributeConfig[null])*
('edge' edges+=edgeConfig)*
{
	$root = new GraphConfig();
	
	for(int i=0;i<$global.size();i++) {
		GlobalOptionContext n = $global.get(i);
		$root.addGlobalOption(n.option);
	}
	
	for(int i=0;i<$nodes.size();i++) {
	  NodeConfigContext n = $nodes.get(i);
	  $root.getNodeConfigs().add(n.config);
	}
	for(int i=0;i<$attributes.size();i++) {
	  AttributeConfigContext n = $attributes.get(i);
	  $root.getAttributeConfigs().add(n.config);
	}
	for(int i=0;i<$edges.size();i++) {
	  EdgeConfigContext n = $edges.get(i);
	  $root.getEdgeConfigs().add(n.config);
	}
}
;

globalOption returns [Triple<String,String,Object> option]:
  'root' ':' root=('auto'|'manual'|ID)
  {
  	$option = new Triple<String,String,Object>("root","root",LexerUtil.normalizeID($root.text));
  }
| 'define' name=ID ':' spec = specification
{
	$option = new Triple<String,String,Object>("define",LexerUtil.normalizeID($name.text),$spec.function);
}
| 'save' name=('schema_location'|'compact'|'xmi_id') ':' value=BOOLEAN
{
	$option = new Triple<String,String,Object>("save",$name.text,Boolean.parseBoolean($value.text));
}
;

nodeConfig returns [NodeConfig config]:
	type=propertyName[QualifiedName.defaultType]
{
	$config = new NodeConfig($type.prop);
}
	( bound=specification   
		{
			$config.setBound((AbstractRandomFunction<Integer>)$bound.function);
		}
	 '{'
		(attributes+=attributeConfig[$type.text] (',' attributes+=attributeConfig[$type.text])*)?
	 '}'
	)? 
	('instance' (instances+=nodeSetInstance)+)?
{
	if($attributes!=null){
		for(int i=0;i<$attributes.size();i++) {
			AttributeConfigContext ac = $attributes.get(i);
			$config.addAttributeConfig(ac.config);
		}
	}
	if($instances!=null) {
		for(int i=0;i<$instances.size();i++) {
			NodeSetInstanceContext ns = $instances.get(i);
			$config.getInstanceSetConfig().add(ns.config);
		}
	}
}
;

nodeSetInstance returns [NodeInstanceSetConfig config]:
	name=ID nodes+=nodeInstance (',' nodes+=nodeInstance)*
	{
		String name = LexerUtil.normalizeID($name.text);
		$config = new NodeInstanceSetConfig(name);
		for(int i=0;i<$nodes.size();i++){
			NodeInstanceContext nc = $nodes.get(i);
			$config.getInstanceConfig().add(nc.config);
		}
	}
;

nodeInstance returns [NodeInstanceConfig config]:
	'{' (attributes+=nodeAttributeInstance (',' attributes+=nodeAttributeInstance)*)? '}'
{
	$config = new NodeInstanceConfig();
	for(int i=0;i<$attributes.size();i++) {
		NodeAttributeInstanceContext na = $attributes.get(i);
		$config.addPropertyFunction(na.attr);
	}
}
|	'external' uri=URI
{
	$config = new NodeInstanceConfig(LexerUtil.normalizeURI($uri.text));
}
;

nodeAttributeInstance returns [AttributeInstanceConfig attr]:
	name=ID '=' value=specification
	{
		$attr = new AttributeInstanceConfig(LexerUtil.normalizeID($name.text),$value.function);
	}
;

attributeConfig[String type] returns [AttributeConfig config]:
	name=propertyName[type]  '=' value=specification
{
	$config = new AttributeConfig($name.prop, $value.function);
}
;

edgeConfig returns [EdgeSetConfig config]:
{
	QualifiedName[] names = null;
	boolean idem=true; // no multiple edges
	boolean refl=true; // self pointer
	boolean ord=false; // non ordered
	boolean sourceUnique=false;
	boolean targetUnique=false;
	boolean sourceRequired=false;
	boolean targetRequired=false;
}
	name+=propertyName[null]+ 
	('(' 
		('idem' '=' idem=BOOLEAN {idem = LexerUtil.checkBoolean($idem.text);} 
			|'refl' '=' refl=BOOLEAN {refl = LexerUtil.checkBoolean($refl.text);}
			|'ord' '=' ord=BOOLEAN {ord = LexerUtil.checkBoolean($ord.text);}
			|'unique' '=' unique+=('source'|'target')+ {
				for(int i=0;i<$unique.size();i++) {
					Token t = $unique.get(i);
					if(t.getText().equals("source")) sourceUnique=true;
					else if(t.getText().equals("target")) targetUnique=true;
				}
			}
			|'required' '=' required+=('source'|'target')+ {
				for(int i=0;i<$required.size();i++) {
					Token t = $required.get(i);
					if(t.getText().equals("source")) sourceRequired=true;
					else if(t.getText().equals("target")) targetRequired=true;
				}
			}
		)+
	  ')'
	)?
	(generations+=edgeGenerationConfig)*
	('instance' (instances+=edgeInstance)+)?
{
	names = new QualifiedName[$name.size()];
	for(int i=0;i<names.length;i++) {
		names[i] = $name.get(i).prop;
	}
	$config = new EdgeSetConfig(names,idem,refl,ord,sourceUnique,sourceRequired,targetUnique,targetRequired);
	for(int i=0;i<$generations.size();i++) {
		EdgeGenerationConfigContext c = $generations.get(i);
		$config.getGenerationConfigs().add(c.config);
	}
	for(int i=0;i<$instances.size();i++) {
		EdgeInstanceContext c = $instances.get(i);
		if($config.addInstanceConfig(c.config)==false) {
			
		}
	}
}
;
edgeGenerationConfig returns [EdgeSetGenerationConfig config]:
{
	Filter s = null;
	Filter t = null;
}
	'{' 'bound' '=' bound=specification ('source' source=filter {s=$source.ftr;})? ('target' target=filter {t=$target.ftr;})?'}'
	{
		
		$config = new EdgeSetGenerationConfig((AbstractRandomFunction<Integer>)$bound.function,s, t);
	}
;
edgeInstance returns [EdgeInstanceConfig config]:
  source=ID ('[' sourceId=INT ']')? '-(' name=propertyName[null] ')->' target=ID ('[' targetId=INT ']')?
{
	String s = LexerUtil.normalizeID($source.text);
	String t = LexerUtil.normalizeID($target.text);
	int sourceId = -1;
	int targetId = -1;
	if($sourceId!=null)
	  sourceId = Integer.parseInt($sourceId.text);
	if($targetId!=null)
	  targetId = Integer.parseInt($targetId.text);
	  
	$config = new EdgeInstanceConfig($name.prop,s,sourceId,t,targetId);
}
;

propertyName[String tn] returns [QualifiedName prop]:
  name=ID (':' type=ID)?
  {
  	
  	String realType = tn!=null ? tn : $type.text;
  	$prop = new QualifiedName(LexerUtil.normalizeID($name.text),LexerUtil.normalizeID(realType));
  }
;

filter returns [Filter ftr]:
  (range+=ID)+
;


URI
:
	'<'(.)+?'>'
;

NL
:
    '\r'? '\n' -> skip
;
WS
:
    [ \t\f]+ -> skip
;
