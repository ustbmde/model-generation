package edu.ustb.sei.rmg.random.condition;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import edu.ustb.sei.rmg.random.RandomUtil;

abstract public class Condition <T>{
	public Double possibility = null;
	final static public String LAZY = "__LAZY_GENERATION__";
	public List<String> uniqueScope = null;
	
	protected Condition() {
		
	}
	
	public Object[] getGenerationBound() throws BoundSpecException{
		throw new BoundSpecException(this);
	}
	/**
	 * extra[0] = EObject
	 * extra[1] = EAttribute
	 * extra[2] = allowLazy
	 * extra[3] = [remainingSize,index]
	 * extra[4] = uniqueMap 
	 **/
	abstract public T getValue(RandomUtil r, Object... extra);
	
	static public CompositeIntegerFunction createCompositeIntegerFunction(DistributionFunction<?> df, RangeCondition<Integer> range) {
		CompositeIntegerFunction cif = new CompositeIntegerFunction();
		cif.function = df;
		cif.range = range;
		return cif;
	}
	
	static public CompositeLongFunction createCompositeLongFunction(DistributionFunction<?> df, RangeCondition<Long> range) {
		CompositeLongFunction cif = new CompositeLongFunction();
		cif.function = df;
		cif.range = range;
		return cif;
	}
	
	static public SeparateFunction createSeparateFunction(Condition<Integer> sum, Condition<Integer> count) {
		SeparateFunction sf = new SeparateFunction();
		sf.sumCondition = sum;
		sf.countCondition = count;
		return sf;
	}
	
	static public ListCondition createListCondition(Condition<?> e,  Condition<Integer> count) {
		ListCondition lc = new ListCondition();
		lc.element = e;
		lc.size = count;
		return lc;
	}
	
	static public RealDistributionFunction createRealDistributionFunction(String name, List<Double> list) {
		RealDistributionFunction df = new RealDistributionFunction();
		df.name = name;
		df.copyParamters(list);
		return df;
	}
	
	static public IntegerDistributionFunction createIntegerDistributionFunction(String name, List<Double> list) {
		IntegerDistributionFunction df = new IntegerDistributionFunction();
		df.name = name;
		df.copyParamters(list);
		return df;
	}
	
	static public LongDistributionFunction createLongDistributionFunction(String name, List<Double> list) {
		LongDistributionFunction df = new LongDistributionFunction();
		df.name = name;
		df.copyParamters(list);
		return df;
	}
	
	static public <U> Condition<U> createAtomicCondition(Condition<U> c){
		AtomicCondition<U> u = new AtomicCondition<U>();
		u.body = c;
		return u;
	}
	
	static public <U> ConditionList<U> createConditionList(Condition<U> one, ConditionList<U> two) {
		ConditionList<U> list = new ConditionList<U>();
		//list.list = new ArrayList<Condition<U>>();
		if(one!=null)
			list.list.add(one);
		if(two!=null)
			list.list.add(two);
		return list;
	}
	
	static public IntegerRangeCondition createIntegerRangeCondition(Integer lower, Integer upper) {
		IntegerRangeCondition c = new IntegerRangeCondition();
		c.lower = lower;
		c.upper = upper;
		return c;
	}
	
	static public LongRangeCondition createLongRangeCondition(Long lower, Long upper) {
		LongRangeCondition c = new LongRangeCondition();
		c.lower = lower;
		c.upper = upper;
		return c;
	}
	
	static public RealRangeCondition createRealRangeCondition(Double lower, Double upper) {
		RealRangeCondition c = new RealRangeCondition();
		c.lower = lower;
		c.upper = upper;
		return c;
	}
	
	static public CharRangeCondition createCharRangeCondition(Character lower, Character upper) {
		CharRangeCondition c = new CharRangeCondition();
		c.lower = lower;
		c.upper = upper;
		return c;
	}
	
	static public StringRangeCondition createStringRangeCondition(Condition<Character> charRange, Condition<Integer> sizeRange) {
		StringRangeCondition c  = new StringRangeCondition();
		c.charRange = charRange;
		c.sizeRange = sizeRange;
		return c;
	}
	
	static public StringConcatCondition createStringConcatCondition(Condition<String> first, Condition<String> second) {
		if(first instanceof StringConcatCondition) {
			StringConcatCondition c  = (StringConcatCondition)first;
			c.list.add(second);
			return c;
		} else {
			StringConcatCondition c  = new StringConcatCondition();
			c.list = new ArrayList<Condition<String>>();
			c.list.add(first);
			c.list.add(second);
			return c;
		}
	}
	
	static public <U> LiteralCondition<U> createLiteralCondition(U literal) {
		LiteralCondition<U> l = new LiteralCondition<U>();
		l.literal = literal;
		return l;
	}
	
	static public DateRangeCondition createDateRangeCondition(Calendar l, Calendar u) {
		DateRangeCondition r = new DateRangeCondition();
		r.lower = l;
		r.upper = u;
		return r;
	}
	
	static public String convertString(String str) {
		StringBuilder sb = new StringBuilder();
		boolean flag = false;
		for(int i=1;i<str.length()-1;i++){
			char ch = str.charAt(i);
			if(flag ||ch!='\\') {
				sb.append(ch);
				flag = false;
			} else {
				flag=true;
			}
		}
		return sb.toString();
	}
	
	static public Character convertChar(String charLiteral) {
		int i=1;
		char c = charLiteral.charAt(i);
		if(c=='\\')
			return charLiteral.charAt(i+1);
		else return c;
		
	}
	
	static public Calendar convertCalendar(String dateLiterl) {
		String buf[] = dateLiterl.split("-");
		int year = Integer.parseInt(buf[0]);
		int mon = Integer.parseInt(buf[1]);
		int day = Integer.parseInt(buf[2]);
		
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, mon-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		return c;
	}
	
	static public FeaturePathCondition<String> createStringFeatureCondition(List<String> list) {
		FeaturePathCondition<String> c = new FeaturePathCondition<String>(String.class);
		String[] path = new String[list.size()];
		list.toArray(path);
		c.path = path;
		return c;
	}
	
	static public <N> InevitableCondition<N> createInevitableCondition(Condition<N> body) {
		InevitableCondition<N> ic = new InevitableCondition<N>();
		ic.body = body;
		return ic;
	}
	
	static public UnequalCondition createUnequalCondition(Condition<?> core, List<String> list, Double p) {
		FeaturePathCondition<Object> c = new FeaturePathCondition<Object>(Object.class);
		String[] path = new String[list.size()];
		list.toArray(path);
		c.path = path;
		UnequalCondition uc = new UnequalCondition(core,c,p);
		return uc;
	}
}
