/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.AttributeOption;
import edu.ustb.sei.rmg.config.BasicFilter;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.CombinationOperator;
import edu.ustb.sei.rmg.config.CombinationalFilter;
import edu.ustb.sei.rmg.config.ComparisonOperator;
import edu.ustb.sei.rmg.config.Condition;
import edu.ustb.sei.rmg.config.ConfigFactory;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.Filter;
import edu.ustb.sei.rmg.config.GlobalOption;
import edu.ustb.sei.rmg.config.GlobalOptionEnum;
import edu.ustb.sei.rmg.config.OptionModel;
import edu.ustb.sei.rmg.config.ReferenceFilterOption;
import edu.ustb.sei.rmg.config.ReferenceGroupOption;
import edu.ustb.sei.rmg.config.ReferenceOption;

import edu.ustb.sei.rmg.config.ScalableCondition;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigPackageImpl extends EPackageImpl implements ConfigPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass optionModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceGroupOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractReferenceOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass globalOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalableConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass combinationalFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceFilterOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum globalOptionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum comparisonOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum combinationOperatorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ConfigPackageImpl() {
		super(eNS_URI, ConfigFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ConfigPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ConfigPackage init() {
		if (isInited) return (ConfigPackage)EPackage.Registry.INSTANCE.getEPackage(ConfigPackage.eNS_URI);

		// Obtain or create and register package
		ConfigPackageImpl theConfigPackage = (ConfigPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ConfigPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ConfigPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theConfigPackage.createPackageContents();

		// Initialize created meta-data
		theConfigPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theConfigPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ConfigPackage.eNS_URI, theConfigPackage);
		return theConfigPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOptionModel() {
		return optionModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_MetamodelURI() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_OutputFileURI() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_RootClass() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_AutoNumber() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_UniqueRoot() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_EnableConcurrency() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_GlobalOptions() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_ClsOptions() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_AttrOptions() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_RefOptions() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_FilterOptions() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOptionModel_BoundSolver() {
		return (EAttribute)optionModelEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOptionModel_Metamodel() {
		return (EReference)optionModelEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassOption() {
		return classOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassOption_ClassRef() {
		return (EReference)classOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassOption_AttributeOptions() {
		return (EReference)classOptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassOption_ReferenceOptions() {
		return (EReference)classOptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceOption() {
		return referenceOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceOption_ReferenceRef() {
		return (EReference)referenceOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceGroupOption() {
		return referenceGroupOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceGroupOption_ReferencesRef() {
		return (EReference)referenceGroupOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceGroupOption_TypeFilter() {
		return (EReference)referenceGroupOptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractReferenceOption() {
		return abstractReferenceOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_SourceUnique() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_SourceRequired() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_TargetUnique() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_TargetRequired() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_Transitivity() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_Ring() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractReferenceOption_Circle() {
		return (EAttribute)abstractReferenceOptionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractReferenceOption_ExternalObjects() {
		return (EReference)abstractReferenceOptionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeOption() {
		return attributeOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeOption_AttributeRef() {
		return (EReference)attributeOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobalOption() {
		return globalOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobalOption_Key() {
		return (EAttribute)globalOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCondition() {
		return conditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCondition_BoundCondition() {
		return (EAttribute)conditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalableCondition() {
		return scalableConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScalableCondition_Scalable() {
		return (EAttribute)scalableConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFilter() {
		return filterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicFilter() {
		return basicFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicFilter_Feature() {
		return (EReference)basicFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicFilter_ValueLiteral() {
		return (EAttribute)basicFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicFilter_Operator() {
		return (EAttribute)basicFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCombinationalFilter() {
		return combinationalFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCombinationalFilter_Left() {
		return (EReference)combinationalFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCombinationalFilter_Right() {
		return (EReference)combinationalFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCombinationalFilter_Operator() {
		return (EAttribute)combinationalFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceFilterOption() {
		return referenceFilterOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceFilterOption_SourceFilter() {
		return (EReference)referenceFilterOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceFilterOption_TargetFilter() {
		return (EReference)referenceFilterOptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceFilterOption_ReferenceRef() {
		return (EReference)referenceFilterOptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGlobalOptionEnum() {
		return globalOptionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getComparisonOperator() {
		return comparisonOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCombinationOperator() {
		return combinationOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigFactory getConfigFactory() {
		return (ConfigFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		optionModelEClass = createEClass(OPTION_MODEL);
		createEAttribute(optionModelEClass, OPTION_MODEL__METAMODEL_URI);
		createEAttribute(optionModelEClass, OPTION_MODEL__OUTPUT_FILE_URI);
		createEReference(optionModelEClass, OPTION_MODEL__ROOT_CLASS);
		createEAttribute(optionModelEClass, OPTION_MODEL__AUTO_NUMBER);
		createEAttribute(optionModelEClass, OPTION_MODEL__UNIQUE_ROOT);
		createEAttribute(optionModelEClass, OPTION_MODEL__ENABLE_CONCURRENCY);
		createEReference(optionModelEClass, OPTION_MODEL__GLOBAL_OPTIONS);
		createEReference(optionModelEClass, OPTION_MODEL__CLS_OPTIONS);
		createEReference(optionModelEClass, OPTION_MODEL__ATTR_OPTIONS);
		createEReference(optionModelEClass, OPTION_MODEL__REF_OPTIONS);
		createEReference(optionModelEClass, OPTION_MODEL__FILTER_OPTIONS);
		createEAttribute(optionModelEClass, OPTION_MODEL__BOUND_SOLVER);
		createEReference(optionModelEClass, OPTION_MODEL__METAMODEL);

		classOptionEClass = createEClass(CLASS_OPTION);
		createEReference(classOptionEClass, CLASS_OPTION__CLASS_REF);
		createEReference(classOptionEClass, CLASS_OPTION__ATTRIBUTE_OPTIONS);
		createEReference(classOptionEClass, CLASS_OPTION__REFERENCE_OPTIONS);

		referenceOptionEClass = createEClass(REFERENCE_OPTION);
		createEReference(referenceOptionEClass, REFERENCE_OPTION__REFERENCE_REF);

		referenceGroupOptionEClass = createEClass(REFERENCE_GROUP_OPTION);
		createEReference(referenceGroupOptionEClass, REFERENCE_GROUP_OPTION__REFERENCES_REF);
		createEReference(referenceGroupOptionEClass, REFERENCE_GROUP_OPTION__TYPE_FILTER);

		abstractReferenceOptionEClass = createEClass(ABSTRACT_REFERENCE_OPTION);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__TRANSITIVITY);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__RING);
		createEAttribute(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__CIRCLE);
		createEReference(abstractReferenceOptionEClass, ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS);

		attributeOptionEClass = createEClass(ATTRIBUTE_OPTION);
		createEReference(attributeOptionEClass, ATTRIBUTE_OPTION__ATTRIBUTE_REF);

		globalOptionEClass = createEClass(GLOBAL_OPTION);
		createEAttribute(globalOptionEClass, GLOBAL_OPTION__KEY);

		conditionEClass = createEClass(CONDITION);
		createEAttribute(conditionEClass, CONDITION__BOUND_CONDITION);

		scalableConditionEClass = createEClass(SCALABLE_CONDITION);
		createEAttribute(scalableConditionEClass, SCALABLE_CONDITION__SCALABLE);

		filterEClass = createEClass(FILTER);

		basicFilterEClass = createEClass(BASIC_FILTER);
		createEReference(basicFilterEClass, BASIC_FILTER__FEATURE);
		createEAttribute(basicFilterEClass, BASIC_FILTER__VALUE_LITERAL);
		createEAttribute(basicFilterEClass, BASIC_FILTER__OPERATOR);

		combinationalFilterEClass = createEClass(COMBINATIONAL_FILTER);
		createEReference(combinationalFilterEClass, COMBINATIONAL_FILTER__LEFT);
		createEReference(combinationalFilterEClass, COMBINATIONAL_FILTER__RIGHT);
		createEAttribute(combinationalFilterEClass, COMBINATIONAL_FILTER__OPERATOR);

		referenceFilterOptionEClass = createEClass(REFERENCE_FILTER_OPTION);
		createEReference(referenceFilterOptionEClass, REFERENCE_FILTER_OPTION__SOURCE_FILTER);
		createEReference(referenceFilterOptionEClass, REFERENCE_FILTER_OPTION__TARGET_FILTER);
		createEReference(referenceFilterOptionEClass, REFERENCE_FILTER_OPTION__REFERENCE_REF);

		// Create enums
		globalOptionEnumEEnum = createEEnum(GLOBAL_OPTION_ENUM);
		comparisonOperatorEEnum = createEEnum(COMPARISON_OPERATOR);
		combinationOperatorEEnum = createEEnum(COMBINATION_OPERATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		classOptionEClass.getESuperTypes().add(this.getScalableCondition());
		referenceOptionEClass.getESuperTypes().add(this.getAbstractReferenceOption());
		referenceGroupOptionEClass.getESuperTypes().add(this.getAbstractReferenceOption());
		abstractReferenceOptionEClass.getESuperTypes().add(this.getScalableCondition());
		attributeOptionEClass.getESuperTypes().add(this.getCondition());
		globalOptionEClass.getESuperTypes().add(this.getCondition());
		scalableConditionEClass.getESuperTypes().add(this.getCondition());
		basicFilterEClass.getESuperTypes().add(this.getFilter());
		combinationalFilterEClass.getESuperTypes().add(this.getFilter());

		// Initialize classes and features; add operations and parameters
		initEClass(optionModelEClass, OptionModel.class, "OptionModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOptionModel_MetamodelURI(), theEcorePackage.getEString(), "metamodelURI", null, 0, 1, OptionModel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptionModel_OutputFileURI(), theEcorePackage.getEString(), "outputFileURI", null, 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_RootClass(), theEcorePackage.getEClass(), null, "rootClass", null, 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptionModel_AutoNumber(), theEcorePackage.getEBoolean(), "autoNumber", "false", 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptionModel_UniqueRoot(), theEcorePackage.getEBoolean(), "uniqueRoot", "true", 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptionModel_EnableConcurrency(), theEcorePackage.getEBoolean(), "enableConcurrency", null, 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_GlobalOptions(), this.getGlobalOption(), null, "globalOptions", null, 0, -1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_ClsOptions(), this.getClassOption(), null, "clsOptions", null, 0, -1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_AttrOptions(), this.getAttributeOption(), null, "attrOptions", null, 0, -1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_RefOptions(), this.getAbstractReferenceOption(), null, "refOptions", null, 0, -1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_FilterOptions(), this.getReferenceFilterOption(), null, "filterOptions", null, 0, -1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOptionModel_BoundSolver(), theEcorePackage.getEBoolean(), "boundSolver", null, 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOptionModel_Metamodel(), theEcorePackage.getEPackage(), null, "metamodel", null, 0, 1, OptionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(optionModelEClass, theEcorePackage.getEBoolean(), "isConstrained", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEReference(), "ref", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(classOptionEClass, ClassOption.class, "ClassOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClassOption_ClassRef(), theEcorePackage.getEClass(), null, "classRef", null, 0, 1, ClassOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassOption_AttributeOptions(), this.getAttributeOption(), null, "attributeOptions", null, 0, -1, ClassOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassOption_ReferenceOptions(), this.getReferenceGroupOption(), null, "referenceOptions", null, 0, -1, ClassOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceOptionEClass, ReferenceOption.class, "ReferenceOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceOption_ReferenceRef(), theEcorePackage.getEReference(), null, "referenceRef", null, 0, 1, ReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceGroupOptionEClass, ReferenceGroupOption.class, "ReferenceGroupOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceGroupOption_ReferencesRef(), theEcorePackage.getEReference(), null, "referencesRef", null, 0, -1, ReferenceGroupOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferenceGroupOption_TypeFilter(), theEcorePackage.getEClass(), null, "typeFilter", null, 0, -1, ReferenceGroupOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractReferenceOptionEClass, AbstractReferenceOption.class, "AbstractReferenceOption", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractReferenceOption_SourceUnique(), theEcorePackage.getEBoolean(), "sourceUnique", "false", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_SourceRequired(), theEcorePackage.getEBoolean(), "sourceRequired", "false", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_TargetUnique(), theEcorePackage.getEBoolean(), "targetUnique", "false", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_TargetRequired(), theEcorePackage.getEBoolean(), "targetRequired", "false", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_Transitivity(), theEcorePackage.getEBoolean(), "transitivity", "false", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_Ring(), theEcorePackage.getEBoolean(), "ring", "true", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractReferenceOption_Circle(), theEcorePackage.getEBoolean(), "circle", "true", 0, 1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractReferenceOption_ExternalObjects(), theEcorePackage.getEObject(), null, "externalObjects", null, 0, -1, AbstractReferenceOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeOptionEClass, AttributeOption.class, "AttributeOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeOption_AttributeRef(), theEcorePackage.getEAttribute(), null, "attributeRef", null, 0, 1, AttributeOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(globalOptionEClass, GlobalOption.class, "GlobalOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGlobalOption_Key(), this.getGlobalOptionEnum(), "key", null, 0, 1, GlobalOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionEClass, Condition.class, "Condition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCondition_BoundCondition(), theEcorePackage.getEString(), "boundCondition", null, 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scalableConditionEClass, ScalableCondition.class, "ScalableCondition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScalableCondition_Scalable(), theEcorePackage.getEBoolean(), "scalable", "true", 0, 1, ScalableCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(filterEClass, Filter.class, "Filter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(filterEClass, theEcorePackage.getEBoolean(), "filter", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "obj", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(filterEClass, theEcorePackage.getEObject(), "filter", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "list", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(basicFilterEClass, BasicFilter.class, "BasicFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicFilter_Feature(), theEcorePackage.getEAttribute(), null, "feature", null, 0, 1, BasicFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicFilter_ValueLiteral(), theEcorePackage.getEString(), "valueLiteral", null, 0, 1, BasicFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicFilter_Operator(), this.getComparisonOperator(), "operator", null, 0, 1, BasicFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(combinationalFilterEClass, CombinationalFilter.class, "CombinationalFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCombinationalFilter_Left(), this.getFilter(), null, "left", null, 0, 1, CombinationalFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCombinationalFilter_Right(), this.getFilter(), null, "right", null, 0, 1, CombinationalFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCombinationalFilter_Operator(), this.getCombinationOperator(), "operator", null, 0, 1, CombinationalFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceFilterOptionEClass, ReferenceFilterOption.class, "ReferenceFilterOption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceFilterOption_SourceFilter(), this.getFilter(), null, "sourceFilter", null, 0, 1, ReferenceFilterOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferenceFilterOption_TargetFilter(), this.getFilter(), null, "targetFilter", null, 0, 1, ReferenceFilterOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReferenceFilterOption_ReferenceRef(), theEcorePackage.getEReference(), null, "referenceRef", null, 0, 1, ReferenceFilterOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(globalOptionEnumEEnum, GlobalOptionEnum.class, "GlobalOptionEnum");
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.GLOBAL_OBJECT_BOUND);
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.DEFAULT_OBJECT_BOUND);
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.DEFAULT_LINK_BOUND);
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.DEFAULT_STRING_CONDITION);
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.DEFAULT_INTEGER_RANGE);
		addEEnumLiteral(globalOptionEnumEEnum, GlobalOptionEnum.SIZE_FACTOR);

		initEEnum(comparisonOperatorEEnum, ComparisonOperator.class, "ComparisonOperator");
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.EQ);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.LEQ);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GT);
		addEEnumLiteral(comparisonOperatorEEnum, ComparisonOperator.GEQ);

		initEEnum(combinationOperatorEEnum, CombinationOperator.class, "CombinationOperator");
		addEEnumLiteral(combinationOperatorEEnum, CombinationOperator.AND);
		addEEnumLiteral(combinationOperatorEEnum, CombinationOperator.OR);
		addEEnumLiteral(combinationOperatorEEnum, CombinationOperator.NOT);

		// Create resource
		createResource(eNS_URI);
	}

} //ConfigPackageImpl
