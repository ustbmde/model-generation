/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.InputParameterConfiguration;
import edu.pku.ustb.rt4mt.Rt4mtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Parameter Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.InputParameterConfigurationImpl#getRmgModelURI <em>Rmg Model URI</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InputParameterConfigurationImpl extends ParameterConfigurationImpl implements InputParameterConfiguration {
	/**
	 * The default value of the '{@link #getRmgModelURI() <em>Rmg Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRmgModelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String RMG_MODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRmgModelURI() <em>Rmg Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRmgModelURI()
	 * @generated
	 * @ordered
	 */
	protected String rmgModelURI = RMG_MODEL_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InputParameterConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Rt4mtPackage.Literals.INPUT_PARAMETER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRmgModelURI() {
		return rmgModelURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRmgModelURI(String newRmgModelURI) {
		String oldRmgModelURI = rmgModelURI;
		rmgModelURI = newRmgModelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI, oldRmgModelURI, rmgModelURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI:
				return getRmgModelURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI:
				setRmgModelURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI:
				setRmgModelURI(RMG_MODEL_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI:
				return RMG_MODEL_URI_EDEFAULT == null ? rmgModelURI != null : !RMG_MODEL_URI_EDEFAULT.equals(rmgModelURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (rmgModelURI: ");
		result.append(rmgModelURI);
		result.append(')');
		return result.toString();
	}

} //InputParameterConfigurationImpl
