/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgSourceView
 * erConfiguration. This class is only generated to avoid compile errors with
 * existing versions of this class.
 */
@Deprecated
public class StructuralrmgEditorConfiguration {
	
}
