package edu.ustb.sei.mt4mt.base.actions;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

final public class EmptyAction implements IAction {
	
	final static public EmptyAction SINGLETON = new EmptyAction();

	@Override
	public void run(Context context) {}

}
