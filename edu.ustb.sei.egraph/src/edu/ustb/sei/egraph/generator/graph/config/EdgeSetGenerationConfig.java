package edu.ustb.sei.egraph.generator.graph.config;

import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class EdgeSetGenerationConfig {
	public EdgeSetGenerationConfig(AbstractRandomFunction<Integer> bound, Filter source, Filter target) {
		super();
		this.bound = bound;
		this.source = source;
		this.target = target;
	}
	
	private AbstractRandomFunction<Integer> bound;
	private Filter source;
	private Filter target;
	
	public AbstractRandomFunction<Integer> getBound() {
		return bound;
	}
	public Filter getSource() {
		return source;
	}
	public Filter getTarget() {
		return target;
	}
}
