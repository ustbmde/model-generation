package edu.ustb.sei.egraph.generator.tree.config;

import java.util.BitSet;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import edu.ustb.sei.egraph.generator.tree.config.TreeConfigLexer;
import edu.ustb.sei.egraph.generator.tree.config.TreeConfigParser;
import edu.ustb.sei.egraph.generator.tree.config.TreeConfigParser.ProductionContext;

public class TreeConfigUtil implements ANTLRErrorListener{
	final static public TreeConfigUtil SINGLETON = new TreeConfigUtil();
	
	private TreeConfigParser parser;
	
	public TreeConfigParser getParser() {
		if(parser==null) {
			parser = new TreeConfigParser(null);
			parser.addErrorListener(this);
		}
		return parser;
	}
	
	public ProductionContext parse(String code) {
		ANTLRInputStream s = new ANTLRInputStream(code);
		TreeConfigLexer lexer = new TreeConfigLexer(s);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		TreeConfigParser p = this.getParser();
		p.setTokenStream(tokens);
		p.reset();
		return p.production();
	}

	@Override
	public void reportAmbiguity(Parser arg0, DFA arg1, int arg2, int arg3, boolean arg4, BitSet arg5,
			ATNConfigSet arg6) {
	}

	@Override
	public void reportAttemptingFullContext(Parser arg0, DFA arg1, int arg2, int arg3, BitSet arg4, ATNConfigSet arg5) {
	}

	@Override
	public void reportContextSensitivity(Parser arg0, DFA arg1, int arg2, int arg3, int arg4, ATNConfigSet arg5) {
	}

	@Override
	public void syntaxError(Recognizer<?, ?> arg0, Object arg1, int arg2, int arg3, String arg4,
			RecognitionException arg5) {
		throw new RuntimeException(arg5);
	}
}
