package edu.ustb.sei.egraph.bridge;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

public class CustomizedResourceSet extends ResourceSetImpl {

	public CustomizedResourceSet() {
		this.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	}
	
	@Override
	public VirtualUIRConverter getURIConverter() {
		if (uriConverter == null)
	    {
	      uriConverter = new VirtualUIRConverter();
	    }
	    return (VirtualUIRConverter)uriConverter;
	}
	
	public void setDefaultPath(String str) {
		this.getURIConverter().setDefaultPath(str);
	}
	
	public void registerScheme(String scheme, String redirect) {
		this.getURIConverter().registerScheme(scheme, redirect);
	}
}

class VirtualUIRConverter extends ExtensibleURIConverterImpl {
	private String defaultPath;
	private Map<String,String> schemeMap = new HashMap<String, String>();
	
	@Override
	public URI normalize(URI uri) {
	    String scheme = uri.scheme();
	    
	    if(scheme==null) {
	    	if(!uri.hasAbsolutePath() && this.defaultPath!=null) {
	    		uri = URI.createURI(defaultPath+uri.toString());
	    	}
	    } else {
	    	String prefix = this.schemeMap.get(scheme);
	    	if(prefix!=null) {
	    		uri = URI.createURI(prefix + uri.toString().substring(scheme.length()+2));
	    	}
	    }
	    


	    return super.normalize(uri);
	}

	public String getDefaultPath() {
		return defaultPath;
	}

	public void setDefaultPath(String defaultPath) {
		if(defaultPath.charAt(defaultPath.length()-1)!='/')
			defaultPath = defaultPath + "/";
		this.defaultPath = defaultPath;
	}

	public void registerScheme(String scheme, String redirect) {
		if(redirect.charAt(defaultPath.length()-1)!='/')
			redirect = redirect + "/";
		this.schemeMap.put(scheme, redirect);
	}
}

class CustomizedXMIResourceFactory extends XMIResourceFactoryImpl {
	@Override
	public Resource createResource(URI uri) {
		return new CustomizedXMIResource(uri);
	}
}

class CustomizedXMIResource extends XMIResourceImpl {
	
	
	public CustomizedXMIResource() {
		super();
	}
	public CustomizedXMIResource(URI uri) {
		super(uri);
	}
	private boolean useUUID = false;
	@Override
	protected boolean useUUIDs() {
		return useUUID;
	}
	public void setUseUUID(boolean useUUID) {
		this.useUUID = useUUID;
	}
}
