package edu.ustb.sei.egraph.generator.graph.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;

/**
 * This class is used to maintain the edge constraints
 * @author hexiao
 *
 */
public class ReachabilityService<NT,ET> {
	public ReachabilityService(EGraph<NT,ET,?,?> hostGraph, List<ET> references, boolean isIdempotent, boolean isSelfReflective,
			boolean isOrdered) {
		super();
		this.hostGraph = hostGraph;
		this.references = references;
		this.isIdempotent = isIdempotent;
		this.isSelfReflective = isSelfReflective;
		this.isOrdered = isOrdered;
	}

	private boolean isSelfReflective;
	private boolean isOrdered;
	private boolean isIdempotent; // true -> only one edge between s and t is allowed
	
	private List<ET> references;
	@SuppressWarnings("rawtypes")
	private EGraph hostGraph;
	
	private PathFindingAlgorithm<NT,ET> defaultPathFinding = null;
	
	public PathFindingAlgorithm<NT,ET> getPathFindingAlgorithm() {
		if(defaultPathFinding==null) {
			defaultPathFinding = this.sourceToTargetFinding;
		}
		return defaultPathFinding;
	}
	
	public void setPathFindingAlgorithm(PathFindingAlgorithmType type ) {
		if(type==PathFindingAlgorithmType.srcToTar)
			this.defaultPathFinding = sourceToTargetFinding;
		else if(type==PathFindingAlgorithmType.tarToSrc)
			this.defaultPathFinding = targetToSourceFinding;
	}
	
	private PathFindingAlgorithm<NT,ET> sourceToTargetFinding = (src,tar,visited)->{
		visited.add(src);
		
		ConcurrentHashMap<ENode<NT, ET, ?>, Boolean> reachableStatus = getReachableStatusFromSource(src);
		
		Boolean reachable = reachableStatus.get(tar);
		
		if(reachable!=null) return reachable;
		
		for(ET r : this.references) {
			@SuppressWarnings("unchecked")
			Collection<EEdge<NT,ET,?>> links = this.hostGraph.getOutgoings(src, r);
			
			links.parallelStream().forEach((l)->{
				ENode<NT,ET,?> t = (ENode<NT, ET, ?>) l.getTarget();
				
				if(visited.contains(t)) 
					return; // we don't visit a node multiple times
				
				if(t==tar) { // directly reachable
					reachableStatus.put(tar, true);
					return;
				}
				
				if(isOrdered) {
					if(this.sourceToTargetFinding.findPath(t, tar, visited)) {
						reachableStatus.put(tar, true);
						return;
					}
				}
			});
			
			if(reachableStatus.get(tar)==Boolean.TRUE) {
				return true;
			}
		}
		
		return false;
	};
	
	private PathFindingAlgorithm<NT,ET> targetToSourceFinding = (src,tar,visited)->{
		visited.add(tar);
		
		ConcurrentHashMap<ENode<NT, ET, ?>, Boolean> reachableStatus = getReachableStatusFromSource(src);
		
		Boolean reachable = reachableStatus.get(tar);
		
		if(reachable!=null) return reachable;
		
		for(ET r : this.references) {
			@SuppressWarnings("unchecked")
			Collection<EEdge<NT,ET,?>> links = this.hostGraph.getIncomings(tar, r);
			
			links.parallelStream().forEach((l)->{
				ENode<NT,ET,?> s = (ENode<NT, ET, ?>) l.getSource();
				
				if(visited.contains(s)) 
					return; // we don't visit a node multiple times
				
				if(s==src) { // directly reachable
					reachableStatus.put(tar, true);
					return;
				}
				
				if(isOrdered) {
					if(this.targetToSourceFinding.findPath(src, s, visited)) {
						reachableStatus.put(tar, true);
						return;
					}
				}
			});
			
			if(reachableStatus.get(tar)==true) {
				return true;
			}
		}
		
		return false;
	};
	
	
	private CriticalPathCollectingAlgorithm<NT,ET> defaultCriticalPathCollectingAlgorithm = null;
	public CriticalPathCollectingAlgorithm<NT,ET> getDefaultCriticalPathCollectingAlgorithm() {
		if(defaultCriticalPathCollectingAlgorithm==null)
			defaultCriticalPathCollectingAlgorithm = this.srcToTarCriticalPathFinding;
		return defaultCriticalPathCollectingAlgorithm;
	}
	public void setCriticalPathFindingAlgorithm(CriticalPathCollectingAlgorithmType type) {
		if(type==CriticalPathCollectingAlgorithmType.srcToTar)
			this.defaultCriticalPathCollectingAlgorithm = this.srcToTarCriticalPathFinding;
		else if(type==CriticalPathCollectingAlgorithmType.tarToSrc)
			this.defaultCriticalPathCollectingAlgorithm = this.tarToSrcCriticalPathFinding;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private CriticalPathCollectingAlgorithm<NT, ET> srcToTarCriticalPathFinding = (s,t,info) -> {
		// check whether critical belongs to every path from s to t
				List<EEdge<NT,ET,?>> critials = info.get(s, t);
				if(critials==null) {
					critials = new ArrayList<EEdge<NT,ET,?>>();
					if(this.isReachable(s, t)) {
						ETypeModel typeModel = this.hostGraph.getTypeModel();
						
						List<EEdge<NT,ET,?>> edgesFromS2T = new ArrayList<EEdge<NT,ET,?>>();
						this.references.forEach((r)->{
							if(typeModel.isSuperNodeType(s.getType(), typeModel.getSource(r))) {
								List<EEdge<NT,ET,?>> edges = (List<EEdge<NT, ET, ?>>) this.hostGraph.getOutgoings(s, r);
								
								edges.forEach(
										(e)->{
											ENode<NT, ET, ?> targetOfE = e.getTarget();
											if(targetOfE!=s && (targetOfE==t || this.isReachable(targetOfE, t))) {
												edgesFromS2T.add(e);
											}
										});
							}
						});
						
						if(edgesFromS2T.size()==0) {
							// should not be here, because we have checked the reachability first
						} else {
							if(edgesFromS2T.size()==1) {
								critials.add(edgesFromS2T.get(0));
							}
							
							for(EEdge<NT,ET,?> e : edgesFromS2T) {
								critials.addAll(this.srcToTarCriticalPathFinding.collectPaths(e.getTarget(),t,info));
							};
						}
					}
					info.put(s, t, critials);
				}
				return critials;
	};
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private CriticalPathCollectingAlgorithm<NT, ET> tarToSrcCriticalPathFinding = (s,t,info) -> {
		// check whether critical belongs to every path from s to t
				List<EEdge<NT,ET,?>> critials = info.get(s, t);
				if(critials==null) {
					critials = new ArrayList<EEdge<NT,ET,?>>();
					if(this.isReachable(s, t)) {
						ETypeModel typeModel = this.hostGraph.getTypeModel();
						
						List<EEdge<NT,ET,?>> edgesFromS2T = new ArrayList<EEdge<NT,ET,?>>();
						this.references.forEach((r)->{
							if(typeModel.isSuperNodeType(t.getType(), typeModel.getTarget(r))) {
								List<EEdge<NT,ET,?>> edges = (List<EEdge<NT, ET, ?>>) this.hostGraph.getIncomings(t, r);
								
								edges.forEach(
										(e)->{
											ENode<NT, ET, ?> sourceOfE = e.getSource();
											if(sourceOfE!=t && (sourceOfE==s || this.isReachable(s, sourceOfE))) {
												edgesFromS2T.add(e);
											}
										});
							}
						});
						
						if(edgesFromS2T.size()==0) {
							// should not be here, because we have checked the reachability first
						} else {
							if(edgesFromS2T.size()==1) {
								critials.add(edgesFromS2T.get(0));
							}
							
							for(EEdge<NT,ET,?> e : edgesFromS2T) {
								critials.addAll(this.srcToTarCriticalPathFinding.collectPaths(s,e.getSource(),info));
							};
						}
					}
					info.put(s, t, critials);
				}
				return critials;
	};
	
	
	private ConcurrentHashMap<ENode<NT,ET,?>,ConcurrentHashMap<ENode<NT,ET,?>,Boolean>> reachabilityCache = new ConcurrentHashMap<ENode<NT,ET,?>,ConcurrentHashMap<ENode<NT,ET,?>,Boolean>>();
	
	public boolean canCreateEdgeBetween(ENode<NT,ET,?> src, ENode<NT,ET,?> tar) {
		if(src==tar && !isSelfReflective) 
			return false;
		else {
			if(isIdempotent) {
				// check Idempotent
				ConcurrentHashMap<ENode<NT, ET, ?>, Boolean> reachableStatus = getReachableStatusFromSource(src);
				for(ET ref : references) {
					@SuppressWarnings("unchecked")
					Collection<EEdge<NT,ET,?>> links = hostGraph.getOutgoings(src, ref);
					for(EEdge<NT,ET,?> l : links) {
						reachableStatus.put((ENode<NT, ET, ?>) l.getTarget(), true);
						if(l.getTarget()==tar)
							return false;
					}
				}
			}
			
			if(isOrdered) {
				// check if there is a path from tar to src
				return isReachable(tar, src)==false;
			}
		}
		return true;
	}
	
	
	
	public boolean isReachable(ENode<NT,ET,?> src, ENode<NT,ET,?> tar) {
		ConcurrentHashMap<ENode<NT, ET, ?>, Boolean> reachableStatus = getReachableStatusFromSource(src);
		
		Boolean reachable = reachableStatus.get(tar);			
		if(reachable!=null) { // we have an answer
			return reachable;
		} else {
			// we haven't computed
			HashSet<ENode<NT,ET,?>> visited = new HashSet<ENode<NT,ET,?>>();
			reachable = findPath(src,tar,visited);
			if(reachable==false) { // false status is not cached
				reachableStatus.put(tar, false);
			}
			return reachable;
		}
	}
	
	/**
	 * Find a path from src to tar. If such a path exists, cache the reachability
	 * @param src
	 * @param tar
	 * @param visited
	 * @return
	 */
	private boolean findPath(ENode<NT,ET,?> src, ENode<NT,ET,?> tar, Set<ENode<NT,ET,?>> visited) {
		return this.getPathFindingAlgorithm().findPath(src, tar, visited);
	}



	private ConcurrentHashMap<ENode<NT, ET, ?>, Boolean> getReachableStatusFromSource(ENode<NT, ET, ?> src) {
		ConcurrentHashMap<ENode<NT,ET,?>, Boolean> reachableNodes = this.reachabilityCache.get(src);
		
		if(reachableNodes==null) {
			reachableNodes = new ConcurrentHashMap<ENode<NT,ET,?>, Boolean>();
			this.reachabilityCache.put(src, reachableNodes);
		}
		return reachableNodes;
	}
	
	
	/**
	 * if a new edge is created 
	 * @param node
	 */
	public void reset() {
		this.reachabilityCache.clear();
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<ENode<NT,ET,?>> collectNonForbiddenObjects(ENode<NT,ET,?> src, List<ENode<NT,ET,?>> tar, NT typeFilter) {
		return new ConditionalIterator(src, tar, true, typeFilter, this, this.hostGraph.getTypeModel());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<ENode<NT,ET,?>> collectNonForbiddenObjects(ENode<NT,ET,?> src, Iterator<ENode<NT,ET,?>> tar, NT typeFilter) {
		return new ConditionalIterator(src, tar, true, typeFilter, this, this.hostGraph.getTypeModel());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<ENode<NT,ET,?>> collectNonForbiddenObjects(List<ENode<NT,ET,?>> src, ENode<NT,ET,?> tar, NT typeFilter) {
		return new ConditionalIterator(tar, src, false, typeFilter, this, this.hostGraph.getTypeModel());
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<ENode<NT,ET,?>> collectNonForbiddenObjects(Iterator<ENode<NT,ET,?>> src, ENode<NT,ET,?> tar, NT typeFilter) {
		return new ConditionalIterator(tar, src, false, typeFilter, this, this.hostGraph.getTypeModel());
	}



	public void setIdempotent(boolean b) {
		this.isIdempotent = b;
	}

	public boolean isIdempotent() {
		return this.isIdempotent;
	}


	public void setSelfReflective(boolean b) {
		this.isSelfReflective = b;
	}

	public boolean isSelfReflective() {
		return this.isSelfReflective;
	}

	public void setOrdered(boolean b) {
		this.isOrdered = b;
	}
	
	public boolean isOrdered() {
		return this.isOrdered;
	}
	
	
	// can only be called when isOrdered=true
	public List<EEdge<NT,ET,?>> collectCriticalEdgeBetween(ENode<NT,ET,?> s,ENode<NT,ET,?> t, edu.ustb.sei.commonutil.util.PairHashMap<ENode<NT,ET,?>, ENode<NT,ET,?>, List<EEdge<NT,ET,?>>> info) {
		return this.getDefaultCriticalPathCollectingAlgorithm().collectPaths(s, t, info);
	}
	
	public Iterator<ENode<NT,ET,?>> getReachableIterator(ENode<NT,ET,?> seed, NT type,boolean isForward) {
		return new ReachableNodeIterator(seed,type,isForward,this);
	}
	
	class ReachableNodeIterator implements Iterator<ENode<NT,ET,?>> {
		
		public ReachableNodeIterator(ENode<NT, ET, ?> seed, NT type, boolean isForward,
				ReachabilityService<NT, ET> reachability) {
			super();
			this.isForward = isForward;
			this.reachability = reachability;
			this.visited = new HashSet<ENode<NT,ET,?>>();
			this.curNode = null;
			this.candidate = new ArrayList<ENode<NT,ET,?>>();
			this.candidate.add(seed);
			this.type = type;
		}
		
		private NT type;
		private Set<ENode<NT,ET,?>> visited;
		private List<ENode<NT,ET,?>> candidate;
		private boolean isForward;
		private ReachabilityService<NT, ET> reachability;
		private ENode<NT,ET,?> curNode;
		

		@SuppressWarnings("unchecked")
		@Override
		public boolean hasNext() {
			if(curNode==null) {
				while(!candidate.isEmpty() && curNode==null) {
					ENode<NT, ET, ?> c = candidate.get(candidate.size()-1);
					candidate.remove(candidate.size()-1);
					if(visited.contains(c)==false) {
						visited.add(c);
						// if c instanceof type, curNode = c
						// put c reachable to candidate
						if(this.reachability.hostGraph.getTypeModel().isSuperNodeType(c.getType(), type)) {
							curNode = c;
						}
						if(isForward) {
							this.reachability.references.forEach(
									(r)->{
										List<EEdge<NT,ET,?>> edges = (List<EEdge<NT, ET, ?>>) this.reachability.hostGraph.getOutgoings(c, r);
										edges.forEach((e)->{
											candidate.add(e.getTarget());
										});										
									});
						} else {
							this.reachability.references.forEach(
									(r)->{
										List<EEdge<NT,ET,?>> edges = (List<EEdge<NT, ET, ?>>) this.reachability.hostGraph.getIncomings(c, r);
										edges.forEach((e)->{
											candidate.add(e.getTarget());
										});										
									});
						}
					}
				}
				return curNode!=null;
			} else
				return true;
		}

		@Override
		public ENode<NT, ET, ?> next() {
			ENode<NT,ET,?> x = curNode;
			curNode = null;
			return x;
		}
		
	}
}

class ConditionalIterator<NT,ET> implements Iterator<ENode<NT,ET,?>> {
	public ConditionalIterator(ENode<NT, ET, ?> anchorNode, List<ENode<NT, ET, ?>> candidates, boolean isSourceAnchor,
			NT typeFilter, ReachabilityService<NT, ET> reachability, ETypeModel<NT,ET,?,?> typeModel) {
		super();
		this.anchorNode = anchorNode;
		this.isSourceAnchor = isSourceAnchor;
		this.typeFilter = typeFilter;
		this.reachability = reachability;
		this.typeModel = typeModel;
		this.internalItr = candidates.iterator();
		this.curNode = null;
	}
	
	public ConditionalIterator(ENode<NT, ET, ?> anchorNode, Iterator<ENode<NT, ET, ?>> candidates, boolean isSourceAnchor,
			NT typeFilter, ReachabilityService<NT, ET> reachability, ETypeModel<NT,ET,?,?> typeModel) {
		super();
		this.anchorNode = anchorNode;
		this.isSourceAnchor = isSourceAnchor;
		this.typeFilter = typeFilter;
		this.reachability = reachability;
		this.typeModel = typeModel;
		this.internalItr = candidates;
		this.curNode = null;
	}

	private NT typeFilter;
	private ETypeModel<NT,ET,?,?> typeModel;
	private ReachabilityService<NT,ET> reachability;
	private ENode<NT,ET,?> anchorNode;
	private boolean isSourceAnchor;
	private Iterator<ENode<NT,ET,?>> internalItr;
	private ENode<NT,ET,?> curNode;

	@Override
	public boolean hasNext() {
		if(curNode==null) {
			while(this.internalItr.hasNext()) {
				curNode = this.internalItr.next();
				
				if(this.typeFilter!=null) {
					NT type = curNode.getType();
					if(!this.typeModel.isSuperNodeType(type, this.typeFilter)) 
						continue;
				}
				
				if(this.isSourceAnchor) {
					if(this.reachability.canCreateEdgeBetween(anchorNode, curNode)) {
						return true;
					}
				} else {
					if(this.reachability.canCreateEdgeBetween(curNode, anchorNode)) {
						return true;
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}

	@Override
	public ENode<NT, ET, ?> next() {
		ENode<NT,ET,?> x = curNode;
		curNode = null;
		return x;
	}

}



