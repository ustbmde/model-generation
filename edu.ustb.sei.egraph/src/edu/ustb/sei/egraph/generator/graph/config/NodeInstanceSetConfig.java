package edu.ustb.sei.egraph.generator.graph.config;

import java.util.ArrayList;
import java.util.List;

public class NodeInstanceSetConfig {
	public NodeInstanceSetConfig(String name) {
		super();
		this.name = name;
	}
	private String name;
	private List<NodeInstanceConfig> instanceConfig = new ArrayList<NodeInstanceConfig>();
	public String getName() {
		return name;
	}
	public List<NodeInstanceConfig> getInstanceConfig() {
		return instanceConfig;
	}

}
