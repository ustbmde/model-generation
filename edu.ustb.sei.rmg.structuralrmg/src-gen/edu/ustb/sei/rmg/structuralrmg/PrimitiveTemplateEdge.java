/**
 */
package edu.ustb.sei.rmg.structuralrmg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Template Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSourcePos <em>Source Pos</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTargetPos <em>Target Pos</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateEdge()
 * @model
 * @generated
 */
public interface PrimitiveTemplateEdge extends TemplateEdge {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(TemplateNode)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateEdge_Source()
	 * @model required="true"
	 * @generated
	 */
	TemplateNode getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(TemplateNode value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(TemplateNode)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateEdge_Target()
	 * @model required="true"
	 * @generated
	 */
	TemplateNode getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(TemplateNode value);

	/**
	 * Returns the value of the '<em><b>Source Pos</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Pos</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Pos</em>' attribute.
	 * @see #setSourcePos(Integer)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateEdge_SourcePos()
	 * @model
	 * @generated
	 */
	Integer getSourcePos();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getSourcePos <em>Source Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Pos</em>' attribute.
	 * @see #getSourcePos()
	 * @generated
	 */
	void setSourcePos(Integer value);

	/**
	 * Returns the value of the '<em><b>Target Pos</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Pos</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Pos</em>' attribute.
	 * @see #setTargetPos(Integer)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateEdge_TargetPos()
	 * @model
	 * @generated
	 */
	Integer getTargetPos();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge#getTargetPos <em>Target Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Pos</em>' attribute.
	 * @see #getTargetPos()
	 * @generated
	 */
	void setTargetPos(Integer value);

} // PrimitiveTemplateEdge
