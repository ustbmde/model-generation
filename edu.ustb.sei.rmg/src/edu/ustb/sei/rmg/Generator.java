package edu.ustb.sei.rmg;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

import javax.management.RuntimeErrorException;
import javax.xml.crypto.Data;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.ui.console.MessageConsole;

import choco.Options;
import edu.ustb.sei.rmg.config.*;
import edu.ustb.sei.rmg.proxy.EListProxy;
import edu.ustb.sei.rmg.random.RandomUtil;
import edu.ustb.sei.rmg.random.condition.FeatureConditionError;
import edu.ustb.sei.rmg.random.condition.UniqueValueScopeGenerationRequest;
import edu.ustb.sei.rmg.random.condition.ValueLazyGenerationRequest;
import edu.ustb.sei.rmg.strategy.IStringGenerationStrategy;
import edu.ustb.sei.rmg.util.ConsoleFactory;
import edu.ustb.sei.rmg.util.HashableBasicEList;
import edu.ustb.sei.rmg.util.HashableHashMap;
import edu.ustb.sei.rmg.util.KeyManager;
import edu.ustb.sei.rmg.util.OrderedPairMap;
import edu.ustb.sei.rmg.util.OrderedTripleMap;
import edu.ustb.sei.rmg.util.Pair;
import edu.ustb.sei.rmg.util.PerformanceMonitor;
import edu.ustb.sei.rmg.util.Quad;
import edu.ustb.sei.rmg.util.Triple;

public class Generator {
	
	public static boolean enableConcurrency = false;
	public static final String GLOBAL_OBJECT_BOUND = "global_object_bound";
	public static final String DEFAULT_OBJECT_BOUND = "default_object_bound";
	public static final String DEFAULT_LINK_BOUND = "default_link_bound";
	public static final String DEFAULT_STRING_CHARACTER = "{'_',['a'..'z'],['A'..'Z'],['0'..'9']}";
	public static final String STRING_ATTRIBUTE_CONDITION = "default_string_condition";
	public static final String INTEGER_ATTRIBUTE_RANGE = "default_integer_range";
	final static public int MAX_CLASS_OBJECTS = 2000;
	final static public int MAX_REFERENCE_LINKS = 1000;
	final static public int MAX_STRING_SIZE = 20;
	private static final Object IS_UNIQUE_ROOT = "is_unique_root";
	
	private ResourceSet resourceSet;
	
	//private Random random ;
	
	protected RandomUtil random;
	
	protected Map<Object, Object> generationConstraint = new HashMap<Object, Object>();
	public Map<Object, Object> getGenerationConstraint() {
		return generationConstraint;
	}

	private Map<EReference, Filter[]> referenceFilter = new HashMap<EReference, Filter[]>();
	
	private boolean autoNumber = false;
	
	public boolean isAutoNumber() {
		return autoNumber;
	}

	public void setAutoNumber(boolean autoNumber) {
		this.autoNumber = autoNumber;
	}

	protected Map<EClass, EList<EObject>> allObjMap = new HashableHashMap<EClass, EList<EObject>>();
	protected Map<EClass, EList<EObject>> objMap = new HashableHashMap<EClass, EList<EObject>>();
	protected EList<EObject> allObjs = new HashableBasicEList<EObject>();
	protected EList<Triple<EObject, EAttribute, RuntimeException>> lazyGenerations = new HashableBasicEList<Triple<EObject, EAttribute, RuntimeException>>();
	protected KeyManager keyManager = new KeyManager();
	
	public Generator(ResourceSet resSet) {
		resourceSet = resSet;
		init();
		resourceSet.getPackageRegistry().put(ConfigPackage.eNS_URI, ConfigPackage.eINSTANCE);
	}
	
	public Generator(){
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		resourceSet.setPackageRegistry(EPackage.Registry.INSTANCE);
		init();
		resourceSet.getPackageRegistry().put(ConfigPackage.eNS_URI, ConfigPackage.eINSTANCE);
	}
	
	protected void init() {
		random = new RandomUtil();
		generationConstraint.put(DEFAULT_OBJECT_BOUND, "[0.."+MAX_CLASS_OBJECTS+"]");
		generationConstraint.put(DEFAULT_LINK_BOUND, "[0.."+MAX_REFERENCE_LINKS+"]");
		generationConstraint.put(STRING_ATTRIBUTE_CONDITION, DEFAULT_STRING_CHARACTER+"@"+"[1.."+MAX_STRING_SIZE+"]");
	}
	
	
	


	private EPackage metamodel;
	
	private EList<EObject> modelroots = new UniqueEList<EObject>();
	protected EClass rootClass;
	
	protected EList<EClass> allClasses = new HashableBasicEList<EClass>();
	protected EList<EReference> allContainmentReferences = new HashableBasicEList<EReference>();
	protected EList<Pair<EClass,EList<EReference>>> specifiedReferenceGroups = new HashableBasicEList<Pair<EClass,EList<EReference>>>();
	protected EList<EReference> otherReferences = new HashableBasicEList<EReference>();
	protected OrderedPairMap<EClass,EClass,Boolean> hasCommonSubClass = new OrderedPairMap<EClass,EClass,Boolean>();
	protected Map<EClass, List<EClass>> concreteSubClassMap = new HashMap<EClass, List<EClass>>();
	
	public EPackage getMetamodel(){
		return metamodel;
	}
	public boolean loadMetamodel(EPackage pkg) {
//		URI mmuri;
//		try {
//			File metamodelFile = new File(path);
//			if(metamodelFile.exists()) 
//				mmuri = URI.createFileURI(metamodelFile.getAbsolutePath()); // e.g. "c:\.."
//			else 
//				mmuri = URI.createPlatformResourceURI(path, true);
//		} catch (Exception e) {
//			mmuri = URI.createURI(path);
//		}
//		
//		
//		Resource res = resourceSet.getResource(mmuri, true);
//		metamodel = ((EPackage)res.getContents().get(0));
		
		
		EcoreUtil.resolveAll(resourceSet);
		
//		Resource res = resourceSet.getResources().get(1);
		metamodel = pkg;
		
		EPackage.Registry.INSTANCE.put(metamodel.getNsURI(), metamodel);
		
		iterateMetaModel(metamodel, allClasses, allContainmentReferences, otherReferences,hasCommonSubClass, concreteSubClassMap);
		
		return true;
	}
	
	public OptionModel loadConfig(String path) {
		URI mmuri;
		
		try {
			File metamodelFile = new File(path);
			if(metamodelFile.exists()) 
				mmuri = URI.createFileURI(metamodelFile.getAbsolutePath()); // e.g. "c:\.."
			else 
				mmuri = URI.createPlatformResourceURI(path, true);
		} catch (Exception e) {
			mmuri = URI.createURI(path);
		}
		
		Resource res = resourceSet.getResource(mmuri, true);
		
		OptionModel config = ((OptionModel)res.getContents().get(0));
		return config;
	}
	
	
	private OptionModel optionModel = null;
	
	public void doConfig(OptionModel root) {
		optionModel = root;
		
		setAutoNumber(root.isAutoNumber());
		setRootClass(root.getRootClass());
		setUniqueRoot(root.isUniqueRoot());
				
		for(Object v : root.getGlobalOptions()) {
			GlobalOption o = (GlobalOption)v;
			generationConstraint.put(o.getKey().getLiteral(), o.getBoundCondition());
			if(o.getKey()==GlobalOptionEnum.SIZE_FACTOR) {
				random.setFactor(random.randomInt(o.getBoundCondition()));
			}
		}
		
		
		for(Object v : root.getClsOptions()) {
			ClassOption o = (ClassOption)v;
			generationConstraint.put(o.getClassRef(), o.getBoundCondition());
			for(AttributeOption a : o.getAttributeOptions()) {
				generationConstraint.put(keyManager.getKey(o.getClassRef(),a.getAttributeRef()),a.getBoundCondition());
			}
			
			//to be implemented
			//note that if two lists contain the same elements in the same order, they are equal to each other
			//that is to say, generationConstraint cannot differ them
			for(ReferenceGroupOption r : o.getReferenceOptions()) {
				otherReferences.removeAll(r.getReferencesRef());
				Pair<EClass,EList<EReference>> key = new Pair<EClass,EList<EReference>>(o.getClassRef(),r.getReferencesRef());
				specifiedReferenceGroups.add(key);
				generationConstraint.put(key, 
						new Object[]{r.getBoundCondition(), r.isSourceUnique(), r.isSourceRequired(), r.isTargetUnique(), r.isTargetRequired(), r.isTransitivity(), r.isRing(), r.isCircle()});
			}
		}
		
		for(Object v :root.getAttrOptions()) {
			AttributeOption o = (AttributeOption)v;
			//generationConstraint.put(keyManager.getKey(o.getAttributeRef().getEContainingClass(),o.getAttributeRef()), o.getBoundCondition());
			generationConstraint.put(o.getAttributeRef(), o.getBoundCondition());
		}
		
		for(Object v : root.getRefOptions()) {
			AbstractReferenceOption o = (AbstractReferenceOption)v;
			if(o.eClass() == ConfigPackage.eINSTANCE.getReferenceOption()) {
				ReferenceOption c = (ReferenceOption)o;
				generationConstraint.put(c.getReferenceRef(), 
						new Object[]{c.getBoundCondition(), c.isSourceUnique(), c.isSourceRequired(), c.isTargetUnique(), c.isTargetRequired(), c.isTransitivity(), c.isRing(), c.isCircle()});
				checkAndReplace(c.getReferenceRef());
				
			} else if(o.eClass() == ConfigPackage.eINSTANCE.getReferenceGroupOption()) {
				ReferenceGroupOption c = (ReferenceGroupOption)o;
				EList<EReference> refs = c.getReferencesRef();
				otherReferences.removeAll(refs);
				Pair<EClass, EList<EReference>> key = new Pair<EClass,EList<EReference>>(null,refs);
				specifiedReferenceGroups.add(key);
				generationConstraint.put(key, 
						new Object[]{c.getBoundCondition(), c.isSourceUnique(), c.isSourceRequired(), c.isTargetUnique(), c.isTargetRequired(), c.isTransitivity(), c.isRing(), c.isCircle()});
			}
		}
		
		for(Object v : root.getFilterOptions()) {
			ReferenceFilterOption o = (ReferenceFilterOption)v;
			Filter[] filters = new Filter[2];
			filters[0] = o.getSourceFilter();
			filters[1] = o.getTargetFilter();
			referenceFilter.put(o.getReferenceRef(), filters);
		}
	}
	
	protected boolean checkAndReplace(EReference ref) {
		if(ref.getEOpposite()==null) return true;
		if(ref.isContainment()) return true;
		if(ref.isContainer()) return false;
		
		EReference oRef = ref.getEOpposite();
		int id = this.otherReferences.indexOf(oRef);
		
		if(id==-1) return true;
		this.otherReferences.set(id, ref);
		return true;
	}
	
	public EClass findClass(String name) {
		return (EClass) Generator.getClassByName(metamodel, name);
	}
	
	public EAttribute findAttribute(String cName, String aName) {
		EClass c = (EClass)Generator.getClassByName(metamodel, cName);
		if(c==null) return null;
		for(EAttribute a : c.getEAllAttributes()) {
			if(a.getName().equals(aName))
				return a;
		}
		return null;
	}
	
	public EReference findReference(String cName, String rName) {
		EClass c = (EClass)Generator.getClassByName(metamodel, cName);
		if(c==null) return null;
		for(EReference r : c.getEAllReferences()) {
			if(r.getName().equals(rName))
				return r;
		}
		return null;
	}
	
	public EList<EObject> getModelRoot(){
		return modelroots;
	}
	static private String lastSuffix = "";
	static private int lastCount = 1;
	
	public String saveModel(String path){
		if(autoNumber) {
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH)+1;
			int day = c.get(Calendar.DAY_OF_MONTH);
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int min = c.get(Calendar.MINUTE);
			int sec = c.get(Calendar.SECOND);
			String str = "-"+year+"-"+month+"-"+day+"-"+hour+"-"+min+"-"+sec;
			
			if(lastSuffix.equals(str)) {
				str = str + "-"+lastCount;
				lastCount++;
			} else {
				lastSuffix = str;
				lastCount = 1;
			}
			
			int id = path.lastIndexOf(".");
			if(id==-1) {
				path = path+str+".xmi";
			} else {
				path = path.substring(0, id)+str+path.substring(id);
			}
		}
		
		URI mmuri;
		try {
			
			File metamodelFile = new File(path);
			if(metamodelFile.exists()) 
				mmuri = URI.createFileURI(metamodelFile.getAbsolutePath()); // e.g. "c:\.."
			else 
				mmuri = URI.createPlatformResourceURI(path, true);
		} catch (Exception e) {
			mmuri = URI.createURI(path);
		}
		
		HashMap<Object, Object> options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_SCHEMA_LOCATION, true);
		options.put(XMLResource.OPTION_FORMATTED, false);
		options.put(XMLResource.OPTION_ENCODING, "UTF-8");
		
		Resource resource = resourceSet.createResource(mmuri);
		int amount = 0;
//		if(modelroots.size()!=0) 
//			resource.getContents().addAll(modelroots);
//		else 
		{
			for(EObject o : allObjs) {
				if(o.eContainer()==null)
					resource.getContents().add(o);
			}
			
			amount = checkElementAmount(resource);
			ConsoleFactory.printToConsole("amounts: "+amount,"RMG",true);
		}

		ConsoleFactory.printToConsole("generat ok to save","RMG",true);
		
		System.out.println("actual saving now");
		
		try {
			resource.save(options);
			//if(amount>=100000) {
			if(true) {
				ConsoleFactory.printToConsole("The output file has been ejected due to its large size","RMG",true);
				resourceSet.getResources().remove(resource);
			}
			
		} catch (Exception e) {
			System.out.println("when saving "+mmuri);
			e.printStackTrace();
			return null;
		}
		
		System.out.println("finish saving now");
		
		return path;
	}
	
	public int checkElementAmount(Resource resource) {
		TreeIterator<EObject> it = resource.getAllContents();
		int amount = 0;
		
		while(it.hasNext()) {
			it.next();
			amount++;
		}
		return amount;
	}
	
	public void setRootClass(EClass c){
		rootClass = c;
	}
	
	public void setRootClass(String str) {
		setRootClass((EClass)Generator.getClassByName(getMetamodel(), str));
	}
	
	static public EClassifier getClassByName(EPackage pkg, String name){
		EClassifier c = pkg.getEClassifier(name);
		if(c!=null)
			return c;
		else {
			for(EPackage p : pkg.getESubpackages()) {
				c = getClassByName(p,name);
				if(c!=null)
					return c;
			}
		}
		return null;
	}
	
	static protected void iterateMetaModel(EPackage pkg, EList<EClass> allClasses, EList<EReference> allContainmentReferences, EList<EReference> allNormalReferences, OrderedPairMap<EClass,EClass,Boolean> commonSubClass, Map<EClass,List<EClass>> concreteSubClassMap) {
		for(EClassifier c : pkg.getEClassifiers()) {
			if(c instanceof EClass) {
				allClasses.add((EClass) c);
				
				registerSubClass((EClass)c, (EClass)c, concreteSubClassMap);
				
				EList<EClass> superClasses = ((EClass) c).getEAllSuperTypes();
				
				for(int id1=0;id1<=superClasses.size();id1++) {
					for(int id2=0;id2<=superClasses.size();id2++) {
						EClass c1 = (id1==superClasses.size()) ? (EClass)c : superClasses.get(id1);
						EClass c2 = (id2==superClasses.size()) ? (EClass)c : superClasses.get(id2);
						commonSubClass.put(c1, c2, true);
						commonSubClass.put(c2, c1, true);
					}
				}
				
				for(EReference r : ((EClass) c).getEReferences()) {
					if(r.isChangeable()==false) continue;
					
					if(r.isContainment())
						allContainmentReferences.add(r);
					else if(r.isContainer())
						continue;
					else {
						//handle special cases
						
						//handle normal cases
						if(r.getEOpposite()==null 
								|| allNormalReferences.contains(r.getEOpposite())==false)
							allNormalReferences.add(r);
					}
				}
			}
		}
		
		for(EPackage p : pkg.getESubpackages()) {
			iterateMetaModel(p,allClasses,allContainmentReferences,allNormalReferences, commonSubClass, concreteSubClassMap);
		}
	}
	
	protected static void registerSubClass(EClass c, EClass sub, Map<EClass, List<EClass>> concreteSubClassMap) {		
		List<EClass> subclass = concreteSubClassMap.get(c);
		if(subclass==null) {
			subclass = new UniqueEList<EClass>();
			concreteSubClassMap.put(c, subclass);
		}
		if(sub.isAbstract()==false)
			subclass.add(sub);
		
		for(EClass cc : c.getESuperTypes()) {
			registerSubClass(cc, sub, concreteSubClassMap);
		}
	}

	protected PerformanceMonitor monitor = new PerformanceMonitor();
	
	public void randomGenerateModel() {
		time = 0L;
		
		monitor.begin("GEN ELEM");
		randomGenerateElements();
		monitor.end("GEN ELEM");
		pickRootObject();
		monitor.begin("GEN REL");
		randomGenerateReferences();
		monitor.end("GEN REL");
		generateLazyAttributes();
		
//		writeBackShadowList();
		System.out.println("time: "+time);
		
		monitor.print("INNER", "GEN ELEM", "GEN REL");
	}

	protected void generateLazyAttributes() {
		ConsoleFactory.printToConsole("Begin Lazy Generation " + lazyGenerations.size(),"RMG",true);
		for(Triple<EObject,EAttribute,RuntimeException> r : lazyGenerations) {
			if(r.third instanceof ValueLazyGenerationRequest) {
				try {
					Object value = ((ValueLazyGenerationRequest)r.third).getRequester().getValue(random, r.first,r.second,false);
					
					r.first.eSet(r.second, convertToPrimitiveValue(r.second.getEAttributeType(), value));
				} catch (FeatureConditionError e) {
					e.printStackTrace();
				}
			} else if(r.third instanceof UniqueValueScopeGenerationRequest) {
				edu.ustb.sei.rmg.random.condition.Condition<?> cd = ((UniqueValueScopeGenerationRequest)(r.third)).getRequester();
				Object feature = null;
				
				try {
					EObject obj = r.first;
					for(int i=0;i<cd.uniqueScope.size()-1;i++){
						String featureName = cd.uniqueScope.get(i);
						Object nObj = obj.eGet(obj.eClass().getEStructuralFeature(featureName));
						if(nObj instanceof EObject) 
							obj = (EObject) nObj;
						else 
							throw new Exception("feature breaked");
					}
					feature = obj.eGet(obj.eClass().getEStructuralFeature(cd.uniqueScope.get(cd.uniqueScope.size()-1)));
				} catch(Exception e) {
					feature = null;
				}
				
				UniqueValueGenerator ug = this.getUniqueMap(feature, r.first.eClass(), r.second);
				Object value = ug.getValue(r.first, r.second, ((UniqueValueScopeGenerationRequest)r.third).getConditionString(), ((UniqueValueScopeGenerationRequest)r.third).getPair(),this);
				r.first.eSet(r.second, convertToPrimitiveValue(r.second.getEAttributeType(), value));
			}
		}
	}

	static public Object convertToPrimitiveValue(EDataType type, Object value) {
		if(type==EcorePackage.eINSTANCE.getEInt() || type==EcorePackage.eINSTANCE.getEIntegerObject()) {
			if(value instanceof Integer) return value;
			else if(value instanceof Double) return (int)((Double) value).doubleValue();
			else if(value instanceof String) return Integer.parseInt(value.toString());
			else if(value instanceof EEnumLiteral) return ((EEnumLiteral) value).getValue();
			else if(value instanceof Boolean) return value==Boolean.TRUE ? 1 : 0;
			else return value.hashCode();
		}
		else if(type==EcorePackage.eINSTANCE.getEDouble()||type==EcorePackage.eINSTANCE.getEDoubleObject()) {
			if(value instanceof Integer) return ((Integer)value).doubleValue();
			else if(value instanceof Double) return value;
			else if(value instanceof String) return Double.parseDouble(value.toString());
			else if(value instanceof EEnumLiteral) return (double)((EEnumLiteral) value).getValue();
			else if(value instanceof Boolean) return value==Boolean.TRUE ? 1.0 : 0.0;
			else return (double)value.hashCode();
		}
		else if(type==EcorePackage.eINSTANCE.getEBoolean()||type==EcorePackage.eINSTANCE.getEBooleanObject()) {
			if(value instanceof Integer) return (Integer)value==1;
			else if(value instanceof Double) return (Double)value==1;
			else if(value instanceof String) return Boolean.parseBoolean(value.toString());
			else if(value instanceof Boolean) return value;
			else return null;
		}
		else if(type==EcorePackage.eINSTANCE.getEFloat()||type==EcorePackage.eINSTANCE.getEFloatObject()) {
			if(value instanceof Integer) return ((Integer)value).floatValue();
			else if(value instanceof Float) return value;
			else if(value instanceof Double) return ((Double)value).floatValue();
			else if(value instanceof String) return Float.parseFloat(value.toString());
			return null;
		}
		else if(type==EcorePackage.eINSTANCE.getELong()||type==EcorePackage.eINSTANCE.getELongObject()) {
			if(value instanceof Integer) return value;
			else if(value instanceof Long) return ((Long) value).intValue();
			else if(value instanceof Double) return (long)((Double) value).doubleValue();
			else if(value instanceof String) return Long.parseLong(value.toString());
			else if(value instanceof EEnumLiteral) return (long)((EEnumLiteral) value).getValue();
			else if(value instanceof Boolean) return value==Boolean.TRUE ? 1l : 0l;
			else return value.hashCode();
		}
		else if(type==EcorePackage.eINSTANCE.getEChar() || type==EcorePackage.eINSTANCE.getECharacterObject()) {
			if(value instanceof Character) return value;
			return (char)value.hashCode();
		}
		else if(type==EcorePackage.eINSTANCE.getEByte() || type==EcorePackage.eINSTANCE.getEByteObject()) {
			if(value instanceof Byte) return value;
			return (byte)value.hashCode();
		}
		else if(type==EcorePackage.eINSTANCE.getEDate()) {
			if(value instanceof Data) return value;
			else return null;
		}
		else if(type==EcorePackage.eINSTANCE.getEString()) {
			if(value==null) return "";
			else return value.toString();
		}
		else if(type instanceof EEnum) {
			if(value instanceof Integer)
				return ((EEnum)type).getEEnumLiteral((Integer)value);
			else if(value instanceof String)
				return ((EEnum)type).getEEnumLiteral((String)value);
			else if(value instanceof EEnumLiteral) return value;
			else return null;
			
		} else {
			ConsoleFactory.printToConsole("unknown type: "+type.getName(),"RMG",true);
			return null;
		}
	}

	protected void randomGenerateReferences() {
		randomGenerateContainmentReferences();
		randomGenerateReferenceGroups();
		randomGenerateOtherReferences();
	}
	
	protected void randomGenerateReferenceGroups() {
		for(Pair<EClass,EList<EReference>> refs : specifiedReferenceGroups) {
			
			EList<EObject> parents = new UniqueEList<EObject>();
			EList<EObject> children = null;
			
			ReferenceGroupOption ro = this.getReferenceGroupOption(refs);
			
			//prepare source
			if(ro.eContainer() instanceof ClassOption) {
				ClassOption opt = (ClassOption) ro.eContainer();
				parents.addAll(allObjMap.get(opt.getClassRef()));
			} else {
				for(EReference ref :refs.second) {
					parents.addAll(allObjMap.get(ref.getEContainingClass()));
				}
			}
			
			boolean isExternal = (ro!=null && ro.getExternalObjects().size()!=0);
			if(isExternal) {
				children = this.filterByRefs(ro.getExternalObjects(),refs.second.toArray(new EReference[refs.second.size()]));
			} else {
				children = new UniqueEList<EObject>();
				for(EReference ref :refs.second) {
//					EList<EObject> p = allObjMap.get(ref.getEContainingClass());
//					if(p!=null) 
//						parents.addAll(p);
//					
//					if(isExternal==false) {
						EList<EObject> c = allObjMap.get(ref.getEReferenceType());
						if(c!=null)
							children.addAll(c);
//					}
				}
			}
			
			
			if(parents.size()==0 || children.size()==0) return;
			
			Object[] boundInfo = (Object[]) generationConstraint.get(refs);
			String bound = (String)boundInfo[0];
			boolean sourceUnique = (boolean)boundInfo[1];
			boolean sourceRequired = (boolean)boundInfo[2];
			boolean targetUnique = (boolean)boundInfo[3];
			boolean targetRequired = (boolean)boundInfo[4];
			boolean transitivity = (boolean)boundInfo[5];
			boolean ring = (boolean)boundInfo[6];
			boolean circle = (boolean)boundInfo[7];
			
			children = this.filterByTypes(children,ro.getTypeFilter().toArray(new EClass[ro.getTypeFilter().size()]));
			
			
//			ReferenceGroupOption ro = this.getReferenceGroupOption(refs);
			int size = (ro==null || ro.isScalable()) ? random.randomIntWithScale(bound) : random.randomInt(bound);
			
			
			
			ForbiddenMap map = new ForbiddenMap(ring, circle, transitivity);
			randomGenerateReferences(refs.second, parents, children, sourceRequired, sourceUnique, targetRequired, targetUnique, map, size);
		}
	}



	protected void randomGenerateElements(){
		for(EClass c : allClasses) {
			randomGenerateElements(c);
		}
	}
	
	
	protected Map<EClass,EList<EReference>> containerMap = new HashableHashMap<EClass, EList<EReference>>();
	
	protected EList<EReference> getContainerReferenceForClass(EClass t) {
		EList<EReference> ref = containerMap.get(t);
		if(ref==null) {
			ref = new HashableBasicEList<EReference>();
			containerMap.put(t, ref);
			for(EReference r : allContainmentReferences) {
				if(r.getEReferenceType().isSuperTypeOf(t)) 
					ref.add(r);
			}
		}
		return ref;
	}
	

	
	protected boolean uniqueRoot = true;
	
	public boolean isUniqueRoot() {
		return uniqueRoot;
	}

	public void setUniqueRoot(boolean uniqueRoot) {
		this.uniqueRoot = uniqueRoot;
	}

	protected void randomGenerateElements(EClass t){
		int containerSize = getContainerReferenceForClass(t).size();
		if(t.isAbstract() || (containerSize == 0 && t!=rootClass && rootClass!=null)) {
			
			EList<EObject> objs = allObjMap.get(t);
			if(objs!=null) return;
			
			objs = new HashableBasicEList<EObject>();
			allObjMap.put(t, objs);
			return;
		}
		
		int size = 0;
		
		
		if(containerSize == 0 && (t==rootClass && uniqueRoot))
			size = 1;
		else size = randomObjectSize(t);
		
		ConsoleFactory.printToConsole("to generate "+t+" "+size,"RMG",true);
		
		EList<EObject> objs = new HashableBasicEList<EObject>(size);
		int[] pair = new int[]{size,0};
		for(int i = 0;i<size;i++){
			EObject o = EcoreUtil.create(t);
			objs.add(o);
			randomGenerateAttributes(o,pair);
			pair[0]--;
		}
		
		
		objMap.put(t, objs);
		allObjs.addAll(objs);
		addObjToAllObjMap(t, objs);
		addObjToParent(t, objs);
	}
	
	protected void addObjToAllObjMap(EClass t, EList<EObject> objs) {
		EList<EObject> cObjs = allObjMap.get(t);
		if(cObjs==null) {
			cObjs = new HashableBasicEList<EObject>();
			allObjMap.put(t, cObjs);
		}
		cObjs.addAll(objs);
	}
	
	public void pickRootObject(){
		EList<EObject> cObjs = null;
		int size = 0;
		
		if(rootClass!=null) {
			cObjs = allObjMap.get(rootClass);
		} else {
			cObjs = allObjs;
		}
		
		if(uniqueRoot) {
			size = 1;
		} else {
			size = random.randomUIntU(cObjs.size())+1;
		}
		
		
		while(modelroots.size()<size) {
			int id = random.randomUIntU(cObjs.size());
			modelroots.add(cObjs.get(id));
		}
		
	}
	
	protected ClassOption getClassOption(EClass t) {
		for(ClassOption co : this.optionModel.getClsOptions()) {
			if(co.getClassRef() ==t ) return co;
		}
		return null;
	}
	
	protected ReferenceOption getReferenceOption(EReference t) {
		for(AbstractReferenceOption ro : this.optionModel.getRefOptions()) {
			if(ro instanceof ReferenceOption) {
				if(((ReferenceOption) ro).getReferenceRef()==t)
					return (ReferenceOption)ro;
			}
		}
		return null;
	}
	
	protected ReferenceGroupOption getReferenceGroupOption(Pair<EClass,EList<EReference>> refs) {
		if(refs.first==null) {
			for(AbstractReferenceOption ro : this.optionModel.getRefOptions()) {
				if(ro instanceof ReferenceGroupOption) {
					if(((ReferenceGroupOption) ro).getReferencesRef().equals(refs.second))
						return (ReferenceGroupOption)ro;
				}
			}
		} else {
			ClassOption co = this.getClassOption(refs.first);
			if(co!=null) {
				for(AbstractReferenceOption ro : co.getReferenceOptions()) {
					if(ro instanceof ReferenceGroupOption) {
						if(((ReferenceGroupOption) ro).getReferencesRef().equals(refs.second))
							return (ReferenceGroupOption)ro;
					}
				}
			}
		}
		return null;
	}
	
	protected int randomObjectSize(EClass t) {
		String bound = null;
		
		bound = (String)generationConstraint.get(t);
		if(bound==null)
			bound = (String)generationConstraint.get(DEFAULT_OBJECT_BOUND);
		
		ClassOption co = this.getClassOption(t);
		
		int size = (co==null || co.isScalable()) ? random.randomIntWithScale(bound) : random.randomInt(bound);
		return size;
	}
	
	private int randomLinkSize(EReference t) {
		String bound = null;
		
		Object[] m = (Object[])generationConstraint.get(t);
		
		if(m!=null)
			bound = (String)m[0];
		else
			bound = (String)generationConstraint.get(DEFAULT_LINK_BOUND);
		
		
		ReferenceOption ro = this.getReferenceOption(t);
		
		int size = (ro == null || ro.isScalable()) ? random.randomIntWithScale(bound) : random.randomInt(bound);
		return size;
	}
	
	public void randomGenerateContainmentReferences() {
		ForbiddenMap map = ForbiddenMap.createSpecialLinkForbiddenMap();
		EList<EObject> parents = new HashableBasicEList<EObject>(allObjs);
		EList<EObject> children = new HashableBasicEList<EObject>(allObjs);
		int size = 0;
		
		if(modelroots.size()!=0){
			children.removeAll(modelroots);
			size = allObjs.size()-1;
		} else {
			size = this.random.randomUIntU(allObjs.size());
		}
		//TODO: we should remove unused references here
		EList<EReference> availableReferences = reduceReferences(allContainmentReferences);
		
		randomGenerateReferences(availableReferences, parents, children, true, true, false, false, map, size);
	}

	protected EList<EReference> reduceReferences(EList<EReference> allRefs) {
		EList<EReference> availableReferences = new BasicEList<EReference>();
		for(EReference ref : allRefs) {
			EList<EObject> par = allObjMap.get(ref.getEContainingClass());
			if(par==null || par.size()==0) continue;
			EList<EObject> chd = allObjMap.get(ref.getEReferenceType());
			if(chd==null || chd.size()==0) continue;
			availableReferences.add(ref);
		}
		return availableReferences;
	}
	
	public void randomGenerateOtherReferences() {
		for(EReference ref : otherReferences) {
			randomGenerateOtherReference(ref);
		}
	}
	
	protected EList<EObject> filterByRefs(EList<EObject> obj, EReference... refs) {
		if(refs==null || refs.length==0) {
			return obj;
		}
		EList<EObject> result = new BasicEList<EObject>(obj);
		
		List<EObject> toBeDel = new ArrayList<EObject>();
		
		for(EObject o : obj) {
			boolean del = true;
			for(EReference r : refs) {
				if(r.getEReferenceType().isSuperTypeOf(o.eClass())) {
					del =  false;
					break;
				}
			}
			if(del) toBeDel.add(o);
		}
		
		result.removeAll(toBeDel);
		return result;
	}

	protected EList<EObject> filterByTypes(EList<EObject> obj, EClass... cls) {
		if(cls==null || cls.length==0) {
			return obj;
		}

		EList<EObject> result = new BasicEList<EObject>(obj);
		
		List<EObject> toBeDel = new ArrayList<EObject>();
		
		for(EObject o : obj) {
			boolean del = true;
			for(EClass r : cls) {
				if(r.isSuperTypeOf(o.eClass())) {
					del =  false;
					break;
				}
			}
			if(del) toBeDel.add(o);
		}
		
		result.removeAll(toBeDel);
		return result;
	}
	
	protected void randomGenerateOtherReference(EReference ref) {
		Filter[] filters = referenceFilter.get(ref);

		EList<EObject> parents = allObjMap.get(ref.getEContainingClass());
		EList<EObject> children = null;
		
		ReferenceOption ro = this.getReferenceOption(ref);
		boolean isExternal = (ro!=null && ro.getExternalObjects().size()!=0);
		if(isExternal) {
			children = this.filterByRefs(ro.getExternalObjects(), ref);
		} else {
			children = allObjMap.get(ref.getEReferenceType());
		}
		
		if(parents==null || children==null ||parents.size()==0 ||children.size()==0) return;
		
		if(filters!=null) {
			if(filters[0]!=null)
				parents = (EList<EObject>)filters[0].filter(parents);
			if(filters[1]!=null)
				children = (EList<EObject>)filters[1].filter(children);
		}
		
		EList<EReference> rl = new BasicEList<EReference>(1);
		rl.add(ref);
		
		int size = randomLinkSize(ref);
		
		Object[] b = (Object[]) generationConstraint.get(ref);
		if(b!=null) {
			boolean sourceUnique = (boolean)b[1];
			boolean sourceRequired = (boolean)b[2];
			boolean targetUnique = (boolean)b[3];
			boolean targetRequired = (boolean)b[4];
			boolean transitivity = (boolean)b[5];
			boolean ring = (boolean)b[6];
			boolean circle = (boolean)b[7];
			
			ForbiddenMap map = new ForbiddenMap(ring, circle, transitivity);
			randomGenerateReferences(rl, parents, children, sourceRequired, sourceUnique, targetRequired, targetUnique, map, size);
			
		} else {
			ForbiddenMap map = ForbiddenMap.createNormalLinkForbiddenMap();
			randomGenerateReferences(rl, parents, children, false, false, false, false, map, size);
		}
	}
	
//	protected boolean checkSimpleCase1ForSrc(AbstractLayeredObjectSelector srcSelector,AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap, int remain) {
//		if(remain<=0) return true;
//		
//		if(srcSelector.secondAvailable.size()==1) {
//			Pair<EObject,EReference> srcRef = srcSelector.secondAvailable.get(0);
//			if(srcRef.second.getUpperBound()<0) {
//				List<EObject> list = tarSelector.getAvailableObjects();
//				Collections.shuffle(list);
//				for(EObject to : list) {
//					if(remain<=0) break;
//					if(!forbiddenMap.isForbidden(srcRef.first, to)) {
//						remain--;
//						setReference(srcRef.first, srcRef.second, to);
//						srcSelector.consume(srcRef.first, srcRef.second);
//						tarSelector.consume(to, srcRef.second);
//						forbiddenMap.forbid(srcRef.first, to, srcRef.second);// no multiple links
//					}
//				}
//				System.out.println("in simple case 1");
//				return true;
//			}
//			return false;
//		}
//		
//		return false;
//	}
	
//	protected boolean checkSimpleCase2ForSrc(AbstractLayeredObjectSelector srcSelector,AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap, int remain) {
//		if(remain<=0) return true;
//		
//		if(srcSelector.getAllRefs().size()==1) {
//			while(remain>0) {
//				if(srcSelector.secondAvailable.size()==0) break;
//				if(tarSelector.second.size()==0) break;
//				
//				Pair<EObject,EReference> pair = srcSelector.pickObjectAndReference(AbstractLayeredObjectSelector.SEARCH_SCOPE_SECOND);
//				
//				Collections.shuffle(tarSelector.second);
//				boolean hasTar = false;
//				for(EObject to : tarSelector.second) {
//					if(!forbiddenMap.isForbidden(pair.first, to)) {
//						setReference(pair.first, pair.second, to);
//						srcSelector.consume(pair.first, pair.second);
//						tarSelector.consume(to, pair.second);
//						forbiddenMap.forbid(pair.first, to, pair.second);// no multiple links
//						hasTar = true;
//						remain--;
//						break;
//					}
//				}
//				if(hasTar==false) {
//					srcSelector.putUnhandledObj(pair.first, pair.second, false);
//				}
//			}
//			System.out.println("in simple case 2");
//			return true;
//		}
//		return false;
//	}
	
//	protected boolean checkSimpleCase2ForTar(AbstractLayeredObjectSelector srcSelector,AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap, int remain) {
//		if(remain<=0) return true;
//		
//		if(tarSelector.getAllRefs().size()==1) {
//			while(remain>0) {
//				if(tarSelector.secondAvailable.size()==0) break;
//				if(srcSelector.second.size()==0) break;
//				
//				Pair<EObject,EReference> pair = tarSelector.pickObjectAndReference(AbstractLayeredObjectSelector.SEARCH_SCOPE_SECOND);
//				
//				Collections.shuffle(srcSelector.second);
//				boolean hasTar = false;
//				for(EObject so : srcSelector.second) {
//					if(!forbiddenMap.isForbidden(so, pair.first)) {
//						setReference(so, pair.second, pair.first);
//						srcSelector.consume(so, pair.second);
//						tarSelector.consume(pair.first, pair.second);
//						forbiddenMap.forbid(so, pair.first, pair.second);// no multiple links
//						hasTar = true;
//						remain--;
//						break;
//					}
//				}
//				if(hasTar==false) {
//					tarSelector.putUnhandledObj(pair.first, pair.second, false);
//				}
//			}
//			System.out.println("in simple case 2");
//			return true;
//		}
//		return false;
//	}
//	
//	protected boolean checkSimpleCase1ForTar(AbstractLayeredObjectSelector srcSelector,AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap, int remain) {
//		if(remain<=0) return true;
//		
//		if(tarSelector.secondAvailable.size()==1) {
//			Pair<EObject,EReference> tarRef = tarSelector.secondAvailable.get(0);
//			if(tarRef.second.getUpperBound()<0) {
//				List<EObject> list = srcSelector.getAvailableObjects();
//				Collections.shuffle(list);
//				for(EObject so : list) {
//					if(remain<=0) break;
//					if(!forbiddenMap.isForbidden(so,tarRef.first)) {
//						remain--;
//						setReference(so, tarRef.second, tarRef.first);
//						srcSelector.consume(so, tarRef.second);
//						tarSelector.consume(tarRef.first, tarRef.second);
//						forbiddenMap.forbid(so, tarRef.first, tarRef.second);// no multiple links
//					}
//				}
//				System.out.println("in simple case 1");
//				return true;
//			}
//			return false;
//		}
//		
//		return false;
//	}
	
	/*
	 * 聚合关系：sourceRequired=true, sourceUnique=true, circleAllowed = false
	 * 继承关系：sourceRequired=false, sourceUnique=false, circleAllowed = false
	 * sourceRequired和sourceUnique最后用来初始化targetSelector
	 * targetRequired和targetUnique最后用来初始化sourceSelector
	 */
	protected void randomGenerateReferences(EList<EReference> refs, EList<EObject> parentRanges, EList<EObject> targetRanges, 
			boolean sourceRequired, boolean sourceUnique, boolean targetRequired, boolean targetUnique,  ForbiddenMap forbiddenMap, int size) {
		
		refineConstraints(refs, forbiddenMap);
		
		ConsoleFactory.printToConsole("now to generate "+refs+" "+size,"RMG",true);
		
		AbstractLayeredObjectSelector srcSelector = new LayeredObjectSelectorForRefSrc(this,parentRanges, refs, targetRequired, targetUnique);
		AbstractLayeredObjectSelector tarSelector = new LayeredObjectSelectorForRefTar(this,targetRanges, refs, sourceRequired, sourceUnique);
		
		System.out.println("begin to fill min: "+refs);
		int size1 = linkSrcToTar(srcSelector, tarSelector, forbiddenMap, true, size);
		//System.out.println("src to tar");
		
		int size2 = linkTarToSrc(srcSelector, tarSelector, forbiddenMap, true);
		
		int remain = size - size1- size2;
		
		System.out.println("begin to fill required:"+remain + " "+refs);
		
//		if(checkSimpleCase1ForSrc(srcSelector, tarSelector, forbiddenMap, remain) 
//				|| checkSimpleCase1ForTar(srcSelector, tarSelector, forbiddenMap, remain)
//				|| checkSimpleCase2ForSrc(srcSelector, tarSelector, forbiddenMap, remain)
//				|| checkSimpleCase2ForTar(srcSelector, tarSelector, forbiddenMap, remain))
//			return;

		if(remain>0) { //�����
			boolean lastFails = false;
			while(true) {
				int size3 = 0;
				size3 = linkSrcToTar(srcSelector, tarSelector, forbiddenMap, false, remain);
				if(size3==-1) {
					if(lastFails) break;
					else 
						lastFails = true;
				}
				else {
					remain = remain - size3;
					lastFails = false;
					if(remain<=0)
						break;
				}
				
				size3 = linkTarToSrc(srcSelector, tarSelector, forbiddenMap, false);
				if(size3==-1) {
					if(lastFails==true) break;
					else lastFails = true;
				} else {
					remain = remain - size3;
					lastFails = false;
					if(remain<=0)
						break;
				}
				
				//System.out.println(remain);
			}
			
			if(remain>0) {
				ConsoleFactory.printToConsole("I cannot satisfy the size of link","RMG",true);
			}
		}
	}

	private void refineConstraints(EList<EReference> refs, ForbiddenMap forbiddenMap) {
		if(refs.size()==1) {
			EReference ref = refs.get(0);
			if(!ref.getEContainingClass().isSuperTypeOf(ref.getEReferenceType())
					&&!ref.getEReferenceType().isSuperTypeOf(ref.getEContainingClass())) {
				forbiddenMap.circleAllowed = true;
				forbiddenMap.ringAllowed=true;
				forbiddenMap.transitive = false;
				System.out.println(ref+" refine constraints");
			}
		}
	}
	
	protected int linkTarToSrc(
			AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector,
			ForbiddenMap forbiddenMap, boolean fillMin) {
		int count = 0;
		int scope = fillMin ? AbstractLayeredObjectSelector.SEARCH_SCOPE_FIRST : AbstractLayeredObjectSelector.SEARCH_SCOPE_TILL_SECOND;
		//if refs.size()=1 && src.size()=1 && (refs[0].lower=1 || srcRequired) && !tarUnique in simple case 1
		//if refs.size()=1 && !tarUnique && !transitive && ring && circle && (refs[0].lower > 0 || srcRequire) in simple case 2
//		EReference singleRef = tarSelector.getAllRefs().get(0);
		
		//try to handle simple cases
		if(fillMin) {
//			if(srcSelector.allRefs.size()==1) {
//				EReference singleRef = srcSelector.allRefs.get(0);
				// swap tar with src to reuse the test method
				if(testSimpleCaseAForMin(srcSelector.allRefs, tarSelector, srcSelector, forbiddenMap)) {
					return tarToSrcSimpleCaseAForMin(srcSelector.allRefs, srcSelector, tarSelector, forbiddenMap);
				}
//			}
			
//			if(tarSelector.getAllRefs().size()==1
//					&& (tarSelector.getRefLowerBound(singleRef)==1 || tarSelector.required)
//					&& !srcSelector.unique && srcSelector.getRefUpperBound(singleRef)<0) {
//				if(srcSelector.allObjs.size()==1) {
//					// in simple case 1
//					System.out.println("[tar to src] in simple case 1: "+srcSelector.allRefs);
//					EObject singleSrc = srcSelector.allObjs.get(0);
//					int allSize = tarSelector.firstAvailable.size();				
//					int i = allSize;
//					
//					for(;i>0;i--) {
//						Pair<EObject, EReference> pair = tarSelector.firstAvailable.get(i-1);
//						setReference(singleSrc, pair.second, pair.first);
//						if(!singleRef.isContainment()) {
//							//NOTE: if it is containment, it would not enter the mode when fillMin = false
//							//      so in this case, we do not have to mark it.
//							srcSelector.consume(singleSrc, pair.second);
//							tarSelector.consume(pair.first, pair.second);
//							forbiddenMap.forbid(singleSrc, pair.first, pair.second);
//						}
//					}
//					System.out.println("[tar to src] out simple case 1: "+srcSelector.allRefs);
//					//throw new RuntimeException("xxxx");
//					return allSize;					
//				} else if(
//						(!singleRef.getEContainingClass().isSuperTypeOf(singleRef.getEReferenceType())
//								&& !singleRef.getEReferenceType().isSuperTypeOf(singleRef.getEContainingClass())
//						|| (forbiddenMap.circleAllowed && forbiddenMap.ringAllowed))
//						) {
//					System.out.println("[tar to src] in simple case 2: "+srcSelector.allRefs);
//					
//					int allSize = tarSelector.firstAvailable.size();
//					int i = allSize;
//					
//					for(;i>0;i--) {
//						EObject singleSrc = srcSelector.allObjs.get(random.randomUInt() % srcSelector.allObjs.size());
//						
//						Pair<EObject, EReference> pair = tarSelector.firstAvailable.get(i-1);
//						
//						setReference(singleSrc, pair.second, pair.first);
//						srcSelector.consume(singleSrc, pair.second);
//						tarSelector.consume(pair.first, pair.second);
//						forbiddenMap.forbid(singleSrc, pair.first, pair.second);
//					}
//					
//					System.out.println("[tar to src] out simple case 2: "+srcSelector.allRefs);
//					
//					return allSize;
//				}
//			}
		} else {
			
		}
		
		do { //����tar�����Ҫ��
			Pair<EObject, EReference> pair = null;
			
			pair = tarSelector.pickObjectAndReference(scope);
			
			if(pair==null) {
				if(fillMin==false)
					return -1;
				else
					break;
			}
			
			//System.out.println(tarSelector.firstAvailable.size());
			//System.out.println(pair.first + ","+pair.second);

			int d[] = tarSelector.getDegree(pair.first);
			int b[] = fillMin ? 
					tarSelector.getLowerBound(pair.first.eClass()) : tarSelector.getUpperBound(pair.first.eClass());
					
			List<EReference> cRefs = tarSelector.getRefs(pair.first.eClass());
			int id = cRefs.indexOf(pair.second);
			
			int querySize = fillMin ? b[id+1]-d[id+1] : 1;
			if(querySize==0) querySize = 1;
			
			//long begin = Calendar.getInstance().getTimeInMillis();
			//long start = Calendar.getInstance().getTimeInMillis();
			List<EObject> srcs = srcSelector.pickObjectWithConditions(pair.first, 
					pair.second, forbiddenMap, querySize);
			//time  += Calendar.getInstance().getTimeInMillis()-start;
			//long end = Calendar.getInstance().getTimeInMillis();
			//System.out.println("time:"+(end-begin));
			
			if(srcs!=null && srcs.size()>0) {
				//System.out.println("srcs not empty");
				for(EObject src : srcs) {
					if(src!=null){
						setReference(src, pair.second, pair.first);
						count++;
						srcSelector.consume(src, pair.second);
						tarSelector.consume(pair.first, pair.second);
//						if(circleAllowed==false) {
//							forbiddenMap.forbid(pair.first,src, pair.second);
//						}
						forbiddenMap.forbid(src, pair.first, pair.second);
					}
				}
			}
			else {
				//�����޲�
				System.out.println("try fix 2: "+pair.first+", "+pair.second);
				// x.[pair.second] = pair.first������
				//EClass expectedType = pair.second.getEContainingClass();
				//long begin = Calendar.getInstance().getTimeInMillis();
				
				List<EObject> availableSrcs = srcSelector.getAvailableObjects();
				
				List<Quad<EObject, EReference, EObject, List<EObject>>> sub= 
						pickSrcSubstitute(pair.first, pair.second, availableSrcs,
								srcSelector, tarSelector, forbiddenMap, querySize);
				
				//long end = Calendar.getInstance().getTimeInMillis();
				//System.out.println("Time:"+(end-begin)+", Found:"+sub.size());
				
				if(sub.size()==0) {
					//�޲����ɹ�ʱ������������ƫ���Ե��µĳ�ͻ�����ھۺϹ�ϵ�����Բ��ò��뷨���������ڸ�һ���ƫ���ϵ����ʱ��������
					//���ڼ̳���Ĺ�ϵ�����Բ��ò��뷨+��ת��ϵ
					boolean inserted=false;
					
					if(checkPreconditionCase2(pair,tarSelector)){
						//Ŀǰֻ�޲��ۺϹ�ϵ
						//->表示聚合 <表示继承
						//这其中有两种情况：1) r1:A<-B, C<B, D<B, r2:C->D，其中B和C、B和D都可以相同，但当B和D相同时，已经被之前的算法处理了；2）r1:A<-B, r2:B->C, r3:C->B
						//对第一种情况，当为D创建r1时，可能出现失败。此时，要找 a.r1=c, d._=b，以及rx，令a.r1=d，d.rx=c，删除a.r1=c
						//对第二种情况，当为C创建r3时，可能出现失败。此时，一定有一个a，a.r1=null，且任意b，c.r2=b。那么需要找到这样的<a,r1>，<x,rx,y>, 以及一个<b,r2>，令a.r1=y，b.r3=c，删除x.rx=y
						{
							System.out.println("in case 2: "+pair.first.eClass().getName()+","+pair.second.getName());
							//case 2
							//d = pair.first
							//r1 = pair.second
							//a belongs to allObjMap.get(pair.second.getEContainingClass())
							//b is a descendant of d, owning an available reference rx, where rx.eReferenceType.isSuperTypeOf(a.r1.eClass())
							//<ao,co,bo,rx>
							Quad<EObject,EObject,EObject,EReference> result = searchCandidateCase2(pair.first,pair.second,forbiddenMap,tarSelector.allRefs);
							if(result!=null) {
								System.out.println("fixed in case 2");
								// +a.r1=d, -a.r1=c
								replaceReferenceTar(result.first,pair.second,result.second,pair.first);
								//+b.rx=c
								setReference(result.third,result.fourth,result.second);
								
								srcSelector.consume(result.third, result.fourth);
								tarSelector.consume(result.second, pair.second, result.fourth);
								tarSelector.consume(pair.first, pair.second);
								
								forbiddenMap.reset(result.first, result.second, pair.second);
								forbiddenMap.forbid(result.first, pair.first, pair.second);
								forbiddenMap.forbid(result.third, result.second, result.fourth);
								
								count++;
								inserted = true;
//								System.out.println("insert case 2");
							}
						}
						
//						if(false) {
//							//case 3
//							//c = pair.first
//							//r3 = pair.second
//							//b is a descendant of c
//							//a.r1=null, a belongs to {x|r1.eContainingClass().isSuperTypeOf(x.eClass()) and r1.eReferenceType().isSuperTypeOf(c.eClass())}
//							//_.r2 = b
//							//+a.r1 = b, +b.r3=c, -_.r2=b
//							System.out.println("in case 3 "+pair.first.eClass().getName()+", "+pair.second.getName());
//							//<<a,r1>，<x,rx,y>,b>，令+a.r1=y，+b.r3=c，-x.rx=y
//							Triple<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject> result = searchCandidateCase3(pair.first,pair.second,forbiddenMap,tarSelector.allRefs);
//							System.out.println(result);
//							if(result!=null) {
//								//-x.rx=y
//								EcoreUtil.remove(result.second.first, result.second.second, result.second.third);
//								//+a.r1=y
//								setReference(result.first.first,result.first.second,result.second.third);
//								//+b.r3=c
//								setReference(result.third,pair.second,pair.first);
//								
//								//+a.r1
//								srcSelector.consume(result.first.first, result.first.second);
//								//+b.r3
//								srcSelector.consume(result.third, pair.second);
//								//-.rx=y +.r1=y
//								tarSelector.consume(result.second.third, result.second.second, result.first.second);
//								//+.r3=c
//								tarSelector.consume(pair.first, pair.second);
//								//-x.rx
//								srcSelector.release(result.second.first,result.second.second);//not implemented yet
//								
//								forbiddenMap.reset(result.second.first, result.second.third, result.second.second);
//								forbiddenMap.forbid(result.first.first, result.second.third, result.first.second);
//								forbiddenMap.forbid(result.third,pair.first,pair.second);
//								
//								count++;
//								inserted = true;
//							}
//						}
						
					} else if(pair.second.isContainer()){
						//�Ƿ��������������֣�
						System.out.println("detected a container reference");
					}
					if(!inserted)
						tarSelector.putUnhandledObj(pair.first, pair.second,fillMin);
				} else {
					int need = querySize;
					
					need = need<sub.size() ? need : sub.size();
					
					for(int i=0;i<need;i++) {
						int ri =0;
						
						ri = random.randomUIntU(sub.size());
						Quad<EObject,EReference, EObject, List<EObject>> q = sub.get(ri);
						sub.remove(ri);
						ri = random.randomUIntU(q.fourth.size());
						EObject nSrc = q.fourth.get(ri);
						q.fourth.remove(ri);
						
						replaceReferenceSrc(q.third, nSrc, q.second, q.first);
						setReference(q.third, pair.second, pair.first);
						count++;
						
						srcSelector.consume(nSrc, q.second);
						srcSelector.consume(q.third, q.second, pair.second);					
						tarSelector.consume(pair.first, pair.second);
						
						//no multiple references
						forbiddenMap.forbid(nSrc, q.first,q.second);
						forbiddenMap.reset(q.third, q.first, q.second);
						forbiddenMap.forbid(q.third, pair.first, pair.second);
						
//						if(circleAllowed==false) {
//							forbiddenMap.forbid(q.first, nSrc, q.second);
//							forbiddenMap.reset(q.first, q.third, q.second);
//							forbiddenMap.forbid(pair.first, q.third, pair.second);
//						}
					}
				}
			}
			//System.out.println(count);
		}while(fillMin);
		
		if(tarSelector.first.size()!=0) {
			
			// handle case 3 here
			int unhandled = tarSelector.first.size();
			List<EObject> unhandledObjects = new ArrayList<EObject>(tarSelector.first);
			for(EObject o : unhandledObjects) {
				RC:for(EReference ref : tarSelector.allRefs) {
					// test if ref belongs to a circle
					// if not continue
					if(ref.getEReferenceType().isSuperTypeOf(o.eClass())) {
						System.out.println("in case 3 "+tarSelector.allRefs);
						Pair<EObject,EReference> pair = new Pair<EObject,EReference>(o,ref);
						
						//we hope insert _.ref=o
						//result.first: a.r1, where we can insert result.second.third
						//result.second: x.rx=y, where y can be the new root of the tree of o
						//result.third: b, where we can insert b.ref=o
						//result.fourth: <d,z,rz>, where a.r1=d, z is a descendant node of o, and we can insert z.rz=d
						Quad<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject,Triple<EObject,EObject,EReference>> result = searchCandidateCase3(pair.first,pair.second,forbiddenMap,tarSelector.allRefs);
						if(result!=null) {
//							if(result.fourth!=null) {
//								//-a.r1=d
//								//+z.rz=d
//								
//								EcoreUtil.remove(result.first.first, result.first.second,result.fourth.first);
//								setReference(result.fourth.second,result.fourth.third,result.fourth.first);
//								
//								
//								srcSelector.consume(result.fourth.second, result.fourth.third);
//								tarSelector.consume(result.fourth.third, result.first.second, result.fourth.third);
//								srcSelector.release(result.first.first,result.first.second);
//								
//								forbiddenMap.reset(result.first.first, result.fourth.first, result.first.second);
//								forbiddenMap.forbid(result.fourth.second, result.fourth.first, result.fourth.third);
//							}
							
							//-x.rx=y
							EcoreUtil.remove(result.second.first, result.second.second, result.second.third);
							
							if(result.fourth!=null) {
								//-a.r1=d
								EcoreUtil.remove(result.first.first,result.first.second,result.fourth.first);
							}
							//+a.r1=y
							setReference(result.first.first,result.first.second,result.second.third);
							
							//+b.r3=c
							setReference(result.third,pair.second,pair.first);
							
							if(result.fourth!=null) {
								//+z.rz=d
								setReference(result.fourth.second,result.fourth.third,result.fourth.first);
							}
							
							if(result.fourth==null) {//in this case, |a.r1| increases
								//+a.r1
								srcSelector.consume(result.first.first, result.first.second);
							}
							//+b.r3
							srcSelector.consume(result.third, pair.second);
							//-.rx=y +.r1=y
							tarSelector.consume(result.second.third, result.second.second, result.first.second);
							//+.r3=c
							tarSelector.consume(pair.first, pair.second);
							
							
							//-x.rx
							srcSelector.release(result.second.first,result.second.second);//not implemented yet
							
							if(result.fourth!=null) {
								srcSelector.consume(result.fourth.second, result.fourth.third);//add d to c
								tarSelector.consume(result.fourth.first, result.first.second, result.fourth.third);//move d under c
							}
							
							forbiddenMap.reset(result.second.first, result.second.third, result.second.second);
							forbiddenMap.forbid(result.first.first, result.second.third, result.first.second);
							forbiddenMap.forbid(result.third,pair.first,pair.second);
							
							if(result.fourth!=null) {
								forbiddenMap.reset(result.first.first, result.fourth.first, result.first.second);
								forbiddenMap.forbid(result.fourth.second, result.fourth.first, result.fourth.third);
							}
							
							count++;
							unhandled--;
							System.out.println("insert case 3");
							break RC;
						}
					}
				}
			}
			if(unhandled!=0) {
				ConsoleFactory.printToConsole("it seems that I cannot complete the model #2","RMG",true);
				ConsoleFactory.printToConsole(tarSelector.first.toString(), "RMG", true);
			}
		}
		return count;
	}

	@Deprecated
	protected int tarToSrcSimpleCaseAForMin(EReference singleRef, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		System.out.println("[tar to src] in simple case 1: "+srcSelector.allRefs);
		int allSize = tarSelector.firstAvailable.size();
		int i = allSize;
		int mul = tarSelector.getRefLowerBound(singleRef);
		if(mul==0) mul = 1;
		for(;i>0;i--) {
			Pair<EObject, EReference> pair = tarSelector.firstAvailable.get(i-1);					
			for(int j=0;j<mul;j++) {
				EObject singleSrc = srcSelector.pick();
				setReference(singleSrc, pair.second, pair.first);
				srcSelector.consume(singleSrc, pair.second);
				tarSelector.consume(pair.first, pair.second);
				forbiddenMap.forbid(singleSrc, pair.first, pair.second);
			}
		}
		System.out.println("[tar to src] out simple case 1: "+srcSelector.allRefs);
		return allSize;
	}
	
	protected int tarToSrcSimpleCaseAForMin(List<EReference> refs, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		System.out.println("[tar to src] in simple case 1: "+srcSelector.allRefs);
		int allSize = tarSelector.firstAvailable.size();
		int i = allSize;
		
		
		
		for(;i>0;i--) {
			Pair<EObject, EReference> pair = tarSelector.firstAvailable.get(i-1);					
			
			int mul = tarSelector.getRefLowerBound(pair.second);
			if(mul==0) mul = 1;
			
			for(int j=0;j<mul;j++) {
				EObject singleSrc = srcSelector.pick();
				setReference(singleSrc, pair.second, pair.first);
				srcSelector.consume(singleSrc, pair.second);
				tarSelector.consume(pair.first, pair.second);
				forbiddenMap.forbid(singleSrc, pair.first, pair.second);
			}
		}
		System.out.println("[tar to src] out simple case 1: "+srcSelector.allRefs);
		return allSize;
	}

	protected boolean checkPreconditionCase2(Pair<EObject, EReference> pair, AbstractLayeredObjectSelector tarSelector) {
		if(pair.second.isContainment()==false) return false;
		EList<EReference> firstContainment = pair.first.eClass().getEAllContainments();
		if(firstContainment.isEmpty()) return false;
		if(pair.second.getEReferenceType()==pair.first.eClass()) return false;
		if(tarSelector.testDisjoin(firstContainment)) return false;
		
		return true;
	}
	
	/*
	 * find a triple <alt,obj,reference>, to
	 * (alt.container).(alt.eContainingFeature)=first and obj.reference = alt
	 */
//	private Triple<EObject, EObject, EReference> collectUnsatisifiedDescendantObject(
//			EObject first, EReference second, EList<EObject> alternatives, EList<EReference> refs , ForbiddenMap forbiddenMap) {
//		// TODO Auto-generated method stub
//		HashSet<EObject> visited = new HashSet<EObject>();
//		for(EObject alt : alternatives) {
//			EObject container = alt.eContainer();
//			if(visited.contains(alt)) continue;
//			
//			if(!forbiddenMap.isForbidden(container, first)){
//				EClass containerClass = container.eClass();
//				EReference containmentRef = alt.eContainmentFeature();
//				
//				Pair<EObject,EReference> pair = null;
//				
//				if(second != containmentRef)
//					pair = collectUnsatisifiedDescendantObject(first, second.getEContainingClass(), second, visited);
//				else 
//					pair = collectUnsatisifiedDescendantObject(first, refs, alt.eClass(), visited);
//				
//				if(pair!=null) {
//					return new Triple<EObject,EObject,EReference>(alt,pair.first,pair.second);
//				}
//			}
//		}
//		
//		return null;
//	}
//
//	private Pair<EObject, EReference> collectUnsatisifiedDescendantObject(
//			EObject first, EList<EReference> refs, EClass type, HashSet<EObject> visited) {
//		if(visited.contains(first)) return null;
//		visited.add(first);
//		
//		EList<EObject> children = first.eContents();
//		for(EObject c : children) {
//			for(EReference reference : refs) {
//				if(reference.getEReferenceType().isSuperTypeOf(type)) {
//					if(reference.isMany()){
//						int lower = reference.getLowerBound();
//						int upper = reference.getUpperBound();
//						if(upper<0) return new Pair<EObject,EReference>(c,reference);
//						if(((EList)c.eGet(reference)).size()<upper) 
//							return new Pair<EObject,EReference>(c,reference);
//					} else {
//						if(c.eGet(reference)==null) 
//							return new Pair<EObject,EReference>(c,reference);
//					}
//				}
//			}
//			Pair<EObject,EReference> cand = collectUnsatisifiedDescendantObject(c,refs,type,visited);
//			if(cand!=null) return cand;
//		}
//		return null;
//	}

	//<<a,r1>，<x,rx,y>, b, <d,z,rz>>
	private Quad<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject, Triple<EObject,EObject,EReference>> searchCandidateCase3(
			EObject cObj, EReference r3, ForbiddenMap forbiddenMap,
			EList<EReference> allRefs) {
		// TODO Auto-generated method stub
		HashMap<EObject,EObject> map = new HashMap<EObject,EObject>();
		for(EReference r1 : allRefs) {
			EList<EObject> aList = allObjMap.get(r1.getEContainingClass());
			for(EObject ao : aList) {
				if(forbiddenMap.isForbidden(ao, cObj)) continue;
				if(notFull(ao,r1)) {
					Pair<Triple<EObject,EReference,EObject>, EObject> pair = searchDescendantCase3(cObj,r3,r1,allRefs,map);
					if(pair!=null) {
						return new Quad<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject,Triple<EObject,EObject,EReference>>(new Pair<EObject,EReference>(ao,r1),pair.first,pair.second, null);
					}					
				} else {
					System.out.println("full in case 3");
					Pair<Triple<EObject,EReference,EObject>, EObject> pair = searchDescendantCase3(cObj,r3,r1,allRefs,map);
					if(pair!=null) {
						if(r1.isMany()) {
							List<EObject> dList = (List<EObject>)ao.eGet(r1);
							for(EObject d : dList) {
								Triple<EObject,EObject,EReference> ret = searchDescendantCase3Part3(d,cObj,allRefs,pair.first.first,pair.first.second);
								if(ret!=null) {
									return new Quad<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject,Triple<EObject,EObject,EReference>>(new Pair<EObject,EReference>(ao,r1),pair.first,pair.second, ret);
								} else return null;
							}							
						} else {
							EObject d = (EObject)ao.eGet(r1);
							Triple<EObject,EObject,EReference> ret = searchDescendantCase3Part3(d,cObj,allRefs,pair.first.first,pair.first.second);
							if(ret!=null) {
								return new Quad<Pair<EObject,EReference>, Triple<EObject,EReference,EObject>, EObject,Triple<EObject,EObject,EReference>>(new Pair<EObject,EReference>(ao,r1),pair.first,pair.second, ret);
							} else return null;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	private Triple<EObject,EObject,EReference> searchDescendantCase3Part3(EObject d, EObject c,List<EReference> allRefs, EObject toBeDelO, EReference toBeDelR) {
		for(EReference r : allRefs) {
			if(r.getEContainingClass().isSuperTypeOf(c.eClass())) {
				if(r.getEReferenceType().isSuperTypeOf(d.eClass()) && ((c==toBeDelO && r==toBeDelR) || notFull(c,r))) {
					return new Triple<EObject,EObject,EReference>(d,c,r);
				}
				if(r.isMany()) {
					List<EObject> children = (List<EObject>) c.eGet(r);
					for(EObject nc : children) {
						Triple<EObject,EObject,EReference> ret = searchDescendantCase3Part3(d,nc,allRefs,toBeDelO, toBeDelR);
						if(ret!=null) return ret;
					}					
				} else {
					EObject nc = (EObject)c.eGet(r);
					Triple<EObject,EObject,EReference> ret = searchDescendantCase3Part3(d,nc,allRefs,toBeDelO, toBeDelR);
					if(ret!=null) return ret;
				}
			} else continue;
		}
		return null;
	}

	private Pair<Triple<EObject, EReference, EObject>, EObject> searchDescendantCase3(
			EObject xo, EReference r3, EReference r1, EList<EReference> allRefs,HashMap<EObject,EObject> map) {
		// TODO Auto-generated method stub
		for(EObject yo : xo.eContents()) {
			EReference rx = yo.eContainmentFeature();
			if(r1.getEReferenceType().isSuperTypeOf(yo.eClass()) && allRefs.contains(rx)) {
				EObject bo = searchDescendantCase3Part2(yo,r3,map);
				if(bo!=null)
					return new Pair<Triple<EObject, EReference, EObject>, EObject>(new Triple<EObject, EReference, EObject>(xo,rx,yo),bo);
			}
			Pair<Triple<EObject, EReference, EObject>, EObject> pair = searchDescendantCase3(yo, r3, r1, allRefs,map);
			if(pair!=null)
				return pair;
		}
		return null;
	}

	private EObject searchDescendantCase3Part2(EObject yo, EReference r3, HashMap<EObject,EObject> map) {
		EObject cBo = map.get(yo);
		if(cBo!=null) return cBo;
		
		for(EObject bo : yo.eContents()) {
			if(r3.getEContainingClass().isSuperTypeOf(bo.eClass()) && notFull(bo,r3)) {
				map.put(yo, bo);
				return bo;
			}
			EObject s = searchDescendantCase3Part2(bo,r3,map);
			if(s!=null)
				return s;
		}
		return null;
	}

	//<ao,co,bo,rx>
	private Quad<EObject, EObject, EObject, EReference> searchCandidateCase2(
			EObject dObj, EReference r1, ForbiddenMap forbiddenMap, EList<EReference> allRefs) {
		EList<EObject> alternatives = allObjMap.get(r1.getEContainingClass());
		HashSet<EObject> visited = new HashSet<EObject>();
		for(EObject ao : alternatives) {
			if(forbiddenMap.isForbidden(ao, dObj)) continue;
			if(r1.isMany()) {
				EList<EObject> cList = (EList<EObject>) ao.eGet(r1);
				for(EObject co : cList) {
					Pair<EObject, EReference> bObjAndRx = searchDescendantCase2(dObj,co,visited,allRefs);
					if(bObjAndRx!=null)
						return new Quad<EObject, EObject, EObject, EReference>(ao,co,bObjAndRx.first,bObjAndRx.second);
				}
			} else {
				EObject co = (EObject) ao.eGet(r1);
				if(co==null) return null;
				Pair<EObject, EReference> bObjAndRx = searchDescendantCase2(dObj,co,visited,allRefs);
				if(bObjAndRx!=null)
					return new Quad<EObject, EObject, EObject, EReference>(ao,co,bObjAndRx.first,bObjAndRx.second);
			}
		}
		
		return null;
	}

	private Pair<EObject, EReference> searchDescendantCase2(EObject dObj,
			EObject co, HashSet<EObject> visited, EList<EReference> allRefs) {
		if(visited.contains(dObj)) return null;
		visited.add(dObj);
		EClass coCls = co.eClass();
		EClass doCls = dObj.eClass();
		
		for(EReference rx : allRefs) {
			if(rx.getEReferenceType().isSuperTypeOf(coCls) && notFull(dObj,rx)) {
				return new Pair<EObject,EReference>(dObj,rx);
			}
		}
		
		for(EObject child : dObj.eContents()) {
			Pair<EObject,EReference> pair = searchDescendantCase2(child,co,visited,allRefs);
			if(pair!=null)
				return pair;
		}
		
		
		return null;
	}
	
	private boolean notFull(EObject dObj, EReference rx) {
		if(rx.getEContainingClass().isSuperTypeOf(dObj.eClass())==false) return false;
		if(rx.isMany()) {
			return rx.getUpperBound()<0 || ((EList<?>)dObj.eGet(rx)).size()<rx.getUpperBound();
		} else {
			return !dObj.eIsSet(rx);
		}
	}

//	private EObject collectUnsatisifiedDescendantObject(EObject first,
//			EClass type, EReference reference, HashSet<EObject> visited) {
//		if(visited.contains(first)) return null;
//		visited.add(first);
//		
//		EList<EObject> children = first.eContents();
//		for(EObject c : children) {
//			if(type.isSuperTypeOf(c.eClass())) {
//				if(reference.isMany()){
//					int lower = reference.getLowerBound();
//					int upper = reference.getUpperBound();
//					if(upper<0) return c;
//					if(((EList)c.eGet(reference)).size()<upper) 
//						return c;
//				} else {
//					if(c.eGet(reference)==null) 
//						return c;
//				}
//			}
//			EObject cand = collectUnsatisifiedDescendantObject(c,type,reference,visited);
//			if(cand!=null) return cand;
//		}
//		return null;
//	}


	static long time = 0;

	/**
	 * 
	 * @param srcSelector
	 * @param tarSelector
	 * @param circleAllowed
	 * @param forbiddenMap
	 * @param fillMin- when fillMin is true, the function is executed in batch mode
	 * @return
	 */
	protected int linkSrcToTar(AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector,
			ForbiddenMap forbiddenMap, boolean fillMin, int remain) {
		//check simple condition
		//condition 1: if there is only one src.ref,select count target elements randomly, where count = ref.upper or target.count
		//condition 2: if there is only one ref, and circle constraint will not be violated, random select a target		
		int count = 0;
		int scope = fillMin ? AbstractLayeredObjectSelector.SEARCH_SCOPE_FIRST : AbstractLayeredObjectSelector.SEARCH_SCOPE_TILL_SECOND;
		//if refs.size()=1 && tar.size()=1 && (refs[0].lower=1 || tarRequired) && !srcUnique in simple case 1
		//if refs.size()=1 && !srcUnique && !transitive && ring && circle && (refs[0].lower > 0 || tarRequire) in simple case 2
		
		//try to handle simple cases
		if(fillMin) {
//			if (srcSelector.getAllRefs().size() == 1) {
				int simpleCase = 0;
				boolean triedSimpleCase = false;
				if (testSimpleCaseAForMin(srcSelector.allRefs, srcSelector, tarSelector, forbiddenMap)) {
					triedSimpleCase = true;
					simpleCase += srcToTarSimpleCaseAForMin(srcSelector.allRefs, srcSelector, tarSelector, forbiddenMap);
				}
				if(testSimpleCaseBForMin(srcSelector.allRefs, srcSelector, tarSelector, forbiddenMap)) {
					triedSimpleCase = true;
					simpleCase += srcToTarSimpleCaseBForMin(srcSelector.allRefs, srcSelector, tarSelector, forbiddenMap, remain-simpleCase);
				}
				if(triedSimpleCase)
					return simpleCase;
//			}
		} else {
		}
		
		//try to handle complex cases
		do{//��������src�����Ҫ��
			Pair<EObject, EReference> pair = null;
			
			pair = srcSelector.pickObjectAndReference(scope);
			//System.out.println("select object and ref:"+pair);
			
			if(pair==null) {
				if(fillMin==false) 
					return -1;
				else 
					break;
			}
			
			int d[] = srcSelector.getDegree(pair.first);
			int b[] = fillMin ? 
					srcSelector.getLowerBound(pair.first.eClass()) : srcSelector.getUpperBound(pair.first.eClass());
					
			List<EReference> cRefs = srcSelector.getRefs(pair.first.eClass());
			int id = cRefs.indexOf(pair.second);
			
			int querySize = fillMin ? b[id+1]-d[id+1] : 1;
			if(querySize==0) querySize = 1;
			
			//long start = Calendar.getInstance().getTimeInMillis();
			List<EObject> tars = tarSelector.pickObjectWithConditions(pair.first, 
					pair.second, forbiddenMap, querySize);
			//time  += Calendar.getInstance().getTimeInMillis()-start;
			
			if(tars!=null && tars.size()>0) {//�������Ҫ��
				//System.out.println("set");
				for(EObject tar : tars) {
					setReference(pair.first, pair.second, tar);
					count++;
					srcSelector.consume(pair.first, pair.second);
					tarSelector.consume(tar, pair.second);
//					if(circleAllowed==false) {
//						forbiddenMap.forbid(tar, pair.first, pair.second);
//					}
					forbiddenMap.forbid(pair.first, tar, pair.second);// no multiple links
				}
			} else {
				//�����޲�
//				System.out.println("fix "+pair.first+","+pair.second.getName());
				//EClass expectedType = pair.second.getEReferenceType();
				List<EObject> availableTars = tarSelector.getAvailableObjects();
				
				List<Quad<EObject, EReference, EObject, List<EObject>>> sub= 
						pickTarSubstitute(pair.first, pair.second, availableTars,
								srcSelector, tarSelector, forbiddenMap,querySize);
				if(sub.size()==0) {
					//���linkTarToSrc
//					boolean inserted=false;
//					if(pair.second.isContainment()){
//						//Ŀǰֻ�޲��ۺϹ�ϵ
//						EList<EObject> alternatives = allObjMap.get(pair.second.getEReferenceType());
//						for(EObject alt : alternatives){
////							if(alt!=pair.first && alt.eContainer()!=null && alt.eContainmentFeature()==pair.second) {
////								EObject container = alt.eContainer();
////								if(!forbiddenMap.isForbidden(container, pair.first)){
////									EClass containerClass = container.eClass();
////									EObject obj = collectUnsatisifiedDescendantObject(pair.first,containerClass,pair.second);
////									if(obj!=null) {
//
////										inserted = true;
////									}
////								}
//							}
//						}
//					
//					if(!inserted)
					//�ݲ��������ܲ�����ִ���
					srcSelector.putUnhandledObj(pair.first, pair.second, fillMin);
				} else {// circle
					int need = querySize;
					
					need = need<sub.size() ? need:sub.size();
					
					for(int i=0;i<need;i++) {
						int ri =0;
						
						ri = random.randomUIntU(sub.size());
						Quad<EObject,EReference, EObject, List<EObject>> q = sub.get(ri);
						sub.remove(ri);
						ri = random.randomUIntU(q.fourth.size());
						EObject nTar = q.fourth.get(ri);
						q.fourth.remove(ri);//��Ϊ���Ѿ��������������Ԫ����,����ɾ��
						replaceReferenceTar(q.first, q.second, q.third, nTar);
						setReference(pair.first, pair.second, q.third);
						count++;
						
						srcSelector.consume(pair.first, pair.second);
						tarSelector.consume(q.third, q.second, pair.second);					
						tarSelector.consume(nTar, q.second);
						
//						if(circleAllowed==false) {
//							forbiddenMap.forbid(nTar, q.first, q.second);
//							forbiddenMap.reset(q.third,q.first, q.second);
//							forbiddenMap.forbid(q.third,pair.first, pair.second);
//						}
						
						forbiddenMap.forbid(q.first,nTar, q.second);
						forbiddenMap.reset(q.first,q.third, q.second);
						forbiddenMap.forbid(pair.first,q.third, pair.second);
					}
				}
			}
			//System.out.println(count);
		}while(fillMin);
		
		if(srcSelector.first.size()!=0 && fillMin) {
			ConsoleFactory.printToConsole("it seems that I cannot complete the model #1","RMG",true);
		}
		
		return count;
	}

	protected int srcToTarSimpleCaseBForMin(EList<EReference> allRefs, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap, int remain) {
		System.out.println("[src to tar] in simple case 2: " + srcSelector.allRefs);
		int actualSize = 0;
		for(int i=0;i<remain;i++) {
			Pair<EObject, EReference> pair = srcSelector.pickObjectAndReference(AbstractLayeredObjectSelector.SEARCH_SCOPE_ALL);
			if(pair==null) continue;
			EObject singleTar = tarSelector.pick();
			if(singleTar==null) continue;
			setReference(pair.first, pair.second, singleTar);
			srcSelector.consume(pair.first, pair.second);
			tarSelector.consume(singleTar, pair.second);
			forbiddenMap.forbid(pair.first, singleTar, pair.second);// no multiple links
			actualSize++;
		}
		
		System.out.println("[src to tar] out simple case 2: " + srcSelector.allRefs);
		return actualSize;
	}

	/*
	 * simple case A的意图是，只有一条边，且不会产生环
	 */
	@Deprecated
	protected boolean testSimpleCaseAForMin(EReference singleRef, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		return (srcSelector.getRefLowerBound(singleRef) >= 1 || srcSelector.required)
				&& !tarSelector.required && !tarSelector.unique // 没法同时满足两端的约束
				&& (!this.hasCommonSubClass.get(singleRef.getEContainingClass(), singleRef.getEReferenceType())
						|| (forbiddenMap.circleAllowed && forbiddenMap.ringAllowed));
	}
	
	protected boolean testSimpleCaseAForMin(List<EReference> refs, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		
		if(!srcSelector.required) {
			for(EReference ref : refs) {
				if(srcSelector.getRefLowerBound(ref) < 1)
					return false;
			}
		}
		
//		if(tarSelector.required && !tarSelector.unique) // maybe removed
//			return false;
		
		if(!(forbiddenMap.circleAllowed && forbiddenMap.ringAllowed)){
			for(EReference ra : refs) {
				for(EReference rb : refs) {
					if(hasCommonSubClass.get(ra.getEContainingClass(), rb.getEReferenceType())==Boolean.TRUE) {
						return false;
					}
				}
			}
		}
		
		// this ensures the co-domains of all refs are identical
		List<EClass> cc = null;
		for(EReference ra : refs) {
			EClass ca = tarSelector.getReferenceClass(ra);
			List<EClass> cca = concreteSubClassMap.get(ca);
			if(cc==null) cc = cca;
			else {
				if(!(cca.containsAll(cc) && cc.containsAll(cca)))
					return false;
				cc = cca;
			}
		}
		return true;
	}
	
	// linkSrcToTar only
	protected boolean testSimpleCaseBForMin(List<EReference> refs, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {

		for(EReference ref : refs) {
			if(srcSelector.getRefUpperBound(ref) > 0)
				return false;
		}
		
//		if(tarSelector.required)
//			return false;
		
		if(!(forbiddenMap.circleAllowed && forbiddenMap.ringAllowed)){
			for(EReference ra : refs) {
				for(EReference rb : refs) {
					if(hasCommonSubClass.get(ra.getEContainingClass(), rb.getEReferenceType())==Boolean.TRUE) {
						return false;
					}
				}
			}
		}
		
		List<EClass> cc = null;
		for(EReference ra : refs) {
			EClass ca = tarSelector.getReferenceClass(ra);
			List<EClass> cca = concreteSubClassMap.get(ca);
			if(cc==null) cc = cca;
			else {
				if(!(cca.containsAll(cc) && cc.containsAll(cca)))
					return false;
				cc = cca;
			}
		}
		return true;
	}
	
	@Deprecated
	protected int srcToTarSimpleCaseAForMin(EReference singleRef, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		System.out.println("[src to tar] in simple case 1: " + srcSelector.allRefs);
		int allSize = srcSelector.firstAvailable.size();
		int i = allSize;
		int mul = srcSelector.getRefLowerBound(singleRef);
		if (mul == 0)
			mul = 1;
		for (; i > 0; i--) {
			Pair<EObject, EReference> pair = srcSelector.firstAvailable.get(i - 1);
			for (int j = 0; j < mul; j++) {
				EObject singleTar = tarSelector.pick();
				setReference(pair.first, pair.second, singleTar);
				srcSelector.consume(pair.first, pair.second);
				tarSelector.consume(singleTar, pair.second);
				forbiddenMap.forbid(pair.first, singleTar, pair.second);// no
																		// multiple
																		// links
			}
		}
		System.out.println("[src to tar] out simple case 1: " + srcSelector.allRefs);
		return allSize;
	}
	
	protected int srcToTarSimpleCaseAForMin(List<EReference> refs, AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector, ForbiddenMap forbiddenMap) {
		System.out.println("[src to tar] in simple case 1: " + srcSelector.allRefs);
		int allSize = srcSelector.firstAvailable.size();
		int i = allSize;
		for (; i > 0; i--) {
			Pair<EObject, EReference> pair = srcSelector.firstAvailable.get(i - 1);
			int mul = srcSelector.getRefLowerBound(pair.second);
			if (mul == 0) mul = 1;
			for (int j = 0; j < mul; j++) {
				EObject singleTar = tarSelector.pick();
				setReference(pair.first, pair.second, singleTar);
				srcSelector.consume(pair.first, pair.second);
				tarSelector.consume(singleTar, pair.second);
				forbiddenMap.forbid(pair.first, singleTar, pair.second);// no
																		// multiple
																		// links
			}
		}
		System.out.println("[src to tar] out simple case 1: " + srcSelector.allRefs);
		return allSize;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setReference(EObject src, EReference ref, EObject tar) {
		if(ref.isMany()) {
//			if(ref.isUnique()) {
//				// where ref.isUnique()==true, add() will check if there is a duplicate element
//				// however, this can be done afterwards
//				this.addToCacheList(src, ref, tar);
//			} else {
				Object eGet = src.eGet(ref);
				((EList)eGet).add(tar);
//			}
		}
		else src.eSet(ref, tar);
	}
	
//	protected OrderedPairMap<EObject, EReference, List<Object>> duplicate = new OrderedPairMap<EObject, EReference, List<Object>>();
//	public void addToCacheList(EObject o, EReference r, Object v) {
//		List<Object> list = getShadowList(o,r);
//		if(list==null) {
//			list = new ArrayList<Object>();
//			duplicate.put(o, r, list);			
//		}
//		list.add(v);
//	}
//	public void removeFromCacheList(EObject o, EReference r, Object v) {
//		List<Object> list = getShadowList(o,r);
//		if(list==null) {
//			return;	
//		}
//		list.remove(v);
//	}
//	
//	public List<Object> getShadowList(EObject o, EReference r) {
//		return duplicate.get(o,r);
//	}
//	public void writeBackShadowList() {
//		duplicate.map.entrySet().stream().forEach(new Consumer<Entry<EObject, ConcurrentHashMap<EReference, List<Object>>>>() {
//			@Override
//			public void accept(Entry<EObject, ConcurrentHashMap<EReference, List<Object>>> t) {
//				for(Entry<EReference, List<Object>> l : t.getValue().entrySet()) {
//					t.getKey().eSet(l.getKey(), l.getValue());
//				}
//			}
//		});
//	}
	
	@SuppressWarnings({ "rawtypes" })
	private void replaceReferenceTar(EObject src, EReference ref, EObject tar, EObject nTar) {
		if(ref.isMany()) {
//			if(ref.isUnique()) {
//				this.removeFromCacheList(src, ref, tar);
//			} else
				((EList)src.eGet(ref)).remove(tar);
		} else {
			src.eSet(ref, null);
		}
		setReference(src, ref, nTar);
	}
	@SuppressWarnings({"rawtypes" })
	private void replaceReferenceSrc(EObject src, EObject nSrc, EReference ref, EObject tar) {
		if(ref.isMany()) {
//			if(ref.isUnique()) {
//				this.removeFromCacheList(src, ref, tar);
//			} else
				((EList)src.eGet(ref)).remove(tar);
		} else {
			src.eSet(ref, null);
		}
		setReference(nSrc, ref, tar);
	}
	
	
	protected void addObjToParent(EClass t, EList<EObject> objs){
		EList<EClass> parents = t.getEAllSuperTypes();
		for(EClass p : parents) {
			addObjToAllObjMap(p, objs);
		}
	}
	
	
	protected void randomGenerateAttributes(EObject o, int[] pair){
		EClass c = o.eClass();
		
		EList<EAttribute> attrs = c.getEAllAttributes();
		for(EAttribute a : attrs){
			randomGenerateAttribute(o,a,pair);
		}
	}
	
	private void randomGenerateAttribute(EObject o, EAttribute a, int[] pair){
		if(a.isChangeable()==false) return;
		if(FeatureMapUtil.isFeatureMapEntry(a.getEType()))
			return;

		Object value = null;
		
		try {
			UniqueValueGenerator ug = null;
			boolean isSpecific = true;
			//TODO ��������Ϊÿ���ඨ�����Ե�ȡֵ��Χ
			String bound = (String) generationConstraint.get(keyManager.getKey(o.eClass(),a));
			if(bound==null) {
				bound = (String) generationConstraint.get(a);
				isSpecific = false;
			}
			
			if(bound!=null) {
				edu.ustb.sei.rmg.random.condition.Condition<?> cd = random.getParsedCondition(bound);
				if(cd.uniqueScope!=null) {
					if(cd.uniqueScope.size()==0) {
						// new unique map
						if(isSpecific) {
							ug = getUniqueMap(null, o.eClass(), a);
						} else {
							ug = getUniqueMap(null, null, a);
						}
					} else {
						throw new UniqueValueScopeGenerationRequest(cd,o,a,pair,bound);
					}
				}
			}
			
			if(ug==null)
				value = randomValue(o, a, bound, pair);
			else value = ug.getValue(o, a, bound, pair, this);
			o.eSet(a, value);
		} catch (ValueLazyGenerationRequest e) {
			addLazyGeneration(o, a, e);
		} catch (UniqueValueScopeGenerationRequest e) {
			addLazyGeneration(o, a, e);
		}
	}
	

	protected OrderedTripleMap<Object,Object,Object,UniqueValueGenerator> uniqueMap = new OrderedTripleMap<Object,Object,Object,UniqueValueGenerator>();
	
	protected UniqueValueGenerator getUniqueMap(Object feature, EClass c, EAttribute a) {
		Object k1 = (feature == null ? UniqueValueGenerator.EMPTY_FEATURE : feature); 
		Object k2 = (c == null ? UniqueValueGenerator.EMPTY_CLASS : c); 
		Object k3 = a;
		
		UniqueValueGenerator g = uniqueMap.get(k1, k2, k3);
		if(g==null) {
			g = new UniqueValueGenerator();
			uniqueMap.put(k1, k2, k3, g);
		}
		//System.out.println("get unique for "+ k1+" "+k2+" "+k3);
		return g;
	}

	protected void addLazyGeneration(EObject o, EAttribute a,
			RuntimeException e) {
		this.lazyGenerations.add(new Triple<EObject, EAttribute, RuntimeException>(o,a,e));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Object randomValue(EObject o, EAttribute a, String condition, int[] pair) {
		EDataType type = a.getEAttributeType();
		if(a.isMany()) {
			if(condition==null) {
				int size = random.randomUIntU(20);
				EList list = new BasicEList(size);
				int[] fixedPair = new int[]{-1,0};
				for(int i=0;i<size;i++){
					list.add(randomPrimitiveValue(type,null,o,a, fixedPair));
				}
				return list;
			} else {
				Object[] value = (Object[])random.randomList(condition);
				EList list = new BasicEList(value.length);
				for(Object f : value) {
					list.add(f);
				}
				return list;
			}
		} else {
			return randomPrimitiveValue(type, condition, o, a,pair);
		}
	}

	protected Object randomPrimitiveValue(EDataType type, String condition, EObject o, EAttribute a, int[] pair) {
		if(type==EcorePackage.eINSTANCE.getEInt() || type==EcorePackage.eINSTANCE.getEIntegerObject() 
				|| type.getInstanceClass() == int.class || type.getInstanceClass() == Integer.class) {
			if(condition==null)
				return random.randomInt();
			else return random.randomInt(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEDouble()||type==EcorePackage.eINSTANCE.getEDoubleObject()
				|| type.getInstanceClass() == double.class || type.getInstanceClass() == Double.class) {
			if(condition==null)
				return random.randomDouble();
			else return random.randomDouble(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEBoolean()||type==EcorePackage.eINSTANCE.getEBooleanObject()
				|| type.getInstanceClass() == boolean.class || type.getInstanceClass() == Boolean.class) {
			if(condition==null)
				return random.randomBoolean();
			else return random.randomBoolean(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEFloat()||type==EcorePackage.eINSTANCE.getEFloatObject()
				|| type.getInstanceClass() == float.class || type.getInstanceClass() == Float.class) {
			if(condition==null)
				return random.randomFloat();
			else return random.randomFloat(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getELong()||type==EcorePackage.eINSTANCE.getELongObject()
				|| type.getInstanceClass() == long.class || type.getInstanceClass() == Long.class) {
			if(condition==null)
				return random.randomLong();
			else return random.randomLong(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEChar() || type==EcorePackage.eINSTANCE.getECharacterObject()
				|| type.getInstanceClass() == char.class || type.getInstanceClass() == Character.class) {
			if(condition==null)
				return random.randomChar();
			else return random.randomChar(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEByte() || type==EcorePackage.eINSTANCE.getEByteObject()
				|| type.getInstanceClass() == byte.class || type.getInstanceClass() == Byte.class) {
			if(condition==null)
				return random.randomByte();
			else return random.randomByte(condition,o,a,true,pair);
		}
		else if(type==EcorePackage.eINSTANCE.getEDate()
				|| type.getInstanceClass() == Date.class) {
			if(condition==null) {
				int year = 1900;
				year = random.randomUInt(1900,2100);
				
				int month = random.randomUInt(1,13);
				int day = random.randomUInt(1,32);
				
				
				Calendar c = Calendar.getInstance();
				c.set(year, month, day);
				
				c.clear(Calendar.MINUTE);
				c.clear(Calendar.SECOND);
				c.clear(Calendar.MILLISECOND);
				
				return c.getTime();
			} else 
				return random.randomDate(condition,o,a,true,pair).getTime();
		}
		else if(type==EcorePackage.eINSTANCE.getEString()
				|| type.getInstanceClass() == String.class) {
			if(condition==null) {
				condition = (String) generationConstraint.get(STRING_ATTRIBUTE_CONDITION);
			}
			return random.randomString(condition,o,a,true,pair);
		}
		else if(type instanceof EEnum) {
			EEnum e = (EEnum)type;
			EList<EEnumLiteral> list = e.getELiterals();
			if(condition==null) {
				if(list.size()==0) 
					return null;
				else {
					return list.get(random.randomInt(0, list.size()-1));
				}
			} else {
				Object v = random.randomEnum(condition,o,a,true,pair);
				if(v instanceof Integer)
					return ((EEnum)type).getEEnumLiteral((Integer)v);
				else if(v instanceof String)
					return ((EEnum)type).getEEnumLiteral((String)v);
				else return list.get(0);
			}
		} else {
			ConsoleFactory.printToConsole("unknown type: "+type.getName(),"RMG",true);
			return null;
		}
	}
	
//	public Object randomEnum(EEnum type) {
//		EList<EEnumLiteral> list = type.getELiterals();
//		if(list.size()==0)
//			return null;
//		EEnumLiteral literal =  list.get(random.randomUIntU(list.size()));
//		return type.getEPackage().getEFactoryInstance()
//				.createFromString(type, literal.getLiteral());
//	}
	
	
	//��objs���ҵ����Է���src.ref��Ԫ��
	private List<EObject> collectAvailableTarObjects(List<EObject> objs, EObject src, EReference ref, 
			AbstractLayeredObjectSelector selector, ForbiddenMap forbiddenMap) {
		List<EObject> result = new ArrayList<EObject>();
		Iterator<EObject> it = forbiddenMap.collectNonForbiddenObjects(src, objs, ref.getEReferenceType());
		
		//for(EObject o : objs) {
			//if(ref.getEReferenceType().isSuperTypeOf(o.eClass())==false) continue;
			//if(forbiddenMap.isForbidden(src, o)) continue;
		while(it.hasNext()){
			EObject o = it.next();
			int[] d = selector.getDegree(o);
			int[] ub = selector.getUpperBound(o.eClass());
			List<EReference> refs = selector.getRefs(o.eClass());
			int id = refs.indexOf(ref);
			if(!(d[id+1]<ub[id+1]||ub[id+1]==-1)) continue;
			//TODO add filter
			result.add(o);
		}
		
		return result;
	}
	
	//��objs���ҵ����Թ���o.ref=tar��Ԫ��
	private List<EObject> collectAvailableSrcObjects(List<EObject> objs, EObject tar, EReference ref, 
			AbstractLayeredObjectSelector selector, ForbiddenMap forbiddenMap) {
		List<EObject> result = new ArrayList<EObject>();
		
		Iterator<EObject> it = forbiddenMap.collectNonForbiddenObjects(objs, tar, ref.getEContainingClass());
		
		//for(EObject o : objs) {
			//if(ref.getEContainingClass().isSuperTypeOf(o.eClass())==false) continue;
			//if(forbiddenMap.isForbidden(o, tar)) continue;
		while(it.hasNext()){
			EObject o = it.next();
			int[] d = selector.getDegree(o);
			int[] ub = selector.getUpperBound(o.eClass());
			List<EReference> refs = selector.getRefs(o.eClass());
			
			int id = refs.indexOf(ref);
			if(!(d[id+1]<ub[id+1]||ub[id+1]==-1)) continue;
			
			result.add(o);
		}
		
		return result;
	}
	
	private List<Quad<EObject, EReference, EObject, List<EObject>>> pickSrcSubstitute(
			EObject o, EReference ref, List<EObject> availableSrcs,
			AbstractLayeredObjectSelector srcSelector,
			AbstractLayeredObjectSelector tarSelector,
			ForbiddenMap forbiddenMap, int limit) {
		List<Quad<EObject, EReference, EObject, List<EObject>>> result = null;
		
		if(tarSelector.getAllRefs().size()==1)
			return Collections.emptyList();
		if(tarSelector.getAllRefs().stream().allMatch(r->
			r==ref || hasCommonSubClass.get(ref.getEReferenceType(), r.getEReferenceType())==null
		)) return Collections.emptyList();
		
//		for(EObject x : tarSelector.allObjs) {
		result = tarSelector.allObjs.parallelStream().collect(
				()->new ArrayList<Quad<EObject, EReference, EObject, List<EObject>>>(),
				(list,x)->{
					List<EReference> refs = tarSelector.getRefs(x.eClass());
//					System.out.println("size of refs "+refs.size());
					for(EReference y : refs) {
						if(y==ref || hasCommonSubClass.get(ref.getEContainingClass(), y.getEContainingClass())==null) {
							continue;
						}
						
						// ��availableSrcs���������ܹ�����x.|y|�ĵ�
						// x.|y|���������ܹ���o.|ref|�ĵ�
						//long begin1 = Calendar.getInstance().getTimeInMillis();
						//EList<EObject> freeze = getReverse(x, y, srcSelector.allObjs);
						EList<EObject> freeze = getReverse(x, y, this.allObjMap.get(y.getEContainingClass()));
//						long end1 = Calendar.getInstance().getTimeInMillis();
//						System.out.println("Time1:"+(end1-begin1));
						
//						System.out.println("get reverse "+freeze.size());
//						long begin2 = Calendar.getInstance().getTimeInMillis();
						//��freeze���ҵ�������Ϊ o.|ref|Դ��Ԫ��, ��Ȼy.eContainingClass��ref.eContainingClassû�й������࣬�򲻻����������������
						List<EObject> A = collectAvailableSrcObjects(freeze, o, ref, srcSelector, forbiddenMap);
//						long end2 = Calendar.getInstance().getTimeInMillis();
//						System.out.println("Time2:"+(end2-begin2));
						
						if(A.size()==0)
							continue;
						
//						long begin3 = Calendar.getInstance().getTimeInMillis();
						List<EObject> B = collectAvailableSrcObjects(availableSrcs, x, y, srcSelector, forbiddenMap);
//						long end3 = Calendar.getInstance().getTimeInMillis();
//						System.out.println("Time3:"+(end3-begin3));
						if(B.size()==0)
							continue;
						
						for(EObject a : A) {
							list.add(new Quad<EObject, EReference, EObject, List<EObject>>(x,y,a,B));
						}
					}
				},
				(left,right)->left.addAll(right));
		
		return result;
	}
	

	@SuppressWarnings("unchecked")
	private EList<EObject> getReverse(EObject tar, EReference ref, EList<EObject> scope) {
		EList<EObject> result = new HashableBasicEList<EObject>();
		if(ref.isContainment()) {
			EObject container = tar.eContainer();
			if(container!=null)
				result.add(container);
			return result;
		} else {
			// 这个地方说明需要对模型进行封装，缓存逆向关系
			// 可以考虑将模型封装成图，这样所有根据类型取元素、元模型信息都可以封装进去
			boolean flag = (ref.getUpperBound()==1) ;
			
			EList<EObject> filterScope = filterScope(scope, ref);
			
			for(EObject o : filterScope) {			
				if(flag) {
					if(o.eGet(ref)==tar)
						result.add(o);
				} else {
					if(((EList<EObject>)o.eGet(ref)).contains(tar))
						result.add(o);
				}
			}
			return result;
		}
	}
	
	private OrderedPairMap<Object,EReference,EList<EObject>> filterScopeCache = new OrderedPairMap<Object,EReference,EList<EObject>>();
	private EList<EObject> filterScope(EList<EObject> scope, EReference ref) {
		EList<EObject> obj = filterScopeCache.get(scope,ref);
		if(obj==null) {
			obj = new HashableBasicEList<EObject>(scope.size());
			EClass srcType = ref.getEContainingClass();
			for(EObject o : scope) {
				EClass oType = o.eClass();
				if(srcType.isSuperTypeOf(oType))
					obj.add(o);
			}
			
			filterScopeCache.put(scope, ref, obj);
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	private List<Quad<EObject, EReference, EObject, List<EObject>>> pickTarSubstitute(EObject o,
			EReference ref, List<EObject> availableTars,
			AbstractLayeredObjectSelector srcSelector, AbstractLayeredObjectSelector tarSelector, 
			ForbiddenMap forbiddenMap, int limit) {
		// TODO Auto-generated method stub
		List<Quad<EObject, EReference, EObject, List<EObject>>> result = null;
		
		if(srcSelector.getAllRefs().size()==1) //�϶�û�п����滻��Ԫ�� 
			return Collections.emptyList();
		if(srcSelector.getAllRefs().stream().allMatch(r->
		r==ref || hasCommonSubClass.get(ref.getEReferenceType(), r.getEReferenceType())==null
				)) return Collections.emptyList();
		
//		for(EObject x : srcSelector.allObjs) {
		result = srcSelector.allObjs.parallelStream().collect(
				()->new ArrayList<Quad<EObject, EReference, EObject, List<EObject>>>(),
				(list,x)->{
					List<EReference> refs = srcSelector.getRefs(x.eClass());
					for(EReference y : refs) {
						
						if(y==ref || hasCommonSubClass.get(ref.getEReferenceType(), y.getEReferenceType())==null) {
							//TODO ���ref == y����ô�Ƿ����ҵ��أ�
							// ֤���������ҵ������_.ref = w ���Է��� _.y����ô��֮ǰ�������п϶������ҵ�w��Ϊ_.y�ĺ�ѡ
							continue;
						}
						//����availableTars���ҵ����п��Է���x.y�ĵ�, ��������, ������, ì����
						//��x.y���ҵ����п��Է���o.ref�еĵ�, ��������, ������, ì����
						
						EList<EObject> freeze = null;
						if(y.getUpperBound()==1) {
							EObject eGet = (EObject) x.eGet(y);
							freeze = new HashableBasicEList<EObject>();
							if(eGet!=null)
								freeze.add(eGet);
						} else {
							freeze = (EList<EObject>) x.eGet(y);
							if(freeze==null)
								freeze = new HashableBasicEList<EObject>();
						}
						// bug, 当o~>x（或x=o）时，不能找到正确的候选元素。在此情况下，我们将freeze中的元素从x移动到o，这样不会破坏有序性，
						// 但是目前的程序会判定失败，因为此时o到freeze已经存在通路，该通路是为了防止重复创建关系而建立的
						// 原因是当前的forbiddenMap无法判断边的产生原因（因为序关系或者是幂等律）
						// 替换操作不会破坏幂等律，所以需要检测是否会破坏有序性即可
						List<EObject> A = collectAvailableTarObjects(freeze, o, ref, tarSelector, forbiddenMap);
						if(A.size()==0)
							continue;
						
						List<EObject> B = collectAvailableTarObjects(availableTars, x, y, tarSelector, forbiddenMap);
						if(B.size()==0)
							continue;
						
						for(EObject a : A) {
							list.add(new Quad<EObject, EReference, EObject, List<EObject>>(x,y,a,B));
						}
					}
				},
				(left,right)->left.addAll(right));
//		}
		
		return result;
	}
}


class UniqueValueGenerator {
	private HashSet<Object> valueSet = new HashSet<Object>();
	static final private Object NULL_VALUE = new Object[0];
	static final private int MAX_TEST = 3;
	public static final Object EMPTY_FEATURE = new Object[0];
	public static final Object EMPTY_CLASS = new Object[0];
	
	public Object getValue(EObject o, EAttribute a, String condition, int[] pair, Generator g) {
		Object v = null;
		for(int i=0;i<MAX_TEST;i++){
			v = g.randomValue(o, a, condition, pair);
			if(v==null) v = NULL_VALUE;
			if(valueSet.contains(v)) continue;
			else {
				//System.out.println("find unique");
				valueSet.add(v);
				return v;
			}
		}
		ConsoleFactory.printToConsole("Unique Value Generator has tested "+MAX_TEST+" times!", "RMG", true);
		return v;
	}

}