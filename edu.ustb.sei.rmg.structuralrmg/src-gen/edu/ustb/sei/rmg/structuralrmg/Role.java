/**
 */
package edu.ustb.sei.rmg.structuralrmg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends NamedElement {
} // Role
