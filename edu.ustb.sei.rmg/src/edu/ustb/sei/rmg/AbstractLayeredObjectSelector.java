package edu.ustb.sei.rmg;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.jface.util.Assert;

import edu.ustb.sei.rmg.proxy.EListProxy;
import edu.ustb.sei.rmg.random.RandomUtil;
import edu.ustb.sei.rmg.util.FastArrayList;
import edu.ustb.sei.rmg.util.LinkedEList;
import edu.ustb.sei.rmg.util.OrderedPairMap;
import edu.ustb.sei.rmg.util.OrderedTripleMap;
import edu.ustb.sei.rmg.util.Pair;

abstract class AbstractLayeredObjectSelector {
	protected static OrderedTripleMap<Class<?>,EList<EReference>, EClass, List<EReference>> refsOfObj = 
			new OrderedTripleMap<Class<?>,EList<EReference>, EClass, List<EReference>>();
	
	protected static OrderedPairMap<AbstractLayeredObjectSelector, EObject, int[]> objDegree = new OrderedPairMap<AbstractLayeredObjectSelector, EObject, int[]>();
	
	protected static OrderedTripleMap<Class<?>, EList<EReference>, EClass, Boolean> clsCase = new OrderedTripleMap<Class<?>, EList<EReference>, EClass, Boolean>();
	
	protected static OrderedTripleMap<Class<?>, EList<EReference>, EClass, int[]> lowerBoundCache = new
			OrderedTripleMap<Class<?>,EList<EReference>, EClass, int[]>();
    protected static OrderedTripleMap<Class<?>,EList<EReference>, EClass, int[]> upperBoundCache = new 
    		OrderedTripleMap<Class<?>,EList<EReference>, EClass, int[]>();
	
    protected FastArrayList<EObject> first;
    //protected ReadWriteLock firstLock;
    protected FastArrayList<Pair<EObject, EReference>> firstAvailable;
    //protected ReadWriteLock firstAvailableLock;
    
	protected FastArrayList<EObject> second;
	//protected ReadWriteLock secondLock;
	protected FastArrayList<Pair<EObject, EReference>> secondAvailable;
	//protected ReadWriteLock secondAvailableLock;
	
	
	protected List<EObject> third;
	//protected ReadWriteLock thirdLock;
	
	protected EList<EObject> allObjs;
	protected EList<EReference> allRefs;
	
	protected HashMap<Object,Boolean> disjoinMap = new HashMap<Object,Boolean>();
	
	public boolean testDisjoin(Collection<EReference> col) {
		Boolean f = disjoinMap.get(col);
		if(f==null) {
			f = Collections.disjoint(col, allRefs);
			disjoinMap.put(col, f);
		}
		return f;
	}
	
	final public EList<EReference> getAllRefs() {
		return allRefs;
	}
	
	
	//protected OrderedPairEMap<EObject, EReference, Boolean> unhandledObjs = new OrderedPairEMap<EObject, EReference, Boolean>();
		
	protected Generator host;
	protected Class<?> currentClass = null;
	protected boolean required;
	protected boolean unique;
	
	
	public static final int SEARCH_SCOPE_FIRST = 0x1;
	public static final int SEARCH_SCOPE_SECOND = 0x2;
	public static final int SEARCH_SCOPE_THIRD = 0x4;
	public static final int SEARCH_SCOPE_TILL_SECOND = 0x3;
	public static final int SEARCH_SCOPE_ALL = 0x7;
	
	public AbstractLayeredObjectSelector(Generator host, EList<EObject> allObjs, EList<EReference> refs,
			boolean required, boolean unique){
		this.host = host;
		this.allObjs = allObjs;
		this.allRefs = refs;
		
		this.required = required;
		this.unique = unique;
		
		first = new FastArrayList<EObject>(allObjs.size());
		firstAvailable = new FastArrayList<Pair<EObject, EReference>>(allObjs.size()*refs.size()/2);
		//firstLock = new ReentrantReadWriteLock();
		//firstAvailableLock = new ReentrantReadWriteLock();
		
		
		second = new FastArrayList<EObject>(allObjs.size());
		secondAvailable = new FastArrayList<Pair<EObject, EReference>>(allObjs.size()*refs.size()/2);
		//secondLock = new ReentrantReadWriteLock();
		//secondAvailableLock = new ReentrantReadWriteLock();
		
		third = new ArrayList<EObject>(allObjs.size());
		
//		first = (EList<EObject>)Proxy.newProxyInstance(EList.class.getClassLoader(),new Class<?>[]{EList.class}, new EListProxy(new BasicEList<EObject>(allObjs.size())));
//		firstAvailable = (EList<Pair<EObject, EReference>>)Proxy.newProxyInstance(EList.class.getClassLoader(),new Class<?>[]{EList.class}, new EListProxy(new BasicEList<Pair<EObject, EReference>>(allObjs.size()*refs.size()/2)));
//		second = (EList<EObject>)Proxy.newProxyInstance(EList.class.getClassLoader(),new Class<?>[]{EList.class}, new EListProxy(new BasicEList<EObject>(allObjs.size())));
//		secondAvailable = (EList<Pair<EObject, EReference>>)Proxy.newProxyInstance(EList.class.getClassLoader(),new Class<?>[]{EList.class}, new EListProxy(new BasicEList<Pair<EObject, EReference>>(allObjs.size()*refs.size()/2)));
//		third = (EList<EObject>)Proxy.newProxyInstance(EList.class.getClassLoader(),new Class<?>[]{EList.class}, new EListProxy(new BasicEList<EObject>(allObjs.size())));
//
		
		//thirdLock = new ReentrantReadWriteLock();
	}
	
	public abstract EObject pick();
	
	public void putUnhandledObj(EObject o, EReference r, boolean fillMin) {
		if(fillMin){
			firstAvailable.remove(new Pair<EObject, EReference>(o,r));
		} else {
			secondAvailable.remove(new Pair<EObject, EReference>(o,r));
		}
		//unhandledObjs.put(o, r, true);
	}
	
//	public void resetUnhandledObjs() {
//		unhandledObjs.clear();
//	}
	
	public void release(EObject o, EReference ref) {
		int[] lb = getLowerBound(o.eClass());
		int[] ub = getUpperBound(o.eClass());
		int[] d = getDegree(o);
		List<EReference> refs = getRefs(o.eClass());
		boolean special = getClassCase(o);
		
		//Pair<EObject, EReference> p = new Pair<EObject,EReference>(o,ref);
		//boolean special = getClassCase(o,refs);
		
		
		int id = refs.indexOf(ref);
		
		if(id>=0) {
			d[0]--;
			d[id+1]--;
			
			if(d[0]==lb[0]-1) {//drop from the second
				removeFromSecond(o);
				clearAvailable(o,secondAvailable);
				addToFirst(o);
				appendAvailable(o,refs,d,ub,firstAvailable,false);
			} else if(d[0]<lb[0]) {
				if(d[id+1]<lb[id+1] || (special && d[id+1]==0 && lb[id+1]==0))
					firstAvailable.add(new Pair<EObject,EReference>(o,ref));	
			} 
			
			if(d[0]==ub[0]-1) {//drop from the third? not impossible
				removeFromThird(o);
				addToSecond(o);
				appendAvailable(o,refs,d,ub,secondAvailable,false);
			} else if(d[0]<ub[0]) {
				if(d[id+1]<ub[id+1])
					secondAvailable.add(new Pair<EObject,EReference>(o,ref));	
			}
		}
	}
	
	private void removeFromThird(EObject o) {
		this.third.remove(o);
	}
	
	
	public void consume(EObject o, EReference ref) {
		// TODO Auto-generated method stub
		int[] lb = getLowerBound(o.eClass());
		int[] ub = getUpperBound(o.eClass());
		int[] d = getDegree(o);
		List<EReference> refs = getRefs(o.eClass());
		boolean special = getClassCase(o);
		
		Pair<EObject, EReference> p = new Pair<EObject,EReference>(o,ref);
		//boolean special = getClassCase(o,refs);
		
		
		int id = refs.indexOf(ref);
		
		if(id>=0) {
			d[0]++;
			d[id+1]++;
			
			if(d[0]==lb[0]) {
				removeFromFirst(o);
				clearAvailable(o,firstAvailable);
				
				if(d[0]<ub[0]){
					addToSecond(o);
					appendAvailable(o,refs,d,ub,secondAvailable,false);
				}
				else 
					addToThird(o);
			}
			
			if(d[0]==ub[0]) {
				removeFromSecond(o);
				clearAvailable(o,secondAvailable);
				addToThird(o);
			}
			
			if(d[0]<lb[0]) {//锟斤拷锟节碉拷一锟斤拷
				if((!special && d[id+1]>=lb[id+1]) || (special && d[id+1]>lb[id+1]))
					firstAvailable.remove(p);
			} else if(d[0]<ub[0]){
				if(d[id+1]>=ub[id+1])
					secondAvailable.remove(p);
			}
			
			
		} else 
			assert false;
		return;
	}

	public boolean removeFromSecond(EObject o) {
		//secondLock.writeLock().lock();
		boolean b = second.remove(o);
		//secondLock.writeLock().unlock();
		return b;
	}

	public boolean addToSecond(EObject o) {
		//secondLock.writeLock().lock();
		boolean b = second.add(o);
		//secondLock.writeLock().unlock();
		
		return b;
	}

	public boolean removeFromFirst(EObject o) {
		//firstLock.writeLock().lock();
		boolean b = first.remove(o);
		//firstLock.writeLock().unlock();
		return b;
	}
	
	public boolean addToFirst(EObject o) {
		//firstLock.writeLock().lock();
		boolean b = first.add(o);
		//firstLock.writeLock().unlock();
		return b;
	}

	public boolean addToThird(EObject o) {
		//thirdLock.writeLock().lock();
		boolean b = third.add(o);
		//thirdLock.writeLock().unlock();
		return b;
	}
	
	private void appendAvailable(EObject o, List<EReference> refs, int[] d, int[] b,
			List<Pair<EObject, EReference>> list, boolean special) {
		for(int i=1;i<d.length;i++){
			if(d[i]<b[i] || (special && d[i]==0 && b[i] == 0)) {
				//synchronized(list) {//It means that this synchronized statement can be removed. 2015/5/22
				list.add(new Pair<EObject,EReference>(o,refs.get(i-1)));
				//}
			}
		}
	}

	private void clearAvailable(EObject o, FastArrayList<Pair<EObject,EReference>> list) {
		if(allRefs.size()<200) {
			for(EReference r : getAllRefs()) {
				list.remove(new Pair<EObject,EReference>(o,r));
			}
		} else {
			int i =list.size()-1;
			for(;i>=0;i--) {
				Pair<EObject, EReference> p = list.get(i);
				if(p.first==o)
					list.remove(i);
			}		
		}
	}
	
	
	protected List<EReference> getRefs(EClass c) {
		List<EReference> result = refsOfObj.get(currentClass, allRefs, c);
		if(result!=null) return result;
		result = new ArrayList<EReference>();
		refsOfObj.put(currentClass, allRefs,c,result);
		for(EReference r : allRefs) {
			if(getReferenceClass(r).isSuperTypeOf(c))
				result.add(r);
		}
		
		return result;
	}
	
	public void consume(EObject o, EReference oldRef, EReference newRef) {
		int[] d = getDegree(o);
		int[] lb = getLowerBound(o.eClass());
		int[] ub = getUpperBound(o.eClass());
		List<EReference> refs = getRefs(o.eClass());
		boolean special = getClassCase(o.eClass());
		
		int oid = refs.indexOf(oldRef);
		int nid = refs.indexOf(newRef);
		
		d[oid+1]--;
		d[nid+1]++;
		
		if(d[0]<lb[0]) {
			if((!special && d[nid+1]>=lb[nid+1]) || (special && d[nid+1] > lb[nid+1]))
				firstAvailable.remove(new Pair<EObject,EReference>(o,newRef));
			if(d[oid+1]<lb[oid+1] || (special && d[oid+1]==0 && lb[oid+1]==0))
				firstAvailable.add(new Pair<EObject,EReference>(o,oldRef));	
		} 
		else if(d[0]<ub[0]) {
			if(d[nid+1]>=ub[nid+1])
				secondAvailable.remove(new Pair<EObject,EReference>(o,newRef));
			if(d[oid+1]<ub[oid+1])
				secondAvailable.add(new Pair<EObject,EReference>(o,oldRef));	
		}
	}
//	
//	private void concurrentPartition() {
//		PartionTask p = new PartionTask(this,0,allObjs.size());
//		ForkJoinPool fjp = new ForkJoinPool();
//		Future<Void> f = fjp.submit(p);
//		
//		try {
//			f.get();
//		} catch (InterruptedException | ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		fjp.shutdown();
//	}
//	
//	class PartionTask extends RecursiveAction {
//		private int start;
//		private int end;
//		private AbstractLayeredObjectSelector selector;
//		
//		public PartionTask(AbstractLayeredObjectSelector selector, 
//				int start, int end) {
//			this.start = start;
//			this.end = end;
//			this.selector = selector;
//		}
//
//		@Override
//		protected void compute() {
//			// TODO Auto-generated method stub
//			recursivePartition();
//		}
//		
//		public void sequentialPartition() {
//			partition(selector, start, end);
//		}
//		
//		private void recursivePartition() {
//			if(end-start<2000)
//				sequentialPartition();
//			else {
//				int mid = (end+start)/2;
//				PartionTask left = new PartionTask(selector, start, mid);
//				PartionTask right = new PartionTask(selector, mid, end);
//				left.fork();
//				right.fork();
//				
//				left.join();
//				right.join();
//			}
//			
//		}
//	}
	
	private static void partition(AbstractLayeredObjectSelector selector, int start, int end) {
		for(int i=start ; i<end; i++) {
			EObject o = selector.allObjs.get(i);
			
		    int[] lb = selector.getLowerBound(o.eClass());
			int[] ub = selector.getUpperBound(o.eClass());
			int[] d = new int[lb.length];
			Arrays.fill(d, 0);
			objDegree.put(selector, o, d);
			
			List<EReference> refs = selector.getRefs(o.eClass());

			if(lb[0]==-1) {//没锟叫癸拷锟斤拷
				selector.addToThird(o);
			}
			else if(lb[0]==0){
				selector.addToSecond(o);
				selector.appendAvailable(o,refs, d,ub, selector.secondAvailable, false);
			}
			else {
				selector.addToFirst(o);
				selector.appendAvailable(o,refs, d,lb, selector.firstAvailable, selector.getClassCase(o));
			}
		}
	}
	
	

	protected void partition() {
//		for(EObject o : allObjs) {
//			int[] lb = getLowerBound(o.eClass());
//			int[] ub = getUpperBound(o.eClass());
//			int[] d = new int[lb.length];
//			Arrays.fill(d, 0);
//			objDegree.put(this, o, d);
//			EList<EReference> refs = getRefs(o.eClass());
//
//			if(lb[0]==-1)//没锟叫癸拷锟斤拷
//				third.add(o);
//			else if(lb[0]==0){
//				second.add(o);
//				appendAvailable(o,refs, d,ub, secondAvailable, false);
//			}
//			else {
//				first.add(o);
//				appendAvailable(o,refs, d,lb, firstAvailable, getClassCase(o));
//			}
//		}
		
		partition(this, 0 , allObjs.size());
		
		//concurrentPartition();
		//System.out.println(first);
	}



	protected boolean getClassCase(EObject o) {
		Boolean b = clsCase.get(currentClass, allRefs, o.eClass());
		return b!=null && b==true;
	}
	
	protected abstract int getRefLowerBound(EReference r);
	
	protected abstract int getRefUpperBound(EReference r);

	
	public List<EObject> getAvailableObjects() {
		List<EObject> available = new ArrayList<EObject>(first.size()+second.size());
		available.addAll(first);
		available.addAll(second);
		return available;
	}

	protected int[] getDegree(EObject o) {
		return objDegree.get(this, o);
	}

	protected int[] getLowerBound(EClass c) {
		int[] b = lowerBoundCache.get(currentClass, allRefs, c);
		if(b!=null) return b;
		List<EReference> objRefs = getRefs(c);
		b = new int[objRefs.size()+1];
		
		if(objRefs.size()==0)
			b[0]=-1;
		else {
			b[0]=0;
			for(int i=0;i<objRefs.size();i++){
				EReference r = objRefs.get(i);
				int lowerBound = getRefLowerBound(r);
				b[0]+=lowerBound;
				b[i+1] = lowerBound;
			}
		}
		if((required && b[0]==0)||(unique&&b[0]>1)){
			b[0]=1;//如果将来要支持总体的多重性，可以在这里扩展
			clsCase.put(currentClass, allRefs, c, true);
		} else {			
			clsCase.put(currentClass, allRefs, c, false);
		}
		
		lowerBoundCache.put(currentClass, allRefs,c,b);
		
		return b;//b[0]锟斤拷锟狡讹拷锟斤拷锟角凤拷锟杰憋拷选锟斤拷, b[1]...b[n]锟斤拷锟狡憋拷锟角凤拷锟杰憋拷选锟斤拷
	}

	protected int[] getUpperBound(EClass c) {
		int[] b = upperBoundCache.get(currentClass, allRefs, c);
		if(b!=null) return b;
		List<EReference> objRefs = getRefs(c);
		b = new int[objRefs.size()+1];
		b[0] = 0;
		for(int i=0;i<objRefs.size();i++){
			EReference r = objRefs.get(i);
			int upperBound = getRefUpperBound(r);
			if(upperBound<0) 
				upperBound = Integer.MAX_VALUE;
			
			if(upperBound == Integer.MAX_VALUE 
					|| b[0]==Integer.MAX_VALUE) {
				b[0]=Integer.MAX_VALUE;
			} else {
				b[0]+=upperBound;
			}
			b[i+1] = upperBound;
		}
		
		if(unique) b[0] = 1;//锟斤拷锟斤拷锟絬nique, 锟斤拷么锟斤拷锟斤拷锟斤拷锟斤拷为1
		
		upperBoundCache.put(currentClass, allRefs,c,b);
		
		return b;//b[0]锟斤拷锟狡讹拷锟斤拷锟角凤拷锟杰憋拷选锟斤拷, b[1]...b[n]锟斤拷锟狡憋拷锟角凤拷锟杰憋拷选锟斤拷
	}

	/**
	 * 选取一锟斤拷锟斤拷锟揭伙拷锟斤拷锟� 锟斤拷锟节癸拷锟斤拷锟斤拷
	 * @param scope
	 * @return
	 * @throws Exception
	 */
	public Pair<EObject, EReference> pickObjectAndReference(int scope) {
		
		
		if(((scope&SEARCH_SCOPE_FIRST)!=0) 
				&& first.size()!=0 &&firstAvailable.size()!=0){//锟斤拷firstLayer锟斤拷锟斤拷锟斤拷元锟斤拷, 锟斤拷锟斤拷选锟斤拷锟斤拷锟斤拷锟斤拷元锟斤拷
//			int id = ri % first.size();
//			
//			EObject o = first.get(id);
//			EList<EReference> refs = getRefs(o.eClass());
//			int[] degree = getDegree(o);
//			int[] bounds = ((scope&SEARCH_SCOPE_SECOND)!=0) ? 
//					getLowerBound(o.eClass()) : getUpperBound(o.eClass());
//			int availableCount = 0;
//			
//			boolean special = getClassCase(o, refs);
//			if(!special) {
//				//锟斤拷锟斤拷锟斤拷通锟斤拷锟斤拷锟斤拷屑锟斤拷锟�
//				for(int i=1;i<degree.length;i++)
//					if(degree[i]<bounds[i]&&unhandledObjs.get(o)==null) availableCount++;
//			} else {
//				//锟斤拷锟睫讹拷锟斤拷0, 一锟斤拷锟斤拷说锟斤拷锟斤拷锟斤拷毛锟斤拷锟斤拷
//				//锟斤拷锟斤拷锟斤拷锟轿ㄒ伙拷目锟斤拷芫锟斤拷锟絩equired=true, d[0]=0  b[i]=0, 锟斤拷锟斤拷锟斤拷锟揭伙拷锟斤拷锟斤拷锟斤拷锟�
//				//require=false时, 锟斤拷锟斤拷同锟斤拷通锟斤拷锟�
//				//require=true时, 锟斤拷锟斤拷锟斤拷锟叫碉拷锟斤拷锟睫讹拷锟斤拷0, b[0]也锟斤拷锟斤拷1
//				//firstSize锟叫碉拷元锟斤拷一锟斤拷锟斤拷d[0]<b[0]
//				for(int i=1;i<degree.length;i++)
//					if(bounds[i]==0&&unhandledObjs.get(o)==null) availableCount++;
//			}
//			
//			int test = host.randomUIntU(availableCount);
//			
//			for(int i=1;i<degree.length;i++) {
//				if((degree[i]<bounds[i]|| (special&&bounds[i]==0))
//						&&unhandledObjs.get(o)==null) {
//					test--;
//					if(test==0)
//						return new Pair<EObject, EReference>(o,refs.get(i-1));
//				}
//			}
			int ri = host.random.randomUInt();
			int id = ri % firstAvailable.size();
			return firstAvailable.get(id);
		} 
		
		if(((scope&SEARCH_SCOPE_SECOND)!=0)
				&& second.size()!=0 && secondAvailable.size()!=0) {//锟斤拷firstLayer锟斤拷锟斤拷锟斤拷元锟斤拷, 锟斤拷secondLayer锟斤拷选锟斤拷
//			int id = ri % second.size();
//			EObject o = second.get(id);
//			EList<EReference> refs = getRefs(o.eClass());
//			int[] d = getDegree(o);
//			int[] ub = getUpperBound(o.eClass());
//			int availableCount = 0;
//			
//			for(int i=1;i<d.length;i++)
//				if((d[i]<ub[i]||ub[i]==-1)
//						&&unhandledObjs.get(o)==null) availableCount++;
//			int test = host.randomUIntU(availableCount);
//			
//			
//			for(int i=1;i<d.length;i++)
//				if((d[i]<ub[i] || ub[i]==-1)
//						&&unhandledObjs.get(o)==null) {
//					test--;
//					if(test==0)
//						return new Pair<EObject, EReference>(o,refs.get(i-1));
//				}
//			throw new Exception("SecondLayer锟叫达拷锟斤拷一锟斤拷锟皆达拷锟斤拷");
			
			int ri = host.random.randomUInt();
			int id = ri % secondAvailable.size();
			return secondAvailable.get(id);
		} 
	
		if((scope&SEARCH_SCOPE_THIRD)!=0){ //锟斤拷锟斤拷锟斤拷, 锟斤拷thridLayer锟斤拷选锟斤拷, 锟斤拷锟斤拷锟斤拷锟斤拷锟狡伙拷模锟酵的结构约锟斤拷
			if(((scope&SEARCH_SCOPE_THIRD)!=0)) { 
				EObject o = third.get(host.random.randomUIntU(third.size()));
				List<EReference> refs = getRefs(o.eClass());
				EReference r = refs.get(host.random.randomUIntU(refs.size()));
				
				return new Pair<EObject, EReference>(o,r);
			} else {
				return null;//锟揭诧拷锟斤拷元锟斤拷锟斤拷!
			}
		}
		
		return null;
	}
	
	public abstract EClass getReferenceClass(EReference r);
	
//	class CollectAvailableTask extends RecursiveTask<EList<EObject>> {
//
//		private int start;
//		public CollectAvailableTask(AbstractLayeredObjectSelector selector,
//				boolean first, int start, int end, EObject obj, EReference ref,
//				ForbiddenMap forbiddenMap) {
//			super();
//			this.selector = selector;
//			this.first = first;
//			this.start = start;
//			this.end = end;
//			this.obj = obj;
//			this.ref = ref;
//			this.forbiddenMap = forbiddenMap;
//		}
//
//
//		private int end;
//		private AbstractLayeredObjectSelector selector;
//		private boolean first;
//		private EObject obj;
//		private EReference ref;
//		private ForbiddenMap forbiddenMap;
//		
//		private int id = 0;
//		
//		
//		@Override
//		protected EList<EObject> compute() {
//			// TODO Auto-generated method stub
//			return concurrentCollect();
//		}
//		
//		private EList<EObject> sequentialCollect() {
//			if(first)
//				return collectFirstAvailable(selector, obj, ref, forbiddenMap, start, end);
//			else
//				return collectSecondAvailable(selector, obj, ref, forbiddenMap, start, end);
//		}
//		private EList<EObject> concurrentCollect() {
//			
//			if((end-start)<500)
//				return sequentialCollect();
//			else {
//				//LinkedEList<EObject> result = null;
//				int mid = (end+start)/2;
//				
//				System.out.println("split:"+id+"=>end:"+end+", mid:"+mid+", start:"+start);
//				
//				CollectAvailableTask left = new CollectAvailableTask(selector, first, start, 
//						mid, obj, ref, forbiddenMap);
//				left.id = id +1;
//				
//				CollectAvailableTask right = new CollectAvailableTask(selector, first, mid, 
//						end, obj, ref, forbiddenMap);
//				right.id = id +1;
//				
//				left.fork();
//				right.fork();
//				
//				
//				EList<EObject> lr = left.join();
//				EList<EObject> rr = right.join();
//				
//				//System.out.println("join");
//				//result = new LinkedEList<EObject>();
////				result.addAll(lr);
////				result.addAll(rr);
//				lr.addAll(rr);
//				
//				return lr;
//			}
//		}
//		
//	}
	
	protected abstract Iterator<EObject> checkForbiddenConditionForPickObject(EObject opposite, EClass typeFilter, List<EObject> candidate, ForbiddenMap forbiddenMap);
	
	static List<EObject> collectFirstAvailable(AbstractLayeredObjectSelector selector, EObject opposite, EReference ref,
			ForbiddenMap forbiddenMap, int start, int end, int size) {
		
		ArrayList<EObject> availableObjs = new ArrayList<EObject>(selector.first.size());
		
		Iterator<EObject> it = selector.checkForbiddenConditionForPickObject(opposite, selector.getReferenceClass(ref), selector.first, forbiddenMap);
		
		while(it.hasNext()) {
			//EObject o = selector.first.get(i);
			if(size==availableObjs.size()) return availableObjs;
			
			EObject o = it.next();
			
//			if(selector.getReferenceClass(ref).isSuperTypeOf(o.eClass()))
			{
//				if(selector.checkForbiddenConditionForPickObject(opposite, o, forbiddenMap)) 
				{
					boolean special = selector.getClassCase(o);
					
					List<EReference> refs = selector.getRefs(o.eClass());
					int id = refs.indexOf(ref);
					int[] lb = selector.getLowerBound(o.eClass());
					int[] d = selector.getDegree(o);
					
					if(!special) {
						if(d[id+1]<lb[id+1])
							availableObjs.add(o);
					} else {
						if(d[id+1]==0)
							availableObjs.add(o);
					}
				}
			}
			//else reject by forbidden map
		}
		return availableObjs;
	}
	
	static List<EObject> collectSecondAvailable(AbstractLayeredObjectSelector selector, EObject opposite, EReference ref,
			ForbiddenMap forbiddenMap, int start, int end, int size) {
//		LinkedEList<EObject> availableObjs = new LinkedEList<EObject>();
		ArrayList<EObject> availableObjs = new ArrayList<EObject>(size);
		
		//long timeUsed = 0;
		Iterator<EObject> it = selector.checkForbiddenConditionForPickObject(opposite,selector.getReferenceClass(ref), selector.second, forbiddenMap);
		
		while(it.hasNext()) {
			//EObject o = selector.second.get(i);
			if(size==availableObjs.size()) return availableObjs;
			
			EObject o = it.next();
			
//				if(selector.getReferenceClass(ref).isSuperTypeOf(o.eClass())) 
				{
					//long begin = Calendar.getInstance().getTimeInMillis();
					//boolean checkForbiddenConditionForPickObject = selector.checkForbiddenConditionForPickObject(opposite, o, forbiddenMap);
					//long last = Calendar.getInstance().getTimeInMillis();
					//timeUsed+=(last-begin);

					//if(checkForbiddenConditionForPickObject) 
					{
						List<EReference> refs = selector.getRefs(o.eClass());
						int id = refs.indexOf(ref);
						int[] ub = selector.getUpperBound(o.eClass());
						int[] d = selector.getDegree(o);
						
						if(d[id+1]<ub[id+1]||ub[id+1]==-1)
							availableObjs.add(o);
					}
				}
		}
		//System.out.println("size:"+selector.second.size()+", collect second available time: "+timeUsed);
		
		return availableObjs;
	}
	
//	private EList<EObject> concurrentCollect(boolean first, EObject o, 
//			EReference ref, ForbiddenMap forbiddenMap) {
//		int size = first ? this.first.size() : this.second.size();
//		
//		CollectAvailableTask p = new CollectAvailableTask(this, first, 0, size, o, ref, forbiddenMap);
//		EList<EObject> r = null;
//		
//		ForkJoinPool fjp = new ForkJoinPool();
//		Future<EList<EObject>> result = fjp.submit(p);
//		try {
//			r = result.get();
//		} catch (InterruptedException | ExecutionException e) {
//			e.printStackTrace();
//		}
//		fjp.shutdown();
//		return r;
//	}
	
	private List<Integer> singleton = new ArrayList<Integer>();

	public List<EObject> pickObjectWithConditions(EObject src, EReference ref,
			ForbiddenMap forbiddenMap, int size) {
				//int ri = host.randomUInt();
	//EList<EObject> result = new BasicEList<EObject>(size);

//		if(allRefs.size()==1) {
//			if(first.size()!=0) {
//				size = first.size()>size ? size : first.size();
//				while(result.size()<size) {
//					int id = host.randomUIntU(first.size());
//					result.add(first.get(id));
//				}
//				return result;
//			}
//			
//			if(second.size()!=0) {
//				size = second.size()>size ? size : second.size();
//				while(result.size()<size) {
//					int id = host.randomUIntU(second.size());
//					result.add(second.get(id));
//				}
//				return result;
//			}
//		} else 
		{
			List<EObject> availableObjs = null;
			
			
			if(first.size()!=0){
/*				for(EObject o : first) {
					if(getReferenceClass(ref).isSuperTypeOf(o.eClass())){
						if(forbiddenMap==null||forbiddenMap.isForbidden(src, o)==false) {
							boolean special = getClassCase(o);
							
							EList<EReference> refs = getRefs(o.eClass());
							int id = refs.indexOf(ref);
							int[] lb = this.getLowerBound(o.eClass());
							int[] d = getDegree(o);
							
							if(!special) {
								if(d[id+1]<lb[id+1])
									availableObjs.add(o);
							} else {
								if(d[id+1]==0)
									availableObjs.add(o);
							}
						}
					}
					//else reject by forbidden map
				}*/
//				Collections.shuffle(first);
				first.doShuffle(size);
				availableObjs = collectFirstAvailable(this,src,ref, forbiddenMap, 0 ,first.size(),size);
				//FIXME:
				
				//System.out.print("shuffle "+first.size()+" :");
				
				//System.out.println(availableObjs);
				//availableObjs = concurrentCollect(true, src, ref, forbiddenMap);
				//System.out.println("collected "+availableObjs.size());
				//System.out.println(first.size()+","+availableObjs.size());;
				
				//2015/3/16 removed
//				if(availableObjs.size()!=0){
//					size = availableObjs.size()>size ? size : availableObjs.size();
//					UniqueEList<Integer> idlist = new UniqueEList<Integer>(size);
//					while(idlist.size()<size) {
//						int id = host.random.randomUIntU(availableObjs.size());
//						idlist.add(id);
//					}
//					singleton.clear();
//					singleton.addAll(idlist);
//					
//					Collections.sort(singleton);
//					
//					int id = 0;
//					int idc = 0;
//					Iterator<EObject> it = availableObjs.iterator();
//					while(result.size()<size) {
//						EObject c = it.next();
//						if(id==singleton.get(idc)) {
//							result.add(c);
//							idc++;
//						}
//						id++;
//					}
//					
//					return result;
//				}
				//2015/3/16 removed
				
				//Collections.shuffle(availableObjs, host.random.getRandom());
				
				
				return availableObjs;
			} 
			
			if(second.size()!=0) {
				/*for(EObject o : second) {
					if(getReferenceClass(ref).isSuperTypeOf(o.eClass())) {
						if(forbiddenMap==null||forbiddenMap.isForbidden(src, o)==false) {
							EList<EReference> refs = getRefs(o.eClass());
							int id = refs.indexOf(ref);
							int[] ub = this.getUpperBound(o.eClass());
							int[] d = getDegree(o);
							
							if(d[id+1]<ub[id+1]||ub[id+1]==-1)
								availableObjs.add(o);
						}
					}
				}*/

				//System.out.print("shuffle "+second.size()+" :"+second.get(0)+" to ");
//				Collections.shuffle(second);
				second.doShuffle(size);
				availableObjs = collectSecondAvailable(this,src,ref, forbiddenMap, 0 ,second.size(),size);
				
				//System.out.println(availableObjs);
				//availableObjs = concurrentCollect(false, src, ref, forbiddenMap);
				//System.out.println("collected "+availableObjs.size());
				//System.out.println(second.size()+","+availableObjs.size());;
				
//				if(availableObjs.size()!=0){
//					size = availableObjs.size()>size ? size : availableObjs.size();
//					UniqueEList<Integer> idlist = new UniqueEList<Integer>(size);
//					while(idlist.size()<size) {
//						int id = host.random.randomUIntU(availableObjs.size());
//						idlist.add(id);
//					}
//					
//					singleton.clear();
//					singleton.addAll(idlist);
//					
//					Collections.sort(singleton);
//					
//					int id = 0;
//					int idc = 0;
//					Iterator<EObject> it = availableObjs.iterator();
//					while(result.size()<size) {
//						EObject c = it.next();
//						if(id==singleton.get(idc)) {
//							result.add(c);
//							idc++;
//						}
//						id++;
//					}
//					
//					return result;
//				}
				
				return availableObjs;
			}
		}
		
		
		return null;
	}
				
	
}