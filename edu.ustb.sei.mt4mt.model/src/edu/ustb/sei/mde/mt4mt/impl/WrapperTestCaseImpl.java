/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.TestAction;
import edu.ustb.sei.mde.mt4mt.TestCase;
import edu.ustb.sei.mde.mt4mt.WrapperTestCase;
import edu.ustb.sei.mt4mt.base.Actions;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wrapper Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl#getBeforeTest <em>Before Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl#getAfterTest <em>After Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl#getCoreTest <em>Core Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.WrapperTestCaseImpl#getRepeat <em>Repeat</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WrapperTestCaseImpl extends TestCaseImpl implements WrapperTestCase {
	/**
	 * The cached value of the '{@link #getBeforeTest() <em>Before Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeforeTest()
	 * @generated
	 * @ordered
	 */
	protected TestAction beforeTest;

	/**
	 * The cached value of the '{@link #getAfterTest() <em>After Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAfterTest()
	 * @generated
	 * @ordered
	 */
	protected TestAction afterTest;

	/**
	 * The cached value of the '{@link #getCoreTest() <em>Core Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreTest()
	 * @generated
	 * @ordered
	 */
	protected TestCase coreTest;

	/**
	 * The default value of the '{@link #getRepeat() <em>Repeat</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeat()
	 * @generated
	 * @ordered
	 */
	protected static final int REPEAT_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getRepeat() <em>Repeat</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeat()
	 * @generated
	 * @ordered
	 */
	protected int repeat = REPEAT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WrapperTestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.WRAPPER_TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestAction getBeforeTest() {
		return beforeTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBeforeTest(TestAction newBeforeTest, NotificationChain msgs) {
		TestAction oldBeforeTest = beforeTest;
		beforeTest = newBeforeTest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST, oldBeforeTest, newBeforeTest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeforeTest(TestAction newBeforeTest) {
		if (newBeforeTest != beforeTest) {
			NotificationChain msgs = null;
			if (beforeTest != null)
				msgs = ((InternalEObject)beforeTest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST, null, msgs);
			if (newBeforeTest != null)
				msgs = ((InternalEObject)newBeforeTest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST, null, msgs);
			msgs = basicSetBeforeTest(newBeforeTest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST, newBeforeTest, newBeforeTest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestAction getAfterTest() {
		return afterTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAfterTest(TestAction newAfterTest, NotificationChain msgs) {
		TestAction oldAfterTest = afterTest;
		afterTest = newAfterTest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST, oldAfterTest, newAfterTest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAfterTest(TestAction newAfterTest) {
		if (newAfterTest != afterTest) {
			NotificationChain msgs = null;
			if (afterTest != null)
				msgs = ((InternalEObject)afterTest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST, null, msgs);
			if (newAfterTest != null)
				msgs = ((InternalEObject)newAfterTest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST, null, msgs);
			msgs = basicSetAfterTest(newAfterTest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST, newAfterTest, newAfterTest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCase getCoreTest() {
		return coreTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoreTest(TestCase newCoreTest, NotificationChain msgs) {
		TestCase oldCoreTest = coreTest;
		coreTest = newCoreTest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST, oldCoreTest, newCoreTest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoreTest(TestCase newCoreTest) {
		if (newCoreTest != coreTest) {
			NotificationChain msgs = null;
			if (coreTest != null)
				msgs = ((InternalEObject)coreTest).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST, null, msgs);
			if (newCoreTest != null)
				msgs = ((InternalEObject)newCoreTest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST, null, msgs);
			msgs = basicSetCoreTest(newCoreTest, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST, newCoreTest, newCoreTest));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRepeat() {
		return repeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepeat(int newRepeat) {
		int oldRepeat = repeat;
		repeat = newRepeat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.WRAPPER_TEST_CASE__REPEAT, oldRepeat, repeat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
				return basicSetBeforeTest(null, msgs);
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
				return basicSetAfterTest(null, msgs);
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				return basicSetCoreTest(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
				return getBeforeTest();
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
				return getAfterTest();
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				return getCoreTest();
			case MT4MTPackage.WRAPPER_TEST_CASE__REPEAT:
				return getRepeat();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
				setBeforeTest((TestAction)newValue);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
				setAfterTest((TestAction)newValue);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				setCoreTest((TestCase)newValue);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__REPEAT:
				setRepeat((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
				setBeforeTest((TestAction)null);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
				setAfterTest((TestAction)null);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				setCoreTest((TestCase)null);
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__REPEAT:
				setRepeat(REPEAT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
				return beforeTest != null;
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
				return afterTest != null;
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				return coreTest != null;
			case MT4MTPackage.WRAPPER_TEST_CASE__REPEAT:
				return repeat != REPEAT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (repeat: ");
		result.append(repeat);
		result.append(')');
		return result.toString();
	}

	@Override
	public AbstractTransformationTestCase build() {
		IAction before = this.beforeTest==null ? Actions.skip() : this.beforeTest.build();
		IAction after = this.afterTest==null ? Actions.skip() : this.afterTest.build();
		AbstractTransformationTestCase core = this.coreTest.build();
		AbstractTransformationTestCase test = new edu.ustb.sei.mt4mt.base.test.WrapperTestCase(before, after, core, this.repeat);
		test.setMetamodels(getMetamodels().toArray(AbstractTransformationTestCase.STRING_ARRAY));
		test.setIdentifier("test " + getId()+" (wrapper)");
		return test;
	}

} //WrapperTestCaseImpl
