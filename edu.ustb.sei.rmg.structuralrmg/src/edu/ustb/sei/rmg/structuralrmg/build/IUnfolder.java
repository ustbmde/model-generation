package edu.ustb.sei.rmg.structuralrmg.build;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;

public interface IUnfolder {
	public void travelPackage(EPackage pkg);
	public Resource buildResource(Resource root);
}
