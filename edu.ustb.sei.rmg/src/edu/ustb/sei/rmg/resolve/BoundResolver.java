package edu.ustb.sei.rmg.resolve;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.resource.Resource;

import choco.Choco;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.common.opres.pack.LowerBoundFactory;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.variables.integer.IntDomainVar;
import choco.kernel.solver.variables.integer.IntVar;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.ReferencePartition;
import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.GlobalOption;
import edu.ustb.sei.rmg.config.GlobalOptionEnum;
import edu.ustb.sei.rmg.config.OptionModel;
import edu.ustb.sei.rmg.config.ReferenceGroupOption;
import edu.ustb.sei.rmg.config.ReferenceOption;
import edu.ustb.sei.rmg.random.RandomUtil;
import edu.ustb.sei.rmg.random.condition.BoundSpecException;
import edu.ustb.sei.rmg.random.condition.Condition;
import edu.ustb.sei.rmg.random.condition.ConditionList;
import edu.ustb.sei.rmg.random.condition.RangeCondition;
import edu.ustb.sei.rmg.util.OrderedPairMap;
import edu.ustb.sei.rmg.util.Pair;

//FIXME currently, reference groups are not supported
public class BoundResolver {
	public EPackage metamodel;
	public OptionModel option;
	
	final static int ChocoUPPERBOUND = 10000;
	
	
	/*
	 * 1. collect subclasses and the whole inheritance hierarchy of each class
	 * 2. collect lowerContainer and upperContainer
	 * 3. create variable of each class
	 * 4. create variable of all instances of each class and its calculation relation
	 * 5. create bound constraint for class variable
	 * 6. create bound constraint for all-instance variable
	 */
	
	/*
	 List<EClass> eClasses;
	 List<EReference> eReferences;
	 Map<EClass,List<EClass>> subclasses; //eClasses
	 Map<EClass,List<EClass>> hierarchy; //eClasses
	 Map<EClass,List<EReference>> lowerContainer; //eReferences, subclasses
	 Map<EClass,List<EReference>> upperContainer; //eReferences, hierarchy
	 
	 Map<EClass,Variable> clsVar; 
	 Map<EClass,Variable> clsAllVar;
	 
	 
	 */
	
	List<EClass> eClasses = new ArrayList<EClass>(); // all EClasses
	List<EReference> eReferences = new ArrayList<EReference>(); // all EReference
	List<EReference> allContainmentReferences = new ArrayList<EReference>(); // all Containment EReferences
	Map<EClass,List<EClass>> subclassMap = null; // map from a class to its subclass (including itself)
	Map<EClass,IntegerVariable> clsVarMap = new HashMap<EClass,IntegerVariable>(); // map from a class to an integer variable
	protected OrderedPairMap<EClass,EClass,Boolean> hasCommonSubClass = new OrderedPairMap<EClass,EClass,Boolean>();
	
	ReferencePartition referencePartition = new ReferencePartition(hasCommonSubClass);
	
	public void prepare() {
		iterateMetamodel(metamodel);
		computeSubclassMap();
	}
	
	private void iterateMetamodel(EPackage ePackage) {
		TreeIterator<EObject> iter = ePackage.eAllContents();
		while(iter.hasNext()) {
			EObject o = iter.next();
			if(o.eClass().equals(EcorePackage.eINSTANCE.getEClass())) {
				eClasses.add((EClass)o);
				for(EReference ref : ((EClass)o).getEReferences()) {
					eReferences.add(ref);
					if(ref.isContainment()) 
						allContainmentReferences.add(ref);
				}
			}
		}
	}
	
	static public <T> Map<T,List<T>> toMapOfList(List<T> list) {
		Map<T,List<T>> result = new HashMap<T,List<T>>();
		
		for(T o : list) {
			List<T> ol = new ArrayList<T>();
			ol.add(o);
 			result.put(o, ol);
		}
		
		return result;
	}
	
	private IntegerVariable[] toBoundVariables(List<EClass> clazzes) {
		IntegerVariable[] vars = new IntegerVariable[clazzes.size()];
		for(int i = 0; i<clazzes.size() ; i++) {
			vars[i] = clsVarMap.get(clazzes.get(i));
		}
		return vars;
	}
	
	private void computeSubclassMap() {
		subclassMap = toMapOfList(eClasses);
		for(EClass c1 : eClasses) {
			for(EClass c2 : eClasses) {
				if(c1==c2) continue;
				if(c1.isSuperTypeOf(c2)) {
					List<EClass> l1 = subclassMap.get(c1);
					List<EClass> l2 = subclassMap.get(c2);
					for(EClass e : l2) {
						if(l1.contains(e)==false)
							l1.add(e);
					}
				}
			}
		}
		
		for(EClass c1 : eClasses) {
			for(EClass c2 : eClasses) {
				if(c1==c2) {
					this.hasCommonSubClass.put(c1, c2, true);
				} else {
					List<EClass> l1 = subclassMap.get(c1);
					List<EClass> l2 = subclassMap.get(c2);
					
					System.out.println(c1.getName()+" subclasses: "+l1);
					System.out.println(c2.getName()+" subclasses: "+l2);
					
					if(Collections.disjoint(l1, l2)==false) {
						this.hasCommonSubClass.put(c1, c2, true);
					}
				}
			}
		}
	}
	
	private int[] getClassBound(EClass c) throws BoundSpecException {
		
		for(Object v : option.getClsOptions()) {
			ClassOption co = (ClassOption)v;
			
			if(co.getClassRef()==c) {
				Condition<?> id = RandomUtil.parseCondition(co.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};			
			}
		}
		return null;
	}
	
	private int[] getDefaultObjectBound() throws BoundSpecException {
		for(Object v : option.getGlobalOptions()) {
			GlobalOption o = (GlobalOption)v;
			
			if(o.getKey()==GlobalOptionEnum.DEFAULT_OBJECT_BOUND) {
				Condition<?> id = RandomUtil.parseCondition(o.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};			
			}
		}
		return null;
	}
	
	public void createEClassBoundVariablesAndConstraints(CPModel model) {
		for(EClass e : eClasses) {
			IntegerVariable v = Choco.makeIntVar(e.getName(), 0, ChocoUPPERBOUND);
			model.addVariable(v);
			clsVarMap.put(e, v);
		}
		//using different loops for the sake of clarity
		for(EClass e : eClasses) {
			IntegerVariable v = clsVarMap.get(e);
			if(e.isAbstract()) {
				model.addConstraint(Choco.eq(0, v));
				System.out.println("C1 0="+v.getName());
			} else {
				try {
					int[] bound = this.getClassBound(e);
					if(bound==null) bound = this.getDefaultObjectBound();
					
					if(bound==null) {
						System.out.println("Class bound for "+e.getName()+" is null");
					} else {
						if(bound[0]==bound[1]) {
							model.addConstraint(Choco.eq(bound[0], v));
							System.out.println("C1 "+bound[0]+"="+v.getName());
						} else {
							model.addConstraints(Choco.leq(bound[0], v),Choco.leq(v, bound[1]));
							System.out.println("C1 "+bound[0]+"<="+v.getName()+"<="+bound[1]);
						}
					}
					
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}
	}
	
//	public void createAllInstanceVariableAndConstraint
	
	public void createContainmentReferenceConstraints(CPModel model) {
		EList<EList<EReference>> lists = referencePartition.splitRefereneGroup(allContainmentReferences, 
				true, true, false, false, true, true, true); // we should not consider reflexivity and ordering
		// this hints that we only have to consider uniqueness & necessity
		// uniqueness affects the lower bound, necessity affects the upper bound
		// ordering and reflexivity affect the number of relationships
		
		for(EList<EReference> group : lists) {
			List<EClass> content = this.collectContentTypes(group);
			
			List<IntegerExpressionVariable> lowerBound = new ArrayList<IntegerExpressionVariable>();
			List<IntegerExpressionVariable> upperBound = new ArrayList<IntegerExpressionVariable>();
			boolean isUnlimited = false;
			
			StringBuilder ls = new StringBuilder();
			StringBuilder us = new StringBuilder();
			boolean isFirst = true;
			
			for(EReference ref : group) {
				EClass c = ref.getEContainingClass();
				List<EClass> ac = subclassMap.get(c);
				IntegerVariable[] acv = this.toBoundVariables(ac);
				int l = ref.getLowerBound();
				int u = ref.getUpperBound();
				lowerBound.add(Choco.mult(l, Choco.sum(acv)));
				
				if(isFirst) isFirst = false;
				else {
					ls.append("+");
					us.append("+");
				}
				
				printSumOfMult(ls, l, acv);
				
				if(u>0) {
					upperBound.add(Choco.mult(u, Choco.sum(acv)));
				} else {
					isUnlimited = true;
				}
				printSumOfMult(us, u, acv);
				
			}
			
			IntegerVariable[] allContentVariables = this.toBoundVariables(content);
			IntegerExpressionVariable l = Choco.sum(lowerBound.toArray(new IntegerExpressionVariable[0]));
			IntegerExpressionVariable s = Choco.sum(allContentVariables);
			model.addConstraint(Choco.leq(l, s));
			
			StringBuilder cs = new StringBuilder();
			printSumOfMult(cs, 1, allContentVariables);
			
			System.out.println("C3 "+ls.toString()+"<="+cs.toString());
			
			if(isUnlimited==false) {
				IntegerExpressionVariable u = Choco.sum(upperBound.toArray(new IntegerExpressionVariable[0]));
				model.addConstraint(Choco.leq(s, u));
				System.out.println("C4 "+cs.toString()+"<="+us.toString());
			}
		}
	}
	
	static public void printSumOfMult(StringBuilder s, int mult,IntegerVariable... vars) {
//		if(mult==0) return;
		if(mult!=1)
			s.append(mult+"*(");
		s.append(vars[0].getName());
		
		for(int i = 1 ; i<vars.length;i++)
			s.append("+"+vars[i].getName());
		
		if(mult!=1)
			s.append(")");
	}
	
	private List<EClass> collectContainerTypes(List<EReference> group) {
		List<EClass> res = new UniqueEList<EClass>();
		for(EReference ref : group) {
			List<EClass> subcls = subclassMap.get(ref.getEContainingClass());
			res.addAll(subcls);
		}
		
		return res;
	}
	
	private List<EClass> collectContentTypes(List<EReference> group) {
		List<EClass> res = new UniqueEList<EClass>();
		for(EReference ref : group) {
			res.add(ref.getEReferenceType());
			List<EClass> subcls = subclassMap.get(ref.getEReferenceType());
			res.addAll(subcls);
		}
		
		return res;
	}
	
	public CPModel createCPModel() {
		this.prepare();
		CPModel model = new CPModel();
		this.createEClassBoundVariablesAndConstraints(model);
		this.createContainmentReferenceConstraints(model);
		return model;
	}
	
	static public void solve(OptionModel configModel) {
		BoundResolver s = new BoundResolver();
		s.metamodel = configModel.getMetamodel();
		s.option = configModel;
		
		CPModel model = s.createCPModel();
		CPSolver cps = new CPSolver();
		cps.read(model);
		
		cps.setRandomSelectors();
		
		System.out.println("++++++++++++++++++++++++");
		
		if(cps.solve()) {
			int id = 0;
			//do {
			System.out.println("solution #"+id++);
			for(EClass c : s.eClasses) {
				IntegerVariable v = s.clsVarMap.get(c);
				IntDomainVar val = cps.getVar(v);
				System.out.println(v.getName()+":"+val.getVal());
			}
			
//			for(EReference c : s.normalReferences) {
//				IntegerVariable v = s.refVarMap.get(c);
//				IntDomainVar val = cps.getVar(v);
//				System.out.println(v.getName()+":"+val.getVal());
//			}
			
			//}while(cps.nextSolution());
		} else {
			System.out.println("no solution");
		}
	}
}
