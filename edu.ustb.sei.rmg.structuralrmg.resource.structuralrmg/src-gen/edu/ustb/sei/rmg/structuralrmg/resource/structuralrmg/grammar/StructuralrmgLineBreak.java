/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public class StructuralrmgLineBreak extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFormattingElement {
	
	private final int tabs;
	
	public StructuralrmgLineBreak(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
