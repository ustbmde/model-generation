package edu.ustb.sei.mt4mt.base;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.jface.action.ExternalActionManager;

import edu.ustb.sei.mt4mt.base.actions.*;
import edu.ustb.sei.mt4mt.base.updates.IExternalAction;
import edu.ustb.sei.mt4mt.base.updates.IUpdate;

public class Actions {
	static public IAction sequence(IAction... actions) {
		return new SequenceAction(actions);
	}
	
	static public IAction copy(String fromUri, String toUri) {
		if(fromUri.equals(toUri)) return skip();
		else return new CopyModelAction(fromUri, toUri);
	}
	static public IAction load(String fromUri, String toKey) {
		return new LoadModelAction(fromUri, toKey);
	}
	static public IAction save(String fromKey, String toUri) {
		return new SaveModelAction(fromKey, toUri);
	}
	static public IAction compare(String leftUri, String rightUri, String resultKey) {
		if(leftUri.equals(rightUri)) return skip();
		else return new CompareModelAction(leftUri, rightUri, resultKey);
	}
	static public IAction assertComparison(String resultKey, int differenceCount) {
		return new AssertComparisonResultAction(resultKey, differenceCount);
	}
	
	static public IAction assertComparison(String resultKey, int differenceCount, int kind) {
		return new AssertComparisonResultAction(resultKey, differenceCount, kind);
	}
	
	static public IAction assertOCL(String ocl, String modelKey, String[] imports) {
		return new AssertOCLInvariantAction(ocl, modelKey, imports);
	}
	static public IAction query(String ocl, String modelKey, String resultKey, String[] imports) {
		return new EvaluateOCLQueryAction(ocl, modelKey, resultKey, imports);
	}
	static public IAction java(Function<Context, Void> action) {
		return new JavaAction(action);
	}
	
	static public IAction clear(String[] files) {
		return new ClearAction(files);
	}
	
	static public IAction launch(String name) {
		return new LaunchExecutor(name);
	}
	
	static public IAction launch(String name, long wait) {
		return new LaunchExecutor(name, wait);
	}
	
	static public IAction compareAndAssert(String left, String right, String key) {
		IAction compare = compare(left,right, key);
		
		if(compare instanceof EmptyAction) return compare;
		else return sequence(compare,
				assertComparison(key, 0));
	}
	
	static public IAction compareAndAssert(String[] left, String[] right, String key) {
		List<IAction> list = new ArrayList<IAction>();
		for(int i = 0 ; i<left.length ; i++) {
			list.add(compareAndAssert(left[i], right[i], key));
		}
		return sequence(list.toArray(new IAction[left.length]));
	}
	
	static public IAction copy(String[] from, String[]... to) {
		int fromSize = from.length;
		int toSize = 0;
		for(String[] t : to) {
			toSize += t.length;
		}
		if(fromSize!=toSize) 
			throw new RuntimeException("the actual input size and the formal input are differet");
		
		List<IAction> list = new ArrayList<IAction>();
		
		int index = 0;
		
		for(String[] t : to) {
			for(String tu : t) {
				list.add(copy(from[index],tu));
				index++;
			}
		}
		
		return sequence(list.toArray(new IAction[fromSize]));
	}
	
	static public IAction set(String k, String v) {
		return new SetVariableAction(k, v);
	}
	
	static public IAction skip() {
		return EmptyAction.SINGLETON;
	}
	
	static public IAction fail() {
		return FailAction.SINGLETON;
	}
	
	static public IAction update(String modelKey, IUpdate[] updates) {
		return new UpdateModelAction(modelKey, updates);
	}

	public static IAction external(Map<String, String> params, String resultName, IExternalAction exAction) {
		return new ExternalAction(params,resultName,exAction);
	}
}
