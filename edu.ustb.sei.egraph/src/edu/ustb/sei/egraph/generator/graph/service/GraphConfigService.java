package edu.ustb.sei.egraph.generator.graph.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;


import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.egraph.generator.graph.config.AttributeConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetGenerationConfig;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfig;
import edu.ustb.sei.egraph.generator.graph.config.NodeConfig;
import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;
import edu.ustb.sei.egraph.generator.value.RandomValueFunction;
import edu.ustb.sei.egraph.structure.ETypeModel;

public class GraphConfigService<NT,ET,NAT,EAT> {
	public GraphConfigService(GraphConfig config, ETypeModel<NT,ET,NAT,EAT> typeModel) {
		super();
		this.config = config;
		this.typeModel = typeModel;
		
		makeContainmentReferencesConfig();
	}
	private GraphConfig config;
	private ETypeModel<NT,ET,NAT,EAT> typeModel;
	
	private EdgeSetConfig containmentEdgesConfig;
	
	private void makeContainmentReferencesConfig() {
		containmentEdgesConfig = new EdgeSetConfig(null, true, false, true, true, true, false, false);
		EdgeSetGenerationConfig eg = new EdgeSetGenerationConfig(null, null, null);
		this.containmentEdgesConfig.getGenerationConfigs().add(eg);
	}
	
	public EdgeSetConfig getContainmentsConfig() {
		return this.containmentEdgesConfig;
	}
	
	
	
	private List<Pair<NT,NodeConfig>> nodeConfigs = null;
	public List<Pair<NT,NodeConfig>> getAllNodeConfigs() {
		if(nodeConfigs==null) {
			nodeConfigs = new ArrayList<Pair<NT,NodeConfig>>();
			for(NodeConfig c : this.config.getNodeConfigs()) {
				NT nt = this.typeModel.resolveNodeType(c.getType());
				if(nt==null) {
					throw new RuntimeException("cannot resolve node type "+c.getType());
				} else {
					Pair<NT, NodeConfig> pair = new Pair<NT,NodeConfig>(nt,c);
					nodeConfigs.add(pair);
				}
			}
		}
		return nodeConfigs;
	}
	
	private List<Pair<List<ET>,EdgeSetConfig>> edgeConfigs = null;
	public List<Pair<List<ET>,EdgeSetConfig>> getAllEdgeSetConfigs() {
		if(edgeConfigs==null) {
			edgeConfigs = new ArrayList<Pair<List<ET>,EdgeSetConfig>>();
			for(EdgeSetConfig c : this.config.getEdgeConfigs()) {
				List<ET> edges = new ArrayList<ET>();
				for(QualifiedName n : c.getEdges()) {
					ET et = this.typeModel.resolveEdgeType(n);
					if(et==null) {
						throw new RuntimeException("cannot resolve edge type "+n);
					} else {
						edges.add(et);
					}
				}
				Pair<List<ET>,EdgeSetConfig> pair = new Pair<List<ET>,EdgeSetConfig>(edges,c);
				edgeConfigs.add(pair);
			}
		}
		return edgeConfigs;
	}
	
	
	private Map<Object,Integer> boundMap = new HashMap<Object,Integer>();
	public void resolveBound(RandomService r) {
		List<Pair<Object,RandomValueFunction<Integer>>> pending = new ArrayList<Pair<Object,RandomValueFunction<Integer>>>();
		Set<String> finished = new HashSet<String>();
		Set<String> intermeidate = new HashSet<String>();
		
		for(Pair<NT, NodeConfig> c : this.getAllNodeConfigs()) {
			AbstractRandomFunction<Integer> boundSpec = c.getSecond().getBound();
			if(boundSpec==null) continue;
			RandomValueFunction<Integer> bound = boundSpec.getValue(r);
			pending.add(new Pair<Object,RandomValueFunction<Integer>>(c.getSecond(),bound));
		}
		
		for(Pair<List<ET>, EdgeSetConfig> c : this.getAllEdgeSetConfigs()) {
			for(EdgeSetGenerationConfig gc : c.getSecond().getGenerationConfigs()) {
				AbstractRandomFunction<Integer> boundSpec = gc.getBound();
				if(boundSpec==null) continue;
				RandomValueFunction<Integer> bound = boundSpec.getValue(r);
				pending.add(new Pair<Object,RandomValueFunction<Integer>>(gc,bound));	
			}
		}
		
		List<Pair<Object,RandomValueFunction<Integer>>> current = new ArrayList<Pair<Object,RandomValueFunction<Integer>>>();
		
		int lastSize = pending.size();
		while(pending.size()>0) {
			current.clear();
			current.addAll(pending);
			pending.clear();
			
			for(Pair<Object,RandomValueFunction<Integer>> bound : current) {
				Set<String> required = bound.getSecond().requiredScopes();
				if(finished.containsAll(required)) {
					Integer i = bound.getSecond().getValue(r);
					boundMap.put(bound.getFirst(), i);
					intermeidate.addAll(bound.getSecond().providedScopes());
				} else {
					pending.add(bound);
				}
			}
			
			Set<String> mayContribute = new HashSet<String>();
			pending.forEach((p)->{mayContribute.addAll(p.getSecond().providedScopes());});
			intermeidate.forEach((s)->{
				if(mayContribute.contains(s)==false)
					finished.add(s);
			});
			intermeidate.clear();
			
			if(lastSize == pending.size())
				throw new RuntimeException("Recursive dependencies");
			else
				lastSize = pending.size();
		}
	}
	
	public Integer getBound(Object config) {
		return this.boundMap.get(config);
	}
	
	
	public List<Pair<NAT,AttributeConfig>> getAttributeConfigsForNodeType(NT type) {
		Collection<NT> allSuperTypes = typeModel.collectAllSuperTypes(type);
		// 1. get node config for type
		// 2. get attribute configs from node config
		// 3. get global attribute configs that are defined in node config
		List<Pair<NAT,AttributeConfig>> result = new ArrayList<Pair<NAT,AttributeConfig>>();
		
		for(NT t : allSuperTypes) {
			NodeConfig nc = getNodeConfig(t);
			if(nc!=null)
				collectAttributeConfigsFrom(nc.getAttributeConfigs(), result);
		}
		
		Collection<NAT> attributes = this.typeModel.getAllAttributesOfNodeType(type);
		
		for(AttributeConfig a : this.config.getAttributeConfigs()) {
			NT definedType = this.typeModel.resolveNodeType(new QualifiedName(a.getName().type,QualifiedName.defaultType));
			if(definedType!=null && this.typeModel.isSuperNodeType(type, definedType)) {
				NAT attr = this.typeModel.resolveNodeAttribute(a.getName());
				if(attr!=null && attributes.contains(attr)) {
					uniqueAdd(result, new Pair<NAT,AttributeConfig>(attr,a), comparatorInSuperTypes);
				} else {
					throw new RuntimeException("Cannot resolve attribute "+a);
				}
			}
		}
		return result;
	}
	
	public NodeConfig getNodeConfig(NT type) {
		QualifiedName name = typeModel.desolveNodeType(type);
		for(NodeConfig nc : this.config.getNodeConfigs()) {
			if(nc.getType().equals(name)) 
				return nc;
		}
		return null;
	}
	
	final private BiFunction<Pair<NAT,AttributeConfig>,Pair<NAT,AttributeConfig>,Boolean> comparatorInSuperTypes = (l,r)->l.getSecond().getName().equals(r.getSecond().getName());
	private void collectAttributeConfigsFrom(List<AttributeConfig> source, List<Pair<NAT,AttributeConfig>> result) {
		for(AttributeConfig a : source) {
			NAT attr = this.typeModel.resolveNodeAttribute(a.getName());
			uniqueAdd(result, new Pair<NAT,AttributeConfig>(attr,a), comparatorInSuperTypes);
		}
	}
	private <T> void uniqueAdd(List<T> list, T o, BiFunction<T, T, Boolean> comparator) {
		for(T t : list) {
			if(comparator.apply(t, o)) return;
		}
		list.add(o);
	}


	public NT getRootType() {
		QualifiedName root = this.config.getRootType();
		if(root==null) 
			return null;
		else 
			return (NT)this.typeModel.resolveNodeType(root);
	}

	public boolean shouldGenerateContainment() {
		return config.shouldGenerateContainment();
	}

	public Map<Object, Object> getSaveOptions() {
		return config.getSaveOptions();
	}
	
	
}
