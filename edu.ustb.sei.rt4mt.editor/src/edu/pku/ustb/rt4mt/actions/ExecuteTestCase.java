package edu.pku.ustb.rt4mt.actions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
//import org.eclipse.emf.compare.diff.engine.GenericDiffEngine;
//import org.eclipse.emf.compare.diff.metamodel.DiffModel;
//import org.eclipse.emf.compare.diff.service.DiffService;
//import org.eclipse.emf.compare.match.metamodel.MatchModel;
//import org.eclipse.emf.compare.match.service.MatchService;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import edu.pku.ustb.rt4mt.ExpectedOutput;
import edu.pku.ustb.rt4mt.InputParameterConfiguration;
import edu.pku.ustb.rt4mt.OutputParameterConfiguration;
import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.TestCase;
import edu.pku.ustb.rt4mt.TestData;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;
import edu.pku.ustb.rt4mt.TestRoundGroup;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.OptionModel;

public class ExecuteTestCase extends Action implements
		ISelectionChangedListener {
	protected ISelectionProvider selectionProvider;
	public ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	public void setSelectionProvider(ISelectionProvider selectionProvider) {
		this.selectionProvider = selectionProvider;
	}

	protected TestRound testRound;
	protected EditingDomain domain;
	protected RegressionTestModel testModel;
	
	public EditingDomain getDomain() {
		return domain;
	}

	public void setDomain(EditingDomain domain) {
		this.domain = domain;
	}

	public ExecuteTestCase() {
		super("Execute Test Case");
		setDescription("Execute a New Test Case");
		
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	}
	
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		selectionProvider = event.getSelectionProvider();
	    if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	public boolean updateSelection(IStructuredSelection selection) {
		if(selection.size()==1) {
			Object se = selection.getFirstElement();
			if(se instanceof TestRound) {
				testRound = (TestRound) se;
				testModel = (RegressionTestModel) testRound.eContainer().eContainer();
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		//domain.getResourceSet().getPackageRegistry().put(ConfigPackage.eNS_URI, ConfigPackage.eINSTANCE);
		String exec = testModel.getRunConfigurationName();
		
		ILaunchConfiguration launchConfig = getLaunchConfiguration(exec);		
		IWorkbench wb = PlatformUI.getWorkbench(); 
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow(); 
		

//		String input = testModel.getInput();
//		String output = testModel.getOutput();
//		
//		URI inputURI = URI.createPlatformResourceURI(input, true);
//		URI outputURI = URI.createPlatformResourceURI(output, true);
//		
//		String outputEcore = testModel.getOutputMetamodel();
//		loadMetamodel(outputEcore);
		
		boolean allPassed = true;
		
		for(TestExecution e : testRound.getTestExecutions()) {
			if(e.isPassed()==false) {
				TestCase tc = e.getTestCase();

				prepareTestInputs(tc);
				
				try {
					ILaunch result = launchConfig.launch(ILaunchManager.RUN_MODE, null);
					if(result.isTerminated()) {
						System.out.println("auto check test output");
						boolean matched = false;

						//matched = checkExpectedOutput(outputURI, e, tc);
						matched = checkExpectedOutputs(e,tc);
						
						if(matched==false) {
							MessageDialog.openError(win.getShell(), 
									"Test Failded", "Test failed at "+e.getTestCase().getId());
							allPassed = false;
							break;
						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		if(allPassed) {
			MessageDialog.openInformation(win.getShell(), 
					"Test Successful", "All tests are passed");
		}
		
		
		
	}

	private void prepareTestInputs(TestCase tc) {
		EList<TestData> inputs = tc.getInputs();
		EList<InputParameterConfiguration> iParams = testModel.getInput().getParameters();
		ArrayList<String> nameLists = new ArrayList<String>(iParams.size());
		HashMap<String,InputParameterConfiguration> formalMap = new HashMap<String,InputParameterConfiguration>();
		HashMap<String,TestData> actualMap = new HashMap<String,TestData>();
		
		for(InputParameterConfiguration i : iParams) {
			nameLists.add(i.getName());
			formalMap.put(i.getName(), i);
		}
		
		for(TestData t : inputs) {
			actualMap.put(t.getName(), t);
		}
		
		for(String name : nameLists){
			InputParameterConfiguration p = formalMap.get(name);
			TestData d = actualMap.get(name);
			
			URI formalURI = URI.createPlatformResourceURI(p.getModelURI(), true);
			URI actualURI = URI.createPlatformResourceURI(d.getModelURI(), true);
			
			try {
				FileUtil.forChannel(actualURI, formalURI);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void loadMetamodel(String outputEcore) {
		URI mmuri = URI.createPlatformResourceURI(outputEcore, true);
		Resource mm = resourceSet.getResource(mmuri, true);
		EPackage pkg = (EPackage) mm.getContents().get(0);
		resourceSet.getPackageRegistry().put(pkg.getNsURI(), pkg);
	}
	
	private boolean checkExpectedOutputs(TestExecution te, TestCase tc) {
		boolean matched = false;
		
		EList<OutputParameterConfiguration> actualOutputs = testModel.getOutput().getParameters();
		EList<ExpectedOutput> expectedOutputs = tc.getExpectedOutputs();
		
		ArrayList<String> nameLists = new ArrayList<String>(actualOutputs.size());
		HashMap<String,OutputParameterConfiguration> actualMap = new HashMap<String,OutputParameterConfiguration>();
		HashMap<String,TestData> expectedMap = new HashMap<String,TestData>();
		
		for(OutputParameterConfiguration o : actualOutputs) {
			nameLists.add(o.getName());
			actualMap.put(o.getName(), o);
		}
		
		for(ExpectedOutput eo : expectedOutputs) {
			boolean flag = true;
			
			EList<TestData> expectedOutputData = eo.getData();
			for(TestData o : expectedOutputData) {
				expectedMap.put(o.getName(), o);
			}
			
			for(String name : nameLists) {
				OutputParameterConfiguration op = actualMap.get(name);
				TestData expectedData = expectedMap.get(name);
				
				loadMetamodel(op.getMetamodelURI());
				URI actualURI = URI.createPlatformResourceURI(op.getModelURI(), true);
				String exp = expectedData.getModelURI();
				URI expectedURI = URI.createPlatformResourceURI(exp, true);
				
				clearResource(actualURI);
				Resource actualOutput = resourceSet.getResource(actualURI, true);
				Resource expectedOutput = resourceSet.getResource(expectedURI, true);
				
				EMFCompare comparator = EMFCompare.builder().build();
				IComparisonScope scope = EMFCompare.createDefaultScope(actualOutput, expectedOutput);
				Comparison diff = comparator.compare(scope);
				
				if(diff.getDifferences().size()==0) {
					continue;
				} else {
					flag = false;
					String str = exp.substring(0,exp.lastIndexOf(".xmi"));
					URI diffURI = URI.createPlatformResourceURI(str+"-diff.xmi", true);
					Resource res = resourceSet.createResource(diffURI);
					res.getContents().add(diff);
					try {
						res.save(null);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					System.out.println(diff);
				}
				
			}
			
			if(flag){
				matched = true;
				te.setPassed(true);
				te.eResource().setModified(true);
				break;
			}
			
		}
		
		return matched;
	}

//	private boolean checkExpectedOutput(URI outputURI, TestExecution e,
//			TestCase tc) throws InterruptedException {
//		
//		boolean matched = false;
//		
//		clearResource(outputURI);
//		
//		Resource actualOutput = resourceSet.getResource(outputURI, true);
//		
//		
//		for(String exp : tc.getOutputs()) {
//			URI expectedOutputURI = URI.createPlatformResourceURI(exp, true);
//			Resource expectedOutput = resourceSet.getResource(expectedOutputURI, true);
//			
////			IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
////			IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
////			IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
////			IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
////		    matchEngineFactory.setRanking(20);
////
////		    matchEngineRegistry.add(matchEngineFactory);
////			EMFCompare comparator = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();
//			
//			EMFCompare comparator = EMFCompare.builder().build();
//			IComparisonScope scope = EMFCompare.createDefaultScope(actualOutput, expectedOutput);
//			Comparison diff = comparator.compare(scope);
//			
////			MatchModel match = MatchService.doResourceMatch(actualOutput, expectedOutput, Collections.<String, Object> emptyMap());
////			DiffModel diff =  DiffService.doDiff(match);
//			if(diff.getDifferences().size()==0) {
//				e.setPassed(true);
//				e.eResource().setModified(true);
//				matched = true;
//				break;
//			} else {
//				String str = exp.substring(0,exp.lastIndexOf(".xmi"));
//				URI diffURI = URI.createPlatformResourceURI(str+"-diff.xmi", true);
//				Resource res = resourceSet.createResource(diffURI);
//				res.getContents().add(diff);
//				try {
//					res.save(null);
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//				System.out.println(diff);
//			}
//		}
//		return matched;
//	}

	private void clearResource(URI outputURI) {
		{
			int i=0;
			for(i=0;i<resourceSet.getResources().size();i++) {
				Resource res = resourceSet.getResources().get(i);
				if(res.getURI().equals(outputURI)) {
					break;
				}
			}
			if(i!=resourceSet.getResources().size())
				resourceSet.getResources().remove(i);
		}
	}
	
	protected int count = 0;
	
	protected ResourceSet resourceSet = new ResourceSetImpl();
	
	
	protected ILaunchConfiguration getLaunchConfiguration(String name) {
		ILaunchConfiguration[] launchConfigurations;
		try {
			launchConfigurations = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations();
			for(ILaunchConfiguration o : launchConfigurations) {
				if(name.equals(o.getName()))
					return o;
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
