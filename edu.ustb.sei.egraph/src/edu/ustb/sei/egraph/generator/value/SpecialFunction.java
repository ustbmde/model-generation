package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class SpecialFunction<T> extends AbstractRandomFunction<T> {
	
	@SuppressWarnings("rawtypes")
	static final public SpecialFunction emptyFunction = new SpecialFunction(Type.emptyFunction);
	@SuppressWarnings("rawtypes")
	static final public SpecialFunction nullFunction = new SpecialFunction(Type.nullFunction);
	
	enum Type {
		emptyFunction,
		nullFunction
	}

	private Type type;
	public SpecialFunction(Type type) {
		this.type = type;
	}

	@SuppressWarnings("unchecked")
	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		if(type==Type.nullFunction)
			return (NullValueFunction<T>)NullValueFunction.function;
		else if(type==Type.emptyFunction)
			return null;
		else return null;
	}
	
	static class NullValueFunction<T> extends RandomValueFunction<T> {

		@SuppressWarnings("rawtypes")
		static final NullValueFunction function = new NullValueFunction();
		
		@Override
		public T getValue(RandomService t) {
			return null;
		}
		
	}

}
