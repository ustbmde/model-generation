/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Round Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.TestRoundGroup#getRounds <em>Rounds</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestRoundGroup()
 * @model
 * @generated
 */
public interface TestRoundGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Rounds</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.TestRound}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounds</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounds</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestRoundGroup_Rounds()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestRound> getRounds();

} // TestRoundGroup
