package edu.ustb.sei.mt4mt.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class Context {
	public Context() {
		ePackages = new ArrayList<EPackage>();
		metamodelResourceSet = new ResourceSetImpl();
		metamodelResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		data = new HashMap<String,Object>();
	}
	
	private List<EPackage> ePackages ;
	private ResourceSet metamodelResourceSet ;
	
	public void addEPackage(EPackage e) {
		for(EPackage p : ePackages) {
			if(p.getNsURI().equals(e.getNsURI())) 
				return;
		}
		ePackages.add(e);
	}
	
	public void loadMetamodel(String uri) {
		URI mmuri = URI.createURI(uri);
		Resource mm = metamodelResourceSet.getResource(mmuri, true);
		EPackage pkg = (EPackage) mm.getContents().get(0);
		addEPackage(pkg);
	}
	
	public List<EPackage> getEPackages() {
		return ePackages;
	}
	
	private Map<String, Object> data;
	public <T> T getData(String key) {
		Object object = data.get(key);
		return (T)object;
	}
	public void putData(String key, Object d) {
		if(d==null) data.remove(key);
		else data.put(key, d);
	}
	
	public ResourceSet createResourceSet() {
		ResourceSet tempSet = new ResourceSetImpl();
		
		tempSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		for(EPackage p : getEPackages()) {
			tempSet.getPackageRegistry().put(p.getNsURI(), p);
		}
		return tempSet;
	}
}
