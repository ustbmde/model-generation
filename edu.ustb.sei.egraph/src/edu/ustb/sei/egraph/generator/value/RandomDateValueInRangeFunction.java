package edu.ustb.sei.egraph.generator.value;

import java.util.Date;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomDateValueInRangeFunction extends RandomNumValueInRangeFunction<Date> {
	
	public RandomDateValueInRangeFunction(Date min, Date max) {
		super(min,max);
	}

	@Override
	public Date getValue(RandomService t) {
		long maxT = max.getTime();
		long minT = min.getTime();
		
		long d = maxT<=minT ? minT : t.randomLong(minT, maxT);
		
		return new Date(d);
	}

}
