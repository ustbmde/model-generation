package edu.ustb.sei.rmg.util;

import java.util.concurrent.ConcurrentHashMap;

public class HashableConcurrentHashMap<K,V> extends ConcurrentHashMap<K, V> {
	private Object hashKey = new Object[]{};
	
	@Override
	public int hashCode() {
		return hashKey.hashCode();
	}
}
