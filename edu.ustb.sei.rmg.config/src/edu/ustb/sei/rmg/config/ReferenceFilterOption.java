/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Filter Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getSourceFilter <em>Source Filter</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getTargetFilter <em>Target Filter</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getReferenceRef <em>Reference Ref</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceFilterOption()
 * @model
 * @generated
 */
public interface ReferenceFilterOption extends EObject {
	/**
	 * Returns the value of the '<em><b>Source Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Filter</em>' containment reference.
	 * @see #setSourceFilter(Filter)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceFilterOption_SourceFilter()
	 * @model containment="true"
	 * @generated
	 */
	Filter getSourceFilter();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getSourceFilter <em>Source Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Filter</em>' containment reference.
	 * @see #getSourceFilter()
	 * @generated
	 */
	void setSourceFilter(Filter value);

	/**
	 * Returns the value of the '<em><b>Target Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Filter</em>' containment reference.
	 * @see #setTargetFilter(Filter)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceFilterOption_TargetFilter()
	 * @model containment="true"
	 * @generated
	 */
	Filter getTargetFilter();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getTargetFilter <em>Target Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Filter</em>' containment reference.
	 * @see #getTargetFilter()
	 * @generated
	 */
	void setTargetFilter(Filter value);

	/**
	 * Returns the value of the '<em><b>Reference Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Ref</em>' reference.
	 * @see #setReferenceRef(EReference)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceFilterOption_ReferenceRef()
	 * @model
	 * @generated
	 */
	EReference getReferenceRef();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getReferenceRef <em>Reference Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Ref</em>' reference.
	 * @see #getReferenceRef()
	 * @generated
	 */
	void setReferenceRef(EReference value);

} // ReferenceFilterOption
