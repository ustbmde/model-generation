package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomCharValueInRangeFunction extends RandomNumValueInRangeFunction<Character> {
	
	public RandomCharValueInRangeFunction(Character min, Character max) {
		super(min,max);
	}

	@Override
	public Character getValue(RandomService t) {
		return (char) t.randomInt(min, max+1);
	}

}
