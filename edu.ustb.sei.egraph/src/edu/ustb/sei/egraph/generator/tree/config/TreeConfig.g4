grammar TreeConfig;
import ValueSpec;

@header {
	import edu.ustb.sei.egraph.generator.value.*;
	import edu.ustb.sei.commonutil.util.Triple;
}

production :
  name=ID ':' type=ID ('<' strategy=('deep'|'wide') '>')? '{' (assignment (',' assignment)* )?'}'
;

assignment :
  attributeAssignment
| containmentAssignment
| crossEdgeAssignment
;

featureBound :
name=ID ('[' lower=INT ('..' upper=INT)? ']')?
;

attributeAssignment :
  featureBound '=' value=query
;

containmentAssignment :
  featureBound '=' rules+=application ('|' rules+=application)*
;

application :
  name=ID '(' (params+=query (',' params+=query)*)? ')'
;

crossEdgeAssignment :
  featureBound '=' '[' query ']'
;

query :
relationalExp
| relationalChild
;


variableExp :
name=('self'|ID)
;

constExp :
INT
| LONG
| STRING
| REAL
| BOOLEAN
;

randomExp :
'random' '(' (intSpec | longSpec | boolSpec | realSpec | stringSpec) ')'
;

navigationExp :
  root=navigatableExp path+=pathExp+
;

navigatableExp :
  variableExp
| randomExp
| constExp
| '(' query ')'
;

pathExp :
  '.' feature=ID
| '->' operator=('select'|'collect'|'forAll'|'exists') '(' var=ID '|' condition=query ')'
| '->' ('get'|'at') '(' index=query ')'
| '->' operator=('size'|'any') '(' ')'
;

logicalNotExp :
'not' logicalNotChild
;

logicalNotChild :
  atomExp
;

logicalAndExp :
 operands+=logicalAndChild ('and' operands+=logicalAndChild)+
;

logicalAndChild :
logicalNotExp
| logicalNotChild
;

logicalOrExp :
 operands+=logicalOrChild ('or' operands+=logicalOrChild)+
;

logicalOrChild:
logicalAndExp
| logicalAndChild
;

logicalImpliesExp :
logicalImpliesChild ('=>'|'implies') logicalImpliesChild
;

logicalImpliesChild :
logicalOrExp
| logicalOrChild
;

relationalExp :
relationalChild operator=('>'|'<'|'='|'>='|'<=') relationalChild
;

relationalChild :
logicalImpliesExp
| logicalImpliesChild
;

atomExp :
navigatableExp
| navigationExp
;