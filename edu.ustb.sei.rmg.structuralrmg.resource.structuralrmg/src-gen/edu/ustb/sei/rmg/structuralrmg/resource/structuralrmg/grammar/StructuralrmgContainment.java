/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public class StructuralrmgContainment extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgTerminal {
	
	private final org.eclipse.emf.ecore.EClass[] allowedTypes;
	
	public StructuralrmgContainment(org.eclipse.emf.ecore.EStructuralFeature feature, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality, org.eclipse.emf.ecore.EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public org.eclipse.emf.ecore.EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.explode(allowedTypes, ", ", new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgFunction1<String, org.eclipse.emf.ecore.EClass>() {
				public String execute(org.eclipse.emf.ecore.EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
