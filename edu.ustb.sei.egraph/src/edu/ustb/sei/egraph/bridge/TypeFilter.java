package edu.ustb.sei.egraph.bridge;

import java.util.HashSet;
import java.util.Set;

public class TypeFilter<NT, ET, NAT, EAT> {
	private Set<NT> loadedNodeTypes = new HashSet<NT>();
	private Set<ET> loadedEdgeTypes = new HashSet<ET>();
	private Set<NAT> loadedNodeAttrTypes = new HashSet<NAT>();
	private Set<EAT> loadedEdgeAttrTypes = new HashSet<EAT>();
	
	private Set<NT> ignoredNodeTypes =  new HashSet<NT>();
	private Set<ET> ignoredEdgeTypes =  new HashSet<ET>();
	private Set<NAT> ignoredNodeAttrTypes = new HashSet<NAT>();
	private Set<EAT> ignoredEdgeAttrTypes = new HashSet<EAT>();
	
	
	public void addLoadedNodeType(NT t) {
		loadedNodeTypes.add(t);
		ignoredNodeTypes.remove(t);
	}
	
	public void addLoadedEdgeType(ET t) {
		loadedEdgeTypes.add(t);
		ignoredEdgeTypes.remove(t);
	}
	
	public void addIgnoredNodeType(NT t) {
		ignoredNodeTypes.add(t);
		loadedNodeTypes.remove(t);
	}
	
	public void addIgnoredEdgeType(ET t) {
		ignoredEdgeTypes.add(t);
		loadedEdgeTypes.remove(t);
	}
	
	public void addLoadedAttrNodeType(NAT t) {
		loadedNodeAttrTypes.add(t);
		ignoredNodeAttrTypes.remove(t);
	}
	
	public void addLoadedAttrEdgeType(EAT t) {
		loadedEdgeAttrTypes.add(t);
		ignoredEdgeAttrTypes.remove(t);
	}
	
	public void addIgnoredAttrNodeType(NAT t) {
		ignoredNodeAttrTypes.add(t);
		loadedNodeAttrTypes.remove(t);
	}
	
	public void addIgnoredAttrEdgeType(EAT t) {
		ignoredEdgeAttrTypes.add(t);
		loadedEdgeAttrTypes.remove(t);
	}
	
	public void removeLoadedNodeType(NT t) {
		loadedNodeTypes.remove(t);
	}
	
	public void removeLoadedEdgeType(ET t) {
		loadedEdgeTypes.remove(t);
	}
	
	public void removeIgnoredNodeType(NT t) {
		ignoredNodeTypes.remove(t);
	}
	
	public void removeIgnoredEdgeType(ET t) {
		ignoredEdgeTypes.remove(t);
	}
	
	public void removeLoadedNodeAttrType(NT t) {
		loadedNodeAttrTypes.remove(t);
	}
	
	public void removeLoadedEdgeAttrType(ET t) {
		loadedEdgeAttrTypes.remove(t);
	}
	
	public void removeIgnoredNodeAttrType(NT t) {
		ignoredNodeAttrTypes.remove(t);
	}
	
	public void removeIgnoredEdgeAttrType(ET t) {
		ignoredEdgeAttrTypes.remove(t);
	}
	
	public boolean shouldLoadNodeOfType(NT t) {
		return !ignoredNodeTypes.contains(t) && (loadedNodeTypes.isEmpty() || loadedNodeTypes.contains(t));
	}
	
	public boolean shouldLoadEdgeOfType(ET t) {
		return !ignoredEdgeTypes.contains(t) && (loadedEdgeTypes.isEmpty() || loadedEdgeTypes.contains(t));
	}
	
	public boolean shouldLoadNodeAttrOfType(NAT t) {
		return !ignoredNodeAttrTypes.contains(t) && (loadedNodeAttrTypes.isEmpty() || loadedNodeAttrTypes.contains(t));
	}
	
	public boolean shouldLoadEdgeAttrOfType(EAT t) {
		return !ignoredEdgeAttrTypes.contains(t) && (loadedEdgeAttrTypes.isEmpty() || loadedEdgeAttrTypes.contains(t));
	}
	
	@SuppressWarnings("rawtypes")
	final public static  TypeFilter defaultFilter = new  TypeFilter();
	
}
