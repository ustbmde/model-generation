package edu.ustb.sei.mt4mt.base.updates;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

abstract public class DeleteObjectUpdate implements IUpdate {

	protected DeleteObjectUpdate(String hostClass, boolean typeOf, double possibility) {
		super();
		this.hostClass = hostClass;
		this.typeOf = typeOf;
		this.possibility = possibility;
	}
	
	protected boolean typeOf;
	protected String hostClass;
	protected double possibility;

	@Override
	public boolean apply(EObject o, EClass oClass, EPackage oPackage, RandomUtil util, Set<EObject> createdObjects) {
		if(checkTypeValidity(oClass) && createdObjects.contains(o)==false && checkObjectValidity(o, createdObjects)) {
			if(shouldDelete(o, util)) {
				doDeletion(o,util);
				return true;
			}
		}
		return false;
	}

	protected void doDeletion(EObject o, RandomUtil util) {
		EcoreUtil.delete(o);
	}

	protected boolean checkObjectValidity(EObject o, Set<EObject> createdObjects) {
		return true;
	}

	protected boolean shouldDelete(EObject o, RandomUtil util) {
		return util.decision(possibility);
	}

	protected boolean checkTypeValidity(EClass oClass) {
		return this.typeOf ? IUpdate.isTypeOf(oClass, hostClass):  IUpdate.isKindOf(oClass, this.hostClass);
	}

}
