package edu.ustb.sei.egraph.generator.util;

public class LexerUtil {
	static public String normalizeID(String id) {
		if(id==null) return null;
		if(id.charAt(0)=='#') return id.substring(1);
		else return id;
	}

	static public String normalizeURI(String uri) {
		return uri.substring(1,uri.length()-1);
	}
	
	static public boolean checkBoolean(String b) {
		try {
			return Boolean.parseBoolean(b);
		} catch(Exception e) {
			return false;
		}
	}
}
