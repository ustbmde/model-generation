package edu.ustb.sei.mt4mt.base.actions;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class SequenceAction implements IAction {
	public SequenceAction(IAction[] sequence) {
		super();
		this.sequence = sequence;
	}

	private IAction[] sequence;

	@Override
	public void run(Context context) {
		for(IAction a : sequence) {
			a.run(context);
		}
	}

}
