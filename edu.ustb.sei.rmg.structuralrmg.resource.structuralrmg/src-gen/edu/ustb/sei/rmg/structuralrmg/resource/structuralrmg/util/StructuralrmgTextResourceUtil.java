/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util;

/**
 * Class StructuralrmgTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResource
 * Util.
 */
public class StructuralrmgTextResourceUtil {
	
	/**
	 * Use
	 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResource
	 * Util.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource getResource(org.eclipse.core.resources.IFile file) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResource
	 * Util.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResource
	 * Util.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource getResource(org.eclipse.emf.common.util.URI uri) {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResource
	 * Util.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResourceUtil.getResource(uri, options);
	}
	
}
