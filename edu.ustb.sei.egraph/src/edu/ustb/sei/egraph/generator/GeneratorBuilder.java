package edu.ustb.sei.egraph.generator;


import edu.ustb.sei.egraph.generator.graph.GraphGenerator;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfig;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfigUtil;
import edu.ustb.sei.egraph.generator.graph.service.GraphGenerationService;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ETypeModel;

public class GeneratorBuilder {
	static public <NT,ET,NAT,EAT> GraphGenerator<NT,ET,NAT,EAT> buildGraphGenerator(long seed, ETypeModel<NT,ET,NAT,EAT> typeModel, String config) {
		EGraph<NT,ET,NAT,EAT> graph = new EGraph<NT,ET,NAT,EAT>();
		graph.setTypeModel(typeModel);
		GraphConfigUtil g = new GraphConfigUtil();
		GraphConfig r = g.parse(config).root;
		GraphGenerationService<NT,ET,NAT,EAT> service = new GraphGenerationService<NT,ET,NAT,EAT>(seed, graph, r);
		GraphGenerator<NT,ET,NAT,EAT> generator = new GraphGenerator<NT, ET, NAT, EAT>(service);
		return generator;
	}
	
	static public <NT,ET,NAT,EAT> GraphGenerator<NT,ET,NAT,EAT> buildGraphGenerator(ETypeModel<NT,ET,NAT,EAT> typeModel, String config) {
		return buildGraphGenerator(System.currentTimeMillis(), typeModel,config);
	}

}
