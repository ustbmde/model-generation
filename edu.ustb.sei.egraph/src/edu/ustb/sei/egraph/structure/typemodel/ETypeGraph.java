package edu.ustb.sei.egraph.structure.typemodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;


public class ETypeGraph extends EGraph<String, String, String, String>
		implements ETypeModel<ENode<String, String, String>, EEdge<String, String, String>, String, String> {
	
	// meta meta types
	public static final String ABSTRACT = "__ABSTRACT__";
	public static final String TARGET_UPPER = "__TARGET__UPPER__";
	public static final String SOURCE_UPPER = "__SOURCE__UPPER__";
	public static final String TARGET_LOWER = "__TARGET__LOWER__";
	public static final String SOURCE_LOWER = "__SOURCE__LOWER__";
	final static public String SUPERTYPE = "__SUPER__TYPE__";
	final static public String VALID = "__VALID__";
	final static public String CONTAINMENT = "__CONTAINMENT__";
	final static public String NAME = "__NAME__";
	final static public String NODETYPE = "__NODE_TYPE__"; // type of a node type
	final static public String EDGETYPE = "__EDGE_TYPE__"; // type of an edge type
	
	final static public String[] metaTypes = new String[]{NODETYPE,EDGETYPE,SUPERTYPE};
	final static public String[] metaNodeAttributes = new String[]{NAME,ABSTRACT};
	final static public String[] metaEdgeAttributes = new String[]{NAME,CONTAINMENT,SOURCE_LOWER,TARGET_LOWER,SOURCE_UPPER,TARGET_UPPER};
	
	private Map<ENode<String, String, String>,Collection<ENode<String, String, String>>> upstreamNodeTypes = null;
	private Map<ENode<String, String, String>,Collection<ENode<String, String, String>>> downstreamNodeTypes = null;
	
	private List<EEdge<String,String,String>> containmentEdgeTypes = null;
	private Map<ENode<String,String,String>,List<EEdge<String,String,String>>> containmentEdgeTypesOfNodeType = null;
	private Map<ENode<String, String, String>,Map<ENode<String, String, String>,Boolean>> hasCommonSubType;
	
	
	
	public ETypeGraph() {
		super(64,64);
		upstreamNodeTypes = new HashMap<ENode<String, String, String>,Collection<ENode<String, String, String>>>();
		downstreamNodeTypes = new HashMap<ENode<String, String, String>,Collection<ENode<String, String, String>>>();
		containmentEdgeTypes = new ArrayList<EEdge<String,String,String>>();
		containmentEdgeTypesOfNodeType = new HashMap<ENode<String,String,String>,List<EEdge<String,String,String>>>();
		hasCommonSubType = new HashMap<ENode<String,String,String>, Map<ENode<String,String,String>,Boolean>>();
	}


	@Override
	public void addNodeType(ENode<String, String, String> n) {
		this.addNode(n);
	}

	@Override
	public void addEdgeType(EEdge<String, String, String> e) {
		this.addEdge(e);
		Boolean containment = e.get("__CONTAINMENT__");
		if(containment!=null && containment==true) {
			this.containmentEdgeTypes.add(e);
		}

	}

	@Override
	public void addNodeAttrType(ENode<String, String, String> n, String a) {
		n.put(a, VALID);
	}

	@Override
	public void addEdgeAttrType(EEdge<String, String, String> n, String a) {
		n.put(a, VALID);
	}

	@Override
	public void addParent(ENode<String, String, String> child, ENode<String, String, String> parent) {
		EEdge<String,String,String> sup = new EEdge<String, String, String>(SUPERTYPE, child, parent);
		this.addEdge(sup);
	}
	
	

	@SuppressWarnings("unchecked")
	public Collection<ENode<String, String, String>> collectAllSuperTypes(
			ENode<String, String, String> s) {
		Collection<ENode<String, String, String>> up = this.upstreamNodeTypes.get(s);
		if(up==null) {
			up = new HashSet<ENode<String, String, String>>();
			up.add(s);
			
			Collection<EEdge<String,String,String>> supers = getOutgoings(s, SUPERTYPE);
			for(EEdge<String,String,String> e : supers) {
				up.addAll(collectAllSuperTypes((ENode<String, String, String>) e.getTarget()));
			}
			
			this.upstreamNodeTypes.put(s, up);
		}
		return up;
	}

	@SuppressWarnings("unchecked")
	public Collection<ENode<String, String, String>> collectAllConcreteSubTypes(
			ENode<String, String, String> t) {
		Collection<ENode<String, String, String>> down = this.downstreamNodeTypes.get(t);
		if(down==null) {
			down = new HashSet<ENode<String, String, String>>();
			
			if(this.isAbstract(t)==false) down.add(t);
			
			// navigate backward
			Collection<EEdge<String,String,String>> subtypes = getIncomings(t, SUPERTYPE);
			
			for(EEdge<String,String,String> e : subtypes) {
				down.addAll(collectAllConcreteSubTypes((ENode<String, String, String>) e.getSource()));
			}
			
			this.downstreamNodeTypes.put(t, down);
		}
		return down;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENode<String, String, String> getSource(EEdge<String, String, String> e) {
		return (ENode<String, String, String>) e.getSource();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENode<String, String, String> getTarget(EEdge<String, String, String> e) {
		return (ENode<String, String, String>) e.getTarget();
	}

	@Override
	public int getSourceLowerBound(EEdge<String, String, String> e) {
		Integer i = e.get(SOURCE_LOWER);
		if(i==null)
			return 0;
		else return i;
	}

	@Override
	public int getTargetLowerBound(EEdge<String, String, String> e) {
		Integer i = e.get(TARGET_LOWER);
		if(i==null)
			return 0;
		else return i;
	}

	@Override
	public int getSourceUpperBound(EEdge<String, String, String> e) {
		Integer i = e.get(SOURCE_UPPER);
		if(i==null)
			return 0;
		else return Integer.MAX_VALUE;
	}

	@Override
	public int getTargetUpperBound(EEdge<String, String, String> e) {
		Integer i = e.get(TARGET_UPPER);
		if(i==null)
			return 0;
		else return Integer.MAX_VALUE;
	}

	@Override
	public Collection<ENode<String, String, String>> allNodeTypes() {
		return this.getNodes();
	}

	@Override
	public Collection<EEdge<String, String, String>> allEdgeTypes() {
		return this.getEdges();
	}

	@Override
	public boolean isAbstract(ENode<String, String, String> t) {
		Boolean b = t.get(ABSTRACT);
		if(b!=null)
			return b;
		else
			return false;
	}

	@Override
	public List<EEdge<String,String,String>> getContainerReferenceForClass(ENode<String, String, String> t) {
		List<EEdge<String,String,String>> list = containmentEdgeTypesOfNodeType.get(t);
		
		if(list==null) {
			List<EEdge<String,String,String>> allContainments = this.collectAllContainmentReferences();
			list = new ArrayList<EEdge<String,String,String>>();
			
			for(EEdge<String,String,String> e : allContainments) {
				ENode<String,String,String> tar = this.getTarget(e);
				if(this.isSuperNodeType(t, tar))
					list.add(e);
			}
			containmentEdgeTypesOfNodeType.put(t, list);
		}
		
		return list;
	}
	
	@Override
	public List<EEdge<String, String, String>> collectAllContainmentReferences() {
		return this.containmentEdgeTypes;
	}
	
	@Override
	public boolean hasCommonSubTypes(ENode<String, String, String> t1, ENode<String, String, String> t2) {
		Map<ENode<String, String, String>,Boolean> map = this.hasCommonSubType.get(t1);
		Boolean flag = null;
		if(map==null || (flag=map.get(t2))==null) {
			flag = computeHasCommonSubTypes(t1,t2);
		}
		return flag;
	}
	
	private boolean computeHasCommonSubTypes(ENode<String, String, String> t1, ENode<String, String, String> t2) {
		Map<ENode<String, String, String>,Boolean> map1 = this.hasCommonSubType.get(t1);
		Map<ENode<String, String, String>,Boolean> map2 = this.hasCommonSubType.get(t1);
		
		if(map1==null) {
			map1 = new HashMap<ENode<String, String, String>, Boolean>();
			this.hasCommonSubType.put(t1, map1);
		}
		
		if(map2==null) {
			map2 = new HashMap<ENode<String, String, String>, Boolean>();
			this.hasCommonSubType.put(t2, map2);
		}
		
		Collection<ENode<String, String, String>> ts1 = this.collectAllConcreteSubTypes(t1);
		Collection<ENode<String, String, String>> ts2 = this.collectAllConcreteSubTypes(t2);
		
		Boolean flag = !Collections.disjoint(ts1, ts2);
		map1.put(t2, flag);
		map2.put(t1, flag);
		
		return flag;
	}


	@Override
	public Collection<String> getAttributesOfNodeType(ENode<String, String, String> type) {
		Set<String> attributes = new HashSet<String>();
		type.allProperties().forEach((e)->{
			if(e.getValue()==VALID) 
				attributes.add(e.getKey());
			});
		return attributes;
	}


	@Override
	public Collection<String> getAttributesOfEdgeType(EEdge<String, String, String> type) {
		Set<String> attributes = new HashSet<String>();
		type.allProperties().forEach((e)->{
			if(e.getValue()==VALID)
				attributes.add(e.getKey());
			});
		return attributes;
	}
	
	@Override
	public Collection<EEdge<String, String, String>> getOutgoingEdgeTypesOfNodeType(
			ENode<String, String, String> source) {
		return this.getOutgoings(source, EDGETYPE);
	}
	
	@Override
	public Collection<EEdge<String, String, String>> getIncomingEdgeTypesOfNodeType(
			ENode<String, String, String> target) {
		return this.getIncomings(target, EDGETYPE);
	}
	
	// resolving
	@Override
	public ENode<String, String, String> resolveNodeType(QualifiedName name) {
		for(ENode<String,String,String> t : this.getNodes()) {
			if(name.name.equals(t.get(NAME)))
				return t;
		}
		return null;
	}
	
	@Override
	public QualifiedName desolveNodeType(ENode<String, String, String> type) {
		return new QualifiedName((String)type.get(NAME),QualifiedName.defaultType);
	}
	
	@Override
	public EEdge<String, String, String> resolveEdgeType(QualifiedName name) {
		ENode<String, String, String> source = this.resolveNodeType(new QualifiedName(name.type,QualifiedName.defaultType));
		if(source!=null) {
			for(EEdge<String, String, String> e : this.getAllOutgoingEdgeTypesOfNodeType(source)) {
				if(name.name.equals(e.get(NAME)))
					return e;
			}
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public QualifiedName desolveEdgeType(EEdge<String, String, String> type) {
		ENode<String, String, String> source = (ENode<String, String, String>) type.getSource();
		return new QualifiedName(type.get(NAME), source.get(NAME));
	}
	
	@Override
	public QualifiedName desolveNodeAttribute(String attr) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public String resolveNodeAttribute(QualifiedName name) {
		ENode<String, String, String> source = this.resolveNodeType(new QualifiedName(name.type,QualifiedName.defaultType));
		if(source!=null) {
			for(String a : this.getAllAttributesOfNodeType(source))
				if(a.equals(name.name))
					return a;
		}
		return null;
	}

	static public ENode<String,String,String> createNodeType(String name,boolean isAbstract) {
		ENode<String,String,String> n = new ENode<String,String,String>(NODETYPE);
		n.put(NAME, name);
		n.put(ABSTRACT, isAbstract);
		return n;
	}
	
	static public EEdge<String,String,String> createEdgeType(String name, ENode<String,String,String> s, ENode<String,String,String> t) {
		EEdge<String, String, String> e = new EEdge<String, String, String>(EDGETYPE, s, t);
		e.put(NAME, name);
		return e;
	}
}
