package edu.ustb.sei.rmg.strategy;

import java.util.Random;

import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.random.RandomUtil;

public interface IStringGenerationStrategy {
	String generateString(RandomUtil rdm, String scope, Object[] size);
	
	final public static IStringGenerationStrategy defaultStrategy = new DefaultStringGenerationStrategy();
}

class DefaultStringGenerationStrategy implements IStringGenerationStrategy{

	@Override
	public String generateString(RandomUtil rdm, String scope, Object[] size) {
		int l = (int) size[0];
		int u = (int) size[1];
		
		int s = rdm.randomUInt(l,u);
		
		char[] buf = new char[s];
		for(int i=0;i<s;i++){
			buf[i] = scope.charAt(rdm.randomUIntU(scope.length()));
		}
		
		return new String(buf);
	}
	
}