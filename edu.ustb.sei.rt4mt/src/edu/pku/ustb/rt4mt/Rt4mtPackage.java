/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.pku.ustb.rt4mt.Rt4mtFactory
 * @model kind="package"
 * @generated
 */
public interface Rt4mtPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "rt4mt";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "edu.ustb.sei.rt4mt";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "rt4mt";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Rt4mtPackage eINSTANCE = edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl <em>Regression Test Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getRegressionTestModel()
	 * @generated
	 */
	int REGRESSION_TEST_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Input Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__INPUT_BASE = 1;

	/**
	 * The feature id for the '<em><b>Output Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__OUTPUT_BASE = 2;

	/**
	 * The feature id for the '<em><b>Run Configuration Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME = 3;

	/**
	 * The feature id for the '<em><b>Input</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__INPUT = 4;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__OUTPUT = 5;

	/**
	 * The feature id for the '<em><b>Test Case Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__TEST_CASE_GROUP = 6;

	/**
	 * The feature id for the '<em><b>Test Round Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL__TEST_ROUND_GROUP = 7;

	/**
	 * The number of structural features of the '<em>Regression Test Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGRESSION_TEST_MODEL_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestCaseImpl <em>Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestCaseImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestCase()
	 * @generated
	 */
	int TEST_CASE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__ID = 0;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__INPUTS = 1;

	/**
	 * The feature id for the '<em><b>Expected Outputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__EXPECTED_OUTPUTS = 2;

	/**
	 * The number of structural features of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestRoundImpl <em>Test Round</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestRoundImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestRound()
	 * @generated
	 */
	int TEST_ROUND = 2;

	/**
	 * The feature id for the '<em><b>Round Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ROUND__ROUND_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Test Executions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ROUND__TEST_EXECUTIONS = 1;

	/**
	 * The number of structural features of the '<em>Test Round</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ROUND_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestExecutionImpl <em>Test Execution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestExecutionImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestExecution()
	 * @generated
	 */
	int TEST_EXECUTION = 3;

	/**
	 * The feature id for the '<em><b>Test Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_EXECUTION__TEST_CASE = 0;

	/**
	 * The feature id for the '<em><b>Passed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_EXECUTION__PASSED = 1;

	/**
	 * The number of structural features of the '<em>Test Execution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_EXECUTION_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestCaseGroupImpl <em>Test Case Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestCaseGroupImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestCaseGroup()
	 * @generated
	 */
	int TEST_CASE_GROUP = 4;

	/**
	 * The feature id for the '<em><b>Test Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP__TEST_CASES = 0;

	/**
	 * The number of structural features of the '<em>Test Case Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_GROUP_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestRoundGroupImpl <em>Test Round Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestRoundGroupImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestRoundGroup()
	 * @generated
	 */
	int TEST_ROUND_GROUP = 5;

	/**
	 * The feature id for the '<em><b>Rounds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ROUND_GROUP__ROUNDS = 0;

	/**
	 * The number of structural features of the '<em>Test Round Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_ROUND_GROUP_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.InputParameterConfigurationGroupImpl <em>Input Parameter Configuration Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.InputParameterConfigurationGroupImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getInputParameterConfigurationGroup()
	 * @generated
	 */
	int INPUT_PARAMETER_CONFIGURATION_GROUP = 6;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>Input Parameter Configuration Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION_GROUP_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationGroupImpl <em>Output Parameter Configuration Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationGroupImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getOutputParameterConfigurationGroup()
	 * @generated
	 */
	int OUTPUT_PARAMETER_CONFIGURATION_GROUP = 7;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>Output Parameter Configuration Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION_GROUP_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.ParameterConfigurationImpl <em>Parameter Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.ParameterConfigurationImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getParameterConfiguration()
	 * @generated
	 */
	int PARAMETER_CONFIGURATION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CONFIGURATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CONFIGURATION__MODEL_URI = 1;

	/**
	 * The number of structural features of the '<em>Parameter Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CONFIGURATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.InputParameterConfigurationImpl <em>Input Parameter Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.InputParameterConfigurationImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getInputParameterConfiguration()
	 * @generated
	 */
	int INPUT_PARAMETER_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION__NAME = PARAMETER_CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION__MODEL_URI = PARAMETER_CONFIGURATION__MODEL_URI;

	/**
	 * The feature id for the '<em><b>Rmg Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI = PARAMETER_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Input Parameter Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PARAMETER_CONFIGURATION_FEATURE_COUNT = PARAMETER_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationImpl <em>Output Parameter Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getOutputParameterConfiguration()
	 * @generated
	 */
	int OUTPUT_PARAMETER_CONFIGURATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION__NAME = PARAMETER_CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION__MODEL_URI = PARAMETER_CONFIGURATION__MODEL_URI;

	/**
	 * The feature id for the '<em><b>Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION__METAMODEL_URI = PARAMETER_CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Output Parameter Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PARAMETER_CONFIGURATION_FEATURE_COUNT = PARAMETER_CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.TestDataImpl <em>Test Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.TestDataImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestData()
	 * @generated
	 */
	int TEST_DATA = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_DATA__NAME = 0;

	/**
	 * The feature id for the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_DATA__MODEL_URI = 1;

	/**
	 * The number of structural features of the '<em>Test Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_DATA_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link edu.pku.ustb.rt4mt.impl.ExpectedOutputImpl <em>Expected Output</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.pku.ustb.rt4mt.impl.ExpectedOutputImpl
	 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getExpectedOutput()
	 * @generated
	 */
	int EXPECTED_OUTPUT = 12;

	/**
	 * The feature id for the '<em><b>Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTED_OUTPUT__DATA = 0;

	/**
	 * The number of structural features of the '<em>Expected Output</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTED_OUTPUT_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.RegressionTestModel <em>Regression Test Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regression Test Model</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel
	 * @generated
	 */
	EClass getRegressionTestModel();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getName()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EAttribute getRegressionTestModel_Name();

	/**
	 * Returns the meta object for the containment reference '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getInput()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EReference getRegressionTestModel_Input();

	/**
	 * Returns the meta object for the containment reference '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getOutput()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EReference getRegressionTestModel_Output();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInputBase <em>Input Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Base</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getInputBase()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EAttribute getRegressionTestModel_InputBase();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutputBase <em>Output Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Base</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getOutputBase()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EAttribute getRegressionTestModel_OutputBase();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getRunConfigurationName <em>Run Configuration Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Run Configuration Name</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getRunConfigurationName()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EAttribute getRegressionTestModel_RunConfigurationName();

	/**
	 * Returns the meta object for the containment reference '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Case Group</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getTestCaseGroup()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EReference getRegressionTestModel_TestCaseGroup();

	/**
	 * Returns the meta object for the containment reference '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestRoundGroup <em>Test Round Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Test Round Group</em>'.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel#getTestRoundGroup()
	 * @see #getRegressionTestModel()
	 * @generated
	 */
	EReference getRegressionTestModel_TestRoundGroup();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCase
	 * @generated
	 */
	EClass getTestCase();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.TestCase#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCase#getId()
	 * @see #getTestCase()
	 * @generated
	 */
	EAttribute getTestCase_Id();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.TestCase#getInputs <em>Inputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inputs</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCase#getInputs()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Inputs();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.TestCase#getExpectedOutputs <em>Expected Outputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expected Outputs</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCase#getExpectedOutputs()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_ExpectedOutputs();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestRound <em>Test Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Round</em>'.
	 * @see edu.pku.ustb.rt4mt.TestRound
	 * @generated
	 */
	EClass getTestRound();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.TestRound#getRoundNumber <em>Round Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Round Number</em>'.
	 * @see edu.pku.ustb.rt4mt.TestRound#getRoundNumber()
	 * @see #getTestRound()
	 * @generated
	 */
	EAttribute getTestRound_RoundNumber();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.TestRound#getTestExecutions <em>Test Executions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Executions</em>'.
	 * @see edu.pku.ustb.rt4mt.TestRound#getTestExecutions()
	 * @see #getTestRound()
	 * @generated
	 */
	EReference getTestRound_TestExecutions();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestExecution <em>Test Execution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Execution</em>'.
	 * @see edu.pku.ustb.rt4mt.TestExecution
	 * @generated
	 */
	EClass getTestExecution();

	/**
	 * Returns the meta object for the reference '{@link edu.pku.ustb.rt4mt.TestExecution#getTestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Case</em>'.
	 * @see edu.pku.ustb.rt4mt.TestExecution#getTestCase()
	 * @see #getTestExecution()
	 * @generated
	 */
	EReference getTestExecution_TestCase();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.TestExecution#isPassed <em>Passed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Passed</em>'.
	 * @see edu.pku.ustb.rt4mt.TestExecution#isPassed()
	 * @see #getTestExecution()
	 * @generated
	 */
	EAttribute getTestExecution_Passed();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case Group</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCaseGroup
	 * @generated
	 */
	EClass getTestCaseGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.TestCaseGroup#getTestCases <em>Test Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test Cases</em>'.
	 * @see edu.pku.ustb.rt4mt.TestCaseGroup#getTestCases()
	 * @see #getTestCaseGroup()
	 * @generated
	 */
	EReference getTestCaseGroup_TestCases();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestRoundGroup <em>Test Round Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Round Group</em>'.
	 * @see edu.pku.ustb.rt4mt.TestRoundGroup
	 * @generated
	 */
	EClass getTestRoundGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.TestRoundGroup#getRounds <em>Rounds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rounds</em>'.
	 * @see edu.pku.ustb.rt4mt.TestRoundGroup#getRounds()
	 * @see #getTestRoundGroup()
	 * @generated
	 */
	EReference getTestRoundGroup_Rounds();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.InputParameterConfigurationGroup <em>Input Parameter Configuration Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Parameter Configuration Group</em>'.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfigurationGroup
	 * @generated
	 */
	EClass getInputParameterConfigurationGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.InputParameterConfigurationGroup#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfigurationGroup#getParameters()
	 * @see #getInputParameterConfigurationGroup()
	 * @generated
	 */
	EReference getInputParameterConfigurationGroup_Parameters();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup <em>Output Parameter Configuration Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Parameter Configuration Group</em>'.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup
	 * @generated
	 */
	EClass getOutputParameterConfigurationGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup#getParameters()
	 * @see #getOutputParameterConfigurationGroup()
	 * @generated
	 */
	EReference getOutputParameterConfigurationGroup_Parameters();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.ParameterConfiguration <em>Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Configuration</em>'.
	 * @see edu.pku.ustb.rt4mt.ParameterConfiguration
	 * @generated
	 */
	EClass getParameterConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.ParameterConfiguration#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.pku.ustb.rt4mt.ParameterConfiguration#getName()
	 * @see #getParameterConfiguration()
	 * @generated
	 */
	EAttribute getParameterConfiguration_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.ParameterConfiguration#getModelURI <em>Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model URI</em>'.
	 * @see edu.pku.ustb.rt4mt.ParameterConfiguration#getModelURI()
	 * @see #getParameterConfiguration()
	 * @generated
	 */
	EAttribute getParameterConfiguration_ModelURI();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.InputParameterConfiguration <em>Input Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Parameter Configuration</em>'.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfiguration
	 * @generated
	 */
	EClass getInputParameterConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.InputParameterConfiguration#getRmgModelURI <em>Rmg Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rmg Model URI</em>'.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfiguration#getRmgModelURI()
	 * @see #getInputParameterConfiguration()
	 * @generated
	 */
	EAttribute getInputParameterConfiguration_RmgModelURI();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.OutputParameterConfiguration <em>Output Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Parameter Configuration</em>'.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfiguration
	 * @generated
	 */
	EClass getOutputParameterConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.OutputParameterConfiguration#getMetamodelURI <em>Metamodel URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel URI</em>'.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfiguration#getMetamodelURI()
	 * @see #getOutputParameterConfiguration()
	 * @generated
	 */
	EAttribute getOutputParameterConfiguration_MetamodelURI();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.TestData <em>Test Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Data</em>'.
	 * @see edu.pku.ustb.rt4mt.TestData
	 * @generated
	 */
	EClass getTestData();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.TestData#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.pku.ustb.rt4mt.TestData#getName()
	 * @see #getTestData()
	 * @generated
	 */
	EAttribute getTestData_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.pku.ustb.rt4mt.TestData#getModelURI <em>Model URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model URI</em>'.
	 * @see edu.pku.ustb.rt4mt.TestData#getModelURI()
	 * @see #getTestData()
	 * @generated
	 */
	EAttribute getTestData_ModelURI();

	/**
	 * Returns the meta object for class '{@link edu.pku.ustb.rt4mt.ExpectedOutput <em>Expected Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expected Output</em>'.
	 * @see edu.pku.ustb.rt4mt.ExpectedOutput
	 * @generated
	 */
	EClass getExpectedOutput();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.pku.ustb.rt4mt.ExpectedOutput#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data</em>'.
	 * @see edu.pku.ustb.rt4mt.ExpectedOutput#getData()
	 * @see #getExpectedOutput()
	 * @generated
	 */
	EReference getExpectedOutput_Data();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Rt4mtFactory getRt4mtFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl <em>Regression Test Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getRegressionTestModel()
		 * @generated
		 */
		EClass REGRESSION_TEST_MODEL = eINSTANCE.getRegressionTestModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGRESSION_TEST_MODEL__NAME = eINSTANCE.getRegressionTestModel_Name();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGRESSION_TEST_MODEL__INPUT = eINSTANCE.getRegressionTestModel_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGRESSION_TEST_MODEL__OUTPUT = eINSTANCE.getRegressionTestModel_Output();

		/**
		 * The meta object literal for the '<em><b>Input Base</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGRESSION_TEST_MODEL__INPUT_BASE = eINSTANCE.getRegressionTestModel_InputBase();

		/**
		 * The meta object literal for the '<em><b>Output Base</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGRESSION_TEST_MODEL__OUTPUT_BASE = eINSTANCE.getRegressionTestModel_OutputBase();

		/**
		 * The meta object literal for the '<em><b>Run Configuration Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME = eINSTANCE.getRegressionTestModel_RunConfigurationName();

		/**
		 * The meta object literal for the '<em><b>Test Case Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGRESSION_TEST_MODEL__TEST_CASE_GROUP = eINSTANCE.getRegressionTestModel_TestCaseGroup();

		/**
		 * The meta object literal for the '<em><b>Test Round Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGRESSION_TEST_MODEL__TEST_ROUND_GROUP = eINSTANCE.getRegressionTestModel_TestRoundGroup();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestCaseImpl <em>Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestCaseImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestCase()
		 * @generated
		 */
		EClass TEST_CASE = eINSTANCE.getTestCase();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE__ID = eINSTANCE.getTestCase_Id();

		/**
		 * The meta object literal for the '<em><b>Inputs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__INPUTS = eINSTANCE.getTestCase_Inputs();

		/**
		 * The meta object literal for the '<em><b>Expected Outputs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__EXPECTED_OUTPUTS = eINSTANCE.getTestCase_ExpectedOutputs();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestRoundImpl <em>Test Round</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestRoundImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestRound()
		 * @generated
		 */
		EClass TEST_ROUND = eINSTANCE.getTestRound();

		/**
		 * The meta object literal for the '<em><b>Round Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_ROUND__ROUND_NUMBER = eINSTANCE.getTestRound_RoundNumber();

		/**
		 * The meta object literal for the '<em><b>Test Executions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_ROUND__TEST_EXECUTIONS = eINSTANCE.getTestRound_TestExecutions();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestExecutionImpl <em>Test Execution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestExecutionImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestExecution()
		 * @generated
		 */
		EClass TEST_EXECUTION = eINSTANCE.getTestExecution();

		/**
		 * The meta object literal for the '<em><b>Test Case</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_EXECUTION__TEST_CASE = eINSTANCE.getTestExecution_TestCase();

		/**
		 * The meta object literal for the '<em><b>Passed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_EXECUTION__PASSED = eINSTANCE.getTestExecution_Passed();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestCaseGroupImpl <em>Test Case Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestCaseGroupImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestCaseGroup()
		 * @generated
		 */
		EClass TEST_CASE_GROUP = eINSTANCE.getTestCaseGroup();

		/**
		 * The meta object literal for the '<em><b>Test Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_GROUP__TEST_CASES = eINSTANCE.getTestCaseGroup_TestCases();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestRoundGroupImpl <em>Test Round Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestRoundGroupImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestRoundGroup()
		 * @generated
		 */
		EClass TEST_ROUND_GROUP = eINSTANCE.getTestRoundGroup();

		/**
		 * The meta object literal for the '<em><b>Rounds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_ROUND_GROUP__ROUNDS = eINSTANCE.getTestRoundGroup_Rounds();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.InputParameterConfigurationGroupImpl <em>Input Parameter Configuration Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.InputParameterConfigurationGroupImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getInputParameterConfigurationGroup()
		 * @generated
		 */
		EClass INPUT_PARAMETER_CONFIGURATION_GROUP = eINSTANCE.getInputParameterConfigurationGroup();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS = eINSTANCE.getInputParameterConfigurationGroup_Parameters();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationGroupImpl <em>Output Parameter Configuration Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationGroupImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getOutputParameterConfigurationGroup()
		 * @generated
		 */
		EClass OUTPUT_PARAMETER_CONFIGURATION_GROUP = eINSTANCE.getOutputParameterConfigurationGroup();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUTPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS = eINSTANCE.getOutputParameterConfigurationGroup_Parameters();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.ParameterConfigurationImpl <em>Parameter Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.ParameterConfigurationImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getParameterConfiguration()
		 * @generated
		 */
		EClass PARAMETER_CONFIGURATION = eINSTANCE.getParameterConfiguration();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_CONFIGURATION__NAME = eINSTANCE.getParameterConfiguration_Name();

		/**
		 * The meta object literal for the '<em><b>Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_CONFIGURATION__MODEL_URI = eINSTANCE.getParameterConfiguration_ModelURI();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.InputParameterConfigurationImpl <em>Input Parameter Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.InputParameterConfigurationImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getInputParameterConfiguration()
		 * @generated
		 */
		EClass INPUT_PARAMETER_CONFIGURATION = eINSTANCE.getInputParameterConfiguration();

		/**
		 * The meta object literal for the '<em><b>Rmg Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI = eINSTANCE.getInputParameterConfiguration_RmgModelURI();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationImpl <em>Output Parameter Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.OutputParameterConfigurationImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getOutputParameterConfiguration()
		 * @generated
		 */
		EClass OUTPUT_PARAMETER_CONFIGURATION = eINSTANCE.getOutputParameterConfiguration();

		/**
		 * The meta object literal for the '<em><b>Metamodel URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_PARAMETER_CONFIGURATION__METAMODEL_URI = eINSTANCE.getOutputParameterConfiguration_MetamodelURI();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.TestDataImpl <em>Test Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.TestDataImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getTestData()
		 * @generated
		 */
		EClass TEST_DATA = eINSTANCE.getTestData();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_DATA__NAME = eINSTANCE.getTestData_Name();

		/**
		 * The meta object literal for the '<em><b>Model URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_DATA__MODEL_URI = eINSTANCE.getTestData_ModelURI();

		/**
		 * The meta object literal for the '{@link edu.pku.ustb.rt4mt.impl.ExpectedOutputImpl <em>Expected Output</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.pku.ustb.rt4mt.impl.ExpectedOutputImpl
		 * @see edu.pku.ustb.rt4mt.impl.Rt4mtPackageImpl#getExpectedOutput()
		 * @generated
		 */
		EClass EXPECTED_OUTPUT = eINSTANCE.getExpectedOutput();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPECTED_OUTPUT__DATA = eINSTANCE.getExpectedOutput_Data();

	}

} //Rt4mtPackage
