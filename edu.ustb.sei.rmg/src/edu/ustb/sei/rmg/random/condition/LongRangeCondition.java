package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class LongRangeCondition extends RangeCondition<Long> {

	@Override
	protected Long randomLU(RandomUtil r) {
		return r.randomLong(lower, upper);
	}

	@Override
	protected Long randomL(RandomUtil r) {
		return r.randomLongL(lower);
	}

	@Override
	protected Long randomU(RandomUtil r) {
		return r.randomLongU(upper);
	}


}
