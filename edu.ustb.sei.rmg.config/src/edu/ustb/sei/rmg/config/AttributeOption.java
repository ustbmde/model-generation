/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.AttributeOption#getAttributeRef <em>Attribute Ref</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAttributeOption()
 * @model
 * @generated
 */
public interface AttributeOption extends Condition {
	/**
	 * Returns the value of the '<em><b>Attribute Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Ref</em>' reference.
	 * @see #setAttributeRef(EAttribute)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getAttributeOption_AttributeRef()
	 * @model
	 * @generated
	 */
	EAttribute getAttributeRef();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.AttributeOption#getAttributeRef <em>Attribute Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Ref</em>' reference.
	 * @see #getAttributeRef()
	 * @generated
	 */
	void setAttributeRef(EAttribute value);

} // AttributeOption
