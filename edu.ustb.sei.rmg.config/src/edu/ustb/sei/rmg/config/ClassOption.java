/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.ClassOption#getClassRef <em>Class Ref</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.ClassOption#getAttributeOptions <em>Attribute Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.ClassOption#getReferenceOptions <em>Reference Options</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getClassOption()
 * @model
 * @generated
 */
public interface ClassOption extends ScalableCondition {
	/**
	 * Returns the value of the '<em><b>Class Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Ref</em>' reference.
	 * @see #setClassRef(EClass)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getClassOption_ClassRef()
	 * @model
	 * @generated
	 */
	EClass getClassRef();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ClassOption#getClassRef <em>Class Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Ref</em>' reference.
	 * @see #getClassRef()
	 * @generated
	 */
	void setClassRef(EClass value);

	/**
	 * Returns the value of the '<em><b>Attribute Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.AttributeOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getClassOption_AttributeOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeOption> getAttributeOptions();

	/**
	 * Returns the value of the '<em><b>Reference Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.ReferenceGroupOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getClassOption_ReferenceOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ReferenceGroupOption> getReferenceOptions();

} // ClassOption
