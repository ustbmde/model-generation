package edu.ustb.sei.rmg.resolve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.*;

import choco.Choco;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.common.opres.pack.LowerBoundFactory;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.variables.integer.IntDomainVar;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.GlobalOption;
import edu.ustb.sei.rmg.config.GlobalOptionEnum;
import edu.ustb.sei.rmg.config.OptionModel;
import edu.ustb.sei.rmg.config.ReferenceGroupOption;
import edu.ustb.sei.rmg.config.ReferenceOption;
import edu.ustb.sei.rmg.random.RandomUtil;
import edu.ustb.sei.rmg.random.condition.BoundSpecException;
import edu.ustb.sei.rmg.random.condition.Condition;
import edu.ustb.sei.rmg.random.condition.ConditionList;
import edu.ustb.sei.rmg.random.condition.RangeCondition;
import edu.ustb.sei.rmg.util.Pair;

//FIXME currently, reference groups are not supported
public class BoundResolver_bak {
	public EPackage metamodel;
	public OptionModel option;
	
	final static int ChocoUPPERBOUND = 10000;
	
	private List<EClass> eClasses = new ArrayList<EClass>();
	private HashMap<EClass, List<EReference>> lowerContainerReferences = new HashMap<EClass, List<EReference>>();
	private HashMap<EClass, List<EReference>> upperContainerReferences = new HashMap<EClass, List<EReference>>();
	private List<List<EReference>> referenceGroups = new ArrayList<List<EReference>>();
	private List<EReference> normalReferences = new ArrayList<EReference>();
	
	/*
	 * 1. collect subclasses and the whole inheritance hierarchy of each class
	 * 2. collect lowerContainer and upperContainer
	 * 3. create variable of each class
	 * 4. create variable of all instances of each class and its calculation relation
	 * 5. create bound constraint for class variable
	 * 6. create bound constraint for all-instance variable
	 */
	
	/*
	 List<EClass> eClasses;
	 List<EReference> eReferences;
	 Map<EClass,List<EClass>> subclasses; //eClasses
	 Map<EClass,List<EClass>> hierarchy; //eClasses
	 Map<EClass,List<EReference>> lowerContainer; //eReferences, subclasses
	 Map<EClass,List<EReference>> upperContainer; //eReferences, hierarchy
	 
	 Map<EClass,Variable> clsVar; 
	 Map<EClass,Variable> clsAllVar;
	 
	 
	 */
	
	
	
	
	
	
	public void collect() {
		EList<EClass> cls = new BasicEList<EClass>();
		EList<EReference> cRef = new BasicEList<EReference>();
		EList<EReference> nRef = new BasicEList<EReference>();
		
		iterateMetaModel(metamodel,cls,cRef,nRef);
		collectReferenceGroup(metamodel,option,referenceGroups,nRef);
		
		eClasses.addAll(cls);
		normalReferences.addAll(nRef);
		
		for(EClass c : cls) {
			collectReferences(c,cRef);
		}
	}
	
	static private void collectReferenceGroup(EPackage metamodel,
			OptionModel option, List<List<EReference>> referenceGroups, EList<EReference> nRef) {
		// TODO Auto-generated method stub
		for(Object v : option.getRefOptions()){
			AbstractReferenceOption ro = (AbstractReferenceOption)v;
			if(ro instanceof ReferenceGroupOption) {
				EList refs = ((ReferenceGroupOption) ro).getReferencesRef();
				//nRef.removeAll(refs);
				referenceGroups.add(refs);
			}
		}
	}

	private HashMap<EClass, IntegerVariable> objVarMap = new HashMap<EClass,IntegerVariable>();
	private HashMap<EReference, IntegerVariable> refVarMap = new HashMap<EReference, IntegerVariable>();
	private HashMap<List<EReference>,IntegerVariable> refGroupVarMap  = new HashMap<List<EReference>, IntegerVariable>();
	private List<Constraint> globalConstraint = new ArrayList<Constraint>();
	public void createBoundVariables() throws BoundSpecException {
		for(EClass c : eClasses) {
			objVarMap.put(c, Choco.makeIntVar(c.getName(), 0, ChocoUPPERBOUND));
		}
		
		int[] bound = getGlobalObjectBound();
		if(bound!=null) {
			IntegerVariable[] arr = new IntegerVariable[eClasses.size()];
			for(int id = 0; id<arr.length; id++) {
				arr[id] = objVarMap.get(eClasses.get(id));
			}
			
			globalConstraint.add(Choco.leq(bound[0], Choco.sum(arr)));
			globalConstraint.add(Choco.leq(Choco.sum(arr), bound[1]));
		}
		
		createAllInstanceMap();

		for(EReference r : normalReferences) {
			refVarMap.put(r, Choco.makeIntVar(r.getName(), 0, ChocoUPPERBOUND));
			if(r.getEOpposite()!=null&&refVarMap.get(r.getEOpposite())!=null){
				globalConstraint.add(Choco.eq(refVarMap.get(r),refVarMap.get(r.getEOpposite())));
			}
			
			int l = r.getLowerBound();
			int u = r.getUpperBound();
			IntegerExpressionVariable S = allInstanceVarMap.get(r.getEContainingClass());
			
			if(l>0) {
				reqNormalLinkBoundConstraints.add(Choco.leq(Choco.mult(l, S), refVarMap.get(r)));
			}
			if(u>0) {
				reqNormalLinkBoundConstraints.add(Choco.geq(Choco.mult(u, S), refVarMap.get(r)));
			}
		}
		for(List<EReference> refs : referenceGroups) {
			refGroupVarMap.put(refs, Choco.makeIntVar(refs.toString(), 0, ChocoUPPERBOUND));
			IntegerVariable[] varlist = new IntegerVariable[refs.size()];
			for(int i = 0;i<refs.size();i++) {
				varlist[i] = refVarMap.get(refs.get(i));
			}
			globalConstraint.add(Choco.eq(refGroupVarMap.get(refs), Choco.sum(varlist)));
		}
		
		
		
	}
	
	private HashMap<EClass,Constraint> allInstanceConsMap = new HashMap<EClass,Constraint>();
	private HashMap<EClass,IntegerVariable> allInstanceVarMap = new HashMap<EClass,IntegerVariable>();
	
	private void createAllInstanceMap() {
		HashMap<EClass, List<IntegerVariable>> subcls = new HashMap<EClass, List<IntegerVariable>>();
		for(EClass c : eClasses) {
			IntegerVariable v = objVarMap.get(c);
			putAllInstance(c,v,subcls);
		}
		
		for(EClass c: eClasses) {
			List<IntegerVariable> list = subcls.get(c);
			IntegerExpressionVariable[] arr = new IntegerExpressionVariable[list.size()];
			list.toArray(arr);
			IntegerVariable v = Choco.makeIntVar("all"+c.getName(), 0, ChocoUPPERBOUND);
			allInstanceVarMap.put(c, v);
			allInstanceConsMap.put(c, Choco.eq(v,Choco.sum(arr)));
			System.out.print(v.getName()+"=Sum(");
			for(IntegerExpressionVariable e : arr) 
				System.out.print(e.getName()+" ");
			System.out.println(")");
		}
	}
	
	private void putAllInstance(EClass c, IntegerVariable v, HashMap<EClass, List<IntegerVariable>> subcls) {
		// TODO Auto-generated method stub
		List<IntegerVariable> list = subcls.get(c);
		if(list==null) {
			list = new UniqueEList<IntegerVariable>();
			subcls.put(c, list);
		}
		
		list.add(v);
		for(EClass s : c.getESuperTypes()){
			putAllInstance(s,v,subcls);
		}
	}

	private HashMap<EClass, List<Constraint>> reqObjBoundConstraints = new HashMap<EClass,List<Constraint>>();
	private List<Constraint> reqNormalLinkBoundConstraints = new ArrayList<Constraint>();
	public void createRequiredBoundConstaints() {
		for(EClass c : eClasses) {
			List<Constraint> constraints = new ArrayList<Constraint>();
			reqObjBoundConstraints.put(c, constraints);
			
			List<EReference> lowerContainer = lowerContainerReferences.get(c);
			IntegerVariable ss = allInstanceVarMap.get(c);
			if(lowerContainer!=null) {
				int id = 0;
				IntegerExpressionVariable[] lparts = null;
				lparts = new IntegerExpressionVariable[lowerContainer.size()];
				
				System.out.print("[Cls] ");
				
				for(EReference r : lowerContainer) {
					IntegerVariable vs = allInstanceVarMap.get(r.getEContainingClass());
					lparts[id] = Choco.mult(r.getLowerBound(), vs);
					id++;
					System.out.print(r.getLowerBound()+"*"+vs.getName()+"+");
				}
				constraints.add(Choco.leq(Choco.sum(lparts), ss));	
				System.out.println("<="+ss.getName());
			}

			
			List<EReference> upperContainer = upperContainerReferences.get(c);
			if(upperContainer!=null) {
				int id = 0;
				IntegerExpressionVariable[] uparts = null;
				uparts = new IntegerExpressionVariable[upperContainer.size()];
				boolean isMax = false;
				for(EReference r : upperContainer) {
					if(r.getUpperBound()<0) {
						uparts[id] = Choco.constant(ChocoUPPERBOUND);
						isMax = true;
						break;
					}
					else 
						uparts[id] = Choco.mult(r.getUpperBound(), allInstanceVarMap.get(r.getEContainingClass()));
					id++;
				}
				if(isMax==false) 
					constraints.add(Choco.leq(ss,Choco.sum(uparts)));				
			}		
			
//			List<EReference> refs = normalReferences.get(c);
//			if(refs!=null) {
//				for(EReference r : refs) {
//					constraints.add(Choco.leq(r.getLowerBound(), allInstanceMap.get(c)));
//				}
//			}
			
			Constraint allInstanceEqCons = allInstanceConsMap.get(c);
			if(allInstanceEqCons!=null)
				constraints.add(allInstanceEqCons);
			
		}
		
		for(EReference r : normalReferences) {
			int lt = r.getLowerBound();
			int ut = r.getUpperBound();
			int ls = (r.getEOpposite()!=null)?(r.getEOpposite().getLowerBound()):0;
			int us =  (r.getEOpposite()!=null)?(r.getEOpposite().getUpperBound()):-1;
			IntegerExpressionVariable Ss = allInstanceVarMap.get(r.getEContainingClass());
			IntegerExpressionVariable St = allInstanceVarMap.get(r.getEReferenceType());
			
			if(ut>0&&ls>0)
				reqNormalLinkBoundConstraints.add(Choco.geq(Choco.mult(ut, Ss), Choco.mult(ls, St)));
			
			if(us>0&&lt>0)
				reqNormalLinkBoundConstraints.add(Choco.geq(Choco.mult(us, St), Choco.mult(lt, Ss)));
			
			//int l = r.getLowerBound();
			//int u = r.getUpperBound();
			//IntegerExpressionVariable S = allInstanceMap.get(r.getEContainingClass());
			
//			if(lt>0) {
//				reqNormalLinkBoundConstraints.add(Choco.leq(Choco.mult(lt, Ss), refVarMap.get(r)));
//			}
//			if(ut>0) {
//				reqNormalLinkBoundConstraints.add(Choco.geq(Choco.mult(ut, Ss), refVarMap.get(r)));
//			}
		}
	}
	
	private int[] getGlobalObjectBound() throws BoundSpecException {
		for(Object v: option.getGlobalOptions()) {
			GlobalOption o = (GlobalOption)v;
			
			if(o.getKey()==GlobalOptionEnum.GLOBAL_OBJECT_BOUND) {
				Condition<?> id = RandomUtil.parseCondition(o.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};		
			}
		}
		return null;
	}
	
	private int[] getDefaultObjectBound() throws BoundSpecException {
		for(Object v : option.getGlobalOptions()) {
			GlobalOption o = (GlobalOption)v;
			
			if(o.getKey()==GlobalOptionEnum.DEFAULT_OBJECT_BOUND) {
				Condition<?> id = RandomUtil.parseCondition(o.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};			
			}
		}
		return null;
	}
	
	private int[] getDefaultRefBound() throws BoundSpecException {
		for(Object v : option.getGlobalOptions()) {
			GlobalOption o = (GlobalOption)v;
			
			if(o.getKey()==GlobalOptionEnum.DEFAULT_LINK_BOUND) {
				Condition<?> id = RandomUtil.parseCondition(o.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};			
			}
		}
		return null;
	}
	
	//����ÿ����ķ�ΧԼ��
	//FIXME
	private HashMap<EClass, List<Constraint>> optObjBoundConstraints = new HashMap<EClass,List<Constraint>>();
	private HashMap<EReference, List<Constraint>> optRefBoundConstraints = new HashMap<EReference,List<Constraint>>();
	private HashMap<List<EReference>,List<Constraint>> optRefGroupBoundConstraints = new HashMap<List<EReference>,List<Constraint>>();
	public void createOptionBoundConstaints() throws BoundSpecException {
		int[] defaultBound = getDefaultObjectBound();
		int[] defaultRefBound = getDefaultRefBound();
				
		for(EClass c : eClasses) {
			List<Constraint> constraints = null;
			
			int[] clsBound = null;
			
			IntegerVariable x = objVarMap.get(c);
			if(c.isAbstract())
				clsBound = new int[]{0,0};
			else if(option.getRootClass()==c){
				constraints = reqObjBoundConstraints.get(c);
				constraints.add(Choco.leq(1, x));
				
				System.out.println("[Root Cls] "+"1<="+x.getName());
				
				if(option.isUniqueRoot()){
					constraints.add(Choco.eq(1, x));
					System.out.println("[Root Cls] "+"1="+x.getName());
				}
			}
			
			if(clsBound!=null) {
				constraints =  reqObjBoundConstraints.get(c);
				//optObjBoundConstraints.put(c, constraints);
				
				if(clsBound[0]==clsBound[1]) {
					constraints.add(Choco.eq(clsBound[0], x));
					System.out.println("[Cls] "+clsBound[0]+"="+x.getName());
				}
				else {
					constraints.add(Choco.leq(clsBound[0], x));
					constraints.add(Choco.leq(x, clsBound[1]));
					System.out.println("[Cls] "+clsBound[0]+"<="+x.getName()+"<="+clsBound[1]);
				}
			} else {	
				constraints = new ArrayList<Constraint>();
				optObjBoundConstraints.put(c, constraints);
				clsBound = getClassBound(c);
				if(clsBound==null)
					clsBound = defaultBound;		
				
				if(clsBound[0]==clsBound[1]) {
					constraints.add(Choco.eq(clsBound[0], x));
					System.out.println("[Cls] "+clsBound[0]+"="+x.getName());
				}
				else {
					constraints.add(Choco.leq(clsBound[0], x));
					constraints.add(Choco.leq(x, clsBound[1]));
					System.out.println("[Cls] "+clsBound[0]+"<="+x.getName()+"<="+clsBound[1]);
				}
			}			
		}
		
		for(EReference r : normalReferences) {
			List<Constraint> constraints = null;
			int[] refBound = null;
			
			constraints = new ArrayList<Constraint>();
			optRefBoundConstraints.put(r, constraints);
			refBound = getRefBound(r);
			if(refBound==null)
				refBound = defaultRefBound;		
			
			IntegerVariable rv = refVarMap.get(r);
			if(refBound[0]==refBound[1]) {
				constraints.add(Choco.eq(refBound[0], rv));
				System.out.println("[Ref] " + refBound[0] + "=" + rv.getName());
			}
			else {
				constraints.add(Choco.leq(refBound[0], rv));
				constraints.add(Choco.leq(rv, refBound[1]));
				System.out.println("[Ref] " + refBound[0] + "<=" + rv.getName()+ "<="+ refBound[1]);
			}	
		}
		
		for(Object o : option.getRefOptions()){
			if(o instanceof ReferenceGroupOption) {
				List<Constraint> constraints = new ArrayList<Constraint>();
				List<EReference> refs = ((ReferenceGroupOption) o).getReferencesRef();
				boolean sourceUnique = ((ReferenceGroupOption) o).isSourceUnique();
				boolean sourceRequired = ((ReferenceGroupOption) o).isSourceRequired();
				boolean targetUnique = ((ReferenceGroupOption) o).isTargetUnique();
				boolean targetRequired = ((ReferenceGroupOption) o).isTargetRequired();
				
				optRefGroupBoundConstraints.put(refs, constraints);
				
				Condition<?> id = RandomUtil.parseCondition(((ReferenceGroupOption) o).getBoundCondition());
				int[] refGroupBound = null;
				{
					Object[] bound = id.getGenerationBound();
					if(bound==null) refGroupBound = defaultBound;
					else refGroupBound =  new int[]{(int)bound[0],(int)bound[1]};
				}
				
				
				IntegerVariable x = refGroupVarMap.get(refs);
				if(refGroupBound[0]==refGroupBound[1])
					constraints.add(Choco.eq(refGroupBound[0], x));
				else {
					constraints.add(Choco.leq(refGroupBound[0], x));
					constraints.add(Choco.leq(x, refGroupBound[1]));
				}
				
				//FIXME require unique constraints
			}
		}
	
	}
	


	private int[] getClassBound(EClass c) throws BoundSpecException {
		
		for(Object v : option.getClsOptions()) {
			ClassOption co = (ClassOption)v;
			
			if(co.getClassRef()==c) {
				Condition<?> id = RandomUtil.parseCondition(co.getBoundCondition());
				Object[] bound = id.getGenerationBound();
				
				if(bound==null) return null;
				else return new int[]{(int)bound[0],(int)bound[1]};			
			}
		}
		return null;
	}
	
	private int[] getRefBound(EReference r) throws BoundSpecException {
		
		for(Object v : option.getRefOptions()) {
			AbstractReferenceOption ro = (AbstractReferenceOption)v;
			
			if(ro instanceof ReferenceOption) {
				if(((ReferenceOption) ro).getReferenceRef()==r) {
					Condition<?> id = RandomUtil.parseCondition(ro.getBoundCondition());
					Object[] bound = id.getGenerationBound();
					
					if(bound==null) return null;
					else return new int[]{(int)bound[0],(int)bound[1]};	
				}
			}
		}
		return null;
	}
	

	private void collectReferences(EClass c, EList<EReference> cRef) {
		if(option.getRootClass()==c && option.isUniqueRoot()==false) return;
		
		for(EReference r : cRef) {
			
			if(c.isSuperTypeOf(r.getEReferenceType())) {
				putReference(c,r,lowerContainerReferences);
				putReference(c,r,upperContainerReferences);
			}
			
			if(r.getEReferenceType().isSuperTypeOf(c))
				putReference(c,r,upperContainerReferences);
		}
//		for(EReference r : nRef) {
//			if(r.getEReferenceType().isSuperTypeOf(c))
//				putReference(c,r,normalReferences);
//		}
	}



	private void putReference(EClass eReferenceType, EReference ref,  HashMap<EClass, List<EReference>> refMap) {
		List<EReference> refs = refMap.get(eReferenceType);
		if(refs==null) {
			refs = new UniqueEList<EReference>();
			refMap.put(eReferenceType,refs);
		}
		
		refs.add(ref);
	}
	
	static private void iterateMetaModel(EPackage pkg, EList<EClass> allClasses, EList<EReference> allContainmentReferences, EList<EReference> allNormalReferences) {
		for(EClassifier c : pkg.getEClassifiers()) {
			if(c instanceof EClass) {
				allClasses.add((EClass) c);
				for(EReference r : ((EClass) c).getEReferences()) {
					if(r.isChangeable()==false) continue;
					
					if(r.isContainment())
						allContainmentReferences.add(r);
					else {
						if(r.getEOpposite()==null||r.getEOpposite().isContainment()==false)
							allNormalReferences.add(r);
					}
				}
			}
		}
		
		for(EPackage p : pkg.getESubpackages()) {
			iterateMetaModel(p,allClasses,allContainmentReferences,allNormalReferences);
		}
	}
	
	public CPModel buildModel() {
		CPModel model = new CPModel();
		
		for(EClass c : eClasses) {
			model.addVariable(objVarMap.get(c));
		}
		for(EReference r : normalReferences) {
			model.addVariable(refVarMap.get(r));
		}
		
		for(EClass c : eClasses) {
			model.addVariable(this.allInstanceVarMap.get(c));
			model.addConstraints(reqObjBoundConstraints.get(c).toArray(new Constraint[0]));
		}
		
		model.addConstraints(globalConstraint.toArray(new Constraint[0]));
		
		model.addConstraints(reqNormalLinkBoundConstraints.toArray(new Constraint[0]));
		
		for(EClass c : eClasses) {
			List<Constraint> lc = optObjBoundConstraints.get(c);
			if(lc!=null)
				model.addConstraints(lc.toArray(new Constraint[0]));
		}
		
		for(EReference r : normalReferences) {
			List<Constraint> lc = optRefBoundConstraints.get(r);
			if(lc!=null)
				model.addConstraints(lc.toArray(new Constraint[0]));
		}
		
		
		return model;
	}
	
	public void init(EPackage metamodel, OptionModel option) {
		this.metamodel = metamodel;
		this.option = option;
	}
	
	private CPSolver solver = null;
	public boolean solve() throws BoundSpecException {
		collect();
		createBoundVariables();
		createRequiredBoundConstaints();
		createOptionBoundConstaints();
		
		CPModel m = buildModel();
		solver = new CPSolver();
		solver.read(m);
		
		solver.setRandomSelectors();
		
		return solver.solve();
		
	}
	public void solve(OptionModel om, Generator g) {
//		init(g.getMetamodel(),om);
//		if(solve()){
//			for(EClass c : eClasses) {
//				IntegerVariable v = objVarMap.get(c);
//				IntDomainVar val = solver.getVar(v);
//				g.getGenerationConstraint().put(c, String.valueOf(val.getVal()));
//			}
//		}
		
	}
	
	public void solve(OptionModel configModel) {
		BoundResolver_bak s = new BoundResolver_bak();
		s.metamodel = (EPackage) configModel.eResource().getResourceSet().getResources().get(1).getContents().get(0);
		s.option = configModel;
		
		try{
			
			s.collect();
			s.createBoundVariables();
			s.createRequiredBoundConstaints();
			s.createOptionBoundConstaints();
			
			CPModel m = s.buildModel();
			CPSolver cps = new CPSolver();
			cps.read(m);
			
			cps.setRandomSelectors();
			
			if(cps.solve()) {
				int id = 0;
				//do {
				System.out.println("solution #"+id++);
				for(EClass c : s.eClasses) {
					IntegerVariable v = s.objVarMap.get(c);
					IntDomainVar val = cps.getVar(v);
					System.out.println(v.getName()+":"+val.getVal());
				}
				
				for(EReference c : s.normalReferences) {
					IntegerVariable v = s.refVarMap.get(c);
					IntDomainVar val = cps.getVar(v);
					System.out.println(v.getName()+":"+val.getVal());
				}
				
				//}while(cps.nextSolution());
			} else {
				System.out.println("no solution");
			}
		}catch(Exception e) {
			System.out.println(e);
		}
	}
	
//	static public void main(String...arg) throws BoundSpecException {
//		Generator g = new Generator();
//		
//		g.loadMetamodel("JavaSource.ecore");
//		
//		
//		BoundResolver_bak s = new BoundResolver_bak();
//		s.metamodel = g.getMetamodel();
//		s.option = g.loadConfig("ModelGen.rmg");
//		
//		s.collect();
//		s.createBoundVariables();
//		s.createRequiredBoundConstaints();
//		s.createOptionBoundConstaints();
//		
//		CPModel m = s.buildModel();
//		CPSolver cps = new CPSolver();
//		cps.read(m);
//		
//		cps.setRandomSelectors();
//		
//		if(cps.solve()) {
//			int id = 0;
//			//do {
//				System.out.println("solution #"+id++);
//				for(EClass c : s.eClasses) {
//					IntegerVariable v = s.objVarMap.get(c);
//					IntDomainVar val = cps.getVar(v);
//					System.out.println(v.getName()+":"+val.getVal());
//				}
//				
//				for(EReference c : s.normalReferences) {
//					IntegerVariable v = s.refVarMap.get(c);
//					IntDomainVar val = cps.getVar(v);
//					System.out.println(v.getName()+":"+val.getVal());
//				}
//				
//			//}while(cps.nextSolution());
//		} else {
//			System.out.println("no solution");
//		}
//	}
}
