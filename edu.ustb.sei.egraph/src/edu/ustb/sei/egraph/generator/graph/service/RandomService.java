package edu.ustb.sei.egraph.generator.graph.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;
import edu.ustb.sei.egraph.util.ConcurrentArrayList;

public class RandomService {
	private ConcurrentHashMap<String,List<Object>> generatedValues;
	private Set<Object> decisionOnProc;
	private Map<String, AbstractRandomFunction<?>> definedRandomFunction;
	
	public RandomService(Random random) {
		super();
		this.random = random;
		generatedValues = new ConcurrentHashMap<String,List<Object>>();
		decisionOnProc = new HashSet<Object>();
		this.definedRandomFunction = new HashMap<String, AbstractRandomFunction<?>>();
	}
	
	
	
	public void recordGeneratedValue(String key, Object v) {
		List<Object> values = this.generatedValues.get(key);
		if(values==null) {
			synchronized(key) {
				values = this.generatedValues.get(key);
				if(values==null)
					values = new ConcurrentArrayList<Object>(1024);
				this.generatedValues.put(key,values);
			}
		}
		values.add(v);
	}
	
	public List<Object> getGeneratedValues(String key) {
		List<Object> values = this.generatedValues.get(key);
		if(values==null)
			return Collections.emptyList();
		else 
			return values;
	}
	
	public RandomService(long seed) {
		this(new Random(seed));
	}
	
	public RandomService() {
		this(new Random());
	}

	private Random random;
	
	
	public boolean randomBoolean() {
		return random.nextBoolean();
	}
	
	public boolean randomBoolean(double p) {
		return random.nextDouble() < p;
	}
	
	public int randomInt() {
		return random.nextInt();
	}
	
	public int randomInt(int max) {
		return random.nextInt(max);
	}
	
	public int randomInt(int min,int max) {
		if(max<=min) return min; 
		return random.nextInt(max-min)+min;
	}
	
	public long randomLong() {
		return random.nextLong();
	}
	
	public long randomLong(long max) {
		if(max==0) return 0;
		long v = Math.abs(random.nextLong()) % max;
		return v;
	}
	
	public long randomLong(long min, long max) {
		if(max<=min) return max;
		long v = Math.abs(random.nextLong()) % (max-min)+min;
		return v;
	}
	
	public double randomDouble() {
		return random.nextDouble();
	}
	
	public double randomDouble(double min, double max) {
		return random.nextDouble() * (max-min) + min;
	}
	
	public float randomFloat() {
		return random.nextFloat();
	}
	
	public float randomFloat(float min, float max) {
		return random.nextFloat() * (max-min) + min;
	}
	
	public <T> T randomEnum(T[] arr) {
		return arr[random.nextInt(arr.length)];
	}
	
	public <T> T randomEnum(List<T> arr) {
		return arr.get(random.nextInt(arr.size()));
	}
	
	public char randomChar() {
		return (char)random.nextInt();
	}
	
	public char randomChar(char[] charSet) {
		return charSet[random.nextInt(charSet.length)];
	}
	
	public boolean decideOnProcedure(Object p) {
		if(decisionOnProc.contains(p))
			return false;
		else {
			decisionOnProc.add(p);
			return true;
		}
	}

	public Random getRandom() {
		return random;
	}

	public Map<String, AbstractRandomFunction<?>> getDefinedRandomFunction() {
		return definedRandomFunction;
	}
}
