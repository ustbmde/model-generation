/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.UserAction#getClassFileURI <em>Class File URI</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getUserAction()
 * @model
 * @generated
 */
public interface UserAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Class File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class File URI</em>' attribute.
	 * @see #setClassFileURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getUserAction_ClassFileURI()
	 * @model
	 * @generated
	 */
	String getClassFileURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.UserAction#getClassFileURI <em>Class File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class File URI</em>' attribute.
	 * @see #getClassFileURI()
	 * @generated
	 */
	void setClassFileURI(String value);

} // UserAction
