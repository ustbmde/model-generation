/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.GetPutTestCase;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;
import edu.ustb.sei.mt4mt.base.test.TestGetPut;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Get Put Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GetPutTestCaseImpl extends PredefinedBXTestCaseImpl implements GetPutTestCase {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GetPutTestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.GET_PUT_TEST_CASE;
	}
	
	@Override
	public AbstractTransformationTestCase build() {
		TestGetPut test = new TestGetPut(
				getInputs().toArray(AbstractTransformationTestCase.STRING_ARRAY), 
				getFwdLauncher(), 
				getFwdSource().toArray(AbstractTransformationTestCase.STRING_ARRAY), 
				getFwdView().toArray(AbstractTransformationTestCase.STRING_ARRAY), 
				getBwdLauncher(), 
				getBwdSource().toArray(AbstractTransformationTestCase.STRING_ARRAY), 
				getBwdView().toArray(AbstractTransformationTestCase.STRING_ARRAY), 
				getBwdUpdatedSource().toArray(AbstractTransformationTestCase.STRING_ARRAY),
				getDefaultWaitingTime());
		test.setIdentifier("test "+getId());
		test.setMetamodels(getMetamodels().toArray(AbstractTransformationTestCase.STRING_ARRAY));
		return test;
	}

} //GetPutTestCaseImpl
