package edu.ustb.sei.rmg.random.condition;

import java.util.List;

import edu.ustb.sei.rmg.random.RandomUtil;

public class RealDistributionFunction extends DistributionFunction<Double> {



	@Override
	public Double getValue(RandomUtil r, Object... extra) {
		switch(name){
		case "norm":
			if(parameters.length==0)
				return r.gaussian();
			else return r.gaussian(parameters[0], parameters[1]);
		case "pare":
			return r.pareto(parameters[0]);
		case "cauc":
			return r.cauchy();
		}
		return 0.0;
	}

}
