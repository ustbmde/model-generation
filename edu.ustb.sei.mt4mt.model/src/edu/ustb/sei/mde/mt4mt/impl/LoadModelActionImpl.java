/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.LoadModelAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Load Model Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl#getModelURI <em>Model URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.LoadModelActionImpl#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoadModelActionImpl extends TestActionImpl implements LoadModelAction {
	/**
	 * The default value of the '{@link #getModelURI() <em>Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelURI() <em>Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelURI()
	 * @generated
	 * @ordered
	 */
	protected String modelURI = MODEL_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey()
	 * @generated
	 * @ordered
	 */
	protected static final String KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKey()
	 * @generated
	 * @ordered
	 */
	protected String key = KEY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoadModelActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.LOAD_MODEL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelURI() {
		return modelURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelURI(String newModelURI) {
		String oldModelURI = modelURI;
		modelURI = newModelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.LOAD_MODEL_ACTION__MODEL_URI, oldModelURI, modelURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(String newKey) {
		String oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.LOAD_MODEL_ACTION__KEY, oldKey, key));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.LOAD_MODEL_ACTION__MODEL_URI:
				return getModelURI();
			case MT4MTPackage.LOAD_MODEL_ACTION__KEY:
				return getKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.LOAD_MODEL_ACTION__MODEL_URI:
				setModelURI((String)newValue);
				return;
			case MT4MTPackage.LOAD_MODEL_ACTION__KEY:
				setKey((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.LOAD_MODEL_ACTION__MODEL_URI:
				setModelURI(MODEL_URI_EDEFAULT);
				return;
			case MT4MTPackage.LOAD_MODEL_ACTION__KEY:
				setKey(KEY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.LOAD_MODEL_ACTION__MODEL_URI:
				return MODEL_URI_EDEFAULT == null ? modelURI != null : !MODEL_URI_EDEFAULT.equals(modelURI);
			case MT4MTPackage.LOAD_MODEL_ACTION__KEY:
				return KEY_EDEFAULT == null ? key != null : !KEY_EDEFAULT.equals(key);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelURI: ");
		result.append(modelURI);
		result.append(", key: ");
		result.append(key);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		return edu.ustb.sei.mt4mt.base.Actions.load(modelURI, key);
	}

} //LoadModelActionImpl
