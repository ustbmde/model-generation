package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class AtomicCondition<T> extends Condition<T> {
	public Condition<T> body;

	@Override
	public T getValue(RandomUtil r, Object... extra) {
		return body.getValue(r, extra);
	}

	@Override
	public Object[] getGenerationBound() throws BoundSpecException {
		return body.getGenerationBound();
	}

}
