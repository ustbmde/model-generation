/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * The preference page to set the occurrence highlighting is not used anymore.
 * This empty class is only generated to override old existing preference page
 * code to avoid compile errors.
 */
public class StructuralrmgOccurrencePreferencePage {
}
