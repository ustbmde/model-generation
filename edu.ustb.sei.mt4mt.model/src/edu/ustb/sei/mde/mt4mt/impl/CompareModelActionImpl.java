/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.CompareModelAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compare Model Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl#getLeftURI <em>Left URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl#getRightURI <em>Right URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.CompareModelActionImpl#getDiffKey <em>Diff Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompareModelActionImpl extends TestActionImpl implements CompareModelAction {
	/**
	 * The default value of the '{@link #getLeftURI() <em>Left URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftURI()
	 * @generated
	 * @ordered
	 */
	protected static final String LEFT_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLeftURI() <em>Left URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftURI()
	 * @generated
	 * @ordered
	 */
	protected String leftURI = LEFT_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getRightURI() <em>Right URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightURI()
	 * @generated
	 * @ordered
	 */
	protected static final String RIGHT_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRightURI() <em>Right URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightURI()
	 * @generated
	 * @ordered
	 */
	protected String rightURI = RIGHT_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getDiffKey() <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiffKey()
	 * @generated
	 * @ordered
	 */
	protected static final String DIFF_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDiffKey() <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiffKey()
	 * @generated
	 * @ordered
	 */
	protected String diffKey = DIFF_KEY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompareModelActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.COMPARE_MODEL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLeftURI() {
		return leftURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftURI(String newLeftURI) {
		String oldLeftURI = leftURI;
		leftURI = newLeftURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.COMPARE_MODEL_ACTION__LEFT_URI, oldLeftURI, leftURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRightURI() {
		return rightURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightURI(String newRightURI) {
		String oldRightURI = rightURI;
		rightURI = newRightURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.COMPARE_MODEL_ACTION__RIGHT_URI, oldRightURI, rightURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDiffKey() {
		return diffKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiffKey(String newDiffKey) {
		String oldDiffKey = diffKey;
		diffKey = newDiffKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.COMPARE_MODEL_ACTION__DIFF_KEY, oldDiffKey, diffKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.COMPARE_MODEL_ACTION__LEFT_URI:
				return getLeftURI();
			case MT4MTPackage.COMPARE_MODEL_ACTION__RIGHT_URI:
				return getRightURI();
			case MT4MTPackage.COMPARE_MODEL_ACTION__DIFF_KEY:
				return getDiffKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.COMPARE_MODEL_ACTION__LEFT_URI:
				setLeftURI((String)newValue);
				return;
			case MT4MTPackage.COMPARE_MODEL_ACTION__RIGHT_URI:
				setRightURI((String)newValue);
				return;
			case MT4MTPackage.COMPARE_MODEL_ACTION__DIFF_KEY:
				setDiffKey((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.COMPARE_MODEL_ACTION__LEFT_URI:
				setLeftURI(LEFT_URI_EDEFAULT);
				return;
			case MT4MTPackage.COMPARE_MODEL_ACTION__RIGHT_URI:
				setRightURI(RIGHT_URI_EDEFAULT);
				return;
			case MT4MTPackage.COMPARE_MODEL_ACTION__DIFF_KEY:
				setDiffKey(DIFF_KEY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.COMPARE_MODEL_ACTION__LEFT_URI:
				return LEFT_URI_EDEFAULT == null ? leftURI != null : !LEFT_URI_EDEFAULT.equals(leftURI);
			case MT4MTPackage.COMPARE_MODEL_ACTION__RIGHT_URI:
				return RIGHT_URI_EDEFAULT == null ? rightURI != null : !RIGHT_URI_EDEFAULT.equals(rightURI);
			case MT4MTPackage.COMPARE_MODEL_ACTION__DIFF_KEY:
				return DIFF_KEY_EDEFAULT == null ? diffKey != null : !DIFF_KEY_EDEFAULT.equals(diffKey);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (leftURI: ");
		result.append(leftURI);
		result.append(", rightURI: ");
		result.append(rightURI);
		result.append(", diffKey: ");
		result.append(diffKey);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		return edu.ustb.sei.mt4mt.base.Actions.compare(leftURI, rightURI, diffKey);
	}

} //CompareModelActionImpl
