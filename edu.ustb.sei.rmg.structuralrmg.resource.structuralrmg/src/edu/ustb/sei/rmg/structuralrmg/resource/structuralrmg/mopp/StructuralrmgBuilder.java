/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import edu.ustb.sei.rmg.structuralrmg.AttributeBinding;
import edu.ustb.sei.rmg.structuralrmg.CompositionSort;
import edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

public class StructuralrmgBuilder implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgBuilder {
	
	public boolean isBuildingNeeded(org.eclipse.emf.common.util.URI uri) {
		return ("structuralrmg".equals(uri.fileExtension()));
	}
	
	public org.eclipse.core.runtime.IStatus build(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource resource, org.eclipse.core.runtime.IProgressMonitor monitor) {
		// set option overrideBuilder to 'false' and then perform build here
		
		try {
			TemplateModel model = (TemplateModel) resource.getContents().get(0);
			HashMap<EObject,EClass> map = new HashMap<EObject,EClass>();
			
			URI u = resource.getURI();
			URI f = u.trimFileExtension();
			u = f.appendFileExtension("ecore");
			
			Resource res = getResource(u);
			EPackage pkg = EcoreFactory.eINSTANCE.createEPackage();
			pkg.setName(model.getName());
			pkg.setNsPrefix(model.getName());
			pkg.setNsURI("http://www.ustb.edu.cn/sei/mde/rmg/"+model.getName());
			res.getContents().add(pkg);
			
			//model.setTPackage(pkg);
			EAnnotation ann = EcoreFactory.eINSTANCE.createEAnnotation();
			ann.setSource("model");
			ann.getReferences().add(model);
			pkg.getEAnnotations().add(ann);
			
			collectTemplateClass(model,pkg,map);
			collectTemplateDetails(model,pkg,map);
			//refineModel(pkg);
			res.save(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return org.eclipse.core.runtime.Status.CANCEL_STATUS;
		}
		
		return org.eclipse.core.runtime.Status.OK_STATUS;
	}
	
//	private void refineModel(EPackage pack) {
//		// TODO Auto-generated method stub
//		//如果抽象父类只有一个子类，则删除该父类，所有指向该父类的引用都应该指向子类
//		//如果所有子类都有相同的特征，将特征移到父类上
//		//如果两个抽象父类的子类都相同，合并两个父类
//		
//		Map<EObject, Collection<Setting>> map = null;
//		List<EObject> removed = new ArrayList<EObject>();
//		
//		map = EcoreUtil.CrossReferencer.find(Collections.singleton(pack));
//		for(EClassifier cf : pack.getEClassifiers()) {
//			if(cf instanceof EClass) {
//				EClass c = (EClass)cf;
//				Assert.isTrue(c.isAbstract());
//				Collection<Setting> sets = map.get(c);
//				Collection<EObject> children = collectSource(sets, EcorePackage.eINSTANCE.getEClass_EAllSuperTypes());
//				if(children.size()==1) {
//					for(EObject onlyChild : children) {
//						Collection<EObject> src = collectSource(sets,EcorePackage.eINSTANCE.getETypedElement_EType());
//						for(EObject ref : src) {
//							((EReference)ref).setEType((EClass)onlyChild);
//						}
//					}
//					removed.add(c);
//				} else if(children.size()>1){
//					Collection<Collection<EStructuralFeature>> common = collectCommonFeature(children);
//					for(Collection<EStructuralFeature> csf : common) {
//						
//						
//						break;
//					}
//				}
//			}
//		}
//	}
//
//	private Collection<EObject> collectSource(Collection<Setting> sets, EStructuralFeature feature) {
//		HashSet<EObject> res = new HashSet<EObject>();
//		for(Setting s : sets) {
//			if(s.getEStructuralFeature() == feature) {
//				res.add(s.getEObject());
//			}
//		}
//		return res;
//	}
//	
//	private Collection<EStructuralFeature> getCommon(EStructuralFeature feature, Collection<EObject> clss) {
//		Collection<EStructuralFeature> ret = new HashSet<EStructuralFeature>();
//		for(EObject c : clss) {
//			EStructuralFeature f = getCommon(feature,(EClass)c);
//			if(f==null) return null;
//			ret.add(f);
//		}
//		if(ret.size()==clss.size())
//			return ret;
//		return null;
//	}
//	
//	private EStructuralFeature getCommon(EStructuralFeature feature, EClass cls) {
//		if(feature.eContainer()==cls) return feature;
//		for(EStructuralFeature cf : cls.getEAllStructuralFeatures()) {
//			if(feature.eClass()!=cf.eClass()) 
//				continue;
//			if(!feature.getName().equals(cf.getName())) 
//				continue;
//			if(feature.getEType()!=cf.getEType()) 
//				continue;
//			if(feature.getUpperBound()!=cf.getUpperBound() || feature.getLowerBound()!=cf.getLowerBound()) 
//				continue;
//			if(feature instanceof EReference && cf instanceof EReference) {
//				if(((EReference)feature).isContainment() != ((EReference)cf).isContainment())
//					continue;
//			}
//			
//			return cf;
//		}
//		return null;
//	}
//	
//	private Collection<Collection<EStructuralFeature>> collectCommonFeature(Collection<EObject> classes) {
//		Collection<Collection<EStructuralFeature>> ret = new HashSet<Collection<EStructuralFeature>>();
//		for(EObject c1 : classes) {
//			EClass cls = (EClass)c1;
//			for(EStructuralFeature f : cls.getEAllStructuralFeatures()) {
//				Collection<EStructuralFeature> features = getCommon(f,classes);
//				if(features==null) continue;
//				ret.add(features);
//			}
//		}
//		return ret;
//	}

	private ResourceSet resourceSet;

	public StructuralrmgBuilder() {
		resourceSet  = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				"ecore", new EcoreResourceFactoryImpl());
	}
		

	private Resource getResource(URI uri) {
		Resource resource = resourceSet.createResource(uri);
		return resource;
	}


	private void collectTemplateDetails(TemplateModel model,
			EPackage pkg, HashMap<EObject, EClass> map) {
		for(Template t : model.getTemplates()) {
			EClass tc = map.get(t);
			
			if(t.getSuperTemplate()!=null) {
				EClass pc = map.get(t.getSuperTemplate());
				tc.getESuperTypes().add(pc);
			}
			
			for(TemplateNode ntn : t.getNodes()) {
				if(ntn instanceof NestedTemplateNode) {
					CompositionSort sort = ((NestedTemplateNode) ntn).getSort();
					
					if(sort==CompositionSort.REVERSED) continue;
					
					EClass cls = EcoreFactory.eINSTANCE.createEClass();
					cls.setName(t.getName()+"_"+ntn.getName());
					cls.setAbstract(true);
					map.put(ntn, cls);
					pkg.getEClassifiers().add(cls);
					
					for(Template ct : ((NestedTemplateNode) ntn).getTemplates()) {
						EClass cc = map.get(ct);
						cc.getESuperTypes().add(cls);
					}
					
					EReference ref = EcoreFactory.eINSTANCE.createEReference();
					ref.setName(ntn.getName());
					tc.getEStructuralFeatures().add(ref);
					ref.setEType(cls);
					
					if(sort==CompositionSort.SHARED) ref.setContainment(false);
					else ref.setContainment(true);
					
					
//					if(ntn.isSet()) {
//						ref.setLowerBound(ntn.getMin());
//						ref.setUpperBound(ntn.getMax());
////						if(ntn.getMax()>0) {
////							ref.setLowerBound(ntn.getMax());
////						}
//					}
//					else {
						ref.setLowerBound(ntn.getMin());
						ref.setUpperBound(ntn.getMax());
//					}

					EAnnotation ann = EcoreFactory.eINSTANCE.createEAnnotation();
					ann.setSource("nested");
					ann.getReferences().add(ntn);
					ref.getEAnnotations().add(ann);
				} else {
					PrimitiveTemplateNode ptn = (PrimitiveTemplateNode)ntn;
					for(AttributeBinding ab : ptn.getAttributes()) {
						EAttribute att = EcoreFactory.eINSTANCE.createEAttribute();
						att.setName(ab.getName());
						att.setEType(ab.getAttribute().getEType());
						tc.getEStructuralFeatures().add(att);
						
						EAnnotation ann = EcoreFactory.eINSTANCE.createEAnnotation();
						ann.setSource("attribute");
						ann.getReferences().add(ab);
						att.getEAnnotations().add(ann);
					}
				}
			}
		}
	}

	private void collectTemplateClass(TemplateModel model,EPackage pkg, HashMap<EObject,EClass> map) {
		for(Template t : model.getTemplates()) {
			EClass cls = EcoreFactory.eINSTANCE.createEClass();
			cls.setName(t.getName());
			cls.setAbstract(t.isAbstract());
			map.put(t, cls);
			pkg.getEClassifiers().add(cls);
			//t.setTClass(cls);
			EAnnotation ann = EcoreFactory.eINSTANCE.createEAnnotation();
			ann.setSource("template");
			ann.getReferences().add(t);
			cls.getEAnnotations().add(ann);
		}
	}
	
	/**
	 * Handles the deletion of the given resource.
	 */
	public org.eclipse.core.runtime.IStatus handleDeletion(org.eclipse.emf.common.util.URI uri, org.eclipse.core.runtime.IProgressMonitor monitor) {
		if("structuralrmg".equals(uri.fileExtension())==false) return org.eclipse.core.runtime.Status.OK_STATUS;
		
		URI u = uri;
		URI f = u.trimFileExtension();
		u = f.appendFileExtension(".ecore");
		
		Resource res = resourceSet.getResource(u, true);
		try {
			res.delete(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return org.eclipse.core.runtime.Status.OK_STATUS;
	}
	
	
	
}
