/**
 */
package edu.ustb.sei.mde.mt4mt;

import edu.ustb.sei.mt4mt.base.IAction;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestAction()
 * @model abstract="true"
 * @generated
 */
public interface TestAction extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="edu.ustb.sei.mde.mt4mt.IAction"
	 * @generated
	 */
	IAction build();
} // TestAction
