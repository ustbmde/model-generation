/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Rt4mtFactoryImpl extends EFactoryImpl implements Rt4mtFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Rt4mtFactory init() {
		try {
			Rt4mtFactory theRt4mtFactory = (Rt4mtFactory)EPackage.Registry.INSTANCE.getEFactory(Rt4mtPackage.eNS_URI);
			if (theRt4mtFactory != null) {
				return theRt4mtFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Rt4mtFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rt4mtFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL: return createRegressionTestModel();
			case Rt4mtPackage.TEST_CASE: return createTestCase();
			case Rt4mtPackage.TEST_ROUND: return createTestRound();
			case Rt4mtPackage.TEST_EXECUTION: return createTestExecution();
			case Rt4mtPackage.TEST_CASE_GROUP: return createTestCaseGroup();
			case Rt4mtPackage.TEST_ROUND_GROUP: return createTestRoundGroup();
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION_GROUP: return createInputParameterConfigurationGroup();
			case Rt4mtPackage.OUTPUT_PARAMETER_CONFIGURATION_GROUP: return createOutputParameterConfigurationGroup();
			case Rt4mtPackage.INPUT_PARAMETER_CONFIGURATION: return createInputParameterConfiguration();
			case Rt4mtPackage.OUTPUT_PARAMETER_CONFIGURATION: return createOutputParameterConfiguration();
			case Rt4mtPackage.TEST_DATA: return createTestData();
			case Rt4mtPackage.EXPECTED_OUTPUT: return createExpectedOutput();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RegressionTestModel createRegressionTestModel() {
		RegressionTestModelImpl regressionTestModel = new RegressionTestModelImpl();
		return regressionTestModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCase createTestCase() {
		TestCaseImpl testCase = new TestCaseImpl();
		return testCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestRound createTestRound() {
		TestRoundImpl testRound = new TestRoundImpl();
		return testRound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestExecution createTestExecution() {
		TestExecutionImpl testExecution = new TestExecutionImpl();
		return testExecution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseGroup createTestCaseGroup() {
		TestCaseGroupImpl testCaseGroup = new TestCaseGroupImpl();
		return testCaseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestRoundGroup createTestRoundGroup() {
		TestRoundGroupImpl testRoundGroup = new TestRoundGroupImpl();
		return testRoundGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputParameterConfigurationGroup createInputParameterConfigurationGroup() {
		InputParameterConfigurationGroupImpl inputParameterConfigurationGroup = new InputParameterConfigurationGroupImpl();
		return inputParameterConfigurationGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputParameterConfigurationGroup createOutputParameterConfigurationGroup() {
		OutputParameterConfigurationGroupImpl outputParameterConfigurationGroup = new OutputParameterConfigurationGroupImpl();
		return outputParameterConfigurationGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputParameterConfiguration createInputParameterConfiguration() {
		InputParameterConfigurationImpl inputParameterConfiguration = new InputParameterConfigurationImpl();
		return inputParameterConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputParameterConfiguration createOutputParameterConfiguration() {
		OutputParameterConfigurationImpl outputParameterConfiguration = new OutputParameterConfigurationImpl();
		return outputParameterConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestData createTestData() {
		TestDataImpl testData = new TestDataImpl();
		return testData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpectedOutput createExpectedOutput() {
		ExpectedOutputImpl expectedOutput = new ExpectedOutputImpl();
		return expectedOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rt4mtPackage getRt4mtPackage() {
		return (Rt4mtPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Rt4mtPackage getPackage() {
		return Rt4mtPackage.eINSTANCE;
	}

} //Rt4mtFactoryImpl
