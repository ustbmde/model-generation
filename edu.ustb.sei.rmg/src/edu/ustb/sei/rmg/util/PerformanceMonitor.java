package edu.ustb.sei.rmg.util;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class PerformanceMonitor {
	static final public PerformanceMonitor SINGLETON = new PerformanceMonitor();
	
	protected HashMap<String, PerformanceTask> taskMap = new HashMap<String, PerformanceTask>();
	
	public void begin(String id) {
		PerformanceTask task = taskMap.get(id);
		if(task==null) {
			task = new PerformanceTask();
			taskMap.put(id, task);
		}
		task.startTime = System.currentTimeMillis();
	}
	
	public void print(String title, String...ids) {
		if(ids==null||ids.length==0){
			String message = toString();
			ConsoleFactory.printToConsole(message, title, true);
		} else {
			for(String id:ids){
				String message = getString(id);
				ConsoleFactory.printToConsole(message, title, true);
			}
		}
	}
	
	public void clear(String id) {
		taskMap.remove(id);
	}
	public void clear() {
		taskMap.clear();
	}
	
	public void end(String id) {
		PerformanceTask task = taskMap.get(id);
		if(task==null) return;
		
		task.finishTime = System.currentTimeMillis();
		task.totalTime = task.totalTime + (task.finishTime - task.startTime);
	}
	
	public long getTimeCost(String id) {
		PerformanceTask task = taskMap.get(id);
		if(task==null)
			return -1;
		else 
			return task.totalTime;
	}
	
	public String toString() {
		Set<String> ids = taskMap.keySet();
		StringBuilder sb = new StringBuilder();
		for(String id : ids) {
			sb.append("Task ");
			sb.append(id);
			sb.append("\t costed ");
			sb.append(getTimeCost(id));
			sb.append(" ms totally.\n");
		}
		return sb.toString();
	}
	
	public String getString(String id) {
		StringBuilder sb = new StringBuilder();
		sb.append("Task ");
		sb.append(id);
		sb.append("\t costed ");
		sb.append(getTimeCost(id));
		sb.append(" ms totally.");
		return sb.toString();
	}
}



class PerformanceTask {
	public String id;
	public long totalTime = 0;
	public transient long startTime = 0;
	public transient long finishTime = 0;
}