/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IStructuralrmgExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public java.util.Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public org.eclipse.emf.ecore.EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement getSymtaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement follower, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>> getFollowers();
	
}
