/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.Template#getRoles <em>Roles</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.Template#getNodes <em>Nodes</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.Template#getEdges <em>Edges</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.Template#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.Template#getSuperTemplate <em>Super Template</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate()
 * @model
 * @generated
 */
public interface Template extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate_Roles()
	 * @model containment="true"
	 * @generated
	 */
	EList<Role> getRoles();

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.TemplateNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate_Nodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<TemplateNode> getNodes();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate_Edges()
	 * @model containment="true"
	 * @generated
	 */
	EList<TemplateEdge> getEdges();

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate_Abstract()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.Template#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Super Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Template</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Template</em>' reference.
	 * @see #setSuperTemplate(Template)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplate_SuperTemplate()
	 * @model
	 * @generated
	 */
	Template getSuperTemplate();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.Template#getSuperTemplate <em>Super Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Template</em>' reference.
	 * @see #getSuperTemplate()
	 * @generated
	 */
	void setSuperTemplate(Template value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Role> getAllRoles();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<TemplateNode> getAllNodes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<TemplateEdge> getAllEdges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isSuperOf(Template t);

} // Template
