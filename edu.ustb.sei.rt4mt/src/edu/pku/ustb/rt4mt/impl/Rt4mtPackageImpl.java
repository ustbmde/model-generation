/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.ExpectedOutput;
import edu.pku.ustb.rt4mt.InputParameterConfiguration;
import edu.pku.ustb.rt4mt.InputParameterConfigurationGroup;
import edu.pku.ustb.rt4mt.OutputParameterConfiguration;
import edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup;
import edu.pku.ustb.rt4mt.ParameterConfiguration;
import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.Rt4mtPackage;
import edu.pku.ustb.rt4mt.TestCase;
import edu.pku.ustb.rt4mt.TestCaseGroup;
import edu.pku.ustb.rt4mt.TestData;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;

import edu.pku.ustb.rt4mt.TestRoundGroup;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Rt4mtPackageImpl extends EPackageImpl implements Rt4mtPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regressionTestModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testRoundEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testExecutionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testRoundGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputParameterConfigurationGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputParameterConfigurationGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputParameterConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputParameterConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expectedOutputEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Rt4mtPackageImpl() {
		super(eNS_URI, Rt4mtFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Rt4mtPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Rt4mtPackage init() {
		if (isInited) return (Rt4mtPackage)EPackage.Registry.INSTANCE.getEPackage(Rt4mtPackage.eNS_URI);

		// Obtain or create and register package
		Rt4mtPackageImpl theRt4mtPackage = (Rt4mtPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Rt4mtPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Rt4mtPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theRt4mtPackage.createPackageContents();

		// Initialize created meta-data
		theRt4mtPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRt4mtPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Rt4mtPackage.eNS_URI, theRt4mtPackage);
		return theRt4mtPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegressionTestModel() {
		return regressionTestModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegressionTestModel_Name() {
		return (EAttribute)regressionTestModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegressionTestModel_Input() {
		return (EReference)regressionTestModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegressionTestModel_Output() {
		return (EReference)regressionTestModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegressionTestModel_InputBase() {
		return (EAttribute)regressionTestModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegressionTestModel_OutputBase() {
		return (EAttribute)regressionTestModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegressionTestModel_RunConfigurationName() {
		return (EAttribute)regressionTestModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegressionTestModel_TestCaseGroup() {
		return (EReference)regressionTestModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegressionTestModel_TestRoundGroup() {
		return (EReference)regressionTestModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCase() {
		return testCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestCase_Id() {
		return (EAttribute)testCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCase_Inputs() {
		return (EReference)testCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCase_ExpectedOutputs() {
		return (EReference)testCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestRound() {
		return testRoundEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestRound_RoundNumber() {
		return (EAttribute)testRoundEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestRound_TestExecutions() {
		return (EReference)testRoundEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestExecution() {
		return testExecutionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestExecution_TestCase() {
		return (EReference)testExecutionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestExecution_Passed() {
		return (EAttribute)testExecutionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCaseGroup() {
		return testCaseGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseGroup_TestCases() {
		return (EReference)testCaseGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestRoundGroup() {
		return testRoundGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestRoundGroup_Rounds() {
		return (EReference)testRoundGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputParameterConfigurationGroup() {
		return inputParameterConfigurationGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInputParameterConfigurationGroup_Parameters() {
		return (EReference)inputParameterConfigurationGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutputParameterConfigurationGroup() {
		return outputParameterConfigurationGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutputParameterConfigurationGroup_Parameters() {
		return (EReference)outputParameterConfigurationGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterConfiguration() {
		return parameterConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterConfiguration_Name() {
		return (EAttribute)parameterConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterConfiguration_ModelURI() {
		return (EAttribute)parameterConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputParameterConfiguration() {
		return inputParameterConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInputParameterConfiguration_RmgModelURI() {
		return (EAttribute)inputParameterConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutputParameterConfiguration() {
		return outputParameterConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOutputParameterConfiguration_MetamodelURI() {
		return (EAttribute)outputParameterConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestData() {
		return testDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestData_Name() {
		return (EAttribute)testDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestData_ModelURI() {
		return (EAttribute)testDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpectedOutput() {
		return expectedOutputEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExpectedOutput_Data() {
		return (EReference)expectedOutputEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rt4mtFactory getRt4mtFactory() {
		return (Rt4mtFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		regressionTestModelEClass = createEClass(REGRESSION_TEST_MODEL);
		createEAttribute(regressionTestModelEClass, REGRESSION_TEST_MODEL__NAME);
		createEAttribute(regressionTestModelEClass, REGRESSION_TEST_MODEL__INPUT_BASE);
		createEAttribute(regressionTestModelEClass, REGRESSION_TEST_MODEL__OUTPUT_BASE);
		createEAttribute(regressionTestModelEClass, REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME);
		createEReference(regressionTestModelEClass, REGRESSION_TEST_MODEL__INPUT);
		createEReference(regressionTestModelEClass, REGRESSION_TEST_MODEL__OUTPUT);
		createEReference(regressionTestModelEClass, REGRESSION_TEST_MODEL__TEST_CASE_GROUP);
		createEReference(regressionTestModelEClass, REGRESSION_TEST_MODEL__TEST_ROUND_GROUP);

		testCaseEClass = createEClass(TEST_CASE);
		createEAttribute(testCaseEClass, TEST_CASE__ID);
		createEReference(testCaseEClass, TEST_CASE__INPUTS);
		createEReference(testCaseEClass, TEST_CASE__EXPECTED_OUTPUTS);

		testRoundEClass = createEClass(TEST_ROUND);
		createEAttribute(testRoundEClass, TEST_ROUND__ROUND_NUMBER);
		createEReference(testRoundEClass, TEST_ROUND__TEST_EXECUTIONS);

		testExecutionEClass = createEClass(TEST_EXECUTION);
		createEReference(testExecutionEClass, TEST_EXECUTION__TEST_CASE);
		createEAttribute(testExecutionEClass, TEST_EXECUTION__PASSED);

		testCaseGroupEClass = createEClass(TEST_CASE_GROUP);
		createEReference(testCaseGroupEClass, TEST_CASE_GROUP__TEST_CASES);

		testRoundGroupEClass = createEClass(TEST_ROUND_GROUP);
		createEReference(testRoundGroupEClass, TEST_ROUND_GROUP__ROUNDS);

		inputParameterConfigurationGroupEClass = createEClass(INPUT_PARAMETER_CONFIGURATION_GROUP);
		createEReference(inputParameterConfigurationGroupEClass, INPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS);

		outputParameterConfigurationGroupEClass = createEClass(OUTPUT_PARAMETER_CONFIGURATION_GROUP);
		createEReference(outputParameterConfigurationGroupEClass, OUTPUT_PARAMETER_CONFIGURATION_GROUP__PARAMETERS);

		parameterConfigurationEClass = createEClass(PARAMETER_CONFIGURATION);
		createEAttribute(parameterConfigurationEClass, PARAMETER_CONFIGURATION__NAME);
		createEAttribute(parameterConfigurationEClass, PARAMETER_CONFIGURATION__MODEL_URI);

		inputParameterConfigurationEClass = createEClass(INPUT_PARAMETER_CONFIGURATION);
		createEAttribute(inputParameterConfigurationEClass, INPUT_PARAMETER_CONFIGURATION__RMG_MODEL_URI);

		outputParameterConfigurationEClass = createEClass(OUTPUT_PARAMETER_CONFIGURATION);
		createEAttribute(outputParameterConfigurationEClass, OUTPUT_PARAMETER_CONFIGURATION__METAMODEL_URI);

		testDataEClass = createEClass(TEST_DATA);
		createEAttribute(testDataEClass, TEST_DATA__NAME);
		createEAttribute(testDataEClass, TEST_DATA__MODEL_URI);

		expectedOutputEClass = createEClass(EXPECTED_OUTPUT);
		createEReference(expectedOutputEClass, EXPECTED_OUTPUT__DATA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		inputParameterConfigurationEClass.getESuperTypes().add(this.getParameterConfiguration());
		outputParameterConfigurationEClass.getESuperTypes().add(this.getParameterConfiguration());

		// Initialize classes and features; add operations and parameters
		initEClass(regressionTestModelEClass, RegressionTestModel.class, "RegressionTestModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRegressionTestModel_Name(), ecorePackage.getEString(), "name", null, 0, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegressionTestModel_InputBase(), ecorePackage.getEString(), "inputBase", null, 0, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegressionTestModel_OutputBase(), ecorePackage.getEString(), "outputBase", null, 0, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegressionTestModel_RunConfigurationName(), ecorePackage.getEString(), "runConfigurationName", null, 0, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegressionTestModel_Input(), this.getInputParameterConfigurationGroup(), null, "input", null, 1, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegressionTestModel_Output(), this.getOutputParameterConfigurationGroup(), null, "output", null, 1, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegressionTestModel_TestCaseGroup(), this.getTestCaseGroup(), null, "testCaseGroup", null, 1, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegressionTestModel_TestRoundGroup(), this.getTestRoundGroup(), null, "testRoundGroup", null, 1, 1, RegressionTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseEClass, TestCase.class, "TestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestCase_Id(), ecorePackage.getEInt(), "id", null, 0, 1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCase_Inputs(), this.getTestData(), null, "inputs", null, 0, -1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCase_ExpectedOutputs(), this.getExpectedOutput(), null, "expectedOutputs", null, 0, -1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testRoundEClass, TestRound.class, "TestRound", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestRound_RoundNumber(), ecorePackage.getEInt(), "roundNumber", null, 0, 1, TestRound.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestRound_TestExecutions(), this.getTestExecution(), null, "testExecutions", null, 0, -1, TestRound.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testExecutionEClass, TestExecution.class, "TestExecution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestExecution_TestCase(), this.getTestCase(), null, "testCase", null, 0, 1, TestExecution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestExecution_Passed(), ecorePackage.getEBoolean(), "passed", null, 0, 1, TestExecution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseGroupEClass, TestCaseGroup.class, "TestCaseGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestCaseGroup_TestCases(), this.getTestCase(), null, "testCases", null, 0, -1, TestCaseGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testRoundGroupEClass, TestRoundGroup.class, "TestRoundGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestRoundGroup_Rounds(), this.getTestRound(), null, "rounds", null, 0, -1, TestRoundGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputParameterConfigurationGroupEClass, InputParameterConfigurationGroup.class, "InputParameterConfigurationGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInputParameterConfigurationGroup_Parameters(), this.getInputParameterConfiguration(), null, "parameters", null, 0, -1, InputParameterConfigurationGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputParameterConfigurationGroupEClass, OutputParameterConfigurationGroup.class, "OutputParameterConfigurationGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutputParameterConfigurationGroup_Parameters(), this.getOutputParameterConfiguration(), null, "parameters", null, 0, -1, OutputParameterConfigurationGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterConfigurationEClass, ParameterConfiguration.class, "ParameterConfiguration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterConfiguration_Name(), ecorePackage.getEString(), "name", null, 0, 1, ParameterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterConfiguration_ModelURI(), ecorePackage.getEString(), "modelURI", null, 0, 1, ParameterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputParameterConfigurationEClass, InputParameterConfiguration.class, "InputParameterConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInputParameterConfiguration_RmgModelURI(), ecorePackage.getEString(), "rmgModelURI", null, 0, 1, InputParameterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputParameterConfigurationEClass, OutputParameterConfiguration.class, "OutputParameterConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOutputParameterConfiguration_MetamodelURI(), ecorePackage.getEString(), "metamodelURI", null, 0, 1, OutputParameterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testDataEClass, TestData.class, "TestData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestData_Name(), ecorePackage.getEString(), "name", null, 0, 1, TestData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestData_ModelURI(), ecorePackage.getEString(), "modelURI", null, 0, 1, TestData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expectedOutputEClass, ExpectedOutput.class, "ExpectedOutput", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpectedOutput_Data(), this.getTestData(), null, "data", null, 0, -1, ExpectedOutput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Rt4mtPackageImpl
