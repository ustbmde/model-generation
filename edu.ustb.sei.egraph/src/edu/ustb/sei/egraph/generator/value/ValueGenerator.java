package edu.ustb.sei.egraph.generator.value;

import java.util.concurrent.ConcurrentHashMap;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

final public class ValueGenerator {
	final static public ValueGenerator SINGLETON = new ValueGenerator();
	
	private ValueSpecParser parser;
	
	@SuppressWarnings("unchecked")
	public <T> AbstractRandomFunction<T> parse(String code) {
		ANTLRInputStream s = new ANTLRInputStream(code);
		ValueSpecLexer lexer = new ValueSpecLexer(s);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ValueSpecParser p = this.getParser();
		p.setTokenStream(tokens);
		p.reset();
		return (AbstractRandomFunction<T>) p.specification().function;
	}

	public ValueSpecParser getParser() {
		if(parser==null) {
			parser = new ValueSpecParser(null);
		}
		return parser;
	}
	
	
	private ConcurrentHashMap<Object,AbstractRandomFunction<?>> functionCache = new  ConcurrentHashMap<Object,AbstractRandomFunction<?>>();
	
	@SuppressWarnings("unchecked")
	public <T> T generate(Object codeKey, String code, RandomService service) {
		
		
		AbstractRandomFunction<T> function = codeKey==null ? null : (AbstractRandomFunction<T>) functionCache.get(codeKey); 
		if(function==null) {
			function = parse(code);
			functionCache.put(codeKey, function);
		}
		return function.getValue(service).getValue(service);
	}

}
