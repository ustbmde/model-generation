package edu.ustb.sei.rmg;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.proxy.EListProxy;
import edu.ustb.sei.rmg.util.ExistingMap;
import edu.ustb.sei.rmg.util.OrderedPairMap;

/*
 * 默锟斤拷锟斤拷锟斤拷锟� 锟斤拷锟节斤拷止锟截革拷锟斤拷
 * 锟斤拷支锟街斤拷止圈锟酵伙拷
 */
//public class ForbiddenMap extends OrderedPairEMap<EObject, EObject, Object> {
//	
//	
//	
//	
//	public ForbiddenMap(boolean ringAllowed, boolean circleAllowed, boolean isTransitive) {
//		this.ringAllowed = ringAllowed;
//		this.circleAllowed = circleAllowed;
//		this.isTransitive = isTransitive;
//	}
//	
//	static public ForbiddenMap createNormalLinkForbiddenMap() {
//		return new ForbiddenMap(false, true, false);
//	}
//	
//	static public ForbiddenMap createSpecialLinkForbiddenMap() {
//		return new ForbiddenMap(false, false, true);
//	}
//	
//	private ExistingMap<EObject> visited = new ExistingMap<EObject>();
//	static final private Object forbiddenForEver = "ForbiddenForEver";
//	static final private Object forbidden = "Forbidden";
//	
//	protected boolean ringAllowed = false;
//	protected boolean circleAllowed = false;
//	protected boolean isTransitive = true;
//	
//	protected OrderedPairEMap<EObject,EObject,Object> forbiddenCache = 
//			new OrderedPairEMap<EObject,EObject,Object>();
//	
//	
//	public boolean isForbidden(EObject src, EObject tar) {
//		visited.clear();
//		return travelTo(src,tar);
//	}
//	
//	/*
//	 * 锟斤拷锟斤拷锟斤拷锟斤拷锟侥壳帮拷锟斤拷值锟斤拷锟斤拷锟斤拷锟斤拷锟狡匡拷锟� 锟斤拷要锟斤拷细锟叫撅拷
//	 */
//	private boolean travelTo(EObject cur, EObject tar) {
//		
//		Object cachedBoolean = forbiddenCache.get(cur, tar);
//		if(cachedBoolean!=null) return true;
//
//		
//		if(cur==tar&&ringAllowed==false)
//			return true;
//		
//		if(visited.isExist(cur))
//			return false;		
//		visited.setExist(cur);
//
//		HashMap<EObject, Object> map = get(cur,true);
//		if(map==null) return false;
//		
//		Object isForbided = map.get(tar);
//		if(isForbided==forbiddenForEver)
//			return true;
//		if(isForbided!=null)
//			return true;
//		
//		if(isTransitive) {
//			Set<Entry<EObject,Object>> set = map.entrySet();
//			for(Entry<EObject,Object> e : set) {
//				if(e.getKey()==tar){ 
//					continue;
//				} else {
//					boolean b = travelTo(e.getKey(),tar);
//					if(b==true) {
//						forbiddenCache.put(cur, tar, true);
//						return true;
//					}
//				}
//			}
//		}
//		return false;
//	}
//	
//	public void forbid(EObject src, EObject tar, EReference belief){
//		Object list = null;
//		
//		list = get(src,tar);
//		if(list!=forbiddenForEver) {
//			put(src,tar,forbidden);
//		}
//		
//		if(circleAllowed==false) {
//			list = get(tar,src);
//			if(list!=forbiddenForEver) {
//				put(tar,src,forbidden);
//			}
//		}
//	}
//	
//	public void reset(EObject src, EObject tar, EReference ref) {
//		Object isForbided = null;
//		
//		isForbided = get(src,tar);
//		if(isForbided!=forbiddenForEver) {
//			put(src,tar,null);
//		}
//		
//		if(circleAllowed==false) {
//			isForbided = get(tar,src);
//			if(isForbided!=forbiddenForEver) {
//				put(tar,src,null);
//			}
//		}
//		resetCache(src,tar);
//	}
//	
//	private void resetCache(EObject src, EObject tar) {
//		forbiddenCache.clear();
//	}
//
//}

public class ForbiddenMap {
	public ForbiddenMap(boolean ringAllowed, boolean circleAllowed, boolean isTransitive) {
		this.ringAllowed = ringAllowed;
		this.circleAllowed = circleAllowed;
		this.transitive = isTransitive;
	}
	
	static public ForbiddenMap createNormalLinkForbiddenMap() {
//		return (IForbiddenMap)Proxy.newProxyInstance(IForbiddenMap.class.getClassLoader(),new Class<?>[]{IForbiddenMap.class}, new ForbiddenMapProxy(new ForbiddenMap(false, true, false)));
		return new ForbiddenMap(false, true, false);
	}
	
	static public ForbiddenMap createSpecialLinkForbiddenMap() {
//		return (IForbiddenMap)Proxy.newProxyInstance(IForbiddenMap.class.getClassLoader(),new Class<?>[]{IForbiddenMap.class}, new ForbiddenMapProxy(new ForbiddenMap(false, false, true)));
		return new ForbiddenMap(false, false, true);
	}
	
//	private ExistingMap<ForbiddenNode> visited = new ExistingMap<ForbiddenNode>();
	
	protected boolean ringAllowed = false; //锟斤拷锟斤拷锟角凤拷锟斤拷锟斤拷锟皆癸拷锟斤拷
	protected boolean circleAllowed = false; //锟斤拷锟斤拷锟角凤拷锟斤拷锟斤拷锟铰�
	protected boolean transitive = true; //锟斤拷锟斤拷锟角否传碉拷
	
	protected OrderedPairMap<EObject,EObject,Object> forbiddenCache = 
			new OrderedPairMap<EObject,EObject,Object>();
	
	protected HashMap<EObject, ForbiddenNode> obj2node = new HashMap<EObject, ForbiddenNode>();
	
	/* (non-Javadoc)
	 * @see edu.ustb.sei.rmg.IForbiddenMap#collectNonForbiddenObjects(org.eclipse.emf.ecore.EObject, java.util.List, org.eclipse.emf.ecore.EClass)
	 */
	public Iterator<EObject> collectNonForbiddenObjects(EObject src, List<EObject> tar, EClass typeFilter) {
//		visited.clear();
		ExistingMap<ForbiddenNode> visited = new ExistingMap<ForbiddenNode>();
		ForbiddenNode s = getNode(src,false);
		
		ExistingMap<EObject> candidates = new ExistingMap<EObject>(tar, typeFilter);
		
		if(ringAllowed==false) {
//			if(candidates.contains(s.element)) {
				candidates.remove(s.element);
//			}
		}
		
		collectNonForbiddenObjects(s,candidates,visited);
		
		//System.out.println("collect1 "+candidates.size());
		
		return candidates.iterator();
	}
	
	private void collectNonForbiddenObjects(ForbiddenNode cur, ExistingMap<EObject> candidates, ExistingMap<ForbiddenNode> visited) {
		if(visited.contains(cur))
			return;		
		visited.add(cur);
		
		for(ForbiddenNode n : cur.getOrderingTargetNodes()) {
//			if(candidates.contains(n.element)) {
				candidates.remove(n.element);
//			}
		}
		
		if(transitive) {
			for(ForbiddenNode n : cur.getOrderingTargetNodes()) {
				collectNonForbiddenObjects(n,candidates, visited);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.ustb.sei.rmg.IForbiddenMap#collectNonForbiddenObjects(java.util.List, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EClass)
	 */
	public Iterator<EObject> collectNonForbiddenObjects(List<EObject> src, EObject tar, EClass typeFilter) {
//		visited.clear();
		ExistingMap<ForbiddenNode> visited = new ExistingMap<ForbiddenNode>();
		ForbiddenNode t = getNode(tar,false);
		
		ExistingMap<EObject> candidates = new ExistingMap<EObject>(src, typeFilter);
		
		if(ringAllowed==false) {
//			if(candidates.contains(t.element)) {
			candidates.remove(t.element);
//			}
		}
		
		collectNonForbiddenObjects(candidates,t,visited);
		
		//System.out.println("collect2 "+candidates.size());
		
		return candidates.iterator();
	}
	//没锟叫达拷锟斤拷锟斤拷锟斤拷锟斤拷
	private void collectNonForbiddenObjects(ExistingMap<EObject> candidates,
			ForbiddenNode cur,ExistingMap<ForbiddenNode> visited) {
		if(visited.contains(cur))
			return;		
		visited.add(cur);
		
//		if(candidates.contains(cur.element)) {
//			candidates.remove(cur.element);
//		}
		
		for(ForbiddenNode n : cur.getOrderingSourceNodes()) {
//			if(candidates.contains(n.element)) {
				candidates.remove(n.element);
//			}
		}
		
		if(transitive) {
			for(ForbiddenNode n : cur.getOrderingSourceNodes()) { 
				collectNonForbiddenObjects(candidates,n,visited);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.ustb.sei.rmg.IForbiddenMap#forbid(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference)
	 */
	public void forbid(EObject src, EObject tar, EReference belief) {
		ForbiddenNode s = getNode(src, false);
		ForbiddenNode t = getNode(tar,false);
		
		s.createPhysicalEdge(t);
		
		if(circleAllowed==false) {
			t.createOrderingEdge(s);
//			
//			t.targetNodes.add(s);
//			s.sourceNodes.add(t);
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.ustb.sei.rmg.IForbiddenMap#reset(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference)
	 */
	public void reset(EObject src, EObject tar, EReference believe) {
		ForbiddenNode s = getNode(src, true);
		if(s==null) return;
		
		ForbiddenNode t = getNode(tar,true);
		if(t==null) return;
		s.deletePhysicalEdge(t);
//		s.targetNodes.remove(t);
//		t.sourceNodes.remove(s);

		if(circleAllowed==false) {
			t.deleteOrderingEdge(s);
//			t.targetNodes.remove(s);
//			s.sourceNodes.remove(t);
		}
		
		forbiddenCache.clear();
	}
	
	protected ForbiddenNode getNode(EObject o, boolean readOnly) {
		ForbiddenNode r = obj2node.get(o);
		if(r==null && !readOnly) {
			r = new ForbiddenNode(o);
			obj2node.put(o, r);
		}
		return r;
	}
	
	/* (non-Javadoc)
	 * @see edu.ustb.sei.rmg.IForbiddenMap#isForbidden(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 */
	public boolean isForbidden(EObject src, EObject tar) {
		if(ringAllowed==false && src == tar) return true;
//		visited.clear();
		ForbiddenNode s = getNode(src, true);
		if(s==null) return false;
		ForbiddenNode t = getNode(tar, true);
		if(t==null) return false;
		
		if(s.hasTarget(t, false))
			return false; // 违反幂等律，目前我们不能在两点间创建多条边
		
		ExistingMap<ForbiddenNode> visited = new ExistingMap<ForbiddenNode>();
		return travelTo(s,t,visited);
	}
	
	/*
	 * 这个方法只会检查有序性约束
	 */
	private boolean travelTo(ForbiddenNode src, ForbiddenNode tar,ExistingMap<ForbiddenNode> visited) {
		if(visited.contains(src)) 
			return false;
		visited.add(src);
		
		if(forbiddenCache.get(src.element, tar.element)!=null) 
			return true;
		
		if(src.hasTarget(tar, true)) {
			forbiddenCache.put(src.element, tar.element, true);
			return true;
		}
		
		if(transitive) {
			for(ForbiddenNode n : src.getOrderingTargetNodes()) {
				if(travelTo(n,tar,visited)) {
					forbiddenCache.put(n.element, tar.element, true);
					return true;
				}
			}
		}
		
		return false;
	}
}

class ForbiddenNode {
	public EObject element;
	public ForbiddenNode(EObject element) {
		super();
		this.element = element;
		
		physicalSourceNodes = new ArrayList<ForbiddenNode>();
		physicalTargetNodes = new ArrayList<ForbiddenNode>();
		orderingSourceNodes = new ArrayList<ForbiddenNode>();
		orderingTargetNodes = new ArrayList<ForbiddenNode>();
	}

	public void createOrderingEdge(ForbiddenNode s) {
		this.orderingTargetNodes.add(s);
		s.orderingSourceNodes.add(this);
	}

	private List<ForbiddenNode> physicalSourceNodes; // 表示从这些节点到当前节点存在实际边
	private List<ForbiddenNode> physicalTargetNodes; // 表示从当前节点到这些节点存在实际边
	
	private List<ForbiddenNode> orderingSourceNodes; // 表示从这些节点到当前节点存在序约束，即不能从这些节点建立到当前节点的边
	private List<ForbiddenNode> orderingTargetNodes; // 表示从这些节点到当前节点存在序约束
	
	protected boolean hasTarget(ForbiddenNode tar, boolean checkOrdering) {
		if(checkOrdering)
			return orderingTargetNodes.contains(tar);
		else
			return physicalTargetNodes.contains(tar);
	}

	protected boolean hasSource(ForbiddenNode src, boolean checkOrdering) {
		if(checkOrdering)
			return orderingSourceNodes.contains(src);
		else
			return physicalSourceNodes.contains(src);
	}
	
	public void createPhysicalEdge(ForbiddenNode tar) {
		this.physicalTargetNodes.add(tar);
		tar.physicalSourceNodes.add(this);
	}
	
	public void deletePhysicalEdge(ForbiddenNode tar) {
		this.physicalTargetNodes.remove(tar);
		tar.physicalSourceNodes.remove(this);
	}
	
	public void deleteOrderingEdge(ForbiddenNode tar) {
		this.orderingTargetNodes.remove(tar);
		tar.orderingSourceNodes.remove(this);
	}

	public List<ForbiddenNode> getPhysicalSourceNodes() {
		return physicalSourceNodes;
	}

	public List<ForbiddenNode> getPhysicalTargetNodes() {
		return physicalTargetNodes;
	}

	public List<ForbiddenNode> getOrderingSourceNodes() {
		return orderingSourceNodes;
	}

	public List<ForbiddenNode> getOrderingTargetNodes() {
		return orderingTargetNodes;
	}
	
//	protected ForbiddenNode getTargetByElement(EObject e) {
//		for(ForbiddenNode n : targetNodes){
//			if(n.element == e)
//				return n;
//		}
//		return null;
//	}
//	
//	protected ForbiddenNode getSourceByElement(EObject e) {
//		for(ForbiddenNode n : sourceNodes){
//			if(n.element == e)
//				return n;
//		}
//		return null;
//	}
}
