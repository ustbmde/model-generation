package edu.ustb.sei.mt4mt.base.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class ClearAction implements IAction {
	public ClearAction(String[] files) {
		super();
		this.files = files;
	}

	private String[] files;

	@Override
	public void run(Context context) {
		for(String file : files) {
			IPath path = new Path(file);
			IFile f = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			try {
				f.delete(true, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

}
