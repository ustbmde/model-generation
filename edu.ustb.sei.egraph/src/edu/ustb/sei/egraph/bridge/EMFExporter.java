package edu.ustb.sei.egraph.bridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;

import edu.ustb.sei.egraph.generator.graph.config.GraphConfig;
import edu.ustb.sei.egraph.generator.value.ValueParser;
import edu.ustb.sei.egraph.structure.EExternalNode;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;

public class EMFExporter implements Exporter<Resource, EClass, EReference, EAttribute, Void> {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void exportGraph(Resource output, EGraph<EClass, EReference, EAttribute, Void> graph) {
		Map<ENode<EClass, EReference, EAttribute>,EObject> nodeObjectMap = new HashMap<ENode<EClass,EReference,EAttribute>, EObject>();
		
		graph.getNodes().forEach(n->{
			EObject eObj;
			if(n instanceof EExternalNode<?, ?, ?>) {
				URI uri = URI.createURI(((EExternalNode<EClass, EReference, EAttribute>) n).getURI());
				eObj = output.getResourceSet().getEObject(uri, true);
			} else {
				eObj = EcoreUtil.create(n.getType());
				n.allProperties().forEach(e->{
					Object castValue = cast(e.getValue(),e.getKey());
					if(e.getKey().isMany()) {
						((List)eObj.eGet(e.getKey())).addAll((List<?>)castValue);
					} else {
						eObj.eSet(e.getKey(), castValue);						
					}
				});
			}
			nodeObjectMap.put(n, eObj);
		});
		
		graph.getEdges().forEach(e->{
			EReference ref = e.getType();
			EObject src = nodeObjectMap.get(e.getSource());
			EObject tar = nodeObjectMap.get(e.getTarget());
			
			if(ref.isMany()) {
				List<EObject> list = (List<EObject>)src.eGet(ref);
				list.add(tar);
			} else {
				src.eSet(ref,tar);
			}
		});
		
		graph.getNodes().forEach(n->{
			if(!(n instanceof EExternalNode<?, ?, ?>)) {
				EObject obj = nodeObjectMap.get(n);
				if(obj.eContainer()==null)
					output.getContents().add(obj);
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object cast(Object value, EAttribute type) {
		if(type.isMany()) {
			if(!(value instanceof List)) {
				value = Collections.singletonList(value);
			}
			List cast = new ArrayList();
			for(Object v : (List)value) {
				cast.add(cast(v,type.getEAttributeType()));
			}
			return cast;
		} else {
			return cast(value,type.getEAttributeType());
		}
	}

	private Object cast(Object v, EDataType eAttributeType) {
		if(v==null) return null;
		
		if(eAttributeType instanceof EEnum) {
			if(v instanceof Integer) {
				return ((EEnum) eAttributeType).getEEnumLiteral((Integer)v);
			} else if(v instanceof Long) {
				return ((EEnum) eAttributeType).getEEnumLiteral(((Long)v).intValue());
			} else if(v instanceof String) {
				return ((EEnum)eAttributeType).getEEnumLiteral(v.toString());
			}
		} else {
			String instanceName = eAttributeType.getInstanceTypeName();
			if(instanceName==null)
				return v;
			else {
				switch(instanceName) {
				case "int":
				case "java.lang.Integer":
					if(v instanceof Integer) {
						return v;
					} else if(v instanceof Long) {
						return ((Long) v).intValue();
					} else if(v instanceof Double) {
						return ((Double) v).intValue();
					} else if(v instanceof String) {
						return Integer.parseInt((String)v);
					}
					break;
				case "long":
				case "java.lang.Long":
					if(v instanceof Integer) {
						return ((Integer) v).longValue();
					} else if(v instanceof Long) {
						return v;
					} else if(v instanceof Double) {
						return ((Double) v).longValue();
					} else if(v instanceof String) {
						return Long.parseLong((String)v);
					}
					break;
				case "char":
				case "java.lang.Character":
					if(v instanceof Integer) {
						return (char)((Integer) v).intValue();
					} else if(v instanceof Long) {
						return (char)((Long) v).intValue();
					} else if(v instanceof Double) {
						return (char)((Double) v).intValue();
					} else if(v instanceof String) {
						return ((String) v).charAt(0);
					}
					break;
				case "byte":
				case "java.lang.Byte":
					if(v instanceof Integer) {
						return ((Integer) v).byteValue();
					} else if(v instanceof Long) {
						return ((Long) v).byteValue();
					} else if(v instanceof Double) {
						return ((Double) v).byteValue();
					} else if(v instanceof String) {
						return Byte.parseByte((String)v);
					}
					break;
				case "float":
				case "java.lang.Float":
					if(v instanceof Integer) {
						return ((Integer) v).floatValue();
					} else if(v instanceof Long) {
						return ((Long) v).floatValue();
					} else if(v instanceof Double) {
						return ((Double) v).floatValue();
					} else if(v instanceof String) {
						return Float.parseFloat((String)v);
					}
					break;
				case "double":
				case "java.lang.Double":
					if(v instanceof Integer) {
						return ((Integer) v).doubleValue();
					} else if(v instanceof Long) {
						return ((Long) v).doubleValue();
					} else if(v instanceof Double) {
						return v;
					} else if(v instanceof String) {
						return Double.parseDouble((String)v);
					}
					break;
				case "short":
				case "java.lang.Short":
					if(v instanceof Integer) {
						return ((Integer) v).shortValue();
					} else if(v instanceof Long) {
						return ((Long) v).shortValue();
					} else if(v instanceof Double) {
						return ((Double) v).shortValue();
					} else if(v instanceof String) {
						return Short.parseShort((String)v);
					}
					break;
				case "java.util.Date":
					if(v instanceof Integer) {
						return new Date(((Integer) v).longValue());
					} else if(v instanceof Long) {
						return new Date(((Long) v).longValue());
					} else if(v instanceof Double) {
						return new Date(((Double) v).longValue());
					} else if(v instanceof String) {
						return ValueParser.parseDate(v.toString());
					}
					break;
				}
			}
		}
		
		return v;
	}

	@Override
	public void save(Resource output, Map<?, ?> config) throws Exception {
		Map<Object,Object> xmiMap = new HashMap<Object, Object>();
		
		xmiMap.put(XMLResource.OPTION_ENCODING, "UTF-8");
		
		config.entrySet().forEach(e->{
			if(e.getKey().equals(GraphConfig.SAVE_SCHEMA_LOCATION))
				xmiMap.put(XMLResource.OPTION_SCHEMA_LOCATION,e.getValue());
			else if(e.getKey().equals(GraphConfig.SAVE_COMPACT))
				xmiMap.put(XMLResource.OPTION_FORMATTED,e.getValue()==Boolean.FALSE);
			else if(e.getKey().equals(GraphConfig.SAVE_XMI_ID)) {
				if(output instanceof CustomizedXMIResource) {
					((CustomizedXMIResource) output).setUseUUID(e.getValue()==Boolean.TRUE);
				}
			}
		});
		
		output.save(xmiMap);
	}

}
