/**
 */
package edu.ustb.sei.mde.mt4mt;

import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.TestCase#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.TestCase#getMetamodels <em>Metamodels</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestCase()
 * @model abstract="true"
 * @generated
 */
public interface TestCase extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestCase_Id()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getId();

	/**
	 * Returns the value of the '<em><b>Metamodels</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodels</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodels</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestCase_Metamodels()
	 * @model
	 * @generated
	 */
	EList<String> getMetamodels();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="edu.ustb.sei.mde.mt4mt.AbstractTransformationTestCase"
	 * @generated
	 */
	AbstractTransformationTestCase build();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	long getDefaultWaitingTime();

} // TestCase
