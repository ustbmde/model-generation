package edu.ustb.sei.egraph.generator.util;

public class QualifiedName {
	public QualifiedName(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}
	public QualifiedName(String name) {
		this(name,defaultType);
	}
	final public String name;
	final public String type;
	
	static public final String defaultType = "default";
	
	@Override
	public boolean equals(Object obj) {
		if(obj==this) return true;
		if(obj instanceof QualifiedName) {
			if(name.equals(((QualifiedName)obj).name)
					&& type.equals(((QualifiedName)obj).type))
					return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return type==null ? name : name+":"+type;
	}
}
