/**
 */
package edu.ustb.sei.mde.mt4mt.provider;


import edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EvaluateOCLQueryActionItemProvider extends TestActionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluateOCLQueryActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addModelKeyPropertyDescriptor(object);
			addQueryPropertyDescriptor(object);
			addResultKeyPropertyDescriptor(object);
			addImportContextVariablesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Model Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModelKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EvaluateOCLQueryAction_modelKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EvaluateOCLQueryAction_modelKey_feature", "_UI_EvaluateOCLQueryAction_type"),
				 MT4MTPackage.Literals.EVALUATE_OCL_QUERY_ACTION__MODEL_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Query feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addQueryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EvaluateOCLQueryAction_query_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EvaluateOCLQueryAction_query_feature", "_UI_EvaluateOCLQueryAction_type"),
				 MT4MTPackage.Literals.EVALUATE_OCL_QUERY_ACTION__QUERY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Result Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResultKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EvaluateOCLQueryAction_resultKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EvaluateOCLQueryAction_resultKey_feature", "_UI_EvaluateOCLQueryAction_type"),
				 MT4MTPackage.Literals.EVALUATE_OCL_QUERY_ACTION__RESULT_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Import Context Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addImportContextVariablesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EvaluateOCLQueryAction_importContextVariables_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EvaluateOCLQueryAction_importContextVariables_feature", "_UI_EvaluateOCLQueryAction_type"),
				 MT4MTPackage.Literals.EVALUATE_OCL_QUERY_ACTION__IMPORT_CONTEXT_VARIABLES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns EvaluateOCLQueryAction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EvaluateOCLQueryAction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String model = ((EvaluateOCLQueryAction)object).getModelKey();
		String query = ((EvaluateOCLQueryAction)object).getQuery();
		
		return model == null || query==null ?
			"evaluate OCL query" :
			"evaulate "+query+" on "+model;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EvaluateOCLQueryAction.class)) {
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION__MODEL_KEY:
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION__QUERY:
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION__RESULT_KEY:
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION__IMPORT_CONTEXT_VARIABLES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
