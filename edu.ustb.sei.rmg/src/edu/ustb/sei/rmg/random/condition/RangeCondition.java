package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

abstract public class RangeCondition<T> extends Condition<T> {
	public T lower;
	public T upper;
	
	@Override
	public T getValue(RandomUtil r, Object... extra) {
		if(lower==upper && lower != null) {
			return  lower;
		} else {
			if(lower!=null && upper==null) {
				return randomL(r);
			} else if(lower==null&&upper!=null) {
				return randomU(r);
			} else if(lower!=null&&upper!=null) {
				return randomLU(r);
			} else return null;
		}
	}
	@Override
	public Object[] getGenerationBound() {
		// TODO Auto-generated method stub
		return new Object[]{lower,upper};
	}
	
	abstract protected T randomLU(RandomUtil r);
	abstract protected T randomL(RandomUtil r);
	abstract protected T randomU(RandomUtil r);
}
