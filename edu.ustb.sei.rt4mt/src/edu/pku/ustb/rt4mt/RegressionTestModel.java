/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regression Test Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getName <em>Name</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInputBase <em>Input Base</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutputBase <em>Output Base</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getRunConfigurationName <em>Run Configuration Name</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInput <em>Input</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutput <em>Output</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestCaseGroup <em>Test Case Group</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestRoundGroup <em>Test Round Group</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel()
 * @model
 * @generated
 */
public interface RegressionTestModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Input</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' containment reference.
	 * @see #setInput(InputParameterConfigurationGroup)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_Input()
	 * @model containment="true" required="true"
	 * @generated
	 */
	InputParameterConfigurationGroup getInput();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInput <em>Input</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' containment reference.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(InputParameterConfigurationGroup value);

	/**
	 * Returns the value of the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' containment reference.
	 * @see #setOutput(OutputParameterConfigurationGroup)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_Output()
	 * @model containment="true" required="true"
	 * @generated
	 */
	OutputParameterConfigurationGroup getOutput();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutput <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' containment reference.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(OutputParameterConfigurationGroup value);

	/**
	 * Returns the value of the '<em><b>Input Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Base</em>' attribute.
	 * @see #setInputBase(String)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_InputBase()
	 * @model
	 * @generated
	 */
	String getInputBase();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getInputBase <em>Input Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Base</em>' attribute.
	 * @see #getInputBase()
	 * @generated
	 */
	void setInputBase(String value);

	/**
	 * Returns the value of the '<em><b>Output Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Base</em>' attribute.
	 * @see #setOutputBase(String)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_OutputBase()
	 * @model
	 * @generated
	 */
	String getOutputBase();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getOutputBase <em>Output Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Base</em>' attribute.
	 * @see #getOutputBase()
	 * @generated
	 */
	void setOutputBase(String value);

	/**
	 * Returns the value of the '<em><b>Run Configuration Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Run Configuration Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Run Configuration Name</em>' attribute.
	 * @see #setRunConfigurationName(String)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_RunConfigurationName()
	 * @model
	 * @generated
	 */
	String getRunConfigurationName();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getRunConfigurationName <em>Run Configuration Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Run Configuration Name</em>' attribute.
	 * @see #getRunConfigurationName()
	 * @generated
	 */
	void setRunConfigurationName(String value);

	/**
	 * Returns the value of the '<em><b>Test Case Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Case Group</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Case Group</em>' containment reference.
	 * @see #setTestCaseGroup(TestCaseGroup)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_TestCaseGroup()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TestCaseGroup getTestCaseGroup();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestCaseGroup <em>Test Case Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Case Group</em>' containment reference.
	 * @see #getTestCaseGroup()
	 * @generated
	 */
	void setTestCaseGroup(TestCaseGroup value);

	/**
	 * Returns the value of the '<em><b>Test Round Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Round Group</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Round Group</em>' containment reference.
	 * @see #setTestRoundGroup(TestRoundGroup)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getRegressionTestModel_TestRoundGroup()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TestRoundGroup getTestRoundGroup();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.RegressionTestModel#getTestRoundGroup <em>Test Round Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Round Group</em>' containment reference.
	 * @see #getTestRoundGroup()
	 * @generated
	 */
	void setTestRoundGroup(TestRoundGroup value);

} // RegressionTestModel
