SYNTAXDEF structuralrmg
FOR <http://www.ustb.edu.cn/sei/mde/structuralrmg> <structuralrmg.genmodel>
START TemplateModel

IMPORTS { 
    ecore : <http://www.eclipse.org/emf/2002/Ecore> 
}

OPTIONS {
	usePredefinedTokens="false";
	generateCodeFromGeneratorModel = "false";
	reloadGeneratorModel="true";
	overrideBuilder = "false"; 
	additionalDependencies = "org.eclipse.emf.ecore.xmi";
	//overrideLaunchConfigurationDelegate = "false";
	overrideLaunchConfigurationHelper = "false";
	overrideLaunchConfigurationMainTab = "false";
	overrideDefaultLoadOptionsExtensionPointSchema = "false";
}

TOKENS {
	DEFINE TEXT $('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-')*$; 
	DEFINE LINEBREAK $('\r\n'|'\r'|'\n')$;
	DEFINE WHITESPACE $(' '|'\t'|'\f')$;
	DEFINE URI $'<'('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-'|':'|'/'|'\\'|'\.')+'>'$;
	DEFINE PATH TEXT+$'::'$+TEXT;
	DEFINE NUMBER $'-'?('0'..'9')+$;
	DEFINE ML_COMMENT $ '/*'.*'*/'$;
}

TOKENSTYLES { 
    // show YOUR_TOKEN in black 
    "ML_COMMENT" COLOR #408060;
}

RULES {
	TemplateModel ::= "model" name[TEXT] !0("import" packages[URI] !0)* (templates !0)* ;
	
	Template ::= abstract["abstract":""] "template" name[TEXT] ("(" roles ("," roles)* ")")? ("extends" superTemplate[TEXT])? "{" !0   ("node" nodes ";" !0)* ("edge" edges ";"!0)*"}" ;
	
	Role ::= name[TEXT];
	
	Bound ::= "set" "[" min[NUMBER] ".." max[NUMBER] "]";
	
	PrimitiveTemplateNode ::=bound? name[TEXT] ":" type[TEXT]   ("[" attributes ("," attributes)* "]")? ("plays" role[TEXT])?;
	
	AttributeBinding ::= name[TEXT] ":" attribute[TEXT] ;
	
	NestedTemplateNode ::= bound? sort[nested:"", shared:"shared", reversed:"reversed"] name[TEXT] "@" templates[TEXT] ("," templates[TEXT])* ("plays" role[TEXT])?;
	
	PrimitiveTemplateEdge ::= name[TEXT] ":" reference[PATH] "(" source[TEXT] ("[" sourcePos[NUMBER] "]")? "," target[TEXT] ("[" targetPos[NUMBER] "]")?")" binding?;
	
	ChainTemplateEdge ::= name[TEXT] ":" reference[PATH] "in" round["loop":""] node[TEXT] binding?;
	
	EndpointBinding ::= ("for" "source" sourceMappings+)? ("for" "target" targetMappings+)?; 
	
	BindingMapping ::= template[TEXT] "=>" (roles[TEXT] ("," roles[TEXT])*)+ ("[" position[NUMBER] "]")?;
}