package edu.ustb.sei.rmg.run;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import edu.ustb.sei.rmg.ConcurrentGenerator;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.OptionModel;

public class RMGResourceSet extends ResourceSetImpl {
	private String baseURI = null;

	public RMGResourceSet(String base) {
		super();
		baseURI = base;
		if(baseURI.endsWith("/"))
			baseURI = baseURI.substring(0,baseURI.length()-1);
		this.uriConverter = new RMGURIConverter(baseURI);
		
		this.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		this.setPackageRegistry(EPackage.Registry.INSTANCE);
		this.getPackageRegistry().put(ConfigPackage.eNS_URI, ConfigPackage.eINSTANCE);
	}
	
	public Generator createModelGenerator(String configPath) {
		Generator g = null;
		
		Resource configRes = this.getResource(URI.createURI(configPath), true);
		OptionModel configModel = (OptionModel)configRes.getContents().get(0);
		
		if(configModel.isEnableConcurrency())
			g = new ConcurrentGenerator(this);
		else g = new Generator(this);

		g.loadMetamodel(configModel.getMetamodel());
		g.doConfig(configModel);
		
		return g;
	}
}

class RMGURIConverter extends ExtensibleURIConverterImpl {
	public RMGURIConverter(String basePath) {
		super();
		this.basePath = basePath;
	}
	private String basePath;
	public URI normalize(URI uri)
	  {
	    String fragment = uri.fragment();
	    String query = uri.query();
	    URI trimmedURI = uri.trimFragment().trimQuery();
	    URI result = getInternalURIMap().getURI(trimmedURI);
	    String scheme = result.scheme();
	    if (scheme == null) {
	    	if (result.hasAbsolutePath()) {
	    		result = URI.createURI("file:" + result);
	    	}
	    	else {
	    		result = URI.createURI("file:"+basePath + result);
	    	}
	    } else {
	    	String platformRes = "platform:/resource/";
	    	String string = result.toString();
	    	if(string.startsWith(platformRes)) {
	    		result = URI.createURI("file:"+basePath + string.substring(platformRes.length()-1));
	    	}
	    }


	    if (result == trimmedURI)
	    {
	    	System.out.println("Converted URI: "+uri.toString());
	      return uri;
	    }

	    if (query != null)
	    {
	      result = result.appendQuery(query);
	    }
	    if (fragment != null)
	    {
	      result = result.appendFragment(fragment);
	    }


	    return normalize(result);
	  }

}