package edu.ustb.sei.egraph.generator.tree.function;

import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class AttributeAssignmentFunction<NT, ET, NAT, EAT> extends AssignmentFunction<NT, ET, NAT, EAT> {

	public AttributeAssignmentFunction(String feature, int lower, int upper,AbstractRandomFunction<?> function) {
		super(feature, lower, upper);
		this.function = function;
	}

	public AttributeAssignmentFunction(String feature, int lower,AbstractRandomFunction<?> function) {
		this(feature, lower,lower,function);
	}

	public AttributeAssignmentFunction(String feature,AbstractRandomFunction<?> function) {
		this(feature,1,1,function);
	}

	@Override
	public Integer apply(TreeGenerationService<NT, ET, NAT, EAT> service) {
		// TODO Auto-generated method stub
		return null;
	}

	protected AbstractRandomFunction<?> function;
}
