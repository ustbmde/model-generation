/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.UpdateModelAction;
import edu.ustb.sei.mt4mt.base.Actions;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.URIClassLoader;
import edu.ustb.sei.mt4mt.base.updates.IUpdate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Update Model Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl#getModelKey <em>Model Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.UpdateModelActionImpl#getUpdateOperations <em>Update Operations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UpdateModelActionImpl extends TestActionImpl implements UpdateModelAction {
	/**
	 * The default value of the '{@link #getModelKey() <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelKey()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelKey() <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelKey()
	 * @generated
	 * @ordered
	 */
	protected String modelKey = MODEL_KEY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUpdateOperations() <em>Update Operations</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdateOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<String> updateOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdateModelActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.UPDATE_MODEL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelKey() {
		return modelKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelKey(String newModelKey) {
		String oldModelKey = modelKey;
		modelKey = newModelKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.UPDATE_MODEL_ACTION__MODEL_KEY, oldModelKey, modelKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getUpdateOperations() {
		if (updateOperations == null) {
			updateOperations = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.UPDATE_MODEL_ACTION__UPDATE_OPERATIONS);
		}
		return updateOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.UPDATE_MODEL_ACTION__MODEL_KEY:
				return getModelKey();
			case MT4MTPackage.UPDATE_MODEL_ACTION__UPDATE_OPERATIONS:
				return getUpdateOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.UPDATE_MODEL_ACTION__MODEL_KEY:
				setModelKey((String)newValue);
				return;
			case MT4MTPackage.UPDATE_MODEL_ACTION__UPDATE_OPERATIONS:
				getUpdateOperations().clear();
				getUpdateOperations().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.UPDATE_MODEL_ACTION__MODEL_KEY:
				setModelKey(MODEL_KEY_EDEFAULT);
				return;
			case MT4MTPackage.UPDATE_MODEL_ACTION__UPDATE_OPERATIONS:
				getUpdateOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.UPDATE_MODEL_ACTION__MODEL_KEY:
				return MODEL_KEY_EDEFAULT == null ? modelKey != null : !MODEL_KEY_EDEFAULT.equals(modelKey);
			case MT4MTPackage.UPDATE_MODEL_ACTION__UPDATE_OPERATIONS:
				return updateOperations != null && !updateOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelKey: ");
		result.append(modelKey);
		result.append(", updateOperations: ");
		result.append(updateOperations);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		URIClassLoader loader = new URIClassLoader(getClass().getClassLoader());
		List<IUpdate> updates = new ArrayList<IUpdate>();
		
		for(String uri : this.getUpdateOperations()) {
			try {
				Class<?> clazz = loader.loadClass(uri);
				Class<? extends IUpdate> actClazz = clazz.asSubclass(IUpdate.class);
				updates.add(actClazz.newInstance());
			} catch(Exception e) {
			}
		}
		
		if(updates.size()==0) {
			return Actions.fail();
		}
		else 
			return Actions.update(modelKey, updates.toArray(new IUpdate[updates.size()]));
	}

} //UpdateModelActionImpl
