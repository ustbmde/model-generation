package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class IntegerDistributionFunction extends DistributionFunction<Integer> {

	@Override
	public Integer getValue(RandomUtil r, Object... extra) {
		switch(name){
		case "geom":
			return (int)r.geometric(parameters[0]);
		case "pois":
			return (int)r.poisson(parameters[0]);
		}
		return 0;
	}

}
