/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.SequenceAction#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getSequenceAction()
 * @model
 * @generated
 */
public interface SequenceAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mt4mt.TestAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getSequenceAction_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestAction> getActions();

} // SequenceAction
