package edu.ustb.sei.rmg.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 只能保证主键的并发读写，并不能保证值的并发读写
 * 所谓主键并发读写是指，对于一级主键的读写可以保证并发
 * @author He Xiao
 *
 * @param <F>
 * @param <S>
 * @param <V>
 */
public class OrderedPairMap<F, S, V> {
	public ConcurrentHashMap<F, ConcurrentHashMap<S,V>> map = new ConcurrentHashMap<F, ConcurrentHashMap<S,V>>();
	
	private Map<S,V> get(F f) {
		ConcurrentHashMap<S,V> sMap = map.get(f);
		if(sMap==null){
			synchronized(f){//保证一级主键读写并发，防止同一个key的值竞争
				sMap = map.get(f);
				if(sMap==null) {
					sMap = new ConcurrentHashMap<S,V>();
					map.put(f, sMap);
				}
			}
		}
		return sMap;		
	}
	
	public void put(F f, S s,V v) {
		get(f).put(s, v);
	}
	
	public V get(F f, S s) {
		Map<S,V> map = get(f);
		return map.get(s);
	}

	public void clear() {
		map.clear();
	}

}
