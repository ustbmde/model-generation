package edu.ustb.sei.egraph.generator.value;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ValueParser {
	static public Integer parseInt(String str) {
		if(str.matches("-?[0-9]+")) {
			return Integer.parseInt(str);
		} else if(str.matches("0x[a-fA-F0-9]+")) {
			return Integer.parseInt(str.substring(2),16);
		} else if(str.matches("b[01]+")) {
			return Integer.parseInt(str.substring(1),2);
		} else
			throw new RuntimeException("cannot parse int from "+str);
	}
	
	static public Double parseDouble(String str) {
		return Double.parseDouble(str);
	}
	
	static public Character parseChar(String str) {
		if(str.matches("\'.\'")) {
			return str.charAt(1);
		} else if(str.matches("\'\\\\u[a-fA-F0-9]+\'")) {
			return (char)Integer.parseInt(str.substring(3,str.length()-1),16);
		} else
			throw new RuntimeException("cannot parse char from "+str);
	}
	
	static public String parseString(String str) {
		return str.substring(1,str.length()-1); // "..."
	}
	
	static public Long parseLong(String str) {
		if(str.matches("-?[0-9]+L")) {
			return Long.parseLong(str.substring(0, str.length()-1));
		} else
			throw new RuntimeException("cannot parse int from "+str);
	}
	
	static public Boolean parseBoolean(String str) {
		return Boolean.parseBoolean(str);
	}
	
	final static private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	final static private SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
	
	static public Date parseDate(String str) {
		try {
			if(str.matches("[0-9]+\\-[0-9]+\\-[0-9]+")) {
				return dateFormat.parse(str);
			} else
				return datetimeFormat.parse(str);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
