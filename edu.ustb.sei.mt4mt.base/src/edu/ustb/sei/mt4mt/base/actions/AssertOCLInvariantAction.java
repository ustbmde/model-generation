package edu.ustb.sei.mt4mt.base.actions;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.helper.OCLHelper;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class AssertOCLInvariantAction implements IAction {
	public AssertOCLInvariantAction(String invariant, String key, String[] im) {
		super();
		this.invariant = invariant;
		this.key = key;
		this.imports = im;
	}


	private String key;
	private String invariant;
	private String[] imports;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void run(Context context){
		
	    
	    Object model = context.getData(key);
	    
	    EObject firstRoot;
	    
	    if(model==null) {
	    	firstRoot=null;
	    	org.junit.Assert.fail("an empty model will fail any invariant");
	    } else {
	    	if(model instanceof Resource && ((Resource) model).getContents().size()>0) {
	    		firstRoot = ((Resource) model).getContents().get(0);
	    	} else if(model instanceof List && ((List)model).size()>0) {
	    		firstRoot = (EObject) ((List) model).get(0);
	    	} else if(model instanceof EObject) {
	    		firstRoot = (EObject) model;
	    	} else {
	    		firstRoot = null;
	    		org.junit.Assert.fail("an empty model will fail any invariant");
	    	}
	    }
	    
	    OCL<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, Constraint, EClass, EObject> ocl;
	    ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
	    ocl.setExtentMap(null);
	    
	    OCLHelper<EClassifier, ?, ?, Constraint> helper = ocl.createOCLHelper();

	    helper.setContext(firstRoot.eClass());
	    
	    String completeInvariant = "";
	    
	    if(imports!=null) {
	    	boolean first = true;
	    	
	    	for(String im : imports) {
	    		Object value = context.getData(im);
	    		String type = "Any";
	    		if(value==null)
	    			value = "undefined";
	    		else if(value instanceof String) {
	    			type = "String";
	    			value = "'"+value+"'";
	    		} else if(value instanceof Integer)
	    			type = "Integer";
	    		else if(value instanceof Boolean)
	    			type = "Boolean";
	    		else if(value instanceof EObject)
	    			type = ((EObject) value).eClass().getName();
	    		
	    		if(first) {
	    			completeInvariant = completeInvariant +" let "+im+" : "+type+" = "+value;
	    			first = false;
	    		} else 
	    			completeInvariant = completeInvariant +", "+im+" : "+type+" = "+value;
	    	}
	    	
	    	if(imports.length!=0)
	    		completeInvariant+=" in ";
	    }
	    
	    completeInvariant = completeInvariant+invariant;
	    
	    Constraint inv = null;
	    try {
	    	inv = helper.createInvariant(completeInvariant);
	    } catch(Exception e) {
	    	throw new RuntimeException(e);
	    }
	    
	    boolean check = ocl.check(firstRoot, inv);
		org.junit.Assert.assertTrue("the evaluation reslt of "+invariant+" is false", check);
	}
}
