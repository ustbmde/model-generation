/**
 */
package edu.ustb.sei.mde.mt4mt.provider;

import edu.ustb.sei.mde.mt4mt.util.MT4MTAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MT4MTItemProviderAdapterFactory extends MT4MTAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.TestSuite} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestSuiteItemProvider testSuiteItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.TestSuite}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTestSuiteAdapter() {
		if (testSuiteItemProvider == null) {
			testSuiteItemProvider = new TestSuiteItemProvider(this);
		}

		return testSuiteItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomizableTestCaseItemProvider customizableTestCaseItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCustomizableTestCaseAdapter() {
		if (customizableTestCaseItemProvider == null) {
			customizableTestCaseItemProvider = new CustomizableTestCaseItemProvider(this);
		}

		return customizableTestCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.GetPutTestCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GetPutTestCaseItemProvider getPutTestCaseItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.GetPutTestCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGetPutTestCaseAdapter() {
		if (getPutTestCaseItemProvider == null) {
			getPutTestCaseItemProvider = new GetPutTestCaseItemProvider(this);
		}

		return getPutTestCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.PutGetTestCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PutGetTestCaseItemProvider putGetTestCaseItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.PutGetTestCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPutGetTestCaseAdapter() {
		if (putGetTestCaseItemProvider == null) {
			putGetTestCaseItemProvider = new PutGetTestCaseItemProvider(this);
		}

		return putGetTestCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.SequenceAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SequenceActionItemProvider sequenceActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.SequenceAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSequenceActionAdapter() {
		if (sequenceActionItemProvider == null) {
			sequenceActionItemProvider = new SequenceActionItemProvider(this);
		}

		return sequenceActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.LoadModelAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoadModelActionItemProvider loadModelActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.LoadModelAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLoadModelActionAdapter() {
		if (loadModelActionItemProvider == null) {
			loadModelActionItemProvider = new LoadModelActionItemProvider(this);
		}

		return loadModelActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.SaveModelAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SaveModelActionItemProvider saveModelActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.SaveModelAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSaveModelActionAdapter() {
		if (saveModelActionItemProvider == null) {
			saveModelActionItemProvider = new SaveModelActionItemProvider(this);
		}

		return saveModelActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecuteLauncherActionItemProvider executeLauncherActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createExecuteLauncherActionAdapter() {
		if (executeLauncherActionItemProvider == null) {
			executeLauncherActionItemProvider = new ExecuteLauncherActionItemProvider(this);
		}

		return executeLauncherActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluateOCLQueryActionItemProvider evaluateOCLQueryActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createEvaluateOCLQueryActionAdapter() {
		if (evaluateOCLQueryActionItemProvider == null) {
			evaluateOCLQueryActionItemProvider = new EvaluateOCLQueryActionItemProvider(this);
		}

		return evaluateOCLQueryActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.CopyModelAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyModelActionItemProvider copyModelActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.CopyModelAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCopyModelActionAdapter() {
		if (copyModelActionItemProvider == null) {
			copyModelActionItemProvider = new CopyModelActionItemProvider(this);
		}

		return copyModelActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.CompareModelAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompareModelActionItemProvider compareModelActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.CompareModelAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCompareModelActionAdapter() {
		if (compareModelActionItemProvider == null) {
			compareModelActionItemProvider = new CompareModelActionItemProvider(this);
		}

		return compareModelActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertComparisonResultActionItemProvider assertComparisonResultActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertComparisonResultActionAdapter() {
		if (assertComparisonResultActionItemProvider == null) {
			assertComparisonResultActionItemProvider = new AssertComparisonResultActionItemProvider(this);
		}

		return assertComparisonResultActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertOCLInvariantActionItemProvider assertOCLInvariantActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssertOCLInvariantActionAdapter() {
		if (assertOCLInvariantActionItemProvider == null) {
			assertOCLInvariantActionItemProvider = new AssertOCLInvariantActionItemProvider(this);
		}

		return assertOCLInvariantActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.UserAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserActionItemProvider userActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.UserAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUserActionAdapter() {
		if (userActionItemProvider == null) {
			userActionItemProvider = new UserActionItemProvider(this);
		}

		return userActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.SetVariableAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetVariableActionItemProvider setVariableActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.SetVariableAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSetVariableActionAdapter() {
		if (setVariableActionItemProvider == null) {
			setVariableActionItemProvider = new SetVariableActionItemProvider(this);
		}

		return setVariableActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.UpdateModelAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdateModelActionItemProvider updateModelActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.UpdateModelAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUpdateModelActionAdapter() {
		if (updateModelActionItemProvider == null) {
			updateModelActionItemProvider = new UpdateModelActionItemProvider(this);
		}

		return updateModelActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.WrapperTestCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WrapperTestCaseItemProvider wrapperTestCaseItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.WrapperTestCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWrapperTestCaseAdapter() {
		if (wrapperTestCaseItemProvider == null) {
			wrapperTestCaseItemProvider = new WrapperTestCaseItemProvider(this);
		}

		return wrapperTestCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AutoExecutorTestSuiteItemProvider autoExecutorTestSuiteItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAutoExecutorTestSuiteAdapter() {
		if (autoExecutorTestSuiteItemProvider == null) {
			autoExecutorTestSuiteItemProvider = new AutoExecutorTestSuiteItemProvider(this);
		}

		return autoExecutorTestSuiteItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.ClearAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClearActionItemProvider clearActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.ClearAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createClearActionAdapter() {
		if (clearActionItemProvider == null) {
			clearActionItemProvider = new ClearActionItemProvider(this);
		}

		return clearActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.ExternalAction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalActionItemProvider externalActionItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.ExternalAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createExternalActionAdapter() {
		if (externalActionItemProvider == null) {
			externalActionItemProvider = new ExternalActionItemProvider(this);
		}

		return externalActionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalActionParameterItemProvider externalActionParameterItemProvider;

	/**
	 * This creates an adapter for a {@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createExternalActionParameterAdapter() {
		if (externalActionParameterItemProvider == null) {
			externalActionParameterItemProvider = new ExternalActionParameterItemProvider(this);
		}

		return externalActionParameterItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (testSuiteItemProvider != null) testSuiteItemProvider.dispose();
		if (customizableTestCaseItemProvider != null) customizableTestCaseItemProvider.dispose();
		if (getPutTestCaseItemProvider != null) getPutTestCaseItemProvider.dispose();
		if (putGetTestCaseItemProvider != null) putGetTestCaseItemProvider.dispose();
		if (sequenceActionItemProvider != null) sequenceActionItemProvider.dispose();
		if (loadModelActionItemProvider != null) loadModelActionItemProvider.dispose();
		if (saveModelActionItemProvider != null) saveModelActionItemProvider.dispose();
		if (executeLauncherActionItemProvider != null) executeLauncherActionItemProvider.dispose();
		if (evaluateOCLQueryActionItemProvider != null) evaluateOCLQueryActionItemProvider.dispose();
		if (copyModelActionItemProvider != null) copyModelActionItemProvider.dispose();
		if (compareModelActionItemProvider != null) compareModelActionItemProvider.dispose();
		if (assertComparisonResultActionItemProvider != null) assertComparisonResultActionItemProvider.dispose();
		if (assertOCLInvariantActionItemProvider != null) assertOCLInvariantActionItemProvider.dispose();
		if (userActionItemProvider != null) userActionItemProvider.dispose();
		if (setVariableActionItemProvider != null) setVariableActionItemProvider.dispose();
		if (updateModelActionItemProvider != null) updateModelActionItemProvider.dispose();
		if (wrapperTestCaseItemProvider != null) wrapperTestCaseItemProvider.dispose();
		if (autoExecutorTestSuiteItemProvider != null) autoExecutorTestSuiteItemProvider.dispose();
		if (clearActionItemProvider != null) clearActionItemProvider.dispose();
		if (externalActionItemProvider != null) externalActionItemProvider.dispose();
		if (externalActionParameterItemProvider != null) externalActionParameterItemProvider.dispose();
	}

}
