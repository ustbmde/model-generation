package edu.ustb.sei.mt4mt.base.actions;

import java.util.function.Function;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class JavaAction implements IAction {
	
	private Function<Context,Void> action;
	
	public JavaAction(Function<Context,Void> action) {
		super();
		this.action = action;
	}
	

	@Override
	public void run(Context context) {
		action.apply(context);
	}

}
