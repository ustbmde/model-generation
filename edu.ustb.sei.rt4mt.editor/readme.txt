1）首先建立模型转换的Run Configuration和RMG Config模型；
2）建立回归测试模型（在Example EMF Creation Wizard中，好像是这个名字:P），进行参数配置，设置Run Configuration的名字、输入模型位置、输出模型位置，测试时的测试输入目录（用于生成模型）、测试输出目录（用于保存输出）、RMG Model的位置、输出模型的元模型位置等等信息；
3）创建Test Round，此时会将上一个Test Round中的测试用例自动加入到新创建的Test Round中；
4）建立Test Case，此时会自动调用模型随机生成器创建模型；
5）执行Test Case，程序会自动判断输出结果是否符合预期，如不符合会提示用户人工判断，开发人员可以手动设置测试是否通过，并将实际输出加入到预期输出中；
6）回归测试时，可以根据以前的执行结果进行自动测试。