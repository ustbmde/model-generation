package edu.ustb.sei.mt4mt.base.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

abstract public class AbstractTransformationTestCase {
	static final public String[] STRING_ARRAY = new String[0];
	
	private String identifier;
	
	protected String[] metamodels;
	
	public String getExtraMessage() {
		return "no extra message";
	}
	
	public void setMetamodels(String... metamodels) {
		this.metamodels = metamodels;
	}
	
	protected AbstractTransformationTestCase() {
	}
	
	public AbstractTransformationTestCase(IAction action) {
		super();
		this.action = action;
	}

	protected IAction action;
	protected Context context;

	@Before
	public void setUp() {
		createContext();
		loadMetamodels();
	}
	
	protected void createContext() {
		context = new Context();
	}
	
	protected void loadMetamodels() {
		for(String m : metamodels) {
			context.loadMetamodel(m);
		}
	}

	@Test
	public void test() {
		action.run(context);
	}
	
	@After
	public void tearDown() {
		
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}
