package edu.ustb.sei.mt4mt.base.actions;

import java.util.List;

import org.eclipse.emf.compare.ComparePackage;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class AssertComparisonResultAction implements IAction {
	private int kind;
	
	public AssertComparisonResultAction(String key, int differentCount) {
		super();
		this.key = key;
		this.differentCount = differentCount;
		kind = 0;
	}
	
	public AssertComparisonResultAction(String key, int differentCount, int kind) {
		super();
		this.key = key;
		this.differentCount = differentCount;
		this.kind = kind;
	}

	private String key;
	private int differentCount;

	@Override
	public void run(Context context) {
		Comparison compare = (Comparison) context.getData(key);
		int diffSize = calcDiffSize(compare);
		
		if(kind==0)
			org.junit.Assert.assertEquals("assert the comparison result failed: ["+key+"]="+differentCount+" (actually "+diffSize+"!="+differentCount+")", differentCount, diffSize);
		else if(kind>0) {
			org.junit.Assert.assertTrue("assert the comparison result failed: ["+key+"]>"+differentCount+" (actually "+diffSize+"<="+differentCount+")", differentCount<diffSize);
		} else {
			org.junit.Assert.assertTrue("assert the comparison result failed: ["+key+"]<"+differentCount+" (actually "+diffSize+">="+differentCount+")", differentCount>diffSize);
		} 
	}

	protected int calcDiffSize(Comparison compare) {
		int diffSize = 0;
		for(Diff diff : compare.getDifferences()) {
			if(diff.getKind()==DifferenceKind.MOVE && diff.eClass() == ComparePackage.Literals.REFERENCE_CHANGE) {
				ReferenceChange change = (ReferenceChange) diff;
				EReference ref = change.getReference();
				
				
				if(ref.isOrdered()==false) {
					EObject leftObject = change.getValue();
					EObject rightObject = compare.getMatch(leftObject).getRight();
					Match match = change.getMatch();
					if(!(contain(match.getLeft(), ref, leftObject) && contain(match.getRight(), ref, rightObject)))
						diffSize++;
				} else {
					diffSize++;
				}
			} else {
				diffSize++;
			}
		}
		
		return diffSize;
	}

	private boolean contain(EObject container, EReference ref, EObject element) {
		if(container==null || element==null) return false;
		if(ref.isMany()) {
			List<Object> list = (List<Object>) container.eGet(ref);
			return list.contains(element);
		} else {
			return container.eGet(ref)==element;
		}
	}

}
