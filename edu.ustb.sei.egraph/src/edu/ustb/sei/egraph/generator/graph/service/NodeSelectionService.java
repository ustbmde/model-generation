package edu.ustb.sei.egraph.generator.graph.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetConfig;
import edu.ustb.sei.egraph.generator.util.DegreeTable;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;
import edu.ustb.sei.egraph.util.FastArrayList;
import edu.ustb.sei.egraph.util.HashableArrayList;


/**
 * it is defined for picking a node for edge generation
 * @author hexiao
 *
 */
public abstract class NodeSelectionService<NT,ET> {
	
//	private Map<ENode<?,?,?>,DegreeTable> degreeTable;
	
	public FastArrayList<ENode<NT,ET,?>> firstLevel;
	public FastArrayList<ENode<NT,ET,?>> secondLevel;
	public FastArrayList<ENode<NT,ET,?>> thirdLevel;
	
	public FastArrayList<Pair<ENode<NT,ET,?>,ET>> firstLevelAvailable;
	public FastArrayList<Pair<ENode<NT,ET,?>,ET>> secondLevelAvailable;
	
	protected List<ENode<NT,ET,?>> allNodes;
	protected List<ET> allReferences;
	
	protected GraphGenerationService<NT,ET,?,?> generationService;
	protected ETypeModel<NT,ET,?,?> typeModel;
	protected EdgeSetConfig config;
	
	protected HashMap<NT,List<ET>> refsOfTypes;
	
	private Map<NT,int[]> degreeMax;
	private Map<NT,int[]> degreeMin;
	
	private Map<NT,Integer> totalLowerBound;
	private Map<NT,Integer> totalUpperBound;
	
	private Map<NT, Boolean> typeCase;
	

	public NodeSelectionService(GraphGenerationService<NT,ET,?,?> generationService, EdgeSetConfig config, List<ENode<NT,ET,?>> allNodes, List<ET> allReferences) {
		this.allNodes = allNodes;
		this.allReferences = allReferences;
		
		this.generationService = generationService;
		
		typeModel = (ETypeModel<NT, ET, ?, ?>) generationService.getTypeModel();
		this.config = config;
		
		this.firstLevel = new FastArrayList<ENode<NT,ET,?>>(allNodes.size());
		this.firstLevelAvailable = new FastArrayList<Pair<ENode<NT,ET,?>,ET>>(allNodes.size()*allReferences.size()/2);
		
		this.secondLevel = new FastArrayList<ENode<NT,ET,?>>(allNodes.size());
		this.secondLevelAvailable = new FastArrayList<Pair<ENode<NT,ET,?>,ET>>(allNodes.size()*allReferences.size()/2);
		
		this.thirdLevel = new FastArrayList<ENode<NT,ET,?>>(allNodes.size());
		
		refsOfTypes = new HashMap<NT,List<ET>>();
		degreeMax = new HashMap<NT,int[]>();
		degreeMin = new HashMap<NT,int[]>();
		totalLowerBound = new HashMap<NT,Integer>();
		totalUpperBound = new HashMap<NT,Integer>();
		
		this.typeCase = new HashMap<NT, Boolean>();
		
//		this.currentAvailable = new FastArrayList<ENode<NT,ET,?>>();
		
//		this.degreeTable = new HashMap<ENode<?,?,?>, DegreeTable>();
		
		this.partition();
		
	}
	
	/**
	 * get all references that are connected with nodeType
	 * @param nodeType
	 * @return
	 */
	public List<ET> getRelatedRefs(NT nodeType) {
		if(nodeType==null) // no restriction 
			return this.allReferences;
		List<ET> result = refsOfTypes.get(nodeType);
		if(result!=null) return result;
		result = new HashableArrayList<ET>();
		refsOfTypes.put(nodeType,result);
		
		for(ET r : this.allReferences) {
			if(this.typeModel.isSuperNodeType(nodeType, getReferenceClass(r)))
				result.add(r);
		}
		
		return result;
	}
	
	public abstract NT getReferenceClass(ET e); // get the type referred to
	public abstract int getLowerBound(ET e); // get lower bound defined for e
	public abstract int getUpperBound(ET e); // get upper bound defined for e
	
	public abstract boolean isRequiredInConstraint();
	public abstract boolean isUniqueInConstraint();
	
	
	
	protected void partition() {
		this.allNodes.forEach((n)->{
			int lb = getTotalLowerBound(n);
			int ub = getTotalUpperBound(n);
			NT type = n.getType();
			
			List<ET> refs = this.getRelatedRefs(type);
			DegreeTable degree = this.generationService.getDegreeTable(refs, n);
			
			boolean special = this.getClassCase(n);
			
			if(degree.sum>=ub) { // old condition lb==ub && lb==0
				this.thirdLevel.add(n);
			} else if(degree.sum>=lb) {
				this.secondLevel.add(n);
				this.makeAvailable(n,refs,degree,this.getUpperBoundForType(type),this.secondLevelAvailable,false);
			} else {
				this.firstLevel.add(n);
				this.makeAvailable(n,refs,degree,this.getLowerBoundForType(type),this.firstLevelAvailable,special);
			}
		});
	}
	
	protected void makeAvailable(ENode<NT, ET, ?> node, List<ET> refs, DegreeTable degree, int[] bound,
			FastArrayList<Pair<ENode<NT, ET, ?>, ET>> available, boolean enforce) {
		for(int i=0;i<refs.size();i++) {
			if(degree.table[i]<bound[i] || (enforce && degree.table[i]==0 && bound[i] == 0)) {
				available.add(new Pair<ENode<NT,ET,?>,ET>(node,refs.get(i)));
			}
		}
	}


	/**
	 * get the total lower bound for node
	 * @param node
	 * @return
	 */
	protected int getTotalLowerBound(ENode<NT,ET,?> node) {
		// in the future, we may use node-specific lower bound
		NT type = node.getType();
		
		Integer tlb = this.totalLowerBound.get(type);
		if(tlb==null) {
			List<ET> refs = this.getRelatedRefs(type);
			if(refs.size()==0)
				tlb = 0;
			else {
				int[] lb = this.getLowerBoundForType(type);
				int sum = 0;
				for(int l : lb) sum += l;
				tlb = sum;
				
				boolean required = this.isRequiredInConstraint();
				boolean unique = this.isUniqueInConstraint();
				
				if(sum==0) {
					if(required) {
						tlb = 1;
						this.typeCase.put(type, true);
					}
				} else {
					if(unique) {
						tlb = 1;
						this.typeCase.put(type, true);
					}
				}
			}
			this.totalLowerBound.put(type, tlb);
		}
		return tlb;
	}
	
	/**
	 * get the total upper bound for node
	 * @param node
	 * @return
	 */
	protected int getTotalUpperBound(ENode<NT,ET,?> node) {
		// in the future, we may use node-specific upper bound
		NT type = node.getType();
		Integer tub = this.totalUpperBound.get(type);
		if(tub==null) {
		List<ET> refs = this.getRelatedRefs(type);
		if(refs.size()==0)
			tub = 0;
		else {
			int[] ub = this.getUpperBoundForType(type);
			int sum = 0;
			for(int u : ub) {
				int oldSum = sum;
				sum += u;
				if(sum<oldSum) { //overflow
					sum = Integer.MAX_VALUE;
					break;
				}
			}
			boolean unique = this.isUniqueInConstraint();
			if(unique) {
				tub = 1;
			} else
				tub = sum;
			}
		this.totalUpperBound.put(type, tub);
		}
		return tub;
	}
	
	/**
	 * when it returns true, total lower bound must be 1
	 * @param o
	 * @return
	 */
	protected boolean getClassCase(ENode<NT,ET,?> o) {
		Boolean b = typeCase.get(o.getType());
		return b!=null && b==true;
	}
	
	/**
	 * get the lower bounds of each reference for t
	 * @param t
	 * @return
	 */
	public int[] getLowerBoundForType(NT t) {
		int[] bound = this.degreeMin.get(t);
		
		if(bound!=null) return bound;
		
		List<ET> refs = this.getRelatedRefs(t);
		
		bound = new int[refs.size()];
		
		for(int i = 0; i<refs.size(); i++) {
			bound[i] = this.getLowerBound(refs.get(i));
		}
		
		this.degreeMin.put(t, bound);
		
		return bound;
	}
	
	/**
	 * get the upper bounds of each reference for t
	 * @param t
	 * @return
	 */
	public int[] getUpperBoundForType(NT t) {
		int[] bound = this.degreeMax.get(t);
		
		if(bound!=null) return bound;
		
		List<ET> refs = this.getRelatedRefs(t);
		
		bound = new int[refs.size()];
		
		for(int i = 0; i<refs.size(); i++) {
			bound[i] = this.getUpperBound(refs.get(i));
		}
		
		this.degreeMax.put(t, bound);
		
		return bound;
	}
	
	/**
	 * It means a ref-relationship is created for o
	 * @param o
	 * @param ref
	 */
	public void consume(ENode<NT,ET,?> o, ET ref) {
		int[] lb = getLowerBoundForType(o.getType());
		int[] ub = getUpperBoundForType(o.getType());
		int tlb = getTotalLowerBound(o);
		int tub = getTotalUpperBound(o);
		List<ET> refs = getRelatedRefs(o.getType());
		DegreeTable d = this.generationService.getDegreeTable(refs, o);

		Pair<ENode<NT,ET,?>, ET> p = new Pair<ENode<NT,ET,?>, ET>(o,ref);

		int id = refs.indexOf(ref);
		
		if(id>=0) {
			d.increase(id);
			
			if(d.sum==tlb) {
				this.firstLevel.remove(o);
				clearAvailable(o,firstLevelAvailable);
				
				if(d.sum<tub){
					this.secondLevel.add(o);
					makeAvailable(o,refs,d,ub,secondLevelAvailable,false);
				}
				else 
					this.thirdLevel.add(o);
			}
			
			if(d.sum==tub) {
				this.secondLevel.remove(o);
				clearAvailable(o,secondLevelAvailable);
				this.thirdLevel.add(o);
			}
			
			if(d.sum<tlb) { //
				// to check whether we can simplify this condition
//				if((!special && d.table[id]>=lb[id]) || (special && d.table[id]>lb[id])) //?
				if(d.table[id]>=lb[id])
					this.firstLevelAvailable.remove(p);
			} else if(d.sum<tub){
				if(d.table[id]>=ub[id])
					secondLevelAvailable.remove(p);
			}
			
			
		} else 
			assert false;
		return;
	}

	private void clearAvailable(ENode<NT, ET, ?> o, List<Pair<ENode<NT, ET, ?>, ET>> available) {
		if(this.allReferences.size()<200) {
			for(ET r : allReferences) {
				available.remove(new Pair<ENode<NT, ET, ?>,ET>(o,r));
			}
		} else {
			int i =available.size()-1;
			for(;i>=0;i--) {
				Pair<ENode<NT, ET, ?>, ET> p = available.get(i);
				if(p.getFirst()==o)
					available.remove(i);
			}		
		}
	}
	
	public void consume(ENode<NT, ET, ?> o, ET oldRef, ET newRef) {
		List<ET> refs = this.getRelatedRefs(o.getType());
		DegreeTable d = this.generationService.getDegreeTable(refs, o);
		
		int[] lb = this.getLowerBoundForType(o.getType());
		int[] ub = this.getUpperBoundForType(o.getType());
		
		int tlb = this.getTotalLowerBound(o);
		int tub = this.getTotalUpperBound(o);
		
		boolean special = this.getClassCase(o);
		
		int oid = refs.indexOf(oldRef);
		int nid = refs.indexOf(newRef);
		
		d.adjust(oid, nid);
		
		if(d.sum<tlb) {
			if(!special && d.table[nid]>=lb[nid])
				firstLevelAvailable.remove(new Pair<ENode<NT,ET,?>,ET>(o,newRef));
			if(d.table[oid]<lb[oid] || (special && d.table[oid]==0 && lb[oid]==0))
				firstLevelAvailable.add(new Pair<ENode<NT,ET,?>,ET>(o,oldRef));	
		} 
		else if(d.sum<tub) {
			if(d.table[nid]>=ub[nid])
				secondLevelAvailable.remove(new Pair<ENode<NT,ET,?>,ET>(o,newRef));
			if(d.table[oid]<ub[oid])
				secondLevelAvailable.add(new Pair<ENode<NT,ET,?>,ET>(o,oldRef));	
		}
	}
	
	public void release(ENode<NT,ET,?> o, ET ref) {
		List<ET> refs = this.getRelatedRefs(o.getType());
		DegreeTable d = this.generationService.getDegreeTable(refs, o);
		
		int[] lb = this.getLowerBoundForType(o.getType());
		int[] ub = this.getUpperBoundForType(o.getType());
		
		int tlb = this.getTotalLowerBound(o);
		int tub = this.getTotalUpperBound(o);
		
		boolean special = this.getClassCase(o);
		
		int id = refs.indexOf(ref);
		
		if(id>=0) {
			d.decrease(id);
			
			if(d.sum==tlb-1) {//drop from the second
				this.secondLevel.remove(o);
				clearAvailable(o,secondLevelAvailable);
				this.firstLevel.add(o);
				makeAvailable(o,refs,d,ub,firstLevelAvailable,special);
			} else if(d.sum<tlb) {
				if(d.table[id]<lb[id] || (special && d.table[id]==0 && lb[id]==0))
					firstLevelAvailable.add(new Pair<ENode<NT,ET,?>,ET>(o,ref));	
			} 
			
			if(d.sum==tub-1) {//drop from the third? not impossible
				this.thirdLevel.remove(o);
				this.secondLevel.add(o);
				makeAvailable(o,refs,d,ub,secondLevelAvailable,false);
			} else if(d.sum<tub) {
				if(d.table[id]<ub[id])
					secondLevelAvailable.add(new Pair<ENode<NT,ET,?>,ET>(o,ref));	
			}
		}
	}
	
	public enum Scope {
		first,
		second,
		third,
		firstAndSecond,
		all
	}
	
	public Pair<ENode<NT,ET,?>, ET> pickObjectAndReference(Scope scope) {
		RandomService random = this.generationService.getRandomService();
		
		if((scope==Scope.first || scope==Scope.firstAndSecond || scope==Scope.all) 
				&& firstLevel.size()!=0 &&firstLevelAvailable.size()!=0) {
			int id = random.randomInt(firstLevelAvailable.size());
			return firstLevelAvailable.get(id);
		} 
		
		if((scope == Scope.second || scope==Scope.firstAndSecond || scope==Scope.all)
				&& secondLevel.size()!=0 && secondLevelAvailable.size()!=0) {
			int id = random.randomInt(secondLevelAvailable.size());
			return secondLevelAvailable.get(id);
		} 
	
		if(scope==Scope.third || scope==Scope.all) { 
			ENode<NT,ET,?> o = thirdLevel.get(random.randomInt(thirdLevel.size()));
			List<ET> refs = getRelatedRefs(o.getType());
			ET r = refs.get(random.randomInt(refs.size()));
			return new Pair<ENode<NT,ET,?>, ET>(o,r);
		} else {
			return null;
		}
	}

	public List<ENode<NT,ET,?>> pickObjectWithConditions(ENode<NT,ET,?> src, ET ref,
			ReachabilityService<NT, ET> reachability, int size) {
		{
			List<ENode<NT,ET,?>> availableObjs = null;
			
			
			if(firstLevel.size()!=0){
				firstLevel.doShuffle(size);
				availableObjs = collectFirstAvailable(src,ref, reachability,size);
				return availableObjs;
			} 
			
			if(secondLevel.size()!=0) {
				secondLevel.doShuffle(size);
				availableObjs = collectSecondAvailable(src,ref, reachability,size);
				
				return availableObjs;
			}
		}
		
		
		return null;
	}
	
	protected abstract Iterator<ENode<NT,ET,?>> checkForbiddenConditionForPickObject(ENode<NT,ET,?> anchor, 
			NT typeFilter, List<ENode<NT,ET,?>> candidate, ReachabilityService<NT, ET> reachability);

	
	private List<ENode<NT,ET,?>> collectFirstAvailable(ENode<NT,ET,?> opposite, ET ref,
			ReachabilityService<NT, ET> forbiddenMap, int size) {
		ArrayList<ENode<NT,ET,?>> availableObjs = new ArrayList<ENode<NT,ET,?>>(size);
		Iterator<ENode<NT,ET,?>> it = checkForbiddenConditionForPickObject(opposite, this.getReferenceClass(ref), firstLevel, forbiddenMap);
		
		while(it.hasNext()) {
			if(size==availableObjs.size()) return availableObjs;
			ENode<NT,ET,?> o = it.next();
			
			boolean special = getClassCase(o);
			
			NT type = o.getType();
			List<ET> refs = getRelatedRefs(type);
			int id = refs.indexOf(ref);
			int[] lb = this.getLowerBoundForType(type);
			DegreeTable d = this.generationService.getDegreeTable(refs, o);
			
			if(!special) {
				if(d.table[id]<lb[id])
					availableObjs.add(o);
			} else {
				if(d.table[id]==0)
					availableObjs.add(o);
			}
		}
		return availableObjs;
	}
	

	private List<ENode<NT,ET,?>> collectSecondAvailable(ENode<NT,ET,?> opposite, ET ref,
			ReachabilityService<NT, ET> reachability, int size) {
		ArrayList<ENode<NT,ET,?>> availableObjs = new ArrayList<ENode<NT,ET,?>>(size);
		
		Iterator<ENode<NT,ET,?>> it = checkForbiddenConditionForPickObject(opposite,getReferenceClass(ref), secondLevel, reachability);
		
		while(it.hasNext()) {
			if(size==availableObjs.size()) return availableObjs;
			
			ENode<NT,ET,?> o = it.next();
			 
				{
					{
						NT type = o.getType();
						List<ET> refs = getRelatedRefs(type);
						int id = refs.indexOf(ref);
						int[] ub = getUpperBoundForType(type);
						DegreeTable d = this.generationService.getDegreeTable(refs, o);
						
						if(d.table[id]<ub[id])
							availableObjs.add(o);
					}
				}
		}
		
		return availableObjs;
	}

	public List<ET> getAllReferences() {
		return allReferences;
	}
	
	public ENode<NT,ET,?> pick() {
		int size = 0;
		if((size = firstLevel.size())!=0){
			int id = this.generationService.getRandomService().randomInt(size);
			return firstLevel.get(id);
		} else if((size=secondLevel.size())!=0) {
			int id = this.generationService.getRandomService().randomInt(size);
			return secondLevel.get(id);
		}
		
		return null;
	}
	
	public void putUnhandledObj(ENode<NT,ET,?> o, ET r, Scope scope) {
		if(scope==Scope.first){
			firstLevelAvailable.remove(new Pair<ENode<NT,ET,?>, ET>(o,r));
		} else {
			secondLevelAvailable.remove(new Pair<ENode<NT,ET,?>, ET>(o,r));
		}
	}
	
	public Iterator<ENode<NT,ET,?>> getAvailableObjects(Scope scope) {
		return getAvailableObjects(null,scope);
	}
	
	public Iterator<ENode<NT,ET,?>> getAvailableObjects(NT type, Scope scope) {
		ConjunctionIterator<ENode<NT,ET,?>> iter = null;
		
		switch(scope) {
		case first:
			iter = new ConjunctionIterator<ENode<NT,ET,?>>(firstLevel);
			break;
		case second:
			iter = new ConjunctionIterator<ENode<NT,ET,?>>(secondLevel);
			break;
		case firstAndSecond:
			iter = new ConjunctionIterator<ENode<NT,ET,?>>(firstLevel,secondLevel);
			break;
		case all:
		default:
			iter = new ConjunctionIterator<ENode<NT,ET,?>>(firstLevel,secondLevel,thirdLevel);
			break;
		}
		
		if(type==null) {
			return iter;
		} else {			
			return new TypeFilterIterator(iter,type,this.typeModel);
		}
	}
	
	class TypeFilterIterator implements Iterator<ENode<NT,ET,?>> {
		TypeFilterIterator(Iterator<ENode<NT,ET,?>> iter, NT type,ETypeModel<NT,ET,?,?> typeModel) {
			this.iterator = iter;
			this.type = type;
			this.curNode = null;
			this.typeModel = typeModel;
		}
		private Iterator<ENode<NT,ET,?>> iterator;
		private NT type;
		private ENode<NT,ET,?> curNode;
		private ETypeModel<NT,ET,?,?> typeModel;
		@Override
		public boolean hasNext() {
			if(curNode==null) {
				while(curNode==null) {
					if(iterator.hasNext()==false) 
						return false;				
					curNode = iterator.next();
					if(this.typeModel.isSuperNodeType(curNode.getType(), type)) {
						break;
					}
					curNode = null;
				}
			}

			return true;
		}

		@Override
		public ENode<NT, ET, ?> next() {
			ENode<NT, ET, ?> ret = curNode;
			curNode = null;
			return ret;
		}
		
	}
	
	class ConjunctionIterator<T> implements Iterator<T> {
		private List<List<T>> lists;
		private Iterator<List<T>> listIterator;
		private Iterator<T> elemIterator;
		
		@SafeVarargs
		ConjunctionIterator(List<T>... lists) {
			this.lists = Arrays.asList(lists);
			this.listIterator = this.lists.iterator();
			this.elemIterator = null;
		}

		@Override
		public boolean hasNext() {
			if(this.elemIterator==null || this.elemIterator.hasNext()==false) {
				if(listIterator.hasNext()) {
					this.elemIterator = listIterator.next().iterator();
					return hasNext();
				} else
					return false;
			}
			return true;
		}

		@Override
		public T next() {
			return this.elemIterator.next();
		}
		
	}
	
	abstract public boolean addEdge(ENode<NT,ET,?> sn,ENode<NT,ET,?> tn,ET et);
	
	abstract public boolean isFull(ENode<NT,ET,?> node, ET et);
	
	public boolean isFull(ENode<NT,ET,?> node) {
		DegreeTable table = this.generationService.getDegreeTable(this.getRelatedRefs(node.getType()), node);
		return table.sum <= this.getTotalUpperBound(node);
	}
	
	abstract public boolean isEqualToOrLowerThanNeeded(ENode<NT,ET,?> node, ET et);
}