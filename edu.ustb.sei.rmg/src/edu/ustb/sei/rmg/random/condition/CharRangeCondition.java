package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class CharRangeCondition extends RangeCondition<Character> {

	@Override
	protected Character randomLU(RandomUtil r) {
		return r.randomCharLU(lower, upper);
	}

	@Override
	protected Character randomL(RandomUtil r) {
		return null;
	}

	@Override
	protected Character randomU(RandomUtil r) {
		return null;
	}

}
