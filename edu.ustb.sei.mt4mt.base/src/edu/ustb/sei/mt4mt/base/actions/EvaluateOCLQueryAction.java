package edu.ustb.sei.mt4mt.base.actions;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class EvaluateOCLQueryAction implements IAction {
	public EvaluateOCLQueryAction(String query, String modelKey, String resultKey, String[] im) {
		super();
		this.query = query;
		this.modelKey = modelKey;
		this.resultKey = resultKey;
		this.imports = im;
	}

	private String modelKey;
	private String query;
	private String resultKey;
	private String[] imports;
	
	@Override
	public void run(Context context) {
		
	    
	    Object model = context.getData(modelKey);
	    
	    EObject firstRoot;
	    
	    if(model==null) {
	    	firstRoot=null;
	    	org.junit.Assert.fail("an empty model will fail any invariant");
	    } else {
	    	if(model instanceof Resource && ((Resource) model).getContents().size()>0) {
	    		firstRoot = ((Resource) model).getContents().get(0);
	    	} else if(model instanceof List && ((List)model).size()>0) {
	    		firstRoot = (EObject) ((List) model).get(0);
	    	} else if(model instanceof EObject) {
	    		firstRoot = (EObject) model;
	    	} else {
	    		firstRoot = null;
	    		org.junit.Assert.fail("an empty model will fail any invariant");
	    	}
	    }
	    
	    OCL<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, Constraint, EClass, EObject> ocl;
	    ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
	    ocl.setExtentMap(null);
	    
	    OCLHelper<EClassifier, ?, ?, Constraint> helper = ocl.createOCLHelper();

	    helper.setContext(firstRoot.eClass());
	    
	    OCLExpression<EClassifier> q = null;
	    
	    String completeQuery = "";
	    
	    if(imports!=null) {
	    	boolean first = true;
	    	
	    	for(String im : imports) {
	    		Object value = context.getData(im);
	    		String type = "Any";
	    		if(value==null)
	    			value = "undefined";
	    		else if(value instanceof String) {
	    			type = "String";
	    			value = "'"+value+"'";
	    		} else if(value instanceof Integer)
	    			type = "Integer";
	    		else if(value instanceof Boolean)
	    			type = "Boolean";
	    		else if(value instanceof EObject)
	    			type = ((EObject) value).eClass().getName();
	    		
	    		if(first) {
	    			completeQuery = completeQuery +" let "+im+" : "+type+" = "+value;
	    			first = false;
	    		} else 
	    			completeQuery = completeQuery +", "+im+" : "+type+" = "+value;
	    	}
	    	
	    	if(imports.length!=0)
	    		completeQuery+=" in ";
	    }
	    
	    completeQuery = completeQuery+query;
	    
	    try {
			q = helper.createQuery(completeQuery);
		} catch (ParserException e) {
			throw new RuntimeException(e);
		}
	    
	    Object result = ocl.evaluate(firstRoot, q);
	    
	    context.putData(resultKey, result);
	}

}
