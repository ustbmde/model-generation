/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.TestCaseGroup#getTestCases <em>Test Cases</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCaseGroup()
 * @model
 * @generated
 */
public interface TestCaseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Test Cases</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.TestCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Cases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Cases</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCaseGroup_TestCases()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCase> getTestCases();

} // TestCaseGroup
