package edu.ustb.sei.rmg;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.random.RandomUtil;

public class TarCandidateIterator implements Iterator<EObject> {
	private EObject src;
	private List<EObject> tar;
	private EClass typeFilter;
	private ForbiddenMap forbiddenMap;
	private boolean start = false;
	
	public TarCandidateIterator(EObject src,List<EObject> tar, 
			EClass typeFilter, ForbiddenMap forbiddenMap, RandomUtil random) {
		super();
		this.src = src;
		this.tar = tar;
		this.typeFilter = typeFilter;
		this.forbiddenMap = forbiddenMap;
		startPos = random.randomUIntU(tar.size());
		curPos = startPos;
	}

	private int startPos;
	private int curPos;

	private boolean checkCondition() {
		EObject obj = tar.get(curPos);
		if(typeFilter.isInstance(obj)==false) return false;
		if(forbiddenMap.isForbidden(src, obj)) return false;
		return true;
	}
	
	@Override
	public boolean hasNext() {
		if(start && curPos==startPos) return false;
		while(checkCondition()==false) {
			moveCur();
			if(curPos==startPos) return false;//visited all
		}
		return true;
	}

	protected void moveCur() {
		curPos++;
		if(curPos>=tar.size()) curPos = 0;//visited end
	}

	@Override
	public EObject next() {
		EObject obj = tar.get(curPos);
		moveCur();
		start = true;
		return obj;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
