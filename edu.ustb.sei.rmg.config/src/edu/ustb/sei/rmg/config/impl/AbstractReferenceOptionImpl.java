/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.ConfigPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Reference Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isSourceUnique <em>Source Unique</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isSourceRequired <em>Source Required</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isTargetUnique <em>Target Unique</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isTargetRequired <em>Target Required</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isTransitivity <em>Transitivity</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isRing <em>Ring</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#isCircle <em>Circle</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl#getExternalObjects <em>External Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractReferenceOptionImpl extends ScalableConditionImpl implements AbstractReferenceOption {
	/**
	 * The default value of the '{@link #isSourceUnique() <em>Source Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSourceUnique()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SOURCE_UNIQUE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSourceUnique() <em>Source Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSourceUnique()
	 * @generated
	 * @ordered
	 */
	protected boolean sourceUnique = SOURCE_UNIQUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isSourceRequired() <em>Source Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSourceRequired()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SOURCE_REQUIRED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSourceRequired() <em>Source Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSourceRequired()
	 * @generated
	 * @ordered
	 */
	protected boolean sourceRequired = SOURCE_REQUIRED_EDEFAULT;

	/**
	 * The default value of the '{@link #isTargetUnique() <em>Target Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTargetUnique()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TARGET_UNIQUE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTargetUnique() <em>Target Unique</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTargetUnique()
	 * @generated
	 * @ordered
	 */
	protected boolean targetUnique = TARGET_UNIQUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isTargetRequired() <em>Target Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTargetRequired()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TARGET_REQUIRED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTargetRequired() <em>Target Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTargetRequired()
	 * @generated
	 * @ordered
	 */
	protected boolean targetRequired = TARGET_REQUIRED_EDEFAULT;

	/**
	 * The default value of the '{@link #isTransitivity() <em>Transitivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransitivity()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TRANSITIVITY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTransitivity() <em>Transitivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransitivity()
	 * @generated
	 * @ordered
	 */
	protected boolean transitivity = TRANSITIVITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isRing() <em>Ring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRing()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RING_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isRing() <em>Ring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRing()
	 * @generated
	 * @ordered
	 */
	protected boolean ring = RING_EDEFAULT;

	/**
	 * The default value of the '{@link #isCircle() <em>Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCircle()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CIRCLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isCircle() <em>Circle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCircle()
	 * @generated
	 * @ordered
	 */
	protected boolean circle = CIRCLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExternalObjects() <em>External Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> externalObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractReferenceOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSourceUnique() {
		return sourceUnique;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceUnique(boolean newSourceUnique) {
		boolean oldSourceUnique = sourceUnique;
		sourceUnique = newSourceUnique;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE, oldSourceUnique, sourceUnique));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSourceRequired() {
		return sourceRequired;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceRequired(boolean newSourceRequired) {
		boolean oldSourceRequired = sourceRequired;
		sourceRequired = newSourceRequired;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED, oldSourceRequired, sourceRequired));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTargetUnique() {
		return targetUnique;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetUnique(boolean newTargetUnique) {
		boolean oldTargetUnique = targetUnique;
		targetUnique = newTargetUnique;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE, oldTargetUnique, targetUnique));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTargetRequired() {
		return targetRequired;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetRequired(boolean newTargetRequired) {
		boolean oldTargetRequired = targetRequired;
		targetRequired = newTargetRequired;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED, oldTargetRequired, targetRequired));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransitivity() {
		return transitivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitivity(boolean newTransitivity) {
		boolean oldTransitivity = transitivity;
		transitivity = newTransitivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY, oldTransitivity, transitivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRing() {
		return ring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRing(boolean newRing) {
		boolean oldRing = ring;
		ring = newRing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING, oldRing, ring));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCircle() {
		return circle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCircle(boolean newCircle) {
		boolean oldCircle = circle;
		circle = newCircle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE, oldCircle, circle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getExternalObjects() {
		if (externalObjects == null) {
			externalObjects = new EObjectResolvingEList<EObject>(EObject.class, this, ConfigPackage.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS);
		}
		return externalObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE:
				return isSourceUnique();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED:
				return isSourceRequired();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE:
				return isTargetUnique();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED:
				return isTargetRequired();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY:
				return isTransitivity();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING:
				return isRing();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE:
				return isCircle();
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS:
				return getExternalObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE:
				setSourceUnique((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED:
				setSourceRequired((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE:
				setTargetUnique((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED:
				setTargetRequired((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY:
				setTransitivity((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING:
				setRing((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE:
				setCircle((Boolean)newValue);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS:
				getExternalObjects().clear();
				getExternalObjects().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE:
				setSourceUnique(SOURCE_UNIQUE_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED:
				setSourceRequired(SOURCE_REQUIRED_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE:
				setTargetUnique(TARGET_UNIQUE_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED:
				setTargetRequired(TARGET_REQUIRED_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY:
				setTransitivity(TRANSITIVITY_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING:
				setRing(RING_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE:
				setCircle(CIRCLE_EDEFAULT);
				return;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS:
				getExternalObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE:
				return sourceUnique != SOURCE_UNIQUE_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED:
				return sourceRequired != SOURCE_REQUIRED_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE:
				return targetUnique != TARGET_UNIQUE_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED:
				return targetRequired != TARGET_REQUIRED_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY:
				return transitivity != TRANSITIVITY_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING:
				return ring != RING_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE:
				return circle != CIRCLE_EDEFAULT;
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS:
				return externalObjects != null && !externalObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourceUnique: ");
		result.append(sourceUnique);
		result.append(", sourceRequired: ");
		result.append(sourceRequired);
		result.append(", targetUnique: ");
		result.append(targetUnique);
		result.append(", targetRequired: ");
		result.append(targetRequired);
		result.append(", transitivity: ");
		result.append(transitivity);
		result.append(", ring: ");
		result.append(ring);
		result.append(", circle: ");
		result.append(circle);
		result.append(')');
		return result.toString();
	}

} //AbstractReferenceOptionImpl
