package edu.ustb.sei.egraph.generator.graph.config;

import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class AttributeConfig {
	public AttributeConfig(QualifiedName name, AbstractRandomFunction<?> function) {
		super();
		this.name = name;
		this.function = function;
	}
	private QualifiedName name;
	private AbstractRandomFunction<?> function;
	public QualifiedName getName() {
		return name;
	}
	public AbstractRandomFunction<?> getFunction() {
		return function;
	}

}
