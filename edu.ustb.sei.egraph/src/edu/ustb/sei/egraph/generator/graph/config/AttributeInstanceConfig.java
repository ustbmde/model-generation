package edu.ustb.sei.egraph.generator.graph.config;

import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class AttributeInstanceConfig {
	public AttributeInstanceConfig(String name, AbstractRandomFunction<?> function) {
		super();
		this.name = name;
		this.function = function;
	}
	private String name;
	private AbstractRandomFunction<?> function;
	public String getName() {
		return name;
	}
	public AbstractRandomFunction<?> getFunction() {
		return function;
	}

	
}
