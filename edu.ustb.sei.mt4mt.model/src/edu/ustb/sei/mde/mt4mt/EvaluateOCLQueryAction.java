/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluate OCL Query Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getModelKey <em>Model Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getQuery <em>Query</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getResultKey <em>Result Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getImportContextVariables <em>Import Context Variables</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getEvaluateOCLQueryAction()
 * @model
 * @generated
 */
public interface EvaluateOCLQueryAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Key</em>' attribute.
	 * @see #setModelKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getEvaluateOCLQueryAction_ModelKey()
	 * @model
	 * @generated
	 */
	String getModelKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getModelKey <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Key</em>' attribute.
	 * @see #getModelKey()
	 * @generated
	 */
	void setModelKey(String value);

	/**
	 * Returns the value of the '<em><b>Query</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query</em>' attribute.
	 * @see #setQuery(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getEvaluateOCLQueryAction_Query()
	 * @model
	 * @generated
	 */
	String getQuery();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getQuery <em>Query</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Query</em>' attribute.
	 * @see #getQuery()
	 * @generated
	 */
	void setQuery(String value);

	/**
	 * Returns the value of the '<em><b>Result Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Key</em>' attribute.
	 * @see #setResultKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getEvaluateOCLQueryAction_ResultKey()
	 * @model
	 * @generated
	 */
	String getResultKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction#getResultKey <em>Result Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Key</em>' attribute.
	 * @see #getResultKey()
	 * @generated
	 */
	void setResultKey(String value);

	/**
	 * Returns the value of the '<em><b>Import Context Variables</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Context Variables</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Context Variables</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getEvaluateOCLQueryAction_ImportContextVariables()
	 * @model
	 * @generated
	 */
	EList<String> getImportContextVariables();

} // EvaluateOCLQueryAction
