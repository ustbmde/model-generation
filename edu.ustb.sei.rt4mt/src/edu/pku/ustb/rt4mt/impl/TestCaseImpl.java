/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.ExpectedOutput;
import edu.pku.ustb.rt4mt.Rt4mtPackage;
import edu.pku.ustb.rt4mt.TestCase;

import edu.pku.ustb.rt4mt.TestData;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestCaseImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestCaseImpl#getInputs <em>Inputs</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestCaseImpl#getExpectedOutputs <em>Expected Outputs</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TestCaseImpl extends EObjectImpl implements TestCase {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInputs() <em>Inputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<TestData> inputs;

	/**
	 * The cached value of the '{@link #getExpectedOutputs() <em>Expected Outputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedOutputs()
	 * @generated
	 * @ordered
	 */
	protected EList<ExpectedOutput> expectedOutputs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Rt4mtPackage.Literals.TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.TEST_CASE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestData> getInputs() {
		if (inputs == null) {
			inputs = new EObjectContainmentEList<TestData>(TestData.class, this, Rt4mtPackage.TEST_CASE__INPUTS);
		}
		return inputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExpectedOutput> getExpectedOutputs() {
		if (expectedOutputs == null) {
			expectedOutputs = new EObjectContainmentEList<ExpectedOutput>(ExpectedOutput.class, this, Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS);
		}
		return expectedOutputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Rt4mtPackage.TEST_CASE__INPUTS:
				return ((InternalEList<?>)getInputs()).basicRemove(otherEnd, msgs);
			case Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS:
				return ((InternalEList<?>)getExpectedOutputs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Rt4mtPackage.TEST_CASE__ID:
				return getId();
			case Rt4mtPackage.TEST_CASE__INPUTS:
				return getInputs();
			case Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS:
				return getExpectedOutputs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Rt4mtPackage.TEST_CASE__ID:
				setId((Integer)newValue);
				return;
			case Rt4mtPackage.TEST_CASE__INPUTS:
				getInputs().clear();
				getInputs().addAll((Collection<? extends TestData>)newValue);
				return;
			case Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS:
				getExpectedOutputs().clear();
				getExpectedOutputs().addAll((Collection<? extends ExpectedOutput>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_CASE__ID:
				setId(ID_EDEFAULT);
				return;
			case Rt4mtPackage.TEST_CASE__INPUTS:
				getInputs().clear();
				return;
			case Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS:
				getExpectedOutputs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_CASE__ID:
				return id != ID_EDEFAULT;
			case Rt4mtPackage.TEST_CASE__INPUTS:
				return inputs != null && !inputs.isEmpty();
			case Rt4mtPackage.TEST_CASE__EXPECTED_OUTPUTS:
				return expectedOutputs != null && !expectedOutputs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //TestCaseImpl
