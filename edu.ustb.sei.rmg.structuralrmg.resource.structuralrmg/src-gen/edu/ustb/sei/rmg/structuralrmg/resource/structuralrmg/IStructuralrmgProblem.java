/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

public interface IStructuralrmgProblem {
	public String getMessage();
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity getSeverity();
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType getType();
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> getQuickFixes();
}
