package edu.ustb.sei.mt4mt.base.test;

import static edu.ustb.sei.mt4mt.base.Actions.assertComparison;
import static edu.ustb.sei.mt4mt.base.Actions.assertOCL;
import static edu.ustb.sei.mt4mt.base.Actions.compare;
import static edu.ustb.sei.mt4mt.base.Actions.copy;
import static edu.ustb.sei.mt4mt.base.Actions.load;
import static edu.ustb.sei.mt4mt.base.Actions.sequence;

public class Test extends AbstractTransformationTestCase{
	
	public Test() {
		super(sequence(
				copy("/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/model.xmi","/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/model_copy.xmi"),
				compare("/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/model.xmi","/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/model_copy.xmi","cr"),
				assertComparison("cr", 0),
				load("/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/model.xmi","model"),
				assertOCL("TypeB::allInstances()->forAll(b1,b2|b1<>b2 implies b1.name<>b2.name)", "model",null)
				));
		
		this.setMetamodels("/Volumes/Macintosh HD Data/Eclipse Projects/RMG/edu.ustb.sei.mt4mt.base/test/My.ecore");
	}
}
