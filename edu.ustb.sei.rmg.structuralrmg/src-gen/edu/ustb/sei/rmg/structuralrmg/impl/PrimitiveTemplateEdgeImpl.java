/**
 */
package edu.ustb.sei.rmg.structuralrmg.impl;

import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Primitive Template Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl#getSourcePos <em>Source Pos</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.PrimitiveTemplateEdgeImpl#getTargetPos <em>Target Pos</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrimitiveTemplateEdgeImpl extends TemplateEdgeImpl implements PrimitiveTemplateEdge {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected TemplateNode source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected TemplateNode target;

	/**
	 * The default value of the '{@link #getSourcePos() <em>Source Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePos()
	 * @generated
	 * @ordered
	 */
	protected static final Integer SOURCE_POS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourcePos() <em>Source Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePos()
	 * @generated
	 * @ordered
	 */
	protected Integer sourcePos = SOURCE_POS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetPos() <em>Target Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPos()
	 * @generated
	 * @ordered
	 */
	protected static final Integer TARGET_POS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetPos() <em>Target Pos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPos()
	 * @generated
	 * @ordered
	 */
	protected Integer targetPos = TARGET_POS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrimitiveTemplateEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructuralRmgPackage.Literals.PRIMITIVE_TEMPLATE_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateNode getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (TemplateNode)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateNode basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(TemplateNode newSource) {
		TemplateNode oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateNode getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (TemplateNode)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateNode basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(TemplateNode newTarget) {
		TemplateNode oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getSourcePos() {
		return sourcePos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourcePos(Integer newSourcePos) {
		Integer oldSourcePos = sourcePos;
		sourcePos = newSourcePos;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS, oldSourcePos, sourcePos));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getTargetPos() {
		return targetPos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetPos(Integer newTargetPos) {
		Integer oldTargetPos = targetPos;
		targetPos = newTargetPos;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS, oldTargetPos, targetPos));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS:
				return getSourcePos();
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS:
				return getTargetPos();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE:
				setSource((TemplateNode)newValue);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET:
				setTarget((TemplateNode)newValue);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS:
				setSourcePos((Integer)newValue);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS:
				setTargetPos((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE:
				setSource((TemplateNode)null);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET:
				setTarget((TemplateNode)null);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS:
				setSourcePos(SOURCE_POS_EDEFAULT);
				return;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS:
				setTargetPos(TARGET_POS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE:
				return source != null;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET:
				return target != null;
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS:
				return SOURCE_POS_EDEFAULT == null ? sourcePos != null : !SOURCE_POS_EDEFAULT.equals(sourcePos);
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS:
				return TARGET_POS_EDEFAULT == null ? targetPos != null : !TARGET_POS_EDEFAULT.equals(targetPos);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sourcePos: ");
		result.append(sourcePos);
		result.append(", targetPos: ");
		result.append(targetPos);
		result.append(')');
		return result.toString();
	}

} //PrimitiveTemplateEdgeImpl
