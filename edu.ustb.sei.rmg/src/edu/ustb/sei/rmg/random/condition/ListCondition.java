package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class ListCondition extends Condition<Object[]> {
	public Condition<?> element;
	public Condition<Integer> size;

	@Override
	public Object[] getValue(RandomUtil r, Object... extra){
		int s = size.getValue(r);
		
		Object[] result = new Object[s];
		
		for(int i=0;i<s;i++) {
			try {
				result[i] = element.getValue(r, extra);
			} catch (ValueLazyGenerationRequest | FeatureConditionError e) {
				result[i] = "_ERROR_";
			}
		}
			
		
		return result;
	}

}
