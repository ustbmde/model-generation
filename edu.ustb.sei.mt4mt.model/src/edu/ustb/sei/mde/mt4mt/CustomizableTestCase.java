/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customizable Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCustomizableTestCase()
 * @model
 * @generated
 */
public interface CustomizableTestCase extends TestCase {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(TestAction)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCustomizableTestCase_Action()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TestAction getAction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(TestAction value);

} // CustomizableTestCase
