package edu.ustb.sei.mt4mt.base.updates;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

abstract public class CreateObjectUpdate implements IUpdate {

	protected CreateObjectUpdate(String hostClass, boolean typeOf, double possibility, String feature, String targetClass) {
		super();
		this.hostClass = hostClass;
		this.typeOf = typeOf;
		this.possibility = possibility;
		this.feature = feature;
		this.targetClass = targetClass;
	}
	
	protected boolean typeOf;
	protected String hostClass;
	protected double possibility;
	protected String feature;
	protected String targetClass;

	@Override
	public boolean apply(EObject o, EClass oClass, EPackage oPackage, RandomUtil util, Set<EObject> createdObjects) {
		if(checkTypeValidity(oClass) && checkHostObjectValidity(o,createdObjects)) {
			EStructuralFeature cf = IUpdate.searchFeature(oClass, feature);
			EClass ecls = IUpdate.searchClassAtPackage(oPackage, targetClass);
			boolean modified = false;
			while(shouldCreate(o, util)) {
				EObject e = EcoreUtil.create(ecls);
				if(cf.isMany()) {
					List<EObject> list = (List<EObject>) o.eGet(cf);
					list.add(e);
				} else {
					e.eSet(cf, e);
				}
				initObject(e,ecls, oPackage, util, createdObjects);
				createdObjects.add(e);
				modified = true;
			}
			return modified;
		}
		return false;
	}

	protected boolean checkHostObjectValidity(EObject o, Set<EObject> createdObjects) {
		return true;
	}

	abstract protected void initObject(EObject o, EClass oClass, EPackage oPackage, RandomUtil util, Set<EObject> createdObjects);

	protected boolean shouldCreate(EObject o, RandomUtil util) {
		return util.decision(possibility);
	}

	protected boolean checkTypeValidity(EClass oClass) {
		return this.typeOf ? IUpdate.isTypeOf(oClass, hostClass):  IUpdate.isKindOf(oClass, this.hostClass);
	}

}
