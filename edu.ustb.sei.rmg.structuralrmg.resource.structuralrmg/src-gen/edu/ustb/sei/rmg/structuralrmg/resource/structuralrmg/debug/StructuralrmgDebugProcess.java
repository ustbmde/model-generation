/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug;

public class StructuralrmgDebugProcess extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugElement implements org.eclipse.debug.core.model.IProcess, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.IStructuralrmgDebugEventListener {
	
	private org.eclipse.debug.core.ILaunch launch;
	
	private boolean terminated = false;
	
	public StructuralrmgDebugProcess(org.eclipse.debug.core.ILaunch launch) {
		super(launch.getDebugTarget());
		this.launch = launch;
	}
	
	public boolean canTerminate() {
		return !terminated;
	}
	
	public boolean isTerminated() {
		return terminated;
	}
	
	public void terminate() throws org.eclipse.debug.core.DebugException {
		terminated = true;
	}
	
	public String getLabel() {
		return null;
	}
	
	public org.eclipse.debug.core.ILaunch getLaunch() {
		return launch;
	}
	
	public org.eclipse.debug.core.model.IStreamsProxy getStreamsProxy() {
		return null;
	}
	
	public void setAttribute(String key, String value) {
	}
	
	public String getAttribute(String key) {
		return null;
	}
	
	public int getExitValue() throws org.eclipse.debug.core.DebugException {
		return 0;
	}
	
	public void handleMessage(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugMessage message) {
		if (message.hasType(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes.TERMINATED)) {
			terminated = true;
		} else {
			// ignore other events
		}
	}
	
}
