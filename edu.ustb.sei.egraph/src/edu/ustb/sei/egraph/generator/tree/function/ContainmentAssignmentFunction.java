package edu.ustb.sei.egraph.generator.tree.function;

public class ContainmentAssignmentFunction<NT, ET, NAT, EAT> extends AssignmentFunction<NT, ET, NAT, EAT> {

	public ContainmentAssignmentFunction(String feature,NodeCreationFunction<NT, ET, NAT, EAT> nodeCreation) {
		this(feature,1,1,nodeCreation);
	}

	public ContainmentAssignmentFunction(String feature, int lower,NodeCreationFunction<NT, ET, NAT, EAT> nodeCreation) {
		this(feature, lower, lower, nodeCreation);
	}

	public ContainmentAssignmentFunction(String feature, int lower, int upper,NodeCreationFunction<NT, ET, NAT, EAT> nodeCreation) {
		super(feature, lower, upper);
		this.nodeCreation = nodeCreation;
	}

	@Override
	public Integer apply(TreeGenerationService<NT, ET, NAT, EAT> service) {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected NodeCreationFunction<NT, ET, NAT, EAT> nodeCreation;

}
