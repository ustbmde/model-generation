package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomRealValueInRangeFunction extends RandomNumValueInRangeFunction<Double> {
	
	public RandomRealValueInRangeFunction(Double min, Double max) {
		super(min,max);
	}
	
	@Override
	public Double getValue(RandomService t) {
		return t.randomDouble(min, max);
	}
	

}
