package edu.ustb.sei.rmg.config.provider;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import edu.ustb.sei.rmg.config.AttributeOption;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.ReferenceOption;

public class FeatureItemPropertyDescriptor extends ItemPropertyDescriptor {

	public FeatureItemPropertyDescriptor(AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature, boolean isSettable, boolean multiLine,
			boolean sortChoices, Object staticImage, String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description, feature, isSettable, multiLine, sortChoices,
				staticImage, category, filterFlags);
		// TODO Auto-generated constructor stub
		this.itemDelegator = new ItemDelegator(adapterFactory, resourceLocator) {

			@Override
			public String getText(Object object) {
				// TODO Auto-generated method stub
				String str = super.getText(object);
				
				if(object instanceof EStructuralFeature) {
					EStructuralFeature f = (EStructuralFeature)object;
					str = str + "\t\t--" + ((EClass)f.eContainer()).getName();
				}
				
				return str;
			}
			
		};
	}
	
	@Override
	protected Collection<?> getComboBoxObjects(Object object) {
		// TODO Auto-generated method stub
		if(object instanceof AttributeOption) {
			AttributeOption ao = (AttributeOption)object;
			if(ao.eContainer() instanceof ClassOption) {
				ClassOption co = (ClassOption) ao.eContainer();
				if(co.getClassRef()==null) return Collections.EMPTY_LIST;
				else {
					return co.getClassRef().getEAllAttributes();
				}
			}
		}
		
		
		return super.getComboBoxObjects(object);
	}

}
