model SF

import <platform:/plugin/edu.ustb.sei.mde.tuml/model/tUML.ecore>
template WModel {
	role (ACTIVITY)
	
	node[1] n : Model;
	node[1] a : Activity plays ACTIVITY;
	
	edge e1 : Model::activities(n,a);
}

template Flow {
	node[1] i : InitialNode;
	node[1] f : ActivityFinalNode;
	node[1] a @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] c1 : ControlFlow;
	node[1] c2 : ControlFlow;
	
	node[1] wm @ WModel;
	
	edge e1 : ActivityEdge::source(c1,i);
	edge e2 : ActivityEdge::target(c1,a);
	edge e3 : ActivityEdge::source(c2,a);
	edge e4 : ActivityEdge::target(c2,f);
	
	edge e5 : ActivityGroup::nodes(wm,i);
	edge e6 : ActivityGroup::nodes(wm,f);
	edge e7 : ActivityGroup::edges(wm,c1);
	edge e8 : ActivityGroup::edges(wm,c2);

	binding e2 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e3 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	
	binding e5 for source WModel=>ACTIVITY
	binding e6 for source WModel=>ACTIVITY
	binding e7 for source WModel=>ACTIVITY
	binding e8 for source WModel=>ACTIVITY
}

template SimpleAction {
	role (ACT)
	node[1] a : CallAction [actionName : name] plays ACT;
	node[1] shared wm @ WModel;
	
	edge e1 : ActivityGroup::nodes(wm,a);
	binding e1 for source WModel=>ACTIVITY
}

template Sequence {
	role (IN, OUT)
	node[1] a1 @ SimpleAction,Sequence,Choice,Parallel,Loop plays IN;
	node[1] a2 @ SimpleAction,Sequence,Choice,Parallel,Loop plays OUT;
	node[1] c : ControlFlow;
	node[1] shared wm @ WModel;
	
	edge e1 : ActivityEdge::source(c,a1);
	edge e2 : ActivityEdge::target(c,a2);
	
	edge e5 : ActivityGroup::edges(wm,c);
	
	binding e1 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	binding e2 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	
	binding e5 for source WModel=>ACTIVITY
}

template Choice {
	role (IN, OUT)
	node[1] d : DecisionNode plays IN;
	node[1] m : MergeNode plays OUT;
	node[1] b1 @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] b2 @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] c1 : ControlFlow;
	node[1] c2 : ControlFlow;
	node[1] c3 : ControlFlow;
	node[1] c4 : ControlFlow;
	node[1] shared wm @ WModel;
	
	edge e1 : ActivityEdge::source(c1,d);
	edge e2 : ActivityEdge::target(c1,b1);
	edge e3 : ActivityEdge::source(c2,d);
	edge e4 : ActivityEdge::target(c2,b2);
	edge e5 : ActivityEdge::source(c3,b1);
	edge e6 : ActivityEdge::target(c3,m);
	edge e7 : ActivityEdge::source(c4,b2);
	edge e8 : ActivityEdge::target(c4,m);
	
	edge e9 : ActivityGroup::nodes(wm,d);
	edge e10 : ActivityGroup::nodes(wm,m);
	edge e11 : ActivityGroup::edges(wm,c1);
	edge e12 : ActivityGroup::edges(wm,c2);
	edge e13 : ActivityGroup::edges(wm,c3);
	edge e14 : ActivityGroup::edges(wm,c4);
	
	binding e2 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e4 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e5 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	binding e7 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	
	binding e9 for source WModel=>ACTIVITY
	binding e10 for source WModel=>ACTIVITY
	binding e11 for source WModel=>ACTIVITY
	binding e12 for source WModel=>ACTIVITY
	binding e13 for source WModel=>ACTIVITY
	binding e14 for source WModel=>ACTIVITY
}

template Parallel {
	role (IN, OUT)
	node[1] d : ForkNode plays IN;
	node[1] m : JoinNode plays OUT;
	node[1] b1 @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] b2 @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] c1 : ControlFlow;
	node[1] c2 : ControlFlow;
	node[1] c3 : ControlFlow;
	node[1] c4 : ControlFlow;
	node[1] shared wm @ WModel;
	
	edge e1 : ActivityEdge::source(c1,d);
	edge e2 : ActivityEdge::target(c1,b1);
	edge e3 : ActivityEdge::source(c2,d);
	edge e4 : ActivityEdge::target(c2,b2);
	edge e5 : ActivityEdge::source(c3,b1);
	edge e6 : ActivityEdge::target(c3,m);
	edge e7 : ActivityEdge::source(c4,b2);
	edge e8 : ActivityEdge::target(c4,m);
	
	edge e9 : ActivityGroup::nodes(wm,d);
	edge e10 : ActivityGroup::nodes(wm,m);
	edge e11 : ActivityGroup::edges(wm,c1);
	edge e12 : ActivityGroup::edges(wm,c2);
	edge e13 : ActivityGroup::edges(wm,c3);
	edge e14 : ActivityGroup::edges(wm,c4);
	
	binding e2 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e4 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e5 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	binding e7 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	
	binding e9 for source WModel=>ACTIVITY
	binding e10 for source WModel=>ACTIVITY
	binding e11 for source WModel=>ACTIVITY
	binding e12 for source WModel=>ACTIVITY
	binding e13 for source WModel=>ACTIVITY
	binding e14 for source WModel=>ACTIVITY
}

template Loop {
	role (IN,OUT)
	node[1] m : MergeNode plays IN;
	node[1] d : DecisionNode plays OUT;
	node[1] a @ SimpleAction,Sequence,Choice,Parallel,Loop;
	node[1] shared wm @ WModel;
	
	node[1] c1 : ControlFlow;
	node[1] c2 : ControlFlow;
	node[1] c3 : ControlFlow;
	
	edge e1 : ActivityEdge::source(c1,m);
	edge e2 : ActivityEdge::target(c1,d);
	edge e3 : ActivityEdge::source(c2,d);
	edge e4 : ActivityEdge::target(c2,a);
	edge e5 : ActivityEdge::source(c3,a);
	edge e6 : ActivityEdge::target(c3,m);
	
	edge e7 : ActivityGroup::nodes(wm,m);
	edge e8 : ActivityGroup::nodes(wm,d);
	edge e9 : ActivityGroup::edges(wm,c1);
	edge e10 : ActivityGroup::edges(wm,c2);
	edge e11 : ActivityGroup::edges(wm,c3);
	
	binding e4 for target SimpleAction=>ACT Sequence=>IN Choice=>IN Parallel=>IN Loop=>IN
	binding e5 for target SimpleAction=>ACT Sequence=>OUT Choice=>OUT Parallel=>OUT Loop=>OUT
	
	binding e7 for source WModel=>ACTIVITY
	binding e8 for source WModel=>ACTIVITY
	binding e9 for source WModel=>ACTIVITY
	binding e10 for source WModel=>ACTIVITY
	binding e11 for source WModel=>ACTIVITY
}