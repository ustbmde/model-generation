/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.Condition#getBoundCondition <em>Bound Condition</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCondition()
 * @model abstract="true"
 * @generated
 */
public interface Condition extends EObject {
	/**
	 * Returns the value of the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound Condition</em>' attribute.
	 * @see #setBoundCondition(String)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCondition_BoundCondition()
	 * @model
	 * @generated
	 */
	String getBoundCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.Condition#getBoundCondition <em>Bound Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound Condition</em>' attribute.
	 * @see #getBoundCondition()
	 * @generated
	 */
	void setBoundCondition(String value);

} // Condition
