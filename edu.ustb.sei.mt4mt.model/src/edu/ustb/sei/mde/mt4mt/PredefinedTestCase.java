/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Predefined Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedTestCase#getInputs <em>Inputs</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedTestCase()
 * @model abstract="true"
 * @generated
 */
public interface PredefinedTestCase extends TestCase {
	/**
	 * Returns the value of the '<em><b>Inputs</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedTestCase_Inputs()
	 * @model
	 * @generated
	 */
	EList<String> getInputs();

} // PredefinedTestCase
