package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult;

public class ResolverUtil {

	static public boolean matchName(String identifier, String name) {
		if(identifier.equals(name)) return true;
		else if(identifier.startsWith("_") && identifier.substring(1).equals(name))
			return true;
		return false;
	}

	static public Template upToTemplate(EObject m) {
		if(m==null) return null;
		if(m instanceof Template) return (Template)m;
		else return upToTemplate(m.eContainer());
	}

	static public EClass searchClass(String string,EList<EPackage> list) {
		for(EPackage p : list) {
			TreeIterator<EObject> it = p.eAllContents();
			while(it.hasNext()) {
				EObject o = it.next();
				if(o instanceof EClass) {
					if(matchName(string, ((EClass) o).getName())) {
						return (EClass)o;
					}
				}
			}
		}
		return null;
	}

	static public TemplateModel getModel(EObject o) {
		if(o==null) return null;
		if(o instanceof TemplateModel) return (TemplateModel)o;
		else return getModel(o.eContainer());
	}

	static public Role searchRole(
			String identifier,
			Template template) {
		for(Role r : template.getAllRoles()) {
			String name = r.getName();
			if(matchName(identifier, name)) {
				return r;
			}
		}
		return null;
	}

	static public Template searchTemplate(
			String identifier,
			TemplateModel t) {
		for(Template tt : t.getTemplates()) {
			if(matchName(identifier, tt.getName())) {
				return tt;
			}
		}
		return null;
	}

}
