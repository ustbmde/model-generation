package edu.ustb.sei.rmg.random.condition;

import java.util.Calendar;

import edu.ustb.sei.rmg.random.RandomUtil;

public class DateRangeCondition extends RangeCondition<Calendar> {

	@Override
	protected Calendar randomLU(RandomUtil r) {
		long diff = upper.getTimeInMillis() - lower.getTimeInMillis();
		long add = r.randomLong(0, diff);
		int addDay = (int)(add/1000/60/60/24);
		Calendar c = (Calendar) lower.clone();
		c.add(Calendar.DAY_OF_MONTH, addDay);
		return c;
	}

	@Override
	protected Calendar randomL(RandomUtil r) {
		long add = r.randomLongL(0);
		int addDay = (int)(add/1000/60/60/24);
		Calendar c = (Calendar) lower.clone();
		c.add(Calendar.DAY_OF_MONTH, addDay);
		return c;
	}

	@Override
	protected Calendar randomU(RandomUtil r) {
		long add = r.randomLongL(0);
		int addDay = -(int)(add/1000/60/60/24);
		Calendar c = (Calendar) upper.clone();
		c.add(Calendar.DAY_OF_MONTH, addDay);
		return c;
	}


}
