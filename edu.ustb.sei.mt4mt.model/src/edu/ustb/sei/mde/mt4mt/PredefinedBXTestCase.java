/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Predefined BX Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdLauncher <em>Fwd Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdSource <em>Fwd Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdView <em>Fwd View</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdLauncher <em>Bwd Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdSource <em>Bwd Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdView <em>Bwd View</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdUpdatedSource <em>Bwd Updated Source</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase()
 * @model abstract="true"
 * @generated
 */
public interface PredefinedBXTestCase extends PredefinedTestCase {
	/**
	 * Returns the value of the '<em><b>Fwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fwd Launcher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fwd Launcher</em>' attribute.
	 * @see #setFwdLauncher(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_FwdLauncher()
	 * @model
	 * @generated
	 */
	String getFwdLauncher();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getFwdLauncher <em>Fwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fwd Launcher</em>' attribute.
	 * @see #getFwdLauncher()
	 * @generated
	 */
	void setFwdLauncher(String value);

	/**
	 * Returns the value of the '<em><b>Fwd Source</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fwd Source</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fwd Source</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_FwdSource()
	 * @model
	 * @generated
	 */
	EList<String> getFwdSource();

	/**
	 * Returns the value of the '<em><b>Fwd View</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fwd View</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fwd View</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_FwdView()
	 * @model
	 * @generated
	 */
	EList<String> getFwdView();

	/**
	 * Returns the value of the '<em><b>Bwd Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bwd Launcher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bwd Launcher</em>' attribute.
	 * @see #setBwdLauncher(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_BwdLauncher()
	 * @model
	 * @generated
	 */
	String getBwdLauncher();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase#getBwdLauncher <em>Bwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bwd Launcher</em>' attribute.
	 * @see #getBwdLauncher()
	 * @generated
	 */
	void setBwdLauncher(String value);

	/**
	 * Returns the value of the '<em><b>Bwd Source</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bwd Source</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bwd Source</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_BwdSource()
	 * @model
	 * @generated
	 */
	EList<String> getBwdSource();

	/**
	 * Returns the value of the '<em><b>Bwd View</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bwd View</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bwd View</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_BwdView()
	 * @model
	 * @generated
	 */
	EList<String> getBwdView();

	/**
	 * Returns the value of the '<em><b>Bwd Updated Source</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bwd Updated Source</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bwd Updated Source</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPredefinedBXTestCase_BwdUpdatedSource()
	 * @model
	 * @generated
	 */
	EList<String> getBwdUpdatedSource();

} // PredefinedBXTestCase
