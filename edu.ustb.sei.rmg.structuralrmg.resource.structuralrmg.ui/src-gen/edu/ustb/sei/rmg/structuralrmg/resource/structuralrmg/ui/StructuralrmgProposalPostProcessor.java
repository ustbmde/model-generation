/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class StructuralrmgProposalPostProcessor {
	
	public java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal> process(java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
