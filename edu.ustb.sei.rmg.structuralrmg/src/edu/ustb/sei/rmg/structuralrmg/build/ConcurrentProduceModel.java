package edu.ustb.sei.rmg.structuralrmg.build;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.function.Consumer;

import javax.management.RuntimeErrorException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.commonutil.util.BidirectionalMap;
import edu.ustb.sei.commonutil.util.PairHashMap;
import edu.ustb.sei.rmg.structuralrmg.AttributeBinding;
import edu.ustb.sei.rmg.structuralrmg.BindingMapping;
import edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.CompositionSort;
import edu.ustb.sei.rmg.structuralrmg.EndpointBinding;
import edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

public class ConcurrentProduceModel implements IUnfolder {
	final static int MAX_SIZE = 100;

	// static
	private BidirectionalMap<Object, Object> templateTypeMap = new BidirectionalMap<Object, Object>();
	private EPackage metamodel = null;
	private TemplateModel template = null;

	// dynamic
	ConcurrentHashMap<EObject, TemplateInstance> elementInstanceMap = new ConcurrentHashMap<EObject, TemplateInstance>();

	private List<EObject> allElements = new LinkedList<EObject>();
	
	private List<BuildEdgeTask> edgeTasks = new LinkedList<BuildEdgeTask>();
	
	private PairHashMap<EObject, EReference, List<Object>> duplicate = new PairHashMap<EObject, EReference, List<Object>>();
	
	public void register(EObject o, EReference r, Object v) {
		List<Object> list = getShadowList(o,r);
		if(list==null) {
			synchronized (duplicate) {
				list = getShadowList(o, r);
				if(list==null) {
					list = new ArrayList<Object>();
					duplicate.put(o, r, list);			
				}
			}
		}
		synchronized (list) {
			list.add(v);
		}
	}
	public List<Object> getShadowList(EObject o, EReference r) {
		return duplicate.get(o,r);
	}
	public void writeBackShadowList() {
		duplicate.getMap().entrySet().parallelStream().forEach(new Consumer<Entry<EObject, HashMap<EReference, List<Object>>>>() {
			@Override
			public void accept(Entry<EObject, HashMap<EReference, List<Object>>> t) {
				for(Entry<EReference, List<Object>> l : t.getValue().entrySet()) {
					t.getKey().eSet(l.getKey(), l.getValue());
				}
			}
		});
	}
	
	public void registerEdgeTask(BuildEdgeTask t) {
		synchronized (edgeTasks) {
			edgeTasks.add(t);
		}
	}

	public Object forward(Object f) {
		return templateTypeMap.forward(f);
	}

	public Object backward(Object b) {
		return templateTypeMap.backward(b);
	}

	protected EObject createObject(EClass cls) {
		EObject o = EcoreUtil.create(cls);
		synchronized (allElements) {
			allElements.add(o);
		}
		return o;
	}

	public static Random random = new Random();

	public void travelPackage(EPackage pkg) {
		EAnnotation pa = pkg.getEAnnotation("model");
		if (pa != null) {
			template = (TemplateModel) pa.getReferences().get(0);
		}

		TreeIterator<EObject> it = pkg.eAllContents();
		while (it.hasNext()) {
			EObject o = it.next();
			if (o instanceof EClass) {
				EClass cls = (EClass) o;
				EAnnotation ann = cls.getEAnnotation("template");
				if (ann != null) {
					templateTypeMap.put(ann.getReferences().get(0), cls);
					for (EStructuralFeature f : cls.getEStructuralFeatures()) {
						EAnnotation fann = f.getEAnnotation("nested");
						if (fann != null) {
							templateTypeMap.put(fann.getReferences().get(0), f);
						}
					}
				}
			}
		}
		metamodel = pkg;
	}

	public Resource buildResource(Resource root) {
		// no unique root: to be fixed
		long start = Calendar.getInstance().getTimeInMillis();
		List<BuildElementTask> tasks = new ArrayList<BuildElementTask>();
		
		for (EObject o : root.getContents()) {
			BuildElementTask task = new BuildElementTask(this, o);
			tasks.add(task);
		}
		
		RecursiveAction.invokeAll(tasks);
		RecursiveAction.invokeAll(this.edgeTasks);
		this.writeBackShadowList();
		long end = Calendar.getInstance().getTimeInMillis();
		
		long timeSpan = end-start;

		URI uri = root.getURI().trimFileExtension().appendFileExtension(template.getPackages().get(0).getName());
		Resource nres = root.getResourceSet().createResource(uri);
		int count = 0;
		for (EObject o : allElements) {
			if (o.eContainer() == null)
				nres.getContents().add(o);
			count++;
		}

		System.out.println("Total elements in the required model:" + count+"\t "+timeSpan+" ms was used excluding the final checking time");

		return nres;
	}

}

@SuppressWarnings("serial")
class BuildNestedElementTask extends RecursiveAction {
	private ConcurrentProduceModel host;
	private Object target;
	private TemplateInstance parentTempalateInstance;
	private TemplateNode parentNode;

	public BuildNestedElementTask(ConcurrentProduceModel host, Object target, TemplateInstance parentTempalateInstance,
			TemplateNode parentNode) {
		super();
		this.host = host;
		this.target = target;
		this.parentTempalateInstance = parentTempalateInstance;
		this.parentNode = parentNode;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void compute() {
		if (target instanceof List) {
			List<BuildElementTask> arr = new ArrayList<BuildElementTask>();

			for (EObject o : (List<EObject>) target) {
				BuildElementTask task = new BuildElementTask(this.host, o);
				arr.add(task);
			}

			List<TemplateInstance> res = new ArrayList<TemplateInstance>();
			for (BuildElementTask t : invokeAll(arr)) {
				res.add(t.join());
			}

			this.parentTempalateInstance.putNodeInstance(parentNode, res);

		} else {
			BuildElementTask task = new BuildElementTask(this.host, (EObject) this.target);
			TemplateInstance ti = task.invoke();
			this.parentTempalateInstance.putNodeInstance(this.parentNode, ti);
		}
	}

}

@SuppressWarnings("serial")
class BuildEdgeTask extends RecursiveAction {
	public BuildEdgeTask(ConcurrentProduceModel host, EObject target) {
		super();
		this.host = host;
		this.target = target;
	}

	private ConcurrentProduceModel host;
	private EObject target;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void compute() {
		TemplateInstance ti = host.elementInstanceMap.get(target);
		EClass cls = target.eClass();
		Template temp = (Template) host.backward(cls);
		if (temp == null)
			return;
		
		for (TemplateEdge e : temp.getAllEdges()) {
			EReference ref = e.getReference();

			if (e instanceof PrimitiveTemplateEdge) {
				TemplateNode sn = ((PrimitiveTemplateEdge) e).getSource();
				TemplateNode tn = ((PrimitiveTemplateEdge) e).getTarget();
				Object so = ti.getNodeInstance(sn);
				Integer sp = ((PrimitiveTemplateEdge) e).getSourcePos();
				Object to = ti.getNodeInstance(tn);
				Integer tp = ((PrimitiveTemplateEdge) e).getTargetPos();

				if (so == null || to == null)
					continue;

				if (so instanceof List) {
					List<Object> sarr = (List<Object>) so;
					if (sp == null) {
						for (Object s : sarr) {
							createEdgeFromSource(temp, e, tn, ref, s, to, tp);
						}
					} else {
						if (sarr.size() == 0)
							continue;
						if (sp == -1 || sp >= sarr.size())
							sp = sarr.size() - 1;
						Object s = sarr.get(sp);
						createEdgeFromSource(temp, e, tn, ref, s, to, tp);
					}

				} else {
					Object s = so;
					createEdgeFromSource(temp, e, tn, ref, s, to, tp);
				}
			} else if (e instanceof ChainTemplateEdge) {
				TemplateNode n = ((ChainTemplateEdge) e).getNode();
				Object node = ti.getNodeInstance(n);

				if (node == null)
					continue;

				if (node instanceof List) {
					List<Object> nodeArr = (List<Object>) node;
					Object first = null, prev = null, cur = null;

					if (nodeArr.size() != 0) {
						for (int i = 0; i <= nodeArr.size(); i++) {
							if (i == 0) {
								first = nodeArr.get(0);
								prev = first;
							} else {
								if (i == nodeArr.size()) {
									if (((ChainTemplateEdge) e).isRound() == false)
										break;
									cur = first;
								} else
									cur = nodeArr.get(i);
								createEdgeFromSource(temp, e, n, ref, prev, cur, null);
								prev = cur;
							}
						}
					}
				}

			}

		}
	}

	@SuppressWarnings("unchecked")
	private void basicCreateEdge(Object s, EReference ref, Object t) {
		synchronized (s) {
			if (ref.getEType().isInstance(t)) {
				if (ref.isMany()) {
					if(ref.isUnique()) {
						host.register((EObject)s, ref, t);
					} else
						((List<Object>) ((EObject) s).eGet(ref)).add(t);
				} else {
					((EObject) s).eSet(ref, t);
				}
			}
		}
	}

	protected void createEdge(Template temp, TemplateEdge e, TemplateNode tn, EReference ref, Object s, Object to,
			Integer tp) {
	
		if (((EClass) ref.eContainer()).isInstance(s)) {
			if (to instanceof List) {
				@SuppressWarnings("unchecked")
				List<Object> tarr = (List<Object>) to;
				if (tp == null) {
					for (Object t : tarr) {
						if (t instanceof EObject) {
							basicCreateEdge(s, ref, t);
						} else if (t instanceof TemplateInstance) {
	
							List<Object> ol = ((TemplateInstance) t).getObject(temp, e, false);
							for (Object o : ol) {
								basicCreateEdge(s, ref, o);
							}
						}
					}
				} else {
					if (tarr == null || tarr.size() == 0)
						return;
					if (tp == -1 || tp >= tarr.size())
						tp = tarr.size() - 1;
					Object t = tarr.get(tp);
					if (t instanceof EObject) {
						basicCreateEdge(s, ref, t);
					} else if (t instanceof TemplateInstance) {
	
						List<Object> ol = ((TemplateInstance) t).getObject(temp, e, false);
						for (Object o : ol) {
							basicCreateEdge(s, ref, o);
						}
					}
				}
			} else {
				Object t = to;
				if (t instanceof EObject) {
					basicCreateEdge(s, ref, t);
				} else if (t instanceof TemplateInstance) {
					List<Object> ol = ((TemplateInstance) t).getObject(temp, e, false);
					for (Object o : ol) {
						basicCreateEdge(s, ref, o);
					}
				}
			}
	
		}
	}

	private void createEdgeFromSource(Template temp, TemplateEdge e, TemplateNode tn, EReference ref, Object s,
			Object to, Integer tp) {
		if (s instanceof EObject) {
			createEdge(temp, e, tn, ref, s, to, tp);
		} else if (s instanceof TemplateInstance) {
			List<Object> ssl = ((TemplateInstance) s).getObject(temp, e, true);
			for (Object ss : ssl) {
				createEdge(temp, e, tn, ref, ss, to, tp);
			}
		}
	}

	
}

@SuppressWarnings("serial")
class BuildElementTask extends RecursiveTask<TemplateInstance> {

	private ConcurrentProduceModel host;
	private EObject target;

	public BuildElementTask(ConcurrentProduceModel h, EObject t) {
		this.host = h;
		this.target = t;
	}

	@Override
	protected TemplateInstance compute() {
		TemplateInstance ti = buildElement(target);
		return ti;
	}

	private TemplateInstance buildElement(EObject target) {
		TemplateInstance ti = host.elementInstanceMap.get(target);
		if (ti != null)
			return ti;

		synchronized (target) {
			// try second time
			ti = host.elementInstanceMap.get(target);
			if (ti != null)
				return ti;

			// in this case, this thread is the first thread to create ti

			EClass cls = target.eClass();

			Template temp = (Template) host.backward(cls);

			if (temp == null)
				return null;

			ti = new TemplateInstance(target, temp, true);
			host.elementInstanceMap.put(target, ti);
			
			List<BuildNestedElementTask> tasks = new ArrayList<BuildNestedElementTask>();

			for (TemplateNode n : temp.getAllNodes()) {
				if (n instanceof PrimitiveTemplateNode) {
					if (n.isSet()) {
						ArrayList<EObject> arr = new ArrayList<EObject>();
						int max = n.getMax();
						if (max < 0)
							max = ConcurrentProduceModel.MAX_SIZE;
						int min = n.getMin();

						int size = min + ConcurrentProduceModel.random.nextInt(max);

						while (size >= 0) {
							EObject i = host.createObject(((PrimitiveTemplateNode) n).getType());
							arr.add(i);
							writeAttribute(target, cls, (PrimitiveTemplateNode) n, i);
							size--;
						}

						ti.putNodeInstance(n, arr);
					} else {
						EObject i = host.createObject(((PrimitiveTemplateNode) n).getType());

						ti.putNodeInstance(n, i);
						writeAttribute(target, cls, (PrimitiveTemplateNode) n, i);
					}

				} else {
					NestedTemplateNode ntn = (NestedTemplateNode) n;
					CompositionSort sort = ntn.getSort();
					if (sort == CompositionSort.REVERSED) {
						// taverse eContainer first;
						if (n.isSet()) {
							throw new RuntimeException("I don't know how to create a set of reversed nodes!");
						} else {
							EObject cur = target;

							while (cur != null) {
								EClass rCls = cur.eClass();
								Template rTemp = (Template) host.backward(rCls);
								if (ntn.getTemplates().contains(rTemp)) {
									BuildNestedElementTask task = new BuildNestedElementTask(this.host, cur, ti, n);
									tasks.add(task);
									break;
								}
								cur = cur.eContainer();
							}
						}

					} else {
						EReference ref = (EReference) host.forward(n);

						if (n.isSet()) {
							@SuppressWarnings("unchecked")
							List<EObject> cElems = (List<EObject>) target.eGet(ref);

							BuildNestedElementTask task = new BuildNestedElementTask(this.host, cElems, ti, n);
							tasks.add(task);

						} else {
							EObject c = (EObject) target.eGet(ref);
							if (c == null)
								continue;

							BuildNestedElementTask task = new BuildNestedElementTask(this.host, c, ti, n);
							tasks.add(task);
						}
					}
				}
			}
			invokeAll(tasks);
		}
		
		host.registerEdgeTask(new BuildEdgeTask(this.host, this.target));

		return ti;
	}

	protected void writeAttribute(EObject host, EClass hostClass, PrimitiveTemplateNode node, EObject guest) {
		// write through attribute
		for (EAttribute att : hostClass.getEAllAttributes()) {
			EAnnotation ann = att.getEAnnotation("attribute");
			if (ann == null)
				continue;
			AttributeBinding ab = (AttributeBinding) ann.getReferences().get(0);
			if (ab.eContainer() != node)
				continue;

			Object value = host.eGet(att);
			guest.eSet(ab.getAttribute(), value);
		}
	}

}
