package edu.ustb.sei.egraph.structure;

import edu.ustb.sei.egraph.generator.util.QualifiedName;

public class EEdge<NT,ET,AT> extends PropertyHolder<AT> {

	public EEdge(ET type, ENode<NT,ET,?> source, ENode<NT,ET,?> target) {
		super();
		this.source = source;
		this.target = target;
		this.type = type;
	}
	int internalID = -1;
	
	private EGraph<NT,ET,?,AT> graph;
	private ENode<NT,ET,?> source;
	private ENode<NT,ET,?> target;
	private ET type;
	
	public ENode<NT,ET,?> getSource() {
		return source;
	}
	public void setSource(ENode<NT,ET,?> source) {
		this.source = source;
	}
	public ENode<NT,ET,?> getTarget() {
		return target;
	}
	public void setTarget(ENode<NT,ET,?> target) {
		this.target = target;
	}
	public ET getType() {
		return type;
	}
	public void setType(ET type) {
		this.type = type;
	}
	public EGraph<NT, ET, ?, AT> getGraph() {
		return graph;
	}
	public void setGraph(EGraph<NT, ET, ?, AT> graph) {
		this.graph = graph;
	}
	
	public void setInternalID(int internalID) {
		this.internalID = internalID;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.internalID);
		builder.append(", ");
		try{
			builder.append(source.internalID);
		}catch(Exception e) {
			builder.append("-1");
		}
		builder.append("-(");
		try {
			QualifiedName typeName = this.graph.getTypeModel().desolveEdgeType(type);
			builder.append(typeName);
		} catch(Exception e) {
			builder.append(type.toString());
		}
		builder.append(")->");
		try{
			builder.append(target.internalID);
		}catch(Exception e) {
			builder.append("-1");
		}

		return builder.toString();
	}
}
