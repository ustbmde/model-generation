package edu.ustb.sei.egraph.generator.graph.config;

import edu.ustb.sei.egraph.generator.util.QualifiedName;

public class EdgeInstanceConfig {

	public EdgeInstanceConfig(QualifiedName type, String source, int sourceId, String target, int targetId) {
		super();
		this.type = type;
		this.source = source;
		this.sourceId = sourceId;
		this.target = target;
		this.targetId = targetId;
	}
	public EdgeInstanceConfig(QualifiedName type, String source, String target) {
		this(type,source,-1,target,-1);
	}
	private String source;
	private int sourceId;
	private String target;
	private int targetId;
	private QualifiedName type;
	
	public String getSource() {
		return source;
	}
	public String getTarget() {
		return target;
	}
	public QualifiedName getType() {
		return type;
	}
	public int getSourceId() {
		return sourceId;
	}
	public int getTargetId() {
		return targetId;
	}
	
	
}
