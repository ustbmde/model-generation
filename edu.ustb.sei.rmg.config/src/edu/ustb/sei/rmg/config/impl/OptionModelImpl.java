/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.AttributeOption;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.GlobalOption;
import edu.ustb.sei.rmg.config.OptionModel;
import edu.ustb.sei.rmg.config.ReferenceFilterOption;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Option Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getMetamodelURI <em>Metamodel URI</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getOutputFileURI <em>Output File URI</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getRootClass <em>Root Class</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#isAutoNumber <em>Auto Number</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#isUniqueRoot <em>Unique Root</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#isEnableConcurrency <em>Enable Concurrency</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getGlobalOptions <em>Global Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getClsOptions <em>Cls Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getAttrOptions <em>Attr Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getRefOptions <em>Ref Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getFilterOptions <em>Filter Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#isBoundSolver <em>Bound Solver</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl#getMetamodel <em>Metamodel</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OptionModelImpl extends EObjectImpl implements OptionModel {
	/**
	 * The default value of the '{@link #getMetamodelURI() <em>Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String METAMODEL_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOutputFileURI() <em>Output File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_FILE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputFileURI() <em>Output File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputFileURI()
	 * @generated
	 * @ordered
	 */
	protected String outputFileURI = OUTPUT_FILE_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRootClass() <em>Root Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootClass()
	 * @generated
	 * @ordered
	 */
	protected EClass rootClass;

	/**
	 * The default value of the '{@link #isAutoNumber() <em>Auto Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoNumber()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_NUMBER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAutoNumber() <em>Auto Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoNumber()
	 * @generated
	 * @ordered
	 */
	protected boolean autoNumber = AUTO_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #isUniqueRoot() <em>Unique Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueRoot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UNIQUE_ROOT_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isUniqueRoot() <em>Unique Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueRoot()
	 * @generated
	 * @ordered
	 */
	protected boolean uniqueRoot = UNIQUE_ROOT_EDEFAULT;

	/**
	 * The default value of the '{@link #isEnableConcurrency() <em>Enable Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnableConcurrency()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENABLE_CONCURRENCY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEnableConcurrency() <em>Enable Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnableConcurrency()
	 * @generated
	 * @ordered
	 */
	protected boolean enableConcurrency = ENABLE_CONCURRENCY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGlobalOptions() <em>Global Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<GlobalOption> globalOptions;

	/**
	 * The cached value of the '{@link #getClsOptions() <em>Cls Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClsOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassOption> clsOptions;

	/**
	 * The cached value of the '{@link #getAttrOptions() <em>Attr Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttrOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeOption> attrOptions;

	/**
	 * The cached value of the '{@link #getRefOptions() <em>Ref Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractReferenceOption> refOptions;

	/**
	 * The cached value of the '{@link #getFilterOptions() <em>Filter Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceFilterOption> filterOptions;

	/**
	 * The default value of the '{@link #isBoundSolver() <em>Bound Solver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBoundSolver()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BOUND_SOLVER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBoundSolver() <em>Bound Solver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBoundSolver()
	 * @generated
	 * @ordered
	 */
	protected boolean boundSolver = BOUND_SOLVER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMetamodel() <em>Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodel()
	 * @generated
	 * @ordered
	 */
	protected EPackage metamodel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OptionModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.OPTION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getMetamodelURI() {
		EPackage pkg = this.getMetamodel();
		if(pkg==null) return "No Metamodel";
		else {
			Resource res = pkg.eResource();
			return res.getURI().toString();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutputFileURI() {
		return outputFileURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputFileURI(String newOutputFileURI) {
		String oldOutputFileURI = outputFileURI;
		outputFileURI = newOutputFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI, oldOutputFileURI, outputFileURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRootClass() {
		if (rootClass != null && rootClass.eIsProxy()) {
			InternalEObject oldRootClass = (InternalEObject)rootClass;
			rootClass = (EClass)eResolveProxy(oldRootClass);
			if (rootClass != oldRootClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.OPTION_MODEL__ROOT_CLASS, oldRootClass, rootClass));
			}
		}
		return rootClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetRootClass() {
		return rootClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootClass(EClass newRootClass) {
		EClass oldRootClass = rootClass;
		rootClass = newRootClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__ROOT_CLASS, oldRootClass, rootClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAutoNumber() {
		return autoNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAutoNumber(boolean newAutoNumber) {
		boolean oldAutoNumber = autoNumber;
		autoNumber = newAutoNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__AUTO_NUMBER, oldAutoNumber, autoNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUniqueRoot() {
		return uniqueRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniqueRoot(boolean newUniqueRoot) {
		boolean oldUniqueRoot = uniqueRoot;
		uniqueRoot = newUniqueRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__UNIQUE_ROOT, oldUniqueRoot, uniqueRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnableConcurrency() {
		return enableConcurrency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnableConcurrency(boolean newEnableConcurrency) {
		boolean oldEnableConcurrency = enableConcurrency;
		enableConcurrency = newEnableConcurrency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY, oldEnableConcurrency, enableConcurrency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GlobalOption> getGlobalOptions() {
		if (globalOptions == null) {
			globalOptions = new EObjectContainmentEList<GlobalOption>(GlobalOption.class, this, ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS);
		}
		return globalOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassOption> getClsOptions() {
		if (clsOptions == null) {
			clsOptions = new EObjectContainmentEList<ClassOption>(ClassOption.class, this, ConfigPackage.OPTION_MODEL__CLS_OPTIONS);
		}
		return clsOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeOption> getAttrOptions() {
		if (attrOptions == null) {
			attrOptions = new EObjectContainmentEList<AttributeOption>(AttributeOption.class, this, ConfigPackage.OPTION_MODEL__ATTR_OPTIONS);
		}
		return attrOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractReferenceOption> getRefOptions() {
		if (refOptions == null) {
			refOptions = new EObjectContainmentEList<AbstractReferenceOption>(AbstractReferenceOption.class, this, ConfigPackage.OPTION_MODEL__REF_OPTIONS);
		}
		return refOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceFilterOption> getFilterOptions() {
		if (filterOptions == null) {
			filterOptions = new EObjectContainmentEList<ReferenceFilterOption>(ReferenceFilterOption.class, this, ConfigPackage.OPTION_MODEL__FILTER_OPTIONS);
		}
		return filterOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBoundSolver() {
		return boundSolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBoundSolver(boolean newBoundSolver) {
		boolean oldBoundSolver = boundSolver;
		boundSolver = newBoundSolver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__BOUND_SOLVER, oldBoundSolver, boundSolver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EPackage getMetamodel() {
		if (metamodel != null && metamodel.eIsProxy()) {
			InternalEObject oldMetamodel = (InternalEObject)metamodel;
			metamodel = (EPackage)eResolveProxy(oldMetamodel);
			if (metamodel != oldMetamodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.OPTION_MODEL__METAMODEL, oldMetamodel, metamodel));
			}
		}
		
		if(metamodel==null) {
			try {
				List<Resource> lists = this.eResource().getResourceSet().getResources();
				for(int i = 1 ; i < lists.size() ; i ++) {
					Resource res = lists.get(i);
					EList<EObject> contents = res.getContents();
					if(contents==null || contents.size()==0) continue;
					EObject eObject = contents.get(0);
					if(eObject instanceof EPackage) {
						this.setMetamodel((EPackage) eObject);
						break;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return metamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetMetamodel() {
		return metamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetamodel(EPackage newMetamodel) {
		EPackage oldMetamodel = metamodel;
		metamodel = newMetamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.OPTION_MODEL__METAMODEL, oldMetamodel, metamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstrained(EReference ref) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
				return ((InternalEList<?>)getGlobalOptions()).basicRemove(otherEnd, msgs);
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
				return ((InternalEList<?>)getClsOptions()).basicRemove(otherEnd, msgs);
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
				return ((InternalEList<?>)getAttrOptions()).basicRemove(otherEnd, msgs);
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
				return ((InternalEList<?>)getRefOptions()).basicRemove(otherEnd, msgs);
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				return ((InternalEList<?>)getFilterOptions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.OPTION_MODEL__METAMODEL_URI:
				return getMetamodelURI();
			case ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI:
				return getOutputFileURI();
			case ConfigPackage.OPTION_MODEL__ROOT_CLASS:
				if (resolve) return getRootClass();
				return basicGetRootClass();
			case ConfigPackage.OPTION_MODEL__AUTO_NUMBER:
				return isAutoNumber();
			case ConfigPackage.OPTION_MODEL__UNIQUE_ROOT:
				return isUniqueRoot();
			case ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY:
				return isEnableConcurrency();
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
				return getGlobalOptions();
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
				return getClsOptions();
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
				return getAttrOptions();
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
				return getRefOptions();
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				return getFilterOptions();
			case ConfigPackage.OPTION_MODEL__BOUND_SOLVER:
				return isBoundSolver();
			case ConfigPackage.OPTION_MODEL__METAMODEL:
				if (resolve) return getMetamodel();
				return basicGetMetamodel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI:
				setOutputFileURI((String)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__ROOT_CLASS:
				setRootClass((EClass)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__AUTO_NUMBER:
				setAutoNumber((Boolean)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__UNIQUE_ROOT:
				setUniqueRoot((Boolean)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY:
				setEnableConcurrency((Boolean)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
				getGlobalOptions().clear();
				getGlobalOptions().addAll((Collection<? extends GlobalOption>)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
				getClsOptions().clear();
				getClsOptions().addAll((Collection<? extends ClassOption>)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
				getAttrOptions().clear();
				getAttrOptions().addAll((Collection<? extends AttributeOption>)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
				getRefOptions().clear();
				getRefOptions().addAll((Collection<? extends AbstractReferenceOption>)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				getFilterOptions().clear();
				getFilterOptions().addAll((Collection<? extends ReferenceFilterOption>)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__BOUND_SOLVER:
				setBoundSolver((Boolean)newValue);
				return;
			case ConfigPackage.OPTION_MODEL__METAMODEL:
				setMetamodel((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI:
				setOutputFileURI(OUTPUT_FILE_URI_EDEFAULT);
				return;
			case ConfigPackage.OPTION_MODEL__ROOT_CLASS:
				setRootClass((EClass)null);
				return;
			case ConfigPackage.OPTION_MODEL__AUTO_NUMBER:
				setAutoNumber(AUTO_NUMBER_EDEFAULT);
				return;
			case ConfigPackage.OPTION_MODEL__UNIQUE_ROOT:
				setUniqueRoot(UNIQUE_ROOT_EDEFAULT);
				return;
			case ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY:
				setEnableConcurrency(ENABLE_CONCURRENCY_EDEFAULT);
				return;
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
				getGlobalOptions().clear();
				return;
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
				getClsOptions().clear();
				return;
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
				getAttrOptions().clear();
				return;
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
				getRefOptions().clear();
				return;
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				getFilterOptions().clear();
				return;
			case ConfigPackage.OPTION_MODEL__BOUND_SOLVER:
				setBoundSolver(BOUND_SOLVER_EDEFAULT);
				return;
			case ConfigPackage.OPTION_MODEL__METAMODEL:
				setMetamodel((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.OPTION_MODEL__METAMODEL_URI:
				return METAMODEL_URI_EDEFAULT == null ? getMetamodelURI() != null : !METAMODEL_URI_EDEFAULT.equals(getMetamodelURI());
			case ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI:
				return OUTPUT_FILE_URI_EDEFAULT == null ? outputFileURI != null : !OUTPUT_FILE_URI_EDEFAULT.equals(outputFileURI);
			case ConfigPackage.OPTION_MODEL__ROOT_CLASS:
				return rootClass != null;
			case ConfigPackage.OPTION_MODEL__AUTO_NUMBER:
				return autoNumber != AUTO_NUMBER_EDEFAULT;
			case ConfigPackage.OPTION_MODEL__UNIQUE_ROOT:
				return uniqueRoot != UNIQUE_ROOT_EDEFAULT;
			case ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY:
				return enableConcurrency != ENABLE_CONCURRENCY_EDEFAULT;
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
				return globalOptions != null && !globalOptions.isEmpty();
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
				return clsOptions != null && !clsOptions.isEmpty();
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
				return attrOptions != null && !attrOptions.isEmpty();
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
				return refOptions != null && !refOptions.isEmpty();
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				return filterOptions != null && !filterOptions.isEmpty();
			case ConfigPackage.OPTION_MODEL__BOUND_SOLVER:
				return boundSolver != BOUND_SOLVER_EDEFAULT;
			case ConfigPackage.OPTION_MODEL__METAMODEL:
				return metamodel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (outputFileURI: ");
		result.append(outputFileURI);
		result.append(", autoNumber: ");
		result.append(autoNumber);
		result.append(", uniqueRoot: ");
		result.append(uniqueRoot);
		result.append(", enableConcurrency: ");
		result.append(enableConcurrency);
		result.append(", boundSolver: ");
		result.append(boundSolver);
		result.append(')');
		return result.toString();
	}

} //OptionModelImpl
