/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface IStructuralrmgBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.IStructuralrmgBracketHandler getBracketHandler();
	
}
