/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.ExternalAction;
import edu.ustb.sei.mde.mt4mt.ExternalActionParameter;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.Actions;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.URIClassLoader;
import edu.ustb.sei.mt4mt.base.updates.IExternalAction;
import edu.ustb.sei.mt4mt.base.updates.IUpdate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl#getResultName <em>Result Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExternalActionImpl#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExternalActionImpl extends TestActionImpl implements ExternalAction {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getResultName() <em>Result Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultName()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResultName() <em>Result Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultName()
	 * @generated
	 * @ordered
	 */
	protected String resultName = RESULT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ExternalActionParameter> parameters;

	/**
	 * The default value of the '{@link #getAction() <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected String action = ACTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.EXTERNAL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.EXTERNAL_ACTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResultName() {
		return resultName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultName(String newResultName) {
		String oldResultName = resultName;
		resultName = newResultName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.EXTERNAL_ACTION__RESULT_NAME, oldResultName, resultName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExternalActionParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ExternalActionParameter>(ExternalActionParameter.class, this, MT4MTPackage.EXTERNAL_ACTION__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(String newAction) {
		String oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.EXTERNAL_ACTION__ACTION, oldAction, action));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MT4MTPackage.EXTERNAL_ACTION__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.EXTERNAL_ACTION__NAME:
				return getName();
			case MT4MTPackage.EXTERNAL_ACTION__RESULT_NAME:
				return getResultName();
			case MT4MTPackage.EXTERNAL_ACTION__PARAMETERS:
				return getParameters();
			case MT4MTPackage.EXTERNAL_ACTION__ACTION:
				return getAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.EXTERNAL_ACTION__NAME:
				setName((String)newValue);
				return;
			case MT4MTPackage.EXTERNAL_ACTION__RESULT_NAME:
				setResultName((String)newValue);
				return;
			case MT4MTPackage.EXTERNAL_ACTION__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ExternalActionParameter>)newValue);
				return;
			case MT4MTPackage.EXTERNAL_ACTION__ACTION:
				setAction((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.EXTERNAL_ACTION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MT4MTPackage.EXTERNAL_ACTION__RESULT_NAME:
				setResultName(RESULT_NAME_EDEFAULT);
				return;
			case MT4MTPackage.EXTERNAL_ACTION__PARAMETERS:
				getParameters().clear();
				return;
			case MT4MTPackage.EXTERNAL_ACTION__ACTION:
				setAction(ACTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.EXTERNAL_ACTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MT4MTPackage.EXTERNAL_ACTION__RESULT_NAME:
				return RESULT_NAME_EDEFAULT == null ? resultName != null : !RESULT_NAME_EDEFAULT.equals(resultName);
			case MT4MTPackage.EXTERNAL_ACTION__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case MT4MTPackage.EXTERNAL_ACTION__ACTION:
				return ACTION_EDEFAULT == null ? action != null : !ACTION_EDEFAULT.equals(action);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer();
		result.append(name);
		result.append(" (");
		result.append(action);
		result.append(") => ");
		result.append(resultName);
		
		return result.toString();
	}
	
	@Override
	public IAction build() {
		URIClassLoader loader = new URIClassLoader(getClass().getClassLoader());
		IExternalAction exAction = null;
		
		String uri = this.getAction();
		
		Map<String,String> params = new HashMap<String,String>();
		for(ExternalActionParameter p : this.getParameters()) {
			params.put(p.getName(), p.getValue());
		}
		
		try {
			Class<?> clazz = loader.loadClass(uri);
			Class<? extends IExternalAction> actClazz = clazz.asSubclass(IExternalAction.class);
			exAction = (IExternalAction) actClazz.newInstance();
		} catch(Exception e) {
		}
		
		if(exAction==null) {
			return Actions.fail();
		}
		else 
			return Actions.external(params, this.getResultName(), exAction);
	}

} //ExternalActionImpl
