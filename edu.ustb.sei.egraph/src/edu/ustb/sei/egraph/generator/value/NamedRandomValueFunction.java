package edu.ustb.sei.egraph.generator.value;

import java.util.HashSet;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class NamedRandomValueFunction<T> extends RandomValueFunction<T> {
	
	public NamedRandomValueFunction(String name, RandomValueFunction<T> host) {
		super();
		this.name = name;
		this.host = host;
	}

	protected RandomValueFunction<T> host;

	@Override
	public T getValue(RandomService t) {
		T value = host.getValue(t);
		t.recordGeneratedValue(name, value);
		return value;
	}
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	private Set<String> provided = null;
	@Override
	public Set<String> providedScopes() {
		if(provided==null) {
			provided = new HashSet<String>();
			provided.add(name);
			provided.addAll(host.providedScopes());
		}
		return provided;
	}
	
	@Override
	public Set<String> requiredScopes() {
		return host.requiredScopes();
	}

}
