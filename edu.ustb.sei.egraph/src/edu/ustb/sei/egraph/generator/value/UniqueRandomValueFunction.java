package edu.ustb.sei.egraph.generator.value;

import java.util.List;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class UniqueRandomValueFunction<T> extends NamedRandomValueFunction<T> {
	private int retry;

	public UniqueRandomValueFunction(String name, RandomValueFunction<T> host, int retry) {
		super(name, host);// TODO Auto-generated constructor stub
		this.retry = retry;
	}
	
	@Override
	public T getValue(RandomService t) {
		int times = 0;
		T value = null;
		while(times<retry) {
			value = this.host.getValue(t);
			List<Object> values = t.getGeneratedValues(this.getName());
			
			if(!values.contains(value)) {
				t.recordGeneratedValue(getName(), value);
				return value;
			}
			
			times++;
		}
		
		// TODO: print warning!
		System.out.println("duplicate value "+value);
		return value;
	}

}
