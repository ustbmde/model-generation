/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.config.ConfigFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "config";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "edu.ustb.sei.rmg.config";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "config";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigPackage eINSTANCE = edu.ustb.sei.rmg.config.impl.ConfigPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl <em>Option Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.OptionModelImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getOptionModel()
	 * @generated
	 */
	int OPTION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__METAMODEL_URI = 0;

	/**
	 * The feature id for the '<em><b>Output File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__OUTPUT_FILE_URI = 1;

	/**
	 * The feature id for the '<em><b>Root Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__ROOT_CLASS = 2;

	/**
	 * The feature id for the '<em><b>Auto Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__AUTO_NUMBER = 3;

	/**
	 * The feature id for the '<em><b>Unique Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__UNIQUE_ROOT = 4;

	/**
	 * The feature id for the '<em><b>Enable Concurrency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__ENABLE_CONCURRENCY = 5;

	/**
	 * The feature id for the '<em><b>Global Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__GLOBAL_OPTIONS = 6;

	/**
	 * The feature id for the '<em><b>Cls Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__CLS_OPTIONS = 7;

	/**
	 * The feature id for the '<em><b>Attr Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__ATTR_OPTIONS = 8;

	/**
	 * The feature id for the '<em><b>Ref Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__REF_OPTIONS = 9;

	/**
	 * The feature id for the '<em><b>Filter Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__FILTER_OPTIONS = 10;

	/**
	 * The feature id for the '<em><b>Bound Solver</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__BOUND_SOLVER = 11;

	/**
	 * The feature id for the '<em><b>Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL__METAMODEL = 12;

	/**
	 * The number of structural features of the '<em>Option Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_MODEL_FEATURE_COUNT = 13;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ConditionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 7;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__BOUND_CONDITION = 0;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ScalableConditionImpl <em>Scalable Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ScalableConditionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getScalableCondition()
	 * @generated
	 */
	int SCALABLE_CONDITION = 8;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABLE_CONDITION__BOUND_CONDITION = CONDITION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Scalable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABLE_CONDITION__SCALABLE = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Scalable Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABLE_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ClassOptionImpl <em>Class Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ClassOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getClassOption()
	 * @generated
	 */
	int CLASS_OPTION = 1;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION__BOUND_CONDITION = SCALABLE_CONDITION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Scalable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION__SCALABLE = SCALABLE_CONDITION__SCALABLE;

	/**
	 * The feature id for the '<em><b>Class Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION__CLASS_REF = SCALABLE_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION__ATTRIBUTE_OPTIONS = SCALABLE_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reference Options</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION__REFERENCE_OPTIONS = SCALABLE_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Class Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_OPTION_FEATURE_COUNT = SCALABLE_CONDITION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl <em>Abstract Reference Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getAbstractReferenceOption()
	 * @generated
	 */
	int ABSTRACT_REFERENCE_OPTION = 4;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__BOUND_CONDITION = SCALABLE_CONDITION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Scalable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__SCALABLE = SCALABLE_CONDITION__SCALABLE;

	/**
	 * The feature id for the '<em><b>Source Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE = SCALABLE_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED = SCALABLE_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE = SCALABLE_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED = SCALABLE_CONDITION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transitivity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__TRANSITIVITY = SCALABLE_CONDITION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ring</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__RING = SCALABLE_CONDITION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__CIRCLE = SCALABLE_CONDITION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>External Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS = SCALABLE_CONDITION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Abstract Reference Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT = SCALABLE_CONDITION_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceOptionImpl <em>Reference Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ReferenceOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceOption()
	 * @generated
	 */
	int REFERENCE_OPTION = 2;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__BOUND_CONDITION = ABSTRACT_REFERENCE_OPTION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Scalable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__SCALABLE = ABSTRACT_REFERENCE_OPTION__SCALABLE;

	/**
	 * The feature id for the '<em><b>Source Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__SOURCE_UNIQUE = ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE;

	/**
	 * The feature id for the '<em><b>Source Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__SOURCE_REQUIRED = ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED;

	/**
	 * The feature id for the '<em><b>Target Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__TARGET_UNIQUE = ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE;

	/**
	 * The feature id for the '<em><b>Target Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__TARGET_REQUIRED = ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED;

	/**
	 * The feature id for the '<em><b>Transitivity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__TRANSITIVITY = ABSTRACT_REFERENCE_OPTION__TRANSITIVITY;

	/**
	 * The feature id for the '<em><b>Ring</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__RING = ABSTRACT_REFERENCE_OPTION__RING;

	/**
	 * The feature id for the '<em><b>Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__CIRCLE = ABSTRACT_REFERENCE_OPTION__CIRCLE;

	/**
	 * The feature id for the '<em><b>External Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__EXTERNAL_OBJECTS = ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS;

	/**
	 * The feature id for the '<em><b>Reference Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION__REFERENCE_REF = ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPTION_FEATURE_COUNT = ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl <em>Reference Group Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceGroupOption()
	 * @generated
	 */
	int REFERENCE_GROUP_OPTION = 3;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__BOUND_CONDITION = ABSTRACT_REFERENCE_OPTION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Scalable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__SCALABLE = ABSTRACT_REFERENCE_OPTION__SCALABLE;

	/**
	 * The feature id for the '<em><b>Source Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__SOURCE_UNIQUE = ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE;

	/**
	 * The feature id for the '<em><b>Source Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__SOURCE_REQUIRED = ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED;

	/**
	 * The feature id for the '<em><b>Target Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__TARGET_UNIQUE = ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE;

	/**
	 * The feature id for the '<em><b>Target Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__TARGET_REQUIRED = ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED;

	/**
	 * The feature id for the '<em><b>Transitivity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__TRANSITIVITY = ABSTRACT_REFERENCE_OPTION__TRANSITIVITY;

	/**
	 * The feature id for the '<em><b>Ring</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__RING = ABSTRACT_REFERENCE_OPTION__RING;

	/**
	 * The feature id for the '<em><b>Circle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__CIRCLE = ABSTRACT_REFERENCE_OPTION__CIRCLE;

	/**
	 * The feature id for the '<em><b>External Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__EXTERNAL_OBJECTS = ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS;

	/**
	 * The feature id for the '<em><b>References Ref</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__REFERENCES_REF = ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type Filter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION__TYPE_FILTER = ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reference Group Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_GROUP_OPTION_FEATURE_COUNT = ABSTRACT_REFERENCE_OPTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.AttributeOptionImpl <em>Attribute Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.AttributeOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getAttributeOption()
	 * @generated
	 */
	int ATTRIBUTE_OPTION = 5;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPTION__BOUND_CONDITION = CONDITION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Attribute Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPTION__ATTRIBUTE_REF = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPTION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.GlobalOptionImpl <em>Global Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.GlobalOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getGlobalOption()
	 * @generated
	 */
	int GLOBAL_OPTION = 6;

	/**
	 * The feature id for the '<em><b>Bound Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_OPTION__BOUND_CONDITION = CONDITION__BOUND_CONDITION;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_OPTION__KEY = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Global Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_OPTION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.FilterImpl <em>Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.FilterImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getFilter()
	 * @generated
	 */
	int FILTER = 9;

	/**
	 * The number of structural features of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.BasicFilterImpl <em>Basic Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.BasicFilterImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getBasicFilter()
	 * @generated
	 */
	int BASIC_FILTER = 10;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_FILTER__FEATURE = FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_FILTER__VALUE_LITERAL = FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_FILTER__OPERATOR = FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Basic Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_FILTER_FEATURE_COUNT = FILTER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.CombinationalFilterImpl <em>Combinational Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.CombinationalFilterImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCombinationalFilter()
	 * @generated
	 */
	int COMBINATIONAL_FILTER = 11;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_FILTER__LEFT = FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_FILTER__RIGHT = FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_FILTER__OPERATOR = FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Combinational Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_FILTER_FEATURE_COUNT = FILTER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl <em>Reference Filter Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceFilterOption()
	 * @generated
	 */
	int REFERENCE_FILTER_OPTION = 12;

	/**
	 * The feature id for the '<em><b>Source Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_OPTION__SOURCE_FILTER = 0;

	/**
	 * The feature id for the '<em><b>Target Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_OPTION__TARGET_FILTER = 1;

	/**
	 * The feature id for the '<em><b>Reference Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_OPTION__REFERENCE_REF = 2;

	/**
	 * The number of structural features of the '<em>Reference Filter Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_OPTION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.GlobalOptionEnum <em>Global Option Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.GlobalOptionEnum
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getGlobalOptionEnum()
	 * @generated
	 */
	int GLOBAL_OPTION_ENUM = 13;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.ComparisonOperator <em>Comparison Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.ComparisonOperator
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getComparisonOperator()
	 * @generated
	 */
	int COMPARISON_OPERATOR = 14;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.rmg.config.CombinationOperator <em>Combination Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.rmg.config.CombinationOperator
	 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCombinationOperator()
	 * @generated
	 */
	int COMBINATION_OPERATOR = 15;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.OptionModel <em>Option Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Option Model</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel
	 * @generated
	 */
	EClass getOptionModel();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#getMetamodelURI <em>Metamodel URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel URI</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getMetamodelURI()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_MetamodelURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#getOutputFileURI <em>Output File URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output File URI</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getOutputFileURI()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_OutputFileURI();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.OptionModel#getRootClass <em>Root Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Class</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getRootClass()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_RootClass();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#isAutoNumber <em>Auto Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Auto Number</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#isAutoNumber()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_AutoNumber();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#isUniqueRoot <em>Unique Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique Root</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#isUniqueRoot()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_UniqueRoot();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#isEnableConcurrency <em>Enable Concurrency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enable Concurrency</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#isEnableConcurrency()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_EnableConcurrency();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.OptionModel#getGlobalOptions <em>Global Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Options</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getGlobalOptions()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_GlobalOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.OptionModel#getClsOptions <em>Cls Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cls Options</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getClsOptions()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_ClsOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.OptionModel#getAttrOptions <em>Attr Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attr Options</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getAttrOptions()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_AttrOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.OptionModel#getRefOptions <em>Ref Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ref Options</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getRefOptions()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_RefOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.OptionModel#getFilterOptions <em>Filter Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filter Options</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getFilterOptions()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_FilterOptions();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.OptionModel#isBoundSolver <em>Bound Solver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bound Solver</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#isBoundSolver()
	 * @see #getOptionModel()
	 * @generated
	 */
	EAttribute getOptionModel_BoundSolver();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.OptionModel#getMetamodel <em>Metamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metamodel</em>'.
	 * @see edu.ustb.sei.rmg.config.OptionModel#getMetamodel()
	 * @see #getOptionModel()
	 * @generated
	 */
	EReference getOptionModel_Metamodel();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.ClassOption <em>Class Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Option</em>'.
	 * @see edu.ustb.sei.rmg.config.ClassOption
	 * @generated
	 */
	EClass getClassOption();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.ClassOption#getClassRef <em>Class Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class Ref</em>'.
	 * @see edu.ustb.sei.rmg.config.ClassOption#getClassRef()
	 * @see #getClassOption()
	 * @generated
	 */
	EReference getClassOption_ClassRef();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.ClassOption#getAttributeOptions <em>Attribute Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Options</em>'.
	 * @see edu.ustb.sei.rmg.config.ClassOption#getAttributeOptions()
	 * @see #getClassOption()
	 * @generated
	 */
	EReference getClassOption_AttributeOptions();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.rmg.config.ClassOption#getReferenceOptions <em>Reference Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reference Options</em>'.
	 * @see edu.ustb.sei.rmg.config.ClassOption#getReferenceOptions()
	 * @see #getClassOption()
	 * @generated
	 */
	EReference getClassOption_ReferenceOptions();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.ReferenceOption <em>Reference Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Option</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceOption
	 * @generated
	 */
	EClass getReferenceOption();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.ReferenceOption#getReferenceRef <em>Reference Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Ref</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceOption#getReferenceRef()
	 * @see #getReferenceOption()
	 * @generated
	 */
	EReference getReferenceOption_ReferenceRef();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.ReferenceGroupOption <em>Reference Group Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Group Option</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceGroupOption
	 * @generated
	 */
	EClass getReferenceGroupOption();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.config.ReferenceGroupOption#getReferencesRef <em>References Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>References Ref</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceGroupOption#getReferencesRef()
	 * @see #getReferenceGroupOption()
	 * @generated
	 */
	EReference getReferenceGroupOption_ReferencesRef();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.config.ReferenceGroupOption#getTypeFilter <em>Type Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Type Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceGroupOption#getTypeFilter()
	 * @see #getReferenceGroupOption()
	 * @generated
	 */
	EReference getReferenceGroupOption_TypeFilter();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption <em>Abstract Reference Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Reference Option</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption
	 * @generated
	 */
	EClass getAbstractReferenceOption();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceUnique <em>Source Unique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Unique</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceUnique()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_SourceUnique();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceRequired <em>Source Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Required</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isSourceRequired()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_SourceRequired();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetUnique <em>Target Unique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Unique</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetUnique()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_TargetUnique();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetRequired <em>Target Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Required</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isTargetRequired()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_TargetRequired();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isTransitivity <em>Transitivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transitivity</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isTransitivity()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_Transitivity();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isRing <em>Ring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ring</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isRing()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_Ring();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#isCircle <em>Circle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Circle</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#isCircle()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EAttribute getAbstractReferenceOption_Circle();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.rmg.config.AbstractReferenceOption#getExternalObjects <em>External Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>External Objects</em>'.
	 * @see edu.ustb.sei.rmg.config.AbstractReferenceOption#getExternalObjects()
	 * @see #getAbstractReferenceOption()
	 * @generated
	 */
	EReference getAbstractReferenceOption_ExternalObjects();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.AttributeOption <em>Attribute Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Option</em>'.
	 * @see edu.ustb.sei.rmg.config.AttributeOption
	 * @generated
	 */
	EClass getAttributeOption();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.AttributeOption#getAttributeRef <em>Attribute Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Ref</em>'.
	 * @see edu.ustb.sei.rmg.config.AttributeOption#getAttributeRef()
	 * @see #getAttributeOption()
	 * @generated
	 */
	EReference getAttributeOption_AttributeRef();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.GlobalOption <em>Global Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Option</em>'.
	 * @see edu.ustb.sei.rmg.config.GlobalOption
	 * @generated
	 */
	EClass getGlobalOption();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.GlobalOption#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see edu.ustb.sei.rmg.config.GlobalOption#getKey()
	 * @see #getGlobalOption()
	 * @generated
	 */
	EAttribute getGlobalOption_Key();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see edu.ustb.sei.rmg.config.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.Condition#getBoundCondition <em>Bound Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bound Condition</em>'.
	 * @see edu.ustb.sei.rmg.config.Condition#getBoundCondition()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_BoundCondition();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.ScalableCondition <em>Scalable Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scalable Condition</em>'.
	 * @see edu.ustb.sei.rmg.config.ScalableCondition
	 * @generated
	 */
	EClass getScalableCondition();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.ScalableCondition#isScalable <em>Scalable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scalable</em>'.
	 * @see edu.ustb.sei.rmg.config.ScalableCondition#isScalable()
	 * @see #getScalableCondition()
	 * @generated
	 */
	EAttribute getScalableCondition_Scalable();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.Filter
	 * @generated
	 */
	EClass getFilter();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.BasicFilter <em>Basic Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.BasicFilter
	 * @generated
	 */
	EClass getBasicFilter();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.BasicFilter#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see edu.ustb.sei.rmg.config.BasicFilter#getFeature()
	 * @see #getBasicFilter()
	 * @generated
	 */
	EReference getBasicFilter_Feature();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.BasicFilter#getValueLiteral <em>Value Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Literal</em>'.
	 * @see edu.ustb.sei.rmg.config.BasicFilter#getValueLiteral()
	 * @see #getBasicFilter()
	 * @generated
	 */
	EAttribute getBasicFilter_ValueLiteral();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.BasicFilter#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see edu.ustb.sei.rmg.config.BasicFilter#getOperator()
	 * @see #getBasicFilter()
	 * @generated
	 */
	EAttribute getBasicFilter_Operator();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.CombinationalFilter <em>Combinational Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinational Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.CombinationalFilter
	 * @generated
	 */
	EClass getCombinationalFilter();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see edu.ustb.sei.rmg.config.CombinationalFilter#getLeft()
	 * @see #getCombinationalFilter()
	 * @generated
	 */
	EReference getCombinationalFilter_Left();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see edu.ustb.sei.rmg.config.CombinationalFilter#getRight()
	 * @see #getCombinationalFilter()
	 * @generated
	 */
	EReference getCombinationalFilter_Right();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see edu.ustb.sei.rmg.config.CombinationalFilter#getOperator()
	 * @see #getCombinationalFilter()
	 * @generated
	 */
	EAttribute getCombinationalFilter_Operator();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption <em>Reference Filter Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Filter Option</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceFilterOption
	 * @generated
	 */
	EClass getReferenceFilterOption();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getSourceFilter <em>Source Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceFilterOption#getSourceFilter()
	 * @see #getReferenceFilterOption()
	 * @generated
	 */
	EReference getReferenceFilterOption_SourceFilter();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getTargetFilter <em>Target Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Filter</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceFilterOption#getTargetFilter()
	 * @see #getReferenceFilterOption()
	 * @generated
	 */
	EReference getReferenceFilterOption_TargetFilter();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.rmg.config.ReferenceFilterOption#getReferenceRef <em>Reference Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Ref</em>'.
	 * @see edu.ustb.sei.rmg.config.ReferenceFilterOption#getReferenceRef()
	 * @see #getReferenceFilterOption()
	 * @generated
	 */
	EReference getReferenceFilterOption_ReferenceRef();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.rmg.config.GlobalOptionEnum <em>Global Option Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Global Option Enum</em>'.
	 * @see edu.ustb.sei.rmg.config.GlobalOptionEnum
	 * @generated
	 */
	EEnum getGlobalOptionEnum();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.rmg.config.ComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator</em>'.
	 * @see edu.ustb.sei.rmg.config.ComparisonOperator
	 * @generated
	 */
	EEnum getComparisonOperator();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.rmg.config.CombinationOperator <em>Combination Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Combination Operator</em>'.
	 * @see edu.ustb.sei.rmg.config.CombinationOperator
	 * @generated
	 */
	EEnum getCombinationOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigFactory getConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.OptionModelImpl <em>Option Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.OptionModelImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getOptionModel()
		 * @generated
		 */
		EClass OPTION_MODEL = eINSTANCE.getOptionModel();

		/**
		 * The meta object literal for the '<em><b>Metamodel URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__METAMODEL_URI = eINSTANCE.getOptionModel_MetamodelURI();

		/**
		 * The meta object literal for the '<em><b>Output File URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__OUTPUT_FILE_URI = eINSTANCE.getOptionModel_OutputFileURI();

		/**
		 * The meta object literal for the '<em><b>Root Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__ROOT_CLASS = eINSTANCE.getOptionModel_RootClass();

		/**
		 * The meta object literal for the '<em><b>Auto Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__AUTO_NUMBER = eINSTANCE.getOptionModel_AutoNumber();

		/**
		 * The meta object literal for the '<em><b>Unique Root</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__UNIQUE_ROOT = eINSTANCE.getOptionModel_UniqueRoot();

		/**
		 * The meta object literal for the '<em><b>Enable Concurrency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__ENABLE_CONCURRENCY = eINSTANCE.getOptionModel_EnableConcurrency();

		/**
		 * The meta object literal for the '<em><b>Global Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__GLOBAL_OPTIONS = eINSTANCE.getOptionModel_GlobalOptions();

		/**
		 * The meta object literal for the '<em><b>Cls Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__CLS_OPTIONS = eINSTANCE.getOptionModel_ClsOptions();

		/**
		 * The meta object literal for the '<em><b>Attr Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__ATTR_OPTIONS = eINSTANCE.getOptionModel_AttrOptions();

		/**
		 * The meta object literal for the '<em><b>Ref Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__REF_OPTIONS = eINSTANCE.getOptionModel_RefOptions();

		/**
		 * The meta object literal for the '<em><b>Filter Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__FILTER_OPTIONS = eINSTANCE.getOptionModel_FilterOptions();

		/**
		 * The meta object literal for the '<em><b>Bound Solver</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_MODEL__BOUND_SOLVER = eINSTANCE.getOptionModel_BoundSolver();

		/**
		 * The meta object literal for the '<em><b>Metamodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_MODEL__METAMODEL = eINSTANCE.getOptionModel_Metamodel();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ClassOptionImpl <em>Class Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ClassOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getClassOption()
		 * @generated
		 */
		EClass CLASS_OPTION = eINSTANCE.getClassOption();

		/**
		 * The meta object literal for the '<em><b>Class Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_OPTION__CLASS_REF = eINSTANCE.getClassOption_ClassRef();

		/**
		 * The meta object literal for the '<em><b>Attribute Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_OPTION__ATTRIBUTE_OPTIONS = eINSTANCE.getClassOption_AttributeOptions();

		/**
		 * The meta object literal for the '<em><b>Reference Options</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_OPTION__REFERENCE_OPTIONS = eINSTANCE.getClassOption_ReferenceOptions();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceOptionImpl <em>Reference Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ReferenceOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceOption()
		 * @generated
		 */
		EClass REFERENCE_OPTION = eINSTANCE.getReferenceOption();

		/**
		 * The meta object literal for the '<em><b>Reference Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_OPTION__REFERENCE_REF = eINSTANCE.getReferenceOption_ReferenceRef();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl <em>Reference Group Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceGroupOption()
		 * @generated
		 */
		EClass REFERENCE_GROUP_OPTION = eINSTANCE.getReferenceGroupOption();

		/**
		 * The meta object literal for the '<em><b>References Ref</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_GROUP_OPTION__REFERENCES_REF = eINSTANCE.getReferenceGroupOption_ReferencesRef();

		/**
		 * The meta object literal for the '<em><b>Type Filter</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_GROUP_OPTION__TYPE_FILTER = eINSTANCE.getReferenceGroupOption_TypeFilter();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl <em>Abstract Reference Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.AbstractReferenceOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getAbstractReferenceOption()
		 * @generated
		 */
		EClass ABSTRACT_REFERENCE_OPTION = eINSTANCE.getAbstractReferenceOption();

		/**
		 * The meta object literal for the '<em><b>Source Unique</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE = eINSTANCE.getAbstractReferenceOption_SourceUnique();

		/**
		 * The meta object literal for the '<em><b>Source Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED = eINSTANCE.getAbstractReferenceOption_SourceRequired();

		/**
		 * The meta object literal for the '<em><b>Target Unique</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE = eINSTANCE.getAbstractReferenceOption_TargetUnique();

		/**
		 * The meta object literal for the '<em><b>Target Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED = eINSTANCE.getAbstractReferenceOption_TargetRequired();

		/**
		 * The meta object literal for the '<em><b>Transitivity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__TRANSITIVITY = eINSTANCE.getAbstractReferenceOption_Transitivity();

		/**
		 * The meta object literal for the '<em><b>Ring</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__RING = eINSTANCE.getAbstractReferenceOption_Ring();

		/**
		 * The meta object literal for the '<em><b>Circle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_REFERENCE_OPTION__CIRCLE = eINSTANCE.getAbstractReferenceOption_Circle();

		/**
		 * The meta object literal for the '<em><b>External Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS = eINSTANCE.getAbstractReferenceOption_ExternalObjects();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.AttributeOptionImpl <em>Attribute Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.AttributeOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getAttributeOption()
		 * @generated
		 */
		EClass ATTRIBUTE_OPTION = eINSTANCE.getAttributeOption();

		/**
		 * The meta object literal for the '<em><b>Attribute Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_OPTION__ATTRIBUTE_REF = eINSTANCE.getAttributeOption_AttributeRef();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.GlobalOptionImpl <em>Global Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.GlobalOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getGlobalOption()
		 * @generated
		 */
		EClass GLOBAL_OPTION = eINSTANCE.getGlobalOption();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_OPTION__KEY = eINSTANCE.getGlobalOption_Key();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ConditionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Bound Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__BOUND_CONDITION = eINSTANCE.getCondition_BoundCondition();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ScalableConditionImpl <em>Scalable Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ScalableConditionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getScalableCondition()
		 * @generated
		 */
		EClass SCALABLE_CONDITION = eINSTANCE.getScalableCondition();

		/**
		 * The meta object literal for the '<em><b>Scalable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALABLE_CONDITION__SCALABLE = eINSTANCE.getScalableCondition_Scalable();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.FilterImpl <em>Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.FilterImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getFilter()
		 * @generated
		 */
		EClass FILTER = eINSTANCE.getFilter();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.BasicFilterImpl <em>Basic Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.BasicFilterImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getBasicFilter()
		 * @generated
		 */
		EClass BASIC_FILTER = eINSTANCE.getBasicFilter();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_FILTER__FEATURE = eINSTANCE.getBasicFilter_Feature();

		/**
		 * The meta object literal for the '<em><b>Value Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_FILTER__VALUE_LITERAL = eINSTANCE.getBasicFilter_ValueLiteral();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_FILTER__OPERATOR = eINSTANCE.getBasicFilter_Operator();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.CombinationalFilterImpl <em>Combinational Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.CombinationalFilterImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCombinationalFilter()
		 * @generated
		 */
		EClass COMBINATIONAL_FILTER = eINSTANCE.getCombinationalFilter();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMBINATIONAL_FILTER__LEFT = eINSTANCE.getCombinationalFilter_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMBINATIONAL_FILTER__RIGHT = eINSTANCE.getCombinationalFilter_Right();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMBINATIONAL_FILTER__OPERATOR = eINSTANCE.getCombinationalFilter_Operator();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl <em>Reference Filter Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.impl.ReferenceFilterOptionImpl
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getReferenceFilterOption()
		 * @generated
		 */
		EClass REFERENCE_FILTER_OPTION = eINSTANCE.getReferenceFilterOption();

		/**
		 * The meta object literal for the '<em><b>Source Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_FILTER_OPTION__SOURCE_FILTER = eINSTANCE.getReferenceFilterOption_SourceFilter();

		/**
		 * The meta object literal for the '<em><b>Target Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_FILTER_OPTION__TARGET_FILTER = eINSTANCE.getReferenceFilterOption_TargetFilter();

		/**
		 * The meta object literal for the '<em><b>Reference Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_FILTER_OPTION__REFERENCE_REF = eINSTANCE.getReferenceFilterOption_ReferenceRef();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.GlobalOptionEnum <em>Global Option Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.GlobalOptionEnum
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getGlobalOptionEnum()
		 * @generated
		 */
		EEnum GLOBAL_OPTION_ENUM = eINSTANCE.getGlobalOptionEnum();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.ComparisonOperator <em>Comparison Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.ComparisonOperator
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getComparisonOperator()
		 * @generated
		 */
		EEnum COMPARISON_OPERATOR = eINSTANCE.getComparisonOperator();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.rmg.config.CombinationOperator <em>Combination Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.rmg.config.CombinationOperator
		 * @see edu.ustb.sei.rmg.config.impl.ConfigPackageImpl#getCombinationOperator()
		 * @generated
		 */
		EEnum COMBINATION_OPERATOR = eINSTANCE.getCombinationOperator();

	}

} //ConfigPackage
