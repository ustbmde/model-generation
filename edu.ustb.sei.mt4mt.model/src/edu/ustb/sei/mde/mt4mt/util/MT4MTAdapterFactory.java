/**
 */
package edu.ustb.sei.mde.mt4mt.util;

import edu.ustb.sei.mde.mt4mt.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage
 * @generated
 */
public class MT4MTAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MT4MTPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MT4MTPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MT4MTSwitch<Adapter> modelSwitch =
		new MT4MTSwitch<Adapter>() {
			@Override
			public Adapter caseTestSuite(TestSuite object) {
				return createTestSuiteAdapter();
			}
			@Override
			public Adapter caseTestCase(TestCase object) {
				return createTestCaseAdapter();
			}
			@Override
			public Adapter caseCustomizableTestCase(CustomizableTestCase object) {
				return createCustomizableTestCaseAdapter();
			}
			@Override
			public Adapter casePredefinedTestCase(PredefinedTestCase object) {
				return createPredefinedTestCaseAdapter();
			}
			@Override
			public Adapter caseGetPutTestCase(GetPutTestCase object) {
				return createGetPutTestCaseAdapter();
			}
			@Override
			public Adapter casePutGetTestCase(PutGetTestCase object) {
				return createPutGetTestCaseAdapter();
			}
			@Override
			public Adapter caseTestAction(TestAction object) {
				return createTestActionAdapter();
			}
			@Override
			public Adapter casePredefinedBXTestCase(PredefinedBXTestCase object) {
				return createPredefinedBXTestCaseAdapter();
			}
			@Override
			public Adapter caseSequenceAction(SequenceAction object) {
				return createSequenceActionAdapter();
			}
			@Override
			public Adapter caseLoadModelAction(LoadModelAction object) {
				return createLoadModelActionAdapter();
			}
			@Override
			public Adapter caseSaveModelAction(SaveModelAction object) {
				return createSaveModelActionAdapter();
			}
			@Override
			public Adapter caseExecuteLauncherAction(ExecuteLauncherAction object) {
				return createExecuteLauncherActionAdapter();
			}
			@Override
			public Adapter caseEvaluateOCLQueryAction(EvaluateOCLQueryAction object) {
				return createEvaluateOCLQueryActionAdapter();
			}
			@Override
			public Adapter caseCopyModelAction(CopyModelAction object) {
				return createCopyModelActionAdapter();
			}
			@Override
			public Adapter caseCompareModelAction(CompareModelAction object) {
				return createCompareModelActionAdapter();
			}
			@Override
			public Adapter caseAssertComparisonResultAction(AssertComparisonResultAction object) {
				return createAssertComparisonResultActionAdapter();
			}
			@Override
			public Adapter caseAssertOCLInvariantAction(AssertOCLInvariantAction object) {
				return createAssertOCLInvariantActionAdapter();
			}
			@Override
			public Adapter caseUserAction(UserAction object) {
				return createUserActionAdapter();
			}
			@Override
			public Adapter caseSetVariableAction(SetVariableAction object) {
				return createSetVariableActionAdapter();
			}
			@Override
			public Adapter caseUpdateModelAction(UpdateModelAction object) {
				return createUpdateModelActionAdapter();
			}
			@Override
			public Adapter caseWrapperTestCase(WrapperTestCase object) {
				return createWrapperTestCaseAdapter();
			}
			@Override
			public Adapter caseAutoExecutorTestSuite(AutoExecutorTestSuite object) {
				return createAutoExecutorTestSuiteAdapter();
			}
			@Override
			public Adapter caseClearAction(ClearAction object) {
				return createClearActionAdapter();
			}
			@Override
			public Adapter caseExternalAction(ExternalAction object) {
				return createExternalActionAdapter();
			}
			@Override
			public Adapter caseExternalActionParameter(ExternalActionParameter object) {
				return createExternalActionParameterAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.TestSuite <em>Test Suite</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.TestSuite
	 * @generated
	 */
	public Adapter createTestSuiteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.TestCase
	 * @generated
	 */
	public Adapter createTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.CustomizableTestCase <em>Customizable Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.CustomizableTestCase
	 * @generated
	 */
	public Adapter createCustomizableTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.PredefinedTestCase <em>Predefined Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedTestCase
	 * @generated
	 */
	public Adapter createPredefinedTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.GetPutTestCase <em>Get Put Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.GetPutTestCase
	 * @generated
	 */
	public Adapter createGetPutTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.PutGetTestCase <em>Put Get Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.PutGetTestCase
	 * @generated
	 */
	public Adapter createPutGetTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.TestAction <em>Test Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.TestAction
	 * @generated
	 */
	public Adapter createTestActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase <em>Predefined BX Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase
	 * @generated
	 */
	public Adapter createPredefinedBXTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.SequenceAction <em>Sequence Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.SequenceAction
	 * @generated
	 */
	public Adapter createSequenceActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction <em>Load Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.LoadModelAction
	 * @generated
	 */
	public Adapter createLoadModelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.SaveModelAction <em>Save Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.SaveModelAction
	 * @generated
	 */
	public Adapter createSaveModelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction <em>Execute Launcher Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction
	 * @generated
	 */
	public Adapter createExecuteLauncherActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction <em>Evaluate OCL Query Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.EvaluateOCLQueryAction
	 * @generated
	 */
	public Adapter createEvaluateOCLQueryActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction <em>Copy Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.CopyModelAction
	 * @generated
	 */
	public Adapter createCopyModelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction <em>Compare Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.CompareModelAction
	 * @generated
	 */
	public Adapter createCompareModelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction <em>Assert Comparison Result Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction
	 * @generated
	 */
	public Adapter createAssertComparisonResultActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction <em>Assert OCL Invariant Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction
	 * @generated
	 */
	public Adapter createAssertOCLInvariantActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.UserAction <em>User Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.UserAction
	 * @generated
	 */
	public Adapter createUserActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.SetVariableAction <em>Set Variable Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.SetVariableAction
	 * @generated
	 */
	public Adapter createSetVariableActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction <em>Update Model Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.UpdateModelAction
	 * @generated
	 */
	public Adapter createUpdateModelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase <em>Wrapper Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.WrapperTestCase
	 * @generated
	 */
	public Adapter createWrapperTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite <em>Auto Executor Test Suite</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite
	 * @generated
	 */
	public Adapter createAutoExecutorTestSuiteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.ClearAction <em>Clear Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.ClearAction
	 * @generated
	 */
	public Adapter createClearActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.ExternalAction <em>External Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalAction
	 * @generated
	 */
	public Adapter createExternalActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.mt4mt.ExternalActionParameter <em>External Action Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.mt4mt.ExternalActionParameter
	 * @generated
	 */
	public Adapter createExternalActionParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MT4MTAdapterFactory
