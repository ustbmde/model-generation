package edu.ustb.sei.rmg.random.condition;

import java.util.List;

public abstract class DistributionFunction<T> extends Condition<T> {
	public double[] parameters;
	public String name;

	protected void copyParamters(List<Double> list) {
		if(list==null){
			parameters = new double[0];
		} else {
			parameters = new double[list.size()];
			int i=0;
			for(Double d : list) {
				parameters[i] = d;
				i++;
			}
		}
	}
}
