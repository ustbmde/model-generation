package edu.ustb.sei.mt4mt.base;

import java.io.File;
import java.io.FileInputStream;

public class PathClassLoader extends ClassLoader {
	public PathClassLoader(String b) {
		super();
		this.base = b;
	}
	private String base;
	
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		String fileName = base+File.separator+name.replaceAll("\\.", "/")+".class";
		File file = new File(fileName);
		
		try {
			if(file.exists()) {
				FileInputStream in = new FileInputStream(file);
				byte[] buf = new byte[(int)file.length()];
				in.read(buf);
				in.close();
				return this.defineClass(name, buf, 0, buf.length);
			}
		} catch(Exception e) {
		}
		
		return super.findClass(name);
	}
}