package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class StringValueConcatFunction extends RandomValueFunction<String> {
	
	public StringValueConcatFunction(AbstractRandomFunction<String>[] components) {
		super();
		this.components = components;
	}


	private AbstractRandomFunction<String>[] components;
	

	@Override
	public String getValue(RandomService t) {
		String buf = null;
		
		for(AbstractRandomFunction<String> c : components) {
			String v = c.getValue(t).getValue(t);
			if(buf==null)
				buf = v;
			else
				buf += v;
		}

		return buf;
	}

}
