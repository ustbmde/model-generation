/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nested Template Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getTemplates <em>Templates</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getSort <em>Sort</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getNestedTemplateNode()
 * @model
 * @generated
 */
public interface NestedTemplateNode extends TemplateNode {
	/**
	 * Returns the value of the '<em><b>Templates</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.Template}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Templates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Templates</em>' reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getNestedTemplateNode_Templates()
	 * @model required="true"
	 * @generated
	 */
	EList<Template> getTemplates();

	/**
	 * Returns the value of the '<em><b>Sort</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.rmg.structuralrmg.CompositionSort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sort</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sort</em>' attribute.
	 * @see edu.ustb.sei.rmg.structuralrmg.CompositionSort
	 * @see #setSort(CompositionSort)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getNestedTemplateNode_Sort()
	 * @model required="true"
	 * @generated
	 */
	CompositionSort getSort();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode#getSort <em>Sort</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sort</em>' attribute.
	 * @see edu.ustb.sei.rmg.structuralrmg.CompositionSort
	 * @see #getSort()
	 * @generated
	 */
	void setSort(CompositionSort value);

} // NestedTemplateNode
