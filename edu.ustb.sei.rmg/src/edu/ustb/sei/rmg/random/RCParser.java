package edu.ustb.sei.rmg.random;

import lpg.runtime.*;

import lpg.runtime.BadParseException;
import lpg.runtime.BadParseSymFileException;
import lpg.runtime.DiagnoseParser;
import lpg.runtime.ErrorToken;
import lpg.runtime.IToken;
import lpg.runtime.ILexStream;
import lpg.runtime.Monitor;
import lpg.runtime.NullExportedSymbolsException;
import lpg.runtime.NullTerminalSymbolsException;
import lpg.runtime.ParseTable;
import lpg.runtime.RuleAction;
import lpg.runtime.UndefinedEofSymbolException;
import lpg.runtime.UnimplementedTerminalsException;	

import edu.ustb.sei.rmg.random.condition.*;
import java.util.*;

public class RCParser implements RuleAction
{
    private PrsStream prsStream = null;
    
    private boolean unimplementedSymbolsWarning = false;

    private static ParseTable prsTable = new RCParserprs();
    public ParseTable getParseTable() { return prsTable; }

    private DeterministicParser dtParser = null;
    public DeterministicParser getParser() { return dtParser; }

    private void setResult(Object object) { dtParser.setSym1(object); }
    public Object getRhsSym(int i) { return dtParser.getSym(i); }

    public int getRhsTokenIndex(int i) { return dtParser.getToken(i); }
    public IToken getRhsIToken(int i) { return prsStream.getIToken(getRhsTokenIndex(i)); }
    
    public int getRhsFirstTokenIndex(int i) { return dtParser.getFirstToken(i); }
    public IToken getRhsFirstIToken(int i) { return prsStream.getIToken(getRhsFirstTokenIndex(i)); }

    public int getRhsLastTokenIndex(int i) { return dtParser.getLastToken(i); }
    public IToken getRhsLastIToken(int i) { return prsStream.getIToken(getRhsLastTokenIndex(i)); }

    public int getLeftSpan() { return dtParser.getFirstToken(); }
    public IToken getLeftIToken()  { return prsStream.getIToken(getLeftSpan()); }

    public int getRightSpan() { return dtParser.getLastToken(); }
    public IToken getRightIToken() { return prsStream.getIToken(getRightSpan()); }

    public int getRhsErrorTokenIndex(int i)
    {
        int index = dtParser.getToken(i);
        IToken err = prsStream.getIToken(index);
        return (err instanceof ErrorToken ? index : 0);
    }
    public ErrorToken getRhsErrorIToken(int i)
    {
        int index = dtParser.getToken(i);
        IToken err = prsStream.getIToken(index);
        return (ErrorToken) (err instanceof ErrorToken ? err : null);
    }

    public void reset(ILexStream lexStream)
    {
        prsStream = new PrsStream(lexStream);
        dtParser.reset(prsStream);

        try
        {
            prsStream.remapTerminalSymbols(orderedTerminalSymbols(), prsTable.getEoftSymbol());
        }
        catch(NullExportedSymbolsException e) {
        }
        catch(NullTerminalSymbolsException e) {
        }
        catch(UnimplementedTerminalsException e)
        {
            if (unimplementedSymbolsWarning) {
                java.util.ArrayList unimplemented_symbols = e.getSymbols();
                System.out.println("The Lexer will not scan the following token(s):");
                for (int i = 0; i < unimplemented_symbols.size(); i++)
                {
                    Integer id = (Integer) unimplemented_symbols.get(i);
                    System.out.println("    " + RCParsersym.orderedTerminalSymbols[id.intValue()]);               
                }
                System.out.println();
            }
        }
        catch(UndefinedEofSymbolException e)
        {
            throw new Error(new UndefinedEofSymbolException
                                ("The Lexer does not implement the Eof symbol " +
                                 RCParsersym.orderedTerminalSymbols[prsTable.getEoftSymbol()]));
        }
    }
    
    public RCParser()
    {
        try
        {
            dtParser = new DeterministicParser(prsStream, prsTable, (RuleAction) this);
        }
        catch (NotDeterministicParseTableException e)
        {
            throw new Error(new NotDeterministicParseTableException
                                ("Regenerate RCParserprs.java with -NOBACKTRACK option"));
        }
        catch (BadParseSymFileException e)
        {
            throw new Error(new BadParseSymFileException("Bad Parser Symbol File -- RCParsersym.java. Regenerate RCParserprs.java"));
        }
    }

    public RCParser(ILexStream lexStream)
    {
        this();
        reset(lexStream);
    }

    public int numTokenKinds() { return RCParsersym.numTokenKinds; }
    public String[] orderedTerminalSymbols() { return RCParsersym.orderedTerminalSymbols; }
    public String getTokenKindName(int kind) { return RCParsersym.orderedTerminalSymbols[kind]; }            
    public int getEOFTokenKind() { return prsTable.getEoftSymbol(); }
    public IPrsStream getIPrsStream() { return prsStream; }

    /**
     * @deprecated replaced by {@link #getIPrsStream()}
     *
     */
    public PrsStream getPrsStream() { return prsStream; }

    /**
     * @deprecated replaced by {@link #getIPrsStream()}
     *
     */
    public PrsStream getParseStream() { return prsStream; }

    public Condition parser()
    {
        return parser(null, 0);
    }
        
    public Condition parser(Monitor monitor)
    {
        return parser(monitor, 0);
    }
        
    public Condition parser(int error_repair_count)
    {
        return parser(null, error_repair_count);
    }
        
    public Condition parser(Monitor monitor, int error_repair_count)
    {
        dtParser.setMonitor(monitor);

        try
        {
            return (Condition) dtParser.parse();
        }
        catch (BadParseException e)
        {
            prsStream.reset(e.error_token); // point to error token

            DiagnoseParser diagnoseParser = new DiagnoseParser(prsStream, prsTable);
            diagnoseParser.diagnose(e.error_token);
        }

        return null;
    }

    //
    // Additional entry points, if any
    //
    

    public void ruleAction(int ruleNumber)
    {
        switch (ruleNumber)
        {

            //
            // Rule 12:  UnequalConditionGoal ::= EqualConditionGoal <> FeaturePath : Possibility
            //
            case 12: {
				
Condition<?> eqCondition = (Condition<?>)getRhsSym(1);
List<String> path = (List<String>)getRhsSym(3);
Double realNumber = (Double)getRhsSym(5);
UnequalCondition lc = Condition.createUnequalCondition(eqCondition, path, realNumber);
setResult(lc);
                break;
            }

            //
            // Rule 15:  Possibility ::= RealLiteral
            //
            case 15: {
				
Object result = getRhsSym(1);
setResult(result);
                break;
            }
 
            //
            // Rule 18:  IDFuncName ::= geom
            //
            
            case 18:

            //
            // Rule 19:  IDFuncName ::= pois
            //
            case 19: {
				
String literal = getRhsIToken(1).toString();
setResult(literal);
                break;
            }
 
            //
            // Rule 20:  RDFuncName ::= norm
            //
            
            case 20:
 
            //
            // Rule 21:  RDFuncName ::= pare
            //
            
            case 21:

            //
            // Rule 22:  RDFuncName ::= cauc
            //
            case 22: {
				
String literal = getRhsIToken(1).toString();
setResult(literal);
                break;
            }

            //
            // Rule 23:  ParametersOpt ::= $Empty
            //
            case 23: {
				
setResult(null);
                break;
            }

            //
            // Rule 25:  Parameters ::= Parameter
            //
            case 25: {
				
Double literal = (Double)getRhsSym(1);
List<Double> list = new ArrayList<Double>();
list.add(literal);
setResult(list);
                break;
            }

            //
            // Rule 26:  Parameters ::= Parameters , Parameter
            //
            case 26: {
				
List<Double> list = (List<Double>)getRhsSym(1);
Double literal = (Double)getRhsSym(3);
list.add(literal);
setResult(list);
                break;
            }

            //
            // Rule 28:  Parameter ::= IntegerLiteral
            //
            case 28: {
				
int literal = (int)getRhsSym(2);
setResult((double)literal);
                break;
            }

            //
            // Rule 29:  SeparateFunction ::= separate ( IntegerLiteral ) @ AtomicIntegerConditionWithoutDP
            //
            case 29: {
				
Condition<Integer> sum = (Condition<Integer>)getRhsSym(3);
Condition<Integer> count = (Condition<Integer>)getRhsSym(6);

SeparateFunction sf = Condition.createSeparateFunction(sum, count);
setResult(sf);
                break;
            }

            //
            // Rule 30:  ListCondition ::= list ( goal ) @ AtomicIntegerConditionWithoutDP
            //
            case 30: {
				
Condition<?> element = (Condition<?>)getRhsSym(3);
Condition<Integer> count = (Condition<Integer>)getRhsSym(6);

ListCondition lc = Condition.createListCondition(element, count);
setResult(lc);
                break;
            }

            //
            // Rule 31:  Scope ::= all
            //
            case 31: {
				
List<String> buf = new ArrayList<String>(0);
setResult(buf);
                break;
            }

            //
            // Rule 32:  Scope ::= FeaturePath
            //
            case 32: {
				
List<String> buf = (List<String>)getRhsSym(1);
setResult(buf);
                break;
            }

            //
            // Rule 33:  IntegerConditionGoal ::= IntegerConditionList
            //
            case 33: {
				
Condition<Integer> c = (Condition<Integer>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Integer>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 34:  IntegerConditionGoal ::= unique IntegerConditionList for Scope
            //
            case 34: {
				
Condition<Integer> c = (Condition<Integer>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Integer>)c).recover();
}
c.uniqueScope = (List<String>)getRhsSym(4);
setResult(c);
                break;
            }

            //
            // Rule 35:  IntegerConditionList ::= AtomicIntegerCondition
            //
            case 35: {
				
Condition<Integer> left = (Condition<Integer>)getRhsSym(1);
ConditionList<Integer> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 36:  IntegerConditionList ::= IntegerConditionList , AtomicIntegerCondition
            //
            case 36: {
				
ConditionList<Integer> left = (ConditionList<Integer>)getRhsSym(1);
Condition<Integer> right = (Condition<Integer>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 37:  AtomicIntegerCondition ::= AtomicIntegerConditionWithoutDP : Possibility
            //
            case 37: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Integer> cond = (Condition<Integer>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 38:  IntegerConditionList ::= AtomicInevitableIntegerCondition
            //
            case 38: {
				
InevitableCondition<Integer> left = (InevitableCondition<Integer>)getRhsSym(1);
ConditionList<Integer> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 39:  IntegerConditionList ::= IntegerConditionList , AtomicInevitableIntegerCondition
            //
            case 39: {
				
ConditionList<Integer> left = (ConditionList<Integer>)getRhsSym(1);
InevitableCondition<Integer> right = (InevitableCondition<Integer>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 40:  AtomicInevitableIntegerCondition ::= < AtomicIntegerConditionWithoutDP >
            //
            case 40: {
				
Condition<Integer> body = (Condition<Integer>)getRhsSym(2);
InevitableCondition<Integer> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 43:  AtomicIntegerConditionWithoutDP ::= IntegerLiteral
            //
            case 43: {
				
Integer literal = (Integer)getRhsSym(1);
LiteralCondition<Integer> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 45:  AtomicIntegerConditionWithoutDP ::= { IntegerConditionList }
            //
            case 45: {
				
Condition<Integer> c = (Condition<Integer>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Integer>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 46:  IntegerRange ::= [ IntegerLiteral .. IntegerLiteral ]
            //
            case 46: {
				
Integer l = (Integer)getRhsSym(2);
Integer u = (Integer)getRhsSym(4);
IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 47:  IntegerRange ::= [ IntegerLiteral .. + ]
            //
            case 47: {
				
Integer l = (Integer)getRhsSym(2);
Integer u = null;
IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 48:  IntegerRange ::= [ - .. IntegerLiteral ]
            //
            case 48: {
				
Integer l = null;
Integer u = (Integer)getRhsSym(4);
IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 49:  IntegerLiteral ::= - IntegerLiteral
            //
            case 49: {
				
Integer literal = (Integer)getRhsSym(2);
setResult(-literal);
                break;
            }

            //
            // Rule 50:  IntegerLiteral ::= INTEGER_LITERAL
            //
            case 50: {
				
String literal = getRhsIToken(1).toString();
setResult(Integer.parseInt(literal));
                break;
            }

            //
            // Rule 51:  IntegerDistFunc ::= DistFunc @ IntegerRange
            //
            case 51: {
				
DistributionFunction<?> f = (DistributionFunction<?>)getRhsSym(1);
RangeCondition<Integer> b = (RangeCondition<Integer>)getRhsSym(3);
CompositeIntegerFunction d = Condition.createCompositeIntegerFunction(f,b);
setResult(d);
                break;
            }

            //
            // Rule 52:  IntegerDistFunc ::= IDFuncName ( ParametersOpt )
            //
            case 52: {
				
String name = (String)getRhsSym(1);
List<Double> list = (List<Double>)getRhsSym(3);
IntegerDistributionFunction d = Condition.createIntegerDistributionFunction(name,list);
setResult(d);
                break;
            }

            //
            // Rule 53:  LongConditionGoal ::= LongConditionList
            //
            case 53: {
				
Condition<Long> c = (Condition<Long>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Long>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 54:  LongConditionGoal ::= unique LongConditionList for Scope
            //
            case 54: {
				
Condition<Long> c = (Condition<Long>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Long>)c).recover();
}
c.uniqueScope = (List<String>)getRhsSym(4);
setResult(c);
                break;
            }

            //
            // Rule 55:  LongConditionList ::= AtomicLongCondition
            //
            case 55: {
				
Condition<Long> left = (Condition<Long>)getRhsSym(1);
ConditionList<Long> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 56:  LongConditionList ::= LongConditionList , AtomicLongCondition
            //
            case 56: {
				
ConditionList<Long> left = (ConditionList<Long>)getRhsSym(1);
Condition<Long> right = (Condition<Long>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 57:  AtomicLongCondition ::= AtomicLongConditionWithoutDP : Possibility
            //
            case 57: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Long> cond = (Condition<Long>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 58:  LongConditionList ::= AtomicInevitableLongCondition
            //
            case 58: {
				
InevitableCondition<Long> left = (InevitableCondition<Long>)getRhsSym(1);
ConditionList<Long> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 59:  LongConditionList ::= LongConditionList , AtomicInevitableLongCondition
            //
            case 59: {
				
ConditionList<Long> left = (ConditionList<Long>)getRhsSym(1);
InevitableCondition<Long> right = (InevitableCondition<Long>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 60:  AtomicInevitableLongCondition ::= < AtomicLongConditionWithoutDP >
            //
            case 60: {
				
Condition<Long> body = (Condition<Long>)getRhsSym(2);
InevitableCondition<Long> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 63:  AtomicLongConditionWithoutDP ::= LongLiteral
            //
            case 63: {
				
Long literal = (Long)getRhsSym(1);
LiteralCondition<Long> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 64:  AtomicLongConditionWithoutDP ::= { LongConditionList }
            //
            case 64: {
				
Condition<Long> c = (Condition<Long>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Long>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 65:  LongRange ::= [ LongLiteral .. LongLiteral ]
            //
            case 65: {
				
Long l = (Long)getRhsSym(2);
Long u = (Long)getRhsSym(4);
LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 66:  LongRange ::= [ LongLiteral .. + ]
            //
            case 66: {
				
Long l = (Long)getRhsSym(2);
Long u = null;
LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 67:  LongRange ::= [ - .. LongLiteral ]
            //
            case 67: {
				
Long l = null;
Long u = (Long)getRhsSym(4);
LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 68:  LongLiteral ::= - LongLiteral
            //
            case 68: {
				
Long literal = (Long)getRhsSym(2);
setResult(-literal);
                break;
            }

            //
            // Rule 69:  LongLiteral ::= LONG_LITERAL
            //
            case 69: {
				
String literal = getRhsIToken(1).toString();
literal = literal.substring(0,literal.length()-1);
setResult(Long.parseLong(literal));
                break;
            }

            //
            // Rule 70:  RealDistFunc ::= RDFuncName ( ParametersOpt )
            //
            case 70: {
				
String name = (String)getRhsSym(1);
List<Double> list = (List<Double>)getRhsSym(3);
RealDistributionFunction d = Condition.createRealDistributionFunction(name,list);
setResult(d);
                break;
            }

            //
            // Rule 71:  RealConditionGoal ::= RealConditionList
            //
            case 71: {
				
Condition<Double> c = (Condition<Double>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Double>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 72:  RealConditionGoal ::= unique RealConditionList for Scope
            //
            case 72: {
				
Condition<Double> c = (Condition<Double>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Double>)c).recover();
}
c.uniqueScope = (List<String>)getRhsSym(4);
setResult(c);
                break;
            }

            //
            // Rule 73:  RealConditionList ::= AtomicRealCondition
            //
            case 73: {
				
Condition<Double> left = (Condition<Double>)getRhsSym(1);
ConditionList<Double> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 74:  RealConditionList ::= RealConditionList , AtomicRealCondition
            //
            case 74: {
				
ConditionList<Double> left = (ConditionList<Double>)getRhsSym(1);
Condition<Double> right = (Condition<Double>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 75:  AtomicRealCondition ::= AtomicRealConditionWithoutDP : Possibility
            //
            case 75: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Double> cond = (Condition<Double>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 76:  RealConditionList ::= AtomicInevitableRealCondition
            //
            case 76: {
				
InevitableCondition<Double> left = (InevitableCondition<Double>)getRhsSym(1);
ConditionList<Double> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 77:  RealConditionList ::= LongConditionList , AtomicInevitableRealCondition
            //
            case 77: {
				
ConditionList<Double> left = (ConditionList<Double>)getRhsSym(1);
InevitableCondition<Double> right = (InevitableCondition<Double>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 78:  AtomicInevitableRealCondition ::= < AtomicRealConditionWithoutDP >
            //
            case 78: {
				
Condition<Double> body = (Condition<Double>)getRhsSym(2);
InevitableCondition<Double> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 82:  AtomicRealConditionWithoutDP ::= RealLiteral
            //
            case 82: {
				
Double literal = (Double)getRhsSym(1);
LiteralCondition<Double> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 83:  AtomicRealConditionWithoutDP ::= { RealConditionList }
            //
            case 83: {
				
Condition<Double> c = (Condition<Double>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Double>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 84:  RealRange ::= [ RealLiteral .. RealLiteral ]
            //
            case 84: {
				
Double l = (Double)getRhsSym(2);
Double u = (Double)getRhsSym(4);
RealRangeCondition cond = Condition.createRealRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 85:  RealLiteral ::= - RealLiteral
            //
            case 85: {
				
Double literal = (Double)getRhsSym(2);
setResult(-literal);
                break;
            }

            //
            // Rule 86:  RealLiteral ::= REAL_LITERAL
            //
            case 86: {
				
String literal = getRhsIToken(1).toString();
setResult(Double.parseDouble(literal));
                break;
            }

            //
            // Rule 87:  CharConditionGoal ::= CharConditionList
            //
            case 87: {
				
Condition<Character> c = (Condition<Character>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Character>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 88:  CharConditionList ::= AtomicCharCondition
            //
            case 88: {
				
Condition<Character> left = (Condition<Character>)getRhsSym(1);
ConditionList<Character> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 89:  CharConditionList ::= CharConditionList , AtomicCharCondition
            //
            case 89: {
				
ConditionList<Character> left = (ConditionList<Character>)getRhsSym(1);
Condition<Character> right = (Condition<Character>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 90:  AtomicCharCondition ::= AtomicCharConditionWithoutDP : Possibility
            //
            case 90: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Character> cond = (Condition<Character>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 91:  CharConditionList ::= AtomicInevitableCharCondition
            //
            case 91: {
				
InevitableCondition<Character> left = (InevitableCondition<Character>)getRhsSym(1);
ConditionList<Character> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 92:  CharConditionList ::= CharConditionList , AtomicInevitableCharCondition
            //
            case 92: {
				
ConditionList<Character> left = (ConditionList<Character>)getRhsSym(1);
InevitableCondition<Character> right = (InevitableCondition<Character>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 93:  AtomicInevitableCharCondition ::= < AtomicCharConditionWithoutDP >
            //
            case 93: {
				
Condition<Character> body = (Condition<Character>)getRhsSym(2);
InevitableCondition<Character> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 96:  AtomicCharConditionWithoutDP ::= CharLiteral
            //
            case 96: {
				
Character literal = (Character)getRhsSym(1);
LiteralCondition<Character> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 97:  AtomicCharConditionWithoutDP ::= { CharConditionList }
            //
            case 97: {
				
Condition<Character> c = (Condition<Character>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Character>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 98:  CharRange ::= [ CharLiteral .. CharLiteral ]
            //
            case 98: {
				
Character l = (Character)getRhsSym(2);
Character u = (Character)getRhsSym(4);
CharRangeCondition cond = Condition.createCharRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 99:  CharLiteral ::= CHAR_LITERAL
            //
            case 99: {
				
String literal = getRhsIToken(1).toString();
setResult(Condition.convertChar(literal));
                break;
            }

            //
            // Rule 100:  StringConditionGoal ::= StringConditionList
            //
            case 100: {
				
Condition<String> c = (Condition<String>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<String>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 101:  StringConditionGoal ::= unique StringConditionList for Scope
            //
            case 101: {
				
Condition<String> c = (Condition<String>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<String>)c).recover();
}
c.uniqueScope = (List<String>)getRhsSym(4);
setResult(c);
                break;
            }

            //
            // Rule 102:  StringConditionList ::= AtomicStringCondition
            //
            case 102: {
				
Condition<String> left = (Condition<String>)getRhsSym(1);
ConditionList<String> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 103:  StringConditionList ::= StringConditionList , AtomicStringCondition
            //
            case 103: {
				
ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
Condition<String> right = (Condition<String>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 104:  AtomicStringCondition ::= AtomicStringConditionWithoutDP : Possibility
            //
            case 104: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<String> cond = (Condition<String>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 105:  StringConditionList ::= AtomicInevitableStringCondition
            //
            case 105: {
				
InevitableCondition<String> left = (InevitableCondition<String>)getRhsSym(1);
ConditionList<String> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 106:  StringConditionList ::= StringConditionList , AtomicInevitableStringCondition
            //
            case 106: {
				
ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
InevitableCondition<String> right = (InevitableCondition<String>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 107:  AtomicInevitableStringCondition ::= < AtomicStringConditionWithoutDP >
            //
            case 107: {
				
Condition<String> body = (Condition<String>)getRhsSym(2);
InevitableCondition<String> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 110:  AtomicStringConcatConditionWithoutDP ::= AtomicStringConcatConditionWithoutDP + AtomicStringSimpleConditionWithoutDP
            //
            case 110: {
				
Condition<String> first = (Condition<String>)getRhsSym(1);
Condition<String> second = (Condition<String>)getRhsSym(3);

StringConcatCondition result = Condition.createStringConcatCondition(first, second);
setResult(result);
                break;
            }

            //
            // Rule 112:  AtomicStringSimpleConditionWithoutDP ::= AtomicCharConditionWithoutDP @ AtomicIntegerConditionWithoutDP
            //
            case 112: {
				
Condition<Character> charRange = (Condition<Character>)getRhsSym(1);
Condition<Integer> sizeRange = (Condition<Integer>)getRhsSym(3);

StringRangeCondition result = Condition.createStringRangeCondition(charRange,sizeRange);
setResult(result);
                break;
            }

            //
            // Rule 113:  AtomicStringSimpleConditionWithoutDP ::= StringLiteral
            //
            case 113: {
				
String literal = (String)getRhsSym(1);
LiteralCondition<String> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 114:  AtomicStringSimpleConditionWithoutDP ::= { StringConditionList }
            //
            case 114: {
				
Condition<String> c = (Condition<String>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<String>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 115:  AtomicStringSimpleConditionWithoutDP ::= FeaturePath
            //
            case 115: {
				
List<String> list = (List<String>)getRhsSym(1);
FeaturePathCondition<String> sfc = Condition.createStringFeatureCondition(list);
setResult(sfc);
                break;
            }

            //
            // Rule 116:  StringLiteral ::= STRING_LITERAL
            //
            case 116: {
				
String literal = getRhsIToken(1).toString();
setResult(Condition.convertString(literal));
                break;
            }

            //
            // Rule 117:  FeaturePath ::= $ IDENTIFIER
            //
            case 117: {
				
String literal = getRhsIToken(2).toString();
List<String> list = new ArrayList<String>();
list.add(literal);
setResult(list);
                break;
            }

            //
            // Rule 118:  FeaturePath ::= FeaturePath . IDENTIFIER
            //
            case 118: {
				
List<String> list = (List<String>)getRhsSym(1);
String literal = getRhsIToken(3).toString();
list.add(literal);
setResult(list);
                break;
            }

            //
            // Rule 119:  BooleanConditionGoal ::= BooleanConditionList
            //
            case 119: {
				
Condition<Boolean> c = (Condition<Boolean>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Boolean>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 120:  BooleanConditionList ::= AtomicBooleanCondition
            //
            case 120: {
				
Condition<Boolean> left = (Condition<Boolean>)getRhsSym(1);
ConditionList<Boolean> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 121:  BooleanConditionList ::= BooleanConditionList , AtomicBooleanCondition
            //
            case 121: {
				
ConditionList<Boolean> left = (ConditionList<Boolean>)getRhsSym(1);
Condition<Boolean> right = (Condition<Boolean>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 122:  AtomicBooleanCondition ::= AtomicBooleanConditionWithoutDP : Possibility
            //
            case 122: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Boolean> cond = (Condition<Boolean>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 123:  BooleanConditionList ::= AtomicInevitableBooleanCondition
            //
            case 123: {
				
InevitableCondition<Boolean> left = (InevitableCondition<Boolean>)getRhsSym(1);
ConditionList<Boolean> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 124:  BooleanConditionList ::= BooleanConditionList , AtomicInevitableBooleanCondition
            //
            case 124: {
				
ConditionList<Boolean> left = (ConditionList<Boolean>)getRhsSym(1);
InevitableCondition<Boolean> right = (InevitableCondition<Boolean>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 125:  AtomicInevitableBooleanCondition ::= < AtomicBooleanConditionWithoutDP >
            //
            case 125: {
				
Condition<Boolean> body = (Condition<Boolean>)getRhsSym(2);
InevitableCondition<Boolean> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 127:  AtomicBooleanConditionWithoutDP ::= BooleanLiteral
            //
            case 127: {
				
Boolean literal = (Boolean)getRhsSym(1);
LiteralCondition<Boolean> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 128:  AtomicBooleanConditionWithoutDP ::= { BooleanConditionList }
            //
            case 128: {
				
Condition<Boolean> c = (Condition<Boolean>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Boolean>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 129:  BooleanLiteral ::= true
            //
            case 129: {
				
setResult(true);
                break;
            }

            //
            // Rule 130:  BooleanLiteral ::= false
            //
            case 130: {
				
setResult(false);
                break;
            }

            //
            // Rule 131:  EnumConditionGoal ::= EnumConditionList
            //
            case 131: {
				
Condition<String> c = (Condition<String>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<String>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 132:  EnumConditionList ::= AtomicEnumCondition
            //
            case 132: {
				
Condition<String> left = (Condition<String>)getRhsSym(1);
ConditionList<String> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 133:  EnumConditionList ::= EnumConditionList , AtomicEnumCondition
            //
            case 133: {
				
ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
Condition<String> right = (Condition<String>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 134:  AtomicEnumCondition ::= AtomicEnumConditionWithoutDP : Possibility
            //
            case 134: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<String> cond = (Condition<String>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 135:  EnumConditionList ::= AtomicInevitableEnumCondition
            //
            case 135: {
				
InevitableCondition<String> left = (InevitableCondition<String>)getRhsSym(1);
ConditionList<String> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 136:  EnumConditionList ::= EnumConditionList , AtomicInevitableEnumCondition
            //
            case 136: {
				
ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
InevitableCondition<String> right = (InevitableCondition<String>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 137:  AtomicInevitableEnumCondition ::= < AtomicEnumConditionWithoutDP >
            //
            case 137: {
				
Condition<String> body = (Condition<String>)getRhsSym(2);
InevitableCondition<String> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 139:  AtomicEnumConditionWithoutDP ::= EnumLiteral
            //
            case 139: {
				
String literal = (String)getRhsSym(1);
LiteralCondition<String> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 140:  AtomicEnumConditionWithoutDP ::= { EnumConditionList }
            //
            case 140: {
				
Condition<String> c = (Condition<String>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<String>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 141:  EnumLiteral ::= IDENTIFIER
            //
            case 141: {
				
String literal = getRhsIToken(1).toString();
setResult(literal);
                break;
            }

            //
            // Rule 142:  DateConditionGoal ::= DateConditionList
            //
            case 142: {
				
Condition<Calendar> c = (Condition<Calendar>)getRhsSym(1);
if(c instanceof ConditionList) {
  ((ConditionList<Calendar>)c).recover();
}
setResult(c);
                break;
            }

            //
            // Rule 143:  DateConditionGoal ::= unique DateConditionList for Scope
            //
            case 143: {
				
Condition<Calendar> c = (Condition<Calendar>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Calendar>)c).recover();
}
c.uniqueScope = (List<String>)getRhsSym(4);
setResult(c);
                break;
            }

            //
            // Rule 144:  DateConditionList ::= AtomicDateCondition
            //
            case 144: {
				
Condition<Calendar> left = (Condition<Calendar>)getRhsSym(1);
ConditionList<Calendar> cond = Condition.createConditionList(left,null);
setResult(cond);
                break;
            }

            //
            // Rule 145:  DateConditionList ::= DateConditionList , AtomicDateCondition
            //
            case 145: {
				
ConditionList<Calendar> left = (ConditionList<Calendar>)getRhsSym(1);
Condition<Calendar> right = (Condition<Calendar>)getRhsSym(3);
left.list.add(right);
setResult(left);
                break;
            }

            //
            // Rule 146:  AtomicDateCondition ::= AtomicDateConditionWithoutDP : Possibility
            //
            case 146: {
				
Double realNumber = (Double)getRhsSym(3);
Condition<Calendar> cond = (Condition<Calendar>)getRhsSym(1);
cond.possibility = realNumber;
setResult(cond);
                break;
            }

            //
            // Rule 147:  DateConditionList ::= AtomicInevitableDateCondition
            //
            case 147: {
				
InevitableCondition<Calendar> left = (InevitableCondition<Calendar>)getRhsSym(1);
ConditionList<Calendar> cond = Condition.createConditionList(null,null);
cond.inevitableConditionList.add(left);
setResult(cond);
                break;
            }

            //
            // Rule 148:  DateConditionList ::= DateConditionList , AtomicInevitableDateCondition
            //
            case 148: {
				
ConditionList<Calendar> left = (ConditionList<Calendar>)getRhsSym(1);
InevitableCondition<Calendar> right = (InevitableCondition<Calendar>)getRhsSym(3);
left.inevitableConditionList.add(right);
setResult(left);
                break;
            }

            //
            // Rule 149:  AtomicInevitableDateCondition ::= < AtomicDateConditionWithoutDP >
            //
            case 149: {
				
Condition<Calendar> body = (Condition<Calendar>)getRhsSym(2);
InevitableCondition<Calendar> iC = Condition.createInevitableCondition(body);
setResult(iC);
                break;
            }

            //
            // Rule 152:  AtomicDateConditionWithoutDP ::= DateLiteral
            //
            case 152: {
				
Calendar literal = (Calendar)getRhsSym(1);
LiteralCondition<Calendar> result = Condition.createLiteralCondition(literal);
setResult(result);
                break;
            }

            //
            // Rule 153:  AtomicDateConditionWithoutDP ::= { DateConditionList }
            //
            case 153: {
				
Condition<Calendar> c = (Condition<Calendar>)getRhsSym(2);
if(c instanceof ConditionList) {
  ((ConditionList<Calendar>)c).recover();
}
setResult(Condition.createAtomicCondition(c));
                break;
            }

            //
            // Rule 154:  DateRange ::= [ DateLiteral .. DateLiteral ]
            //
            case 154: {
				
Calendar l = (Calendar)getRhsSym(2);
Calendar u = (Calendar)getRhsSym(4);
DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 155:  DateRange ::= [ DateLiteral .. + ]
            //
            case 155: {
				
Calendar l = (Calendar)getRhsSym(2);
Calendar u = null;
DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 156:  DateRange ::= [ - .. DateLiteral ]
            //
            case 156: {
				
Calendar l = null;
Calendar u = (Calendar)getRhsSym(4);
DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
setResult(cond);
                break;
            }

            //
            // Rule 157:  DateLiteral ::= DATE_LITERAL
            //
            case 157: {
				
String literal = getRhsIToken(1).toString();
setResult(Condition.convertCalendar(literal));
                break;
            }

    
            default:
                break;
        }
        return;
    }
}

