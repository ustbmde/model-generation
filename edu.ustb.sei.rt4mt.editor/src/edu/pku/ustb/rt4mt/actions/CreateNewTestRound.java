package edu.pku.ustb.rt4mt.actions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;
import edu.pku.ustb.rt4mt.TestRoundGroup;

public class CreateNewTestRound extends Action implements
		ISelectionChangedListener {

	protected ISelectionProvider selectionProvider;
	public ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	public void setSelectionProvider(ISelectionProvider selectionProvider) {
		this.selectionProvider = selectionProvider;
	}

	protected TestRoundGroup testRoundGroup;
	
	public CreateNewTestRound() {
		super("Create Test Round");
		setDescription("Create a New Test Round");
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		selectionProvider = event.getSelectionProvider();
	    if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	public boolean updateSelection(IStructuredSelection selection) {
		if(selection.size()==1) {
			Object se = selection.getFirstElement();
			if(se instanceof TestRoundGroup) {
				testRoundGroup = (TestRoundGroup) se;
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		TestRound r = Rt4mtFactory.eINSTANCE.createTestRound();
		EList<TestRound> round = testRoundGroup.getRounds();
		if(round.size()==0) {
			r.setRoundNumber(0);
		}
		else {
			TestRound testRound = round.get(round.size()-1);
			r.setRoundNumber(testRound.getRoundNumber()+1);

			for(TestExecution t : testRound.getTestExecutions()) {
				TestExecution nt = Rt4mtFactory.eINSTANCE.createTestExecution();
				nt.setTestCase(t.getTestCase());
				r.getTestExecutions().add(nt);
			}
		}
		round.add(r);
		testRoundGroup.eResource().setModified(true);
	}

	
	
}
