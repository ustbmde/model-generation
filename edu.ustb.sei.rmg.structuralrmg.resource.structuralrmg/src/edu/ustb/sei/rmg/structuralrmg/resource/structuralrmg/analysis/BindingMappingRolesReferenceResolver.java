/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.Template;

public class BindingMappingRolesReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.BindingMapping container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<edu.ustb.sei.rmg.structuralrmg.Role> result) {
		if(container==null) return;
		if(identifier==null) return;
		
		Template template = container.getTemplate();
		
		Role r = ResolverUtil.searchRole(identifier, template);
		
		if(r!=null)
			result.addMapping(identifier, r);
	}
	
	public String deResolve(edu.ustb.sei.rmg.structuralrmg.Role element, edu.ustb.sei.rmg.structuralrmg.BindingMapping container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
