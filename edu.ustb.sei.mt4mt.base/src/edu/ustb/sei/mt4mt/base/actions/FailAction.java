package edu.ustb.sei.mt4mt.base.actions;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class FailAction implements IAction {

	public static final IAction SINGLETON = new FailAction();

	public FailAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run(Context context) {
		throw new RuntimeException("it is a FailAction");
	}

}
