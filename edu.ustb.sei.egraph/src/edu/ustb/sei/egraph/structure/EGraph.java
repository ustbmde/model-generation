package edu.ustb.sei.egraph.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import edu.ustb.sei.egraph.util.ConcurrentArrayList;

public class EGraph<NT,ET,NAT,EAT> {
	
	private int nextID = 0;
	
	private ConcurrentArrayList<ENode<NT,ET,NAT>> nodes;
	private ConcurrentArrayList<EEdge<NT,ET,EAT>> edges;
	protected ConcurrentHashMap<ENode<NT,ET,NAT>, ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>> incomingEdges;
	protected ConcurrentHashMap<ENode<NT,ET,NAT>, ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>> outgoingEdges;
	private ETypeModel<NT,ET,NAT,EAT> typeModel = null;
	private static final int defaultSize = 8192; 
	
	
	public EGraph(int initialNodeSize, int initialEdgeSize) {
		nodes = new ConcurrentArrayList<ENode<NT,ET,NAT>>(initialNodeSize);
		edges = new ConcurrentArrayList<EEdge<NT,ET,EAT>>(initialEdgeSize);
		incomingEdges = new ConcurrentHashMap<ENode<NT,ET,NAT>, ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>>();
		outgoingEdges = new ConcurrentHashMap<ENode<NT,ET,NAT>, ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>>();
	}
	
	public EGraph() {
		this(defaultSize,defaultSize);
	}
	
	public void addNode(ENode<NT,ET,NAT> node) {
		nodes.add(node);
		node.setGraph(this);
		incomingEdges.put(node, new ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>());
		outgoingEdges.put(node, new ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>());
		node.setInternalID(nextID());
	}
	
	public void addNodes(Collection<ENode<NT,ET,NAT>> nodes) {
		nodes.addAll(nodes);
		
		for(ENode<NT,ET,NAT> node : nodes) {
			node.setGraph(this);
			incomingEdges.put(node, new ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>());
			outgoingEdges.put(node, new ConcurrentHashMap<ET,Collection<EEdge<NT,ET,EAT>>>());
			node.setInternalID(nextID());
		}
	}
	
	private int nextID() {
		return nextID++;
	}

	/**
	 * This method does not assure that all the edges connected with the deleted node are already removed 
	 * @param node
	 */
	public void removeNode(ENode<NT,ET,NAT> node) {
		basicRemoveNode(node);
		incomingEdges.remove(node);
		outgoingEdges.remove(node);
	}

	protected void basicRemoveNode(ENode<NT, ET, NAT> node) {
		nodes.remove(node);
		node.setGraph(null);
	}
	
	public void removeNodeAndEdges(ENode<NT,ET,NAT> node) {
		ConcurrentHashMap<ET, Collection<EEdge<NT, ET, EAT>>> incomingEdgesForNode = incomingEdges.remove(node);
		ConcurrentHashMap<ET, Collection<EEdge<NT, ET, EAT>>> outgoingEdgesForNode = outgoingEdges.remove(node);
		
		for(Collection<EEdge<NT, ET, EAT>> list : incomingEdgesForNode.values()) {
			for(EEdge<NT, ET, EAT> e : list) {
				this.basicRemoveEdge(e);
			}
		}
		for(Collection<EEdge<NT, ET, EAT>> list : outgoingEdgesForNode.values()) {
			for(EEdge<NT, ET, EAT> e : list) {
				this.basicRemoveEdge(e);
			}
		}
		
		this.basicRemoveNode(node);
	}
	
	public Collection<EEdge<NT,ET,EAT>> getIncomings(ENode<NT,ET,?> targetNode, ET edgeType) {
		ConcurrentHashMap<ET, Collection<EEdge<NT, ET, EAT>>> incomingEdgesForNode = incomingEdges.get(targetNode);
		Collection<EEdge<NT,ET,EAT>> icol = incomingEdgesForNode.get(edgeType);
		// NOTE! icol is not supposed to be thread-safe
		if(icol==null) {
			icol = new ArrayList<EEdge<NT,ET,EAT>>();
			incomingEdgesForNode.put(edgeType,icol);
		}
		return icol;
	}
	
	public Collection<EEdge<NT,ET,EAT>> getOutgoings(ENode<NT,ET,?> sourceNode, ET edgeType) {
		ConcurrentHashMap<ET, Collection<EEdge<NT, ET, EAT>>> outgoingEdgesForNode = outgoingEdges.get(sourceNode);
		Collection<EEdge<NT,ET,EAT>> ocol = outgoingEdgesForNode.get(edgeType);
		// NOTE! icol is not supposed to be thread-safe
		if(ocol==null) {
			ocol = new ArrayList<EEdge<NT,ET,EAT>>();
			outgoingEdgesForNode.put(edgeType,ocol);
		}
		return ocol;
	}
	
	public void addEdge(EEdge<NT,ET,EAT> edge) {
		basicRemoveEdge(edge);
		
		Collection<EEdge<NT,ET,EAT>> icol = getIncomings(edge.getTarget(),edge.getType());
		icol.add(edge);
		
		Collection<EEdge<NT,ET,EAT>> ocol = getOutgoings(edge.getSource(),edge.getType());
		ocol.add(edge);
		
		
	}

	protected void basicRemoveEdge(EEdge<NT, ET, EAT> edge) {
		edges.add(edge);
		edge.setGraph(this);
	}
	
	public void removeEdge(EEdge<NT,ET,EAT> edge) {
		edges.remove(edge);
		edge.setGraph(null);
		
		Collection<EEdge<NT,ET,EAT>> icol = getIncomings(edge.getTarget(), edge.getType());
		icol.remove(edge);
		
		Collection<EEdge<NT,ET,EAT>> ocol = getOutgoings(edge.getSource(),edge.getType());
		ocol.remove(edge);
	}
	
	public Collection<ENode<NT,ET,NAT>> getExternalNodes() {
		Collection<ENode<NT,ET,NAT>> list = new ArrayList<ENode<NT,ET,NAT>>();
		this.nodes.stream().filter((n)->(n instanceof EExternalNode)).forEach(n->{list.add(n);});
		return list;
	}
	
	public Collection<ENode<NT,ET,NAT>> getNodes() {
		return this.nodes;
	}
	
	public Collection<EEdge<NT,ET,EAT>> getEdges() {
		return this.edges;
	}

	public ETypeModel<NT,ET,NAT,EAT> getTypeModel() {
		return typeModel;
	}

	public void setTypeModel(ETypeModel<NT,ET,NAT,EAT> typeModel) {
		this.typeModel = typeModel;
	}
}
