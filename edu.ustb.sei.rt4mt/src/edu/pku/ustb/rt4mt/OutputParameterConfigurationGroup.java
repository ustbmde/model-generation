/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Parameter Configuration Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getOutputParameterConfigurationGroup()
 * @model
 * @generated
 */
public interface OutputParameterConfigurationGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.OutputParameterConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getOutputParameterConfigurationGroup_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutputParameterConfiguration> getParameters();

} // OutputParameterConfigurationGroup
