/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.TestCase;
import edu.ustb.sei.mde.mt4mt.TestSuite;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Suite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl#getTestCases <em>Test Cases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.TestSuiteImpl#getDefaultWaitingTime <em>Default Waiting Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestSuiteImpl extends MinimalEObjectImpl.Container implements TestSuite {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTestCases() <em>Test Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestCases()
	 * @generated
	 * @ordered
	 */
	protected EList<TestCase> testCases;

	/**
	 * The default value of the '{@link #getDefaultWaitingTime() <em>Default Waiting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultWaitingTime()
	 * @generated
	 * @ordered
	 */
	protected static final long DEFAULT_WAITING_TIME_EDEFAULT = 10000L;

	/**
	 * The cached value of the '{@link #getDefaultWaitingTime() <em>Default Waiting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultWaitingTime()
	 * @generated
	 * @ordered
	 */
	protected long defaultWaitingTime = DEFAULT_WAITING_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestSuiteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.TEST_SUITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.TEST_SUITE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestCase> getTestCases() {
		if (testCases == null) {
			testCases = new EObjectContainmentEList<TestCase>(TestCase.class, this, MT4MTPackage.TEST_SUITE__TEST_CASES);
		}
		return testCases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDefaultWaitingTime() {
		return defaultWaitingTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultWaitingTime(long newDefaultWaitingTime) {
		long oldDefaultWaitingTime = defaultWaitingTime;
		defaultWaitingTime = newDefaultWaitingTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.TEST_SUITE__DEFAULT_WAITING_TIME, oldDefaultWaitingTime, defaultWaitingTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MT4MTPackage.TEST_SUITE__TEST_CASES:
				return ((InternalEList<?>)getTestCases()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.TEST_SUITE__NAME:
				return getName();
			case MT4MTPackage.TEST_SUITE__TEST_CASES:
				return getTestCases();
			case MT4MTPackage.TEST_SUITE__DEFAULT_WAITING_TIME:
				return getDefaultWaitingTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.TEST_SUITE__NAME:
				setName((String)newValue);
				return;
			case MT4MTPackage.TEST_SUITE__TEST_CASES:
				getTestCases().clear();
				getTestCases().addAll((Collection<? extends TestCase>)newValue);
				return;
			case MT4MTPackage.TEST_SUITE__DEFAULT_WAITING_TIME:
				setDefaultWaitingTime((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.TEST_SUITE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MT4MTPackage.TEST_SUITE__TEST_CASES:
				getTestCases().clear();
				return;
			case MT4MTPackage.TEST_SUITE__DEFAULT_WAITING_TIME:
				setDefaultWaitingTime(DEFAULT_WAITING_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.TEST_SUITE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MT4MTPackage.TEST_SUITE__TEST_CASES:
				return testCases != null && !testCases.isEmpty();
			case MT4MTPackage.TEST_SUITE__DEFAULT_WAITING_TIME:
				return defaultWaitingTime != DEFAULT_WAITING_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", defaultWaitingTime: ");
		result.append(defaultWaitingTime);
		result.append(')');
		return result.toString();
	}

} //TestSuiteImpl
