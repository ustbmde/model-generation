package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomFromSetFunction<T> extends AbstractRandomFunction<T> {
	private AbstractRandomFunction<T>[] functions;
	private double totalSetWeight;
	
	public RandomFromSetFunction(AbstractRandomFunction<T>[] functions) {
		this.functions = functions;
		totalSetWeight = 0;
		
		for(AbstractRandomFunction<T> f : functions) {
			totalSetWeight += f.getWeight();
		}
	}

	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		double pick = t.randomDouble(0.0, totalSetWeight);
		double sum = 0;
		
		for(AbstractRandomFunction<T> f : functions) {
			if(sum <= pick && pick < f.getWeight()+sum) {
				return f.getValue(t);
			}
			sum += f.getWeight();
		}
		
		throw new RuntimeException("cannot pick a function from the function sets");
	}

}
