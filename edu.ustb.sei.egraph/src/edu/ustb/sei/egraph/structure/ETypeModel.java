package edu.ustb.sei.egraph.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.egraph.generator.util.QualifiedName;

public interface ETypeModel<NT,ET,NAT,EAT> {
	// basic additive operations
	public void addNodeType(NT n);
	public void addEdgeType(ET e);
	public void addNodeAttrType(NT n, NAT a);
	public void addEdgeAttrType(ET e, EAT a);
	
	public void addParent(NT child, NT parent);
	
	/**
	 * return all upstream (super) types including seeds
	 * @param seeds
	 * @return
	 */
	default public Collection<NT> collectAllSuperTypes(Collection<NT> seeds) {
		Collection<NT> parents = new HashSet<NT>();
		for(NT s : seeds) {
			Collection<NT> spar =  collectAllSuperTypes(s);
			parents.addAll(spar);
		}
		return parents;
	}
	public Collection<NT> collectAllSuperTypes(NT seed);
	
	/**
	 * return all concrete downstream (sub) types including seeds
	 * @param seeds
	 * @return
	 */
	default public Collection<NT> collectAllConcreteSubTypes(Collection<NT> seeds) {
		Collection<NT> children = new HashSet<NT>();
		for(NT s : seeds) {
			Collection<NT> schd = collectAllConcreteSubTypes(s);
			children.addAll(schd);
		}
		return children;
	}
	public Collection<NT> collectAllConcreteSubTypes(NT seed);
	
	
	public Collection<NT> allNodeTypes();
	public Collection<ET> allEdgeTypes();
	
	
	default public boolean isSuperNodeType(NT child, NT parent) {
		if(child==null) return false;
		if(parent==null) return true;
		if(child==parent) return true;
		return collectAllSuperTypes(Collections.singleton(child)).contains(parent);
	}
	
	/**
	 * return null means any type
	 * @param e
	 * @return
	 */
	default public NT getSource(ET e) {
		return null;
	}
	
	/**
	 * return null means any type
	 * @param e
	 * @return
	 */
	default public NT getTarget(ET e){
		return null;
	}
	default public int getSourceLowerBound(ET e){
		return 0;
	}
	default public int getTargetLowerBound(ET e){
		return 0;
	}
	default public int getSourceUpperBound(ET e) {
		return Integer.MAX_VALUE;
	}
	default public int getTargetUpperBound(ET e) {
		return Integer.MAX_VALUE;
	}
	public boolean isAbstract(NT t);
	
	default public List<ET> getContainerReferenceForClass(NT t) {
		List<ET> allContainments = this.collectAllContainmentReferences();
		
		List<ET> containers = new ArrayList<ET>();
		
		for(ET e : allContainments) {
			NT tar = this.getTarget(e);
			if(this.isSuperNodeType(t, tar))
				containers.add(e);
		}
		return containers;
	}
	
	public List<ET> collectAllContainmentReferences();
	
	default public ENode<NT,ET,NAT> createNode(NT type) {
		return new ENode<NT,ET,NAT>(type);
	}
	
	default public ENode<NT, ET, NAT> createExternalNode(NT type, Object uri) {
		return new EExternalNode<NT,ET,NAT>(type,uri);
	}
	
	default public EEdge<NT,ET,EAT> createEdge(ET type,ENode<NT,ET,NAT> s, ENode<NT,ET,NAT> t) {
		return new EEdge<NT,ET,EAT>(type,s,t);
	}
	
	public boolean hasCommonSubTypes(NT t1, NT t2);
	
	public Collection<NAT> getAttributesOfNodeType(NT type);
	default public Collection<NAT> getAllAttributesOfNodeType(NT type) {
		Set<NAT> attributes = new HashSet<NAT>();
		this.collectAllSuperTypes(type).forEach(st->attributes.addAll(this.getAttributesOfNodeType(st)));
		return attributes;
	}
	public Collection<EAT> getAttributesOfEdgeType(ET type);
	
	public Collection<ET> getOutgoingEdgeTypesOfNodeType(NT source);
	default public Collection<ET> getAllOutgoingEdgeTypesOfNodeType(NT source) {
		Set<ET> all = new HashSet<ET>();
		this.collectAllSuperTypes(source).forEach(t->{all.addAll(this.getOutgoingEdgeTypesOfNodeType(t));});
		return all;
	}
	
	
	public Collection<ET> getIncomingEdgeTypesOfNodeType(NT target);
	default public Collection<ET> getAllIncomingEdgeTypesOfNodeType(NT target)  {
		Set<ET> all = new HashSet<ET>();
		this.collectAllSuperTypes(target).forEach(t->{all.addAll(this.getIncomingEdgeTypesOfNodeType(t));});
		return all;
	}
	
	
	// name resolving
	NT resolveNodeType(QualifiedName name);
	QualifiedName desolveNodeType(NT type);
	ET resolveEdgeType(QualifiedName name);
	QualifiedName desolveEdgeType(ET type);
	NAT resolveNodeAttribute(QualifiedName name);
	QualifiedName desolveNodeAttribute(NAT attr);
	default EAT resolveEdgeAttribute(QualifiedName name) {return null;}
	default QualifiedName desolveEdgeAttribute(EAT attr) {return null;}
	
}
