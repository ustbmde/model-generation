/**
 */
package edu.ustb.sei.rmg.structuralrmg.impl;

import edu.ustb.sei.rmg.structuralrmg.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructuralRmgFactoryImpl extends EFactoryImpl implements StructuralRmgFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StructuralRmgFactory init() {
		try {
			StructuralRmgFactory theStructuralRmgFactory = (StructuralRmgFactory)EPackage.Registry.INSTANCE.getEFactory(StructuralRmgPackage.eNS_URI);
			if (theStructuralRmgFactory != null) {
				return theStructuralRmgFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StructuralRmgFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralRmgFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StructuralRmgPackage.TEMPLATE_MODEL: return createTemplateModel();
			case StructuralRmgPackage.TEMPLATE: return createTemplate();
			case StructuralRmgPackage.BOUND: return createBound();
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE: return createPrimitiveTemplateEdge();
			case StructuralRmgPackage.CHAIN_TEMPLATE_EDGE: return createChainTemplateEdge();
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE: return createPrimitiveTemplateNode();
			case StructuralRmgPackage.ATTRIBUTE_BINDING: return createAttributeBinding();
			case StructuralRmgPackage.NESTED_TEMPLATE_NODE: return createNestedTemplateNode();
			case StructuralRmgPackage.ROLE: return createRole();
			case StructuralRmgPackage.BINDING_MAPPING: return createBindingMapping();
			case StructuralRmgPackage.ENDPOINT_BINDING: return createEndpointBinding();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case StructuralRmgPackage.COMPOSITION_SORT:
				return createCompositionSortFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case StructuralRmgPackage.COMPOSITION_SORT:
				return convertCompositionSortToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateModel createTemplateModel() {
		TemplateModelImpl templateModel = new TemplateModelImpl();
		return templateModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template createTemplate() {
		TemplateImpl template = new TemplateImpl();
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bound createBound() {
		BoundImpl bound = new BoundImpl();
		return bound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveTemplateEdge createPrimitiveTemplateEdge() {
		PrimitiveTemplateEdgeImpl primitiveTemplateEdge = new PrimitiveTemplateEdgeImpl();
		return primitiveTemplateEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChainTemplateEdge createChainTemplateEdge() {
		ChainTemplateEdgeImpl chainTemplateEdge = new ChainTemplateEdgeImpl();
		return chainTemplateEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveTemplateNode createPrimitiveTemplateNode() {
		PrimitiveTemplateNodeImpl primitiveTemplateNode = new PrimitiveTemplateNodeImpl();
		return primitiveTemplateNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeBinding createAttributeBinding() {
		AttributeBindingImpl attributeBinding = new AttributeBindingImpl();
		return attributeBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NestedTemplateNode createNestedTemplateNode() {
		NestedTemplateNodeImpl nestedTemplateNode = new NestedTemplateNodeImpl();
		return nestedTemplateNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BindingMapping createBindingMapping() {
		BindingMappingImpl bindingMapping = new BindingMappingImpl();
		return bindingMapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndpointBinding createEndpointBinding() {
		EndpointBindingImpl endpointBinding = new EndpointBindingImpl();
		return endpointBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositionSort createCompositionSortFromString(EDataType eDataType, String initialValue) {
		CompositionSort result = CompositionSort.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCompositionSortToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralRmgPackage getStructuralRmgPackage() {
		return (StructuralRmgPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StructuralRmgPackage getPackage() {
		return StructuralRmgPackage.eINSTANCE;
	}

} //StructuralRmgFactoryImpl
