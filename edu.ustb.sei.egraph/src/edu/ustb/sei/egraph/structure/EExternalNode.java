package edu.ustb.sei.egraph.structure;

public class EExternalNode<NT, ET, AT> extends ENode<NT, ET, AT> {

	public EExternalNode(NT type, Object uri) {
		super(type);
		this.uri = uri;
	}
	
	private Object uri;
	
	@SuppressWarnings("unchecked")
	public <T> T getURI() {
		return (T) uri;
	}

}
