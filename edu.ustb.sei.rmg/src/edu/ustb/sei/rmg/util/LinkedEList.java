package edu.ustb.sei.rmg.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.common.util.EList;

public class LinkedEList<E> implements EList<E> {
	
	private class Node {
		public E content = null;
		public Node next = null;
	}
	
	private Node first;
	private Node last;
	private int size;
	
	public LinkedEList() {
		size = 0;
		first = null;
		last = null;
	}

	@Override
	public boolean add(E e) {
		Node n = new Node();
		n.content = e;
		
		if(first==null) {
			first = n;
			last = n;
		} else {
			last.next = n;
			last = n;
		}
		
		size++;
		
		return true;
	}

	@Override
	public void add(int index, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		if(c.size()==0) return false;
		if(c instanceof LinkedEList) {
			LinkedEList<E> linkedEList = (LinkedEList<E>) c;
			if(first==null) {
				first = linkedEList.first;
				last = linkedEList.last;
			} else {
				last.next = linkedEList.first;
				last = linkedEList.last;
			}
			
			size = size + c.size();
		} else {
			throw new UnsupportedOperationException();
		}
		return true;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E get(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}

	@Override
	public Iterator<E> iterator() {
		return new LinkedEListIterator(this);
	}
	
	class LinkedEListIterator implements Iterator<E> {
		public LinkedEListIterator(LinkedEList<E> host) {
			this.host = host;
			current = host.first;
		}
		public LinkedEList<E> host;
		public Node current = null;

		@Override
		public boolean hasNext() {
			return (current!=last);
		}

		@Override
		public E next() {
			E c = current.content;
			current = current.next;
			return c;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<E> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E set(int index, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void move(int newPosition, E object) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E move(int newPosition, int oldPosition) {
		throw new UnsupportedOperationException();
	}
	

}
