/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * The StructuralrmgTokenResolverFactory class provides access to all generated
 * token resolvers. By giving the name of a defined token, the corresponding
 * resolve can be obtained. Despite the fact that this class is called
 * TokenResolverFactory is does NOT create new token resolvers whenever a client
 * calls methods to obtain a resolver. Rather, this class maintains a map of all
 * resolvers and creates each resolver at most once.
 */
public class StructuralrmgTokenResolverFactory implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolverFactory {
	
	private java.util.Map<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver> featureName2CollectInTokenResolver;
	private static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver defaultResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultTokenResolver();
	
	public StructuralrmgTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver>();
		registerTokenResolver("TEXT", new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgTEXTTokenResolver());
		registerTokenResolver("URI", new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgURITokenResolver());
		registerTokenResolver("PATH", new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgPATHTokenResolver());
		registerTokenResolver("NUMBER", new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgNUMBERTokenResolver());
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver internalCreateResolver(java.util.Map<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver> resolverMap, String key, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
