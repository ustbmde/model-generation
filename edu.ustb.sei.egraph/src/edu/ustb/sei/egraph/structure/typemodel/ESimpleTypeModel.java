package edu.ustb.sei.egraph.structure.typemodel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.structure.ETypeModel;

public class ESimpleTypeModel<NT, ET, NAT, EAT> implements ETypeModel<NT, ET, NAT, EAT> {
	
	private Set<NT> nodeTypes;
	private Set<ET> edgeTypes;
	private Map<NT,Set<NAT>> nodeAttrTypes;
	private Map<ET,Set<EAT>> edgeAttrTypes;
	private Map<NT,Set<NT>> parentMap;
	
	private Map<NT,Collection<NT>> upstreamNodeTypes;
	private Map<NT,Collection<NT>> downstreamNodeTypes;
	
	private Map<NT,Map<NT,Boolean>> hasCommonSubType;
	
	public ESimpleTypeModel() {
		this.nodeTypes = new HashSet<NT>();
		this.edgeTypes = new HashSet<ET>();
		this.nodeAttrTypes = new HashMap<NT,Set<NAT>>();
		this.edgeAttrTypes = new HashMap<ET,Set<EAT>>();
		
		this.parentMap = new HashMap<NT,Set<NT>>();
		
		this.upstreamNodeTypes = new HashMap<NT,Collection<NT>>();
		this.downstreamNodeTypes = new HashMap<NT,Collection<NT>>();
		
		this.hasCommonSubType = new HashMap<NT, Map<NT,Boolean>>();
	}

	@Override
	public void addNodeType(NT n) {
		nodeTypes.add(n);
	}

	@Override
	public void addEdgeType(ET e) {
		edgeTypes.add(e);
	}

	@Override
	public void addNodeAttrType(NT n, NAT a) {
		Set<NAT> attrs = this.nodeAttrTypes.get(n);
		
		if(attrs==null) {
			attrs = new HashSet<NAT>();
			this.nodeAttrTypes.put(n, attrs);
		}
		
		attrs.add(a);
	}

	@Override
	public void addEdgeAttrType(ET e, EAT a) {
		Set<EAT> attrs = this.edgeAttrTypes.get(e);
		
		if(attrs==null) {
			attrs = new HashSet<EAT>();
			this.edgeAttrTypes.put(e, attrs);
		}
		
		attrs.add(a);
	}

	@Override
	public void addParent(NT child, NT parent) {
		Set<NT> parents = parentMap.get(child);
		if(parents==null)
			parents = new HashSet<NT>();
		parents.add(parent);
		
	}
	
	public Collection<NT> collectAllSuperTypes(NT seed) {
		if(seed==null)
			return Collections.emptyList();
		
		Collection<NT> allPars = this.upstreamNodeTypes.get(seed);
		
		if(allPars!=null) 
			return allPars;
		else {
			allPars = new HashSet<NT>();
			allPars.add(seed); // contain self
			Collection<NT> pars = this.parentMap.get(seed);
			if(pars!=null) {
				for(NT p : pars) {
					allPars.addAll(collectAllSuperTypes(p));
				}
			}
			this.upstreamNodeTypes.put(seed, allPars);
			return allPars;
		}
	}

	public Collection<NT> collectAllConcreteSubTypes(NT seed) {
		if(seed==null) return this.nodeTypes;
		
		Collection<NT> allChd = this.downstreamNodeTypes.get(seed);
		if(allChd!=null)
			return allChd;
		else {
			allChd = new HashSet<NT>();
			
			if(this.isAbstract(seed)==false) // filter out abstract types
				allChd.add(seed);
			
			for(NT n : this.nodeTypes) {
				if(this.isAbstract(n)) continue;
				Collection<NT> pars = this.collectAllSuperTypes(n);
				if(pars.contains(seed)) {
					allChd.add(n);
				}
			}
			this.downstreamNodeTypes.put(seed, allChd);
			return allChd;
		}
	}

	@Override
	public Collection<NT> allNodeTypes() {
		return nodeTypes;
	}

	@Override
	public Collection<ET> allEdgeTypes() {
		return edgeTypes;
	}

	@Override
	public boolean isAbstract(NT t) {
		return false;
	}

	@Override
	public List<ET> getContainerReferenceForClass(NT t) {
		return Collections.emptyList();
	}

	@Override
	public List<ET> collectAllContainmentReferences() {
		return Collections.emptyList();
	}

	@Override
	public boolean hasCommonSubTypes(NT t1, NT t2) {
		Map<NT,Boolean> map = this.hasCommonSubType.get(t1);
		Boolean flag = null;
		if(map==null || (flag=map.get(t2))==null) {
			flag = computeHasCommonSubTypes(t1,t2);
		}
		return flag;
	}
	
	private boolean computeHasCommonSubTypes(NT t1, NT t2) {
		Map<NT,Boolean> map1 = this.hasCommonSubType.get(t1);
		Map<NT,Boolean> map2 = this.hasCommonSubType.get(t1);
		
		if(map1==null) {
			map1 = new HashMap<NT, Boolean>();
			this.hasCommonSubType.put(t1, map1);
		}
		
		if(map2==null) {
			map2 = new HashMap<NT, Boolean>();
			this.hasCommonSubType.put(t2, map2);
		}
		
		Collection<NT> ts1 = this.collectAllConcreteSubTypes(t1);
		Collection<NT> ts2 = this.collectAllConcreteSubTypes(t2);
		Boolean flag = !Collections.disjoint(ts1, ts2);
		map1.put(t2, flag);
		map2.put(t1, flag);
		
		return flag;
	}

	@Override
	public Collection<NAT> getAttributesOfNodeType(NT type) {
		return nodeAttrTypes.get(type);
	}

	@Override
	public Collection<EAT> getAttributesOfEdgeType(ET type) {
		return this.edgeAttrTypes.get(type);
	}
	
	@Override
	public Collection<ET> getIncomingEdgeTypesOfNodeType(NT target) {
		return edgeTypes;
	}
	
	@Override
	public Collection<ET> getOutgoingEdgeTypesOfNodeType(NT source) {
		return edgeTypes;
	}
	
	@Override
	public NT resolveNodeType(QualifiedName name) {
		return this.nodeTypeResolver.resolve(name);
	}
	
	@Override
	public QualifiedName desolveNodeType(NT type) {
		return this.nodeTypeDesolver.desolve(type);
	}
	
	@Override
	public ET resolveEdgeType(QualifiedName name) {
		return this.edgeTypeResolver.resolve(name);
	}
	
	@Override
	public QualifiedName desolveEdgeType(ET type) {
		return this.edgeTypeDesolver.desolve(type);
	}
	
	@Override
	public NAT resolveNodeAttribute(QualifiedName name) {
		return this.nodeAttributeResolver.resolve(name);
	}
	
	@Override
	public QualifiedName desolveNodeAttribute(NAT type) {
		return this.nodeAttributeDesolver.desolve(type);
	}
	
	@Override
	public EAT resolveEdgeAttribute(QualifiedName name) {
		return this.edgeAttributeResolver.resolve(name);
	}
	
	@Override
	public QualifiedName desolveEdgeAttribute(EAT type) {
		return this.edgeAttributeDesolver.desolve(type);
	}
	
	
	public void configureNodeTypeResolving(Resolver<NT> r, Desolver<NT> d) {
		this.nodeTypeResolver = r;
		this.nodeTypeDesolver = d;
	}
	
	public void configureEdgeTypeResolving(Resolver<ET> r, Desolver<ET> d) {
		this.edgeTypeResolver = r;
		this.edgeTypeDesolver = d;
	}
	
	public void configureNodeAttributeResolving(Resolver<NAT> r, Desolver<NAT> d) {
		this.nodeAttributeResolver = r;
		this.nodeAttributeDesolver = d;
	}
	
	public void configureEdgeAttributeResolving(Resolver<EAT> r, Desolver<EAT> d) {
		this.edgeAttributeResolver = r;
		this.edgeAttributeDesolver = d;
	}
	
	private Resolver<NT> nodeTypeResolver;
	private Resolver<ET> edgeTypeResolver;
	private Resolver<NAT> nodeAttributeResolver;
	private Resolver<EAT>edgeAttributeResolver;
	
	private Desolver<NT> nodeTypeDesolver;
	private Desolver<ET> edgeTypeDesolver;
	private Desolver<NAT> nodeAttributeDesolver;
	private Desolver<EAT>edgeAttributeDesolver;

	@FunctionalInterface
	interface Resolver<T> {
		T resolve(QualifiedName name);
	}
	
	@FunctionalInterface
	interface Desolver<T> {
		QualifiedName desolve(T elem);
	}
	
}
