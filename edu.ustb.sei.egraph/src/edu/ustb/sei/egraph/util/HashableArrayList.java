package edu.ustb.sei.egraph.util;

import java.util.ArrayList;
import java.util.Collection;


public class HashableArrayList<E> extends ArrayList<E> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4437221151337890341L;
	private Object hashKey = new Object[]{};
	
	public HashableArrayList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HashableArrayList(Collection<? extends E> collection) {
		super(collection);
		// TODO Auto-generated constructor stub
	}

	public HashableArrayList(int initialCapacity) {
		super(initialCapacity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		return hashKey.hashCode();
	}
}
