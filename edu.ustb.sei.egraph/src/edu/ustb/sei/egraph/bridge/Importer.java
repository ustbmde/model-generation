package edu.ustb.sei.egraph.bridge;

import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ETypeModel;

public interface Importer<T,NT,ET,NAT,EAT> {
	EGraph<NT,ET,NAT,EAT> importGraph(T datasource, ETypeModel<NT,ET,NAT,EAT> typemodel);
}
