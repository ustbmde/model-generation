/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.TestCase#getId <em>Id</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.TestCase#getInputs <em>Inputs</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.TestCase#getExpectedOutputs <em>Expected Outputs</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCase()
 * @model
 * @generated
 */
public interface TestCase extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCase_Id()
	 * @model
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.TestCase#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Inputs</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.TestData}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCase_Inputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestData> getInputs();

	/**
	 * Returns the value of the '<em><b>Expected Outputs</b></em>' containment reference list.
	 * The list contents are of type {@link edu.pku.ustb.rt4mt.ExpectedOutput}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Outputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Outputs</em>' containment reference list.
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestCase_ExpectedOutputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExpectedOutput> getExpectedOutputs();

} // TestCase
