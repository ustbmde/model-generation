/**
 */
package edu.pku.ustb.rt4mt.util;

import edu.pku.ustb.rt4mt.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage
 * @generated
 */
public class Rt4mtAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Rt4mtPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rt4mtAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Rt4mtPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Rt4mtSwitch<Adapter> modelSwitch =
		new Rt4mtSwitch<Adapter>() {
			@Override
			public Adapter caseRegressionTestModel(RegressionTestModel object) {
				return createRegressionTestModelAdapter();
			}
			@Override
			public Adapter caseTestCase(TestCase object) {
				return createTestCaseAdapter();
			}
			@Override
			public Adapter caseTestRound(TestRound object) {
				return createTestRoundAdapter();
			}
			@Override
			public Adapter caseTestExecution(TestExecution object) {
				return createTestExecutionAdapter();
			}
			@Override
			public Adapter caseTestCaseGroup(TestCaseGroup object) {
				return createTestCaseGroupAdapter();
			}
			@Override
			public Adapter caseTestRoundGroup(TestRoundGroup object) {
				return createTestRoundGroupAdapter();
			}
			@Override
			public Adapter caseInputParameterConfigurationGroup(InputParameterConfigurationGroup object) {
				return createInputParameterConfigurationGroupAdapter();
			}
			@Override
			public Adapter caseOutputParameterConfigurationGroup(OutputParameterConfigurationGroup object) {
				return createOutputParameterConfigurationGroupAdapter();
			}
			@Override
			public Adapter caseParameterConfiguration(ParameterConfiguration object) {
				return createParameterConfigurationAdapter();
			}
			@Override
			public Adapter caseInputParameterConfiguration(InputParameterConfiguration object) {
				return createInputParameterConfigurationAdapter();
			}
			@Override
			public Adapter caseOutputParameterConfiguration(OutputParameterConfiguration object) {
				return createOutputParameterConfigurationAdapter();
			}
			@Override
			public Adapter caseTestData(TestData object) {
				return createTestDataAdapter();
			}
			@Override
			public Adapter caseExpectedOutput(ExpectedOutput object) {
				return createExpectedOutputAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.RegressionTestModel <em>Regression Test Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.RegressionTestModel
	 * @generated
	 */
	public Adapter createRegressionTestModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestCase
	 * @generated
	 */
	public Adapter createTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestRound <em>Test Round</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestRound
	 * @generated
	 */
	public Adapter createTestRoundAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestExecution <em>Test Execution</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestExecution
	 * @generated
	 */
	public Adapter createTestExecutionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestCaseGroup <em>Test Case Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestCaseGroup
	 * @generated
	 */
	public Adapter createTestCaseGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestRoundGroup <em>Test Round Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestRoundGroup
	 * @generated
	 */
	public Adapter createTestRoundGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.InputParameterConfigurationGroup <em>Input Parameter Configuration Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfigurationGroup
	 * @generated
	 */
	public Adapter createInputParameterConfigurationGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup <em>Output Parameter Configuration Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup
	 * @generated
	 */
	public Adapter createOutputParameterConfigurationGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.ParameterConfiguration <em>Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.ParameterConfiguration
	 * @generated
	 */
	public Adapter createParameterConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.InputParameterConfiguration <em>Input Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.InputParameterConfiguration
	 * @generated
	 */
	public Adapter createInputParameterConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.OutputParameterConfiguration <em>Output Parameter Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.OutputParameterConfiguration
	 * @generated
	 */
	public Adapter createOutputParameterConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.TestData <em>Test Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.TestData
	 * @generated
	 */
	public Adapter createTestDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.pku.ustb.rt4mt.ExpectedOutput <em>Expected Output</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.pku.ustb.rt4mt.ExpectedOutput
	 * @generated
	 */
	public Adapter createExpectedOutputAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Rt4mtAdapterFactory
