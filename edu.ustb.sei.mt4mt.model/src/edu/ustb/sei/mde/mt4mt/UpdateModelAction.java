/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Update Model Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction#getModelKey <em>Model Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction#getUpdateOperations <em>Update Operations</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getUpdateModelAction()
 * @model
 * @generated
 */
public interface UpdateModelAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Key</em>' attribute.
	 * @see #setModelKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getUpdateModelAction_ModelKey()
	 * @model
	 * @generated
	 */
	String getModelKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.UpdateModelAction#getModelKey <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Key</em>' attribute.
	 * @see #getModelKey()
	 * @generated
	 */
	void setModelKey(String value);

	/**
	 * Returns the value of the '<em><b>Update Operations</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update Operations</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update Operations</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getUpdateModelAction_UpdateOperations()
	 * @model
	 * @generated
	 */
	EList<String> getUpdateOperations();

} // UpdateModelAction
