/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A basic implementation of the
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceRes
 * olveResult interface that collects mappings in a list.
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class StructuralrmgReferenceResolveResult<ReferenceType> implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<ReferenceType> {
	
	private java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private java.util.Set<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> quickFixes;
	
	public StructuralrmgReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix>();
		}
		return java.util.Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
