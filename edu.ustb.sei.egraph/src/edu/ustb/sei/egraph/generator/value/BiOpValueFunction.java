package edu.ustb.sei.egraph.generator.value;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiFunction;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class BiOpValueFunction<T> extends RandomValueFunction<T> {
	
	public static BiOpValueFunction<Integer> createIntMul(AbstractRandomFunction<Integer>[] operands) {
		return new BiOpValueFunction<Integer>(
				operands,
				1,
				(l,r)->{return l*r;}
				);
	}
	
	public static BiOpValueFunction<Long> createLongMul(AbstractRandomFunction<Long>[] operands) {
		return new BiOpValueFunction<Long>(
				operands,
				1L,
				(l,r)->{return l*r;}
				);
	}
	
	public static BiOpValueFunction<Double> createRealMul(AbstractRandomFunction<Double>[] operands) {
		return new BiOpValueFunction<Double>(
				operands,
				1.0,
				(l,r)->{return l*r;}
				);
	}
	
	public static BiOpValueFunction<Integer> createIntAdd(AbstractRandomFunction<Integer>[] operands) {
		return new BiOpValueFunction<Integer>(
				operands,
				0,
				(l,r)->{return l+r;}
				);
	}
	
	public static BiOpValueFunction<Long> createLongAdd(AbstractRandomFunction<Long>[] operands) {
		return new BiOpValueFunction<Long>(
				operands,
				0L,
				(l,r)->{return l+r;}
				);
	}
	
	public static BiOpValueFunction<Double> createRealAdd(AbstractRandomFunction<Double>[] operands) {
		return new BiOpValueFunction<Double>(
				operands,
				0.0,
				(l,r)->{return l+r;}
				);
	}
	
	public static BiOpValueFunction<String> createStringConc(AbstractRandomFunction<?>[] operands) {
		return new BiOpValueFunction<String>(
				operands,
				"",
				(l,r)->{return l.toString()+r.toString();}
				);
	}
	
	

	private BiOpValueFunction(AbstractRandomFunction<?>[] operands, T identity, BiFunction<T, T, T> op) {
		super();
		this.operands = operands;
		this.identity = identity;
		this.operator = op;
	}

	private AbstractRandomFunction<?>[] operands;

	@SuppressWarnings("unchecked")
	@Override
	public T getValue(RandomService t) {
		T result = identity;
		for(AbstractRandomFunction<?> o : operands) {
			result = operator.apply(result, (T) o.getValue(t).getValue(t));
		}
		return result;
	}
	
	private T identity;
	private BiFunction<T, T, T> operator;
	
	private Set<String> provided = null;
	@Override
	public Set<String> providedScopes() {
		if(provided==null) {
			provided = new HashSet<String>();
			for(AbstractRandomFunction<?> f : operands) 
				provided.addAll(f.providedScopes());
		}
		return provided;
	}
	
	private Set<String> required = null;
	@Override
	public Set<String> requiredScopes() {
		if(required==null) {
			required = new HashSet<String>();
			for(AbstractRandomFunction<?> f : operands) 
				required.addAll(f.requiredScopes());
		}
		return required;
	}

}
