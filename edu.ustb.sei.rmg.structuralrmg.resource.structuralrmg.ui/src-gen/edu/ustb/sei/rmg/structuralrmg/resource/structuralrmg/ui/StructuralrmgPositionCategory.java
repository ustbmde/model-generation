/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * An enumeration of all position categories.
 */
public enum StructuralrmgPositionCategory {
	BRACKET, DEFINTION, PROXY;
}
