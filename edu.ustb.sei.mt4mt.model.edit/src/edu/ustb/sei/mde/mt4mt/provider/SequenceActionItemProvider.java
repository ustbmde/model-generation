/**
 */
package edu.ustb.sei.mde.mt4mt.provider;


import edu.ustb.sei.mde.mt4mt.MT4MTFactory;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;

import edu.ustb.sei.mde.mt4mt.SequenceAction;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mt4mt.SequenceAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SequenceActionItemProvider extends TestActionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SequenceAction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SequenceAction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_SequenceAction_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SequenceAction.class)) {
			case MT4MTPackage.SEQUENCE_ACTION__ACTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createLoadModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createSaveModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createExecuteLauncherAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createEvaluateOCLQueryAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createCopyModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createCompareModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createAssertComparisonResultAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createAssertOCLInvariantAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createUserAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createSetVariableAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createUpdateModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createClearAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.SEQUENCE_ACTION__ACTIONS,
				 MT4MTFactory.eINSTANCE.createExternalAction()));
	}

}
