package edu.ustb.sei.rmg;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.util.HashableBasicEList;
import edu.ustb.sei.rmg.util.OrderedPairMap;
import edu.ustb.sei.rmg.util.OrderedTripleMap;

public class ReferencePartition {
	public EList<EList<EReference>> splitRefereneGroup(List<EReference> refs,
			boolean sourceUnique, boolean sourceRequired,
			boolean targetUnique, boolean targetRequired, 
			boolean ringAllowed, boolean circleAllowed, boolean transitive) {
		EList<EList<EReference>> list = new HashableBasicEList<EList<EReference>>();
		for(EReference r : refs) {
			HashableBasicEList<EReference> sl = new HashableBasicEList<EReference>();
			sl.add(r);
			list.add(sl);
		}
		
		int currentBase = 0;
		while(true) {
			if(currentBase==list.size())
				break;
			int currentID = 0;
			while(true){
				if(currentID==currentBase) {
					currentID++;
					continue;
				}
				if(currentID==list.size()) break;
				
				List<EReference> rsa = list.get(currentBase);
				List<EReference> rsb = list.get(currentID);
				
				if(isDependent(refs, rsa, rsb, sourceUnique, sourceRequired, targetUnique, targetRequired, ringAllowed, circleAllowed, transitive)) {
					rsa.addAll(rsb);
					list.remove(rsb);
					
					if(currentBase>currentID) currentBase--;
				} else 
					currentID ++;
			}
			currentBase++;
		}
		
		return list;
	}
	
	
	private OrderedTripleMap<List<EReference>, EReference, EReference,Boolean> dependentMap = new OrderedTripleMap<List<EReference>, EReference, EReference,Boolean>();

	protected boolean isDependent(List<EReference> refs,EReference ra, EReference rb,
			boolean sourceUnique, boolean sourceRequired,
			boolean targetUnique, boolean targetRequired, 
			boolean ringAllowed, boolean circleAllowed, boolean transitive){
			Boolean bv = dependentMap.get(refs, ra, rb);
			if(bv==null) {
				bv = isDependentCalc(refs,ra,rb,sourceUnique,sourceRequired,targetUnique,targetRequired,ringAllowed,circleAllowed,transitive);
				dependentMap.put(refs, ra, rb, bv);
			}
			return bv;
		}
	protected boolean isDependentCalc(List<EReference> refs,EReference ra, EReference rb,
			boolean sourceUnique, boolean sourceRequired,
			boolean targetUnique, boolean targetRequired, 
			boolean ringAllowed, boolean circleAllowed, boolean transitive){
		if(sourceUnique || sourceRequired) {
			if(hasCommonElements(ra.getEReferenceType(),rb.getEReferenceType()))
				return true;
		}
		if(targetUnique || targetRequired) {
			if(hasCommonElements(ra.getEContainingClass(),rb.getEContainingClass()))
				return true;
		}
				
		if(!circleAllowed) {
			if(hasCircle(refs,ra,rb,transitive))
				return true;
		}
		
		return false;
	}
	
	protected boolean isDependent(List<EReference> allRefs, List<EReference> rsa, List<EReference> rsb,
			boolean sourceUnique, boolean sourceRequired,
			boolean targetUnique, boolean targetRequired, 
			boolean ringAllowed, boolean circleAllowed, boolean transitive) {
		for(EReference ra : rsa) {
			for(EReference rb : rsb) {
				if(isDependent(allRefs,ra,rb,sourceUnique,sourceRequired,targetUnique,targetRequired,ringAllowed,circleAllowed,transitive))
					return true;
			}
		}
		return false;
	}
	
	
	public ReferencePartition(OrderedPairMap<EClass,EClass,Boolean> map) {
		this.hasCommonSubClass = map;
	}
	
	protected OrderedPairMap<EClass,EClass,Boolean> hasCommonSubClass = null;

	protected boolean hasCommonElements(EClass ca, EClass cb) {
		if(ca==cb) return true;
		Boolean bv = hasCommonSubClass.get(ca,cb);
		if(bv!=null) return bv;
		else return false;
	}
	
	private OrderedTripleMap<List<EReference>, EReference, EReference,Boolean> circleMap = new OrderedTripleMap<List<EReference>, EReference, EReference,Boolean>();
	protected boolean hasCircle(List<EReference> refs, EReference ra, EReference rb, boolean transitive){
		Boolean bv = circleMap.get(refs, ra, rb);
		
		if(bv == null) {
			if(transitive){
				bv = isReachable(refs,ra.getEReferenceType(),rb.getEContainingClass()) 
						&& isReachable(refs,rb.getEReferenceType(),ra.getEContainingClass());
			} else {
				bv = hasCommonElements(ra.getEReferenceType(),rb.getEContainingClass())
						&&hasCommonElements(rb.getEReferenceType(),ra.getEContainingClass());
			}
			circleMap.put(refs, ra, rb, bv);
		}
		
		return bv;
	}
	
	private OrderedTripleMap<List<EReference>, EClass, EClass,Boolean> reachableMap = new OrderedTripleMap<List<EReference>, EClass, EClass,Boolean>();
	protected boolean isReachable(List<EReference> refs, EClass ca, EClass cb){
		List<EClass> visited = new ArrayList<EClass>();
		return isReachable(refs,ca,cb,visited);
	}
	protected boolean isReachable(List<EReference> refs, EClass ca, EClass cb, List<EClass> visited) {
		if(visited.contains(ca)) return false;
		visited.add(ca);
		
		Boolean bv = reachableMap.get(refs, ca, cb);
		if(bv==null) {
			bv = isReachableCalc(refs, ca, cb, visited);
			reachableMap.put(refs, ca, cb, bv);
		}
		return bv;
	}
	
	protected boolean isReachableCalc(List<EReference> refs, EClass ca, EClass cb, List<EClass> visited) {
		if(hasCommonElements(ca,cb)) return true;
		for(EReference r : refs){
			if(hasCommonElements(ca,r.getEContainingClass())&&isReachable(refs,r.getEReferenceType(),cb, visited))
				return true;
		}
		return false;
	}
	
}
