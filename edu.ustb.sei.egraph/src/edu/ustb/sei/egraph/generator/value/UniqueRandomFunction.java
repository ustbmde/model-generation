package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class UniqueRandomFunction<T> extends NamedRandomFunction<T> {
	private int retry;
	

	public UniqueRandomFunction(String name, AbstractRandomFunction<T> function, int retry) {
		super(name, function);
		this.retry = retry;
	}

	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		UniqueRandomValueFunction<T> f = new UniqueRandomValueFunction<T>(name,function.getValue(t),retry);
		return f;
	}

}
