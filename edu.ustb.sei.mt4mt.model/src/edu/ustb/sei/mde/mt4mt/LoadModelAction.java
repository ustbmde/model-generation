/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Load Model Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getModelURI <em>Model URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getLoadModelAction()
 * @model
 * @generated
 */
public interface LoadModelAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Model URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model URI</em>' attribute.
	 * @see #setModelURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getLoadModelAction_ModelURI()
	 * @model
	 * @generated
	 */
	String getModelURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getModelURI <em>Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model URI</em>' attribute.
	 * @see #getModelURI()
	 * @generated
	 */
	void setModelURI(String value);

	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getLoadModelAction_Key()
	 * @model
	 * @generated
	 */
	String getKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.LoadModelAction#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

} // LoadModelAction
