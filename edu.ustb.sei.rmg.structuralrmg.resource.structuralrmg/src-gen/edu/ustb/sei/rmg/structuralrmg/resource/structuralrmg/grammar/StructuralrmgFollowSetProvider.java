/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

/**
 * This class provides the follow sets for all terminals of the grammar. These
 * sets are used during code completion.
 */
public class StructuralrmgFollowSetProvider {
	
	public final static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement TERMINALS[] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement[76];
	
	public final static org.eclipse.emf.ecore.EStructuralFeature[] FEATURES = new org.eclipse.emf.ecore.EStructuralFeature[9];
	
	public final static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] LINKS = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[36];
	
	public final static edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] EMPTY_LINK_ARRAY = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[0];
	
	public static void initializeTerminals0() {
		TERMINALS[0] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_0);
		TERMINALS[1] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_1);
		TERMINALS[2] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_0);
		TERMINALS[3] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedBooleanTerminal(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_0);
		TERMINALS[4] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_1);
		TERMINALS[5] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_1);
		TERMINALS[6] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_2);
		TERMINALS[7] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_0);
		TERMINALS[8] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_0);
		TERMINALS[9] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_5);
		TERMINALS[10] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_2_0_0_0);
		TERMINALS[11] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_2_0_0_0);
		TERMINALS[12] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_3);
		TERMINALS[13] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_1);
		TERMINALS[14] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_0);
		TERMINALS[15] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_0);
		TERMINALS[16] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_9);
		TERMINALS[17] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_0);
		TERMINALS[18] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_1);
		TERMINALS[19] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedEnumerationTerminal(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_1);
		TERMINALS[20] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_2);
		TERMINALS[21] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_0);
		TERMINALS[22] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_0);
		TERMINALS[23] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_2);
		TERMINALS[24] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_1);
		TERMINALS[25] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_2);
		TERMINALS[26] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_3);
		TERMINALS[27] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_4);
		TERMINALS[28] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_5);
		TERMINALS[29] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_2);
		TERMINALS[30] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_3);
		TERMINALS[31] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_0);
		TERMINALS[32] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_0);
		TERMINALS[33] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_0);
		TERMINALS[34] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_2_0_0_0);
		TERMINALS[35] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_3);
		TERMINALS[36] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_1);
		TERMINALS[37] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_1);
		TERMINALS[38] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_2);
		TERMINALS[39] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_2);
		TERMINALS[40] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_3);
		TERMINALS[41] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_4);
		TERMINALS[42] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_0);
		TERMINALS[43] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_0);
		TERMINALS[44] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_1);
		TERMINALS[45] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_1);
		TERMINALS[46] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_1);
		TERMINALS[47] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_2);
		TERMINALS[48] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_3);
		TERMINALS[49] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_4);
		TERMINALS[50] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_0);
		TERMINALS[51] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_6);
		TERMINALS[52] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_1);
		TERMINALS[53] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_2);
		TERMINALS[54] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_7);
		TERMINALS[55] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_0);
		TERMINALS[56] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_9);
		TERMINALS[57] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_1);
		TERMINALS[58] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_2);
		TERMINALS[59] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_0);
		TERMINALS[60] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_0);
		TERMINALS[61] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_1);
		TERMINALS[62] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_2);
		TERMINALS[63] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_3);
		TERMINALS[64] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedBooleanTerminal(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_4);
		TERMINALS[65] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_5);
		TERMINALS[66] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_1);
		TERMINALS[67] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_0);
		TERMINALS[68] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_1);
		TERMINALS[69] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_1);
		TERMINALS[70] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_0);
		TERMINALS[71] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_0);
		TERMINALS[72] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_0);
		TERMINALS[73] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_1);
		TERMINALS[74] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_1);
		TERMINALS[75] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_2);
	}
	
	public static void initializeTerminals() {
		initializeTerminals0();
	}
	
	public static void initializeFeatures0() {
		FEATURES[0] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__TEMPLATES);
		FEATURES[1] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES);
		FEATURES[2] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateNode().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_NODE__BOUND);
		FEATURES[3] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NODES);
		FEATURES[4] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__EDGES);
		FEATURES[5] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES);
		FEATURES[6] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateEdge().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_EDGE__BINDING);
		FEATURES[7] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS);
		FEATURES[8] = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS);
	}
	
	public static void initializeFeatures() {
		initializeFeatures0();
	}
	
	public static void initializeLinks0() {
		LINKS[0] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[1] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[2] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[3] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[4] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[5] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[6] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[7] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[8] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[9] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[10] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getRole(), FEATURES[1]);
		LINKS[11] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getRole(), FEATURES[1]);
		LINKS[12] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBound(), FEATURES[2]);
		LINKS[13] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), FEATURES[3]);
		LINKS[14] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), FEATURES[3]);
		LINKS[15] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode(), FEATURES[3]);
		LINKS[16] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), FEATURES[4]);
		LINKS[17] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), FEATURES[4]);
		LINKS[18] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[19] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]);
		LINKS[20] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding(), FEATURES[5]);
		LINKS[21] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding(), FEATURES[5]);
		LINKS[22] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]);
		LINKS[23] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]);
		LINKS[24] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]);
		LINKS[25] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]);
		LINKS[26] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[27] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[28] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[8]);
		LINKS[29] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[8]);
		LINKS[30] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[31] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[32] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[33] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[34] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
		LINKS[35] = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]);
	}
	
	public static void initializeLinks() {
		initializeLinks0();
	}
	
	public static void wire0() {
		TERMINALS[0].addFollower(TERMINALS[1], EMPTY_LINK_ARRAY);
		TERMINALS[1].addFollower(TERMINALS[2], EMPTY_LINK_ARRAY);
		TERMINALS[1].addFollower(TERMINALS[3], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[1].addFollower(TERMINALS[4], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[2].addFollower(TERMINALS[5], EMPTY_LINK_ARRAY);
		TERMINALS[5].addFollower(TERMINALS[2], EMPTY_LINK_ARRAY);
		TERMINALS[5].addFollower(TERMINALS[3], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[5].addFollower(TERMINALS[4], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[3].addFollower(TERMINALS[4], EMPTY_LINK_ARRAY);
		TERMINALS[4].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[10], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getRole(), FEATURES[1]), });
		TERMINALS[11].addFollower(TERMINALS[10], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getRole(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[12].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[13].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[14], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[15], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[16], EMPTY_LINK_ARRAY);
		TERMINALS[14].addFollower(TERMINALS[17], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBound(), FEATURES[2]), new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), FEATURES[3]), });
		TERMINALS[14].addFollower(TERMINALS[18], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), FEATURES[3]), });
		TERMINALS[14].addFollower(TERMINALS[19], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode(), FEATURES[3]), });
		TERMINALS[20].addFollower(TERMINALS[14], EMPTY_LINK_ARRAY);
		TERMINALS[20].addFollower(TERMINALS[15], EMPTY_LINK_ARRAY);
		TERMINALS[20].addFollower(TERMINALS[16], EMPTY_LINK_ARRAY);
		TERMINALS[15].addFollower(TERMINALS[21], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), FEATURES[4]), });
		TERMINALS[15].addFollower(TERMINALS[22], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), FEATURES[4]), });
		TERMINALS[23].addFollower(TERMINALS[15], EMPTY_LINK_ARRAY);
		TERMINALS[23].addFollower(TERMINALS[16], EMPTY_LINK_ARRAY);
		TERMINALS[16].addFollower(TERMINALS[3], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[16].addFollower(TERMINALS[4], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[11], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[12], EMPTY_LINK_ARRAY);
		TERMINALS[17].addFollower(TERMINALS[24], EMPTY_LINK_ARRAY);
		TERMINALS[24].addFollower(TERMINALS[25], EMPTY_LINK_ARRAY);
		TERMINALS[25].addFollower(TERMINALS[26], EMPTY_LINK_ARRAY);
		TERMINALS[26].addFollower(TERMINALS[27], EMPTY_LINK_ARRAY);
		TERMINALS[27].addFollower(TERMINALS[28], EMPTY_LINK_ARRAY);
		TERMINALS[28].addFollower(TERMINALS[18], EMPTY_LINK_ARRAY);
		TERMINALS[28].addFollower(TERMINALS[19], EMPTY_LINK_ARRAY);
		TERMINALS[18].addFollower(TERMINALS[29], EMPTY_LINK_ARRAY);
		TERMINALS[29].addFollower(TERMINALS[30], EMPTY_LINK_ARRAY);
		TERMINALS[30].addFollower(TERMINALS[31], EMPTY_LINK_ARRAY);
		TERMINALS[30].addFollower(TERMINALS[32], EMPTY_LINK_ARRAY);
		TERMINALS[30].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[31].addFollower(TERMINALS[33], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding(), FEATURES[5]), });
		TERMINALS[34].addFollower(TERMINALS[33], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding(), FEATURES[5]), });
		TERMINALS[35].addFollower(TERMINALS[32], EMPTY_LINK_ARRAY);
		TERMINALS[35].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[32].addFollower(TERMINALS[36], EMPTY_LINK_ARRAY);
		TERMINALS[36].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[33].addFollower(TERMINALS[37], EMPTY_LINK_ARRAY);
		TERMINALS[37].addFollower(TERMINALS[38], EMPTY_LINK_ARRAY);
		TERMINALS[38].addFollower(TERMINALS[34], EMPTY_LINK_ARRAY);
		TERMINALS[38].addFollower(TERMINALS[35], EMPTY_LINK_ARRAY);
		TERMINALS[39].addFollower(TERMINALS[40], EMPTY_LINK_ARRAY);
		TERMINALS[40].addFollower(TERMINALS[41], EMPTY_LINK_ARRAY);
		TERMINALS[41].addFollower(TERMINALS[42], EMPTY_LINK_ARRAY);
		TERMINALS[41].addFollower(TERMINALS[43], EMPTY_LINK_ARRAY);
		TERMINALS[41].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[42].addFollower(TERMINALS[44], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[42], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[43], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[43].addFollower(TERMINALS[45], EMPTY_LINK_ARRAY);
		TERMINALS[45].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[21].addFollower(TERMINALS[46], EMPTY_LINK_ARRAY);
		TERMINALS[46].addFollower(TERMINALS[47], EMPTY_LINK_ARRAY);
		TERMINALS[47].addFollower(TERMINALS[48], EMPTY_LINK_ARRAY);
		TERMINALS[48].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
		TERMINALS[49].addFollower(TERMINALS[50], EMPTY_LINK_ARRAY);
		TERMINALS[49].addFollower(TERMINALS[51], EMPTY_LINK_ARRAY);
		TERMINALS[50].addFollower(TERMINALS[52], EMPTY_LINK_ARRAY);
		TERMINALS[52].addFollower(TERMINALS[53], EMPTY_LINK_ARRAY);
		TERMINALS[53].addFollower(TERMINALS[51], EMPTY_LINK_ARRAY);
		TERMINALS[51].addFollower(TERMINALS[54], EMPTY_LINK_ARRAY);
		TERMINALS[54].addFollower(TERMINALS[55], EMPTY_LINK_ARRAY);
		TERMINALS[54].addFollower(TERMINALS[56], EMPTY_LINK_ARRAY);
		TERMINALS[55].addFollower(TERMINALS[57], EMPTY_LINK_ARRAY);
		TERMINALS[57].addFollower(TERMINALS[58], EMPTY_LINK_ARRAY);
		TERMINALS[58].addFollower(TERMINALS[56], EMPTY_LINK_ARRAY);
		TERMINALS[56].addFollower(TERMINALS[59], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]), });
		TERMINALS[56].addFollower(TERMINALS[60], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]), });
		TERMINALS[56].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[22].addFollower(TERMINALS[61], EMPTY_LINK_ARRAY);
		TERMINALS[61].addFollower(TERMINALS[62], EMPTY_LINK_ARRAY);
		TERMINALS[62].addFollower(TERMINALS[63], EMPTY_LINK_ARRAY);
		TERMINALS[63].addFollower(TERMINALS[64], EMPTY_LINK_ARRAY);
		TERMINALS[63].addFollower(TERMINALS[65], EMPTY_LINK_ARRAY);
		TERMINALS[64].addFollower(TERMINALS[65], EMPTY_LINK_ARRAY);
		TERMINALS[65].addFollower(TERMINALS[59], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]), });
		TERMINALS[65].addFollower(TERMINALS[60], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), FEATURES[6]), });
		TERMINALS[65].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[59].addFollower(TERMINALS[66], EMPTY_LINK_ARRAY);
		TERMINALS[66].addFollower(TERMINALS[67], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]), });
		TERMINALS[60].addFollower(TERMINALS[68], EMPTY_LINK_ARRAY);
		TERMINALS[68].addFollower(TERMINALS[67], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[8]), });
		TERMINALS[67].addFollower(TERMINALS[69], EMPTY_LINK_ARRAY);
		TERMINALS[69].addFollower(TERMINALS[70], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[71], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[70], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[72], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[67], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]), });
		TERMINALS[70].addFollower(TERMINALS[60], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[71].addFollower(TERMINALS[73], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[71], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[70], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[72], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[67], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]), });
		TERMINALS[73].addFollower(TERMINALS[60], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[72].addFollower(TERMINALS[74], EMPTY_LINK_ARRAY);
		TERMINALS[74].addFollower(TERMINALS[75], EMPTY_LINK_ARRAY);
		TERMINALS[75].addFollower(TERMINALS[67], new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] {new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(), FEATURES[7]), });
		TERMINALS[75].addFollower(TERMINALS[60], EMPTY_LINK_ARRAY);
		TERMINALS[75].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
	}
	
	public static void wire() {
		wire0();
	}
	
	static {
		// initialize the arrays
		initializeTerminals();
		initializeFeatures();
		initializeLinks();
		// wire the terminals
		wire();
	}
}
