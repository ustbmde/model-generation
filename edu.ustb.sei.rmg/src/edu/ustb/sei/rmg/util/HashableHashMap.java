package edu.ustb.sei.rmg.util;

import java.util.HashMap;

public class HashableHashMap<K,V> extends HashMap<K, V> {
	private Object hashKey = new Object[]{};
	
	@Override
	public int hashCode() {
		return hashKey.hashCode();
	}

}
