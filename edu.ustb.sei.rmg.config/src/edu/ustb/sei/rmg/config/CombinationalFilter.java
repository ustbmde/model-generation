/**
 */
package edu.ustb.sei.rmg.config;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Combinational Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.CombinationalFilter#getLeft <em>Left</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.CombinationalFilter#getRight <em>Right</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.CombinationalFilter#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCombinationalFilter()
 * @model
 * @generated
 */
public interface CombinationalFilter extends Filter {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Filter)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCombinationalFilter_Left()
	 * @model containment="true"
	 * @generated
	 */
	Filter getLeft();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Filter value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Filter)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCombinationalFilter_Right()
	 * @model containment="true"
	 * @generated
	 */
	Filter getRight();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Filter value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.rmg.config.CombinationOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see edu.ustb.sei.rmg.config.CombinationOperator
	 * @see #setOperator(CombinationOperator)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getCombinationalFilter_Operator()
	 * @model
	 * @generated
	 */
	CombinationOperator getOperator();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.CombinationalFilter#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see edu.ustb.sei.rmg.config.CombinationOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(CombinationOperator value);

} // CombinationalFilter
