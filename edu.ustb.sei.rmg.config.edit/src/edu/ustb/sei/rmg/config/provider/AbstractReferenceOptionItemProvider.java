/**
 */
package edu.ustb.sei.rmg.config.provider;


import edu.ustb.sei.rmg.config.AbstractReferenceOption;
import edu.ustb.sei.rmg.config.ConfigPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.rmg.config.AbstractReferenceOption} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractReferenceOptionItemProvider
	extends ScalableConditionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractReferenceOptionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourceUniquePropertyDescriptor(object);
			addSourceRequiredPropertyDescriptor(object);
			addTargetUniquePropertyDescriptor(object);
			addTargetRequiredPropertyDescriptor(object);
			addTransitivityPropertyDescriptor(object);
			addRingPropertyDescriptor(object);
			addCirclePropertyDescriptor(object);
			addExternalObjectsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source Unique feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceUniquePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_sourceUnique_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_sourceUnique_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Required feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceRequiredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_sourceRequired_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_sourceRequired_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Unique feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetUniquePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_targetUnique_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_targetUnique_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target Required feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetRequiredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_targetRequired_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_targetRequired_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transitivity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransitivityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_transitivity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_transitivity_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ring feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_ring_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_ring_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__RING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Circle feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCirclePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_circle_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_circle_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__CIRCLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the External Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExternalObjectsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractReferenceOption_externalObjects_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractReferenceOption_externalObjects_feature", "_UI_AbstractReferenceOption_type"),
				 ConfigPackage.Literals.ABSTRACT_REFERENCE_OPTION__EXTERNAL_OBJECTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AbstractReferenceOption)object).getBoundCondition();
		return label == null || label.length() == 0 ?
			getString("_UI_AbstractReferenceOption_type") :
			getString("_UI_AbstractReferenceOption_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractReferenceOption.class)) {
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_UNIQUE:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__SOURCE_REQUIRED:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_UNIQUE:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TARGET_REQUIRED:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__TRANSITIVITY:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__RING:
			case ConfigPackage.ABSTRACT_REFERENCE_OPTION__CIRCLE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
