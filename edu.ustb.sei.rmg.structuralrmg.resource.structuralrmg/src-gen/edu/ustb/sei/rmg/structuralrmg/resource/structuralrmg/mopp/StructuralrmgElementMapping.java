/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A basic implementation of the
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgElementMappi
 * ng interface.
 * 
 * @param <ReferenceType> the type of the reference that can be mapped to
 */
public class StructuralrmgElementMapping<ReferenceType> implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgElementMapping<ReferenceType> {
	
	private final ReferenceType target;
	private String identifier;
	private String warning;
	
	public StructuralrmgElementMapping(String identifier, ReferenceType target, String warning) {
		super();
		this.target = target;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public ReferenceType getTargetElement() {
		return target;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
