package edu.ustb.sei.egraph.generator.value;

import java.util.Collections;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

abstract public class RandomValueFunction<T> {
	private double weight = 1.0;

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	abstract public T getValue(RandomService t);
	
	public Set<String> requiredScopes() {
		return Collections.emptySet();
	}
	
	public Set<String> providedScopes() {
		return Collections.emptySet();
	}
}
