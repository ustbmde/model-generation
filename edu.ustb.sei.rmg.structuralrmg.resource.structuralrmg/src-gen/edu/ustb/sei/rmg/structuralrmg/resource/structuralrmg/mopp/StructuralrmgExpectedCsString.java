/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class StructuralrmgExpectedCsString extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAbstractExpectedElement {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgKeyword keyword;
	
	public StructuralrmgExpectedCsString(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement getSymtaxElement() {
		return keyword;
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof StructuralrmgExpectedCsString) {
			return getValue().equals(((StructuralrmgExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
