/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class StructuralrmgExpectedStructuralFeature extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAbstractExpectedElement {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgPlaceholder placeholder;
	
	public StructuralrmgExpectedStructuralFeature(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgPlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	/**
	 * Returns the expected placeholder.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement getSymtaxElement() {
		return placeholder;
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof StructuralrmgExpectedStructuralFeature) {
			return getFeature().equals(((StructuralrmgExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
	@Override	
	public int hashCode() {
		return getFeature().hashCode();
	}
	
}
