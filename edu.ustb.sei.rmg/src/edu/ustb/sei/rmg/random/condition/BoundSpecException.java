package edu.ustb.sei.rmg.random.condition;

public class BoundSpecException extends Exception {
	
	private Condition condition;

	public Condition getCondition() {
		return condition;
	}

	public BoundSpecException(Condition c) {
		super("The bound condition "+c.toString()+" cannot be solved");
		condition = c;
	}

}
