/**
 */
package edu.ustb.sei.mde.mt4mt.util;

import edu.ustb.sei.mde.mt4mt.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage
 * @generated
 */
public class MT4MTSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MT4MTPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTSwitch() {
		if (modelPackage == null) {
			modelPackage = MT4MTPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MT4MTPackage.TEST_SUITE: {
				TestSuite testSuite = (TestSuite)theEObject;
				T result = caseTestSuite(testSuite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.TEST_CASE: {
				TestCase testCase = (TestCase)theEObject;
				T result = caseTestCase(testCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.CUSTOMIZABLE_TEST_CASE: {
				CustomizableTestCase customizableTestCase = (CustomizableTestCase)theEObject;
				T result = caseCustomizableTestCase(customizableTestCase);
				if (result == null) result = caseTestCase(customizableTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.PREDEFINED_TEST_CASE: {
				PredefinedTestCase predefinedTestCase = (PredefinedTestCase)theEObject;
				T result = casePredefinedTestCase(predefinedTestCase);
				if (result == null) result = caseTestCase(predefinedTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.GET_PUT_TEST_CASE: {
				GetPutTestCase getPutTestCase = (GetPutTestCase)theEObject;
				T result = caseGetPutTestCase(getPutTestCase);
				if (result == null) result = casePredefinedBXTestCase(getPutTestCase);
				if (result == null) result = casePredefinedTestCase(getPutTestCase);
				if (result == null) result = caseTestCase(getPutTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.PUT_GET_TEST_CASE: {
				PutGetTestCase putGetTestCase = (PutGetTestCase)theEObject;
				T result = casePutGetTestCase(putGetTestCase);
				if (result == null) result = casePredefinedBXTestCase(putGetTestCase);
				if (result == null) result = casePredefinedTestCase(putGetTestCase);
				if (result == null) result = caseTestCase(putGetTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.TEST_ACTION: {
				TestAction testAction = (TestAction)theEObject;
				T result = caseTestAction(testAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE: {
				PredefinedBXTestCase predefinedBXTestCase = (PredefinedBXTestCase)theEObject;
				T result = casePredefinedBXTestCase(predefinedBXTestCase);
				if (result == null) result = casePredefinedTestCase(predefinedBXTestCase);
				if (result == null) result = caseTestCase(predefinedBXTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.SEQUENCE_ACTION: {
				SequenceAction sequenceAction = (SequenceAction)theEObject;
				T result = caseSequenceAction(sequenceAction);
				if (result == null) result = caseTestAction(sequenceAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.LOAD_MODEL_ACTION: {
				LoadModelAction loadModelAction = (LoadModelAction)theEObject;
				T result = caseLoadModelAction(loadModelAction);
				if (result == null) result = caseTestAction(loadModelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.SAVE_MODEL_ACTION: {
				SaveModelAction saveModelAction = (SaveModelAction)theEObject;
				T result = caseSaveModelAction(saveModelAction);
				if (result == null) result = caseTestAction(saveModelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION: {
				ExecuteLauncherAction executeLauncherAction = (ExecuteLauncherAction)theEObject;
				T result = caseExecuteLauncherAction(executeLauncherAction);
				if (result == null) result = caseTestAction(executeLauncherAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION: {
				EvaluateOCLQueryAction evaluateOCLQueryAction = (EvaluateOCLQueryAction)theEObject;
				T result = caseEvaluateOCLQueryAction(evaluateOCLQueryAction);
				if (result == null) result = caseTestAction(evaluateOCLQueryAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.COPY_MODEL_ACTION: {
				CopyModelAction copyModelAction = (CopyModelAction)theEObject;
				T result = caseCopyModelAction(copyModelAction);
				if (result == null) result = caseTestAction(copyModelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.COMPARE_MODEL_ACTION: {
				CompareModelAction compareModelAction = (CompareModelAction)theEObject;
				T result = caseCompareModelAction(compareModelAction);
				if (result == null) result = caseTestAction(compareModelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION: {
				AssertComparisonResultAction assertComparisonResultAction = (AssertComparisonResultAction)theEObject;
				T result = caseAssertComparisonResultAction(assertComparisonResultAction);
				if (result == null) result = caseTestAction(assertComparisonResultAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION: {
				AssertOCLInvariantAction assertOCLInvariantAction = (AssertOCLInvariantAction)theEObject;
				T result = caseAssertOCLInvariantAction(assertOCLInvariantAction);
				if (result == null) result = caseTestAction(assertOCLInvariantAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.USER_ACTION: {
				UserAction userAction = (UserAction)theEObject;
				T result = caseUserAction(userAction);
				if (result == null) result = caseTestAction(userAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.SET_VARIABLE_ACTION: {
				SetVariableAction setVariableAction = (SetVariableAction)theEObject;
				T result = caseSetVariableAction(setVariableAction);
				if (result == null) result = caseTestAction(setVariableAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.UPDATE_MODEL_ACTION: {
				UpdateModelAction updateModelAction = (UpdateModelAction)theEObject;
				T result = caseUpdateModelAction(updateModelAction);
				if (result == null) result = caseTestAction(updateModelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.WRAPPER_TEST_CASE: {
				WrapperTestCase wrapperTestCase = (WrapperTestCase)theEObject;
				T result = caseWrapperTestCase(wrapperTestCase);
				if (result == null) result = caseTestCase(wrapperTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE: {
				AutoExecutorTestSuite autoExecutorTestSuite = (AutoExecutorTestSuite)theEObject;
				T result = caseAutoExecutorTestSuite(autoExecutorTestSuite);
				if (result == null) result = caseTestSuite(autoExecutorTestSuite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.CLEAR_ACTION: {
				ClearAction clearAction = (ClearAction)theEObject;
				T result = caseClearAction(clearAction);
				if (result == null) result = caseTestAction(clearAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.EXTERNAL_ACTION: {
				ExternalAction externalAction = (ExternalAction)theEObject;
				T result = caseExternalAction(externalAction);
				if (result == null) result = caseTestAction(externalAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MT4MTPackage.EXTERNAL_ACTION_PARAMETER: {
				ExternalActionParameter externalActionParameter = (ExternalActionParameter)theEObject;
				T result = caseExternalActionParameter(externalActionParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Suite</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Suite</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestSuite(TestSuite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestCase(TestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Customizable Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Customizable Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomizableTestCase(CustomizableTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Predefined Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Predefined Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePredefinedTestCase(PredefinedTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Get Put Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Get Put Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetPutTestCase(GetPutTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Put Get Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Put Get Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePutGetTestCase(PutGetTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Test Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Test Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTestAction(TestAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Predefined BX Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Predefined BX Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePredefinedBXTestCase(PredefinedBXTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequenceAction(SequenceAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Load Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Load Model Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoadModelAction(LoadModelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Save Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Save Model Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSaveModelAction(SaveModelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execute Launcher Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execute Launcher Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecuteLauncherAction(ExecuteLauncherAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Evaluate OCL Query Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Evaluate OCL Query Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvaluateOCLQueryAction(EvaluateOCLQueryAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Copy Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Copy Model Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCopyModelAction(CopyModelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compare Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compare Model Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompareModelAction(CompareModelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assert Comparison Result Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assert Comparison Result Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertComparisonResultAction(AssertComparisonResultAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assert OCL Invariant Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assert OCL Invariant Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertOCLInvariantAction(AssertOCLInvariantAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserAction(UserAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Variable Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Variable Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetVariableAction(SetVariableAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Update Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Update Model Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdateModelAction(UpdateModelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wrapper Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wrapper Test Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWrapperTestCase(WrapperTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Auto Executor Test Suite</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Auto Executor Test Suite</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAutoExecutorTestSuite(AutoExecutorTestSuite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearAction(ClearAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalAction(ExternalAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Action Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Action Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalActionParameter(ExternalActionParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MT4MTSwitch
