grammar ValueSpec;

@header {
	import edu.ustb.sei.egraph.generator.util.*;
}

specification returns [AbstractRandomFunction<?> function] : 
intSpec 
{
	$function = $intSpec.function;
} 
| realSpec
{
	$function = $realSpec.function;
} 
| boolSpec
{
	$function = $boolSpec.function;
}
| longSpec
{
	$function = $longSpec.function;
}
| charSpec
{
	$function = $charSpec.function;
}
| stringSpec
{
	$function = $stringSpec.function;
}
| dateSpec
{
	$function = $dateSpec.function;
}
| namedSpec 
{
	$function = $namedSpec.function;
}
| listSpec
{
	$function = $listSpec.function;
}
| pickSpec
{
	$function = $pickSpec.function;
}
| derivedSpec
{
	$function = $derivedSpec.function;
}
| defaultSpec
{
	$function = $defaultSpec.function;
}
| specialSpec
{
	$function = $specialSpec.function;
}
;

specialSpec  returns [AbstractRandomFunction<?> function] :
'empty'
{
	$function = SpecialFunction.emptyFunction;
}
| 'null'
{
	$function = SpecialFunction.nullFunction;
}
;

defaultSpec returns [AbstractRandomFunction<?> function] :
  'default' name=ID
  {
  	$function = new DefaultFunction(LexerUtil.normalizeID($name.text));
  }
;

listSpec returns [AbstractRandomFunction<?> function] :
'list' '(' element=specification ')' '@' length=intAtomValueSpec
{
	AbstractRandomFunction<?> e = $element.function;
	AbstractRandomFunction<Integer> l = $length.function;
	$function = new RandomFunction(new RandomListValueFunction(e,l));
}
;

// named spec
namedSpec  returns [AbstractRandomFunction<?> function] :
name=ID ':' specification
{
	String id = LexerUtil.normalizeID($name.text);
	$function = new NamedRandomFunction(id,$specification.function);
}
| 'unique' (name=ID '=')? specification ('try' retry = INT)?
{
	String name = null;
	if($name==null) {
		name = this.toString();
	} else {
		name = LexerUtil.normalizeID($name.text);
	}
	Integer retry = $retry==null ? 5 : ValueParser.parseInt($retry.text);
	$function = new UniqueRandomFunction(name,$specification.function,retry);
}
;

// pick spec
pickSpec returns [AbstractRandomFunction<?> function] :
name=ID
{
	String id = LexerUtil.normalizeID($name.text);
	$function = new RandomFunction(new RandomPickValueFunction(id));
}
;

// derived spec
derivedSpec returns [AbstractRandomFunction<?> function] :
{
	String type;
}
  'derived' type=('int'|'long'|'real'|'string') {
  	type = $type.text;
  } operands+=derivedTermSpec[type] ('+' operands+=derivedTermSpec[type])*
  {
	if($operands.size()==1) {
		$function = $operands.get(0).function;
	} else {
		if(type.equals("int")) {
			AbstractRandomFunction<Integer>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedTermSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Integer>)c.function;
			}
			$function = new RandomFunction<Integer>(BiOpValueFunction.createIntAdd(functions));
		} else if(type.equals("long")) {
			AbstractRandomFunction<Long>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedTermSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Long>)c.function;
			}
			$function = new RandomFunction<Long>(BiOpValueFunction.createLongAdd(functions));
		} else if(type.equals("real")) {
			AbstractRandomFunction<Double>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedTermSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Double>)c.function;
			}
			$function = new RandomFunction<Double>(BiOpValueFunction.createRealAdd(functions));
		} else if(type.equals("string")) {
			AbstractRandomFunction<?>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedTermSpecContext c = $operands.get(i);
				functions[i] = c.function;
			}
			$function = new RandomFunction<String>(BiOpValueFunction.createStringConc(functions));
		}
		
	}
  }
;

derivedTermSpec[String type] returns [AbstractRandomFunction<?> function] :
	operands+=derivedAtomSpec ('*' operands+=derivedAtomSpec)*
{
	if($operands.size()==1) {
		$function = $operands.get(0).function;
	} else {
		if(type.equals("int")) {
			AbstractRandomFunction<Integer>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedAtomSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Integer>)c.function;
			}
			$function = new RandomFunction<Integer>(BiOpValueFunction.createIntMul(functions));
		} else if(type.equals("long")) {
			AbstractRandomFunction<Long>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedAtomSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Long>)c.function;
			}
			$function = new RandomFunction<Long>(BiOpValueFunction.createLongMul(functions));
		} else if(type.equals("real")) {
			AbstractRandomFunction<Double>[] functions = new AbstractRandomFunction[$operands.size()];
			for(int i=0;i<$operands.size();i++) {
				DerivedAtomSpecContext c = $operands.get(i);
				functions[i] = (AbstractRandomFunction<Double>)c.function;
			}
			$function = new RandomFunction<Double>(BiOpValueFunction.createRealMul(functions));
		}
		
	}
}
;

derivedAtomSpec returns [AbstractRandomFunction<?> function] :
intSpec 
{
	$function = $intSpec.function;
}
| longSpec
{
	$function = $longSpec.function;
}
| realSpec
{
	$function = $realSpec.function;
}
| namedSpec 
{
	$function = $namedSpec.function;
}
| pickSpec
{
	$function = $pickSpec.function;
}
| defaultSpec
{
	$function = $defaultSpec.function;
}
;

 
// int spec

intSpec returns [AbstractRandomFunction<Integer> function] : 
intValueSpec 
{
	$function = $intValueSpec.function;
}
;

intRootSpec returns [AbstractRandomFunction<Integer> function] :
intValueSpec 
{
	$function = $intValueSpec.function;
} 
| intValueWithPossSpec 
{
	$function = $intValueWithPossSpec.function;
}
;


intValueSpec returns [AbstractRandomFunction<Integer> function] : 
intAtomValueSpec 
{
	$function = $intAtomValueSpec.function;
} 
| intProcSpec 
{
	$function = $intProcSpec.function;
}
;

intAtomValueSpec returns [AbstractRandomFunction<Integer> function] : 
intConsSpec 
{
	$function = $intConsSpec.function;
} 
| intSetSpec 
{
	$function = $intSetSpec.function;
} 
| intRangeSpec 
{
	$function = $intRangeSpec.function;
}
;

intValueWithPossSpec returns [AbstractRandomFunction<Integer> function] : 
source = intValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

intConsSpec returns [AbstractRandomFunction<Integer> function] : 
value = INT 
{
	Integer v = ValueParser.parseInt($value.text);
	ConstantValueFunction<Integer> f = new ConstantValueFunction<Integer>(v);
	$function = new RandomFunction<Integer>(f);
}
;

intSetSpec returns [AbstractRandomFunction<Integer> function] : 
'{' values += intRootSpec (',' values += intRootSpec)* '}'
{
	AbstractRandomFunction<Integer>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		IntRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<Integer>(functions);
};

intRangeSpec returns [AbstractRandomFunction<Integer> function] : 
'[' min = INT '..' max = INT  ']'
{
	Integer min = ValueParser.parseInt($min.text);
	Integer max = ValueParser.parseInt($max.text);
	$function = new RandomFunction<Integer>(new RandomIntValueInRangeFunction(min,max));
} 
| '[' min = INT '..' '*'  ']'
{
	Integer min = ValueParser.parseInt($min.text);
	$function = new RandomFunction<Integer>(new RandomIntValueInRangeFunction(min,null));
};

intProcSpec returns [AbstractRandomFunction<Integer> function] :
  head = intAtomValueSpec '>>' tail=intValueSpec
  {
  	$function = new ProcedureRandomFunction<Integer>($head.function, $tail.function);
  }
;

//real spec

realSpec returns [AbstractRandomFunction<Double> function] : 
realValueSpec 
{
	$function = $realValueSpec.function;
}
;

realRootSpec returns [AbstractRandomFunction<Double> function] :
realValueSpec 
{
	$function = $realValueSpec.function;
} 
| realValueWithPossSpec 
{
	$function = $realValueWithPossSpec.function;
}
;

realValueSpec returns [AbstractRandomFunction<Double> function] : 
realAtomValueSpec 
{
	$function = $realAtomValueSpec.function;
} 
| realProcSpec 
{
	$function = $realProcSpec.function;
}
;

realAtomValueSpec returns [AbstractRandomFunction<Double> function] : 
realConsSpec 
{
	$function = $realConsSpec.function;
} 
| realSetSpec 
{
	$function = $realSetSpec.function;
} 
| realRangeSpec 
{
	$function = $realRangeSpec.function;
}
;

realProcSpec returns [AbstractRandomFunction<Double> function] :
  head = realAtomValueSpec '>>' tail=realValueSpec
  {
  	$function = new ProcedureRandomFunction<Double>($head.function, $tail.function);
  }
;


realValueWithPossSpec returns [AbstractRandomFunction<Double> function] : 
source = realValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

realConsSpec returns [AbstractRandomFunction<Double> function] : 
value = REAL 
{
	Double v = ValueParser.parseDouble($value.text);
	ConstantValueFunction<Double> f = new ConstantValueFunction<Double>(v);
	$function = new RandomFunction<Double>(f);
}
;

realSetSpec returns [AbstractRandomFunction<Double> function] : 
'{' values += realRootSpec (',' values += realRootSpec)* '}'
{
	AbstractRandomFunction<Double>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		RealRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<Double>(functions);
};

realRangeSpec returns [AbstractRandomFunction<Double> function] : 
'[' min = REAL '..' max = REAL  ']'
{
	Double min = ValueParser.parseDouble($min.text);
	Double max = ValueParser.parseDouble($max.text);
	$function = new RandomFunction<Double>(new RandomRealValueInRangeFunction(min,max));
} 
| '[' min = REAL '..' '*'  ']'
{
	Double min = ValueParser.parseDouble($min.text);
	$function = new RandomFunction<Double>(new RandomRealValueInRangeFunction(min,null));
};


// bool spec
boolSpec returns [AbstractRandomFunction<Boolean> function] : 
boolValueSpec 
{
	$function = $boolValueSpec.function;
}
;

boolRootSpec returns [AbstractRandomFunction<Boolean> function] :
boolValueSpec 
{
	$function = $boolValueSpec.function;
} 
| boolValueWithPossSpec 
{
	$function = $boolValueWithPossSpec.function;
}
;

boolValueSpec returns [AbstractRandomFunction<Boolean> function] : 
boolAtomValueSpec 
{
	$function = $boolAtomValueSpec.function;
} 
| boolProcSpec 
{
	$function = $boolProcSpec.function;
}
;

boolAtomValueSpec returns [AbstractRandomFunction<Boolean> function] : 
boolConsSpec 
{
	$function = $boolConsSpec.function;
} 
| boolSetSpec 
{
	$function = $boolSetSpec.function;
}
;

boolProcSpec returns [AbstractRandomFunction<Boolean> function] :
  head = boolAtomValueSpec '>>' tail=boolValueSpec
{
	$function = new ProcedureRandomFunction<Boolean>($head.function, $tail.function);
}
;

boolValueWithPossSpec returns [AbstractRandomFunction<Boolean> function] : 
source = boolValueSpec ':' possibility = BOOLEAN 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

boolConsSpec returns [AbstractRandomFunction<Boolean> function] : 
value = BOOLEAN 
{
	Boolean v = ValueParser.parseBoolean($value.text);
	ConstantValueFunction<Boolean> f = new ConstantValueFunction<Boolean>(v);
	$function = new RandomFunction<Boolean>(f);
}
;

boolSetSpec returns [AbstractRandomFunction<Boolean> function] : 
'{' values += boolRootSpec (',' values += boolRootSpec)* '}'
{
	AbstractRandomFunction<Boolean>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		BoolRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<Boolean>(functions);
};

// long spec

longSpec returns [AbstractRandomFunction<Long> function] : 
longValueSpec 
{
	$function = $longValueSpec.function;
}
;

longRootSpec returns [AbstractRandomFunction<Long> function] :
longValueSpec 
{
	$function = $longValueSpec.function;
} 
| longValueWithPossSpec 
{
	$function = $longValueWithPossSpec.function;
}
;

longValueSpec returns [AbstractRandomFunction<Long> function] :
longAtomValueSpec
{
	$function = $longAtomValueSpec.function;
}
|
longProcSpec
{
	$function = $longProcSpec.function;
}
;

longAtomValueSpec returns [AbstractRandomFunction<Long> function] : 
longConsSpec 
{
	$function = $longConsSpec.function;
} 
| longSetSpec 
{
	$function = $longSetSpec.function;
} 
| longRangeSpec 
{
	$function = $longRangeSpec.function;
}
;

longProcSpec returns [AbstractRandomFunction<Long> function] :
  head = longAtomValueSpec '>>' tail=longValueSpec
{
	$function = new ProcedureRandomFunction<Long>($head.function, $tail.function);
}
;

longValueWithPossSpec returns [AbstractRandomFunction<Long> function] : 
source = longValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

longConsSpec returns [AbstractRandomFunction<Long> function] : 
value = LONG 
{
	String longStr = $value.text;
	Long v = ValueParser.parseLong(longStr);
	ConstantValueFunction<Long> f = new ConstantValueFunction<Long>(v);
	$function = new RandomFunction<Long>(f);
}
;

longSetSpec returns [AbstractRandomFunction<Long> function] : 
'{' values += longRootSpec (',' values += longRootSpec)* '}'
{
	AbstractRandomFunction<Long>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		LongRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<Long>(functions);
};

longRangeSpec returns [AbstractRandomFunction<Long> function] : 
'[' min = LONG '..' max = LONG  ']'
{
	String minStr = $min.text;
	String maxStr = $max.text;
	Long min = ValueParser.parseLong(minStr);
	Long max = ValueParser.parseLong(maxStr);
	$function = new RandomFunction<Long>(new RandomLongValueInRangeFunction(min,max));
} 
| '[' min = LONG '..' '*'  ']'
{
	String minStr = $min.text;
	Long min = ValueParser.parseLong(minStr);
	$function = new RandomFunction<Long>(new RandomLongValueInRangeFunction(min,null));
};

// char spec

charSpec returns [AbstractRandomFunction<Character> function] : 
charValueSpec 
{
	$function = $charValueSpec.function;
}
;

charRootSpec returns [AbstractRandomFunction<Character> function] :
charValueSpec 
{
	$function = $charValueSpec.function;
} 
| charValueWithPossSpec 
{
	$function = $charValueWithPossSpec.function;
}
;

charValueSpec returns [AbstractRandomFunction<Character> function] : 
charAtomValueSpec 
{
	$function = $charAtomValueSpec.function;
} 
| charProcSpec
{
	$function = $charProcSpec.function;
}
;

charAtomValueSpec returns [AbstractRandomFunction<Character> function] :
charConsSpec 
{
	$function = $charConsSpec.function;
} 
| charSetSpec 
{
	$function = $charSetSpec.function;
} 
| charRangeSpec 
{
	$function = $charRangeSpec.function;
}
;

charProcSpec returns [AbstractRandomFunction<Character> function] :
  head = charAtomValueSpec '>>' tail=charValueSpec
{
	$function = new ProcedureRandomFunction<Character>($head.function, $tail.function);
}
;

charValueWithPossSpec returns [AbstractRandomFunction<Character> function] : 
source = charValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

charConsSpec returns [AbstractRandomFunction<Character> function] : 
value = CHAR 
{
	String charStr = $value.text;
	Character v = ValueParser.parseChar(charStr);
	ConstantValueFunction<Character> f = new ConstantValueFunction<Character>(v);
	$function = new RandomFunction<Character>(f);
}
;

charSetSpec returns [AbstractRandomFunction<Character> function] : 
'{' values += charRootSpec (',' values += charRootSpec)* '}'
{
	AbstractRandomFunction<Character>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		CharRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<Character>(functions);
};

charRangeSpec returns [AbstractRandomFunction<Character> function] : 
'[' min = CHAR '..' max = CHAR  ']'
{
	String minStr = $min.text;
	String maxStr = $max.text;
	
	Character min = ValueParser.parseChar(minStr);
	Character max = ValueParser.parseChar(maxStr);;
	
	$function = new RandomFunction<Character>(new RandomCharValueInRangeFunction(min,max));
} 
;

// string spec
stringSpec returns [AbstractRandomFunction<String> function] : 
stringValueSpec 
{
	$function = $stringValueSpec.function;
}
;

stringRootSpec returns [AbstractRandomFunction<String> function] :
stringValueSpec 
{
	$function = $stringValueSpec.function;
} 
| stringValueWithPossSpec 
{
	$function = $stringValueWithPossSpec.function;
}
;

stringValueSpec returns [AbstractRandomFunction<String> function] :
stringAtomValueSpec
{
	$function = $stringAtomValueSpec.function; 
}
|
stringProcSpec
{
	$function = $stringProcSpec.function;
}
| stringConcSpec 
{
	$function = $stringConcSpec.function;
}
;

stringAtomValueSpec returns [AbstractRandomFunction<String> function] : 
stringConsSpec 
{
	$function = $stringConsSpec.function;
} 
| stringSetSpec 
{
	$function = $stringSetSpec.function;
} 
| stringRangeSpec 
{
	$function = $stringRangeSpec.function;
}
;

stringProcSpec returns [AbstractRandomFunction<String> function] :
  head = stringAtomValueSpec '>>' tail=stringValueSpec
{
	$function = new ProcedureRandomFunction<String>($head.function, $tail.function);
}
;

stringValueWithPossSpec returns [AbstractRandomFunction<String> function] : 
source = stringValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

stringConsSpec returns [AbstractRandomFunction<String> function] : 
value = STRING 
{
	String strStr = $value.text;
	String v = ValueParser.parseString(strStr);
	ConstantValueFunction<String> f = new ConstantValueFunction<String>(v);
	$function = new RandomFunction<String>(f);
}
;

stringSetSpec returns [AbstractRandomFunction<String> function] : 
'{' values += stringRootSpec (',' values += stringRootSpec)* '}'
{
	AbstractRandomFunction<String>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		StringRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<String>(functions);
};

stringRangeSpec returns [AbstractRandomFunction<String> function] : 
charRange = charRangeSpec '@' lenRange = intAtomValueSpec
{
	$function = new RandomFunction<String>(new RandomStringValueInRangeFunction($charRange.function,$lenRange.function));
} 
;

stringConcSpec returns [AbstractRandomFunction<String> function] :
components += stringAtomValueSpec ('+' components += stringAtomValueSpec)+
{
	AbstractRandomFunction<String>[] functions = new AbstractRandomFunction[$components.size()];
	for(int i=0;i<$components.size();i++) {
		StringAtomValueSpecContext c = $components.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFunction<String>(new StringValueConcatFunction(functions));
}
;

// date spec

dateSpec returns [AbstractRandomFunction<java.util.Date> function] : 
dateValueSpec 
{
	$function = $dateValueSpec.function;
}
;

dateRootSpec returns [AbstractRandomFunction<java.util.Date> function] :
dateValueSpec 
{
	$function = $dateValueSpec.function;
} 
| dateValueWithPossSpec 
{
	$function = $dateValueWithPossSpec.function;
}
;


dateValueSpec returns [AbstractRandomFunction<java.util.Date> function] : 
dateAtomValueSpec 
{
	$function = $dateAtomValueSpec.function;
} 
| dateProcSpec 
{
	$function = $dateProcSpec.function;
}
;

dateAtomValueSpec returns [AbstractRandomFunction<java.util.Date> function] : 
dateConsSpec 
{
	$function = $dateConsSpec.function;
} 
| dateSetSpec 
{
	$function = $dateSetSpec.function;
} 
| dateRangeSpec 
{
	$function = $dateRangeSpec.function;
}
;

dateValueWithPossSpec returns [AbstractRandomFunction<java.util.Date> function] : 
source = dateValueSpec ':' possibility = REAL 
{
	$function = $source.function;
	Double p = ValueParser.parseDouble($possibility.text);
	$function.setWeight(p);
}
;

dateConsSpec returns [AbstractRandomFunction<java.util.Date> function] : 
value = DATETIME 
{
	java.util.Date v = ValueParser.parseDate($value.text);
	ConstantValueFunction<java.util.Date> f = new ConstantValueFunction<java.util.Date>(v);
	$function = new RandomFunction<java.util.Date>(f);
}
;

dateSetSpec returns [AbstractRandomFunction<java.util.Date> function] : 
'{' values += dateRootSpec (',' values += dateRootSpec)* '}'
{
	AbstractRandomFunction<java.util.Date>[] functions = new AbstractRandomFunction[$values.size()];
	for(int i=0;i<$values.size();i++) {
		DateRootSpecContext c = $values.get(i);
		functions[i] = c.function;
	}
	$function = new RandomFromSetFunction<java.util.Date>(functions);
};

dateRangeSpec returns [AbstractRandomFunction<java.util.Date> function] : 
'[' min = DATETIME '..' max = DATETIME  ']'
{
	java.util.Date min = ValueParser.parseDate($min.text);
	java.util.Date max = ValueParser.parseDate($max.text);
	$function = new RandomFunction<java.util.Date>(new RandomDateValueInRangeFunction(min,max));
} 
;

dateProcSpec returns [AbstractRandomFunction<java.util.Date> function] :
  head = dateAtomValueSpec '>>' tail=dateValueSpec
  {
  	$function = new ProcedureRandomFunction<java.util.Date>($head.function, $tail.function);
  }
;




STRING
:
    '"' .*? '"'
;

LONG
:
'-'?DIGIT+'L'
;

INT
:
  '-'?DIGIT+
;

REAL
:
'-'? DIGIT+ '.' DIGIT+
;

BOOLEAN
:
'true' | 'false'
;

CHAR
:
    '\''. '\''|'\'''\\u'HEX+'\''
;

fragment HEX
:
	[a-fA-F0-9]
;

fragment DIGIT
:
    [0-9]
;

WS
:
    [ \t\f]+ -> skip
;

ID
:
    '#'?('_'|LETTER) ('_'|LETTER | DIGIT)*
;

fragment
LETTER
:
    [a-zA-Z]
;

DATETIME :
DIGIT+ '-' DIGIT+ '-' DIGIT+ (DIGIT+ '-' DIGIT+ '-' DIGIT+)?
;
