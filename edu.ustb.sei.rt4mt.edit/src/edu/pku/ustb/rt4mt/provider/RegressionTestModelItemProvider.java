/**
 */
package edu.pku.ustb.rt4mt.provider;


import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.Rt4mtPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.pku.ustb.rt4mt.RegressionTestModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RegressionTestModelItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RegressionTestModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addInputBasePropertyDescriptor(object);
			addOutputBasePropertyDescriptor(object);
			addRunConfigurationNamePropertyDescriptor(object);
			addInputPropertyDescriptor(object);
			addOutputPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RegressionTestModel_name_feature", "_UI_RegressionTestModel_type"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Input feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInputPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_input_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RegressionTestModel_input_feature", "_UI_RegressionTestModel_type"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__INPUT,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_output_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RegressionTestModel_output_feature", "_UI_RegressionTestModel_type"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__OUTPUT,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Input Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInputBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_inputBase_feature"),
				 getString("_UI_RegressionTestModel_inputBase_description"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__INPUT_BASE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_outputBase_feature"),
				 getString("_UI_RegressionTestModel_outputBase_description"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__OUTPUT_BASE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Run Configuration Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRunConfigurationNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegressionTestModel_runConfigurationName_feature"),
				 getString("_UI_RegressionTestModel_runConfigurationName_description"),
				 Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__INPUT);
			childrenFeatures.add(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__OUTPUT);
			childrenFeatures.add(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__TEST_CASE_GROUP);
			childrenFeatures.add(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns RegressionTestModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RegressionTestModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String label = ((RegressionTestModel)object).getName();
		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RegressionTestModel.class)) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__NAME:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__INPUT,
				 Rt4mtFactory.eINSTANCE.createInputParameterConfigurationGroup()));

		newChildDescriptors.add
			(createChildParameter
				(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__OUTPUT,
				 Rt4mtFactory.eINSTANCE.createOutputParameterConfigurationGroup()));

		newChildDescriptors.add
			(createChildParameter
				(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__TEST_CASE_GROUP,
				 Rt4mtFactory.eINSTANCE.createTestCaseGroup()));

		newChildDescriptors.add
			(createChildParameter
				(Rt4mtPackage.Literals.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP,
				 Rt4mtFactory.eINSTANCE.createTestRoundGroup()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return Rt4mtEditPlugin.INSTANCE;
	}

}
