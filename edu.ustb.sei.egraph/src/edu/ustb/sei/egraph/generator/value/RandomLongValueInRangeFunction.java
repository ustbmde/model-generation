package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomLongValueInRangeFunction extends RandomNumValueInRangeFunction<Long> {
	
	public RandomLongValueInRangeFunction(Long min, Long max) {
		super(min,max);
	}

	@Override
	public Long getValue(RandomService t) {
		if(max!=null)
			return t.randomLong(min, max+1);
		else
			return Math.abs(t.randomLong())+min;
	}

}
