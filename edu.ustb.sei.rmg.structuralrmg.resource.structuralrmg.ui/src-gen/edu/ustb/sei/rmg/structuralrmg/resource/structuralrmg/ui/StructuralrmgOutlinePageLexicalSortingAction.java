/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgOutlinePageLexicalSortingAction extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.AbstractStructuralrmgOutlinePageAction {
	
	public StructuralrmgOutlinePageLexicalSortingAction(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
