/**
 */
package edu.ustb.sei.rmg.config;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.GlobalOption#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getGlobalOption()
 * @model
 * @generated
 */
public interface GlobalOption extends Condition {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.rmg.config.GlobalOptionEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see edu.ustb.sei.rmg.config.GlobalOptionEnum
	 * @see #setKey(GlobalOptionEnum)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getGlobalOption_Key()
	 * @model
	 * @generated
	 */
	GlobalOptionEnum getKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.GlobalOption#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see edu.ustb.sei.rmg.config.GlobalOptionEnum
	 * @see #getKey()
	 * @generated
	 */
	void setKey(GlobalOptionEnum value);

} // GlobalOption
