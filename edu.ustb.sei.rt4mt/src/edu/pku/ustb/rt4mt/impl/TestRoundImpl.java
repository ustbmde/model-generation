/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.Rt4mtPackage;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Round</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestRoundImpl#getRoundNumber <em>Round Number</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestRoundImpl#getTestExecutions <em>Test Executions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TestRoundImpl extends EObjectImpl implements TestRound {
	/**
	 * The default value of the '{@link #getRoundNumber() <em>Round Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int ROUND_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoundNumber() <em>Round Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundNumber()
	 * @generated
	 * @ordered
	 */
	protected int roundNumber = ROUND_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTestExecutions() <em>Test Executions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestExecutions()
	 * @generated
	 * @ordered
	 */
	protected EList<TestExecution> testExecutions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestRoundImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Rt4mtPackage.Literals.TEST_ROUND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoundNumber() {
		return roundNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundNumber(int newRoundNumber) {
		int oldRoundNumber = roundNumber;
		roundNumber = newRoundNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.TEST_ROUND__ROUND_NUMBER, oldRoundNumber, roundNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestExecution> getTestExecutions() {
		if (testExecutions == null) {
			testExecutions = new EObjectContainmentEList<TestExecution>(TestExecution.class, this, Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS);
		}
		return testExecutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS:
				return ((InternalEList<?>)getTestExecutions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND__ROUND_NUMBER:
				return getRoundNumber();
			case Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS:
				return getTestExecutions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND__ROUND_NUMBER:
				setRoundNumber((Integer)newValue);
				return;
			case Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS:
				getTestExecutions().clear();
				getTestExecutions().addAll((Collection<? extends TestExecution>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND__ROUND_NUMBER:
				setRoundNumber(ROUND_NUMBER_EDEFAULT);
				return;
			case Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS:
				getTestExecutions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND__ROUND_NUMBER:
				return roundNumber != ROUND_NUMBER_EDEFAULT;
			case Rt4mtPackage.TEST_ROUND__TEST_EXECUTIONS:
				return testExecutions != null && !testExecutions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (roundNumber: ");
		result.append(roundNumber);
		result.append(')');
		return result.toString();
	}

} //TestRoundImpl
