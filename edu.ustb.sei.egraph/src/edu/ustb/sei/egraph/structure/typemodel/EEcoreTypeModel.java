package edu.ustb.sei.egraph.structure.typemodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.structure.ETypeModel;

public class EEcoreTypeModel implements ETypeModel<EClass, EReference, EAttribute, String> {
	
	private Set<EPackage> packages;
	private Map<EClass,Collection<EClass>> upstreamNodeTypes = null;
	private Map<EClass,Collection<EClass>> downstreamNodeTypes = null;
	private List<EReference> containmentEdgeTypes = null;
	private Map<EClass,List<EReference>> containmentEdgeTypesOfNodeType = null;
	private Map<EClass,Map<EClass,Boolean>> hasCommonSubType;
	private Map<EClass,List<EReference>> incomingEdgeTypesOfNodeType = null;
	private Map<EClass,Collection<EReference>> allIncomingEdgeTypesOfNodeType = null;
	
	private List<EClass> allTypes = null;
	private List<EReference> allReferences = null;
	private Set<EClass> referredTypes = null;
	
	/**
	 * because an EReference may have an eOpposite, we must tell the type model which one should be registered
	 */
	private  Set<EReference> normalizedReferences = null;
	
	public EEcoreTypeModel(EPackage p) {
		this(Collections.singleton(p));
	}
	public EEcoreTypeModel(Set<EPackage> packages) {
		this(packages,Collections.emptySet());
	}
	
	public EEcoreTypeModel(EPackage p,Set<EReference> normalizedReferences) {
		this(Collections.singleton(p),normalizedReferences);
	}
	public EEcoreTypeModel(Set<EPackage> packages, Set<EReference> normalizedReferences) {
		super();
		this.packages = packages;
		this.normalizedReferences = normalizedReferences;
		
		upstreamNodeTypes = new HashMap<EClass,Collection<EClass>>();
		downstreamNodeTypes = new HashMap<EClass,Collection<EClass>>();
		containmentEdgeTypesOfNodeType = new HashMap<EClass,List<EReference>>();
		hasCommonSubType = new HashMap<EClass, Map<EClass,Boolean>>();
		
		incomingEdgeTypesOfNodeType = new HashMap<EClass, List<EReference>>();
		allIncomingEdgeTypesOfNodeType = new HashMap<EClass, Collection<EReference>>();
		
		allTypes = new ArrayList<EClass>();
		allReferences = new ArrayList<EReference>();
		allReferences.addAll(normalizedReferences);
		
		this.referredTypes  = new HashSet<EClass>();
		
		containmentEdgeTypes = null; // new ArrayList<EReference>(); create on demand
		
		collectInformation();
	}

	
	protected void collectInformation() {
		this.packages.forEach((p)->{
			collectInformation(p,this.referredTypes);
			});
		
		this.referredTypes.removeIf(e->this.allTypes.contains(e));
		
		this.allTypes.addAll(this.referredTypes);
	}
	
	protected void collectInformation(EPackage p, Set<EClass> referredTypes) {
		p.getEClassifiers().forEach((c)->{
			if(c instanceof EClass) {
				this.allTypes.add((EClass)c);
				((EClass) c).getEReferences().forEach((r)->{
					EReference nr = this.normalizeReference(r);
					if(nr!=null) {
						this.allReferences.add(nr);
						EClass cls = nr.getEReferenceType();
						if(cls.eIsProxy()) {
							cls = (EClass) EcoreUtil.resolve(cls, c.eResource().getResourceSet());
						}
						List<EReference> list = this.incomingEdgeTypesOfNodeType.get(cls);
						if(list==null) {
							list = new ArrayList<EReference>();
							this.incomingEdgeTypesOfNodeType.put(cls, list);
						}
						list.add(nr);
						referredTypes.add(cls);
					}
				});
			}
		});
		
		p.getESubpackages().forEach((sp)->{
			collectInformation(sp,referredTypes);
		});
	}
	
	protected EReference normalizeReference(EReference r) {
		if(r.getEOpposite()==null) return r;
		if(this.allReferences.contains(r)) return null; // already exists
		if(this.allReferences.contains(r.getEOpposite())) return null; // opposite exists
		
		if(r.isContainer()) return r.getEOpposite();
		if(this.normalizedReferences.contains(r.getEOpposite())) return r.getEOpposite();
		
		return r;
	}
	

	@Override
	public void addNodeType(EClass n) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addEdgeType(EReference e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addNodeAttrType(EClass n, EAttribute a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addEdgeAttrType(EReference e, String a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addParent(EClass child, EClass parent) {
		throw new UnsupportedOperationException();
	}

	
	public Collection<EClass> collectAllSuperTypes(EClass seed) {
		Collection<EClass> allPars = this.upstreamNodeTypes.get(seed);
		
		if(allPars!=null) 
			return allPars;
		else {
			allPars = new HashSet<EClass>();
			allPars.add(seed);
			
			Collection<EClass> pars = seed.getESuperTypes();
			
			if(pars!=null) {
				for(EClass p : pars) {
					allPars.addAll(collectAllSuperTypes(p));
				}
			}
			this.upstreamNodeTypes.put(seed, allPars);
			return allPars;
		}
	}

	@Override
	public Collection<EClass> collectAllConcreteSubTypes(EClass seed) {
		Collection<EClass> allChd = this.downstreamNodeTypes.get(seed);
		if(allChd!=null)
			return allChd;
		else {
			allChd = new HashSet<EClass>();
			
			if(seed.isAbstract()==false)
				allChd.add(seed);
			
			for(EClass c : this.allNodeTypes()) {
				if(((EClass) c).isAbstract()) continue;
				Collection<EClass> pars = this.collectAllSuperTypes((EClass)c);
				
				if(pars.contains(seed)) {
					allChd.add((EClass) c);
				}
			}
			
			this.downstreamNodeTypes.put(seed, allChd);
			return allChd;
		}
	}

	@Override
	public EClass getSource(EReference e) {
		return e.getEContainingClass();
	}

	@Override
	public EClass getTarget(EReference e) {
		return e.getEReferenceType();
	}

	@Override
	public int getSourceLowerBound(EReference e) {
		if(e.getEOpposite()!=null) {
			return e.getEOpposite().getLowerBound();
		}
		return 0;
	}

	@Override
	public int getTargetLowerBound(EReference e) {
		return e.getLowerBound();
	}

	@Override
	public int getSourceUpperBound(EReference e) {
		if(e.getEOpposite()!=null) {
			int u = e.getEOpposite().getUpperBound();
			if(u<0) return Integer.MAX_VALUE;
			else return u;
		}
		return Integer.MAX_VALUE;
	}

	@Override
	public int getTargetUpperBound(EReference e) {
		int u = e.getUpperBound();
		if(u<0) return Integer.MAX_VALUE;
		return u;
	}

	@Override
	public Collection<EClass> allNodeTypes() {
		return this.allTypes;
	}

	@Override
	public Collection<EReference> allEdgeTypes() {
		return this.allReferences;
	}

	@Override
	public boolean isAbstract(EClass t) {
		return t.isAbstract();
	}

	@Override
	public List<EReference> getContainerReferenceForClass(EClass t) {
		List<EReference> list = containmentEdgeTypesOfNodeType.get(t);
		
		if(list==null) {
			List<EReference> allContainments = collectAllContainmentReferences();
			list = new ArrayList<EReference>();
			
			for(EReference e : allContainments) {
				EClass tar = this.getTarget(e);
				if(this.isSuperNodeType(t,tar))
					list.add(e);
			}
			containmentEdgeTypesOfNodeType.put(t, list);
		}
		
		return list;
	}

	@Override
	public List<EReference> collectAllContainmentReferences() {
		if(this.containmentEdgeTypes==null) {
			this.containmentEdgeTypes = new ArrayList<EReference>();
			
			for(EReference r : this.allEdgeTypes()) {
				if(r.isContainment())
					this.containmentEdgeTypes.add(r);
			}
			
		}
		return this.containmentEdgeTypes;
	}
	
	@Override
	public boolean hasCommonSubTypes(EClass t1, EClass t2) {
		Map<EClass,Boolean> map = this.hasCommonSubType.get(t1);
		Boolean flag = null;
		if(map==null || (flag=map.get(t2))==null) {
			flag = computeHasCommonSubTypes(t1,t2);
		}
		return flag;
	}
	
	private boolean computeHasCommonSubTypes(EClass t1, EClass t2) {
		Map<EClass,Boolean> map1 = this.hasCommonSubType.get(t1);
		Map<EClass,Boolean> map2 = this.hasCommonSubType.get(t2);
		
		if(map1==null) {
			map1 = new HashMap<EClass, Boolean>();
			this.hasCommonSubType.put(t1, map1);
		}
		
		if(map2==null) {
			map2 = new HashMap<EClass, Boolean>();
			this.hasCommonSubType.put(t2, map2);
		}
		
		Collection<EClass> ts1 = this.collectAllConcreteSubTypes(t1);
		Collection<EClass> ts2 = this.collectAllConcreteSubTypes(t2);
		
		Boolean flag = !Collections.disjoint(ts1, ts2);
		map1.put(t2, flag);
		map2.put(t1, flag);
		
		return flag;
	}
	
	
	@Override
	public Collection<EAttribute> getAttributesOfNodeType(EClass type) {
		return type.getEAttributes();
	}
	@Override
	public Collection<String> getAttributesOfEdgeType(EReference type) {
		return Collections.emptyList();
	}
	@Override
	public Collection<EAttribute> getAllAttributesOfNodeType(EClass type) {
		return type.getEAllAttributes();
	}
	
	
	@Override
	public Collection<EReference> getOutgoingEdgeTypesOfNodeType(EClass source) {
		return source.getEReferences();
	}
	
	@Override
	public Collection<EReference> getAllOutgoingEdgeTypesOfNodeType(EClass source) {
		return source.getEAllReferences();
	}
	
	@Override
	public Collection<EReference> getAllIncomingEdgeTypesOfNodeType(EClass target) {
		Collection<EReference> refs = allIncomingEdgeTypesOfNodeType.get(target);
		if(refs==null) {
			refs = ETypeModel.super.getAllIncomingEdgeTypesOfNodeType(target);
			allIncomingEdgeTypesOfNodeType.put(target, refs);
		}
		return refs;
	}
	
	@Override
	public Collection<EReference> getIncomingEdgeTypesOfNodeType(EClass target) {
		List<EReference> list = this.incomingEdgeTypesOfNodeType.get(target);
		if(list==null)
			return Collections.emptyList();
		else return list;
	}
	@Override
	public EClass resolveNodeType(QualifiedName name) {
		for(EClass c : this.allTypes) {
			if(c.getName().equals(name.name)) 
				return c;
		}
		return null;
	}
	@Override
	public QualifiedName desolveNodeType(EClass type) {
		return new QualifiedName(type.getName(), QualifiedName.defaultType);
	}
	@Override
	public EReference resolveEdgeType(QualifiedName name) {
		EClass source = this.resolveNodeType(new QualifiedName(name.type,QualifiedName.defaultType));
		if(source!=null) {
			return (EReference)source.getEStructuralFeature(name.name);
		}
		return null;
	}
	@Override
	public QualifiedName desolveEdgeType(EReference type) {
		return new QualifiedName(type.getName(), type.getEContainingClass().getName());
	}
	@Override
	public EAttribute resolveNodeAttribute(QualifiedName name) {
		EClass source = this.resolveNodeType(new QualifiedName(name.type,QualifiedName.defaultType));
		if(source!=null) {
			return (EAttribute)source.getEStructuralFeature(name.name);
		}
		return null;
	}
	@Override
	public QualifiedName desolveNodeAttribute(EAttribute attr) {
		return new QualifiedName(attr.getName(), attr.getEContainingClass().getName());
	}
}
