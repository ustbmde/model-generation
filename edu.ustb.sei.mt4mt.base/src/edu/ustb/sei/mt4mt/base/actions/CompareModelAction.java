package edu.ustb.sei.mt4mt.base.actions;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class CompareModelAction implements IAction {
	
	public CompareModelAction(String leftURI, String rightURI, String diffKey) {
		super();
		this.leftURI = leftURI;
		this.rightURI = rightURI;
		this.diffKey = diffKey;
	}

	private String leftURI;
	private String rightURI;
	private String diffKey;

	@Override
	public void run(Context context) {
		ResourceSet tempSet = context.createResourceSet();
		URI left = URI.createURI(leftURI);
		URI right = URI.createURI(rightURI);
		
		Resource leftRes = tempSet.getResource(left,true);
		Resource rightRes = tempSet.getResource(right,true);
		
		EMFCompare comparator = EMFCompare.builder().build();
		IComparisonScope scope = new DefaultComparisonScope(leftRes, rightRes, null);
		Comparison diff = comparator.compare(scope);
		
		context.putData(diffKey, diff);
	}

}
