/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.AttributeBinding#getAttribute <em>Attribute</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getAttributeBinding()
 * @model
 * @generated
 */
public interface AttributeBinding extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(EAttribute)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getAttributeBinding_Attribute()
	 * @model required="true"
	 * @generated
	 */
	EAttribute getAttribute();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.AttributeBinding#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(EAttribute value);

} // AttributeBinding
