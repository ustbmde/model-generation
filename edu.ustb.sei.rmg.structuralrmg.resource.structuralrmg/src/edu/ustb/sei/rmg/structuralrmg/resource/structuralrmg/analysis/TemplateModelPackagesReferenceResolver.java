/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

public class TemplateModelPackagesReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.TemplateModel container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<org.eclipse.emf.ecore.EPackage> result) {
		if(identifier==null||identifier.length()==0)
			return;
		
		if(identifier.charAt(0)=='<') {
			if(identifier.charAt(identifier.length()-1)=='>')
				identifier = identifier.substring(1,identifier.length()-1);
			else identifier = identifier.substring(1);
		}
		
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(org.eclipse.emf.ecore.EPackage element, edu.ustb.sei.rmg.structuralrmg.TemplateModel container, org.eclipse.emf.ecore.EReference reference) {
		return '<'+element.eResource().getURI().toString()+'>';
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
