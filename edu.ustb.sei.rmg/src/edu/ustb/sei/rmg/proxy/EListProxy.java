package edu.ustb.sei.rmg.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Calendar;

import org.eclipse.emf.common.util.EList;

public class EListProxy implements InvocationHandler {
	static private long timeConsumed = 0L;

	static public long getTimeConsumed() {
		return timeConsumed;
	}
	
	static public void reset() {
		timeConsumed = 0L;
	}
	
	EList<?> host;
	
	public EListProxy(EList<?> host) {
		this.host = host;
	}

	@Override
	public Object invoke(Object arg0, Method arg1, Object[] arg2)
			throws Throwable {
		// TODO Auto-generated method stub
		
		String name = arg1.getName();
		boolean flag = name.equals("remove")||name.equals("add");
		
		long start = 0, end = 0;
		
		if(flag) {
			start = Calendar.getInstance().getTimeInMillis();
		}
		
		Object r = arg1.invoke(host, arg2);
		
		if(flag) {
			end = Calendar.getInstance().getTimeInMillis();
			timeConsumed += (end-start);
		}
		
		return r;
	}

}
