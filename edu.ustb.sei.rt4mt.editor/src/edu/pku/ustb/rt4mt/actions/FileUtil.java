package edu.pku.ustb.rt4mt.actions;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

public class FileUtil {
	protected static URIConverter converter = new ExtensibleURIConverterImpl();
	protected static byte[] buffer = new byte[8192];
	public static boolean forChannel(URI ifu,URI ofu) throws Exception{
		InputStream  f1 =  converter.createInputStream(ifu);
		OutputStream  f2 = converter.createOutputStream(ofu);
		
		while(true){
			int len = f1.read(buffer);
			if(len<=0) break;
			f2.write(buffer, 0, len);
		}
		f1.close();
		f2.close();
		return true;
	}
	
	static public String generateTimeString() {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH)+1;
		int day = c.get(Calendar.DAY_OF_MONTH);
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int min = c.get(Calendar.MINUTE);
		int sec = c.get(Calendar.SECOND);
		String str = "-"+year+"-"+month+"-"+day+"-"+hour+"-"+min+"-"+sec;
		return str;
	}
}
