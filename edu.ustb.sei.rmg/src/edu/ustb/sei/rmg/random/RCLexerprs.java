package edu.ustb.sei.rmg.random;

public class RCLexerprs implements lpg.runtime.ParseTable, RCLexersym {
    public final static int ERROR_SYMBOL = 0;
    public final int getErrorSymbol() { return ERROR_SYMBOL; }

    public final static int SCOPE_UBOUND = 0;
    public final int getScopeUbound() { return SCOPE_UBOUND; }

    public final static int SCOPE_SIZE = 0;
    public final int getScopeSize() { return SCOPE_SIZE; }

    public final static int MAX_NAME_LENGTH = 0;
    public final int getMaxNameLength() { return MAX_NAME_LENGTH; }

    public final static int NUM_STATES = 40;
    public final int getNumStates() { return NUM_STATES; }

    public final static int NT_OFFSET = 103;
    public final int getNtOffset() { return NT_OFFSET; }

    public final static int LA_STATE_OFFSET = 584;
    public final int getLaStateOffset() { return LA_STATE_OFFSET; }

    public final static int MAX_LA = 2;
    public final int getMaxLa() { return MAX_LA; }

    public final static int NUM_RULES = 231;
    public final int getNumRules() { return NUM_RULES; }

    public final static int NUM_NONTERMINALS = 33;
    public final int getNumNonterminals() { return NUM_NONTERMINALS; }

    public final static int NUM_SYMBOLS = 136;
    public final int getNumSymbols() { return NUM_SYMBOLS; }

    public final static int SEGMENT_SIZE = 8192;
    public final int getSegmentSize() { return SEGMENT_SIZE; }

    public final static int START_STATE = 232;
    public final int getStartState() { return START_STATE; }

    public final static int IDENTIFIER_SYMBOL = 0;
    public final int getIdentifier_SYMBOL() { return IDENTIFIER_SYMBOL; }

    public final static int EOFT_SYMBOL = 89;
    public final int getEoftSymbol() { return EOFT_SYMBOL; }

    public final static int EOLT_SYMBOL = 104;
    public final int getEoltSymbol() { return EOLT_SYMBOL; }

    public final static int ACCEPT_ACTION = 352;
    public final int getAcceptAction() { return ACCEPT_ACTION; }

    public final static int ERROR_ACTION = 353;
    public final int getErrorAction() { return ERROR_ACTION; }

    public final static boolean BACKTRACK = false;
    public final boolean getBacktrack() { return BACKTRACK; }

    public final int getStartSymbol() { return lhs(0); }
    public final boolean isValidForParser() { return RCLexersym.isValidForParser; }


    public interface IsNullable {
        public final static byte isNullable[] = {0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,1,0,0,0,0,0,0,
            0,0,0,0,0,0
        };
    };
    public final static byte isNullable[] = IsNullable.isNullable;
    public final boolean isNullable(int index) { return isNullable[index] != 0; }

    public interface ProsthesesIndex {
        public final static byte prosthesesIndex[] = {0,
            17,23,24,25,16,30,29,7,8,20,
            21,22,27,28,2,3,4,5,6,9,
            10,11,12,13,14,15,18,19,26,31,
            32,33,1
        };
    };
    public final static byte prosthesesIndex[] = ProsthesesIndex.prosthesesIndex;
    public final int prosthesesIndex(int index) { return prosthesesIndex[index]; }

    public interface IsKeyword {
        public final static byte isKeyword[] = {0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0
        };
    };
    public final static byte isKeyword[] = IsKeyword.isKeyword;
    public final boolean isKeyword(int index) { return isKeyword[index] != 0; }

    public interface BaseCheck {
        public final static byte baseCheck[] = {0,
            1,1,1,1,2,2,1,3,3,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,2,2,2,1,1,1,1,2,
            1,1,1,2,1,1,1,1,2,5,
            1,1,1,1,1,1,1,1,1,1,
            2,2,2,2,2,2,2,2,2,2,
            2,2,1,1,1,1,1,1,1,1,
            1,2,2,2,2,2,2,2,2,2,
            2,2,2,2,1,1,1,2,2,2,
            2,1,2,3,2,3,3,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,2,1,2,2,2,0,1,1,2,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1,1,1,1,1,2,1,1,1,1,
            1,1,1,1,1,1,1,1,1,1,
            1
        };
    };
    public final static byte baseCheck[] = BaseCheck.baseCheck;
    public final int baseCheck(int index) { return baseCheck[index]; }
    public final static byte rhs[] = baseCheck;
    public final int rhs(int index) { return rhs[index]; };

    public interface BaseAction {
        public final static char baseAction[] = {
            15,15,15,15,15,15,15,15,15,15,
            15,15,15,15,15,15,15,15,15,15,
            15,15,15,15,15,15,15,15,15,15,
            15,15,15,15,15,15,15,8,15,9,
            19,24,25,25,25,25,25,25,25,25,
            25,25,25,25,25,25,25,25,25,25,
            25,25,25,26,26,26,26,26,26,26,
            26,26,26,26,26,26,26,26,26,26,
            26,26,26,26,26,17,18,20,20,20,
            27,27,5,5,28,10,10,10,12,12,
            12,12,12,2,2,2,2,3,3,3,
            3,3,3,3,3,3,3,3,3,3,
            3,3,3,3,3,3,3,3,3,3,
            3,3,3,4,4,4,4,4,4,4,
            4,4,4,4,4,4,4,4,4,4,
            4,4,4,4,4,4,4,4,4,1,
            1,1,1,1,1,1,1,1,1,11,
            11,23,23,16,16,16,16,21,21,29,
            29,13,13,13,13,13,14,14,6,6,
            6,6,6,6,6,6,6,6,6,6,
            6,6,6,6,6,6,6,6,6,6,
            6,6,6,6,6,6,7,30,30,30,
            30,31,31,31,31,22,22,22,22,22,
            32,32,393,347,346,346,346,307,584,288,
            36,38,488,288,348,758,737,242,269,245,
            3,4,7,5,6,246,315,512,172,86,
            276,197,350,350,350,350,748,350,350,495,
            175,174,174,174,350,350,756,99,337,337,
            337,337,338,337,337,757,88,238,640,93,
            291,295,180,180,180,180,760,180,180,337,
            596,607,327,762,180,180,327,566,93,337,
            1,216,216,216,216,763,216,89,238,618,
            329,629,331,690,329,339,331,657,93,668,
            93,679,93,701,82,712,81,490,761,767,
            216,216,734,482,745,486,764,759,771,40,
            773,353,353
        };
    };
    public final static char baseAction[] = BaseAction.baseAction;
    public final int baseAction(int index) { return baseAction[index]; }
    public final static char lhs[] = baseAction;
    public final int lhs(int index) { return lhs[index]; };

    public interface TermCheck {
        public final static byte termCheck[] = {0,
            0,1,2,3,4,5,6,7,8,9,
            10,11,12,13,14,15,16,17,18,19,
            20,21,22,23,24,25,26,27,28,29,
            30,31,32,33,34,35,36,37,38,39,
            40,41,42,43,44,45,46,47,48,49,
            50,51,52,53,54,55,56,57,58,59,
            60,61,62,63,64,65,66,67,68,69,
            70,71,72,73,74,75,76,77,78,79,
            80,81,82,83,84,85,86,87,88,0,
            90,91,92,93,94,95,96,97,0,1,
            2,3,4,5,6,7,8,9,10,11,
            12,13,14,15,16,17,18,19,20,21,
            22,23,24,25,26,27,28,29,30,31,
            32,33,34,35,36,37,38,39,40,41,
            42,43,44,45,46,47,48,49,50,51,
            52,53,54,55,56,57,58,59,60,61,
            62,63,64,65,66,67,68,69,70,71,
            72,73,0,75,76,77,78,79,80,81,
            82,83,84,85,86,87,88,0,90,91,
            92,93,94,95,96,97,0,1,2,3,
            4,5,6,7,8,9,10,11,12,13,
            14,15,16,17,18,19,20,21,22,23,
            24,25,26,27,28,29,30,31,32,33,
            34,35,36,37,38,39,40,41,42,43,
            44,45,46,47,48,49,50,51,52,53,
            54,55,56,57,58,59,60,61,62,63,
            64,65,66,67,68,69,70,71,72,0,
            74,75,76,77,78,79,80,81,82,83,
            84,85,86,87,88,0,90,91,92,93,
            94,95,96,97,0,1,2,3,4,5,
            6,7,8,9,10,11,12,13,14,15,
            16,17,18,19,20,21,22,23,24,25,
            26,27,28,29,30,31,32,33,34,35,
            36,37,38,39,40,41,42,43,44,45,
            46,47,48,49,50,51,52,53,54,55,
            56,57,58,59,60,61,62,63,64,65,
            66,67,68,69,70,71,72,0,74,75,
            76,77,78,79,80,81,82,83,84,85,
            86,87,88,0,90,91,92,93,94,95,
            96,97,0,1,2,3,4,5,6,7,
            8,9,10,11,12,13,14,15,16,17,
            18,19,20,21,22,23,24,25,26,27,
            28,29,30,31,32,33,34,35,36,37,
            38,39,40,41,42,43,44,45,46,47,
            48,49,50,51,52,53,54,55,56,57,
            58,59,60,61,62,63,64,65,66,67,
            68,69,70,71,72,73,74,75,76,77,
            78,79,80,81,82,83,84,85,86,87,
            88,0,1,2,3,0,1,0,3,0,
            98,99,100,101,0,1,2,3,4,5,
            6,7,8,9,10,0,12,13,0,15,
            16,0,0,19,20,21,22,23,24,25,
            26,27,28,29,30,31,32,33,34,35,
            36,37,38,39,40,41,42,43,44,45,
            46,47,48,49,50,51,52,53,54,55,
            56,57,58,59,60,61,62,63,64,65,
            66,67,68,74,70,0,1,2,3,4,
            5,6,7,8,9,10,89,12,13,14,
            15,16,71,0,1,2,3,4,5,6,
            7,8,9,10,11,0,1,2,3,4,
            5,6,7,8,9,10,0,1,2,3,
            4,5,6,7,8,9,10,0,1,2,
            3,4,5,6,7,8,9,10,0,1,
            2,3,4,5,6,7,8,9,10,0,
            1,2,3,4,5,6,7,8,9,10,
            0,0,0,0,89,72,0,1,2,3,
            4,5,6,7,8,9,10,0,1,2,
            3,4,5,6,7,8,9,10,0,1,
            2,3,4,5,6,7,8,9,10,0,
            1,2,3,4,5,6,7,8,9,10,
            0,1,2,3,4,5,6,7,8,9,
            10,0,1,2,3,4,5,6,7,8,
            9,10,0,1,2,3,4,5,6,7,
            8,9,10,0,1,2,0,4,5,6,
            7,8,9,10,0,1,2,0,4,5,
            6,7,8,9,10,0,0,0,0,0,
            0,0,0,0,17,18,0,12,13,11,
            0,14,0,11,18,14,17,11,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,69,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,73,98,99,100,101,0,0,
            0,0,0,0,0,73,0,89,0,0,
            0,0,89,0,0,0,0,0,0,89,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0
        };
    };
    public final static byte termCheck[] = TermCheck.termCheck;
    public final int termCheck(int index) { return termCheck[index]; }

    public interface TermAction {
        public final static char termAction[] = {0,
            353,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,569,
            569,569,569,569,569,569,569,569,569,353,
            569,569,569,569,569,569,569,569,353,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,337,337,337,
            337,337,353,337,337,337,337,337,337,337,
            337,337,337,337,337,337,337,353,337,337,
            337,337,337,337,310,337,177,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,350,350,350,350,353,
            350,350,350,350,350,350,350,350,350,350,
            350,350,350,350,350,353,350,350,350,350,
            350,350,310,350,178,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,533,533,533,533,353,533,533,
            533,533,533,533,533,533,533,533,533,533,
            533,533,533,353,533,533,533,533,533,533,
            310,533,353,347,347,347,347,347,347,347,
            347,347,347,296,346,346,303,346,346,285,
            375,346,346,346,346,346,346,346,346,346,
            346,346,346,346,346,346,346,346,346,346,
            346,346,346,346,346,346,346,346,346,346,
            346,346,346,346,346,346,346,346,346,346,
            346,346,346,346,346,346,346,346,346,346,
            346,348,365,257,364,261,277,368,384,367,
            388,381,382,379,380,385,266,370,369,371,
            372,42,414,415,413,65,437,353,436,353,
            348,348,348,348,1,528,528,528,528,528,
            528,528,528,528,528,353,527,527,353,527,
            527,33,353,527,527,527,527,527,527,527,
            527,527,527,527,527,527,527,527,527,527,
            527,527,527,527,527,527,527,527,527,527,
            527,527,527,527,527,527,527,527,527,527,
            527,527,527,527,527,527,527,527,527,527,
            527,527,527,362,529,41,446,446,446,446,
            446,446,446,446,446,446,352,238,238,1307,
            444,443,387,353,288,288,288,288,288,288,
            288,288,288,288,321,353,343,339,342,339,
            339,339,339,339,339,339,353,327,327,327,
            327,327,327,327,327,327,327,353,329,329,
            329,329,329,329,329,329,329,329,353,331,
            331,331,331,331,331,331,331,331,331,95,
            446,446,446,446,446,446,446,446,446,446,
            353,353,353,353,85,319,94,446,446,446,
            446,446,446,446,446,446,446,97,446,446,
            446,446,446,446,446,446,446,446,96,446,
            446,446,446,446,446,446,446,446,446,353,
            335,333,344,345,393,393,393,393,393,393,
            64,435,435,435,435,435,435,435,435,435,
            435,63,434,434,434,434,434,434,434,434,
            434,434,85,301,301,301,301,301,301,301,
            301,301,301,353,404,405,10,406,407,408,
            409,410,411,412,353,425,426,21,427,428,
            429,430,431,432,433,7,20,2,92,13,
            353,37,353,173,378,377,353,238,238,41,
            171,303,179,300,376,392,383,323,353,353,
            353,353,353,353,353,353,353,353,353,353,
            353,353,353,353,353,353,353,353,353,353,
            353,353,353,353,353,525,353,353,353,353,
            353,353,353,353,353,353,353,353,353,353,
            353,353,353,353,353,353,353,353,353,353,
            353,353,353,361,525,525,525,525,353,353,
            353,353,353,353,353,178,353,85,353,353,
            353,353,1,353,353,353,353,353,353,10
        };
    };
    public final static char termAction[] = TermAction.termAction;
    public final int termAction(int index) { return termAction[index]; }
    public final int asb(int index) { return 0; }
    public final int asr(int index) { return 0; }
    public final int nasb(int index) { return 0; }
    public final int nasr(int index) { return 0; }
    public final int terminalIndex(int index) { return 0; }
    public final int nonterminalIndex(int index) { return 0; }
    public final int scopePrefix(int index) { return 0;}
    public final int scopeSuffix(int index) { return 0;}
    public final int scopeLhs(int index) { return 0;}
    public final int scopeLa(int index) { return 0;}
    public final int scopeStateSet(int index) { return 0;}
    public final int scopeRhs(int index) { return 0;}
    public final int scopeState(int index) { return 0;}
    public final int inSymb(int index) { return 0;}
    public final String name(int index) { return null; }
    public final int originalState(int state) { return 0; }
    public final int asi(int state) { return 0; }
    public final int nasi(int state) { return 0; }
    public final int inSymbol(int state) { return 0; }

    /**
     * assert(! goto_default);
     */
    public final int ntAction(int state, int sym) {
        return baseAction[state + sym];
    }

    /**
     * assert(! shift_default);
     */
    public final int tAction(int state, int sym) {
        int i = baseAction[state],
            k = i + sym;
        return termAction[termCheck[k] == sym ? k : i];
    }
    public final int lookAhead(int la_state, int sym) {
        int k = la_state + sym;
        return termAction[termCheck[k] == sym ? k : la_state];
    }
}
