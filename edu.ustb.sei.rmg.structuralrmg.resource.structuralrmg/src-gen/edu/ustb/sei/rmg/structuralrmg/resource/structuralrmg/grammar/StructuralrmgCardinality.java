/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public enum StructuralrmgCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
