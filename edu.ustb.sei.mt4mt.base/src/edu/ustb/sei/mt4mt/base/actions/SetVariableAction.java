package edu.ustb.sei.mt4mt.base.actions;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class SetVariableAction implements IAction {
	private String key;
	private String value;

	public SetVariableAction(String k, String v) {
		key = k;
		value = v;
	}

	@Override
	public void run(Context context) {
		context.putData(key, value);
	}

}
