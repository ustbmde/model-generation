/**
 */
package edu.ustb.sei.rmg.structuralrmg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getRole <em>Role</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getBound <em>Bound</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateNode()
 * @model abstract="true"
 * @generated
 */
public interface TemplateNode extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Role)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateNode_Role()
	 * @model
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>Bound</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound</em>' containment reference.
	 * @see #setBound(Bound)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateNode_Bound()
	 * @model containment="true"
	 * @generated
	 */
	Bound getBound();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateNode#getBound <em>Bound</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound</em>' containment reference.
	 * @see #getBound()
	 * @generated
	 */
	void setBound(Bound value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getMax();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getMin();

} // TemplateNode
