package edu.ustb.sei.mt4mt.base.test;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

public class TestRunner {
	private PrintStream out;
	private List<AbstractTransformationTestCase> testcases;
	private IFile logFile;
	private String title;
	
	public TestRunner(PrintStream out, IFile logFile, String title) {
		this(out,title);
		this.logFile = logFile;
	}
	
	public TestRunner(PrintStream out, String title) {
		testcases = new ArrayList<AbstractTransformationTestCase>();
		this.out = out;
		this.title = title;
	}
	
	public void addTest(AbstractTransformationTestCase tc) {
		testcases.add(tc);
	}
	
	public int getTaskCount() {
		return testcases.size();
	}
	
	protected void print(Object o) {
		if(out!=null) out.print(o);
		if(logFile!=null) {
			try {
				ByteArrayInputStream bis = new ByteArrayInputStream(o.toString().getBytes());
				if(logFile.exists()) {
					logFile.appendContents(bis,true,false,null);
					bis.close();
				} else {
					logFile.create(bis, true, null);
					bis.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	protected void println(Object o) {
		print(o);
		print("\n");
	}
	
	public void run(IProgressMonitor monitor) {
		println(this.title);
		
		int failedCount = 0;
		
		for(AbstractTransformationTestCase tc : testcases) {
			try{
				tc.setUp();
				tc.test();
				tc.tearDown();
				print(tc.getIdentifier());
				print(" passed");
				print(" <");
				print(tc.getExtraMessage());
				println(">");
			} catch(Exception | Error e) {
				failedCount++;
				print(tc.getIdentifier());
				print(" failed");
				print(" <");
				print(tc.getExtraMessage());
				println(">");
				println("\tDue to the following reason:");
				print("\t\t");
				println(e.getMessage());
				e.printStackTrace();
			}
			
			if(monitor!=null) {
				monitor.worked(1);
				if(monitor.isCanceled())
					return;
			}
		}
		if(failedCount==0) {
			println("Result: all the test cases passed");
		} else {
			println("Result: "+failedCount+" of "+testcases.size()+" tests failed!");
		}
	}

}
