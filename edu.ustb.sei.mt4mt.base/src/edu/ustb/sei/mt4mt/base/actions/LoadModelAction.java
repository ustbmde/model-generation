package edu.ustb.sei.mt4mt.base.actions;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;

public class LoadModelAction implements IAction {

	public LoadModelAction(String modelURI, String key) {
		super();
		this.key = key;
		this.modelURI = modelURI;
	}

	private String modelURI;
	private String key;
	
	@Override
	public void run(Context context) {
		ResourceSet tempSet = context.createResourceSet();
		URI uri = URI.createURI(modelURI);
		Resource res = tempSet.getResource(uri, true);
		if(res==null)
			throw new InvalidTestException("cannot load "+modelURI, false);
		context.putData(key, res.getContents());
	}

}
