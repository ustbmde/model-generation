/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class StructuralrmgDebugMessage {
	
	private static final char DELIMITER = ':';
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes messageType;
	private String[] arguments;
	
	public StructuralrmgDebugMessage(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public StructuralrmgDebugMessage(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes messageType, java.util.List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		java.util.List<String> parts = new java.util.ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.encode(DELIMITER, parts);
	}
	
	public static StructuralrmgDebugMessage deserialize(String response) {
		java.util.List<String> parts = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes type = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes.valueOf(messageType);
		StructuralrmgDebugMessage message = new StructuralrmgDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.EStructuralrmgDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.explode(arguments, ", ") + "]";
	}
	
}
