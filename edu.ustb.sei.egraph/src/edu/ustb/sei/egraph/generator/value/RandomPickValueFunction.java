package edu.ustb.sei.egraph.generator.value;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomPickValueFunction<T> extends RandomValueInRangeFunction<T> {
	public RandomPickValueFunction(String key) {
		super();
		this.key = key;
	}

	private String key;

	@SuppressWarnings("unchecked")
	@Override
	public T getValue(RandomService t) {
		List<Object> values = t.getGeneratedValues(key);
		
		int pos = t.randomInt(values.size());
		
		if(pos>=0)
			return (T) values.get(pos);
		else 
			return null;
	}
	
	private Set<String> required = null;
	@Override
	public Set<String> requiredScopes() {
		if(required==null) {
			required = new HashSet<String>();
			required.add(key);
		}
		return required;
	}
}
