package edu.ustb.sei.mde.mt4mt.actions;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;

abstract public class AbstractEMFAction extends Action implements ISelectionChangedListener {

	public AbstractEMFAction() {
		// TODO Auto-generated constructor stub
	}

	public AbstractEMFAction(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public AbstractEMFAction(String text, ImageDescriptor image) {
		super(text, image);
		// TODO Auto-generated constructor stub
	}

	public AbstractEMFAction(String text, int style) {
		super(text, style);
		// TODO Auto-generated constructor stub
	}
	
	protected ISelectionProvider selectionProvider;
	public ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	public void setSelectionProvider(ISelectionProvider selectionProvider) {
		this.selectionProvider = selectionProvider;
	}
	
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		selectionProvider = event.getSelectionProvider();
		if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	abstract protected boolean updateSelection(IStructuredSelection selection);

}
