package edu.ustb.sei.mt4mt.base.test;

import edu.ustb.sei.mt4mt.base.Actions;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;
import edu.ustb.sei.mt4mt.base.exceptions.NonTerminationException;

public class WrapperTestCase extends AbstractTransformationTestCase {

	public WrapperTestCase(IAction before, IAction after, AbstractTransformationTestCase core, int repeat) {
		this.beforeTest = before == null ? Actions.skip() : before;
		this.afterTest = after == null ? Actions.skip() : after;
		this.coreTest = core;
		this.repeat = repeat;
	}
	protected String extraMessage = null;
	protected IAction beforeTest;
	protected IAction afterTest;
	protected AbstractTransformationTestCase coreTest;
	protected int repeat;
	
	@Override
	public void setUp() {
		coreTest.setUp();
		this.context = coreTest.context;
		this.loadMetamodels();
	}
	
	@Override
	public void test() {
		int i = 0;
		extraMessage = null;
		try {
			boolean nonTerm = true;
			for( i = 0; i< this.repeat; i++) {
				try {
					this.beforeTest.run(context);
					coreTest.test();
					this.afterTest.run(context);
					nonTerm = false;
				} catch(InvalidTestException e) {
					System.out.println(e.getMessage());
					if(e.shouldRetry()==false) {
						extraMessage = "interrupted after "+(i+1)+" attempts due to "+e.getMessage();
						throw e;
					}
				}
			}
			if(nonTerm)
				throw new NonTerminationException("failed due to that all runs are invalid");
			if(i==this.repeat) {
				extraMessage = "finished after "+(i)+" attempts";
			}
		} catch(Exception | Error e) {
			extraMessage = "stopped after "+(i+1)+" attempts";
			throw e;
		}
	}
	
	@Override
	public String getExtraMessage() {
		return extraMessage==null ? super.getExtraMessage() : extraMessage;
	}
	
	@Override
	public void tearDown() {
		super.tearDown();
		coreTest.tearDown();
	}

}
