package edu.ustb.sei.mt4mt.base;

public interface IAction {
	void run(Context context);
}
