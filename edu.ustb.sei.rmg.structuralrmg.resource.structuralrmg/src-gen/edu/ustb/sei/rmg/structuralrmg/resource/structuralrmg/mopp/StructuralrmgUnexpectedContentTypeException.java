/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * An Excpetion to represent invalid content types for parser instances.
 * 
 * @see
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.RESO
 * URCE_CONTENT_TYPE
 */
public class StructuralrmgUnexpectedContentTypeException extends org.antlr.runtime3_4_0.RecognitionException {
	
	private static final long serialVersionUID = 4791359811519433999L;
	
	private Object contentType = null;
	
	public  StructuralrmgUnexpectedContentTypeException(Object contentType) {
		this.contentType = contentType;
	}
	
	public Object getContentType() {
		return contentType;
	}
	
}
