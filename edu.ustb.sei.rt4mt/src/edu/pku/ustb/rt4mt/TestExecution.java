/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Execution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.TestExecution#getTestCase <em>Test Case</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.TestExecution#isPassed <em>Passed</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestExecution()
 * @model
 * @generated
 */
public interface TestExecution extends EObject {
	/**
	 * Returns the value of the '<em><b>Test Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Case</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Case</em>' reference.
	 * @see #setTestCase(TestCase)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestExecution_TestCase()
	 * @model
	 * @generated
	 */
	TestCase getTestCase();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.TestExecution#getTestCase <em>Test Case</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Case</em>' reference.
	 * @see #getTestCase()
	 * @generated
	 */
	void setTestCase(TestCase value);

	/**
	 * Returns the value of the '<em><b>Passed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Passed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Passed</em>' attribute.
	 * @see #setPassed(boolean)
	 * @see edu.pku.ustb.rt4mt.Rt4mtPackage#getTestExecution_Passed()
	 * @model
	 * @generated
	 */
	boolean isPassed();

	/**
	 * Sets the value of the '{@link edu.pku.ustb.rt4mt.TestExecution#isPassed <em>Passed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Passed</em>' attribute.
	 * @see #isPassed()
	 * @generated
	 */
	void setPassed(boolean value);

} // TestExecution
