/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wrapper Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getBeforeTest <em>Before Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getAfterTest <em>After Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getCoreTest <em>Core Test</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getRepeat <em>Repeat</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getWrapperTestCase()
 * @model
 * @generated
 */
public interface WrapperTestCase extends TestCase {
	/**
	 * Returns the value of the '<em><b>Before Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Before Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Before Test</em>' containment reference.
	 * @see #setBeforeTest(TestAction)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getWrapperTestCase_BeforeTest()
	 * @model containment="true"
	 * @generated
	 */
	TestAction getBeforeTest();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getBeforeTest <em>Before Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Before Test</em>' containment reference.
	 * @see #getBeforeTest()
	 * @generated
	 */
	void setBeforeTest(TestAction value);

	/**
	 * Returns the value of the '<em><b>After Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>After Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>After Test</em>' containment reference.
	 * @see #setAfterTest(TestAction)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getWrapperTestCase_AfterTest()
	 * @model containment="true"
	 * @generated
	 */
	TestAction getAfterTest();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getAfterTest <em>After Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>After Test</em>' containment reference.
	 * @see #getAfterTest()
	 * @generated
	 */
	void setAfterTest(TestAction value);

	/**
	 * Returns the value of the '<em><b>Core Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Core Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Core Test</em>' containment reference.
	 * @see #setCoreTest(TestCase)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getWrapperTestCase_CoreTest()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TestCase getCoreTest();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getCoreTest <em>Core Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Core Test</em>' containment reference.
	 * @see #getCoreTest()
	 * @generated
	 */
	void setCoreTest(TestCase value);

	/**
	 * Returns the value of the '<em><b>Repeat</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repeat</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repeat</em>' attribute.
	 * @see #setRepeat(int)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getWrapperTestCase_Repeat()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getRepeat();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.WrapperTestCase#getRepeat <em>Repeat</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repeat</em>' attribute.
	 * @see #getRepeat()
	 * @generated
	 */
	void setRepeat(int value);

} // WrapperTestCase
