package edu.ustb.sei.rmg.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;


import edu.ustb.sei.rmg.random.condition.Condition;
import edu.ustb.sei.rmg.random.condition.NullCondition;
import edu.ustb.sei.rmg.strategy.IStringGenerationStrategy;
import edu.ustb.sei.rmg.util.HashableConcurrentHashMap;
import edu.ustb.sei.rmg.util.Pair;

public class RandomUtil {
	private Random random;
	private ScaledRandomUtil scaleRandom = null;
	
	public void setFactor(int factor) {
		if(scaleRandom==null) {
			scaleRandom = new ScaledRandomUtil();
		}
		scaleRandom.factor = factor;
	}
	
	public Random getRandom() {
		return random;
	}

	private IStringGenerationStrategy stringGenerationStrategy = IStringGenerationStrategy.defaultStrategy;
	private static final String DEFAULT_STRING_SCOPE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_";
	
	/*
	 * �����������
	 */
	
	public RandomUtil() {
		random = new Random(System.currentTimeMillis());
	}
	
	public byte randomByte() {
		// TODO Auto-generated method stub
		return (byte)randomUIntU(0xFF);
	}
	
	public int randomInt(){
		return random.nextInt();
	}
	
	public int randomIntU(int upper){
		return upper-random.nextInt();
	}
	
	public int randomIntL(int lower){
		return lower+Math.abs(random.nextInt());
	}
	
	public int randomInt(int lower, int upper){
		if(upper==lower) return upper;
		if(upper>lower)
			return lower+Math.abs(random.nextInt())%(upper-lower+1);
		return 0;
	}
	
	public long randomLong(long lower, long upper){
		if(upper==lower) return upper;
		if(upper>lower)
			return lower+Math.abs(random.nextLong())%(upper-lower+1);
		return 0L;
	}
	
	public long randomLongU(long upper){
		return upper-Math.abs(random.nextLong());
	}
	
	public long randomLongL(long lower){
		return lower+Math.abs(random.nextLong());
	}
	
	public int randomUInt() {
		return Math.abs(randomInt());
	}
	
	public int randomUIntU(int upper) {
		if(upper==0) return 0;
		if(upper>0)
			return randomUInt()%upper;
		return 0;
	}
	public int randomUIntL(int lower) {
		if(lower>=0)
			return lower+randomUInt();
		return 0;
	}
	public int randomUInt(int lower, int upper) {
		if(lower==upper)
			return upper;
		if(upper>lower)
			return lower+randomUIntU(upper-lower+1);
		return 0;
	}	
	
	public float randomFloat(){
		return random.nextFloat();
	}
	
	public double randomDouble() {
		return random.nextDouble();
	}
	
	public double randomDouble(double a, double b) {
		return a + randomDouble() * (b-a);
	}
	
	public boolean randomBoolean(){
		return random.nextBoolean();
	}
	
	public long randomLong(){
		return random.nextLong();
	}
	
	public String randomStringByScope(String scope, Object[] size) {
		if(scope==null) scope = DEFAULT_STRING_SCOPE;
		return stringGenerationStrategy.generateString(this,scope,size);
	}
	public char randomChar() {
		return (char)randomUIntU(0xffff);
	}
	
	public char randomCharLU(int lower, int upper){
		char c = (char)(randomUInt(lower, upper));
		return c;
	}
	
	/*
	 * ���ֲַ�����
	 */
	
	/**
	 * ����һ�����booleanֵ,��p��ʾ�˲���ֵΪ��ĸ���
	 * @param p 0~1 ֮���doubleֵ,��ʾ����boolean��ֵ�Ŀ�����
	 */
	public boolean bernoulli(double p) {
		return randomDouble() < p;
	}
	
	public double gaussian() {
		return random.nextGaussian();
	}
	
	/**
	 * ����һ������ƽ��ֵΪmean,��׼��Ϊstddev����̬�ֲ���ʵ��
	 * @param mean ��̬�ֲ���ƽ��ֵ
	 * @param stddev ��̫�ֲ��ı�׼��
	 */
	public double gaussian(double mean, double stddev) {
		return mean + stddev * gaussian();
	}
	
	
	
	/**
	 * ����һ�����㼸�ηֲ�������ֵ ƽ��ֵΪ1/p
	 */
	public long geometric(double p) {
		// Knuth
		return (long) Math.ceil(Math.log(randomDouble()) / Math.log(1.0 - p));
	}
	
	/**
	 * ����ָ���Ĳ�������һ�����㲴�ɷֲ���ʵ��
	 */
	public long poisson(double lambda) {
		// ʹ�� Knuth ���㷨
		// �μ� http://en.wikipedia.org/wiki/Poisson_distribution
		int k = 0;
		double p = 1.0;
		double L = Math.exp(-lambda);
		do {
			k++;
			p *= randomDouble();
		} while (p >= L);
		return k-1;
	}
	
	/**
	 * ����ָ���Ĳ���������һ�����������зֲ���ʵ��
	 */
	public double pareto(double alpha) {
		return Math.pow(1 - randomDouble(), -1.0/alpha) - 1.0;
	}
	
	/**
	 * ����һ����������ֲ���ʵ��
	 */
	public double cauchy() {
		return Math.tan(Math.PI * (randomDouble() - 0.5));
	}
	
	/**
	* ����һ��������ɢ�ֲ���int���͵���
	* @param a �㷨�����������������Ҫʹ�ô���������ݣ�a[i]����i���ֵĸ���
	*      ǰ������ a[i] �Ǹ��кͽӽ� 1.0
	*/
	public int randomDiscrete(double[] a) {
		double EPSILON = 1E-14;
		double sum = 0.0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] < 0.0) throw new IllegalArgumentException("����Ԫ�� " + i + " Ϊ����: " + a[i]);
			sum = sum + a[i];
		}
		if (sum > 1.0 + EPSILON || sum < 1.0 - EPSILON)
			throw new IllegalArgumentException("�������Ԫ��֮��Ϊ: " + sum);
		
		while (true) {
			double r = randomDouble();
			sum = 0.0;
			for (int i = 0; i < a.length; i++) {
				sum = sum + a[i];
				if (sum > r) return i;
			}
		}
	}
	
	public <T> T randomDiscrete(Pair<T,Double>[] a) {
		double EPSILON = 1E-14;
		double sum = 0.0;
		for (int i = 0; i < a.length; i++) {
			if (a[i].second < 0.0) throw new IllegalArgumentException("����Ԫ�� " + i + " Ϊ����: " + a[i]);
			sum = sum + a[i].second;
		}
		
		if (sum > 1.0 + EPSILON || sum < 1.0 - EPSILON)
			throw new IllegalArgumentException("�������Ԫ��֮��Ϊ: " + sum);
		
		while (true) {
			double r = randomDouble();
			sum = 0.0;
			for (int i = 0; i < a.length; i++) {
				sum = sum + a[i].second;
				if (sum > r) return a[i].first;
			}
		}
	}
	
	/**
	 * ����һ������ָ���ֲ���ʵ������ָ���ֲ�����Ϊlambda
	 */
	public double exp(double lambda) {
		return -Math.log(1 - randomDouble()) / lambda;
	}
	
	/*
	 * 
	 */
	
	static public void main(String[] argv) {
		//RandomUtil randomUtil = new RandomUtil();
		String exp ="[2013-11-1..2013-11-31]";
		RandomUtil r = new RandomUtil();
		System.out.println(r.randomDate(exp));
		
		
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.clear(Calendar.MILLISECOND);
		c2.clear(Calendar.MILLISECOND);
		
		c2.add(Calendar.DAY_OF_MONTH, 1);
		
		long dif = c2.getTimeInMillis() - c1.getTimeInMillis();
		
		String literal = "1983-12-24";
		
		String buf[] = literal.split("-");
		int year = Integer.parseInt(buf[0]);
		int mon = Integer.parseInt(buf[1]);
		int day = Integer.parseInt(buf[2]);
		
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, mon-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		
		
		System.out.println(c.getTime());
		
		//System.out.println(c);
		
		try {
			throw new Error("hello");
		} catch(Error er) {
			er.printStackTrace();
		}
		
	}
	
	/*
	 * ��������������������ĺ���
	 */
	
	public Date randomDate(String condition, Object... extraArgv) {
		Condition<Calendar> it = (Condition<Calendar>)getParsedCondition(condition);
		Calendar c = it.getValue(this, extraArgv);
		return c.getTime();
	}
	
	public int randomIntWithScale(String condition, Object... extraArgv) {
		if(condition==null)
			return randomInt();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this.scaleRandom==null ? this : this.scaleRandom, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Integer)
			return (Integer)rawValue;
		else if(rawValue instanceof Long)
			return (int)((long)rawValue);
		else if(rawValue instanceof Double) {
			double d = (Double) rawValue;
			return (int)d;
		} else if(rawValue instanceof String) {
			return Integer.parseInt(rawValue.toString());
		}else return 0;
	}

	public int randomInt(String condition, Object... extraArgv) {
		if(condition==null)
			return randomInt();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Integer)
			return (Integer)rawValue;
		else if(rawValue instanceof Long)
			return (int)((long)rawValue);
		else if(rawValue instanceof Double) {
			double d = (Double) rawValue;
			return (int)d;
		} else if(rawValue instanceof String) {
			return Integer.parseInt(rawValue.toString());
		}else return 0;
	}
	
	public byte randomByte(String condition, Object... extraArgv) {
		if(condition==null)
			return randomByte();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Integer)
			return (byte)((int)rawValue);
		else if(rawValue instanceof Long)
			return (byte)((long)rawValue);
		else if(rawValue instanceof Double) {
			double d = (double) rawValue;
			return (byte)d;
		} else return 0;
	}
	
	public char randomChar(String condition, Object... extraArgv) {
		if(condition==null)
			return randomChar();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Integer)
			return (char)((int)rawValue);
		else if(rawValue instanceof Long)
			return (char)((long)rawValue);
		else if(rawValue instanceof Character) return (Character)rawValue;
		else return 0;
	}
	
	public String randomString(String condition, Object... extraArgv) {
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return "";
		else if(rawValue instanceof String)
			return (String)rawValue;
		else return rawValue.toString();
	}
	
	public double randomDouble(String condition, Object... extraArgv) {
		if(condition==null)
			return randomDouble();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Double)
			return (Double)rawValue;
		else if(rawValue instanceof Integer) {
			int d = (Integer) rawValue;
			return (double)d;
		} else if(rawValue instanceof Long) {
			long d = (Long) rawValue;
			return (double)d;
		} else return 0.0;
	}
	
	public float randomFloat(String condition, Object... extraArgv) {
		if(condition==null)
			return randomFloat();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0.0f;
		else if(rawValue instanceof Double)
			return ((Double)rawValue).floatValue();
		else if(rawValue instanceof Integer) {
			int d = (Integer) rawValue;
			return (float)d;
		} else if(rawValue instanceof Long) {
			long d = (Long) rawValue;
			return (float)d;
		}else return 0.0f;
	}

	public Condition<?> getParsedCondition(String condition) {
		try {
			Condition<?> it = conditionMap.get(condition);
			if(it == null) {
				it = parseCondition(condition);
				conditionMap.put(condition, it);
			}
			return it;
		} catch (Exception e) {
			e.printStackTrace();
			return NullCondition.instance;
		}
	}

	static public Condition<?> parseCondition(String condition) {
		Condition<?> it;
		RCLexer lexer = new RCLexer(condition.toCharArray(),condition);
		RCParser parser = new RCParser(lexer.getILexStream());
		lexer.lexer(parser.getIPrsStream());
		it = parser.parser();
		return it;
	}
	
	public Object randomEnum(String condition, Object... extraArgv) {
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0;
		else if(rawValue instanceof Integer)
			return (Integer)rawValue;
		else if(rawValue instanceof String) return (String)rawValue;
		else return 0;
	}
	
	public boolean randomBoolean(String condition, Object... extraArgv) {
		if(condition==null)
			return randomBoolean();
		
		Condition<?> it = getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return false;
		else if(rawValue instanceof Boolean)
			return (Boolean)rawValue;
		else return false;
	}
	
	private HashableConcurrentHashMap<String, Condition<?>> conditionMap = new HashableConcurrentHashMap<String, Condition<?>>();

	public Object randomList(String condition, Object... extraArgv) {
		Condition<?> it = (Condition<?>)getParsedCondition(condition);
		return it.getValue(this, extraArgv);
	}

	public long randomLong(String condition, Object... extraArgv) {
		if(condition==null)
			return randomLong();
		
		Condition<?> it = (Condition<?>)getParsedCondition(condition);
		Object rawValue = it.getValue(this, extraArgv);
		if(rawValue==null) return 0L;
		else if(rawValue instanceof Integer)
			return (long)((int)rawValue);
		else if(rawValue instanceof Long) {
			return (Long)rawValue;
		} else if(rawValue instanceof Double) {
			return (long)((double)rawValue);
		} else if(rawValue instanceof String) {
			return Long.parseLong(rawValue.toString());
		}
		else return 0L;
	}
	
	 
	public <T> T randomConstant(T c) {
		return c;
	}
	
}

class ScaledRandomUtil extends RandomUtil {
	public int factor = 1;

	@Override
	public int randomIntU(int upper) {
		// TODO Auto-generated method stub
		return super.randomIntU(upper*factor);
	}

	@Override
	public int randomIntL(int lower) {
		// TODO Auto-generated method stub
		return super.randomIntL(lower*factor);
	}

	@Override
	public int randomInt(int lower, int upper) {
		// TODO Auto-generated method stub
		return super.randomInt(lower*factor, upper*factor);
	}

	@Override
	public long randomLong(long lower, long upper) {
		// TODO Auto-generated method stub
		return super.randomLong(lower*factor, upper*factor);
	}

	@Override
	public long randomLongU(long upper) {
		// TODO Auto-generated method stub
		return super.randomLongU(upper*factor);
	}

	@Override
	public long randomLongL(long lower) {
		// TODO Auto-generated method stub
		return super.randomLongL(lower*factor);
	}

	@Override
	public int randomUIntU(int upper) {
		// TODO Auto-generated method stub
		return super.randomUIntU(upper*factor);
	}

	@Override
	public int randomUIntL(int lower) {
		// TODO Auto-generated method stub
		return super.randomUIntL(lower*factor);
	}

	@Override
	public int randomUInt(int lower, int upper) {
		// TODO Auto-generated method stub
		return super.randomUInt(lower*factor, upper*factor);
	}

	@Override
	public double randomDouble(double a, double b) {
		// TODO Auto-generated method stub
		return super.randomDouble(a*factor, b*factor);
	}

	@Override
	public <T> T randomConstant(T c) {
		if(c instanceof Integer) 
			return (T) super.randomConstant(((Integer)c)*factor);
		else return super.randomConstant(c);
	}
	
	
}