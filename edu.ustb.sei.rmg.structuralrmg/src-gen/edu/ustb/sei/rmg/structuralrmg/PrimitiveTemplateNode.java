/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Template Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getType <em>Type</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateNode()
 * @model
 * @generated
 */
public interface PrimitiveTemplateNode extends TemplateNode {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(EClass)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateNode_Type()
	 * @model required="true"
	 * @generated
	 */
	EClass getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(EClass value);

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.AttributeBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getPrimitiveTemplateNode_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeBinding> getAttributes();

} // PrimitiveTemplateNode
