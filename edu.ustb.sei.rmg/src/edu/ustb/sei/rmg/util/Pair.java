package edu.ustb.sei.rmg.util;


public class Pair<F,S> {
	public Pair(F f, S s) {
		first = f;
		second = s;
	}
	public F first;
	public S second;
	
//	@Override
//	public boolean equals(Object o) {
//		if(o==this)
//			return true;
//		if(o.getClass() == Pair.class) {
//			return first == ((Pair<?,?>)o).first && second == ((Pair<?,?>)o).second;
//		}
//		return false;
//		
//	}
	
	@Override
	public int hashCode() {
		final int prime = 65535;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		Pair<?,?> other = (Pair<?,?>) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
}
