/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class StructuralrmgRule extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public StructuralrmgRule(org.eclipse.emf.ecore.EClass metaclass, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgChoice choice, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality) {
		super(cardinality, new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgChoice getDefinition() {
		return (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgChoice) getChildren()[0];
	}
	
}

