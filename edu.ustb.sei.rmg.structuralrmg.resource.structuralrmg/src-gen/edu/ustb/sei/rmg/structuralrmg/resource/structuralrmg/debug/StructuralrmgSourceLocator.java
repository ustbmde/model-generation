/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug;

public class StructuralrmgSourceLocator extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector {
	
	public void initializeParticipants() {
		addParticipants(new org.eclipse.debug.core.sourcelookup.ISourceLookupParticipant[]{new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgSourceLookupParticipant()});
	}
	
}
