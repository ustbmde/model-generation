/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Predefined BX Test Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getFwdLauncher <em>Fwd Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getFwdSource <em>Fwd Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getFwdView <em>Fwd View</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getBwdLauncher <em>Bwd Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getBwdSource <em>Bwd Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getBwdView <em>Bwd View</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.PredefinedBXTestCaseImpl#getBwdUpdatedSource <em>Bwd Updated Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PredefinedBXTestCaseImpl extends PredefinedTestCaseImpl implements PredefinedBXTestCase {
	/**
	 * The default value of the '{@link #getFwdLauncher() <em>Fwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFwdLauncher()
	 * @generated
	 * @ordered
	 */
	protected static final String FWD_LAUNCHER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFwdLauncher() <em>Fwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFwdLauncher()
	 * @generated
	 * @ordered
	 */
	protected String fwdLauncher = FWD_LAUNCHER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFwdSource() <em>Fwd Source</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFwdSource()
	 * @generated
	 * @ordered
	 */
	protected EList<String> fwdSource;

	/**
	 * The cached value of the '{@link #getFwdView() <em>Fwd View</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFwdView()
	 * @generated
	 * @ordered
	 */
	protected EList<String> fwdView;

	/**
	 * The default value of the '{@link #getBwdLauncher() <em>Bwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBwdLauncher()
	 * @generated
	 * @ordered
	 */
	protected static final String BWD_LAUNCHER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBwdLauncher() <em>Bwd Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBwdLauncher()
	 * @generated
	 * @ordered
	 */
	protected String bwdLauncher = BWD_LAUNCHER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBwdSource() <em>Bwd Source</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBwdSource()
	 * @generated
	 * @ordered
	 */
	protected EList<String> bwdSource;

	/**
	 * The cached value of the '{@link #getBwdView() <em>Bwd View</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBwdView()
	 * @generated
	 * @ordered
	 */
	protected EList<String> bwdView;

	/**
	 * The cached value of the '{@link #getBwdUpdatedSource() <em>Bwd Updated Source</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBwdUpdatedSource()
	 * @generated
	 * @ordered
	 */
	protected EList<String> bwdUpdatedSource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PredefinedBXTestCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFwdLauncher() {
		return fwdLauncher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFwdLauncher(String newFwdLauncher) {
		String oldFwdLauncher = fwdLauncher;
		fwdLauncher = newFwdLauncher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER, oldFwdLauncher, fwdLauncher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getFwdSource() {
		if (fwdSource == null) {
			fwdSource = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE);
		}
		return fwdSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getFwdView() {
		if (fwdView == null) {
			fwdView = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW);
		}
		return fwdView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBwdLauncher() {
		return bwdLauncher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBwdLauncher(String newBwdLauncher) {
		String oldBwdLauncher = bwdLauncher;
		bwdLauncher = newBwdLauncher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER, oldBwdLauncher, bwdLauncher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getBwdSource() {
		if (bwdSource == null) {
			bwdSource = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE);
		}
		return bwdSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getBwdView() {
		if (bwdView == null) {
			bwdView = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW);
		}
		return bwdView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getBwdUpdatedSource() {
		if (bwdUpdatedSource == null) {
			bwdUpdatedSource = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE);
		}
		return bwdUpdatedSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER:
				return getFwdLauncher();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE:
				return getFwdSource();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW:
				return getFwdView();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER:
				return getBwdLauncher();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE:
				return getBwdSource();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW:
				return getBwdView();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE:
				return getBwdUpdatedSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER:
				setFwdLauncher((String)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE:
				getFwdSource().clear();
				getFwdSource().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW:
				getFwdView().clear();
				getFwdView().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER:
				setBwdLauncher((String)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE:
				getBwdSource().clear();
				getBwdSource().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW:
				getBwdView().clear();
				getBwdView().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE:
				getBwdUpdatedSource().clear();
				getBwdUpdatedSource().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER:
				setFwdLauncher(FWD_LAUNCHER_EDEFAULT);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE:
				getFwdSource().clear();
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW:
				getFwdView().clear();
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER:
				setBwdLauncher(BWD_LAUNCHER_EDEFAULT);
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE:
				getBwdSource().clear();
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW:
				getBwdView().clear();
				return;
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE:
				getBwdUpdatedSource().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER:
				return FWD_LAUNCHER_EDEFAULT == null ? fwdLauncher != null : !FWD_LAUNCHER_EDEFAULT.equals(fwdLauncher);
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE:
				return fwdSource != null && !fwdSource.isEmpty();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW:
				return fwdView != null && !fwdView.isEmpty();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER:
				return BWD_LAUNCHER_EDEFAULT == null ? bwdLauncher != null : !BWD_LAUNCHER_EDEFAULT.equals(bwdLauncher);
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE:
				return bwdSource != null && !bwdSource.isEmpty();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW:
				return bwdView != null && !bwdView.isEmpty();
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE:
				return bwdUpdatedSource != null && !bwdUpdatedSource.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fwdLauncher: ");
		result.append(fwdLauncher);
		result.append(", fwdSource: ");
		result.append(fwdSource);
		result.append(", fwdView: ");
		result.append(fwdView);
		result.append(", bwdLauncher: ");
		result.append(bwdLauncher);
		result.append(", bwdSource: ");
		result.append(bwdSource);
		result.append(", bwdView: ");
		result.append(bwdView);
		result.append(", bwdUpdatedSource: ");
		result.append(bwdUpdatedSource);
		result.append(')');
		return result.toString();
	}

} //PredefinedBXTestCaseImpl
