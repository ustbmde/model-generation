package edu.ustb.sei.mt4mt.base.exceptions;

public class NonTerminationException extends InvalidTestException {

	public NonTerminationException(String message) {
		super(message, false);
	}

}
