package edu.ustb.sei.rmg;


import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.util.OrderedPairMap;

//������LayeredObjectSelectorForRefSrc���Ĳ�ͬ��, refû�ж�Ӧ��
class LayeredObjectSelectorForRefTar extends AbstractLayeredObjectSelector {
	public LayeredObjectSelectorForRefTar(Generator host, EList<EObject> allObjs,EList<EReference> refs,
			boolean required, boolean unique) {
		super(host, allObjs,refs,required,unique);
		currentClass = LayeredObjectSelectorForRefTar.class;
		partition();
	}
	
//	protected EList<EReference> getRefsByTar(EClass c) {
//		EList<EReference> result = refsOfObj.get(currentClass, allRefs, c);
//		if(result!=null) return result;
//		result = new UniqueEList<EReference>();
//		refsOfObj.put(currentClass, allRefs,c,result);
//		for(EReference r : allRefs) {
//			if(getReferenceClass(r).isSuperTypeOf(c))
//				result.add(r);
//		}
//		
//		return result;
//	}
	
	protected int getRefLowerBound(EReference r) {
		int l = 0;
		if(r.getEOpposite()!=null)
			l = r.getEOpposite().getLowerBound();
		
		if(required && l==0 ) l = 1;
		return l;
	}
	
	protected int getRefUpperBound(EReference r) {
		if(r.getEOpposite()!=null)
			return r.getEOpposite().getUpperBound();
		else {
			if(unique) return 1;
			else return -1;//���Ҫ��unique, ��˵��r��Դ�˶�����Ϊ1, �����й�ϵ����Ϊ1
		}
	}
	
	@Override
	public EObject pick() {
		int ri = host.random.randomUInt();
		
		if(first.size()!=0){
			int id = ri % first.size();
			return first.get(id);
		} else if(second.size()!=0) {
			int id = ri % second.size();
			return second.get(id);
		}
		
		return null;
	}
	
//	
//	@Override
//	protected EList<EReference> getRefs(EClass c) {
//		return this.getRefsByTar(c);
//	}

	@Override
	public EClass getReferenceClass(EReference r) {
		return r.getEReferenceType();
	}

	@Override
	protected Iterator<EObject> checkForbiddenConditionForPickObject(EObject opposite, EClass typeFilter,
			List<EObject> candidate, ForbiddenMap forbiddenMap) {
		if(forbiddenMap == null)
			return candidate.iterator();
		return new TarCandidateIterator(opposite, candidate, typeFilter, forbiddenMap, host.random);
//		return forbiddenMap.collectNonForbiddenObjects(opposite, candidate, typeFilter);
	}
	
	
}