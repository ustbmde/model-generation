package edu.ustb.sei.rmg.random;

import lpg.runtime.*;

import java.io.Reader;

public class RCLexer implements RuleAction
{
    private RCLexerLpgLexStream lexStream;
    
    private static ParseTable prs = new RCLexerprs();
    public ParseTable getParseTable() { return prs; }

    private LexParser lexParser = new LexParser();
    public LexParser getParser() { return lexParser; }

    public int getToken(int i) { return lexParser.getToken(i); }
    public int getRhsFirstTokenIndex(int i) { return lexParser.getFirstToken(i); }
    public int getRhsLastTokenIndex(int i) { return lexParser.getLastToken(i); }

    public int getLeftSpan() { return lexParser.getToken(1); }
    public int getRightSpan() { return lexParser.getLastToken(); }

    public void resetKeywordLexer()
    {
        if (kwLexer == null)
              this.kwLexer = new RCKWLexer(lexStream.getInputChars(), RCParsersym.TK_IDENTIFIER);
        else this.kwLexer.setInputChars(lexStream.getInputChars());
    }

    public void reset(String filename, int tab) throws java.io.IOException
    {
        lexStream = new RCLexerLpgLexStream(filename, tab);
        lexParser.reset((ILexStream) lexStream, prs, (RuleAction) this);
        resetKeywordLexer();
    }

    public void reset(char[] input_chars, String filename)
    {
        reset(input_chars, filename, 1);
    }
    
    public void reset(char[] input_chars, String filename, int tab)
    {
        lexStream = new RCLexerLpgLexStream(input_chars, filename, tab);
        lexParser.reset((ILexStream) lexStream, prs, (RuleAction) this);
        resetKeywordLexer();
    }
    
    public RCLexer(String filename, int tab) throws java.io.IOException 
    {
        reset(filename, tab);
    }

    public RCLexer(char[] input_chars, String filename, int tab)
    {
        reset(input_chars, filename, tab);
    }

    public RCLexer(char[] input_chars, String filename)
    {
        reset(input_chars, filename, 1);
    }

    public RCLexer() {}

    public ILexStream getILexStream() { return lexStream; }

    /**
     * @deprecated replaced by {@link #getILexStream()}
     */
    public ILexStream getLexStream() { return lexStream; }

    private void initializeLexer(IPrsStream prsStream, int start_offset, int end_offset)
    {
        if (lexStream.getInputChars() == null)
            throw new NullPointerException("LexStream was not initialized");
        lexStream.setPrsStream(prsStream);
        prsStream.makeToken(start_offset, end_offset, 0); // Token list must start with a bad token
    }

    private void addEOF(IPrsStream prsStream, int end_offset)
    {
        prsStream.makeToken(end_offset, end_offset, RCParsersym.TK_EOF_TOKEN); // and end with the end of file token
        prsStream.setStreamLength(prsStream.getSize());
    }

    public void lexer(IPrsStream prsStream)
    {
        lexer(null, prsStream);
    }
    
    public void lexer(Monitor monitor, IPrsStream prsStream)
    {
        initializeLexer(prsStream, 0, -1);
        lexParser.parseCharacters(monitor);  // Lex the input characters
        addEOF(prsStream, lexStream.getStreamIndex());
    }

    public void lexer(IPrsStream prsStream, int start_offset, int end_offset)
    {
        lexer(null, prsStream, start_offset, end_offset);
    }
    
    public void lexer(Monitor monitor, IPrsStream prsStream, int start_offset, int end_offset)
    {
        if (start_offset <= 1)
             initializeLexer(prsStream, 0, -1);
        else initializeLexer(prsStream, start_offset - 1, start_offset - 1);

        lexParser.parseCharacters(monitor, start_offset, end_offset);

        addEOF(prsStream, (end_offset >= lexStream.getStreamIndex() ? lexStream.getStreamIndex() : end_offset + 1));
    }

    /**
     * If a parse stream was not passed to this Lexical analyser then we
     * simply report a lexical error. Otherwise, we produce a bad token.
     */
    public void reportLexicalError(int startLoc, int endLoc) {
        IPrsStream prs_stream = lexStream.getIPrsStream();
        if (prs_stream == null)
            lexStream.reportLexicalError(startLoc, endLoc);
        else {
            //
            // Remove any token that may have been processed that fall in the
            // range of the lexical error... then add one error token that spans
            // the error range.
            //
            for (int i = prs_stream.getSize() - 1; i > 0; i--) {
                if (prs_stream.getStartOffset(i) >= startLoc)
                     prs_stream.removeLastToken();
                else break;
            }
            prs_stream.makeToken(startLoc, endLoc, 0); // add an error token to the prsStream
        }        
    }

    //
    // The Lexer contains an array of characters as the input stream to be parsed.
    // There are methods to retrieve and classify characters.
    // The lexparser "token" is implemented simply as the index of the next character in the array.
    // The Lexer extends the abstract class LpgLexStream with an implementation of the abstract
    // method getKind.  The template defines the Lexer class and the lexer() method.
    // A driver creates the action class, "Lexer", passing an Option object to the constructor.
    //
    RCKWLexer kwLexer;
    boolean printTokens;
    private final static int ECLIPSE_TAB_VALUE = 4;

    public int [] getKeywordKinds() { return kwLexer.getKeywordKinds(); }

    public RCLexer(String filename) throws java.io.IOException
    {
        this(filename, ECLIPSE_TAB_VALUE);
        this.kwLexer = new RCKWLexer(lexStream.getInputChars(), RCParsersym.TK_IDENTIFIER);
    }

    /**
     * @deprecated function replaced by {@link #reset(char [] content, String filename)}
     */
    public void initialize(char [] content, String filename)
    {
        reset(content, filename);
    }
    
    final void makeToken(int left_token, int right_token, int kind)
    {
        lexStream.makeToken(left_token, right_token, kind);
    }
    
    final void makeToken(int kind)
    {
        int startOffset = getLeftSpan(),
            endOffset = getRightSpan();
        lexStream.makeToken(startOffset, endOffset, kind);
        if (printTokens) printValue(startOffset, endOffset);
    }

    final void makeComment(int kind)
    {
        int startOffset = getLeftSpan(),
            endOffset = getRightSpan();
        lexStream.getIPrsStream().makeAdjunct(startOffset, endOffset, kind);
    }

    final void skipToken()
    {
        if (printTokens) printValue(getLeftSpan(), getRightSpan());
    }
    
    final void checkForKeyWord()
    {
        int startOffset = getLeftSpan(),
            endOffset = getRightSpan(),
            kwKind = kwLexer.lexer(startOffset, endOffset);
        lexStream.makeToken(startOffset, endOffset, kwKind);
        if (printTokens) printValue(startOffset, endOffset);
    }
    
    //
    // This flavor of checkForKeyWord is necessary when the default kind
    // (which is returned when the keyword filter doesn't match) is something
    // other than _IDENTIFIER.
    //
    final void checkForKeyWord(int defaultKind)
    {
        int startOffset = getLeftSpan(),
            endOffset = getRightSpan(),
            kwKind = kwLexer.lexer(startOffset, endOffset);
        if (kwKind == RCParsersym.TK_IDENTIFIER)
            kwKind = defaultKind;
        lexStream.makeToken(startOffset, endOffset, kwKind);
        if (printTokens) printValue(startOffset, endOffset);
    }
    
    final void printValue(int startOffset, int endOffset)
    {
        String s = new String(lexStream.getInputChars(), startOffset, endOffset - startOffset + 1);
        System.out.print(s);
    }

    //
    //
    //
    static class RCLexerLpgLexStream extends LpgLexStream
    {
    public final static int tokenKind[] =
    {
        RCLexersym.Char_CtlCharNotWS,    // 000    0x00
        RCLexersym.Char_CtlCharNotWS,    // 001    0x01
        RCLexersym.Char_CtlCharNotWS,    // 002    0x02
        RCLexersym.Char_CtlCharNotWS,    // 003    0x03
        RCLexersym.Char_CtlCharNotWS,    // 004    0x04
        RCLexersym.Char_CtlCharNotWS,    // 005    0x05
        RCLexersym.Char_CtlCharNotWS,    // 006    0x06
        RCLexersym.Char_CtlCharNotWS,    // 007    0x07
        RCLexersym.Char_CtlCharNotWS,    // 008    0x08
        RCLexersym.Char_HT,              // 009    0x09
        RCLexersym.Char_LF,              // 010    0x0A
        RCLexersym.Char_CtlCharNotWS,    // 011    0x0B
        RCLexersym.Char_FF,              // 012    0x0C
        RCLexersym.Char_CR,              // 013    0x0D
        RCLexersym.Char_CtlCharNotWS,    // 014    0x0E
        RCLexersym.Char_CtlCharNotWS,    // 015    0x0F
        RCLexersym.Char_CtlCharNotWS,    // 016    0x10
        RCLexersym.Char_CtlCharNotWS,    // 017    0x11
        RCLexersym.Char_CtlCharNotWS,    // 018    0x12
        RCLexersym.Char_CtlCharNotWS,    // 019    0x13
        RCLexersym.Char_CtlCharNotWS,    // 020    0x14
        RCLexersym.Char_CtlCharNotWS,    // 021    0x15
        RCLexersym.Char_CtlCharNotWS,    // 022    0x16
        RCLexersym.Char_CtlCharNotWS,    // 023    0x17
        RCLexersym.Char_CtlCharNotWS,    // 024    0x18
        RCLexersym.Char_CtlCharNotWS,    // 025    0x19
        RCLexersym.Char_CtlCharNotWS,    // 026    0x1A
        RCLexersym.Char_CtlCharNotWS,    // 027    0x1B
        RCLexersym.Char_CtlCharNotWS,    // 028    0x1C
        RCLexersym.Char_CtlCharNotWS,    // 029    0x1D
        RCLexersym.Char_CtlCharNotWS,    // 030    0x1E
        RCLexersym.Char_CtlCharNotWS,    // 031    0x1F
        RCLexersym.Char_Space,           // 032    0x20
        RCLexersym.Char_Exclamation,     // 033    0x21
        RCLexersym.Char_DoubleQuote,     // 034    0x22
        RCLexersym.Char_Sharp,           // 035    0x23
        RCLexersym.Char_DollarSign,      // 036    0x24
        RCLexersym.Char_Percent,         // 037    0x25
        RCLexersym.Char_Ampersand,       // 038    0x26
        RCLexersym.Char_SingleQuote,     // 039    0x27
        RCLexersym.Char_LeftParen,       // 040    0x28
        RCLexersym.Char_RightParen,      // 041    0x29
        RCLexersym.Char_Star,            // 042    0x2A
        RCLexersym.Char_Plus,            // 043    0x2B
        RCLexersym.Char_Comma,           // 044    0x2C
        RCLexersym.Char_Minus,           // 045    0x2D
        RCLexersym.Char_Dot,             // 046    0x2E
        RCLexersym.Char_Slash,           // 047    0x2F
        RCLexersym.Char_0,               // 048    0x30
        RCLexersym.Char_1,               // 049    0x31
        RCLexersym.Char_2,               // 050    0x32
        RCLexersym.Char_3,               // 051    0x33
        RCLexersym.Char_4,               // 052    0x34
        RCLexersym.Char_5,               // 053    0x35
        RCLexersym.Char_6,               // 054    0x36
        RCLexersym.Char_7,               // 055    0x37
        RCLexersym.Char_8,               // 056    0x38
        RCLexersym.Char_9,               // 057    0x39
        RCLexersym.Char_Colon,           // 058    0x3A
        RCLexersym.Char_SemiColon,       // 059    0x3B
        RCLexersym.Char_LessThan,        // 060    0x3C
        RCLexersym.Char_Equal,           // 061    0x3D
        RCLexersym.Char_GreaterThan,     // 062    0x3E
        RCLexersym.Char_QuestionMark,    // 063    0x3F
        RCLexersym.Char_AtSign,          // 064    0x40
        RCLexersym.Char_A,               // 065    0x41
        RCLexersym.Char_B,               // 066    0x42
        RCLexersym.Char_C,               // 067    0x43
        RCLexersym.Char_D,               // 068    0x44
        RCLexersym.Char_E,               // 069    0x45
        RCLexersym.Char_F,               // 070    0x46
        RCLexersym.Char_G,               // 071    0x47
        RCLexersym.Char_H,               // 072    0x48
        RCLexersym.Char_I,               // 073    0x49
        RCLexersym.Char_J,               // 074    0x4A
        RCLexersym.Char_K,               // 075    0x4B
        RCLexersym.Char_L,               // 076    0x4C
        RCLexersym.Char_M,               // 077    0x4D
        RCLexersym.Char_N,               // 078    0x4E
        RCLexersym.Char_O,               // 079    0x4F
        RCLexersym.Char_P,               // 080    0x50
        RCLexersym.Char_Q,               // 081    0x51
        RCLexersym.Char_R,               // 082    0x52
        RCLexersym.Char_S,               // 083    0x53
        RCLexersym.Char_T,               // 084    0x54
        RCLexersym.Char_U,               // 085    0x55
        RCLexersym.Char_V,               // 086    0x56
        RCLexersym.Char_W,               // 087    0x57
        RCLexersym.Char_X,               // 088    0x58
        RCLexersym.Char_Y,               // 089    0x59
        RCLexersym.Char_Z,               // 090    0x5A
        RCLexersym.Char_LeftBracket,     // 091    0x5B
        RCLexersym.Char_BackSlash,       // 092    0x5C
        RCLexersym.Char_RightBracket,    // 093    0x5D
        RCLexersym.Char_Caret,           // 094    0x5E
        RCLexersym.Char__,               // 095    0x5F
        RCLexersym.Char_BackQuote,       // 096    0x60
        RCLexersym.Char_a,               // 097    0x61
        RCLexersym.Char_b,               // 098    0x62
        RCLexersym.Char_c,               // 099    0x63
        RCLexersym.Char_d,               // 100    0x64
        RCLexersym.Char_e,               // 101    0x65
        RCLexersym.Char_f,               // 102    0x66
        RCLexersym.Char_g,               // 103    0x67
        RCLexersym.Char_h,               // 104    0x68
        RCLexersym.Char_i,               // 105    0x69
        RCLexersym.Char_j,               // 106    0x6A
        RCLexersym.Char_k,               // 107    0x6B
        RCLexersym.Char_l,               // 108    0x6C
        RCLexersym.Char_m,               // 109    0x6D
        RCLexersym.Char_n,               // 110    0x6E
        RCLexersym.Char_o,               // 111    0x6F
        RCLexersym.Char_p,               // 112    0x70
        RCLexersym.Char_q,               // 113    0x71
        RCLexersym.Char_r,               // 114    0x72
        RCLexersym.Char_s,               // 115    0x73
        RCLexersym.Char_t,               // 116    0x74
        RCLexersym.Char_u,               // 117    0x75
        RCLexersym.Char_v,               // 118    0x76
        RCLexersym.Char_w,               // 119    0x77
        RCLexersym.Char_x,               // 120    0x78
        RCLexersym.Char_y,               // 121    0x79
        RCLexersym.Char_z,               // 122    0x7A
        RCLexersym.Char_LeftBrace,       // 123    0x7B
        RCLexersym.Char_VerticalBar,     // 124    0x7C
        RCLexersym.Char_RightBrace,      // 125    0x7D
        RCLexersym.Char_Tilde,           // 126    0x7E

        RCLexersym.Char_AfterASCII,      // for all chars in range 128..65534
        RCLexersym.Char_EOF              // for '\uffff' or 65535 
    };
            
    public final int getKind(int i)  // Classify character at ith location
    {
        int c = (i >= getStreamLength() ? '\uffff' : getCharValue(i));
        return (c < 128 // ASCII Character
                  ? tokenKind[c]
                  : c == '\uffff'
                       ? RCLexersym.Char_EOF
                       : RCLexersym.Char_AfterASCII);
    }

    public String[] orderedExportedSymbols() { return RCParsersym.orderedTerminalSymbols; }

    public RCLexerLpgLexStream(String filename, int tab) throws java.io.IOException
    {
        super(filename, tab);
    }

    public RCLexerLpgLexStream(char[] input_chars, String filename, int tab)
    {
        super(input_chars, filename, tab);
    }

    public RCLexerLpgLexStream(char[] input_chars, String filename)
    {
        super(input_chars, filename, 1);
    }
    }

    public void ruleAction(int ruleNumber)
    {
        switch(ruleNumber)
        {

            //
            // Rule 1:  Token ::= Identifier
            //
            case 1: { 
				checkForKeyWord();
	              break;
            }
	
            //
            // Rule 2:  Token ::= IntegerLiteral
            //
            case 2:
                break; 
	
            //
            // Rule 3:  Token ::= LongLiteral
            //
            case 3:
                break; 
	
            //
            // Rule 4:  Token ::= DateLiteral
            //
            case 4:
                break; 
	
            //
            // Rule 5:  Token ::= IntegerLiteral DotToken
            //
            case 5:
                break; 
	
            //
            // Rule 6:  Token ::= IntegerLiteral DotDotToken
            //
            case 6:
                break; 
	
            //
            // Rule 7:  Token ::= RealLiteral
            //
            case 7: { 
				makeToken(RCParsersym.TK_REAL_LITERAL);
	              break;
            }
	
            //
            // Rule 8:  Token ::= DoubleQuote SLNotDQOpt DoubleQuote
            //
            case 8: { 
			makeToken(RCParsersym.TK_STRING_LITERAL);
	              break;
            }
	
            //
            // Rule 9:  Token ::= SingleQuote NotSQ SingleQuote
            //
            case 9: { 
			makeToken(RCParsersym.TK_CHAR_LITERAL);
	              break;
            }
	
            //
            // Rule 10:  Token ::= WS
            //
            case 10: { 
				skipToken();
	              break;
            }
	
            //
            // Rule 11:  Token ::= +
            //
            case 11: { 
				makeToken(RCParsersym.TK_PLUS);
	              break;
            }
	
            //
            // Rule 12:  Token ::= $
            //
            case 12: { 
				makeToken(RCParsersym.TK_DOLLAR_SYMBOL);
	              break;
            }
	
            //
            // Rule 13:  Token ::= -
            //
            case 13: { 
				makeToken(RCParsersym.TK_MINUS);
	              break;
            }
	
            //
            // Rule 14:  Token ::= @
            //
            case 14: { 
				makeToken(RCParsersym.TK_AT_SYMBOL);
	              break;
            }
	
            //
            // Rule 15:  Token ::= %
            //
            case 15: { 
		makeToken(RCParsersym.TK_PERCENT);
	              break;
            }
	
            //
            // Rule 16:  Token ::= *
            //
            case 16: { 
				makeToken(RCParsersym.TK_MULTIPLY);
	              break;
            }
	
            //
            // Rule 17:  Token ::= /
            //
            case 17: { 
				makeToken(RCParsersym.TK_DIVIDE);
	              break;
            }
	
            //
            // Rule 18:  Token ::= (
            //
            case 18: { 
				makeToken(RCParsersym.TK_LPAREN);
	              break;
            }
	
            //
            // Rule 19:  Token ::= )
            //
            case 19: { 
				makeToken(RCParsersym.TK_RPAREN);
	              break;
            }
	
            //
            // Rule 20:  Token ::= >
            //
            case 20: { 
				makeToken(RCParsersym.TK_GREATER);
	              break;
            }
	
            //
            // Rule 21:  Token ::= <
            //
            case 21: { 
				makeToken(RCParsersym.TK_LESS);
	              break;
            }
	
            //
            // Rule 22:  Token ::= =
            //
            case 22: { 
				makeToken(RCParsersym.TK_EQUAL);
	              break;
            }
	
            //
            // Rule 23:  Token ::= > =
            //
            case 23: { 
				makeToken(RCParsersym.TK_GREATER_EQUAL);
	              break;
            }
	
            //
            // Rule 24:  Token ::= < =
            //
            case 24: { 
				makeToken(RCParsersym.TK_LESS_EQUAL);
	              break;
            }
	
            //
            // Rule 25:  Token ::= < >
            //
            case 25: { 
				makeToken(RCParsersym.TK_NOT_EQUAL);
	              break;
            }
	
            //
            // Rule 26:  Token ::= [
            //
            case 26: { 
				makeToken(RCParsersym.TK_LBRACKET);
	              break;
            }
	
            //
            // Rule 27:  Token ::= ]
            //
            case 27: { 
				makeToken(RCParsersym.TK_RBRACKET);
	              break;
            }
	
            //
            // Rule 28:  Token ::= {
            //
            case 28: { 
				makeToken(RCParsersym.TK_LBRACE);
	              break;
            }
	
            //
            // Rule 29:  Token ::= }
            //
            case 29: { 
				makeToken(RCParsersym.TK_RBRACE);
	              break;
            }
	
            //
            // Rule 30:  Token ::= - >
            //
            case 30: { 
				makeToken(RCParsersym.TK_ARROW);
	              break;
            }
	
            //
            // Rule 31:  Token ::= |
            //
            case 31: { 
				makeToken(RCParsersym.TK_BAR);
	              break;
            }
	
            //
            // Rule 32:  Token ::= ,
            //
            case 32: { 
				makeToken(RCParsersym.TK_COMMA);
	              break;
            }
	
            //
            // Rule 33:  Token ::= :
            //
            case 33: { 
				makeToken(RCParsersym.TK_COLON);
	              break;
            }
	
            //
            // Rule 34:  Token ::= : :
            //
            case 34: { 
				makeToken(RCParsersym.TK_COLONCOLON);
	              break;
            }
	
            //
            // Rule 35:  Token ::= ;
            //
            case 35: { 
				makeToken(RCParsersym.TK_SEMICOLON);
	              break;
            }
	
            //
            // Rule 36:  Token ::= DotToken
            //
            case 36:
                break; 
	
            //
            // Rule 37:  DotToken ::= .
            //
            case 37: { 
				makeToken(RCParsersym.TK_DOT);
	              break;
            }
	
            //
            // Rule 38:  Token ::= DotDotToken
            //
            case 38:
                break; 
	
            //
            // Rule 39:  DotDotToken ::= . .
            //
            case 39: { 
				makeToken(RCParsersym.TK_DOTDOT);
	              break;
            }
	
            //
            // Rule 40:  DateLiteral ::= Year - Month - Day
            //
            case 40: { 
		makeToken(RCParsersym.TK_DATE_LITERAL);
              break;
            }

            //
            // Rule 85:  IntegerLiteral ::= Integer
            //
            case 85: { 
				makeToken(RCParsersym.TK_INTEGER_LITERAL);
	              break;
            }
	
            //
            // Rule 86:  LongLiteral ::= Long
            //
            case 86: { 
		makeToken(RCParsersym.TK_LONG_LITERAL);
              break;
            }

    
            default:
                break;
        }
        return;
    }
}

