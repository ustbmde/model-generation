/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.CopyModelAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copy Model Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl#getFromURI <em>From URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.CopyModelActionImpl#getToURI <em>To URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CopyModelActionImpl extends TestActionImpl implements CopyModelAction {
	/**
	 * The default value of the '{@link #getFromURI() <em>From URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromURI()
	 * @generated
	 * @ordered
	 */
	protected static final String FROM_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFromURI() <em>From URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromURI()
	 * @generated
	 * @ordered
	 */
	protected String fromURI = FROM_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getToURI() <em>To URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToURI()
	 * @generated
	 * @ordered
	 */
	protected static final String TO_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToURI() <em>To URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToURI()
	 * @generated
	 * @ordered
	 */
	protected String toURI = TO_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyModelActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.COPY_MODEL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFromURI() {
		return fromURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromURI(String newFromURI) {
		String oldFromURI = fromURI;
		fromURI = newFromURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.COPY_MODEL_ACTION__FROM_URI, oldFromURI, fromURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToURI() {
		return toURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToURI(String newToURI) {
		String oldToURI = toURI;
		toURI = newToURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.COPY_MODEL_ACTION__TO_URI, oldToURI, toURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.COPY_MODEL_ACTION__FROM_URI:
				return getFromURI();
			case MT4MTPackage.COPY_MODEL_ACTION__TO_URI:
				return getToURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.COPY_MODEL_ACTION__FROM_URI:
				setFromURI((String)newValue);
				return;
			case MT4MTPackage.COPY_MODEL_ACTION__TO_URI:
				setToURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.COPY_MODEL_ACTION__FROM_URI:
				setFromURI(FROM_URI_EDEFAULT);
				return;
			case MT4MTPackage.COPY_MODEL_ACTION__TO_URI:
				setToURI(TO_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.COPY_MODEL_ACTION__FROM_URI:
				return FROM_URI_EDEFAULT == null ? fromURI != null : !FROM_URI_EDEFAULT.equals(fromURI);
			case MT4MTPackage.COPY_MODEL_ACTION__TO_URI:
				return TO_URI_EDEFAULT == null ? toURI != null : !TO_URI_EDEFAULT.equals(toURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fromURI: ");
		result.append(fromURI);
		result.append(", toURI: ");
		result.append(toURI);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		return edu.ustb.sei.mt4mt.base.Actions.copy(fromURI, toURI);
	}

} //CopyModelActionImpl
