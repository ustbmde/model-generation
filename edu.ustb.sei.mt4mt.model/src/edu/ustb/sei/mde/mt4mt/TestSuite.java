/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Suite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.TestSuite#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.TestSuite#getTestCases <em>Test Cases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.TestSuite#getDefaultWaitingTime <em>Default Waiting Time</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestSuite()
 * @model
 * @generated
 */
public interface TestSuite extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestSuite_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.TestSuite#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Test Cases</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mt4mt.TestCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Cases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Cases</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestSuite_TestCases()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCase> getTestCases();

	/**
	 * Returns the value of the '<em><b>Default Waiting Time</b></em>' attribute.
	 * The default value is <code>"10000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Waiting Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Waiting Time</em>' attribute.
	 * @see #setDefaultWaitingTime(long)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getTestSuite_DefaultWaitingTime()
	 * @model default="10000"
	 * @generated
	 */
	long getDefaultWaitingTime();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.TestSuite#getDefaultWaitingTime <em>Default Waiting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Waiting Time</em>' attribute.
	 * @see #getDefaultWaitingTime()
	 * @generated
	 */
	void setDefaultWaitingTime(long value);

} // TestSuite
