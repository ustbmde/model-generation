package edu.ustb.sei.rmg.random.condition;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;

import edu.ustb.sei.rmg.random.RandomUtil;

public class FeaturePathCondition<T> extends Condition<T> {
	
	public FeaturePathCondition(Class<T> type) {
		super();
		this.type = type;
	}
	private Class<T> type;
	public String[] path = null;
//	private EStructuralFeature[] featurePath = null;
	
	static final EStructuralFeature ROOT = EcoreFactory.eINSTANCE.createEReference();
	static final EStructuralFeature CONTAINER = EcoreFactory.eINSTANCE.createEReference();

	@Override
	public T getValue(RandomUtil r, Object... extra) {
		if(extra==null||extra.length==0)
			throw new FeatureConditionError(this);
		
//		if(featurePath==null) 
//			collectPath(((EObject)extra[0]).eClass());
		
		if((Boolean)extra[2]==true) 
			throw new ValueLazyGenerationRequest(this, (EObject)extra[0],
					(EAttribute)extra[1]);
		else return getValue(r,(EObject)extra[0]);
		
	}
	
	private T getValue(RandomUtil r,EObject o) {
//		Object v = null;
//		int id = 0;
		
		ArrayList<T> values = new ArrayList<T>();
		getValues(o,path,0,values);
		
		if(values.size()==0) {
			throw new FeatureConditionError(this);
		} else {
			return values.get(r.randomUIntU(values.size()));
		}
		
//		for(EStructuralFeature f : featurePath) {
//			v = o.eGet(f);
//			if(v==null) {
//				if(id!=featurePath.length-1) //such feature does not exist
//					throw new FeatureConditionError(this);
//				else return null;
//			}
//			
//			if(v instanceof EObject)
//				o = (EObject)v;
//			id++;
//		}
//		
//		return v.toString();
	}
	
	private void getValues(EObject r, String[] featurePath, int idx, List<T> values) {
		String featureName = featurePath[idx];
		Object  v;
		EStructuralFeature feature = null;
		
		if(featureName.equals("__root")) 
			for(v = r; ((EObject)v).eContainer()!=null ; v = ((EObject)v).eContainer());
		else if(featureName.equals("__container")) v = r.eContainer();
		else {
			feature = r.eClass().getEStructuralFeature(featureName);
			v = r.eGet(feature);
		}
		
		if(v==null)
//			if(idx!=featurePath.length-1) //such feature does not exist
//				return;
//			else return;
			return;
		else {
			if(feature!=null && feature.isMany()) {
				List<?> vv = (List<?>)v;
				if(idx==featurePath.length-1) {
					for(Object o : vv) {
						values.add(castTo(o));
					}
				} else {
					for(Object o : vv) {
						getValues((EObject)o, featurePath,idx+1,values);
					}
				}
			} else {
				if(idx==featurePath.length-1) {
					values.add(castTo(v));
				} else {
					getValues((EObject)v, featurePath,idx+1,values);
				}
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	protected T castTo(Object value) {
		if(type==String.class) return value==null ? (T)"" : (T)value.toString();
		else return (T)value;
	}
	
//	private void collectPath(EClass c) {
//		featurePath = new EStructuralFeature[path.length];
//		for(int i=0;i<path.length;i++) {
//			String f = path[i];
//			if(f.equals("__root")) 
//				featurePath[i] = ROOT;
//			else if(f.equals("__container"))
//				featurePath[i] = CONTAINER;
//			else {
//				featurePath[i] = c.getEStructuralFeature(f);
//				if(featurePath[i] instanceof EReference) {
//					c = ((EReference)featurePath[i]).getEReferenceType();
//				}			
//			}
//		}
//	}

}
