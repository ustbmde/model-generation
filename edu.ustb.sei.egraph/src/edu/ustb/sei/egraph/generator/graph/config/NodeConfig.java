package edu.ustb.sei.egraph.generator.graph.config;

import java.util.ArrayList;
import java.util.List;

import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class NodeConfig {
	public NodeConfig(QualifiedName type) {
		super();
		this.type = type;
	}
	private QualifiedName type;
	private AbstractRandomFunction<Integer> bound;
	private List<AttributeConfig> attributeConfigs = new ArrayList<AttributeConfig>();
	private List<NodeInstanceSetConfig> instanceSetConfig = new ArrayList<NodeInstanceSetConfig>();
	
	public List<AttributeConfig> getAttributeConfigs() {
		return attributeConfigs;
	}
	public void addAttributeConfig(AttributeConfig c) {
		for(AttributeConfig a : attributeConfigs) {
			if(a.getName().equals(c.getName()))
				return;
		}
		this.attributeConfigs.add(c);
	}
	
	public List<NodeInstanceSetConfig> getInstanceSetConfig() {
		return instanceSetConfig;
	}
	public QualifiedName getType() {
		return type;
	}
	public AbstractRandomFunction<Integer> getBound() {
		return bound;
	}
	public void setBound(AbstractRandomFunction<Integer> bound) {
		this.bound = bound;
	}
	
}
