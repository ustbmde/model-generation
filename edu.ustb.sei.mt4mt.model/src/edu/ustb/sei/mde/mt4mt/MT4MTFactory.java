/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage
 * @generated
 */
public interface MT4MTFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MT4MTFactory eINSTANCE = edu.ustb.sei.mde.mt4mt.impl.MT4MTFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Test Suite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Suite</em>'.
	 * @generated
	 */
	TestSuite createTestSuite();

	/**
	 * Returns a new object of class '<em>Customizable Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Customizable Test Case</em>'.
	 * @generated
	 */
	CustomizableTestCase createCustomizableTestCase();

	/**
	 * Returns a new object of class '<em>Get Put Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Get Put Test Case</em>'.
	 * @generated
	 */
	GetPutTestCase createGetPutTestCase();

	/**
	 * Returns a new object of class '<em>Put Get Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Put Get Test Case</em>'.
	 * @generated
	 */
	PutGetTestCase createPutGetTestCase();

	/**
	 * Returns a new object of class '<em>Sequence Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Action</em>'.
	 * @generated
	 */
	SequenceAction createSequenceAction();

	/**
	 * Returns a new object of class '<em>Load Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Load Model Action</em>'.
	 * @generated
	 */
	LoadModelAction createLoadModelAction();

	/**
	 * Returns a new object of class '<em>Save Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Save Model Action</em>'.
	 * @generated
	 */
	SaveModelAction createSaveModelAction();

	/**
	 * Returns a new object of class '<em>Execute Launcher Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Execute Launcher Action</em>'.
	 * @generated
	 */
	ExecuteLauncherAction createExecuteLauncherAction();

	/**
	 * Returns a new object of class '<em>Evaluate OCL Query Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Evaluate OCL Query Action</em>'.
	 * @generated
	 */
	EvaluateOCLQueryAction createEvaluateOCLQueryAction();

	/**
	 * Returns a new object of class '<em>Copy Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Model Action</em>'.
	 * @generated
	 */
	CopyModelAction createCopyModelAction();

	/**
	 * Returns a new object of class '<em>Compare Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compare Model Action</em>'.
	 * @generated
	 */
	CompareModelAction createCompareModelAction();

	/**
	 * Returns a new object of class '<em>Assert Comparison Result Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assert Comparison Result Action</em>'.
	 * @generated
	 */
	AssertComparisonResultAction createAssertComparisonResultAction();

	/**
	 * Returns a new object of class '<em>Assert OCL Invariant Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assert OCL Invariant Action</em>'.
	 * @generated
	 */
	AssertOCLInvariantAction createAssertOCLInvariantAction();

	/**
	 * Returns a new object of class '<em>User Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Action</em>'.
	 * @generated
	 */
	UserAction createUserAction();

	/**
	 * Returns a new object of class '<em>Set Variable Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Set Variable Action</em>'.
	 * @generated
	 */
	SetVariableAction createSetVariableAction();

	/**
	 * Returns a new object of class '<em>Update Model Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Update Model Action</em>'.
	 * @generated
	 */
	UpdateModelAction createUpdateModelAction();

	/**
	 * Returns a new object of class '<em>Wrapper Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wrapper Test Case</em>'.
	 * @generated
	 */
	WrapperTestCase createWrapperTestCase();

	/**
	 * Returns a new object of class '<em>Auto Executor Test Suite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Auto Executor Test Suite</em>'.
	 * @generated
	 */
	AutoExecutorTestSuite createAutoExecutorTestSuite();

	/**
	 * Returns a new object of class '<em>Clear Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clear Action</em>'.
	 * @generated
	 */
	ClearAction createClearAction();

	/**
	 * Returns a new object of class '<em>External Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Action</em>'.
	 * @generated
	 */
	ExternalAction createExternalAction();

	/**
	 * Returns a new object of class '<em>External Action Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Action Parameter</em>'.
	 * @generated
	 */
	ExternalActionParameter createExternalActionParameter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MT4MTPackage getMT4MTPackage();

} //MT4MTFactory
