/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.UserAction;
import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.URIClassLoader;
import edu.ustb.sei.mt4mt.base.actions.JavaAction;

import java.util.function.Function;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.UserActionImpl#getClassFileURI <em>Class File URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserActionImpl extends TestActionImpl implements UserAction {
	/**
	 * The default value of the '{@link #getClassFileURI() <em>Class File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassFileURI()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_FILE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClassFileURI() <em>Class File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassFileURI()
	 * @generated
	 * @ordered
	 */
	protected String classFileURI = CLASS_FILE_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.USER_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClassFileURI() {
		return classFileURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassFileURI(String newClassFileURI) {
		String oldClassFileURI = classFileURI;
		classFileURI = newClassFileURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.USER_ACTION__CLASS_FILE_URI, oldClassFileURI, classFileURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.USER_ACTION__CLASS_FILE_URI:
				return getClassFileURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.USER_ACTION__CLASS_FILE_URI:
				setClassFileURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.USER_ACTION__CLASS_FILE_URI:
				setClassFileURI(CLASS_FILE_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.USER_ACTION__CLASS_FILE_URI:
				return CLASS_FILE_URI_EDEFAULT == null ? classFileURI != null : !CLASS_FILE_URI_EDEFAULT.equals(classFileURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (classFileURI: ");
		result.append(classFileURI);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		try {
			URIClassLoader loader = new URIClassLoader(getClass().getClassLoader());
			Class<?> clazz = loader.loadClass(classFileURI);
			try {
				Class<? extends IAction> actClazz = clazz.asSubclass(IAction.class);
				return actClazz.newInstance();
			} catch(ClassCastException e) {
				@SuppressWarnings("unchecked")
				Class<? extends Function<Context,Void>> funcClazz = (Class<? extends Function<Context, Void>>) clazz.asSubclass(Function.class);
				return new JavaAction(funcClazz.newInstance());
			}
		} catch(Exception e) {
			return edu.ustb.sei.mt4mt.base.Actions.fail();
		}
	}

} //UserActionImpl
