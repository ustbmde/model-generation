/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug;

public class StructuralrmgSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgStackFrame) {
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgStackFrame frame = (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
