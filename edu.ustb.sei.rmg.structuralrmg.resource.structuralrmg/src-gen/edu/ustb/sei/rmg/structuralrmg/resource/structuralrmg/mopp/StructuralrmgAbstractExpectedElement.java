/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class StructuralrmgAbstractExpectedElement implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>> followers = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>>();
	
	public StructuralrmgAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement follower, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] path) {
		followers.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
