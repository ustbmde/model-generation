/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgHoverTextProvider implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgHoverTextProvider {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgDefaultHoverTextProvider defaultProvider = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgDefaultHoverTextProvider();
	
	public String getHoverText(org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(org.eclipse.emf.ecore.EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
