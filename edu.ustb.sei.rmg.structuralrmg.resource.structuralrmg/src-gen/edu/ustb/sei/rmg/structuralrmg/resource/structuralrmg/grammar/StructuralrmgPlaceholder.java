/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

/**
 * A class to represent placeholders in a grammar.
 */
public class StructuralrmgPlaceholder extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgTerminal {
	
	private final String tokenName;
	
	public StructuralrmgPlaceholder(org.eclipse.emf.ecore.EStructuralFeature feature, String tokenName, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
