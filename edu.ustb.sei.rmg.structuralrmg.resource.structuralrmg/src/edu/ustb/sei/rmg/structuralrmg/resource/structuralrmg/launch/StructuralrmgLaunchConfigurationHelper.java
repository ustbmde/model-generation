/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.launch;

import java.util.Calendar;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import edu.ustb.sei.rmg.structuralrmg.build.ConcurrentProduceModel;
import edu.ustb.sei.rmg.structuralrmg.build.IUnfolder;
import edu.ustb.sei.rmg.structuralrmg.build.ProduceModel;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResourceUtil;

/**
 * A class that provides common methods that are required by launch configuration
 * delegates.
 */
public class StructuralrmgLaunchConfigurationHelper {
	
	final static public String USE_CONCURRENT_BUILDER = "USE_CONCURRENT_BUILDER";
	
	public static class SystemOutInterpreter extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.AbstractStructuralrmgInterpreter<Void,Void> {
		
		@Override		
		public Void interprete(org.eclipse.emf.ecore.EObject object, Void context) {
			System.out.println("Found " + object + ", but don't know what to do with it.");
			return null;
		}
	}
	
	private ResourceSet resourceSet; 
	
	public StructuralrmgLaunchConfigurationHelper() {
		resourceSet  = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				"ecore", new EcoreResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("structuralrmg",
				new StructuralrmgResourceUtil());
	}
	
	/**
	 * Launch an example interpreter that prints object to System.out.
	 */
	public void launch(org.eclipse.debug.core.ILaunchConfiguration configuration, String mode, org.eclipse.debug.core.ILaunch launch, org.eclipse.core.runtime.IProgressMonitor monitor) throws org.eclipse.core.runtime.CoreException {
			System.gc();
			
			org.eclipse.emf.common.util.URI[] uris = this.getURIs(configuration);
			
			
			for(org.eclipse.emf.common.util.URI uri : uris) {
				try {
					
					EObject root = getModelRoot(uri);
					
					Resource model = root.eResource();
					
					Resource metamodel = root.eClass().eResource();
					
					IUnfolder pm = null;
					
					if(useConcurrentUnfolder(configuration)) {
						pm = new ConcurrentProduceModel();
					} else {
						pm = new ProduceModel();
					}
					
					System.out.println("Using "+(useConcurrentUnfolder(configuration) ? "Concurrent Unfolder" : "Sequential Unfolder"));
					
					long start = Calendar.getInstance().getTimeInMillis();
					
					pm.travelPackage((EPackage) metamodel.getContents().get(0));
					Resource res = pm.buildResource(model);
					
					long end = Calendar.getInstance().getTimeInMillis();
					
					System.out.println("Unfolding: "+(end-start)+"ms");
					
					HashMap<Object, Object> options = new HashMap<Object, Object>();
					options.put(XMLResource.OPTION_SCHEMA_LOCATION, true);
					
					
					res.save(options);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			resourceSet.getResources().clear();
			
	}
	
	private boolean useConcurrentUnfolder(ILaunchConfiguration configuration) throws CoreException {
		return configuration.getAttribute(USE_CONCURRENT_BUILDER, false);
	}

	public <ResultType, ContextType> void launchInterpreter(org.eclipse.debug.core.ILaunchConfiguration configuration, String mode, org.eclipse.debug.core.ILaunch launch, org.eclipse.core.runtime.IProgressMonitor monitor, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.AbstractStructuralrmgInterpreter<ResultType, ContextType> delegate, final ContextType context) throws org.eclipse.core.runtime.CoreException {
		final boolean enableDebugger = mode.equals(org.eclipse.debug.core.ILaunchManager.DEBUG_MODE);
		// step 1: find two free ports we can use to communicate between the Eclipse and
		// the interpreter
		int requestPort = findFreePort();
		int eventPort = findFreePort();
		if (requestPort < 0 || eventPort < 0) {
			abort("Unable to find free port", null);
		}
		
		final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebuggableInterpreter<ResultType, ContextType> interpreter = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebuggableInterpreter<ResultType, ContextType>(delegate, eventPort);
		
		// step 2: prepare and start interpreter in separate thread
		Thread interpreterThread = new Thread(new Runnable() {
			
			public void run() {
				// if we are in debug mode, the interpreter must wait for the debugger to attach
				interpreter.interprete(context, enableDebugger);
			}
		});
		interpreterThread.start();
		
		// step 3: start debugger listener (sends commands from Eclipse debug framework to
		// running process
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebuggerListener<ResultType, ContextType> debugListener = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebuggerListener<ResultType, ContextType>(requestPort);
		debugListener.setDebuggable(interpreter);
		new Thread(debugListener).start();
		
		// step 4: start debugger
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugProcess process = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugProcess(launch);
		launch.addDebugTarget(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugTarget(process, launch, requestPort, eventPort));
	}
	
//	public org.eclipse.emf.common.util.URI getURI(org.eclipse.debug.core.ILaunchConfiguration configuration) throws org.eclipse.core.runtime.CoreException {
//		return org.eclipse.emf.common.util.URI.createURI(configuration.getAttribute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.launch.StructuralrmgLaunchConfigurationDelegate.ATTR_RESOURCE_URI, (String) null));
//	}
	
	public org.eclipse.emf.common.util.URI[] getURIs(org.eclipse.debug.core.ILaunchConfiguration configuration) throws org.eclipse.core.runtime.CoreException {
		String attribute = configuration.getAttribute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.launch.StructuralrmgLaunchConfigurationDelegate.ATTR_RESOURCE_URI, (String) null);
		if(attribute==null)
			return new org.eclipse.emf.common.util.URI[0];
		else {
			String[] buf = attribute.split(";");
			org.eclipse.emf.common.util.URI[] res = new org.eclipse.emf.common.util.URI[buf.length];
			for(int i=0;i<buf.length;i++) {
				res[i] = org.eclipse.emf.common.util.URI.createURI(buf[i]);
			}
			return res;
		}
	}
	
	public org.eclipse.emf.ecore.EObject getModelRoot(org.eclipse.emf.common.util.URI uri) throws org.eclipse.core.runtime.CoreException {
		//return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgResourceUtil.getResourceContent(getURI(configuration));
		
		
		Resource res = resourceSet.getResource(uri,true);
		EcoreUtil.resolveAll(resourceSet);
		
		return res.getContents().get(0);
	}
	
	/**
	 * Returns a free port number on localhost, or -1 if unable to find a free port.
	 */
	protected int findFreePort() {
		java.net.ServerSocket socket = null;
		try {
			socket = new java.net.ServerSocket(0);
			return socket.getLocalPort();
		} catch (java.io.IOException e) {
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (java.io.IOException e) {
				}
			}
		}
		return -1;
	}
	/**
	 * Throws an exception with a new status containing the given message and optional
	 * exception.
	 * 
	 * @param message error message
	 * @param e underlying exception
	 * 
	 * @throws CoreException
	 */
	protected void abort(String message, Throwable e) throws org.eclipse.core.runtime.CoreException {
		throw new org.eclipse.core.runtime.CoreException(new org.eclipse.core.runtime.Status(org.eclipse.core.runtime.IStatus.ERROR, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgPlugin.DEBUG_MODEL_ID, 0, message, e));
	}
}
