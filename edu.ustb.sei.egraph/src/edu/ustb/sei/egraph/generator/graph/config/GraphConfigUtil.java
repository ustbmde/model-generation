package edu.ustb.sei.egraph.generator.graph.config;

import java.util.BitSet;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import edu.ustb.sei.egraph.generator.graph.config.GraphConfigLexer;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfigParser;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfigParser.ConfigRootContext;

public class GraphConfigUtil implements ANTLRErrorListener{
	final static public GraphConfigUtil SINGLETON = new GraphConfigUtil();
	
	private GraphConfigParser parser;
	
	public GraphConfigParser getParser() {
		if(parser==null) {
			parser = new GraphConfigParser(null);
			parser.addErrorListener(this);
		}
		return parser;
	}
	
	public ConfigRootContext parse(String code) {
		ANTLRInputStream s = new ANTLRInputStream(code);
		GraphConfigLexer lexer = new GraphConfigLexer(s);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		GraphConfigParser p = this.getParser();
		p.setTokenStream(tokens);
		p.reset();
		return p.configRoot();
	}

	@Override
	public void reportAmbiguity(Parser arg0, DFA arg1, int arg2, int arg3, boolean arg4, BitSet arg5,
			ATNConfigSet arg6) {
	}

	@Override
	public void reportAttemptingFullContext(Parser arg0, DFA arg1, int arg2, int arg3, BitSet arg4, ATNConfigSet arg5) {
	}

	@Override
	public void reportContextSensitivity(Parser arg0, DFA arg1, int arg2, int arg3, int arg4, ATNConfigSet arg5) {
	}

	@Override
	public void syntaxError(Recognizer<?, ?> arg0, Object arg1, int arg2, int arg3, String arg4,
			RecognitionException arg5) {
		throw new RuntimeException(arg5);
	}
}
