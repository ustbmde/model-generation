package edu.ustb.sei.mt4mt.base.updates;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

public interface IUpdate {
	boolean apply(EObject o,EClass oClass, EPackage oPackage, RandomUtil util, Set<EObject> createdObjects);
	
	/**
	 * Find the feature according to the given name from the class.
	 * The feature can be an inherited feature.
	 * @param clazz
	 * @param name
	 * @return
	 */
	static public EStructuralFeature searchFeature(EClass clazz, String name) {
		for(EStructuralFeature f : clazz.getEAllStructuralFeatures()) {
			if(name.equals(f.getName()))
				return f;
		}
		return null;
	}
	
	/**
	 * To determine if the class name is equal to the given name
	 * @param clazz
	 * @param name
	 * @return
	 */
	static public boolean isTypeOf(EClass clazz, String name) {
		return name.equals(clazz.getName());
	}
	
	/**
	 * To determine if the name of the class or any of its ancestor class is equal to the given name
	 * @param clazz
	 * @param name
	 * @return
	 */
	static public boolean isKindOf(EClass clazz, String name) {
		if(isTypeOf(clazz,name)) return true;
		else {
			for(EClass s : clazz.getESuperTypes()) {
				if(isKindOf(s,name)) return true;
			}
			return false;
		}
	}

	/**
	 * Find the class defined in the package
	 * @param pkg
	 * @param name
	 * @return
	 */
	static public EClass searchClassAtPackage(EPackage pkg, String name) {
		for(EClassifier c : pkg.getEClassifiers()) {
			if(c instanceof EClass && c.getName().equals(name))
				return (EClass) c;
		}
		return null;
	}
	
	/**
	 * Find the class defined in the package or in the subpackages
	 * @param pkg
	 * @param name
	 * @return
	 */
	static public EClass searchClassInPackage(EPackage pkg, String name) {
		EClass cls = searchClassAtPackage(pkg, name);
		if(cls!=null) return cls;
		
		for(EPackage p : pkg.getESubpackages()) {
			cls = searchClassInPackage(p, name);
			if(cls!=null) return cls;
		}
		
		return null;
	}
	

	/**
	 * Find the class defined in the ecore model containing the given package
	 * @param pkg
	 * @param name
	 * @return
	 */
	static public EClass searchClassFromPackage(EPackage pkg, String name) {
		while(pkg.getESuperPackage()!=null)
			pkg = pkg.getESuperPackage();
		return searchClassInPackage(pkg, name);
	}
	
	static public boolean setOrAddFeatureValue(EObject o, String featureName, Object value) {
		try {
			EClass cls = o.eClass();
			EStructuralFeature f = searchFeature(cls, featureName);
			if(f.isMany()) {
				((List)o.eGet(f)).add(value);
			} else {
				o.eSet(f, value);
			}
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	static public boolean setValue(EObject o, String featureName, Object featureValue) {
		EStructuralFeature feature = IUpdate.searchFeature(o.eClass(), featureName);
		o.eSet(feature, featureValue);
		return true;
	}
	
	static public boolean addValue(EObject host, String featureName, Object featureValue) {
		EStructuralFeature feature = IUpdate.searchFeature(host.eClass(), featureName);
		List<Object> list = (List<Object>) host.eGet(feature);
		list.add(featureValue);
		return true;
	}
	
	static public EObject createObject(String className, EPackage pkg) {
		EClass clazz = IUpdate.searchClassAtPackage(pkg, className);
		return EcoreUtil.create(clazz);
	}
	
	@SuppressWarnings("unchecked")
	static public <T> T getValue(EObject host, String featureName) {
		EStructuralFeature feature = IUpdate.searchFeature(host.eClass(), featureName);
		return (T)host.eGet(feature);
	}
	
	static public boolean removeValue(EObject host, String featureName, Object featureValue) {
		try {
			EClass cls = host.eClass();
			EStructuralFeature f = searchFeature(cls, featureName);
			if(f.isMany()) {
				return ((List)host.eGet(f)).remove(featureValue);
			} else {
				if(featureValue.equals(host.eGet(f))) {
					host.eUnset(f);
					return true;
				}
				else return false;
			}
		} catch(Exception e) {
			return false;
		}
	}
	
	
}
