/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

public class PrimitiveTemplateEdgeSourceReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode> result) {
		if(identifier==null || container==null) return;
		Template t = ResolverUtil.upToTemplate(container);
		if(t==null) return;
		
		for(TemplateNode n : t.getAllNodes()) {
			if(ResolverUtil.matchName(identifier, n.getName())) {
				result.addMapping(identifier, n);
				return;
			}
		}	}
	
	public String deResolve(edu.ustb.sei.rmg.structuralrmg.TemplateNode element, edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
