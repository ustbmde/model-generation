--%options ast_directory=./ast,automatic_ast=toplevel,var=nt,visitor=default
%options ast_type=Condition
%options programming_language=java
%options package=edu.ustb.sei.rmg.random
%options template=./dtParserTemplateF.gi
%options import_terminals=RCLexer.g
%options noserialize
%options la=2
%options escape=$
%options fp=RCParser,prefix=TK_
%options include_directory="./"

%Define
  $default_repair_count /.getDefaultRepairCount()./
  

	$lpg_ns /.lpg.runtime./ -- package namespace of the LPG Runtime API
	

  $super_lexer_class /.ExprLexer./
	-- Some useful macros	
    $NewCase
    /. $Header
                case $rule_number:./


	
    $EmptyListAction -- Deprecated, code inline with correct generic parameter type
    /. $Header
                case $rule_number:
                    setResult(new BasicEList<Object>());
                    break;./
                    
    -- BeginJava and EndJava need to be reworked in order to be able to properly use $NewCase macro
    
    -- BeginJava does nothing
	-- block-actions should call BeginCode, instead
    $BeginJava /../
    
  	-- EndJava does nothing
	-- block-actions should call EndCode, instead
	$EndJava /../
	
	$BeginCode
	/.$BeginAction
					$symbol_declarations./

	$EndCode /.$EndAction./
%End

%Globals
    /.
	import $lpg_ns.BadParseException;
	import $lpg_ns.BadParseSymFileException;
	import $lpg_ns.DiagnoseParser;
	import $lpg_ns.ErrorToken;
	import $lpg_ns.IToken;
	import $lpg_ns.ILexStream;
	import $lpg_ns.Monitor;
	import $lpg_ns.NullExportedSymbolsException;
	import $lpg_ns.NullTerminalSymbolsException;
	import $lpg_ns.ParseTable;
	import $lpg_ns.RuleAction;
	import $lpg_ns.UndefinedEofSymbolException;
	import $lpg_ns.UnimplementedTerminalsException;	
	
	import edu.ustb.sei.rmg.random.condition.*;
	import java.util.*;
    ./
%End


%KeyWords
	norm
	bern
	geom
	pois
	pare
	cauc
	true
	false
	separate
	list
	unique for all
%End

-- Terminals
%Identifier
    IDENTIFIER
%End

%Terminals
    
    INTEGER_LITERAL 
    REAL_LITERAL
    CHAR_LITERAL
	STRING_LITERAL
	LONG_LITERAL
	DATE_LITERAL
    
    PERCENT ::= '%'
    
    PLUS     ::= '+'
    MINUS    ::= '-'
    MULTIPLY ::= '*'
    DIVIDE   ::= '/'

    GREATER       ::= '>'
    LESS          ::= '<'
    EQUAL         ::= '='
    GREATER_EQUAL ::= '>='
    LESS_EQUAL    ::= '<='
    NOT_EQUAL     ::= '<>'

    LPAREN   ::= '('
    RPAREN   ::= ')'
    LBRACE   ::= '{'
    RBRACE   ::= '}'
    LBRACKET ::= '['
    RBRACKET ::= ']'

    ARROW      ::= '->'
    BAR        ::= '|'
    COMMA      ::= ','
    COLON      ::= ':'
    COLONCOLON ::= '::'
    SEMICOLON  ::= ';'
    DOT        ::= '.'
    DOTDOT     ::= '..'
    AT_SYMBOL ::= '@'
    DOLLAR_SYMBOL ::= '$'
%End

%Start
	goal
%End


%Rules
goal ::= EqualConditionGoal
       | UnequalConditionGoal

EqualConditionGoal ::= IntegerConditionGoal
       | RealConditionGoal
       | LongConditionGoal
       | DateConditionGoal
       | StringConditionGoal
       | CharConditionGoal
       | BooleanConditionGoal
       | EnumConditionGoal
       | ArrayConditionGoal



UnequalConditionGoal ::= EqualConditionGoal '<>' FeaturePath ':' Possibility
/.$BeginCode
  Condition<?> eqCondition = (Condition<?>)getRhsSym(1);
  List<String> path = (List<String>)getRhsSym(3);
  Double realNumber = (Double)getRhsSym(5);
  UnequalCondition lc = Condition.createUnequalCondition(eqCondition, path, realNumber);
  setResult(lc);
  $EndCode
./

ArrayConditionGoal -> SeparateFunction
ArrayConditionGoal -> ListCondition

Possibility ::= RealLiteral
/.$BeginCode
  Object result = getRhsSym(1);
  setResult(result);
  $EndCode
./

DistFunc -> RealDistFunc | IntegerDistFunc 
-- | LongDistFunc

IDFuncName ::= geom
/.$NewCase./
IDFuncName ::= pois
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(literal);
$EndCode
./

RDFuncName ::= norm
/.$NewCase./
RDFuncName ::= pare
/.$NewCase./
RDFuncName ::= cauc
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(literal);
$EndCode
./

ParametersOpt ::= %empty
/.$BeginCode
   setResult(null);
$EndCode
./

ParametersOpt -> Parameters
                
Parameters ::= Parameter 
/.$BeginCode
  Double literal = (Double)getRhsSym(1);
  List<Double> list = new ArrayList<Double>();
  list.add(literal);
  setResult(list);
$EndCode
./

Parameters ::= Parameters ',' Parameter
/.$BeginCode
  List<Double> list = (List<Double>)getRhsSym(1);
  Double literal = (Double)getRhsSym(3);
  list.add(literal);
  setResult(list);
$EndCode
./

Parameter -> RealLiteral 

Parameter ::= IntegerLiteral
/.$BeginCode
  int literal = (int)getRhsSym(2);
  setResult((double)literal);
$EndCode
./

-- Array Type

SeparateFunction ::= separate '(' IntegerLiteral ')' '@' AtomicIntegerConditionWithoutDP 
/.$BeginCode
  Condition<Integer> sum = (Condition<Integer>)getRhsSym(3);
  Condition<Integer> count = (Condition<Integer>)getRhsSym(6);
  
  SeparateFunction sf = Condition.createSeparateFunction(sum, count);
  setResult(sf);
  $EndCode
./

ListCondition ::= list '(' goal ')' '@' AtomicIntegerConditionWithoutDP
/.$BeginCode
  Condition<?> element = (Condition<?>)getRhsSym(3);
  Condition<Integer> count = (Condition<Integer>)getRhsSym(6);
  
  ListCondition lc = Condition.createListCondition(element, count);
  setResult(lc);
  $EndCode
./

Scope ::= all
/.$BeginCode
  List<String> buf = new ArrayList<String>(0);
  setResult(buf);
  $EndCode
./

Scope ::= FeaturePath
/.$BeginCode
  List<String> buf = (List<String>)getRhsSym(1);
  setResult(buf);
  $EndCode
./

-- Integer Type --

IntegerConditionGoal ::= IntegerConditionList
/.$BeginCode
  Condition<Integer> c = (Condition<Integer>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Integer>)c).recover();
  }
  setResult(c);
  $EndCode
./

IntegerConditionGoal ::= unique IntegerConditionList for Scope
/.$BeginCode
  Condition<Integer> c = (Condition<Integer>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Integer>)c).recover();
  }
  c.uniqueScope = (List<String>)getRhsSym(4);
  setResult(c);
  $EndCode
./

IntegerConditionList ::= AtomicIntegerCondition
/.$BeginCode
  Condition<Integer> left = (Condition<Integer>)getRhsSym(1);
  ConditionList<Integer> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

IntegerConditionList ::= IntegerConditionList ',' AtomicIntegerCondition
/.$BeginCode
  ConditionList<Integer> left = (ConditionList<Integer>)getRhsSym(1);
  Condition<Integer> right = (Condition<Integer>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicIntegerCondition ::= AtomicIntegerConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Integer> cond = (Condition<Integer>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

IntegerConditionList ::= AtomicInevitableIntegerCondition
/.$BeginCode
  InevitableCondition<Integer> left = (InevitableCondition<Integer>)getRhsSym(1);
  ConditionList<Integer> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

IntegerConditionList ::= IntegerConditionList ',' AtomicInevitableIntegerCondition
/.$BeginCode
  ConditionList<Integer> left = (ConditionList<Integer>)getRhsSym(1);
  InevitableCondition<Integer> right = (InevitableCondition<Integer>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableIntegerCondition ::= '<' AtomicIntegerConditionWithoutDP '>'
/.$BeginCode
  Condition<Integer> body = (Condition<Integer>)getRhsSym(2);
  InevitableCondition<Integer> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./


AtomicIntegerCondition -> AtomicIntegerConditionWithoutDP
                         
AtomicIntegerConditionWithoutDP -> IntegerRange

AtomicIntegerConditionWithoutDP ::= IntegerLiteral
/.$BeginCode
  Integer literal = (Integer)getRhsSym(1);
  LiteralCondition<Integer> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicIntegerConditionWithoutDP -> IntegerDistFunc

AtomicIntegerConditionWithoutDP ::= '{' IntegerConditionList '}'
/.$BeginCode
  Condition<Integer> c = (Condition<Integer>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Integer>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./
                                  
IntegerRange ::= '[' IntegerLiteral '..'  IntegerLiteral ']'
/.$BeginCode
  Integer l = (Integer)getRhsSym(2);
  Integer u = (Integer)getRhsSym(4);
  IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

IntegerRange ::= '[' IntegerLiteral '..' '+' ']'
/.$BeginCode
  Integer l = (Integer)getRhsSym(2);
  Integer u = null;
  IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

IntegerRange ::= '[' '-' '..' IntegerLiteral ']'
/.$BeginCode
  Integer l = null;
  Integer u = (Integer)getRhsSym(4);
  IntegerRangeCondition cond = Condition.createIntegerRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

IntegerLiteral ::= '-' IntegerLiteral
/.$BeginCode
  Integer literal = (Integer)getRhsSym(2);
  setResult(-literal);
  $EndCode
./

IntegerLiteral ::= INTEGER_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(Integer.parseInt(literal));
  $EndCode
./

IntegerDistFunc ::= DistFunc '@' IntegerRange
/.$BeginCode
  DistributionFunction<?> f = (DistributionFunction<?>)getRhsSym(1);
  RangeCondition<Integer> b = (RangeCondition<Integer>)getRhsSym(3);
  CompositeIntegerFunction d = Condition.createCompositeIntegerFunction(f,b);
  setResult(d);
$EndCode
./

IntegerDistFunc ::= IDFuncName '(' ParametersOpt ')'
/.$BeginCode
  String name = (String)getRhsSym(1);
  List<Double> list = (List<Double>)getRhsSym(3);
  IntegerDistributionFunction d = Condition.createIntegerDistributionFunction(name,list);
  setResult(d);
$EndCode
./


-- Long Type --

LongConditionGoal ::= LongConditionList
/.$BeginCode
  Condition<Long> c = (Condition<Long>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Long>)c).recover();
  }
  setResult(c);
  $EndCode
./

LongConditionGoal ::= unique LongConditionList for Scope
/.$BeginCode
  Condition<Long> c = (Condition<Long>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Long>)c).recover();
  }
  c.uniqueScope = (List<String>)getRhsSym(4);
  setResult(c);
  $EndCode
./

LongConditionList ::= AtomicLongCondition
/.$BeginCode
  Condition<Long> left = (Condition<Long>)getRhsSym(1);
  ConditionList<Long> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

LongConditionList ::= LongConditionList ',' AtomicLongCondition
/.$BeginCode
  ConditionList<Long> left = (ConditionList<Long>)getRhsSym(1);
  Condition<Long> right = (Condition<Long>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicLongCondition ::= AtomicLongConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Long> cond = (Condition<Long>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

LongConditionList ::= AtomicInevitableLongCondition
/.$BeginCode
  InevitableCondition<Long> left = (InevitableCondition<Long>)getRhsSym(1);
  ConditionList<Long> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

LongConditionList ::= LongConditionList ',' AtomicInevitableLongCondition
/.$BeginCode
  ConditionList<Long> left = (ConditionList<Long>)getRhsSym(1);
  InevitableCondition<Long> right = (InevitableCondition<Long>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableLongCondition ::= '<' AtomicLongConditionWithoutDP '>'
/.$BeginCode
  Condition<Long> body = (Condition<Long>)getRhsSym(2);
  InevitableCondition<Long> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicLongCondition -> AtomicLongConditionWithoutDP
                         
AtomicLongConditionWithoutDP -> LongRange

AtomicLongConditionWithoutDP ::= LongLiteral
/.$BeginCode
  Long literal = (Long)getRhsSym(1);
  LiteralCondition<Long> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

--AtomicLongConditionWithoutDP -> LongDistFunc

AtomicLongConditionWithoutDP ::= '{' LongConditionList '}'
/.$BeginCode
  Condition<Long> c = (Condition<Long>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Long>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./
                                  
LongRange ::= '[' LongLiteral '..'  LongLiteral ']'
/.$BeginCode
  Long l = (Long)getRhsSym(2);
  Long u = (Long)getRhsSym(4);
  LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

LongRange ::= '[' LongLiteral '..' '+' ']'
/.$BeginCode
  Long l = (Long)getRhsSym(2);
  Long u = null;
  LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

LongRange ::= '[' '-' '..' LongLiteral ']'
/.$BeginCode
  Long l = null;
  Long u = (Long)getRhsSym(4);
  LongRangeCondition cond = Condition.createLongRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

LongLiteral ::= '-' LongLiteral
/.$BeginCode
  Long literal = (Long)getRhsSym(2);
  setResult(-literal);
  $EndCode
./

LongLiteral ::= LONG_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  literal = literal.substring(0,literal.length()-1);
  setResult(Long.parseLong(literal));
  $EndCode
./

--LongDistFunc ::= DistFunc '@' LongRange
--/.$BeginCode
--  DistributionFunction<?> f = (DistributionFunction<?>)getRhsSym(1);
--  RangeCondition<Long> b = (RangeCondition<Long>)getRhsSym(3);
--  CompositeLongFunction d = Condition.createCompositeLongFunction(f,b);
--  setResult(d);
--$EndCode
--./

--LongDistFunc ::= IDFuncName '(' ParametersOpt ')' LongSymbol
--/.$BeginCode
--  String name = (String)getRhsSym(1);
--  List<Double> list = (List<Double>)getRhsSym(3);
--  LongDistributionFunction d = Condition.createLongDistributionFunction(name,list);
--  setResult(d);
--$EndCode
--./

-- LongSymbol -> 'l' | 'L'

-- Real Type --

RealDistFunc ::= RDFuncName '(' ParametersOpt ')'
/.$BeginCode
  String name = (String)getRhsSym(1);
  List<Double> list = (List<Double>)getRhsSym(3);
  RealDistributionFunction d = Condition.createRealDistributionFunction(name,list);
  setResult(d);
$EndCode
./
 
RealConditionGoal ::= RealConditionList
/.$BeginCode
  Condition<Double> c = (Condition<Double>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Double>)c).recover();
  }
  setResult(c);
  $EndCode
./

RealConditionGoal ::= unique RealConditionList for Scope
/.$BeginCode
  Condition<Double> c = (Condition<Double>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Double>)c).recover();
  }
  c.uniqueScope = (List<String>)getRhsSym(4);
  setResult(c);
  $EndCode
./

RealConditionList ::= AtomicRealCondition
/.$BeginCode
  Condition<Double> left = (Condition<Double>)getRhsSym(1);
  ConditionList<Double> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./
                    
RealConditionList ::= RealConditionList ',' AtomicRealCondition
/.$BeginCode
  ConditionList<Double> left = (ConditionList<Double>)getRhsSym(1);
  Condition<Double> right = (Condition<Double>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicRealCondition ::= AtomicRealConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Double> cond = (Condition<Double>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

RealConditionList ::= AtomicInevitableRealCondition
/.$BeginCode
  InevitableCondition<Double> left = (InevitableCondition<Double>)getRhsSym(1);
  ConditionList<Double> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

RealConditionList ::= LongConditionList ',' AtomicInevitableRealCondition
/.$BeginCode
  ConditionList<Double> left = (ConditionList<Double>)getRhsSym(1);
  InevitableCondition<Double> right = (InevitableCondition<Double>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableRealCondition ::= '<' AtomicRealConditionWithoutDP '>'
/.$BeginCode
  Condition<Double> body = (Condition<Double>)getRhsSym(2);
  InevitableCondition<Double> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicRealCondition -> AtomicRealConditionWithoutDP
                         
AtomicRealConditionWithoutDP -> RealRange

AtomicRealConditionWithoutDP -> RealDistFunc

AtomicRealConditionWithoutDP ::= RealLiteral
/.$BeginCode
  Double literal = (Double)getRhsSym(1);
  LiteralCondition<Double> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicRealConditionWithoutDP ::= '{' RealConditionList '}'
/.$BeginCode
  Condition<Double> c = (Condition<Double>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Double>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./
                                  
RealRange ::= '[' RealLiteral '..'  RealLiteral ']'
/.$BeginCode
  Double l = (Double)getRhsSym(2);
  Double u = (Double)getRhsSym(4);
  RealRangeCondition cond = Condition.createRealRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

RealLiteral ::= '-' RealLiteral
/.$BeginCode
  Double literal = (Double)getRhsSym(2);
  setResult(-literal);
  $EndCode
./

RealLiteral ::= REAL_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(Double.parseDouble(literal));
  $EndCode
./

-- Char Type --

CharConditionGoal ::= CharConditionList
/.$BeginCode
  Condition<Character> c = (Condition<Character>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Character>)c).recover();
  }
  setResult(c);
  $EndCode
./

CharConditionList ::= AtomicCharCondition
/.$BeginCode
  Condition<Character> left = (Condition<Character>)getRhsSym(1);
  ConditionList<Character> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

CharConditionList ::= CharConditionList ',' AtomicCharCondition
/.$BeginCode
  ConditionList<Character> left = (ConditionList<Character>)getRhsSym(1);
  Condition<Character> right = (Condition<Character>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicCharCondition ::= AtomicCharConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Character> cond = (Condition<Character>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

CharConditionList ::= AtomicInevitableCharCondition
/.$BeginCode
  InevitableCondition<Character> left = (InevitableCondition<Character>)getRhsSym(1);
  ConditionList<Character> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

CharConditionList ::= CharConditionList ',' AtomicInevitableCharCondition
/.$BeginCode
  ConditionList<Character> left = (ConditionList<Character>)getRhsSym(1);
  InevitableCondition<Character> right = (InevitableCondition<Character>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableCharCondition ::= '<' AtomicCharConditionWithoutDP '>'
/.$BeginCode
  Condition<Character> body = (Condition<Character>)getRhsSym(2);
  InevitableCondition<Character> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicCharCondition -> AtomicCharConditionWithoutDP
                         
AtomicCharConditionWithoutDP -> CharRange

AtomicCharConditionWithoutDP ::= CharLiteral
/.$BeginCode
  Character literal = (Character)getRhsSym(1);
  LiteralCondition<Character> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicCharConditionWithoutDP ::= '{' CharConditionList '}'
/.$BeginCode
  Condition<Character> c = (Condition<Character>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Character>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./

CharRange ::= '[' CharLiteral '..'  CharLiteral ']'
/.$BeginCode
  Character l = (Character)getRhsSym(2);
  Character u = (Character)getRhsSym(4);
  CharRangeCondition cond = Condition.createCharRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

CharLiteral ::= CHAR_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(Condition.convertChar(literal));
  $EndCode
./

-- String Type --

StringConditionGoal ::= StringConditionList
/.$BeginCode
  Condition<String> c = (Condition<String>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<String>)c).recover();
  }
  setResult(c);
  $EndCode
./

StringConditionGoal ::= unique StringConditionList for Scope
/.$BeginCode
  Condition<String> c = (Condition<String>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<String>)c).recover();
  }
  c.uniqueScope = (List<String>)getRhsSym(4);
  setResult(c);
  $EndCode
./

StringConditionList ::= AtomicStringCondition
/.$BeginCode
  Condition<String> left = (Condition<String>)getRhsSym(1);
  ConditionList<String> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

StringConditionList ::= StringConditionList ',' AtomicStringCondition
/.$BeginCode
  ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
  Condition<String> right = (Condition<String>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicStringCondition ::= AtomicStringConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<String> cond = (Condition<String>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

StringConditionList ::= AtomicInevitableStringCondition
/.$BeginCode
  InevitableCondition<String> left = (InevitableCondition<String>)getRhsSym(1);
  ConditionList<String> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

StringConditionList ::= StringConditionList ',' AtomicInevitableStringCondition
/.$BeginCode
  ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
  InevitableCondition<String> right = (InevitableCondition<String>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableStringCondition ::= '<' AtomicStringConditionWithoutDP '>'
/.$BeginCode
  Condition<String> body = (Condition<String>)getRhsSym(2);
  InevitableCondition<String> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicStringCondition -> AtomicStringConditionWithoutDP

AtomicStringConditionWithoutDP -> AtomicStringConcatConditionWithoutDP

AtomicStringConcatConditionWithoutDP ::= AtomicStringConcatConditionWithoutDP '+' AtomicStringSimpleConditionWithoutDP
/.$BeginCode
  Condition<String> first = (Condition<String>)getRhsSym(1);
  Condition<String> second = (Condition<String>)getRhsSym(3);
  
  StringConcatCondition result = Condition.createStringConcatCondition(first, second);
  setResult(result);
  $EndCode
./

AtomicStringConcatConditionWithoutDP ->  AtomicStringSimpleConditionWithoutDP

AtomicStringSimpleConditionWithoutDP ::= AtomicCharConditionWithoutDP '@' AtomicIntegerConditionWithoutDP
/.$BeginCode
  Condition<Character> charRange = (Condition<Character>)getRhsSym(1);
  Condition<Integer> sizeRange = (Condition<Integer>)getRhsSym(3);
  
  StringRangeCondition result = Condition.createStringRangeCondition(charRange,sizeRange);
  setResult(result);
  $EndCode
./


AtomicStringSimpleConditionWithoutDP ::= StringLiteral
/.$BeginCode
  String literal = (String)getRhsSym(1);
  LiteralCondition<String> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicStringSimpleConditionWithoutDP ::= '{' StringConditionList '}'
/.$BeginCode
  Condition<String> c = (Condition<String>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<String>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./

AtomicStringSimpleConditionWithoutDP ::= FeaturePath
/.$BeginCode
  List<String> list = (List<String>)getRhsSym(1);
  FeaturePathCondition<String> sfc = Condition.createStringFeatureCondition(list);
  setResult(sfc);
  $EndCode
./
                                 
StringLiteral ::= STRING_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(Condition.convertString(literal));
  $EndCode
./

FeaturePath ::= '$' IDENTIFIER
/.$BeginCode
  String literal = getRhsIToken(2).toString();
  List<String> list = new ArrayList<String>();
  list.add(literal);
  setResult(list);
  $EndCode
./

FeaturePath ::= FeaturePath '.' IDENTIFIER
/.$BeginCode
  List<String> list = (List<String>)getRhsSym(1);
  String literal = getRhsIToken(3).toString();
  list.add(literal);
  setResult(list);
  $EndCode
./


-- Boolean Type --

BooleanConditionGoal ::= BooleanConditionList
/.$BeginCode
  Condition<Boolean> c = (Condition<Boolean>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Boolean>)c).recover();
  }
  setResult(c);
  $EndCode
./

BooleanConditionList ::= AtomicBooleanCondition
/.$BeginCode
  Condition<Boolean> left = (Condition<Boolean>)getRhsSym(1);
  ConditionList<Boolean> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

BooleanConditionList ::= BooleanConditionList ',' AtomicBooleanCondition
/.$BeginCode
  ConditionList<Boolean> left = (ConditionList<Boolean>)getRhsSym(1);
  Condition<Boolean> right = (Condition<Boolean>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicBooleanCondition ::= AtomicBooleanConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Boolean> cond = (Condition<Boolean>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

BooleanConditionList ::= AtomicInevitableBooleanCondition
/.$BeginCode
  InevitableCondition<Boolean> left = (InevitableCondition<Boolean>)getRhsSym(1);
  ConditionList<Boolean> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

BooleanConditionList ::= BooleanConditionList ',' AtomicInevitableBooleanCondition
/.$BeginCode
  ConditionList<Boolean> left = (ConditionList<Boolean>)getRhsSym(1);
  InevitableCondition<Boolean> right = (InevitableCondition<Boolean>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableBooleanCondition ::= '<' AtomicBooleanConditionWithoutDP '>'
/.$BeginCode
  Condition<Boolean> body = (Condition<Boolean>)getRhsSym(2);
  InevitableCondition<Boolean> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicBooleanCondition -> AtomicBooleanConditionWithoutDP
                         
AtomicBooleanConditionWithoutDP ::= BooleanLiteral
/.$BeginCode
  Boolean literal = (Boolean)getRhsSym(1);
  LiteralCondition<Boolean> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicBooleanConditionWithoutDP ::= '{' BooleanConditionList '}'
/.$BeginCode
  Condition<Boolean> c = (Condition<Boolean>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Boolean>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./

BooleanLiteral ::= true
/.$BeginCode
  setResult(true);
  $EndCode
./

BooleanLiteral ::= false
/.$BeginCode
  setResult(false);
  $EndCode
./

-- Enum Type --

EnumConditionGoal ::= EnumConditionList
/.$BeginCode
  Condition<String> c = (Condition<String>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<String>)c).recover();
  }
  setResult(c);
  $EndCode
./

EnumConditionList ::= AtomicEnumCondition
/.$BeginCode
  Condition<String> left = (Condition<String>)getRhsSym(1);
  ConditionList<String> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

EnumConditionList ::=  EnumConditionList ',' AtomicEnumCondition
/.$BeginCode
  ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
  Condition<String> right = (Condition<String>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicEnumCondition ::= AtomicEnumConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<String> cond = (Condition<String>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

EnumConditionList ::= AtomicInevitableEnumCondition
/.$BeginCode
  InevitableCondition<String> left = (InevitableCondition<String>)getRhsSym(1);
  ConditionList<String> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

EnumConditionList ::= EnumConditionList ',' AtomicInevitableEnumCondition
/.$BeginCode
  ConditionList<String> left = (ConditionList<String>)getRhsSym(1);
  InevitableCondition<String> right = (InevitableCondition<String>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableEnumCondition ::= '<' AtomicEnumConditionWithoutDP '>'
/.$BeginCode
  Condition<String> body = (Condition<String>)getRhsSym(2);
  InevitableCondition<String> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicEnumCondition -> AtomicEnumConditionWithoutDP

AtomicEnumConditionWithoutDP ::= EnumLiteral
/.$BeginCode
  String literal = (String)getRhsSym(1);
  LiteralCondition<String> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicEnumConditionWithoutDP ::= '{' EnumConditionList '}'
/.$BeginCode
  Condition<String> c = (Condition<String>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<String>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./
                           
EnumLiteral ::= IDENTIFIER
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(literal);
  $EndCode
./

-- Date Type --

DateConditionGoal ::= DateConditionList
/.$BeginCode
  Condition<Calendar> c = (Condition<Calendar>)getRhsSym(1);
  if(c instanceof ConditionList) {
	  ((ConditionList<Calendar>)c).recover();
  }
  setResult(c);
  $EndCode
./

DateConditionGoal ::= unique DateConditionList for Scope
/.$BeginCode
  Condition<Calendar> c = (Condition<Calendar>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Calendar>)c).recover();
  }
  c.uniqueScope = (List<String>)getRhsSym(4);
  setResult(c);
  $EndCode
./

DateConditionList ::= AtomicDateCondition
/.$BeginCode
  Condition<Calendar> left = (Condition<Calendar>)getRhsSym(1);
  ConditionList<Calendar> cond = Condition.createConditionList(left,null);
  setResult(cond);
  $EndCode
./

DateConditionList ::= DateConditionList ',' AtomicDateCondition
/.$BeginCode
  ConditionList<Calendar> left = (ConditionList<Calendar>)getRhsSym(1);
  Condition<Calendar> right = (Condition<Calendar>)getRhsSym(3);
  left.list.add(right);
  setResult(left);
  $EndCode
./

AtomicDateCondition ::= AtomicDateConditionWithoutDP ':' Possibility
/.$BeginCode
  Double realNumber = (Double)getRhsSym(3);
  Condition<Calendar> cond = (Condition<Calendar>)getRhsSym(1);
  cond.possibility = realNumber;
  setResult(cond);
  $EndCode
./

DateConditionList ::= AtomicInevitableDateCondition
/.$BeginCode
  InevitableCondition<Calendar> left = (InevitableCondition<Calendar>)getRhsSym(1);
  ConditionList<Calendar> cond = Condition.createConditionList(null,null);
  cond.inevitableConditionList.add(left);
  setResult(cond);
  $EndCode
./

DateConditionList ::= DateConditionList ',' AtomicInevitableDateCondition
/.$BeginCode
  ConditionList<Calendar> left = (ConditionList<Calendar>)getRhsSym(1);
  InevitableCondition<Calendar> right = (InevitableCondition<Calendar>)getRhsSym(3);
  left.inevitableConditionList.add(right);
  setResult(left);
  $EndCode
./

AtomicInevitableDateCondition ::= '<' AtomicDateConditionWithoutDP '>'
/.$BeginCode
  Condition<Calendar> body = (Condition<Calendar>)getRhsSym(2);
  InevitableCondition<Calendar> iC = Condition.createInevitableCondition(body);
  setResult(iC);
  $EndCode
./

AtomicDateCondition -> AtomicDateConditionWithoutDP
                         
AtomicDateConditionWithoutDP -> DateRange

AtomicDateConditionWithoutDP ::= DateLiteral
/.$BeginCode
  Calendar literal = (Calendar)getRhsSym(1);
  LiteralCondition<Calendar> result = Condition.createLiteralCondition(literal);
  setResult(result);
  $EndCode
./

AtomicDateConditionWithoutDP ::= '{' DateConditionList '}'
/.$BeginCode
  Condition<Calendar> c = (Condition<Calendar>)getRhsSym(2);
  if(c instanceof ConditionList) {
	  ((ConditionList<Calendar>)c).recover();
  }
  setResult(Condition.createAtomicCondition(c));
  $EndCode
./
                                  
DateRange ::= '[' DateLiteral '..'  DateLiteral ']'
/.$BeginCode
  Calendar l = (Calendar)getRhsSym(2);
  Calendar u = (Calendar)getRhsSym(4);
  DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

DateRange ::= '[' DateLiteral '..' '+' ']'
/.$BeginCode
  Calendar l = (Calendar)getRhsSym(2);
  Calendar u = null;
  DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

DateRange ::= '[' '-' '..' DateLiteral ']'
/.$BeginCode
  Calendar l = null;
  Calendar u = (Calendar)getRhsSym(4);
  DateRangeCondition cond = Condition.createDateRangeCondition(l,u);
  setResult(cond);
  $EndCode
./

DateLiteral ::= DATE_LITERAL
/.$BeginCode
  String literal = getRhsIToken(1).toString();
  setResult(Condition.convertCalendar(literal));
  $EndCode
./
%End