package edu.ustb.sei.mt4mt.base.actions;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jdt.internal.junit.runner.ITestRunner;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;

public class LaunchExecutor extends AbstractExecutor {
	private long maxWait = 10000; //10 seconds
	
	public long getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(long maxWait) {
		this.maxWait = maxWait;
	}

	public LaunchExecutor() {
		super();
	}
	
	public LaunchExecutor(String ln) {
		super();
		this.launcherName = ln;
	}
	
	public LaunchExecutor(String ln, long w) {
		this(ln);
		this.maxWait = w;
	}
	
	private String launcherName;

	@Override
	public void beforeRun(Context context) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void actualRun(Context context) {
		ILaunchConfiguration launchConfig = getLaunchConfiguration(this.getLauncherName());
		

		try {
			ILaunch result = launchConfig.launch(ILaunchManager.RUN_MODE, null);
			long timeWaited = 0;
			
			while(result.isTerminated()==false && (maxWait<=0 || timeWaited < maxWait)) {
				Thread.sleep(500);
				timeWaited += 500;
			}
			
			if(result.isTerminated()) {
				return;
			} else {
				// assume that test fails due to timeout
				throw new InvalidTestException("the launch configuration is not terminated until max waiting time", false);
			}
		} catch(CoreException | InterruptedException  e) {
			throw new RuntimeException(e);
		} finally {
//			DebugPlugin.getDefault().removeDebugEventListener(listener);
		}
	}

	@Override
	public void afterRun(Context context) {
		// TODO Auto-generated method stub

	}

	
	protected ILaunchConfiguration getLaunchConfiguration(String name) {
		ILaunchConfiguration[] launchConfigurations;
		try {
			launchConfigurations = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations();
			for(ILaunchConfiguration o : launchConfigurations) {
				if(name.equals(o.getName()))
					return o;
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getLauncherName() {
		return launcherName;
	}

	public void setLauncherName(String launcherName) {
		this.launcherName = launcherName;
	}
}
