package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class DefaultFunction<T> extends AbstractRandomFunction<T> {
	
	private String name;

	public DefaultFunction(String name) {
		this.name = name;
	}

	@SuppressWarnings("unchecked")
	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		AbstractRandomFunction<T> func = (AbstractRandomFunction<T>) t.getDefinedRandomFunction().get(this.name);
		return func.getValue(t);
	}

}
