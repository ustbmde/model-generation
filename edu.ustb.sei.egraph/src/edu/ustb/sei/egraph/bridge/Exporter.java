package edu.ustb.sei.egraph.bridge;

import java.util.Map;

import edu.ustb.sei.egraph.structure.EGraph;

public interface Exporter<T,NT,ET,NAT,EAT> {
	void exportGraph(T output, EGraph<NT,ET,NAT,EAT> graph);
	void save(T output, Map<?,?> config) throws Exception;
}
