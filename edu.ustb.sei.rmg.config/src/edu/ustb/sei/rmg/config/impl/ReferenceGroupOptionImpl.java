/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.ReferenceGroupOption;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Group Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl#getReferencesRef <em>References Ref</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceGroupOptionImpl#getTypeFilter <em>Type Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceGroupOptionImpl extends AbstractReferenceOptionImpl implements ReferenceGroupOption {
	/**
	 * The cached value of the '{@link #getReferencesRef() <em>References Ref</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesRef()
	 * @generated
	 * @ordered
	 */
	protected EList<EReference> referencesRef;

	/**
	 * The cached value of the '{@link #getTypeFilter() <em>Type Filter</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeFilter()
	 * @generated
	 * @ordered
	 */
	protected EList<EClass> typeFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceGroupOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.REFERENCE_GROUP_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EReference> getReferencesRef() {
		if (referencesRef == null) {
			referencesRef = new EObjectResolvingEList<EReference>(EReference.class, this, ConfigPackage.REFERENCE_GROUP_OPTION__REFERENCES_REF);
		}
		return referencesRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EClass> getTypeFilter() {
		if (typeFilter == null) {
			typeFilter = new EObjectResolvingEList<EClass>(EClass.class, this, ConfigPackage.REFERENCE_GROUP_OPTION__TYPE_FILTER);
		}
		return typeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_GROUP_OPTION__REFERENCES_REF:
				return getReferencesRef();
			case ConfigPackage.REFERENCE_GROUP_OPTION__TYPE_FILTER:
				return getTypeFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_GROUP_OPTION__REFERENCES_REF:
				getReferencesRef().clear();
				getReferencesRef().addAll((Collection<? extends EReference>)newValue);
				return;
			case ConfigPackage.REFERENCE_GROUP_OPTION__TYPE_FILTER:
				getTypeFilter().clear();
				getTypeFilter().addAll((Collection<? extends EClass>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_GROUP_OPTION__REFERENCES_REF:
				getReferencesRef().clear();
				return;
			case ConfigPackage.REFERENCE_GROUP_OPTION__TYPE_FILTER:
				getTypeFilter().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_GROUP_OPTION__REFERENCES_REF:
				return referencesRef != null && !referencesRef.isEmpty();
			case ConfigPackage.REFERENCE_GROUP_OPTION__TYPE_FILTER:
				return typeFilter != null && !typeFilter.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ReferenceGroupOptionImpl
