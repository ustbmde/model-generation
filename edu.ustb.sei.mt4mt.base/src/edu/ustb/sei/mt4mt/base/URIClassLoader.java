package edu.ustb.sei.mt4mt.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class URIClassLoader extends ClassLoader {

	public URIClassLoader() {
		// TODO Auto-generated constructor stub
	}

	public URIClassLoader(ClassLoader parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}
	
	
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		try {
			Class<?> c = super.loadClass(name);
			return c;
		} catch(Exception e) {}
		String clazzName = name.substring(name.lastIndexOf('/')+1,name.lastIndexOf(".class"));
		String dir = name.substring(0,name.lastIndexOf('/')+1);
		String fileName = dir+clazzName.replace('.', '/').concat(".class");
		
		
		Class<?> c = findLoadedClass(clazzName);
		
		if(c==null) {
			try {
				if(getParent()!=null) {
					c = getParent().loadClass(clazzName);
				}
			}catch(ClassNotFoundException e) {
				
			}
			
			if(c==null) {
				try {
					InputStream is = new FileInputStream(fileName);
					byte[] b = new byte[is.available()];
					is.read(b);
					is.close();
					return defineClass(clazzName, b, 0, b.length);
				} catch(IOException e) {
					throw new ClassNotFoundException(clazzName);
				}
			}
		}
		return c;
	}

}
