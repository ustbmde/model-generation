/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public class StructuralrmgChoice extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement {
	
	public StructuralrmgChoice(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.explode(getChildren(), "|");
	}
	
}
