package edu.ustb.sei.mt4mt.base.exceptions;

public class InvalidTestException extends RuntimeException {
	private boolean retry;

	public boolean shouldRetry() {
		return retry;
	}

	public InvalidTestException(String message, boolean r) {
		super(message);
		this.retry = r;
	}

}
