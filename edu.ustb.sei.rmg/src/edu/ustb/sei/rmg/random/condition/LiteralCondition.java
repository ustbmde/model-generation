package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

/*
 * for String and Enum literals
 */
public class LiteralCondition<T> extends Condition<T> {
	public T literal;

	@Override
	public T getValue(RandomUtil r, Object... extra) { 
		return r.randomConstant(literal);
	}

	@Override
	public Object[] getGenerationBound() {
		return new Object[]{literal,literal};
	}
}
