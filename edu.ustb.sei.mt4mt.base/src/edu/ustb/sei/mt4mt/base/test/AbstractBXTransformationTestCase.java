package edu.ustb.sei.mt4mt.base.test;

import edu.ustb.sei.mt4mt.base.IAction;

abstract public class AbstractBXTransformationTestCase extends AbstractTransformationTestCase {
	
	public AbstractBXTransformationTestCase(String[] inputData, String fwdName, String[] fwdSource,
			String[] fwdView, String bwdName, String[] bwdSource, String[] bwdView, String[] bwdUpdatedSource) {
		super();
		this.inputData = inputData;
		this.fwdName = fwdName;
		this.fwdSource = fwdSource;
		this.fwdView = fwdView;
		this.bwdName = bwdName;
		this.bwdSource = bwdSource;
		this.bwdView = bwdView;
		this.bwdUpdatedSource = bwdUpdatedSource;
	}

	/**
	 * the URIs of the actual input model data
	 */
	protected String[] inputData;

	/**
	 * the name of the launcher of the forward transformation
	 */
	protected String fwdName;
	
	/**
	 * the URIs of the source models of the forward transformation
	 */
	protected String[] fwdSource;
	
	/**
	 * the URIs of the view models of the forward transformation
	 */
	protected String[] fwdView;
	
	
	/**
	 * the name of the launcher of the backward transformation
	 */
	protected String bwdName;
	
	/**
	 * the URIs of the source models of the backward transformation
	 */
	protected String[] bwdSource;
	
	/**
	 * the URIs of the target models of the backward transformation
	 */
	protected String[] bwdView;
	
	/**
	 * the URIs of the updated source models of the backward transformation
	 */
	protected String[] bwdUpdatedSource;

	public AbstractBXTransformationTestCase(IAction action) {
		super(action);
	}

}
