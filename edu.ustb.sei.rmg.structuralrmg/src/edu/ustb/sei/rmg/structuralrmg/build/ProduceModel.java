package edu.ustb.sei.rmg.structuralrmg.build;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.Random;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.eclipse.core.internal.jobs.OrderedLock;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.commonutil.util.BidirectionalMap;
import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.commonutil.util.PairHashMap;
import edu.ustb.sei.rmg.structuralrmg.AttributeBinding;
import edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.CompositionSort;
import edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

public class ProduceModel implements IUnfolder {
	final static int MAX_SIZE = 100;
	
	private BidirectionalMap<Object,Object> templateTypeMap = new BidirectionalMap<Object,Object>();
	private HashMap<EObject,TemplateInstance> elementInstanceMap = new HashMap<EObject,TemplateInstance>();
	private List<EObject> allElements = new LinkedList<EObject>();
	private EPackage metamodel = null;
	private TemplateModel template = null;
	
	private PairHashMap<EObject, EReference, List<Object>> duplicate = new PairHashMap<EObject, EReference, List<Object>>();
	
	public void register(EObject o, EReference r, Object v) {
		List<Object> list = getShadowList(o,r);
		if(list==null) {
			list = new ArrayList<Object>();
			duplicate.put(o, r, list);			
		}
		list.add(v);
	}
	public List<Object> getShadowList(EObject o, EReference r) {
		return duplicate.get(o,r);
	}
	public void writeBackShadowList() {
		duplicate.getMap().entrySet().parallelStream().forEach(new Consumer<Entry<EObject, HashMap<EReference, List<Object>>>>() {
			@Override
			public void accept(Entry<EObject, HashMap<EReference, List<Object>>> t) {
				for(Entry<EReference, List<Object>> l : t.getValue().entrySet()) {
					t.getKey().eSet(l.getKey(), l.getValue());
				}
			}
		});
	}
	
	
	protected EObject createObject(EClass cls) {
		EObject o = EcoreUtil.create(cls);
		allElements.add(o);
		return o;
	}
	
	public static Random random = new Random();
	
	
	public void travelPackage(EPackage pkg) {
		EAnnotation pa = pkg.getEAnnotation("model");
		if(pa!=null) {
			template = (TemplateModel) pa.getReferences().get(0);
		}
		
		TreeIterator<EObject> it = pkg.eAllContents();
		while(it.hasNext()) {
			EObject o = it.next();
			if(o instanceof EClass) {
				EClass cls = (EClass)o;
				EAnnotation ann = cls.getEAnnotation("template");
				if(ann!=null) {
					templateTypeMap.put(ann.getReferences().get(0), cls);
					for(EStructuralFeature f : cls.getEStructuralFeatures()) {
						EAnnotation fann = f.getEAnnotation("nested");
						if(fann!=null) {
							templateTypeMap.put(fann.getReferences().get(0), f);
						}
					}
				}
			}
		}
		metamodel = pkg;
	}
	
	public Resource buildResource(Resource root) {
		//no unique root: to be fixed
		long start = Calendar.getInstance().getTimeInMillis();
		for(EObject o : root.getContents()) {
			buildElement(o);
		}
		this.writeBackShadowList();
		long end = Calendar.getInstance().getTimeInMillis();
		
		long timeSpan = end-start;
		
		URI uri = root.getURI().trimFileExtension().appendFileExtension(template.getPackages().get(0).getName());
		Resource nres = root.getResourceSet().createResource(uri);
		int count = 0;
		for(EObject o : allElements) {
			if(o.eContainer()==null)
				nres.getContents().add(o);
			count++;
		}
		
		System.out.println("Total elements in the required model:"+count + "\t "+timeSpan+" ms was used exlcuding the final checking time");
		
		return nres;
	}
	
	private TemplateInstance buildElement(EObject root) {
		TemplateInstance ti = elementInstanceMap.get(root);
		if(ti!=null) 
			return ti;

		EClass cls = root.eClass();
		
		Template temp = (Template) templateTypeMap.backward(cls);
		
		if(temp==null) return null;
		
		ti = new TemplateInstance(root,temp);
		
		elementInstanceMap.put(root, ti);
		
		for(TemplateNode n : temp.getAllNodes()) {
			if(n instanceof PrimitiveTemplateNode) {
				if(n.isSet()) {
					ArrayList<EObject> arr = new ArrayList<EObject>();
					int max = n.getMax();
					if(max<0) max = MAX_SIZE;
					int min = n.getMin();
					
					int size = min + random.nextInt(max);
					
					while(size>=0) {
						EObject i = createObject(((PrimitiveTemplateNode) n).getType());
						arr.add(i);
						writeAttribute(root, cls, (PrimitiveTemplateNode) n,i);
						size--;
					}
					
					ti.putNodeInstance(n,arr);
				} else {
					EObject i = createObject(((PrimitiveTemplateNode) n).getType());
					
					ti.putNodeInstance(n, i);
					writeAttribute(root, cls, (PrimitiveTemplateNode) n,i);
				}
				
			} else {
				NestedTemplateNode ntn = (NestedTemplateNode)n;
				CompositionSort sort = ntn.getSort();
				if(sort==CompositionSort.REVERSED) {
					//taverse eContainer first;
					if(n.isSet()) {
						throw new RuntimeException("I don't know how to create a set of reversed nodes!");
					} else {
						EObject cur = root;
						
						while(cur!=null) {
							EClass rCls = cur.eClass();
							Template rTemp = (Template) templateTypeMap.backward(rCls);
							if(ntn.getTemplates().contains(rTemp)) {
								TemplateInstance cti = buildElement(cur);
								ti.putNodeInstance(n, cti);
								break;
							}
							cur = cur.eContainer();
						}
					}
					
				} else {
					EReference ref = (EReference) templateTypeMap.forward(n);
					
					if(n.isSet()) {
						ArrayList<Object> arr = new ArrayList<Object>();
						List<EObject> cElems = (List<EObject>) root.eGet(ref);
						for(EObject o : cElems) {
							TemplateInstance cti = buildElement(o);
							if(cti==null) continue;
							arr.add(cti);
						}
						ti.putNodeInstance(n, arr);;
					} else {
						EObject c = (EObject) root.eGet(ref);
						if(c==null) continue;
						TemplateInstance cti = buildElement(c);
						ti.putNodeInstance(n, cti);
					}
				}
			}
		}
		
		for(TemplateEdge e : temp.getAllEdges()) {
			EReference ref = e.getReference();
			
			if(e instanceof PrimitiveTemplateEdge) {
				TemplateNode sn = ((PrimitiveTemplateEdge)e).getSource();
				TemplateNode tn = ((PrimitiveTemplateEdge)e).getTarget();
				Object so =  ti.getNodeInstance(sn);
				Integer sp = ((PrimitiveTemplateEdge) e).getSourcePos();
				Object to =  ti.getNodeInstance(tn);
				Integer tp = ((PrimitiveTemplateEdge) e).getTargetPos();
				
				if(so==null || to==null) continue;
				
				if(so instanceof List) {
					List<Object> sarr = (List<Object>)so;
					if(sp==null) {
						for(Object s : sarr) {
							createEdgeFromSource(temp, e, tn, ref, s, to, tp);
						}						
					} else {
						if(sarr.size()==0) continue;
						if(sp==-1 || sp>=sarr.size()) sp = sarr.size()-1;
						Object s = sarr.get(sp);
						createEdgeFromSource(temp, e, tn, ref, s, to, tp);
					}
					
				} else {
					Object s = so;
					createEdgeFromSource(temp, e, tn, ref, s, to, tp);
				}
			} else if(e instanceof ChainTemplateEdge) {
				TemplateNode n = ((ChainTemplateEdge)e).getNode();
				Object node =  ti.getNodeInstance(n);
				
				if(node==null) continue;
				
				if(node instanceof List) {
					List<Object> nodeArr = (List<Object>)node;
					Object first = null, prev = null, cur = null;
					
					if(nodeArr.size()!=0) {
						for(int i = 0; i <=nodeArr.size() ; i++) {
							if(i==0) {
								first = nodeArr.get(0);
								prev = first;
							} else {
								if(i==nodeArr.size()) {
									if(((ChainTemplateEdge) e).isRound()==false) break;
									cur = first;
								}
								else cur = nodeArr.get(i);
								createEdgeFromSource(temp, e, n, ref, prev, cur, null);
								prev = cur;
							}
						}
					}
				}
				
			}
			
		}
		
		
		
		return ti;
	}

	private void createEdgeFromSource(Template temp, TemplateEdge e, TemplateNode tn, EReference ref, Object s,
			Object to, Integer tp) {
		
		if(s instanceof EObject) {
			createEdge(temp, e, tn, ref, s, to,tp);
		} else if (s instanceof TemplateInstance){
			List<Object> ssl = ((TemplateInstance) s).getObject(temp, e, true);
			for(Object ss : ssl) {
				createEdge(temp, e, tn, ref, ss, to,tp);							
			}
		}
	}

	protected void writeAttribute(EObject host, EClass hostClass, PrimitiveTemplateNode node,EObject guest) {
		// write through attribute
		for(EAttribute att : hostClass.getEAllAttributes()) {
			EAnnotation ann = att.getEAnnotation("attribute");
			if(ann==null) continue;
			AttributeBinding ab = (AttributeBinding)ann.getReferences().get(0);
			if(ab.eContainer()!=node) continue;
			
			Object value = host.eGet(att);
			guest.eSet(ab.getAttribute(), value);
		}
	}

	protected void createEdge(Template temp, TemplateEdge e, TemplateNode tn,
			EReference ref, Object s, Object to, Integer tp) {
		if(((EClass)ref.eContainer()).isInstance(s)) {
			if(to instanceof List) {
				List<Object> tarr = (List<Object>)to;
				if(tp==null) {
					for(Object t : tarr) {
						if(t instanceof EObject) {
							if(ref.getEType().isInstance(t)) {
								createEdge(s, ref, t);
							}
						} else if(t instanceof TemplateInstance){
							
							List<Object> ol = ((TemplateInstance) t).getObject(temp,e,false);
							for(Object o : ol) {
								createEdge(s, ref, o);							
							}
						}
					}
				} else {
					if(tarr==null || tarr.size()==0) return;
					if(tp==-1 || tp>=tarr.size()) tp = tarr.size()-1;
					Object t = tarr.get(tp);
					if(t instanceof EObject) {
						if(ref.getEType().isInstance(t)) {
							createEdge(s, ref, t);
						}
					} else if(t instanceof TemplateInstance){
						
						List<Object> ol = ((TemplateInstance) t).getObject(temp,e,false);
						for(Object o : ol) {
							createEdge(s, ref, o);							
						}
					}
				}
			} else {
				Object t = to;
				if(t instanceof EObject) {
					if(ref.getEType().isInstance(t)) {
						createEdge(s, ref, t);
					}
				} else if(t instanceof TemplateInstance){
					List<Object> ol = ((TemplateInstance) t).getObject(temp,e,false);
					for(Object o : ol) {
						createEdge(s, ref, o);							
					}
				}
			}
			
			
		}
	}

	protected void createEdge(Object s, EReference ref, Object o) {
		if(ref.isMany()) {
			if(ref.isUnique()) {
				this.register((EObject)s, ref, o);
			} else
				((List<Object>)((EObject) s).eGet(ref)).add(o);
			
		} else {
			((EObject) s).eSet(ref, o);
		}
	}
}