/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.*;

import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.test.AbstractTransformationTestCase;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MT4MTFactoryImpl extends EFactoryImpl implements MT4MTFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MT4MTFactory init() {
		try {
			MT4MTFactory theMT4MTFactory = (MT4MTFactory)EPackage.Registry.INSTANCE.getEFactory(MT4MTPackage.eNS_URI);
			if (theMT4MTFactory != null) {
				return theMT4MTFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MT4MTFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MT4MTPackage.TEST_SUITE: return createTestSuite();
			case MT4MTPackage.CUSTOMIZABLE_TEST_CASE: return createCustomizableTestCase();
			case MT4MTPackage.GET_PUT_TEST_CASE: return createGetPutTestCase();
			case MT4MTPackage.PUT_GET_TEST_CASE: return createPutGetTestCase();
			case MT4MTPackage.SEQUENCE_ACTION: return createSequenceAction();
			case MT4MTPackage.LOAD_MODEL_ACTION: return createLoadModelAction();
			case MT4MTPackage.SAVE_MODEL_ACTION: return createSaveModelAction();
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION: return createExecuteLauncherAction();
			case MT4MTPackage.EVALUATE_OCL_QUERY_ACTION: return createEvaluateOCLQueryAction();
			case MT4MTPackage.COPY_MODEL_ACTION: return createCopyModelAction();
			case MT4MTPackage.COMPARE_MODEL_ACTION: return createCompareModelAction();
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION: return createAssertComparisonResultAction();
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION: return createAssertOCLInvariantAction();
			case MT4MTPackage.USER_ACTION: return createUserAction();
			case MT4MTPackage.SET_VARIABLE_ACTION: return createSetVariableAction();
			case MT4MTPackage.UPDATE_MODEL_ACTION: return createUpdateModelAction();
			case MT4MTPackage.WRAPPER_TEST_CASE: return createWrapperTestCase();
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE: return createAutoExecutorTestSuite();
			case MT4MTPackage.CLEAR_ACTION: return createClearAction();
			case MT4MTPackage.EXTERNAL_ACTION: return createExternalAction();
			case MT4MTPackage.EXTERNAL_ACTION_PARAMETER: return createExternalActionParameter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MT4MTPackage.DIFF_KIND:
				return createDiffKindFromString(eDataType, initialValue);
			case MT4MTPackage.IACTION:
				return createIActionFromString(eDataType, initialValue);
			case MT4MTPackage.ABSTRACT_TRANSFORMATION_TEST_CASE:
				return createAbstractTransformationTestCaseFromString(eDataType, initialValue);
			case MT4MTPackage.IEXTERNAL_ACTION:
				return createIExternalActionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MT4MTPackage.DIFF_KIND:
				return convertDiffKindToString(eDataType, instanceValue);
			case MT4MTPackage.IACTION:
				return convertIActionToString(eDataType, instanceValue);
			case MT4MTPackage.ABSTRACT_TRANSFORMATION_TEST_CASE:
				return convertAbstractTransformationTestCaseToString(eDataType, instanceValue);
			case MT4MTPackage.IEXTERNAL_ACTION:
				return convertIExternalActionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestSuite createTestSuite() {
		TestSuiteImpl testSuite = new TestSuiteImpl();
		return testSuite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomizableTestCase createCustomizableTestCase() {
		CustomizableTestCaseImpl customizableTestCase = new CustomizableTestCaseImpl();
		return customizableTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetPutTestCase createGetPutTestCase() {
		GetPutTestCaseImpl getPutTestCase = new GetPutTestCaseImpl();
		return getPutTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PutGetTestCase createPutGetTestCase() {
		PutGetTestCaseImpl putGetTestCase = new PutGetTestCaseImpl();
		return putGetTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceAction createSequenceAction() {
		SequenceActionImpl sequenceAction = new SequenceActionImpl();
		return sequenceAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoadModelAction createLoadModelAction() {
		LoadModelActionImpl loadModelAction = new LoadModelActionImpl();
		return loadModelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SaveModelAction createSaveModelAction() {
		SaveModelActionImpl saveModelAction = new SaveModelActionImpl();
		return saveModelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecuteLauncherAction createExecuteLauncherAction() {
		ExecuteLauncherActionImpl executeLauncherAction = new ExecuteLauncherActionImpl();
		return executeLauncherAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluateOCLQueryAction createEvaluateOCLQueryAction() {
		EvaluateOCLQueryActionImpl evaluateOCLQueryAction = new EvaluateOCLQueryActionImpl();
		return evaluateOCLQueryAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyModelAction createCopyModelAction() {
		CopyModelActionImpl copyModelAction = new CopyModelActionImpl();
		return copyModelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompareModelAction createCompareModelAction() {
		CompareModelActionImpl compareModelAction = new CompareModelActionImpl();
		return compareModelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertComparisonResultAction createAssertComparisonResultAction() {
		AssertComparisonResultActionImpl assertComparisonResultAction = new AssertComparisonResultActionImpl();
		return assertComparisonResultAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertOCLInvariantAction createAssertOCLInvariantAction() {
		AssertOCLInvariantActionImpl assertOCLInvariantAction = new AssertOCLInvariantActionImpl();
		return assertOCLInvariantAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAction createUserAction() {
		UserActionImpl userAction = new UserActionImpl();
		return userAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetVariableAction createSetVariableAction() {
		SetVariableActionImpl setVariableAction = new SetVariableActionImpl();
		return setVariableAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpdateModelAction createUpdateModelAction() {
		UpdateModelActionImpl updateModelAction = new UpdateModelActionImpl();
		return updateModelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WrapperTestCase createWrapperTestCase() {
		WrapperTestCaseImpl wrapperTestCase = new WrapperTestCaseImpl();
		return wrapperTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutoExecutorTestSuite createAutoExecutorTestSuite() {
		AutoExecutorTestSuiteImpl autoExecutorTestSuite = new AutoExecutorTestSuiteImpl();
		return autoExecutorTestSuite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClearAction createClearAction() {
		ClearActionImpl clearAction = new ClearActionImpl();
		return clearAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalAction createExternalAction() {
		ExternalActionImpl externalAction = new ExternalActionImpl();
		return externalAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalActionParameter createExternalActionParameter() {
		ExternalActionParameterImpl externalActionParameter = new ExternalActionParameterImpl();
		return externalActionParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiffKind createDiffKindFromString(EDataType eDataType, String initialValue) {
		DiffKind result = DiffKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDiffKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAction createIActionFromString(EDataType eDataType, String initialValue) {
		return (IAction)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIActionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractTransformationTestCase createAbstractTransformationTestCaseFromString(EDataType eDataType, String initialValue) {
		return (AbstractTransformationTestCase)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAbstractTransformationTestCaseToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public edu.ustb.sei.mt4mt.base.updates.IExternalAction createIExternalActionFromString(EDataType eDataType, String initialValue) {
		return (edu.ustb.sei.mt4mt.base.updates.IExternalAction)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIExternalActionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MT4MTPackage getMT4MTPackage() {
		return (MT4MTPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MT4MTPackage getPackage() {
		return MT4MTPackage.eINSTANCE;
	}

} //MT4MTFactoryImpl
