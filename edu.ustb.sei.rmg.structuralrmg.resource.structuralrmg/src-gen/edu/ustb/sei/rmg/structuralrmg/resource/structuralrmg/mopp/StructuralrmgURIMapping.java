/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

/**
 * A basic implementation of the
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgURIMapping
 * interface that can map identifiers to URIs.
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgURIMapping.
 */
public class StructuralrmgURIMapping<ReferenceType> implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgURIMapping<ReferenceType> {
	
	private org.eclipse.emf.common.util.URI uri;
	private String identifier;
	private String warning;
	
	public StructuralrmgURIMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public org.eclipse.emf.common.util.URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
