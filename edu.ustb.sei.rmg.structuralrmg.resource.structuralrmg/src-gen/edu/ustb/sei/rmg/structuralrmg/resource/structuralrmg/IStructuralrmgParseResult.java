/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

/**
 * An interface used to access the result of parsing a document.
 */
public interface IStructuralrmgParseResult {
	
	/**
	 * Returns the root object of the document.
	 */
	public org.eclipse.emf.ecore.EObject getRoot();
	
	/**
	 * Returns a list of commands that must be executed after parsing the document.
	 */
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>> getPostParseCommands();
	
}
