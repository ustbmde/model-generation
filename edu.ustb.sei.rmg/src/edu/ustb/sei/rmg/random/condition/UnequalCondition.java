package edu.ustb.sei.rmg.random.condition;

import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.random.RandomUtil;

@SuppressWarnings("rawtypes")
public class UnequalCondition extends Condition {
	static final int MAX_TRY = 100;
	
	private Condition<?> core;
	private FeaturePathCondition<?> path;
	public UnequalCondition(Condition<?> cond, FeaturePathCondition<?> path, Double p) {
		super();
		core = cond;
		this.path = path;
		this.possibility = p;
	}
	@Override
	public Object getValue(RandomUtil r, Object... extra) {
		// TODO Auto-generated method stub
		if((Boolean)extra[2]==true) {
			throw new ValueLazyGenerationRequest(this, (EObject)extra[0],
					(EAttribute)extra[1]);
		} else {
			if(this.possibility==1.0) {//一定不等
				return getUnequalValue(r, extra);
				
			} else if(this.possibility==0.0) {//一定相等
				return getEqualValue(r, extra);
			} else {
				double p = r.randomDouble();
				if(p<this.possibility) {
					return getUnequalValue(r, extra);
				} else {
					return getEqualValue(r, extra);
				}
			}
		}
	}
	private Object getEqualValue(RandomUtil r, Object... extra) {
		return path.getValue(r, extra);
	}
	private Object getUnequalValue(RandomUtil r, Object... extra) {
		Object pVal = path.getValue(r, extra);
		Object val = null;
		int count = 0;
		do {
			val = core.getValue(r, extra);
			val = Generator.convertToPrimitiveValue(((EAttribute)extra[1]).getEAttributeType(), val);
			count ++;
		}while((val==pVal) || (val!=null && val.equals(pVal)) && count <= MAX_TRY);
//		System.out.println("path value="+pVal+"\t value="+val);
		return val;
	}


}
