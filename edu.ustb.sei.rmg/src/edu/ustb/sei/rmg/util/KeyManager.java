package edu.ustb.sei.rmg.util;

import java.util.HashMap;

public class KeyManager {
	private HashMap<Object,Object> keyMap = new HashMap<Object,Object>();
	
	public Object getKey(Object...objects) {
		if(objects==null || objects.length==0) return null;
		
		int i = 0;
		
		HashMap<Object,Object> map = keyMap;
		for(i=0;i<objects.length-1;i++) {
			Object nMap = map.get(objects[i]);
			if(nMap == null) {
				nMap = new HashMap<Object,Object>();
				map.put(objects[i], nMap);
			}
			map = (HashMap<Object, Object>)nMap;
		}
		
		Object key = map.get(objects[i]);
		if(key==null){
			key = new Object[]{};
			map.put(objects[i],key);
		}
		return key;
	}
}
