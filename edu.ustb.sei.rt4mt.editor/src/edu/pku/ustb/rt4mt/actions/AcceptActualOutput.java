package edu.pku.ustb.rt4mt.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import edu.pku.ustb.rt4mt.ExpectedOutput;
import edu.pku.ustb.rt4mt.OutputParameterConfiguration;
import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.TestData;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;
import edu.pku.ustb.rt4mt.TestRoundGroup;

public class AcceptActualOutput extends Action implements
		ISelectionChangedListener {
	
	protected EditingDomain domain;
	protected ISelectionProvider selectionProvider;
	public ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	public void setSelectionProvider(ISelectionProvider selectionProvider) {
		this.selectionProvider = selectionProvider;
	}

	protected TestExecution testExecution;
	protected RegressionTestModel testModel;
	
	public AcceptActualOutput() {
		super("Accept Actual Output");
		setDescription("Accept actual output. Copy the actual output to the output base.");
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		selectionProvider = event.getSelectionProvider();
	    if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	public boolean updateSelection(IStructuredSelection selection) {
		if(selection.size()==1) {
			Object se = selection.getFirstElement();
			if(se instanceof TestExecution) {
				testExecution = (TestExecution) se;
				testModel = (RegressionTestModel) testExecution.eContainer().eContainer().eContainer();
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		EList<OutputParameterConfiguration> actualOutputs = testModel.getOutput().getParameters();
		ExpectedOutput eo = Rt4mtFactory.eINSTANCE.createExpectedOutput();
		
		
//		String actualOutput = testModel.getOutput();
		String outputBase = testModel.getOutputBase();
		
		
		for(OutputParameterConfiguration o : actualOutputs) {
			String acceptedOutput = null;
			String str = FileUtil.generateTimeString();
			if(outputBase.charAt(outputBase.length()-1)=='/') {
				acceptedOutput = outputBase + "output"+str+".xmi";
			} else {
				acceptedOutput = outputBase + "/output"+str+".xmi";
			}
			
			String actualOutput = o.getModelURI();
			
			URI ifu = URI.createPlatformResourceURI(actualOutput, true);
			URI ofu = URI.createPlatformResourceURI(acceptedOutput, true);
			
			try {
				FileUtil.forChannel(ifu,ofu);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			TestData td = Rt4mtFactory.eINSTANCE.createTestData();
			
			td.setName(o.getName());
			td.setModelURI(acceptedOutput);
			
			eo.getData().add(td);
		}
		
		testExecution.setPassed(true);
		testExecution.getTestCase().getExpectedOutputs().add(eo);
		testExecution.eResource().setModified(true);
	}

	

	
	/**
	 * This returns the action's domain.
	 */
	public EditingDomain getEditingDomain()
	{
		return domain;
	}
	
	/**
	 * This sets the action's domain.
	 */
	public void setEditingDomain(EditingDomain domain)
	{
		this.domain = domain;
	}
	
	public void setActiveWorkbenchPart(IWorkbenchPart workbenchPart)
	{
		setEditingDomain(workbenchPart instanceof IEditingDomainProvider ? ((IEditingDomainProvider)workbenchPart).getEditingDomain() : null);
	}
	
	public void update()
	{
		setEnabled(domain != null);
	}
}
