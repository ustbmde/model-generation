package edu.ustb.sei.egraph.bridge;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;

import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;

public class EMFImporter implements Importer<Resource,EClass, EReference, EAttribute, Void>{
	
	public EMFImporter(TypeFilter<EClass, EReference, EAttribute, ?> filter) {
		super();
		this.filter = filter;
	}

	private TypeFilter<EClass,EReference,EAttribute,?> filter;

	@Override
	public EGraph<EClass,EReference,EAttribute,Void> importGraph(Resource resource,ETypeModel<EClass, EReference, EAttribute, Void> typemodel) {
		EGraph<EClass,EReference,EAttribute,Void> graph = new EGraph<EClass,EReference,EAttribute,Void>();
		
		HashMap<EObject, ENode<EClass,EReference,EAttribute>> map = new HashMap<EObject, ENode<EClass,EReference,EAttribute>>();
		
		TreeIterator<EObject> passOne = resource.getAllContents();
		while(passOne.hasNext()) {
			EObject o = passOne.next();
			EClass c = o.eClass();
			if(filter.shouldLoadNodeOfType(c)) {
				ENode<EClass,EReference,EAttribute> n = new ENode<EClass,EReference,EAttribute>(c);
				for(EAttribute a : c.getEAllAttributes()) {
					if(filter.shouldLoadNodeAttrOfType(a)) {
						Object v = o.eGet(a);
						if(v!=null) {
							if(a.isMany()) {
								n.addAll(a,(Collection<?>)v);
							} else {
								n.put(a, v);
							}
						}
					}
				}
				graph.addNode(n);
				map.put(o, n);
			}
		}
		
		TreeIterator<EObject> passTwo = resource.getAllContents();
		while(passTwo.hasNext()) {
			EObject o = passTwo.next();
			EClass c = o.eClass();
			if(filter.shouldLoadNodeOfType(c)) {
				ENode<EClass,EReference,EAttribute> n = map.get(o);
				for(EReference r : c.getEAllReferences()) {
					if(filter.shouldLoadEdgeOfType(r)) {
						if(r.isMany()) {
							@SuppressWarnings("unchecked")
							List<EObject> targets = (List<EObject>) o.eGet(r);
							if(targets!=null) {
								for(EObject target : targets) {
									ENode<EClass,EReference,EAttribute> nt = map.get(target);
									if(nt!=null) {
										EEdge<EClass,EReference,Void> edge = new EEdge<EClass,EReference,Void>(r,n,nt);
										graph.addEdge(edge);
									}
								}
							}
						} else {
							EObject target = (EObject) o.eGet(r);
							if(target!=null) {
								ENode<EClass,EReference,EAttribute> nt = map.get(target);
								if(nt!=null) {
									EEdge<EClass,EReference,Void> edge = new EEdge<EClass,EReference,Void>(r,n,nt);
									graph.addEdge(edge);
								}
							}
						}
					}
				}
			}
		}
		
		return graph;
	}
	
	
}
