/**
 */
package edu.ustb.sei.mde.mt4mt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assert OCL Invariant Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getModelKey <em>Model Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getInvariant <em>Invariant</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getImportContextVariables <em>Import Context Variables</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertOCLInvariantAction()
 * @model
 * @generated
 */
public interface AssertOCLInvariantAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Model Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Key</em>' attribute.
	 * @see #setModelKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertOCLInvariantAction_ModelKey()
	 * @model
	 * @generated
	 */
	String getModelKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getModelKey <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Key</em>' attribute.
	 * @see #getModelKey()
	 * @generated
	 */
	void setModelKey(String value);

	/**
	 * Returns the value of the '<em><b>Invariant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Invariant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invariant</em>' attribute.
	 * @see #setInvariant(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertOCLInvariantAction_Invariant()
	 * @model
	 * @generated
	 */
	String getInvariant();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction#getInvariant <em>Invariant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Invariant</em>' attribute.
	 * @see #getInvariant()
	 * @generated
	 */
	void setInvariant(String value);

	/**
	 * Returns the value of the '<em><b>Import Context Variables</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Context Variables</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Context Variables</em>' attribute list.
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertOCLInvariantAction_ImportContextVariables()
	 * @model
	 * @generated
	 */
	EList<String> getImportContextVariables();

} // AssertOCLInvariantAction
