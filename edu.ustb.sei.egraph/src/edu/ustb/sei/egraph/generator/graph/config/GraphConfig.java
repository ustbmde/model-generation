package edu.ustb.sei.egraph.generator.graph.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ustb.sei.commonutil.util.Triple;
import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class GraphConfig {
	static public final String SAVE_SCHEMA_LOCATION = "schema_location";
	static public final String SAVE_COMPACT = "compact";
	static public final String SAVE_XMI_ID = "xmi_id";
	
	private Map<String,AbstractRandomFunction<?>> defaultBoundMap = new HashMap<String, AbstractRandomFunction<?>>();
	private Map<Object,Object> saveOptions = new HashMap<Object, Object>();
	private QualifiedName rootType = null;
	private boolean generateContainment = true;
	
	private List<NodeConfig> nodeConfigs = new ArrayList<NodeConfig>();
	private List<AttributeConfig> attributeConfigs = new ArrayList<AttributeConfig>();
	private List<EdgeSetConfig> edgeConfigs = new ArrayList<EdgeSetConfig>();
	
	public List<NodeConfig> getNodeConfigs() {
		return nodeConfigs;
	}
	public List<AttributeConfig> getAttributeConfigs() {
		return attributeConfigs;
	}
	public List<EdgeSetConfig> getEdgeConfigs() {
		return edgeConfigs;
	}
	public QualifiedName getRootType() {
		return rootType;
	}
	public void setRootType(QualifiedName rootType) {
		this.rootType = rootType;
	}
	
	public boolean shouldGenerateContainment() {
		return this.generateContainment;
	}
	
	public void addGlobalOption(Triple<String,String,?> triple) {
		if(triple.getFirst().equals("root")) {
			if(triple.getThird().equals("auto")) {
				this.rootType = null;
				this.generateContainment = true;
			}
			else if(triple.getThird().equals("manual")) {
				this.rootType = null;
				this.generateContainment = false;
			} else {
				this.rootType = new QualifiedName((String)triple.getThird());
				this.generateContainment = true;
			}
		} else if(triple.getFirst().equals("define")) {
			this.defaultBoundMap.put(triple.getSecond(), (AbstractRandomFunction<?>) triple.getThird());
		} else if(triple.getFirst().equals("save")) {
			this.saveOptions.put(triple.getSecond(), triple.getThird());
		}
	}
	public Map<String, AbstractRandomFunction<?>> getDefaultBoundMap() {
		return defaultBoundMap;
	}
	public Map<Object, Object> getSaveOptions() {
		return saveOptions;
	}
}
