/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.Rt4mtPackage;
import edu.pku.ustb.rt4mt.TestRound;
import edu.pku.ustb.rt4mt.TestRoundGroup;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Round Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.TestRoundGroupImpl#getRounds <em>Rounds</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TestRoundGroupImpl extends EObjectImpl implements TestRoundGroup {
	/**
	 * The cached value of the '{@link #getRounds() <em>Rounds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRounds()
	 * @generated
	 * @ordered
	 */
	protected EList<TestRound> rounds;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestRoundGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Rt4mtPackage.Literals.TEST_ROUND_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestRound> getRounds() {
		if (rounds == null) {
			rounds = new EObjectContainmentEList<TestRound>(TestRound.class, this, Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS);
		}
		return rounds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS:
				return ((InternalEList<?>)getRounds()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS:
				return getRounds();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS:
				getRounds().clear();
				getRounds().addAll((Collection<? extends TestRound>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS:
				getRounds().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.TEST_ROUND_GROUP__ROUNDS:
				return rounds != null && !rounds.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TestRoundGroupImpl
