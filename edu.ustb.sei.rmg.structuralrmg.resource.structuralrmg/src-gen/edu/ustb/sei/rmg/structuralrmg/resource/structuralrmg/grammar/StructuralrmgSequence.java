/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public class StructuralrmgSequence extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement {
	
	public StructuralrmgSequence(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgStringUtil.explode(getChildren(), " ");
	}
	
}
