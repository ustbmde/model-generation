package edu.ustb.sei.mt4mt.base.actions;
import java.io.IOException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;

public class SaveModelAction implements IAction {

	public SaveModelAction(String key, String modelURI) {
		super();
		this.key = key;
		this.modelURI = modelURI;
	}

	private String modelURI;
	private String key;
	
	@SuppressWarnings("unchecked")
	@Override
	public void run(Context context) {
		ResourceSet tempSet = context.createResourceSet();
		
		URI uri = URI.createURI(modelURI);
		Resource res = tempSet.createResource(uri);
		
		Object value = context.getData(key);
		if(value!=null) {
			if(value instanceof Resource) {
				res.getContents().addAll(EcoreUtil.copyAll(((Resource) value).getContents()));
			} else if(value instanceof EList) {
				res.getContents().addAll(EcoreUtil.copyAll((EList<? extends EObject>)value));
			} else if(value instanceof EObject) {
				res.getContents().add(EcoreUtil.copy((EObject)value));
			} else {
				throw new InvalidTestException("cannot save to "+modelURI, false);
			}
		}
		try {
			res.save(null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
