package edu.ustb.sei.rmg.config.actions;

import java.util.Calendar;
import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IWorkbenchPart;

import edu.ustb.sei.rmg.ConcurrentGenerator;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.OptionModel;
import edu.ustb.sei.rmg.run.RMGResourceSet;
import edu.ustb.sei.rmg.util.PerformanceMonitor;

public class RepeatedGenerationAction extends Action implements ISelectionChangedListener {
	protected EditingDomain domain;
	protected ISelectionProvider selectionProvider;
	 protected OptionModel configModel;
	 
	 final public static int time = 50;
	
	public RepeatedGenerationAction() {
		super("Repeat "+time+" Generation");
		setDescription("Begin to generate "+time+" models according to the config model");
	}
	
	public RepeatedGenerationAction(EditingDomain domain)
	{
		this();
		this.domain = domain;
		update();
	}
	
	public void run() {
		for(int i=0;i<time;i++) {
			try{
				Generator g = null;			
				if(configModel.isEnableConcurrency())
					g = new ConcurrentGenerator(domain.getResourceSet());
				else g = new Generator(domain.getResourceSet());
				
				g.loadMetamodel(configModel.getMetamodel());
				g.doConfig(configModel);
				
//			Generator g = new RMGResourceSet(EcorePlugin.getWorkspaceRoot().getLocation().toString()).createModelGenerator(configModel.eResource().getURI().toString());
				
				PerformanceMonitor.SINGLETON.begin("RMG");
				g.randomGenerateModel();			
				PerformanceMonitor.SINGLETON.end("RMG");
				
				PerformanceMonitor.SINGLETON.print("RMG", "RMG");
				PerformanceMonitor.SINGLETON.clear("RMG");
				
				System.out.println("now saving files");
				
				g.saveModel(configModel.getOutputFileURI());
			} catch(Exception e) {
				System.out.println("error happened");
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This returns the action's domain.
	 */
	public EditingDomain getEditingDomain()
	{
		return domain;
	}
	
	/**
	 * This sets the action's domain.
	 */
	public void setEditingDomain(EditingDomain domain)
	{
		this.domain = domain;
	}
	
	public void update()
	{
		setEnabled(domain != null);
	}
	
	public void setActiveWorkbenchPart(IWorkbenchPart workbenchPart)
	{
		setEditingDomain(workbenchPart instanceof IEditingDomainProvider ? ((IEditingDomainProvider)workbenchPart).getEditingDomain() : null);
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		selectionProvider = event.getSelectionProvider();
	    if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	public boolean updateSelection(IStructuredSelection selection) {
		//selectedObjects = new ArrayList<EObject>();
		configModel = null;
		for (Iterator<?> objects = selection.iterator(); objects.hasNext(); )
		{
			Object object = AdapterFactoryEditingDomain.unwrap(objects.next());
			if (object instanceof EObject)
			{
				if(((EObject) object).eClass()==ConfigPackage.eINSTANCE.getOptionModel()) {
					configModel = (OptionModel) object;
					break;
				}
			}
			else if (object instanceof Resource)
			{
				try{
					EObject o = ((Resource) object).getContents().get(0);
					if(o.eClass()==ConfigPackage.eINSTANCE.getOptionModel()) {
						configModel = (OptionModel)o;
						break;
					}
				}catch(Exception e) {
				}
			}      
			else
			{
				return false;
			}
		}
		
		return configModel!=null;
	}

}
