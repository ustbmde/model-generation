package edu.ustb.sei.egraph.generator.graph.service;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.stream.Stream;

import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.egraph.generator.graph.config.AttributeConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetGenerationConfig;
import edu.ustb.sei.egraph.generator.graph.config.GraphConfig;
import edu.ustb.sei.egraph.generator.graph.config.NodeConfig;
import edu.ustb.sei.egraph.generator.util.DegreeTable;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;
import edu.ustb.sei.egraph.util.ConcurrentWrite;
import edu.ustb.sei.egraph.util.ConcurrentWriteArrayList;

/**
 * common cache for GraphBasedGenerator
 * @author hexiao
 *
 */
public class GraphGenerationService<NT,ET,NAT,EAT> {
	
	@ConcurrentWrite
	private ConcurrentHashMap<List<ET>, Map<ENode<NT,ET,NAT>,DegreeTable>> degreeTable; 
	
	private RandomService randomService;
	
	@ConcurrentWrite
	private ConcurrentHashMap<NT,List<ENode<NT,ET,NAT>>> allInstancesMap;
	
	@ConcurrentWrite
	private ConcurrentHashMap<NT,List<ENode<NT,ET,NAT>>> objTypeMap;
	
	private EGraph<NT,ET,NAT,EAT> hostGraph;
	
	private ETypeModel<NT,ET,NAT,EAT> typeModel;
	
	@ConcurrentWrite
	private Map<String,List<ENode<NT,ET,NAT>>> namedNodeSets;
	
	private GraphConfigService<NT,ET,NAT,EAT> configService;
	

	@SuppressWarnings({ "rawtypes", "unchecked"})
	public GraphGenerationService(long seed,EGraph<NT,ET,NAT,EAT> hostGraph, GraphConfig config) {
		degreeTable = new ConcurrentHashMap<List<ET>, Map<ENode<NT,ET,NAT>,DegreeTable>>();
		
		randomService = new RandomService(seed);
		
		allInstancesMap = new ConcurrentHashMap<NT, List<ENode<NT,ET,NAT>>>();
		objTypeMap = new ConcurrentHashMap();
		
		this.hostGraph = hostGraph;
		this.typeModel = hostGraph.getTypeModel();

		this.configService = new GraphConfigService(config, getTypeModel());
		
		this.namedNodeSets = new HashMap<String, List<ENode<NT,ET,NAT>>>();
		
		randomService.getDefinedRandomFunction().putAll(config.getDefaultBoundMap());
		
		initialize();
	}
	
	public void initialize() {
		// init allObjMap for every node type with ConcurrentWriteArrayList!
		for(NT type : this.getTypeModel().allNodeTypes()) {
			this.allInstancesMap.put(type, new ConcurrentWriteArrayList<ENode<NT,ET,NAT>>(1024));
		}
		
		this.configService.resolveBound(this.randomService);
	}
	
	public ETypeModel<NT,ET,NAT,EAT> getTypeModel() {
		return this.typeModel;
	}
	
	@SuppressWarnings("unchecked")
	public DegreeTable getDegreeTable(List<ET> refs, ENode<NT,ET,?> node) {
		Map<ENode<NT,ET,NAT>,DegreeTable> map = this.degreeTable.get(refs);
		if(map==null) {
			map = new HashMap<ENode<NT,ET,NAT>,DegreeTable>();
			this.degreeTable.put(refs, map);
		}
		DegreeTable table = map.get(node);
		if(table==null) {
			table = new DegreeTable(refs.size());
			map.put((ENode<NT,ET,NAT>)node, table);
		}
		return table;
	}
	
	public RandomService getRandomService() {
		return randomService;
	}
	
	public List<ENode<NT,ET,NAT>> getAllInstanceOfType(NT t) {
		List<ENode<NT,ET,NAT>> objs = this.allInstancesMap.get(t);
		return objs;
	}
	
	public List<ENode<NT,ET,NAT>> getObjOfType(NT t) {
		List<ENode<NT,ET,NAT>> objs = this.objTypeMap.get(t);
		return objs;
	}
	
	public void addObjToType(NT t, List<ENode<NT,ET,NAT>> nodes) {
		this.objTypeMap.put(t, nodes);
		addToSelfAndParent(t,nodes);
	}
	
	public EGraph<NT,ET,NAT,EAT> getHostGraph() {
		return this.hostGraph;
	}
	
	private void addToAllInstances(NT type, List<ENode<NT,ET,NAT>> nodes) {
		List<ENode<NT,ET,NAT>> allObjs = this.allInstancesMap.get(type);
		if(allObjs!=null) // for the parent types of a referenced type, allObjs may be null
			allObjs.addAll(nodes);
	}
	
	private void addToSelfAndParent(NT type, List<ENode<NT,ET,NAT>> nodes) {
		Collection<NT> superTypes = this.getTypeModel().collectAllSuperTypes(type);
		for(NT t : superTypes) {
			addToAllInstances(t,nodes);
		}
	}

	public NT getRootType() {
		return this.configService.getRootType();
	}
	
	public boolean shouldGenerateContainment() {
		return this.configService.shouldGenerateContainment();
	}

	public int getElementSize(NodeConfig n) {
		Integer bound = this.configService.getBound(n);
		return bound==null ? 0 : bound;
	}
	
	public int getLinkSize(EdgeSetGenerationConfig n) {
		return this.configService.getBound(n);
	}
	
	
	public EdgeSetConfig getContainmentConfig() {
		return this.configService.getContainmentsConfig();
	}
	
	public <T> Stream<T> fetchStream(Collection<T> list) {
		return list.stream();
	}
	public <T> Stream<T> fetchStream(Collection<T> set, boolean parallel) {
		if(parallel) return set.parallelStream();
		else return set.stream();
	}

	public List<Pair<NT,NodeConfig>> getAllNodeConfigs() {
		return configService.getAllNodeConfigs();
	}

	public List<Pair<List<ET>,EdgeSetConfig>> getAllEdgeSetConfigs() {
		return configService.getAllEdgeSetConfigs();
	}

	public List<Pair<NAT,AttributeConfig>> getAttributeConfigsForNodeType(NT type) {
		return configService.getAttributeConfigsForNodeType(type);
	}
	
	public void addNamdedNodeSet(String name, List<ENode<NT,ET,NAT>> nodes) {
		List<ENode<NT,ET,NAT>> list;
		synchronized (this.namedNodeSets) {
			list = this.namedNodeSets.get(name);
			if(list==null) {
				list = new ArrayList<ENode<NT,ET,NAT>>();
				this.namedNodeSets.put(name, list);
			}
		}
		
		synchronized (list) {
			list.addAll(nodes);
		}
	}
	
	public List<ENode<NT,ET,NAT>> getNamedNodeSet(String name) {
		return this.namedNodeSets.get(name);
	}
	
	public void echo(Level level, String msg) {
		System.out.println(level.toString()+"\t"+msg);
	}

	public Map<Object, Object> getSaveOptions() {
		return this.configService.getSaveOptions();
	}

}
