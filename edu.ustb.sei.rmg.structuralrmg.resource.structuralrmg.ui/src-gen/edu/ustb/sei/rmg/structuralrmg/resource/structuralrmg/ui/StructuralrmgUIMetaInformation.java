/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgUIMetaInformation extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgMetaInformation {
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgHoverTextProvider getHoverTextProvider() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgHoverTextProvider();
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgImageProvider getImageProvider() {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgImageProvider.INSTANCE;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgColorManager createColorManager() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStruct
	 * uralrmgTextResource,
	 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgColorManag
	 * er) instead.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgTokenScanner createTokenScanner(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgTokenScanner createTokenScanner(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgColorManager colorManager) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgTokenScanner(resource, colorManager);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCodeCompletionHelper createCodeCompletionHelper() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgCodeCompletionHelper();
	}
	
}
