package edu.ustb.sei.egraph.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class FastArrayList<E> extends ArrayList<E> {
	private static final long serialVersionUID = -7732574261566065292L;
	
	final static int shuffleThreshold = 50;
	private int shuffleTime = 0;
	
	public void doShuffle(int size) {
		if(shuffleTime<size) {
			Collections.shuffle(this); // shuffle will call set() to automatically update the index map
			shuffleTime = shuffleThreshold;
		} else {
			shuffleTime -= size;
		}
	}
	
	public FastArrayList() {
		super();
		valueToId = new HashMap<E,Integer>();
	}

	public FastArrayList(Collection<? extends E> c) {
		super(c);
		valueToId = new HashMap<E,Integer>(c.size());
		for(int i=0;i<size();i++) {
			valueToId.put(get(i), i);
		}
	}

	public FastArrayList(int initialCapacity) {
		super(initialCapacity);
		valueToId = new HashMap<E,Integer>(initialCapacity);
	}

	private Map<E,Integer> valueToId = null;

	@Override
	public E remove(int index) {
		if(index<0 || index>=size()) return null;
		
		if(index == size()-1) {
			E deleted = super.remove(index);
			valueToId.put(deleted,null);
			return deleted;
		} else {
			E moved = this.get(size()-1);
			E deleted = super.set(index, moved);
			valueToId.put(deleted,null);
			super.remove(size()-1);
			valueToId.put(moved, index);
			return deleted;
		}
	}

	@Override
	public boolean remove(Object o) {
		Integer id = valueToId.get(o);
		if(id==null) {
//			if(size()!=0)
//				System.out.println("I didn't remove anything: "+o);
			return false;
		}
		// the removed object may not be identical to o, but they must be semantically equivalent
		return this.remove(id.intValue())!=null;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("removeAll of FastArrayList is unsupported");
	}

	@Override
	public boolean add(E e) {
		valueToId.put(e, size());
		return super.add(e);
	}

	@Override
	public void add(int index, E element) {
		valueToId.put(element,index);
		for(int i = index; i<size();i++) {
			valueToId.put(get(i),i+1);
		}
		super.add(index, element);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("addAll of FastArrayList is unsupported");
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException("removeAll of FastArrayList is unsupported");
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		valueToId.clear();
		super.clear();
	}

	@Override
	public int indexOf(Object o) {
		try{
			return valueToId.get(o);			
		} catch(Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public E set(int index, E element) {
		valueToId.put(element, index);
		return super.set(index, element);
	}
}
