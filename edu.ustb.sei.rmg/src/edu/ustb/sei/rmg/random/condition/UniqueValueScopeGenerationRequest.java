package edu.ustb.sei.rmg.random.condition;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

public class UniqueValueScopeGenerationRequest extends RuntimeException {
	private static final long serialVersionUID = -2718468888274520210L;
	private Condition<?> requester = null;
	private int[] pair = null;
	private String conditionString = null; 
	public String getConditionString() {
		return conditionString;
	}

	public void setConditionString(String conditionString) {
		this.conditionString = conditionString;
	}

	public int[] getPair() {
		return pair;
	}

	public void setPair(int[] pair) {
		this.pair = pair;
	}

	public Condition<?> getRequester() {
		return requester;
	}

	public void setRequester(Condition<?> requester) {
		this.requester = requester;
	}

	public EObject getObject() {
		return object;
	}

	public void setObject(EObject object) {
		this.object = object;
	}

	public EAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(EAttribute attribute) {
		this.attribute = attribute;
	}

	private EObject object = null;
	private EAttribute attribute = null;
	
	public UniqueValueScopeGenerationRequest(Condition<?> requester,
			EObject object, EAttribute attribute,int[] pair,String condition) {
		super();
		this.requester = requester;
		this.object = object;
		this.attribute = attribute;
		this.pair = pair;
		this.conditionString = condition;
	}
}
