/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;

public class BindingMappingTemplateReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.BindingMapping container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<edu.ustb.sei.rmg.structuralrmg.Template> result) {
		if(identifier==null || container==null) return;

		TemplateModel model = ResolverUtil.getModel(container);
		if(model==null) return;

		for(Template t : model.getTemplates()) {
			if(ResolverUtil.matchName(identifier, t.getName())) {
				result.addMapping(identifier, t);
				return;
			}
		}
	}
	
	public String deResolve(edu.ustb.sei.rmg.structuralrmg.Template element, edu.ustb.sei.rmg.structuralrmg.BindingMapping container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
