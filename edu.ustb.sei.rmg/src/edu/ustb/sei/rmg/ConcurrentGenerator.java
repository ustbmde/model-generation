package edu.ustb.sei.rmg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.ReferenceGroupOption;
import edu.ustb.sei.rmg.random.condition.ValueLazyGenerationRequest;
import edu.ustb.sei.rmg.util.HashableBasicEList;
import edu.ustb.sei.rmg.util.HashableConcurrentHashMap;
import edu.ustb.sei.rmg.util.OrderedPairMap;
import edu.ustb.sei.rmg.util.OrderedTripleMap;
import edu.ustb.sei.rmg.util.Pair;

public class ConcurrentGenerator extends Generator {

	public ConcurrentGenerator() {
		super();
		initLocks();
	}

	public ConcurrentGenerator(ResourceSet resSet) {
		super(resSet);
		initLocks();
	}

	@Override
	protected void init() {
		super.init();
	}
	
	private void initLocks() {
		lockMap = new HashableConcurrentHashMap<Object, Lock>();
		lockMap.put(lockMap, new ReentrantLock());
		lockMap.put(allObjMap, new ReentrantLock());
		lockMap.put(objMap, new ReentrantLock());
		lockMap.put(allObjs, new ReentrantLock());
		lockMap.put(containerMap, new ReentrantLock());
		lockMap.put(lazyGenerations, new ReentrantLock());
		lockMap.put(uniqueMap, new ReentrantLock());
	}
	
	private ForkJoinPool pool = new ForkJoinPool();
	private ConcurrentHashMap<Object, Lock> lockMap = null;
	
	private ReferencePartition referencePartition = new ReferencePartition(this.hasCommonSubClass);
	
	@Override
	protected void randomGenerateElements() {
		
		
		List<Callable<Integer>> tasks = new ArrayList<Callable<Integer>>();
		for(EClass c : allClasses) {
			tasks.add(new RandomGenerateElementActioin(this,c));
		}
		pool.invokeAll(tasks);	
	}
	
	@Override
	protected void randomGenerateReferences() {
		List<Callable<Integer>> tasks = new ArrayList<Callable<Integer>>();
		//tasks.add(new RandomGenerateSpecialReferenceAction(this,true));
		
		System.out.println("AAA");
		{
			EList<EList<EReference>> disjoinGroups = referencePartition.splitRefereneGroup(this.allContainmentReferences, true, true, false, false, false, false, true);
			EList<EObject> roots = this.getModelRoot();
			
			
			for(EList<EReference> refs : disjoinGroups) {
				
				System.out.println(refs);
				
				EList<EObject> parents = new UniqueEList<EObject>();
				EList<EObject> children = new UniqueEList<EObject>();
				
				for(EReference ref :refs) {
					EList<EObject> p = allObjMap.get(ref.getEContainingClass());
					if(p!=null) 
						parents.addAll(p);
					EList<EObject> c = allObjMap.get(ref.getEReferenceType());
					if(c!=null) 
						children.addAll(c);
				}
				children.removeAll(roots);
				int size = children.size();
				if(size==0) continue;
				tasks.add(new RandomGenerateReferenceGroupAction(this,refs,parents,children,true,true,false,false,true,false,false,size));
			}
		}
		System.out.println("BBB");
		//tasks.add(new RandomGenerateSpecialReferenceAction(this,false));
		{
			for(Pair<EClass,EList<EReference>> refs : specifiedReferenceGroups) {
				Object[] boundInfo = (Object[]) generationConstraint.get(refs);
				String bound = (String)boundInfo[0];
				boolean sourceUnique = (boolean)boundInfo[1];
				boolean sourceRequired = (boolean)boundInfo[2];
				boolean targetUnique = (boolean)boundInfo[3];
				boolean targetRequired = (boolean)boundInfo[4];
				boolean transitivity = (boolean)boundInfo[5];
				boolean ring = (boolean)boundInfo[6];
				boolean circle = (boolean)boundInfo[7];
				
				ReferenceGroupOption ro = this.getReferenceGroupOption(refs);

				int size = (ro==null || ro.isScalable()) ? random.randomIntWithScale(bound) : random.randomInt(bound);
				
				EList<EObject> parents = new UniqueEList<EObject>();
				EList<EObject> children = null; //new UniqueEList<EObject>();
				
				
				//prepare source
				if(ro.eContainer() instanceof ClassOption) {
					ClassOption opt = (ClassOption) ro.eContainer();
					parents.addAll(allObjMap.get(opt.getClassRef()));
				} else {
					for(EReference ref :refs.second) {
						parents.addAll(allObjMap.get(ref.getEContainingClass()));
					}
				}
				
				//prepare target
				EList<EObject> externalObjects = ro.getExternalObjects();
				if(externalObjects.size()!=0) {
					children = this.filterByRefs(externalObjects,refs.second.toArray(new EReference[refs.second.size()]));
				} else {
					children = new UniqueEList<EObject>();
					for(EReference ref :refs.second) {
//						parents.addAll(allObjMap.get(ref.getEContainingClass()));
						children.addAll(allObjMap.get(ref.getEReferenceType()));
					}
				}
				
//				for(EReference ref :refs) {
//					parents.addAll(allObjMap.get(ref.getEContainingClass()));
//					children.addAll(allObjMap.get(ref.getEReferenceType()));
//				}
				
				children = this.filterByTypes(children,ro.getTypeFilter().toArray(new EClass[ro.getTypeFilter().size()]));

				
				tasks.add(new RandomGenerateReferenceGroupAction(this,refs.second,parents,children,
						sourceUnique,sourceRequired,targetUnique,targetRequired,
						transitivity,ring,circle,size));
			}
		}
		
		
		for(EReference ref : otherReferences) {
			tasks.add(new RandomGenerateOtherReferenceAction(this,ref));
		}
		
		List<Future<Integer>> futures = pool.invokeAll(tasks);
	}

//	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
//		if (tasks == null)	
//			throw new NullPointerException();
//		List<Future<T>> futures = new ArrayList<Future<T>>(tasks.size());
//		boolean done = false;
//		try {	
//			for (Callable<T> t : tasks) {		
//				RunnableFuture<T> f = newTaskFor(t);		
//				futures.add(f);		
//				execute(f);	
//			}	
//			for (Future<T> f : futures) {		
//				if (!f.isDone()) {			
//					try {				
//						f.get();			
//					} catch (CancellationException ignore) {
//									
//					} catch (ExecutionException ignore) {
//									
//					}		
//				}
//			}	
//			done = true;	
//			return futures;
//		} finally {	
//			if (!done)		
//				for (Future<T> f : futures)			
//					f.cancel(true);
//		}
//	}

	private class RandomGenerateOtherReferenceAction implements Callable<Integer> {
		private ConcurrentGenerator g;
		private EReference ref;
		
		
		public RandomGenerateOtherReferenceAction(ConcurrentGenerator g,
				EReference ref) {
			super();
			this.g = g;
			this.ref = ref;
		}


		@Override
		public Integer call() throws Exception {
			try{
				System.out.println("begin generating "+ref);
				g.randomGenerateOtherReference(ref);
				System.out.println("finish generating "+ref);
				return 0;
			}
			catch(Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		
	}
	
	private class RandomGenerateReferenceGroupAction implements Callable<Integer> {
		public RandomGenerateReferenceGroupAction(ConcurrentGenerator g,
				EList<EReference> refs, EList<EObject> parents,
				EList<EObject> children, boolean sourceUnique,
				boolean sourceRequired, boolean targetUnique,
				boolean targetRequired, boolean transitivity, boolean ring,
				boolean circle, int size) {
			super();
			this.g = g;
			this.refs = refs;
			this.parents = parents;
			this.children = children;
			this.sourceUnique = sourceUnique;
			this.sourceRequired = sourceRequired;
			this.targetUnique = targetUnique;
			this.targetRequired = targetRequired;
			this.transitivity = transitivity;
			this.ring = ring;
			this.circle = circle;
			this.size = size;
		}

		private ConcurrentGenerator g;
		private EList<EReference> refs;
		private EList<EObject> parents;
		private EList<EObject> children;
		private boolean sourceUnique;
		private boolean sourceRequired;
		private boolean targetUnique;
		private boolean targetRequired;
		private boolean transitivity;
		private boolean ring;
		private boolean circle;
		private int size;
		
		@Override
		public Integer call() throws Exception {
			try{
				System.out.println("begin generating "+refs);
				ForbiddenMap map = new ForbiddenMap(ring, circle, transitivity);
				randomGenerateReferences(refs, parents, children, sourceRequired, sourceUnique, targetRequired, targetUnique, map, size);
				System.out.println("finish generating "+refs);
				return 0;
			}catch(Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		
	}
	
/*	private class RandomGenerateSpecialReferenceAction implements Callable<Integer> {
		private ConcurrentGenerator g;
		private boolean containment ;
		
		public RandomGenerateSpecialReferenceAction(ConcurrentGenerator g, boolean containment) {
			super();
			this.g = g;
			this.containment = containment;
		}

		@Override
		public Integer call() throws Exception {
			if(containment) {
				g.randomGenerateContainmentReferences();
			}
			else {
				g.randomGenerateReferenceGroups();
			}
			return 0;
		}
		
	}
*/	
	private class RandomGenerateElementActioin implements Callable<Integer> {
		
		private ConcurrentGenerator g;
		private EClass c;		
		
		public RandomGenerateElementActioin(ConcurrentGenerator g, EClass c) {
			this.g = g;
			this.c = c;
		}

		
		@Override
		public Integer call() throws Exception {
			g.randomGenerateElements(c);
			return 0;
		}
		
	}
	
	

	@Override
	protected EList<EReference> getContainerReferenceForClass(EClass t) {

		EList<EReference> ref = null;
		try {
			Lock lock = lockMap.get(containerMap);
			lock.lock();
			ref = containerMap.get(t);
			if(ref==null) {
				ref = new HashableBasicEList<EReference>();
				containerMap.put(t, ref);
				lock.unlock();
				
				for(EReference r : allContainmentReferences) {
					if(r.getEReferenceType().isSuperTypeOf(t)) 
						ref.add(r);
				}
			} else 
				lock.unlock();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ref;
	}

	@Override
	protected void randomGenerateElements(EClass t) {
		
		int containerSize = getContainerReferenceForClass(t).size();

		if(t.isAbstract() || (containerSize == 0 && t!=rootClass && rootClass!=null)) {
			
			Lock lock = lockMap.get(allObjMap);
			lock.lock();

			EList<EObject> objs = allObjMap.get(t);
			if(objs==null) {
				objs = new HashableBasicEList<EObject>();
				lockMap.put(objs, new ReentrantLock());
				allObjMap.put(t, objs);				
			}
			lock.unlock();
			return;
		}
		
		int size = 0;
		
		
		if(containerSize == 0 && (t==rootClass && uniqueRoot))
			size = 1;
		else size = randomObjectSize(t);
		
		System.out.println("to generate "+t+" "+size);
		
		EList<EObject> objs = new HashableBasicEList<EObject>(size);
		int[] pair = new int[]{size,0};
		for(int i = 0;i<size;i++){
			EObject o = EcoreUtil.create(t);
			objs.add(o);
			randomGenerateAttributes(o,pair);
			pair[0]--;
		}
		Lock lock = null;
		
		//System.out.println("to generate XXX");
		
		lock= lockMap.get(objMap);
		lock.lock();
		objMap.put(t, objs);
		lock.unlock();
		
		//System.out.println("to generate YYY");
		
		lock = lockMap.get(allObjs);
		lock.lock();
		allObjs.addAll(objs);
		lock.unlock();
		
		//System.out.println("to generate ZZZ");
		
		addObjToAllObjMap(t, objs);
		addObjToParent(t, objs);
		
		//System.out.println("to generate VVV");
	}
	
	@Override
	protected void addObjToAllObjMap(EClass t, EList<EObject> objs) {
		Lock lock = lockMap.get(allObjMap);
		lock.lock();
		EList<EObject> cObjs = allObjMap.get(t);
		if(cObjs==null) {
			cObjs = new HashableBasicEList<EObject>();
			allObjMap.put(t, cObjs);
			lockMap.put(cObjs, new ReentrantLock());
		}
		lock.unlock();
		
		lock = lockMap.get(cObjs);
		lock.lock();
		cObjs.addAll(objs);
//		System.out.println("in "+t.getName()+" size = "+cObjs.size());
		lock.unlock();
	}

	@Override
	protected void addLazyGeneration(EObject o, EAttribute a,
			RuntimeException e) {
		Lock lock = lockMap.get(lazyGenerations);
		lock.lock();
		super.addLazyGeneration(o, a, e);
		lock.unlock();
	}
	
	
//	@Override
//	public void addToCacheList(EObject o, EReference r, Object v) {
//		List<Object> list = getShadowList(o,r);
//		if(list==null) {
//			synchronized (duplicate) {
//				list = getShadowList(o, r);
//				if(list==null) {
//					list = new ArrayList<Object>();
//					duplicate.put(o, r, list);			
//				}
//			}
//		}
//		synchronized (list) {
//			list.add(v);
//		}
//	}
//	
//	@Override
//	public void removeFromCacheList(EObject o, EReference r, Object v) {
//		List<Object> list = getShadowList(o,r);
//		if(list==null) {
//			return;	
//		}
//		synchronized(list) {
//			list.remove(v);
//		}
//	}
	
//	public void writeBackShadowList() {
//		duplicate.map.entrySet().parallelStream().forEach(new Consumer<Entry<EObject, ConcurrentHashMap<EReference, List<Object>>>>() {
//			@Override
//			public void accept(Entry<EObject, ConcurrentHashMap<EReference, List<Object>>> t) {
//				for(Entry<EReference, List<Object>> l : t.getValue().entrySet()) {
//					t.getKey().eSet(l.getKey(), l.getValue());
//				}
//			}
//		});
//	}
	//the following code has been moved to ReferencePartition
//	protected boolean hasCommonElements(EClass ca, EClass cb) {
//		if(ca==cb) return true;
//		Boolean bv = hasCommonSubClass.get(ca,cb);
//		if(bv!=null) return bv;
//		else return false;
//	}
//
//	private OrderedTripleMap<EList<EReference>, EReference, EReference,Boolean> circleMap = new OrderedTripleMap<EList<EReference>, EReference, EReference,Boolean>();
//	protected boolean hasCircle(EList<EReference> refs, EReference ra, EReference rb, boolean transitive){
//		Boolean bv = circleMap.get(refs, ra, rb);
//		
//		if(bv == null) {
//			if(transitive){
//				bv = isReachable(refs,ra.getEReferenceType(),rb.getEContainingClass()) 
//						&& isReachable(refs,rb.getEReferenceType(),ra.getEContainingClass());
//			} else {
//				bv = hasCommonElements(ra.getEReferenceType(),rb.getEContainingClass())
//						&&hasCommonElements(rb.getEReferenceType(),ra.getEContainingClass());
//			}
//			circleMap.put(refs, ra, rb, bv);
//		}
//		
//		return bv;
//	}
//	
//	
//	private OrderedTripleMap<EList<EReference>, EClass, EClass,Boolean> reachableMap = new OrderedTripleMap<EList<EReference>, EClass, EClass,Boolean>();
//	protected boolean isReachable(EList<EReference> refs, EClass ca, EClass cb){
//		List<EClass> visited = new ArrayList<EClass>();
//		return isReachable(refs,ca,cb,visited);
//	}
//	protected boolean isReachable(EList<EReference> refs, EClass ca, EClass cb, List<EClass> visited) {
//		if(visited.contains(ca)) return false;
//		visited.add(ca);
//		
//		Boolean bv = reachableMap.get(refs, ca, cb);
//		if(bv==null) {
//			bv = isReachableCalc(refs, ca, cb, visited);
//			reachableMap.put(refs, ca, cb, bv);
//		}
//		return bv;
//	}
//	
//	protected boolean isReachableCalc(EList<EReference> refs, EClass ca, EClass cb, List<EClass> visited) {
//		if(hasCommonElements(ca,cb)) return true;
//		for(EReference r : refs){
//			if(hasCommonElements(ca,r.getEContainingClass())&&isReachable(refs,r.getEReferenceType(),cb, visited))
//				return true;
//		}
//		return false;
//	}
//	
//	
//	private OrderedTripleMap<EList<EReference>, EReference, EReference,Boolean> dependentMap = new OrderedTripleMap<EList<EReference>, EReference, EReference,Boolean>();
//	protected boolean isDependent(EList<EReference> refs,EReference ra, EReference rb,
//			boolean sourceUnique, boolean sourceRequired,
//			boolean targetUnique, boolean targetRequired, 
//			boolean ringAllowed, boolean circleAllowed, boolean transitive){
//			Boolean bv = dependentMap.get(refs, ra, rb);
//			if(bv==null) {
//				bv = isDependentCalc(refs,ra,rb,sourceUnique,sourceRequired,targetUnique,targetRequired,ringAllowed,circleAllowed,transitive);
//				dependentMap.put(refs, ra, rb, bv);
//			}
//			return bv;
//		}
//	protected boolean isDependentCalc(EList<EReference> refs,EReference ra, EReference rb,
//			boolean sourceUnique, boolean sourceRequired,
//			boolean targetUnique, boolean targetRequired, 
//			boolean ringAllowed, boolean circleAllowed, boolean transitive){
//		if(sourceUnique || sourceRequired) {
//			if(hasCommonElements(ra.getEReferenceType(),rb.getEReferenceType()))
//				return true;
//		}
//		if(targetUnique || targetRequired) {
//			if(hasCommonElements(ra.getEContainingClass(),rb.getEContainingClass()))
//				return true;
//		}
//				
//		if(!circleAllowed) {
//			if(hasCircle(refs,ra,rb,transitive))
//				return true;
//		}
//		
//		return false;
//	}
//	
//	protected boolean isDependent(EList<EReference> allRefs, EList<EReference> rsa, EList<EReference> rsb,
//			boolean sourceUnique, boolean sourceRequired,
//			boolean targetUnique, boolean targetRequired, 
//			boolean ringAllowed, boolean circleAllowed, boolean transitive) {
//		for(EReference ra : rsa) {
//			for(EReference rb : rsb) {
//				if(isDependent(allRefs,ra,rb,sourceUnique,sourceRequired,targetUnique,targetRequired,ringAllowed,circleAllowed,transitive))
//					return true;
//			}
//		}
//		return false;
//	}
//	
//	protected EList<EList<EReference>> splitRefereneGroup(EList<EReference> refs,
//			boolean sourceUnique, boolean sourceRequired,
//			boolean targetUnique, boolean targetRequired, 
//			boolean ringAllowed, boolean circleAllowed, boolean transitive) {
//		EList<EList<EReference>> list = new HashableBasicEList<EList<EReference>>();
//		for(EReference r : refs) {
//			HashableBasicEList<EReference> sl = new HashableBasicEList<EReference>();
//			sl.add(r);
//			list.add(sl);
//		}
//		
//		int currentBase = 0;
//		while(true) {
//			if(currentBase==list.size())
//				break;
//			int currentID = 0;
//			while(true){
//				if(currentID==currentBase) {
//					currentID++;
//					continue;
//				}
//				if(currentID==list.size()) break;
//				
//				EList<EReference> rsa = list.get(currentBase);
//				EList<EReference> rsb = list.get(currentID);
//				
//				if(isDependent(refs, rsa, rsb, sourceUnique, sourceRequired, targetUnique, targetRequired, ringAllowed, circleAllowed, transitive)) {
//					rsa.addAll(rsb);
//					list.remove(rsb);
//					
//					if(currentBase>currentID) currentBase--;
//				} else 
//					currentID ++;
//			}
//			currentBase++;
//		}
//		
//		return list;
//	}
	
	
}