/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;

public class EndpointBindingEdgeReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.EndpointBinding, edu.ustb.sei.rmg.structuralrmg.TemplateEdge> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.EndpointBinding, edu.ustb.sei.rmg.structuralrmg.TemplateEdge> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.EndpointBinding, edu.ustb.sei.rmg.structuralrmg.TemplateEdge>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.EndpointBinding container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateEdge> result) {
		if(container==null || identifier==null) return;
		Template t = ResolverUtil.upToTemplate(container);
		if(t==null) return;
		
		for(TemplateEdge e : t.getAllEdges()) {
			if(ResolverUtil.matchName(identifier, e.getName())) {
				result.addMapping(identifier, e);
				return;
			}
		}
	}
	
	public String deResolve(edu.ustb.sei.rmg.structuralrmg.TemplateEdge element, edu.ustb.sei.rmg.structuralrmg.EndpointBinding container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
