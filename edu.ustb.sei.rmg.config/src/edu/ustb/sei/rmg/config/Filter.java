/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getFilter()
 * @model abstract="true"
 * @generated
 */
public interface Filter extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean filter(EObject obj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model listMany="true"
	 * @generated
	 */
	EList<EObject> filter(EList<EObject> list);

} // Filter
