package edu.ustb.sei.mt4mt.base.test;

import edu.ustb.sei.mt4mt.base.IAction;
import static edu.ustb.sei.mt4mt.base.Actions.*;

public class TestGetPut extends AbstractBXTransformationTestCase {

	public TestGetPut(String[] inputData, String fwdName, String[] fwdSource, String[] fwdView,
			String bwdName, String[] bwdSource, String[] bwdView, String[] bwdUpdatedSource, long defaultWaitingtime) {
		super(inputData, fwdName, fwdSource, fwdView, bwdName, bwdSource, bwdView, bwdUpdatedSource);
		
		
		IAction action = sequence(
				copy(inputData, fwdSource),
				launch(fwdName,defaultWaitingtime),
				copy(fwdSource,bwdSource),
				copy(fwdView,bwdView),
				launch(bwdName,defaultWaitingtime),
				compareAndAssert(fwdSource,bwdUpdatedSource,"#GetPut")
				);
		
		this.action = action;
	}

}
