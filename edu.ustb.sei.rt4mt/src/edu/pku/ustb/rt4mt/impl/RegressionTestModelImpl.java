/**
 */
package edu.pku.ustb.rt4mt.impl;

import edu.pku.ustb.rt4mt.InputParameterConfigurationGroup;
import edu.pku.ustb.rt4mt.OutputParameterConfigurationGroup;
import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtPackage;
import edu.pku.ustb.rt4mt.TestCaseGroup;
import edu.pku.ustb.rt4mt.TestRoundGroup;
import edu.pku.ustb.rt4mt.TestCase;
import edu.pku.ustb.rt4mt.TestRound;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regression Test Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getInputBase <em>Input Base</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getOutputBase <em>Output Base</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getRunConfigurationName <em>Run Configuration Name</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getInput <em>Input</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getOutput <em>Output</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getTestCaseGroup <em>Test Case Group</em>}</li>
 *   <li>{@link edu.pku.ustb.rt4mt.impl.RegressionTestModelImpl#getTestRoundGroup <em>Test Round Group</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RegressionTestModelImpl extends EObjectImpl implements RegressionTestModel {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getInputBase() <em>Input Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputBase()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_BASE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInputBase() <em>Input Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputBase()
	 * @generated
	 * @ordered
	 */
	protected String inputBase = INPUT_BASE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutputBase() <em>Output Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputBase()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_BASE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutputBase() <em>Output Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputBase()
	 * @generated
	 * @ordered
	 */
	protected String outputBase = OUTPUT_BASE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRunConfigurationName() <em>Run Configuration Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunConfigurationName()
	 * @generated
	 * @ordered
	 */
	protected static final String RUN_CONFIGURATION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRunConfigurationName() <em>Run Configuration Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunConfigurationName()
	 * @generated
	 * @ordered
	 */
	protected String runConfigurationName = RUN_CONFIGURATION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInput() <em>Input</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput()
	 * @generated
	 * @ordered
	 */
	protected InputParameterConfigurationGroup input;

	/**
	 * The cached value of the '{@link #getOutput() <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput()
	 * @generated
	 * @ordered
	 */
	protected OutputParameterConfigurationGroup output;

	/**
	 * The cached value of the '{@link #getTestCaseGroup() <em>Test Case Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestCaseGroup()
	 * @generated
	 * @ordered
	 */
	protected TestCaseGroup testCaseGroup;

	/**
	 * The cached value of the '{@link #getTestRoundGroup() <em>Test Round Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestRoundGroup()
	 * @generated
	 * @ordered
	 */
	protected TestRoundGroup testRoundGroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RegressionTestModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Rt4mtPackage.Literals.REGRESSION_TEST_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputParameterConfigurationGroup getInput() {
		return input;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInput(InputParameterConfigurationGroup newInput, NotificationChain msgs) {
		InputParameterConfigurationGroup oldInput = input;
		input = newInput;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT, oldInput, newInput);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput(InputParameterConfigurationGroup newInput) {
		if (newInput != input) {
			NotificationChain msgs = null;
			if (input != null)
				msgs = ((InternalEObject)input).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT, null, msgs);
			if (newInput != null)
				msgs = ((InternalEObject)newInput).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT, null, msgs);
			msgs = basicSetInput(newInput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT, newInput, newInput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputParameterConfigurationGroup getOutput() {
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutput(OutputParameterConfigurationGroup newOutput, NotificationChain msgs) {
		OutputParameterConfigurationGroup oldOutput = output;
		output = newOutput;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT, oldOutput, newOutput);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput(OutputParameterConfigurationGroup newOutput) {
		if (newOutput != output) {
			NotificationChain msgs = null;
			if (output != null)
				msgs = ((InternalEObject)output).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT, null, msgs);
			if (newOutput != null)
				msgs = ((InternalEObject)newOutput).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT, null, msgs);
			msgs = basicSetOutput(newOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT, newOutput, newOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInputBase() {
		return inputBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputBase(String newInputBase) {
		String oldInputBase = inputBase;
		inputBase = newInputBase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE, oldInputBase, inputBase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutputBase() {
		return outputBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputBase(String newOutputBase) {
		String oldOutputBase = outputBase;
		outputBase = newOutputBase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE, oldOutputBase, outputBase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRunConfigurationName() {
		return runConfigurationName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunConfigurationName(String newRunConfigurationName) {
		String oldRunConfigurationName = runConfigurationName;
		runConfigurationName = newRunConfigurationName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME, oldRunConfigurationName, runConfigurationName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseGroup getTestCaseGroup() {
		return testCaseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestCaseGroup(TestCaseGroup newTestCaseGroup, NotificationChain msgs) {
		TestCaseGroup oldTestCaseGroup = testCaseGroup;
		testCaseGroup = newTestCaseGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP, oldTestCaseGroup, newTestCaseGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestCaseGroup(TestCaseGroup newTestCaseGroup) {
		if (newTestCaseGroup != testCaseGroup) {
			NotificationChain msgs = null;
			if (testCaseGroup != null)
				msgs = ((InternalEObject)testCaseGroup).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP, null, msgs);
			if (newTestCaseGroup != null)
				msgs = ((InternalEObject)newTestCaseGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP, null, msgs);
			msgs = basicSetTestCaseGroup(newTestCaseGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP, newTestCaseGroup, newTestCaseGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestRoundGroup getTestRoundGroup() {
		return testRoundGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTestRoundGroup(TestRoundGroup newTestRoundGroup, NotificationChain msgs) {
		TestRoundGroup oldTestRoundGroup = testRoundGroup;
		testRoundGroup = newTestRoundGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP, oldTestRoundGroup, newTestRoundGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestRoundGroup(TestRoundGroup newTestRoundGroup) {
		if (newTestRoundGroup != testRoundGroup) {
			NotificationChain msgs = null;
			if (testRoundGroup != null)
				msgs = ((InternalEObject)testRoundGroup).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP, null, msgs);
			if (newTestRoundGroup != null)
				msgs = ((InternalEObject)newTestRoundGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP, null, msgs);
			msgs = basicSetTestRoundGroup(newTestRoundGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP, newTestRoundGroup, newTestRoundGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
				return basicSetInput(null, msgs);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
				return basicSetOutput(null, msgs);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
				return basicSetTestCaseGroup(null, msgs);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				return basicSetTestRoundGroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__NAME:
				return getName();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE:
				return getInputBase();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE:
				return getOutputBase();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME:
				return getRunConfigurationName();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
				return getInput();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
				return getOutput();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
				return getTestCaseGroup();
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				return getTestRoundGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__NAME:
				setName((String)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE:
				setInputBase((String)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE:
				setOutputBase((String)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME:
				setRunConfigurationName((String)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
				setInput((InputParameterConfigurationGroup)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
				setOutput((OutputParameterConfigurationGroup)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
				setTestCaseGroup((TestCaseGroup)newValue);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				setTestRoundGroup((TestRoundGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE:
				setInputBase(INPUT_BASE_EDEFAULT);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE:
				setOutputBase(OUTPUT_BASE_EDEFAULT);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME:
				setRunConfigurationName(RUN_CONFIGURATION_NAME_EDEFAULT);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
				setInput((InputParameterConfigurationGroup)null);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
				setOutput((OutputParameterConfigurationGroup)null);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
				setTestCaseGroup((TestCaseGroup)null);
				return;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				setTestRoundGroup((TestRoundGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Rt4mtPackage.REGRESSION_TEST_MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT_BASE:
				return INPUT_BASE_EDEFAULT == null ? inputBase != null : !INPUT_BASE_EDEFAULT.equals(inputBase);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT_BASE:
				return OUTPUT_BASE_EDEFAULT == null ? outputBase != null : !OUTPUT_BASE_EDEFAULT.equals(outputBase);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__RUN_CONFIGURATION_NAME:
				return RUN_CONFIGURATION_NAME_EDEFAULT == null ? runConfigurationName != null : !RUN_CONFIGURATION_NAME_EDEFAULT.equals(runConfigurationName);
			case Rt4mtPackage.REGRESSION_TEST_MODEL__INPUT:
				return input != null;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__OUTPUT:
				return output != null;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_CASE_GROUP:
				return testCaseGroup != null;
			case Rt4mtPackage.REGRESSION_TEST_MODEL__TEST_ROUND_GROUP:
				return testRoundGroup != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", inputBase: ");
		result.append(inputBase);
		result.append(", outputBase: ");
		result.append(outputBase);
		result.append(", runConfigurationName: ");
		result.append(runConfigurationName);
		result.append(')');
		return result.toString();
	}

} //RegressionTestModelImpl
