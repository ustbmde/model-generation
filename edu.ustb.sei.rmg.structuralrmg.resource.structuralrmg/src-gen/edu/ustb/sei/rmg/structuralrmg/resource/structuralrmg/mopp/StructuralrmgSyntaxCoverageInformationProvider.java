/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getRole(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBound(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(),
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(),
		};
	}
	
}
