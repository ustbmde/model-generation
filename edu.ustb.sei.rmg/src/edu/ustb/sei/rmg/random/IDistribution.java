package edu.ustb.sei.rmg.random;

public interface IDistribution {
	/**
	 * @return 返回一个符合特定分布的随机数
	 */
	double sample(Object... arguments);
	
}
