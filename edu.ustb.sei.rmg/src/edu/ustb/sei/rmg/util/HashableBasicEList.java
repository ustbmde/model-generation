package edu.ustb.sei.rmg.util;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;

public class HashableBasicEList<E> extends BasicEList<E> {
	private Object hashKey = new Object[]{};
	
	public HashableBasicEList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HashableBasicEList(Collection<? extends E> collection) {
		super(collection);
		// TODO Auto-generated constructor stub
	}

	public HashableBasicEList(int initialCapacity) {
		super(initialCapacity);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		return hashKey.hashCode();
	}
}
