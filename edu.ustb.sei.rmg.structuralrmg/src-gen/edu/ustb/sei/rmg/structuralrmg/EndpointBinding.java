/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Endpoint Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge <em>Edge</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getSourceMappings <em>Source Mappings</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getTargetMappings <em>Target Mappings</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getEndpointBinding()
 * @model
 * @generated
 */
public interface EndpointBinding extends EObject {
	/**
	 * Returns the value of the '<em><b>Edge</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge</em>' container reference.
	 * @see #setEdge(TemplateEdge)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getEndpointBinding_Edge()
	 * @see edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding
	 * @model opposite="binding" required="true" transient="false"
	 * @generated
	 */
	TemplateEdge getEdge();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge <em>Edge</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge</em>' container reference.
	 * @see #getEdge()
	 * @generated
	 */
	void setEdge(TemplateEdge value);

	/**
	 * Returns the value of the '<em><b>Source Mappings</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.BindingMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Mappings</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getEndpointBinding_SourceMappings()
	 * @model containment="true"
	 * @generated
	 */
	EList<BindingMapping> getSourceMappings();

	/**
	 * Returns the value of the '<em><b>Target Mappings</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.BindingMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Mappings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Mappings</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getEndpointBinding_TargetMappings()
	 * @model containment="true"
	 * @generated
	 */
	EList<BindingMapping> getTargetMappings();

} // EndpointBinding
