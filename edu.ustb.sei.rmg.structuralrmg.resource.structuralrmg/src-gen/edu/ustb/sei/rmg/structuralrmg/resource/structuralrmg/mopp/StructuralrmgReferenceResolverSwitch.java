/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgReferenceResolverSwitch implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateModelPackagesReferenceResolver templateModelPackagesReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateModelPackagesReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateSuperTemplateReferenceResolver templateSuperTemplateReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateSuperTemplateReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateNodeTypeReferenceResolver primitiveTemplateNodeTypeReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateNodeTypeReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateNodeRoleReferenceResolver templateNodeRoleReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateNodeRoleReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.AttributeBindingAttributeReferenceResolver attributeBindingAttributeReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.AttributeBindingAttributeReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.NestedTemplateNodeTemplatesReferenceResolver nestedTemplateNodeTemplatesReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.NestedTemplateNodeTemplatesReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateEdgeReferenceReferenceResolver templateEdgeReferenceReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.TemplateEdgeReferenceReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateEdgeSourceReferenceResolver primitiveTemplateEdgeSourceReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateEdgeSourceReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateEdgeTargetReferenceResolver primitiveTemplateEdgeTargetReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.PrimitiveTemplateEdgeTargetReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.ChainTemplateEdgeNodeReferenceResolver chainTemplateEdgeNodeReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.ChainTemplateEdgeNodeReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.BindingMappingTemplateReferenceResolver bindingMappingTemplateReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.BindingMappingTemplateReferenceResolver();
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.BindingMappingRolesReferenceResolver bindingMappingRolesReferenceResolver = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.BindingMappingRolesReferenceResolver();
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage> getTemplateModelPackagesReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel_Packages(), templateModelPackagesReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.Template, edu.ustb.sei.rmg.structuralrmg.Template> getTemplateSuperTemplateReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate_SuperTemplate(), templateSuperTemplateReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass> getPrimitiveTemplateNodeTypeReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode_Type(), primitiveTemplateNodeTypeReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.TemplateNode, edu.ustb.sei.rmg.structuralrmg.Role> getTemplateNodeRoleReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateNode_Role(), templateNodeRoleReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute> getAttributeBindingAttributeReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding_Attribute(), attributeBindingAttributeReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode, edu.ustb.sei.rmg.structuralrmg.Template> getNestedTemplateNodeTemplatesReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode_Templates(), nestedTemplateNodeTemplatesReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference> getTemplateEdgeReferenceReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateEdge_Reference(), templateEdgeReferenceReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode> getPrimitiveTemplateEdgeSourceReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge_Source(), primitiveTemplateEdgeSourceReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode> getPrimitiveTemplateEdgeTargetReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge_Target(), primitiveTemplateEdgeTargetReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode> getChainTemplateEdgeNodeReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge_Node(), chainTemplateEdgeNodeReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template> getBindingMappingTemplateReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping_Template(), bindingMappingTemplateReferenceResolver);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role> getBindingMappingRolesReferenceResolver() {
		return getResolverChain(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping_Roles(), bindingMappingRolesReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		templateModelPackagesReferenceResolver.setOptions(options);
		templateSuperTemplateReferenceResolver.setOptions(options);
		primitiveTemplateNodeTypeReferenceResolver.setOptions(options);
		templateNodeRoleReferenceResolver.setOptions(options);
		attributeBindingAttributeReferenceResolver.setOptions(options);
		nestedTemplateNodeTemplatesReferenceResolver.setOptions(options);
		templateEdgeReferenceReferenceResolver.setOptions(options);
		primitiveTemplateEdgeSourceReferenceResolver.setOptions(options);
		primitiveTemplateEdgeTargetReferenceResolver.setOptions(options);
		chainTemplateEdgeNodeReferenceResolver.setOptions(options);
		bindingMappingTemplateReferenceResolver.setOptions(options);
		bindingMappingRolesReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EPackage> frr = new StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EPackage>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("packages")) {
				templateModelPackagesReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.TemplateModel) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("superTemplate")) {
				templateSuperTemplateReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.Template) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EClass> frr = new StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EClass>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("type")) {
				primitiveTemplateNodeTypeReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateNode().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Role> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Role>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("role")) {
				templateNodeRoleReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.TemplateNode) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EAttribute> frr = new StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EAttribute>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("attribute")) {
				attributeBindingAttributeReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.AttributeBinding) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("templates")) {
				nestedTemplateNodeTemplatesReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateEdge().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EReference> frr = new StructuralrmgFuzzyResolveResult<org.eclipse.emf.ecore.EReference>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("reference")) {
				templateEdgeReferenceReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.TemplateEdge) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("source")) {
				primitiveTemplateEdgeSourceReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("target")) {
				primitiveTemplateEdgeTargetReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.TemplateNode>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("node")) {
				chainTemplateEdgeNodeReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Template>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("template")) {
				bindingMappingTemplateReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.BindingMapping) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping().isInstance(container)) {
			StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Role> frr = new StructuralrmgFuzzyResolveResult<edu.ustb.sei.rmg.structuralrmg.Role>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("roles")) {
				bindingMappingRolesReferenceResolver.resolve(identifier, (edu.ustb.sei.rmg.structuralrmg.BindingMapping) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel_Packages()) {
			return getResolverChain(reference, templateModelPackagesReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate_SuperTemplate()) {
			return getResolverChain(reference, templateSuperTemplateReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode_Type()) {
			return getResolverChain(reference, primitiveTemplateNodeTypeReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateNode_Role()) {
			return getResolverChain(reference, templateNodeRoleReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getAttributeBinding_Attribute()) {
			return getResolverChain(reference, attributeBindingAttributeReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getNestedTemplateNode_Templates()) {
			return getResolverChain(reference, nestedTemplateNodeTemplatesReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateEdge_Reference()) {
			return getResolverChain(reference, templateEdgeReferenceReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge_Source()) {
			return getResolverChain(reference, primitiveTemplateEdgeSourceReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge_Target()) {
			return getResolverChain(reference, primitiveTemplateEdgeTargetReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge_Node()) {
			return getResolverChain(reference, chainTemplateEdgeNodeReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping_Template()) {
			return getResolverChain(reference, bindingMappingTemplateReferenceResolver);
		}
		if (reference == edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getBindingMapping_Roles()) {
			return getResolverChain(reference, bindingMappingRolesReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgRuntimeUtil().logWarning("Found value with invalid type for option " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver) {
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver replacingResolver = (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver) resolverValue;
			if (replacingResolver instanceof edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceCache) {
					edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver nextResolver = (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver) next;
					if (nextResolver instanceof edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgRuntimeUtil().logWarning("Found value with invalid type in value map for option " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgRuntimeUtil().logWarning("Found value with invalid type in value map for option " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
