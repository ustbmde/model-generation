/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceOption#getReferenceRef <em>Reference Ref</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceOption()
 * @model
 * @generated
 */
public interface ReferenceOption extends AbstractReferenceOption {
	/**
	 * Returns the value of the '<em><b>Reference Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Ref</em>' reference.
	 * @see #setReferenceRef(EReference)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceOption_ReferenceRef()
	 * @model
	 * @generated
	 */
	EReference getReferenceRef();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ReferenceOption#getReferenceRef <em>Reference Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Ref</em>' reference.
	 * @see #getReferenceRef()
	 * @generated
	 */
	void setReferenceRef(EReference value);

} // ReferenceOption
