package edu.ustb.sei.mt4mt.base.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;
import edu.ustb.sei.mt4mt.base.updates.IUpdate;
import edu.ustb.sei.mt4mt.base.updates.RandomUtil;

public class UpdateModelAction implements IAction {
	private String modelKey;
	private RandomUtil util;
	public UpdateModelAction(String modelKey, IUpdate[] updates) {
		super();
		this.modelKey = modelKey;
		this.updates = updates;
		this.createdObjects = new HashSet<EObject>();
		util = new RandomUtil();
	}

	private IUpdate[] updates;

	@SuppressWarnings("unchecked")
	@Override
	public void run(Context context) {
		Object value = context.getData(modelKey);
		
		if(value==null) {
			throw new InvalidTestException("cannot update a null model", false);
		} else {
			List<? extends EObject> list = null;
			if(value instanceof Resource) {
				list = ((Resource) value).getContents();
			} else if(value instanceof List) {
				list = (List<? extends EObject>) value;
			} else if(value instanceof EObject) {
				list = Collections.singletonList((EObject)value);
			}
			
			boolean modified = apply(list);
			
			if(modified==false) {
				throw new InvalidTestException("the model "+modelKey+" was not changed",true);
			}
		}
	}
	
	private boolean apply(List<? extends EObject> list) {
		boolean modified = false;
		
		for(IUpdate u : updates) {
			for(EObject o : list) {
				modified = apply(o,u) || modified;
			}
		}
		
		return modified;
	}
	
	private boolean apply(EObject o, IUpdate update) {
		boolean root = o.eContainer()==null;
		
		boolean modified = false;
		EClass oClass = o.eClass();
		EPackage oPackage = oClass.getEPackage();

//		for(IUpdate u : updates) {
		modified = update.apply(o, oClass, oPackage, util, createdObjects) || modified;
//		}
		
		if(!root && o.eContainer()==null)
			return modified;
		else {
			List<EObject> array = new ArrayList<EObject>(o.eContents());
			for(EObject c : array) {
				modified = apply(c, update) || modified;
			}
			return modified;
		}
	}
	
	private Set<EObject> createdObjects;

}
