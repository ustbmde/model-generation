/**
 */
package edu.pku.ustb.rt4mt;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.pku.ustb.rt4mt.Rt4mtPackage
 * @generated
 */
public interface Rt4mtFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Rt4mtFactory eINSTANCE = edu.pku.ustb.rt4mt.impl.Rt4mtFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Regression Test Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Regression Test Model</em>'.
	 * @generated
	 */
	RegressionTestModel createRegressionTestModel();

	/**
	 * Returns a new object of class '<em>Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case</em>'.
	 * @generated
	 */
	TestCase createTestCase();

	/**
	 * Returns a new object of class '<em>Test Round</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Round</em>'.
	 * @generated
	 */
	TestRound createTestRound();

	/**
	 * Returns a new object of class '<em>Test Execution</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Execution</em>'.
	 * @generated
	 */
	TestExecution createTestExecution();

	/**
	 * Returns a new object of class '<em>Test Case Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case Group</em>'.
	 * @generated
	 */
	TestCaseGroup createTestCaseGroup();

	/**
	 * Returns a new object of class '<em>Test Round Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Round Group</em>'.
	 * @generated
	 */
	TestRoundGroup createTestRoundGroup();

	/**
	 * Returns a new object of class '<em>Input Parameter Configuration Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Input Parameter Configuration Group</em>'.
	 * @generated
	 */
	InputParameterConfigurationGroup createInputParameterConfigurationGroup();

	/**
	 * Returns a new object of class '<em>Output Parameter Configuration Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Output Parameter Configuration Group</em>'.
	 * @generated
	 */
	OutputParameterConfigurationGroup createOutputParameterConfigurationGroup();

	/**
	 * Returns a new object of class '<em>Input Parameter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Input Parameter Configuration</em>'.
	 * @generated
	 */
	InputParameterConfiguration createInputParameterConfiguration();

	/**
	 * Returns a new object of class '<em>Output Parameter Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Output Parameter Configuration</em>'.
	 * @generated
	 */
	OutputParameterConfiguration createOutputParameterConfiguration();

	/**
	 * Returns a new object of class '<em>Test Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Data</em>'.
	 * @generated
	 */
	TestData createTestData();

	/**
	 * Returns a new object of class '<em>Expected Output</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expected Output</em>'.
	 * @generated
	 */
	ExpectedOutput createExpectedOutput();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Rt4mtPackage getRt4mtPackage();

} //Rt4mtFactory
