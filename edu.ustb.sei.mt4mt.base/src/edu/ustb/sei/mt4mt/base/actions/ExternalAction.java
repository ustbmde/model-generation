package edu.ustb.sei.mt4mt.base.actions;

import java.util.Map;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.exceptions.InvalidTestException;
import edu.ustb.sei.mt4mt.base.updates.IExternalAction;

public class ExternalAction implements IAction {
	
	public ExternalAction(Map<String, String> parameters, String result, IExternalAction action) {
		super();
		this.parameters = parameters;
		this.action = action;
		this.result = result;
	}

	private Map<String,String> parameters;
	private IExternalAction action;
	private String result;

	@Override
	public void run(Context context) {
		try {
		action.execute(parameters, result, context);
		} catch(InvalidTestException e) {
			throw e;
		} catch(Exception ee) {
			throw new InvalidTestException(ee.toString(),false);
		}
	}

}
