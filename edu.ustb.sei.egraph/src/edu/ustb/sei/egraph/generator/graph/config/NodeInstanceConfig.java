package edu.ustb.sei.egraph.generator.graph.config;

import java.util.ArrayList;
import java.util.List;

import edu.ustb.sei.egraph.generator.value.AbstractRandomFunction;

public class NodeInstanceConfig{
	public NodeInstanceConfig(String uri) {
		super();
		this.uri = uri;
	}
	
	public NodeInstanceConfig() {
		super();
		this.uri = null;
	}
	
	private List<AttributeInstanceConfig> attributes = new ArrayList<AttributeInstanceConfig>();

	private String uri;
	
	public boolean isExternal() {
		return this.uri!=null;
	}
	
	public String getURI() {
		return this.uri;
	}
	
	public void addPropertyFunction(AttributeInstanceConfig ac) {
		this.attributes.add(ac);
	}
	
	@SuppressWarnings("unchecked")
	public <T> AbstractRandomFunction<T> getPropertyFunction(String name) {
		for(AttributeInstanceConfig ac : this.attributes) {
			if(ac.getName().equals(name))
				return (AbstractRandomFunction<T>)ac.getFunction();
		}
		return null;
	}

	public List<AttributeInstanceConfig> getAttributes() {
		return attributes;
	}
	
	
}
