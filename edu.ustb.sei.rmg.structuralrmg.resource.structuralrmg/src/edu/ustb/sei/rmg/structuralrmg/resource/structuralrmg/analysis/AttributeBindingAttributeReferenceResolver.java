/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;

public class AttributeBindingAttributeReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.AttributeBinding container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<org.eclipse.emf.ecore.EAttribute> result) {
//		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
		if(identifier ==null || container==null)
			return;
		
		PrimitiveTemplateNode tn = (PrimitiveTemplateNode) container.eContainer();
		
		if(tn==null || tn.getType() == null) return;
		
		for(EAttribute attr : tn.getType().getEAllAttributes()) {
			if(ResolverUtil.matchName(identifier, attr.getName())) {
				result.addMapping(identifier, attr);
				return;
			}
		}
	}
	
	public String deResolve(org.eclipse.emf.ecore.EAttribute element, edu.ustb.sei.rmg.structuralrmg.AttributeBinding container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
