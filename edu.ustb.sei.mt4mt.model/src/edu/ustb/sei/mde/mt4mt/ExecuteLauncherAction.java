/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execute Launcher Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getLauncher <em>Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getMaxWait <em>Max Wait</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getExecuteLauncherAction()
 * @model
 * @generated
 */
public interface ExecuteLauncherAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Launcher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Launcher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Launcher</em>' attribute.
	 * @see #setLauncher(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getExecuteLauncherAction_Launcher()
	 * @model
	 * @generated
	 */
	String getLauncher();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getLauncher <em>Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Launcher</em>' attribute.
	 * @see #getLauncher()
	 * @generated
	 */
	void setLauncher(String value);

	/**
	 * Returns the value of the '<em><b>Max Wait</b></em>' attribute.
	 * The default value is <code>"10000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Wait</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Wait</em>' attribute.
	 * @see #setMaxWait(long)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getExecuteLauncherAction_MaxWait()
	 * @model default="10000"
	 * @generated
	 */
	long getMaxWait();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction#getMaxWait <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Wait</em>' attribute.
	 * @see #getMaxWait()
	 * @generated
	 */
	void setMaxWait(long value);

} // ExecuteLauncherAction
