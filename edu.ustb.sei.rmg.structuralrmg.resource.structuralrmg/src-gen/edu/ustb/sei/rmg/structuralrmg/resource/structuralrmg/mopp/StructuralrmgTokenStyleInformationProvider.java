/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("ML_COMMENT".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x40, 0x80, 0x60}, null, false, false, false, false);
		}
		if ("model".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("import".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("abstract".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("template".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("extends".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("node".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("edge".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("set".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("plays".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("shared".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("reversed".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("in".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("loop".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("for".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("source".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("target".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
