/**
 */
package edu.ustb.sei.rmg.structuralrmg.impl;

import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.impl.TemplateImpl#getSuperTemplate <em>Super Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateImpl extends NamedElementImpl implements Template {
	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> roles;

	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateNode> nodes;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateEdge> edges;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuperTemplate() <em>Super Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperTemplate()
	 * @generated
	 * @ordered
	 */
	protected Template superTemplate;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructuralRmgPackage.Literals.TEMPLATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentEList<Role>(Role.class, this, StructuralRmgPackage.TEMPLATE__ROLES);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateNode> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentEList<TemplateNode>(TemplateNode.class, this, StructuralRmgPackage.TEMPLATE__NODES);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateEdge> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentEList<TemplateEdge>(TemplateEdge.class, this, StructuralRmgPackage.TEMPLATE__EDGES);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.TEMPLATE__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getSuperTemplate() {
		if (superTemplate != null && superTemplate.eIsProxy()) {
			InternalEObject oldSuperTemplate = (InternalEObject)superTemplate;
			superTemplate = (Template)eResolveProxy(oldSuperTemplate);
			if (superTemplate != oldSuperTemplate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE, oldSuperTemplate, superTemplate));
			}
		}
		return superTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template basicGetSuperTemplate() {
		return superTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperTemplate(Template newSuperTemplate) {
		Template oldSuperTemplate = superTemplate;
		superTemplate = newSuperTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE, oldSuperTemplate, superTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Role> getAllRoles() {
		EList<Role> allRoles = new BasicEList<Role>();
		
		if(this.getSuperTemplate()!=null) {
			allRoles.addAll(this.getSuperTemplate().getAllRoles());
		}
		allRoles.addAll(this.getRoles());
		return allRoles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TemplateNode> getAllNodes() {
		EList<TemplateNode> allNodes = new BasicEList<TemplateNode>();
		
		if(this.getSuperTemplate()!=null) {
			allNodes.addAll(this.getSuperTemplate().getAllNodes());
		}
		allNodes.addAll(this.getNodes());
		return allNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TemplateEdge> getAllEdges() {
		EList<TemplateEdge> allEdges = new BasicEList<TemplateEdge>();
		 
		if(this.getSuperTemplate()!=null) {
			allEdges.addAll(this.getSuperTemplate().getAllEdges());
		}
		allEdges.addAll(this.getEdges());
		return allEdges;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSuperOf(Template t) {
		if(t==this) return true;
		else if(t.getSuperTemplate()!=null)
			return this.isSuperOf(t.getSuperTemplate());
		else return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructuralRmgPackage.TEMPLATE__ROLES:
				return ((InternalEList<?>)getRoles()).basicRemove(otherEnd, msgs);
			case StructuralRmgPackage.TEMPLATE__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case StructuralRmgPackage.TEMPLATE__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructuralRmgPackage.TEMPLATE__ROLES:
				return getRoles();
			case StructuralRmgPackage.TEMPLATE__NODES:
				return getNodes();
			case StructuralRmgPackage.TEMPLATE__EDGES:
				return getEdges();
			case StructuralRmgPackage.TEMPLATE__ABSTRACT:
				return isAbstract();
			case StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE:
				if (resolve) return getSuperTemplate();
				return basicGetSuperTemplate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructuralRmgPackage.TEMPLATE__ROLES:
				getRoles().clear();
				getRoles().addAll((Collection<? extends Role>)newValue);
				return;
			case StructuralRmgPackage.TEMPLATE__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends TemplateNode>)newValue);
				return;
			case StructuralRmgPackage.TEMPLATE__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends TemplateEdge>)newValue);
				return;
			case StructuralRmgPackage.TEMPLATE__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE:
				setSuperTemplate((Template)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.TEMPLATE__ROLES:
				getRoles().clear();
				return;
			case StructuralRmgPackage.TEMPLATE__NODES:
				getNodes().clear();
				return;
			case StructuralRmgPackage.TEMPLATE__EDGES:
				getEdges().clear();
				return;
			case StructuralRmgPackage.TEMPLATE__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE:
				setSuperTemplate((Template)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructuralRmgPackage.TEMPLATE__ROLES:
				return roles != null && !roles.isEmpty();
			case StructuralRmgPackage.TEMPLATE__NODES:
				return nodes != null && !nodes.isEmpty();
			case StructuralRmgPackage.TEMPLATE__EDGES:
				return edges != null && !edges.isEmpty();
			case StructuralRmgPackage.TEMPLATE__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE:
				return superTemplate != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StructuralRmgPackage.TEMPLATE___GET_ALL_ROLES:
				return getAllRoles();
			case StructuralRmgPackage.TEMPLATE___GET_ALL_NODES:
				return getAllNodes();
			case StructuralRmgPackage.TEMPLATE___GET_ALL_EDGES:
				return getAllEdges();
			case StructuralRmgPackage.TEMPLATE___IS_SUPER_OF__TEMPLATE:
				return isSuperOf((Template)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (abstract: ");
		result.append(abstract_);
		result.append(')');
		return result.toString();
	}

} //TemplateImpl
