package edu.ustb.sei.egraph.generator.value;

import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomFunction<T> extends AbstractRandomFunction<T> {
	
	public RandomFunction(RandomValueFunction<T> function) {
		super();
		this.function = function;
	}

	private RandomValueFunction<T> function;

	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		return function;
	}
	
	@Override
	public Set<String> providedScopes() {
		return function.providedScopes();
	}
	
	@Override
	public Set<String> requiredScopes() {
		return function.requiredScopes();
	}
	
	
}
