/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assert Comparison Result Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDiffKey <em>Diff Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDifferentCount <em>Different Count</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertComparisonResultAction()
 * @model
 * @generated
 */
public interface AssertComparisonResultAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Diff Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diff Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diff Key</em>' attribute.
	 * @see #setDiffKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertComparisonResultAction_DiffKey()
	 * @model
	 * @generated
	 */
	String getDiffKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDiffKey <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diff Key</em>' attribute.
	 * @see #getDiffKey()
	 * @generated
	 */
	void setDiffKey(String value);

	/**
	 * Returns the value of the '<em><b>Different Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Different Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Different Count</em>' attribute.
	 * @see #setDifferentCount(int)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertComparisonResultAction_DifferentCount()
	 * @model
	 * @generated
	 */
	int getDifferentCount();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getDifferentCount <em>Different Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Different Count</em>' attribute.
	 * @see #getDifferentCount()
	 * @generated
	 */
	void setDifferentCount(int value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.mt4mt.DiffKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.mt4mt.DiffKind
	 * @see #setKind(DiffKind)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getAssertComparisonResultAction_Kind()
	 * @model
	 * @generated
	 */
	DiffKind getKind();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.mt4mt.DiffKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(DiffKind value);

} // AssertComparisonResultAction
