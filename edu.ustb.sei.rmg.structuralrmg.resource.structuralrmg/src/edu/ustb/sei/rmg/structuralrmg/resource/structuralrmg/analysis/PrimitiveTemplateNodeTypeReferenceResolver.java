/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis;

import org.eclipse.emf.ecore.EClass;

import edu.ustb.sei.rmg.structuralrmg.TemplateModel;

public class PrimitiveTemplateNodeTypeReferenceResolver implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolver<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass> {
	
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass> delegate = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultResolverDelegate<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass>();
	
	public void resolve(String identifier, edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolveResult<org.eclipse.emf.ecore.EClass> result) {
		if(identifier ==null || container==null)
			return;
		
		TemplateModel m = ResolverUtil.getModel(container);
		
		if(m==null) return;
		
		EClass cls = ResolverUtil.searchClass(identifier,m.getPackages());
		
		if(cls!=null)
			result.addMapping(identifier, cls);
	}
	
	public String deResolve(org.eclipse.emf.ecore.EClass element, edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
