package edu.ustb.sei.egraph.generator.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Stream;

import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.commonutil.util.PairHashMap;
import edu.ustb.sei.commonutil.util.Triple;
import edu.ustb.sei.egraph.generator.graph.config.AttributeConfig;
import edu.ustb.sei.egraph.generator.graph.config.AttributeInstanceConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeInstanceConfig;
import edu.ustb.sei.egraph.generator.graph.config.EdgeSetConfig;
import edu.ustb.sei.egraph.generator.graph.config.Filter;
import edu.ustb.sei.egraph.generator.graph.config.NodeConfig;
import edu.ustb.sei.egraph.generator.graph.config.NodeInstanceConfig;
import edu.ustb.sei.egraph.generator.graph.config.NodeInstanceSetConfig;
import edu.ustb.sei.egraph.generator.graph.service.CriticalPathCollectingAlgorithmType;
import edu.ustb.sei.egraph.generator.graph.service.GraphGenerationService;
import edu.ustb.sei.egraph.generator.graph.service.NodeSelectionService;
import edu.ustb.sei.egraph.generator.graph.service.RandomService;
import edu.ustb.sei.egraph.generator.graph.service.ReachabilityService;
import edu.ustb.sei.egraph.generator.graph.service.SourceNodeSelectionService;
import edu.ustb.sei.egraph.generator.graph.service.TargetNodeSelectionService;
import edu.ustb.sei.egraph.generator.graph.service.NodeSelectionService.Scope;
import edu.ustb.sei.egraph.generator.graph.service.PathFindingAlgorithmType;
import edu.ustb.sei.egraph.generator.util.DegreeTable;
import edu.ustb.sei.egraph.generator.util.QualifiedName;
import edu.ustb.sei.egraph.generator.value.RandomValueFunction;
import edu.ustb.sei.egraph.generator.value.SpecialFunction;
import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;
import edu.ustb.sei.egraph.structure.ETypeModel;
import edu.ustb.sei.egraph.util.ConcurrentArrayList;
import edu.ustb.sei.egraph.util.ConcurrentWrite;
import edu.ustb.sei.egraph.util.ConcurrentWriteArrayList;


/**
 * GraphGenerator is a revised version of our random model generator.
 * The goal of this version is not high generation efficiency, but a clear implementation of the generation algorithm and correctness.
 * I hope this version is more readable and less buggy than the old one. 
 * However, at present, this version is nether faster nor more reliable than the old version.
 * @author hexiao
 *
 * @param <NT>
 * @param <ET>
 * @param <NAT>
 * @param <EAT>
 */
public class GraphGenerator<NT,ET,NAT,EAT> {
	
	public GraphGenerator(GraphGenerationService<NT,ET,NAT,EAT> s) {
		this.generationService = s;
		this.typeModel = s.getTypeModel();
		this.hostGraph = s.getHostGraph();
		
		this.roots = new ConcurrentWriteArrayList<ENode<NT,ET,NAT>>(256);
		
		this.pendingAttributes = new ConcurrentArrayList<Triple<ENode<NT,ET,NAT>,NAT,RandomValueFunction<?>>>(256);
		this.generatedScopes = new HashSet<String>();
	}
	
	private GraphGenerationService<NT,ET,NAT,EAT> generationService;
	private ETypeModel<NT,ET,NAT,EAT> typeModel;
	private EGraph<NT,ET,NAT,EAT> hostGraph;
	
	@ConcurrentWrite(read=false)
	private List<ENode<NT,ET,NAT>> roots;
	
	@ConcurrentWrite(read=false)
	private List<Triple<ENode<NT,ET,NAT>,NAT,RandomValueFunction<?>>> pendingAttributes;
	
	@ConcurrentWrite(read=false)
	private Set<String> generatedScopes;
	
	public void randomGenerateModel() {

		randomGenerateElements();

		pickRootObject();

		randomGenerateReferences();

//		generateLazyAttributes();
		

	}
	
	protected void pickRootObject() {
		NT rootType = this.generationService.getRootType();
		if(rootType!=null) {
			this.generationService.echo(Level.INFO, "Root type is specified. Pick one root.");
			List<ENode<NT,ET,NAT>> allInstances = this.generationService.getObjOfType(rootType);
			ENode<NT,ET,NAT> root = allInstances.get(this.generationService.getRandomService().randomInt(allInstances.size()));
			this.roots.add(root);
		}
		this.roots = this.removeDuplicate(this.roots);
		this.generationService.echo(Level.INFO, this.roots.size()+" root elements were selected.");
	}

	protected void randomGenerateElements(){
		this.generationService.echo(Level.INFO, "Generating nodes");
		
		List<Pair<NT, NodeConfig>> nodeConfigs = this.generationService.getAllNodeConfigs();
		
		this.fetchStream(nodeConfigs).forEach(pair->{
			randomGenerateElements(pair.getFirst(),pair.getSecond());
			});
		
		generatePendingAttributes();
		this.generationService.echo(Level.INFO, "Finish generating nodes");
	}
	
	protected void generatePendingAttributes() {
		List<Triple<ENode<NT,ET,NAT>,NAT,RandomValueFunction<?>>> current = new ArrayList<Triple<ENode<NT,ET,NAT>,NAT,RandomValueFunction<?>>>();
		Set<String> finished = new HashSet<String>();
		Set<String> mayContribute = new HashSet<String>();
		
		
		//1. check finished
		this.pendingAttributes.forEach(t->{
			mayContribute.addAll(t.getThird().providedScopes());
		});
		this.generatedScopes.forEach(s->{
			if(mayContribute.contains(s)==false)
				finished.add(s);
		});
		
		//2. prepare for pending
		this.generatedScopes.clear();
		mayContribute.clear();
		
		//3. pending
		int lastSize = this.pendingAttributes.size();
		while(lastSize>0) {
			current.clear();
			current.addAll(this.pendingAttributes);
			this.pendingAttributes.clear();
			
			current.forEach(t->{
				Set<String> provided = t.getThird().providedScopes();
				Set<String> required = t.getThird().requiredScopes();
				
				if(finished.containsAll(required)) {
					Object value = t.getThird().getValue(this.generationService.getRandomService());
					t.getFirst().put(t.getSecond(), value);
					this.generatedScopes.addAll(provided);
				} else {
					this.pendingAttributes.add(t);
					mayContribute.addAll(t.getThird().providedScopes());
				}
			});
			
			this.generatedScopes.forEach((s)->{
				if(mayContribute.contains(s)==false)
					finished.add(s);
			});
			mayContribute.clear();
			
			if(lastSize == this.pendingAttributes.size()) {
				this.generationService.echo(Level.SEVERE, "Circle dependencies were detected when generating pending attributes");
				throw new RuntimeException("Circle dependencies");
			}
			else
				lastSize = this.pendingAttributes.size();
		}
		
	}

	protected void randomGenerateElements(NT t, NodeConfig config){
		int containerSize = this.typeModel.getContainerReferenceForClass(t).size();
		
		
		if(this.typeModel.isAbstract(t)) {
			return;
		}
		
		// for exceptional cases, the size must be specified
		int size = this.generationService.getElementSize(config);
		
		
		this.generationService.echo(Level.INFO, "Generating "+size+" nodes of "+config.getType().name);

		List<ENode<NT,ET,NAT>> createdNodes = new ArrayList<ENode<NT,ET,NAT>>(size+32);
		
		if(size!=0) {
			List<Pair<NAT,AttributeConfig>> attributes = this.generationService.getAttributeConfigsForNodeType(t);
			
			for(int i = 0;i<size;i++){
				ENode<NT,ET,NAT> o = this.typeModel.createNode(t);
				createdNodes.add(o);
				this.hostGraph.addNode(o);
				this.fetchStream(attributes).forEach((a)->{
					randomGenerateAttributes(o, a.getFirst(), a.getSecond());
				});
			}
		}
		
		RandomService randomService = this.generationService.getRandomService();
		// generate node instances
		int instanceSize = 0;
		for(NodeInstanceSetConfig nisc : config.getInstanceSetConfig()) {
			String setName = nisc.getName();
			List<ENode<NT,ET,NAT>> nodeSet = new ArrayList<ENode<NT,ET,NAT>>();
			for(NodeInstanceConfig nic : nisc.getInstanceConfig()) {
				if(nic.isExternal()) {
					ENode<NT, ET, NAT> n = this.typeModel.createExternalNode(t,nic.getURI());
					nodeSet.add(n);
					this.hostGraph.addNode(n);
				} else {
					ENode<NT, ET, NAT> n = this.typeModel.createNode(t);
					for(AttributeInstanceConfig aic : nic.getAttributes()) {
						NAT att = this.typeModel.resolveNodeAttribute(new QualifiedName(aic.getName(), config.getType().name));
						if(att==null) {
							this.generationService.echo(Level.WARNING, "Cannot resolve attribute "+aic.getName()+". Skipped");
						} else {
							Object value = aic.getFunction().getValue(randomService).getValue(randomService);
							n.put(att, value);
						}
					}
					nodeSet.add(n);
					createdNodes.add(n);
					this.hostGraph.addNode(n);
					instanceSize++;
				}
			}
			createdNodes.addAll(nodeSet);
			this.generationService.addNamdedNodeSet(setName, nodeSet);
		}
		
		if(instanceSize!=0)
			this.generationService.echo(Level.INFO, "Generating "+instanceSize+" node instances of "+config.getType().name);
		
		this.generationService.addObjToType(t, createdNodes);
		
		if(this.generationService.shouldGenerateContainment()) {
			if(containerSize == 0) {
				if(this.generationService.getRootType()==null) {
					this.generationService.echo(Level.INFO, "Root type is not specified. Root elements were detected.");
				} else {
					this.generationService.echo(Level.INFO, "Root type is specified. But root elements were detected.");
				}
				this.roots.addAll(createdNodes);
			}
		}
	}
	
	protected void randomGenerateAttributes(ENode<NT, ET, NAT> o, NAT attr, AttributeConfig config) {
		RandomValueFunction<?> function = config.getFunction().getValue(this.generationService.getRandomService());
		Set<String> provided = function.providedScopes();
		Set<String> required = function.requiredScopes();
		
		if(function==SpecialFunction.emptyFunction) return; // stop when encountering empty function
		
		if(required.isEmpty()) {
			Object value = function.getValue(this.generationService.getRandomService());
			o.put(attr, value);
			synchronized (this.generatedScopes) {
				this.generatedScopes.addAll(provided);
			}
		} else {
			synchronized (this.pendingAttributes) {
				this.pendingAttributes.add(new Triple<ENode<NT,ET,NAT>, NAT, RandomValueFunction<?>>(o, attr, function));
			}
		}
	}

	protected void randomGenerateReferences() {
		randomGenerateContainmentReferences();
		randomGenerateReferenceGroups();
	}
	
	protected void randomGenerateReferenceGroups() {
		List<Pair<List<ET>, EdgeSetConfig>> edgeSets = this.generationService.getAllEdgeSetConfigs();
		this.fetchStream(edgeSets).forEach(pair->{
			this.randomGenerateReferences(pair.getFirst(), pair.getSecond());
		});
	}

	public void randomGenerateContainmentReferences() {
		if(this.generationService.shouldGenerateContainment()==false) {
			this.generationService.echo(Level.INFO, "Auto containment edge generation is turned off. Containment edges must be created manually!");
			return;
		}
		
		this.generationService.echo(Level.INFO, "Generating containment edges");
		
		if(roots.isEmpty()) {
			this.generationService.echo(Level.SEVERE, "No root was selected! Cannot generate containment edges automatically!");
			throw new RuntimeException("No root was selected! Cannot generate containment edges automatically!");
		}

		List<ET> allContainments = this.typeModel.collectAllContainmentReferences();
		EdgeSetConfig constraint = this.generationService.getContainmentConfig();
		
		ReachabilityService<NT, ET> reachability = new ReachabilityService<NT, ET>(this.hostGraph,allContainments,true,false,true);
		
		reachability.setPathFindingAlgorithm(PathFindingAlgorithmType.tarToSrc);
		reachability.setCriticalPathFindingAlgorithm(CriticalPathCollectingAlgorithmType.tarToSrc);
		
		List<ENode<NT,ET,NAT>> parents = new ArrayList<ENode<NT,ET,NAT>>(this.hostGraph.getNodes());
		List<ENode<NT,ET,NAT>> children = new ArrayList<ENode<NT,ET,NAT>>(this.hostGraph.getNodes());
		
		
		int size = 0;
		children.removeAll(roots);
		size = children.size();
		
		List<ET> availableReferences = reduceReferences(allContainments);
		this.refineConstraints(availableReferences, constraint, reachability);
		
		randomGenerateReferences(availableReferences, parents, children, constraint, reachability, size);
	}

	private List<ET> reduceReferences(List<ET> allContainments) {
		List<ET> availableReferences = new ArrayList<ET>();
		for(ET ref : allContainments) {
			// may be optimized by using selector.getAllAvailable
			List<ENode<NT,ET,NAT>> par = this.generationService.getAllInstanceOfType(this.typeModel.getSource(ref));
			if(par==null || par.size()==0) continue;
			// may be optimized
			List<ENode<NT,ET,NAT>> chd = this.generationService.getAllInstanceOfType(this.typeModel.getTarget(ref));
			if(chd==null || chd.size()==0) continue;
			availableReferences.add(ref);
		}
		return availableReferences;
	}
	
	protected void randomGenerateReferences(List<ET> references, EdgeSetConfig config) {
		this.generationService.echo(Level.INFO, "Generating edge set: "+references);
		
		ReachabilityService<NT, ET> reachability = new ReachabilityService<NT, ET>(this.hostGraph,references,config.isIdem(), config.isRefl(), config.isOrd());
		List<ET> availableReferences = reduceReferences(references);
		this.refineConstraints(availableReferences, config, reachability);
		
		if(config.isSourceUnique() 
				||(availableReferences.size()==1 && this.typeModel.getSourceUpperBound(availableReferences.get(0))==1)) {
			reachability.setPathFindingAlgorithm(PathFindingAlgorithmType.tarToSrc);
			reachability.setCriticalPathFindingAlgorithm(CriticalPathCollectingAlgorithmType.tarToSrc);
		}
		
		
		List<ENode<NT,ET,NAT>> defaultSources = new ArrayList<ENode<NT,ET,NAT>>();
		List<ENode<NT,ET,NAT>> defaultTargets = new ArrayList<ENode<NT,ET,NAT>>();
		
		
		for(ET ref : references) {
			NT st = this.typeModel.getSource(ref);
			NT tt = this.typeModel.getTarget(ref);
			
			defaultSources.addAll(this.generationService.getAllInstanceOfType(st));
			defaultTargets.addAll(this.generationService.getAllInstanceOfType(tt));
		}
		
		List<ENode<NT,ET,NAT>> uniqueDefaultSources = removeDuplicate(defaultSources);
		List<ENode<NT,ET,NAT>> uniqueDefaultTargets = removeDuplicate(defaultTargets);
		
		// cannot be parallelized
		config.getGenerationConfigs().forEach(gen->{
			List<ENode<NT,ET,NAT>> sources = null;
			List<ENode<NT,ET,NAT>> targets = null;
			
			if(gen.getSource()==null)
				sources = uniqueDefaultSources;
			else {
				sources = applyFilter(gen.getSource(),uniqueDefaultSources);
			}
			
			if(gen.getTarget()==null)
				targets = uniqueDefaultTargets;
			else {
				targets = applyFilter(gen.getTarget(),uniqueDefaultTargets);
			}
			
			int size = this.generationService.getLinkSize(gen);		
			randomGenerateReferences(availableReferences, sources,targets,config,reachability,size);
		});
		
		if(config.getInstanceConfigs().isEmpty()==false) {
			this.generationService.echo(Level.INFO, "Generating edge instances");
			// instance edges
			for(EdgeInstanceConfig c : config.getInstanceConfigs()) {
				QualifiedName type = c.getType();
				ET edgeType = this.typeModel.resolveEdgeType(type);
				if(edgeType==null || references.contains(edgeType)==false) {
					this.generationService.echo(Level.INFO, "Cannot resolve the edge type or the type is out of the scope: "+type+". Skipped");
					continue;
				}
				
				List<ENode<NT, ET, NAT>> source = this.generationService.getNamedNodeSet(c.getSource());
				int sourceID = c.getSourceId();
				List<ENode<NT, ET, NAT>> target = this.generationService.getNamedNodeSet(c.getTarget());
				int targetID = c.getTargetId();
				
				List<ENode<NT, ET, NAT>> actualSource;
				List<ENode<NT, ET, NAT>> actualTarget;
				
				if(source==null) actualSource = Collections.emptyList();
				else if(source.isEmpty() || sourceID==-1) actualSource = source;
				else actualSource = Collections.singletonList(source.get(sourceID)); 
				
				if(target==null) actualTarget = Collections.emptyList();
				else if(target.isEmpty() || targetID==-1) actualTarget = target;
				else actualTarget = Collections.singletonList(target.get(targetID));
				
				
				for(ENode<NT,ET,NAT> s : actualSource) {
					for(ENode<NT,ET,NAT> t : actualTarget) {
						EEdge<NT, ET, EAT> edge = this.typeModel.createEdge(edgeType, s, t);
						this.hostGraph.addEdge(edge);
					}
				}
			}
		}
		this.generationService.echo(Level.INFO, "Finish Generating edge set: "+references);
	}

	protected ArrayList<ENode<NT, ET, NAT>> removeDuplicate(List<ENode<NT, ET, NAT>> sources) {
		return this.fetchStream(sources).collect(
				()->new ArrayList<ENode<NT,ET,NAT>>(),
				(list,o)->{
					if(list.contains(o)==false) 
						list.add(o);
				},
				(l,r)->{
					l.addAll(r);
				});
	}

	// FIXME: 
	private List<ENode<NT, ET, NAT>> applyFilter(Filter filter, List<ENode<NT, ET, NAT>> allScope) {
		this.generationService.echo(Level.WARNING, "Filters are not implemented yet.");
		return allScope;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void randomGenerateReferences(List<ET> availableReferences, List<ENode<NT, ET, NAT>> parents,
			List<ENode<NT, ET, NAT>> children, EdgeSetConfig config, ReachabilityService<NT, ET> reachability,
			int size) {
		
		NodeSelectionService<NT, ET> srcSelector = new SourceNodeSelectionService<NT, ET>(generationService,config, (List)parents, availableReferences);
		NodeSelectionService<NT, ET> tarSelector = new TargetNodeSelectionService<NT, ET>(generationService,config, (List)children, availableReferences);
		
		// try simple case for min
		/*
		 * Simple case: the major goal is to avoid checking ordering
		 *   preconditions for src->tar
		 *     1. should not be target unique (we need a variant to handle this case)
		 *       1.1 not target unique: iterate firstLevel available
		 *       1.2 target unique: check and iterate firstLevel available
		 *     2. if target required, there is at least one ref such that the lower bound is not zero (similar to the first)
		 *     3. no order (we can simply check the configuration of reachabilityService, because we have refined the constraint)
		 *     4. not idempotent/not self-reflective (new variant to handle)
		 *     5. the co-domains of all refs are identical, because we simply call pick to find a target without type filtering
		 */
		
		SimpleCase sc1 = SimpleCase.none;
		SimpleCase sc2 = SimpleCase.none;
		
		int created = 0;
		
		sc1 = this.checkSimpleCase(availableReferences, config, reachability, srcSelector, tarSelector);
		sc2 = this.checkSimpleCase(availableReferences, config, reachability, tarSelector, srcSelector);
		
		int simpleCaseSizeASrc = handleSimpleCaseA(sc1,availableReferences,config,reachability,srcSelector,tarSelector);
		int simpleCaseSizeATar = handleSimpleCaseA(sc2,availableReferences,config,reachability,tarSelector,srcSelector);
		
		int remain = size - simpleCaseSizeASrc - simpleCaseSizeATar;
		
		int simpleCaseSizeBSrc = handleSimpleCaseB(sc1,availableReferences,config,reachability,srcSelector,tarSelector,remain);
		
		remain = size-simpleCaseSizeASrc-simpleCaseSizeATar-simpleCaseSizeBSrc;
		
		int simpleCaseSizeBTar = handleSimpleCaseB(sc2,availableReferences,config,reachability,tarSelector,srcSelector,remain);
		
		// fill min
		created+=linkSourceToTarget(availableReferences,srcSelector,tarSelector,reachability,true);
		created+=linkTargetToSource(availableReferences,srcSelector,tarSelector,reachability,true);
		
		remain = size - simpleCaseSizeASrc-simpleCaseSizeATar-simpleCaseSizeBSrc-simpleCaseSizeBTar-created;
		
		// check simple case for max
		
		
		if(remain>0) {
			int lastRemain = remain+1;
			while(lastRemain>remain) {
				lastRemain = remain;
				
				int size1 = linkSourceToTarget(availableReferences,srcSelector,tarSelector,reachability,false);
				remain = remain - size1;
				
				int size2 = linkTargetToSource(availableReferences,srcSelector,tarSelector,reachability,false);
				remain = remain - size2;
			}
		}
		if(remain>0) {
			this.generationService.echo(Level.SEVERE, "Cannot fill the required edge size!");
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	protected int linkSourceToTarget(List<ET> availableReferences, NodeSelectionService<NT, ET> srcSelector, NodeSelectionService<NT, ET> tarSelector,
			ReachabilityService<NT, ET> reachability, boolean fillMin) {
		
		Scope scope = fillMin ? Scope.first : Scope.firstAndSecond;
		int count = 0;
		
		do {
			Pair<ENode<NT,ET,?>, ET> pair = null;
			pair = srcSelector.pickObjectAndReference(scope);
			
			if(pair==null) {
				if(fillMin==false) 
					return 0; // error
				else 
					break; // reach min
			}
			
			int querySize = 1;
			
			if(fillMin) {
				List<ET> cRefs = srcSelector.getRelatedRefs(pair.getFirst().getType());
				int id = cRefs.indexOf(pair.getSecond());
				DegreeTable degree = this.generationService.getDegreeTable(cRefs, pair.getFirst());
				int[] lb = srcSelector.getLowerBoundForType(pair.getFirst().getType());
				
				querySize = lb[id]-degree.table[id];
				
				if(querySize==0) querySize = 1;
			}
			
			List<ENode<NT,ET,?>> tars = tarSelector.pickObjectWithConditions(pair.getFirst(), 
					pair.getSecond(), reachability, querySize);
			
			if(tars!=null && tars.size()>0) {
				for(ENode<NT,ET,?> tar : tars) {
					EEdge<NT, ET, EAT> edge = this.typeModel.createEdge(pair.getSecond(), (ENode<NT, ET, NAT>)pair.getFirst(), (ENode<NT, ET, NAT>)tar);
					this.hostGraph.addEdge(edge);
					count++;
					srcSelector.consume(pair.getFirst(), pair.getSecond());
					tarSelector.consume(tar, pair.getSecond());
					reachability.reset();
				}
			} else {
				int fixed = this.tryToAddFromSrc((ENode<NT, ET, NAT>) pair.getFirst(), pair.getSecond(), availableReferences, srcSelector, tarSelector, reachability);
				if(fixed==1) 
					count++;
				else 
					srcSelector.putUnhandledObj(pair.getFirst(), pair.getSecond(), scope);
			}
			
		} while(fillMin);
		
		if(srcSelector.firstLevel.size()!=0 && fillMin) {
			this.generationService.echo(Level.WARNING, "In source selector, firstLevel is not empty after filling lower bound.");
		}
		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	protected int linkTargetToSource(List<ET> availableReferences, NodeSelectionService<NT, ET> srcSelector, NodeSelectionService<NT, ET> tarSelector,
			ReachabilityService<NT, ET> reachability, boolean fillMin) {
		
		Scope scope = fillMin ? Scope.first : Scope.firstAndSecond;
		int count = 0;
		
		do {
			Pair<ENode<NT,ET,?>, ET> pair = null;
			pair = tarSelector.pickObjectAndReference(scope);
			
			if(pair==null) {
				if(fillMin==false) 
					return 0; // error
				else 
					break; // reach min
			}
			
			int querySize = 1;
			
			if(fillMin) {
				List<ET> cRefs = tarSelector.getRelatedRefs(pair.getFirst().getType());
				int id = cRefs.indexOf(pair.getSecond());
				DegreeTable degree = this.generationService.getDegreeTable(cRefs, pair.getFirst());
				int[] lb = tarSelector.getLowerBoundForType(pair.getFirst().getType());
				
				querySize = lb[id]-degree.table[id];
				
				if(querySize==0) querySize = 1;
			}
			
			List<ENode<NT,ET,?>> srcs = srcSelector.pickObjectWithConditions(pair.getFirst(), 
					pair.getSecond(), reachability, querySize);
			
			if(srcs!=null && srcs.size()>0) {
				for(ENode<NT,ET,?> src : srcs) {
					EEdge<NT, ET, EAT> edge = this.typeModel.createEdge(pair.getSecond(), (ENode<NT, ET, NAT>)src, (ENode<NT, ET, NAT>)pair.getFirst());
					this.hostGraph.addEdge(edge);
					count++;
					tarSelector.consume(pair.getFirst(), pair.getSecond());
					srcSelector.consume(src, pair.getSecond());
					reachability.reset();
				}
			} else {
				int fixed = this.tryToAddFromTar((ENode<NT, ET, NAT>) pair.getFirst(), pair.getSecond(), availableReferences, srcSelector, tarSelector, reachability);
				if(fixed==1) 
					count++;
				else 
					tarSelector.putUnhandledObj(pair.getFirst(), pair.getSecond(), scope);
			}
			
		} while(fillMin);
		
		if(tarSelector.firstLevel.size()!=0 && fillMin) {
			this.generationService.echo(Level.WARNING, "In target selector, firstLevel is not empty after filling lower bound.");
		}
		
		return count;
	}

	private int handleSimpleCaseA(SimpleCase sc, List<ET> availableReferences, EdgeSetConfig constraint,
			ReachabilityService<NT, ET> reachability, NodeSelectionService<NT, ET> srcSelector,
			NodeSelectionService<NT, ET> tarSelector) {
		if(sc==SimpleCase.none) {
			return 0;
		} else if(sc==SimpleCase.simple) {
			this.generationService.echo(Level.INFO, "Filling by simple case A: simple");
			int size = 0;
			for (int i = srcSelector.firstLevelAvailable.size(); i > 0; i=srcSelector.firstLevelAvailable.size()) {
				Pair<ENode<NT,ET,?>, ET> pair = srcSelector.firstLevelAvailable.get(i - 1);
				int mul = srcSelector.getLowerBound(pair.getSecond());
				for (int j = 0; j < mul; j++) {
					ENode<NT,ET,?> singleTar = tarSelector.pick();
					srcSelector.addEdge(pair.getFirst(), singleTar, pair.getSecond());
					srcSelector.consume(pair.getFirst(), pair.getSecond());
					tarSelector.consume(singleTar, pair.getSecond());
					size++;
				}
			}
			return size;
		} else if(sc==SimpleCase.unique) {
			this.generationService.echo(Level.INFO, "Filling by simple case A: unique");
			// it means at least one ref's lower bound is greater than 1
			Collections.shuffle(srcSelector.firstLevelAvailable,this.generationService.getRandomService().getRandom());
			int size = 0;
			for (int i = srcSelector.firstLevelAvailable.size(); i > 0; i=srcSelector.firstLevelAvailable.size()) {
				Pair<ENode<NT,ET,?>, ET> pair = srcSelector.firstLevelAvailable.get(i - 1);
				int mul = srcSelector.getLowerBound(pair.getSecond());
				if(mul>0) {
					ENode<NT,ET,?> singleTar = tarSelector.pick();
					srcSelector.addEdge(pair.getFirst(), singleTar, pair.getSecond());
					srcSelector.consume(pair.getFirst(), pair.getSecond());
					tarSelector.consume(singleTar, pair.getSecond());
					size++;
				} else {
					srcSelector.firstLevelAvailable.remove(i-1); // mul = 0, which means we don't generate
				}
			}
			return size;
		} else if(sc==SimpleCase.required) {
			// it means the lower bound of every ref is zero
			return 0;
		}
		return 0;
	}
	
	private int handleSimpleCaseB(SimpleCase sc, List<ET> availableReferences, EdgeSetConfig constraint,
			ReachabilityService<NT, ET> reachability, NodeSelectionService<NT, ET> srcSelector,
			NodeSelectionService<NT, ET> tarSelector, int remain) {
		if(sc==SimpleCase.none) {
			return 0;
		} else if(sc==SimpleCase.simple) {
			this.generationService.echo(Level.INFO, "Filling by simple case B: simple");
			int size = 0;
			for(int i=0;i<remain;i++) {
				Pair<ENode<NT,ET,?>, ET> pair = srcSelector.pickObjectAndReference(Scope.firstAndSecond);
				if(pair==null) continue;
				ENode<NT,ET,?> singleTar = tarSelector.pick();
				if(singleTar==null) continue;
				srcSelector.addEdge(pair.getFirst(), singleTar, pair.getSecond());
				srcSelector.consume(pair.getFirst(), pair.getSecond());
				tarSelector.consume(singleTar, pair.getSecond());
				size++;
			}
			return size;
		} else if(sc==SimpleCase.required) {
			return 0;
		} else if(sc==SimpleCase.unique) {
			
			return 0;
		}
		return 0;
	}
	
	
	

	protected void refineConstraints(List<ET> allRefs, EdgeSetConfig constraint, ReachabilityService<NT, ET> reachability) {
		// idem -> check uniqueness of allRefs
		if(constraint.isSourceUnique() || constraint.isTargetUnique()) {
			reachability.setIdempotent(false);
		} else {
			if(allRefs.size()==1 
					&& (this.typeModel.getSourceUpperBound(allRefs.get(0))==1  
					|| this.typeModel.getTargetUpperBound(allRefs.get(0))==1)) {
				reachability.setIdempotent(false);
			}
		}
		
		//self-ref -> check disjoin of src and tar for each ref
		
		if(constraint.isRefl()==false) {
			boolean mayHaveRing = false;
			for(ET r : allRefs) {
				NT s = this.typeModel.getSource(r);
				NT t = this.typeModel.getTarget(r);
				
				if(this.typeModel.hasCommonSubTypes(s, t)) {
					mayHaveRing = true; // we have to check self-reflective
					// we may go on checking whether source objects and target objects are disjoint
					break;
				}
			}
			if(mayHaveRing==false) 
				reachability.setSelfReflective(true);
		}
		
		//order -> to find a circle in all ref
		if(constraint.isOrd()) {
			if(isNaturalOrder(allRefs))
				reachability.setOrdered(false);
		}
		
	}
	
	private boolean isNaturalOrder(List<ET> refs) {
		Set<ET> visited = new HashSet<ET>();
		Set<ET> tested = new HashSet<ET>();
		
		for(ET r : refs) {
			if(tested.contains(r)) continue;
			if(findCircleFrom(r, refs, visited,tested))
				return false;
		}
		return true;
	}
	
	private boolean findCircleFrom(ET cur, List<ET> refs, Set<ET> visited, Set<ET> tested) {
		if(visited.contains(cur)) 
			return true; // find a circle
		visited.add(cur);
		
		for(ET r : refs) {
			if(this.typeModel.hasCommonSubTypes(this.typeModel.getTarget(cur), this.typeModel.getSource(r))) {
				if(findCircleFrom(r, refs, visited,tested)) {
					return true;
				}
			}
		}
		
		visited.remove(cur);
		tested.add(cur);
		return false;
	}
	
	enum SimpleCase {
		none,
		simple,
		unique,
		required
	}
	private SimpleCase checkSimpleCase(List<ET> list, EdgeSetConfig constraint, ReachabilityService<NT, ET> reachability, NodeSelectionService<NT,ET> source, NodeSelectionService<NT,ET> target) {
		if(reachability.isOrdered() 
				|| !reachability.isSelfReflective() 
				|| reachability.isIdempotent()) 
			return SimpleCase.none;
		
		// check co-domain
		{
			Collection<NT> codom = null;
			for(ET e : list) {
				NT ct = target.getReferenceClass(e);
				Collection<NT> c = this.typeModel.collectAllConcreteSubTypes(ct);
				if(codom==null) codom=c;
				else {
					if(codom.containsAll(c)&&c.containsAll(codom))
						codom = c;
					else return SimpleCase.none;
				}
			}
		}
		
		// check unique
		boolean unique = false;
		boolean require = false;
		int size = 0;
		
		for(ET e : list) {
			size += source.getLowerBound(e);
		}
		if(source.isUniqueInConstraint()) {
			if(size>1) unique=true;
		}
		
		if(source.isRequiredInConstraint()) {
			if(size==0) require = true;
		}
		
		if(unique==false&&require==false) return SimpleCase.simple;
		else if(unique==false&&require) return SimpleCase.unique;
		else if(unique&&require==false) return SimpleCase.required;
		else return SimpleCase.none;
		
	}
	
	
	/**
	 * this method try to insert an edge of type r between source and target
	 * @param source
	 * @param target
	 * @param r
	 */
	@SuppressWarnings("unchecked")
	protected int tryToAddFromSrc(ENode<NT,ET,NAT> source, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		// try swap
		List<SwapModificationForSrc> swap = this.findSwapFromSrc(source, r, references, srcSelector, tarSelector, reachability);
		
		if(swap!=null && swap.isEmpty()==false) {
			
			// realize fix one first
			int size = swap.size();
			SwapModificationForSrc selection = swap.get(this.generationService.getRandomService().randomInt(size));
			
			this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) selection.edge);
			this.hostGraph.addEdge(this.typeModel.createEdge(r, source, (ENode<NT, ET, NAT>) selection.b));
			this.hostGraph.addEdge(this.typeModel.createEdge(selection.ry, (ENode<NT, ET, NAT>)selection.a, (ENode<NT, ET, NAT>)selection.y));
			
			srcSelector.consume(source, r);
			srcSelector.consume(selection.a, selection.rx, selection.ry);
			
			tarSelector.consume(selection.y, selection.rx);
			tarSelector.consume(selection.b, selection.rx,r);
			
			reachability.reset();
			
			return 1;
		} else {
			if(reachability.isOrdered()) {
				// try reorder
				ReorderModificationForSrc reorder = this.findReorderFromSrc(source, r, references, srcSelector, tarSelector, reachability);
				if(reorder!=null) {
					/*
					 * src-(r)->?b
					 * all b, b-->src => find b, critical edge [x-(rx)->y] along b—>src (i.e., b—>x, y—>src), and find [z1-(rz1)->?] | [?-(rz2)->z2], 
					 * 	-[x-(rx)->y] +[src-(r)->b], +[z1-(rz1)->y] or +swap [z1-(rz1)->y] (when y drops to min) | +[x-(rz2)->z2] or +swap [x-(rz2)->z2] (when x drops to min)
					 * <= not y—->z1, not z2-->x
					 */
					
					// -[x-(rx)->y], +[src-(r)->b]
					this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) reorder.edge);
					this.hostGraph.addEdge(this.typeModel.createEdge(r, source, (ENode<NT, ET, NAT>) reorder.b));
					if(reorder.swapZ1==null) {
						// +[z1-(rz1)-y]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.rz1, (ENode<NT, ET, NAT>)reorder.z1, (ENode<NT, ET, NAT>)reorder.y));
						
						//- x-(rx), + src-(r), (rx)->y => (rz1)->y, + z1-(rz1), + (r)->b
						srcSelector.release(reorder.x, reorder.rx);
						srcSelector.consume(source, r);
						tarSelector.consume(reorder.b, r);
						srcSelector.consume(reorder.z1, reorder.rz1);
						tarSelector.consume(reorder.y, reorder.rx, reorder.rz1);
					} else {
						// -[z1-(rz1)->s.b]
						this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) reorder.swapZ1.edge);
						// +[a-rx->tar] i.e., [z1,rz1,y]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.rz1, (ENode<NT, ET, NAT>)reorder.z1, (ENode<NT, ET, NAT>)reorder.y));
						// +[s.y-s.ry->s.b]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.swapZ1.ry,(ENode<NT, ET, NAT>)reorder.swapZ1.y,(ENode<NT, ET, NAT>)reorder.swapZ1.b)); 
						
						srcSelector.release(reorder.x, reorder.rx);
						srcSelector.consume(source, r);
						tarSelector.consume(reorder.b, r);
						//srcSelector.consume(reorder.z1, reorder.rz1, reorder.rz1);
						tarSelector.consume(reorder.swapZ1.b, reorder.rz1, reorder.swapZ1.ry);
						tarSelector.consume(reorder.y, reorder.rx, reorder.rz1);
					}
					
					reachability.reset();
					return 1;
				}			
			}
		}
		
		return 0;
		
	}
	
	
	/*
	 * src-(r)->? => find [a-(rx)->b] and [?-(ry)->y] => -[a-(rx)->b] +[src-(r)->b] +[a->(ry)->y]
  			<= codom(r)&codom(rx), dom(rx)&dom(ry)
  			a : all scope
  			b : all scope
  			y : first and second
	 */
	protected List<SwapModificationForSrc> findSwapFromSrc(ENode<NT,ET,NAT> source, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		List<SwapModificationForSrc> results = fetchStream(references).collect(
				()->(new ArrayList<SwapModificationForSrc>()), 
				(list, rx)->{
					NT codOfR = this.typeModel.getTarget(r);
					NT codOfRx = this.typeModel.getTarget(rx);
					if(this.typeModel.hasCommonSubTypes(codOfR, codOfRx)) {
						// 1. collect all b in codOfR
//						@SuppressWarnings("unchecked")
//						List<ENode<NT,ET,?>> allB = this.generationService.getAllInstanceOfType(codOfR);
//						Iterator<ENode<NT,ET,?>> bIter = reachability.collectNonForbiddenObjects(source, allB, codOfRx); // use codOfRx to filter allB
						Iterator<ENode<NT,ET,?>> allB = tarSelector.getAvailableObjects(codOfR,Scope.all);
						Iterator<ENode<NT,ET,?>> bIter = reachability.collectNonForbiddenObjects(source, allB, codOfRx); // use codOfRx to filter allB

						// collect all a-(rx)->b
						while(bIter.hasNext()) {
							ENode<NT,ET,?> b = bIter.next();
							// if [*-(r)->b] is full, check next because we cannot create (r) to b
							if(tarSelector.isFull(b, r)) continue; 
							
							// 2. for each a-(rx)->b, go on finding (?)->(ry)->y
							Collection<EEdge<NT,ET,EAT>> edgesA = this.hostGraph.getIncomings(b, rx);
							// I temporally do not use parallelStream here
							List<SwapModificationForSrc> resultsFromB = fetchStream(edgesA,false).collect(
									()->{return new ArrayList<SwapModificationForSrc>();},
									(candidates, aRxB)->{
										// aRxB = a-(rx)->b
										// if [a-(rx)->*] is below or just equal to the lower bound, then rx = ry
										// else search for ry
										
										ENode<NT,ET,?> a = aRxB.getSource();
										
										if(srcSelector.isEqualToOrLowerThanNeeded(a, rx)) {
											ET ry = rx;
											collectSwapFromSource(aRxB, a, rx, b, ry, reachability, tarSelector, candidates);
										} else {
											List<SwapModificationForSrc> candiatesOfAllY = fetchStream(references).collect(
													()->new ArrayList<SwapModificationForSrc>(), 
													(candidatesOfY,ry)->{
														collectSwapFromSource(aRxB, a, rx, b, ry, reachability, tarSelector, candidatesOfY);
													}, 
													(left,right)->{
														left.addAll(right);
													});
											candidates.addAll(candiatesOfAllY);
										}
									}, 
									(left,right)->{
										left.addAll(right);
									});
							list.addAll(resultsFromB);
						}
					}
				}, 
				(left,right)->{
					left.addAll(right);
				});
		
		return results;
	}



	protected void collectSwapFromSource(EEdge<NT,ET,?> edge, ENode<NT, ET, ?> a, ET rx, ENode<NT, ET, ?> b, ET ry,
			ReachabilityService<NT, ET> reachability, NodeSelectionService<NT, ET> tarSelector,
			List<GraphGenerator<NT, ET, NAT, EAT>.SwapModificationForSrc> candidates) {
		NT domOfRx = this.typeModel.getSource(rx);
		NT domOfRy = this.typeModel.getSource(ry);
		if(this.typeModel.hasCommonSubTypes(domOfRx, domOfRy)) {
			NT codOfRy = this.typeModel.getTarget(ry);
//			@SuppressWarnings("unchecked")
//			List<ENode<NT,ET,?>> allY = this.generationService.getAllInstanceOfType(codOfRy);
//			Iterator<ENode<NT,ET,?>> yIter = reachability.collectNonForbiddenObjects(a, allY,null);
			Iterator<ENode<NT,ET,?>> allObj = tarSelector.getAvailableObjects(Scope.firstAndSecond);
			Iterator<ENode<NT,ET,?>> yIter = reachability.collectNonForbiddenObjects(a, allObj, codOfRy);
			
			while(yIter.hasNext()) {
				ENode<NT,ET,?> y = yIter.next();
//				if(tarSelector.isFull(y)==false) {
					// now we have a-(rx)->b, ry,y
					SwapModificationForSrc swap = new SwapModificationForSrc(edge, ry, y);
					candidates.add(swap);
//				}							
			}
		}
	}
	
	class SwapModificationForSrc {
		
		public SwapModificationForSrc(EEdge<NT,ET,?> edge, ET ry, ENode<NT, ET, ?> y) {
			super();
			this.edge = edge;
			this.a = edge.getSource();
			this.rx = edge.getType();
			this.b = edge.getTarget();
			this.ry = ry;
			this.y = y;
		}
		
		public EEdge<NT,ET,?> edge;
		
		public ENode<NT,ET,?> a;
		public ET rx;
		public ENode<NT,ET,?> b;
		public ET ry;
		public ENode<NT,ET,?> y;
		
	}
	
	@SuppressWarnings("unchecked")
	protected int tryToAddFromTar(ENode<NT,ET,NAT> target, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		// try swap
		List<SwapModificationForTar> swap = this.findSwapFromTar(target, r, references, srcSelector, tarSelector, reachability);
		
		if(swap!=null && swap.isEmpty()==false) {
			
			// realize fix one first
			int size = swap.size();
			SwapModificationForTar selection = swap.get(this.generationService.getRandomService().randomInt(size));
			
			this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) selection.edge);
			this.hostGraph.addEdge(this.typeModel.createEdge(r, (ENode<NT, ET, NAT>)selection.a, target));
			this.hostGraph.addEdge(this.typeModel.createEdge(selection.ry, (ENode<NT, ET, NAT>)selection.y, (ENode<NT, ET, NAT>)selection.b));
			
			tarSelector.consume(target, r);
			tarSelector.consume(selection.b, selection.rx, selection.ry);
			
			srcSelector.consume(selection.y, selection.ry);
			srcSelector.consume(selection.a, selection.rx,r);
			
			reachability.reset();
			
			return 1;
		} else {
			if(reachability.isOrdered()) {
				// try reorder
				ReorderModificationForTar reorder = this.findReorderFromTar(target, r, references, srcSelector, tarSelector, reachability);
				if(reorder!=null) {
					/*
					 * ?b-(r)->tar
					 * all b, tar—>b => find b, critical edge [x-(rx)->y] along tar—>b (i.e.,tar-->x, y-->b),  and find [?-(rz1)->z1] |  [z2-(rz2)->?]
					 * -[x-(rx)->y] +[b-(r)->tar], +[x-(rz1)->z1] or +swap [x-(rz1)->z1] | +[z2-(rz2)->y] or +swap [z2-(rz2)->y] must
					 * <= not z1—>x, not y-->z2
					 */
					
					// -[x-(rx)->y], +[b-(r)->tar]
					this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) reorder.edge);
					this.hostGraph.addEdge(this.typeModel.createEdge(r, (ENode<NT, ET, NAT>) reorder.b,target));
					
					if(reorder.swapZ2==null) {
						// +[z2-(rz2)-y]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.rz2, (ENode<NT, ET, NAT>)reorder.z2, (ENode<NT, ET, NAT>)reorder.y));
						
						// -x-(rx), +(r)->tar, (rx)->y => (rz2)->y, +z2-(rz2), +b-(r)
						srcSelector.release(reorder.x, reorder.rx);
						tarSelector.consume(target, r);
						srcSelector.consume(reorder.b, r);
						srcSelector.consume(reorder.z2, reorder.rz2);
						tarSelector.consume(reorder.y, reorder.rx, reorder.rz2);
					} else {
						// -[z2-(rz2)->s.b] i.e., -[s.a-(s.rx)->s.b]
						this.hostGraph.removeEdge((EEdge<NT, ET, EAT>) reorder.swapZ2.edge);
						// +[a-rx->tar] i.e., [z2,rz2,y]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.rz2, (ENode<NT, ET, NAT>)reorder.z2, (ENode<NT, ET, NAT>)reorder.y));
						// +[s.y-s.ry->s.b]
						this.hostGraph.addEdge(this.typeModel.createEdge(reorder.swapZ2.ry,(ENode<NT, ET, NAT>)reorder.swapZ2.y,(ENode<NT, ET, NAT>)reorder.swapZ2.b)); 
						
						// -x-(rx), +(r)->tar, (rx)->y => (rz2)->y, =z2-(rz2), (s.rx)->s.b => (s.ry)->s.b, +b-(r)
						srcSelector.release(reorder.x, reorder.rx);
						tarSelector.consume(target, r);
						srcSelector.consume(reorder.b, r);
						//srcSelector.consume(reorder.z2, reorder.rz2, reorder.rz2);
						tarSelector.consume(reorder.swapZ2.b, reorder.rz2, reorder.swapZ2.ry);
						tarSelector.consume(reorder.y, reorder.rx, reorder.rz2);
					}
					reachability.reset();
					return 1;
				}
			}
		}
		return 0;
	}
	
	/*
	 * it must be a dual implementation of findSwapFromSrc
	 * ?-(r)->tar => find [a-(rx)->b] and [y-(ry)->?] => -[a-(rx)->b] +[a-(r)->tar] +[y-(ry)->b]
  		<= dom(r1)&dom(rx), codom(rx)&codom(ry)
  		a : all scope
  		b : all scope
  		y : first and second
	 */
	protected List<SwapModificationForTar> findSwapFromTar(ENode<NT,ET,NAT> target, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		List<SwapModificationForTar> results = fetchStream(references).collect(
				()->{return new ArrayList<SwapModificationForTar>();}, 
				(list, rx)->{
					NT domOfR = this.typeModel.getSource(r);
					NT domOfRx = this.typeModel.getSource(rx);
					if(this.typeModel.hasCommonSubTypes(domOfR, domOfRx)) {
						// 1. collect all a in domOfR
//						@SuppressWarnings("unchecked")
//						List<ENode<NT,ET,?>> allA = this.generationService.getAllInstanceOfType(domOfR);
//						Iterator<ENode<NT,ET,?>> aIter = reachability.collectNonForbiddenObjects(allA, target, domOfRx); // use domOfRx to filter allA
						
						Iterator<ENode<NT,ET,?>> allA = srcSelector.getAvailableObjects(domOfR, Scope.all);
						Iterator<ENode<NT,ET,?>> aIter = reachability.collectNonForbiddenObjects(allA, target, domOfRx); // use domOfRx to filter allA

						// collect all a-(rx)->b
						while(aIter.hasNext()) {
							ENode<NT,ET,?> a = aIter.next();
							// if [a-(r)->*] is full, check next because we cannot create (r) from a
							if(srcSelector.isFull(a, r)) continue; 
							
							// 2. for each a-(rx)->b, go on finding y->(ry)->(?)
							Collection<EEdge<NT,ET,EAT>> edgesA = this.hostGraph.getOutgoings(a, rx);
							// I temporally do not use parallelStream here
							List<SwapModificationForTar> resultsFromA = fetchStream(edgesA,false).collect(
									()->new ArrayList<SwapModificationForTar>(),
									(candidates, aRxB)->{
										// aRxB = a-(rx)->b
										// if [*-(rx)->b] is below or just equal to the lower bound, then rx = ry
										// else search for ry
										
										ENode<NT,ET,?> b = aRxB.getTarget();
										
										if(tarSelector.isEqualToOrLowerThanNeeded(b, rx)) {
											ET ry = rx;
											collectSwapFromTarget(aRxB, a,rx,b,ry,reachability,srcSelector,candidates);
										} else {
											List<SwapModificationForTar> candiatesOfAllY = fetchStream(references).collect(
													()->new ArrayList<SwapModificationForTar>(), 
													(candidatesOfY,ry)->{
														collectSwapFromTarget(aRxB, a,rx,b,ry,reachability,srcSelector,candidatesOfY);
													}, 
													(left,right)->{
														left.addAll(right);
													});
											candidates.addAll(candiatesOfAllY);
										}
									}, 
									(left,right)->{
										left.addAll(right);
									});
							list.addAll(resultsFromA);
						}
					}
				}, 
				(left,right)->{
					left.addAll(right);
				});
		
		return results;
	}
	
	protected void collectSwapFromTarget(EEdge<NT,ET,?> edge, ENode<NT, ET, ?> a, ET rx, ENode<NT, ET, ?> b, ET ry,
			ReachabilityService<NT, ET> reachability, NodeSelectionService<NT, ET> srcSelector,
			List<GraphGenerator<NT, ET, NAT, EAT>.SwapModificationForTar> candidates) {
		
		NT codOfRx = this.typeModel.getTarget(rx);
		NT codOfRy = this.typeModel.getTarget(ry);
		if(this.typeModel.hasCommonSubTypes(codOfRx, codOfRy)) {
			NT domOfRy = this.typeModel.getSource(ry);
//			@SuppressWarnings("unchecked")
//			List<ENode<NT,ET,?>> allY = this.generationService.getAllInstanceOfType(domOfRy);
//			Iterator<ENode<NT,ET,?>> yIter = reachability.collectNonForbiddenObjects(allY, b, null);
			
			Iterator<ENode<NT,ET,?>> allObj = srcSelector.getAvailableObjects(Scope.firstAndSecond);
			Iterator<ENode<NT,ET,?>> yIter = reachability.collectNonForbiddenObjects(allObj, b, domOfRy);
			
			while(yIter.hasNext()) {
				ENode<NT,ET,?> y = yIter.next();
//				if(srcSelector.isFull(y)==false) {
					// now we have a-(rx)->b, ry,y
					SwapModificationForTar swap = new SwapModificationForTar(edge, y, ry);
					candidates.add(swap);
//				}													
			}
		}
	}
	
	class SwapModificationForTar {
		public SwapModificationForTar(EEdge<NT,ET,?> edge, ENode<NT, ET, ?> y, ET ry) {
			super();
			this.edge = edge;
			this.a = edge.getSource();
			this.rx = edge.getType();
			this.b = edge.getTarget();
			this.ry = ry;
			this.y = y;
		}
		public EEdge<NT,ET,?> edge;
		public ENode<NT,ET,?> a;
		public ET rx;
		public ENode<NT,ET,?> b;
		public ET ry;
		public ENode<NT,ET,?> y;
		
	}
	
	class ReorderModificationForSrc {
		public ENode<NT,ET,?> b;
		public EEdge<NT,ET,?> edge; // x-(rx)->y
		public ENode<NT,ET,?> x;
		public ET rx;
		public ENode<NT,ET,?> y;
		
		public ET rz1;
		public ENode<NT,ET,?> z1;
		public SwapModificationForTar swapZ1;
		
//		public ET rz2;
//		public ENode<NT,ET,?> z2;
//		public SwapModificationForSrc swapZ2;
	}

	/*
	 * src-(r)->?b
	 * all b, b-->src => find b, critical edge [x-(rx)->y] along b—>src (i.e., b—>x, y—>src), and find [z1-(rz1)->?] | [?-(rz2)->z2], 
	 * 	-[x-(rx)->y] +[src-(r)->b], +[z1-(rz1)->y] or +swap [z1-(rz1)->y] (when y drops to min) | +[x-(rz2)->z2] or +swap [x-(rz2)->z2] (when x drops to min)
	 * <= not y—->z1, not z2-->x
	 */
	protected ReorderModificationForSrc findReorderFromSrc(ENode<NT,ET,NAT> source, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		NT codOfR = this.typeModel.getTarget(r);
		// maybe I should walk along from src to all reachable B
		// compare and refine later
//		List<ENode<NT,ET,?>> allB = this.generationService.getAllInstanceOfType(codOfR);
//		Iterator<ENode<NT,ET,?>> bIter = allB.iterator();
		Iterator<ENode<NT,ET,?>> bIter = reachability.getReachableIterator(source, codOfR, false);
		
		PairHashMap<ENode<NT, ET, ?>, ENode<NT, ET, ?>, List<EEdge<NT,ET,?>>> criticalInfo = new PairHashMap<ENode<NT, ET, ?>, ENode<NT, ET, ?>, List<EEdge<NT,ET,?>>>();
		
		HashSet<EEdge<NT,ET,?>> triedCriticalEdges = new HashSet<EEdge<NT,ET,?>>();
		
		while(bIter.hasNext()) {
			ENode<NT,ET,?> b = bIter.next();
			if(tarSelector.isFull(b, r)) continue;
			
			List<EEdge<NT,ET,?>> criticals = reachability.collectCriticalEdgeBetween(b, source, criticalInfo);
			
			for(EEdge<NT, ET, ?> xRxY : criticals) {
				if(triedCriticalEdges.contains(xRxY)) continue;
				triedCriticalEdges.add(xRxY);
				
				ENode<NT,ET,?> x = xRxY.getSource();
				ENode<NT,ET,?> y = xRxY.getTarget();
				ET rx = xRxY.getType();
				
				// find z1-(rz1) based on y, not srcSelector.isFull(z1,rz1) and not y-->z1 
				// 1. find rz1 that is able to end with y (y.type<rz1.target)
				// 2. iterate rz1.source.allInstances with z1, test y-->z1
				// 3. if z1 is full, put it to fullSet
				// 4. if no selection available, check fullSet try swap
				// 5. if no selection, continue loop
				
				Set<Pair<ENode<NT,ET,?>,ET>> fullSet = new HashSet<Pair<ENode<NT,ET,?>,ET>>();
				
				for(ET rz1 : references) {
					NT domOfRz1 = this.typeModel.getSource(rz1);
					NT codOfRz1 = this.typeModel.getTarget(rz1);
					
					if(this.typeModel.isSuperNodeType(y.getType(), codOfRz1)) {
//						List<ENode<NT,ET,?>> allZ1 = this.generationService.getAllInstanceOfType(domOfRz1);
//						Iterator<ENode<NT,ET,?>> z1Iter = reachability.collectNonForbiddenObjects(allZ1, y, null);
						Iterator<ENode<NT,ET,?>> allZ1 = srcSelector.getAvailableObjects(Scope.all);
						Iterator<ENode<NT,ET,?>> z1Iter = reachability.collectNonForbiddenObjects(allZ1, y, domOfRz1);
						
						while(z1Iter.hasNext()) {
							ENode<NT, ET, ?> z1 = z1Iter.next();
							if(srcSelector.isFull(z1)) {
								fullSet.add(new Pair<ENode<NT,ET,?>,ET>(z1,rz1));
							} else {
								ReorderModificationForSrc modification = new ReorderModificationForSrc();
								modification.b = b;
								modification.x = x;
								modification.rx = rx;
								modification.y = y;
								modification.edge = xRxY;
								modification.z1 = z1;
								modification.rz1 = rz1;
								return modification;
							}
						}
					}
				}
				
				// no direct selection, check fullSet
				
				// find [z1-(rz1)->s.b] and [s.y-(s.ry)->?] => -[z1-(rz1)->s.b] +[z1-(rz1)->y] +[s.y-(s.ry)->s.b]
				//  <=  codom(rz1)&codom(s.ry)
				
				for(Pair<ENode<NT,ET,?>,ET> pair : fullSet) {
					ENode<NT,ET,?> z1 = pair.getFirst();
					ET rz1 = pair.getSecond();
					NT codOfRz1 = this.typeModel.getTarget(rz1);
					for(ET s_ry : references) {
						NT domOfSry = this.typeModel.getSource(s_ry);
						NT codOfSry = this.typeModel.getTarget(s_ry);
						if(typeModel.hasCommonSubTypes(codOfRz1, codOfSry)==false) continue;
						Collection<EEdge<NT, ET, EAT>> edgesOfZ1 = this.hostGraph.getOutgoings(z1, rz1);
//						List<ENode<NT,ET,?>> allSy = this.generationService.getAllInstanceOfType(domOfSry);
						Iterator<ENode<NT,ET,?>> allSy = srcSelector.getAvailableObjects(domOfSry, Scope.firstAndSecond);
						for(EEdge<NT,ET,EAT> z1Rz1Sb : edgesOfZ1) {
							ENode<NT,ET,?> s_b = z1Rz1Sb.getTarget();
							if(this.typeModel.isSuperNodeType(s_b.getType(), codOfSry)) {
								Iterator<ENode<NT,ET,?>> syIter = reachability.collectNonForbiddenObjects(allSy, s_b, null);
								while(syIter.hasNext()) {
									ENode<NT,ET,?> s_y = syIter.next();
									if(srcSelector.isFull(s_y)) continue; // FIXME: no need to check
									
									ReorderModificationForSrc modification = new ReorderModificationForSrc();
									modification.b = b;
									modification.x = x;
									modification.rx = rx;
									modification.y = y;
									modification.edge = xRxY;
									
									modification.z1 = z1;
									modification.rz1 = rz1; // + [z1-(rz1)->y]
									modification.swapZ1 = new SwapModificationForTar(z1Rz1Sb, s_y, s_ry); // -z1Rz1Sb +s_y-s_ry->b
									return modification;
								}
							}
						}
					}
				}
				
				
				// find (rz2)->z2 based on x, not tarSelector.isFull(z2,rz2) and not z2-->x (optional)
				// currently do not do that
				// return
			}
		}
		
		return null;
	}
	
	
	
	
	class ReorderModificationForTar {
		public ENode<NT,ET,?> b;
		public EEdge<NT,ET,?> edge; // x-(rx)->y
		public ENode<NT,ET,?> x;
		public ET rx;
		public ENode<NT,ET,?> y;
		
//		public ET rz1;
//		public ENode<NT,ET,?> z1;
//		public SwapModificationForSrc swapZ1;
		
		public ET rz2;
		public ENode<NT,ET,?> z2;
		public SwapModificationForTar swapZ2;
	}
	/*
	 * ?b-(r)->tar
	 * all b, tar—>b => find b, critical edge [x-(rx)->y] along tar—>b (i.e.,tar-->x, y-->b),  and find [?-(rz1)->z1] |  [z2-(rz2)->?]
	 * -[x-(rx)->y] +[b-(r)->tar], +[x-(rz1)->z1] or +swap [x-(rz1)->z1] | +[z2-(rz2)->y] or +swap [z2-(rz2)->y] must
	 * <= not z1—>x, not y-->z2
	 */
	
	protected ReorderModificationForTar findReorderFromTar(ENode<NT,ET,NAT> target, ET r, List<ET> references, 
			NodeSelectionService<NT,ET> srcSelector, NodeSelectionService<NT,ET> tarSelector, ReachabilityService<NT, ET> reachability) {
		
		NT domOfR = this.typeModel.getSource(r);
		// maybe I should walk along from src to all reachable B
		// compare and refine later
//		List<ENode<NT,ET,?>> allB = this.generationService.getAllInstanceOfType(codOfR);
//		Iterator<ENode<NT,ET,?>> bIter = allB.iterator();
		Iterator<ENode<NT,ET,?>> bIter = reachability.getReachableIterator(target, domOfR, true);
		
		PairHashMap<ENode<NT, ET, ?>, ENode<NT, ET, ?>, List<EEdge<NT,ET,?>>> criticalInfo = new PairHashMap<ENode<NT, ET, ?>, ENode<NT, ET, ?>, List<EEdge<NT,ET,?>>>();
		
		HashSet<EEdge<NT,ET,?>> triedCriticalEdges = new HashSet<EEdge<NT,ET,?>>();
		
		while(bIter.hasNext()) {
			ENode<NT,ET,?> b = bIter.next();
			if(srcSelector.isFull(b, r)) continue;
			
			List<EEdge<NT,ET,?>> criticals = reachability.collectCriticalEdgeBetween(target, b, criticalInfo);
			
			for(EEdge<NT, ET, ?> xRxY : criticals) {
				if(triedCriticalEdges.contains(xRxY)) continue;
				triedCriticalEdges.add(xRxY);
				
				ENode<NT,ET,?> x = xRxY.getSource();
				ENode<NT,ET,?> y = xRxY.getTarget();
				ET rx = xRxY.getType();
				
				// find z2-(rz2) based on y, not srcSelector.isFull(z2,rz2) and not y-->z2
				
				Set<Pair<ET,ENode<NT,ET,?>>> fullSet = new HashSet<Pair<ET,ENode<NT,ET,?>>>();
				
				for(ET rz2 : references) {
					NT domOfRz2 = this.typeModel.getSource(rz2);
					NT codOfRz2 = this.typeModel.getTarget(rz2);
					
					if(this.typeModel.isSuperNodeType(y.getType(), codOfRz2)) {
//						List<ENode<NT,ET,?>> allZ2 = this.generationService.getAllInstanceOfType(domOfRz2);
//						Iterator<ENode<NT,ET,?>> z2Iter = reachability.collectNonForbiddenObjects(allZ2, y, null);
						Iterator<ENode<NT,ET,?>> allZ2 = srcSelector.getAvailableObjects(Scope.all);
						Iterator<ENode<NT,ET,?>> z2Iter = reachability.collectNonForbiddenObjects(allZ2, y, domOfRz2);
						
						while(z2Iter.hasNext()) {
							ENode<NT, ET, ?> z2 = z2Iter.next();
							if(srcSelector.isFull(z2)) {
								fullSet.add(new Pair<ET,ENode<NT,ET,?>>(rz2,z2));
							} else {
								ReorderModificationForTar modification = new ReorderModificationForTar();
								modification.b = b;
								modification.x = x;
								modification.rx = rx;
								modification.y = y;
								modification.edge = xRxY;
								modification.z2 = z2;
								modification.rz2 = rz2;
								return modification;
							}
						}
					}
				}
				
				// no direct selection, check fullSet
				// find [z2-(rz2)->s.b] and [s.y-(s.ry)->?] => -[z2-(rz2)->s.b] +[z2-(rz2)->y] +[s.y-(s.ry)->s.b]
				//  <=  codom(rz1)&codom(s.ry)
				
				for(Pair<ET,ENode<NT,ET,?>> pair : fullSet) {
					ET rz2 = pair.getFirst();
					ENode<NT,ET,?> z2 = pair.getSecond();
					NT codOfRz1 = this.typeModel.getTarget(rz2);
					for(ET s_ry : references) {
						NT domOfSry = this.typeModel.getSource(s_ry);
						NT codOfSry = this.typeModel.getTarget(s_ry);
						if(typeModel.hasCommonSubTypes(codOfRz1, codOfSry)==false) continue;
						Collection<EEdge<NT, ET, EAT>> edgesOfZ2 = this.hostGraph.getOutgoings(z2, rz2);
//						List<ENode<NT,ET,?>> allSy = this.generationService.getAllInstanceOfType(domOfSry);
						Iterator<ENode<NT,ET,?>> allSy = srcSelector.getAvailableObjects(domOfSry,Scope.firstAndSecond);
						for(EEdge<NT,ET,EAT> z2Rz2Sb : edgesOfZ2) {
							ENode<NT,ET,?> s_b = z2Rz2Sb.getTarget();
							if(this.typeModel.isSuperNodeType(s_b.getType(), codOfSry)) {
								Iterator<ENode<NT,ET,?>> syIter = reachability.collectNonForbiddenObjects(allSy, s_b, null);
								while(syIter.hasNext()) {
									ENode<NT,ET,?> s_y = syIter.next();
									if(srcSelector.isFull(s_y)) continue; // FIXME: no need to check due to scope restriction
									
									ReorderModificationForTar modification = new ReorderModificationForTar();
									modification.b = b;
									modification.x = x;
									modification.rx = rx;
									modification.y = y;
									modification.edge = xRxY;
									
									modification.z2 = z2;
									modification.rz2 = rz2;
									modification.swapZ2 = new SwapModificationForTar(z2Rz2Sb, s_y, s_ry);
									return modification;
								}
							}
						}
					}
				}
				
				
				// find (rz2)->z2 based on x, not tarSelector.isFull(z2,rz2) and not z2-->x (optional)
				// currently do not do that
				// return
			}
		}
		
		return null;
	}
	
	protected <T> Stream<T> fetchStream(Collection<T> list) {
		return this.generationService.fetchStream(list);
	}
	protected <T> Stream<T> fetchStream(Collection<T> set, boolean parallel) {
		return this.generationService.fetchStream(set, parallel);
	}

	public EGraph<NT, ET, NAT, EAT> getHostGraph() {
		return hostGraph;
	}
	
	public Map<Object,Object> getSaveOptions() {
		return this.generationService.getSaveOptions();
	}
}
