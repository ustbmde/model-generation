package edu.ustb.sei.egraph.generator.value;

import java.util.HashSet;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class NamedRandomFunction<T> extends AbstractRandomFunction<T> {

	public NamedRandomFunction(String name, AbstractRandomFunction<T> function) {
		super();
		this.name = name;
		this.function = function;
	}

	protected String name;
	protected AbstractRandomFunction<T> function;

	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		NamedRandomValueFunction<T> f = new NamedRandomValueFunction<T>(name, function.getValue(t));
		return f;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	private Set<String> provided = null;
	@Override
	public Set<String> providedScopes() {
		if(provided==null) {
			provided = new HashSet<String>();
			provided.add(name);
			provided.addAll(function.providedScopes());
		}
		return provided;
	}
	
	@Override
	public Set<String> requiredScopes() {
		return function.requiredScopes();
	}

}
