/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Copy Model Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getFromURI <em>From URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getToURI <em>To URI</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCopyModelAction()
 * @model
 * @generated
 */
public interface CopyModelAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>From URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From URI</em>' attribute.
	 * @see #setFromURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCopyModelAction_FromURI()
	 * @model
	 * @generated
	 */
	String getFromURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getFromURI <em>From URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From URI</em>' attribute.
	 * @see #getFromURI()
	 * @generated
	 */
	void setFromURI(String value);

	/**
	 * Returns the value of the '<em><b>To URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To URI</em>' attribute.
	 * @see #setToURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCopyModelAction_ToURI()
	 * @model
	 * @generated
	 */
	String getToURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CopyModelAction#getToURI <em>To URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To URI</em>' attribute.
	 * @see #getToURI()
	 * @generated
	 */
	void setToURI(String value);

} // CopyModelAction
