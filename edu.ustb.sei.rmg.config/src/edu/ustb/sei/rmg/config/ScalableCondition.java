/**
 */
package edu.ustb.sei.rmg.config;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scalable Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.ScalableCondition#isScalable <em>Scalable</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getScalableCondition()
 * @model abstract="true"
 * @generated
 */
public interface ScalableCondition extends Condition {
	/**
	 * Returns the value of the '<em><b>Scalable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scalable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scalable</em>' attribute.
	 * @see #setScalable(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getScalableCondition_Scalable()
	 * @model default="true"
	 * @generated
	 */
	boolean isScalable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.ScalableCondition#isScalable <em>Scalable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scalable</em>' attribute.
	 * @see #isScalable()
	 * @generated
	 */
	void setScalable(boolean value);

} // ScalableCondition
