/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Option Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getMetamodelURI <em>Metamodel URI</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getOutputFileURI <em>Output File URI</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getRootClass <em>Root Class</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#isAutoNumber <em>Auto Number</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#isUniqueRoot <em>Unique Root</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#isEnableConcurrency <em>Enable Concurrency</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getGlobalOptions <em>Global Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getClsOptions <em>Cls Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getAttrOptions <em>Attr Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getRefOptions <em>Ref Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getFilterOptions <em>Filter Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#isBoundSolver <em>Bound Solver</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.OptionModel#getMetamodel <em>Metamodel</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel()
 * @model
 * @generated
 */
public interface OptionModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel URI</em>' attribute.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_MetamodelURI()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getMetamodelURI();

	/**
	 * Returns the value of the '<em><b>Output File URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output File URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output File URI</em>' attribute.
	 * @see #setOutputFileURI(String)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_OutputFileURI()
	 * @model
	 * @generated
	 */
	String getOutputFileURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#getOutputFileURI <em>Output File URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output File URI</em>' attribute.
	 * @see #getOutputFileURI()
	 * @generated
	 */
	void setOutputFileURI(String value);

	/**
	 * Returns the value of the '<em><b>Root Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Class</em>' reference.
	 * @see #setRootClass(EClass)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_RootClass()
	 * @model
	 * @generated
	 */
	EClass getRootClass();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#getRootClass <em>Root Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Class</em>' reference.
	 * @see #getRootClass()
	 * @generated
	 */
	void setRootClass(EClass value);

	/**
	 * Returns the value of the '<em><b>Auto Number</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auto Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Number</em>' attribute.
	 * @see #setAutoNumber(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_AutoNumber()
	 * @model default="false"
	 * @generated
	 */
	boolean isAutoNumber();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#isAutoNumber <em>Auto Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Number</em>' attribute.
	 * @see #isAutoNumber()
	 * @generated
	 */
	void setAutoNumber(boolean value);

	/**
	 * Returns the value of the '<em><b>Unique Root</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique Root</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique Root</em>' attribute.
	 * @see #setUniqueRoot(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_UniqueRoot()
	 * @model default="true"
	 * @generated
	 */
	boolean isUniqueRoot();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#isUniqueRoot <em>Unique Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique Root</em>' attribute.
	 * @see #isUniqueRoot()
	 * @generated
	 */
	void setUniqueRoot(boolean value);

	/**
	 * Returns the value of the '<em><b>Enable Concurrency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enable Concurrency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enable Concurrency</em>' attribute.
	 * @see #setEnableConcurrency(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_EnableConcurrency()
	 * @model
	 * @generated
	 */
	boolean isEnableConcurrency();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#isEnableConcurrency <em>Enable Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enable Concurrency</em>' attribute.
	 * @see #isEnableConcurrency()
	 * @generated
	 */
	void setEnableConcurrency(boolean value);

	/**
	 * Returns the value of the '<em><b>Global Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.GlobalOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_GlobalOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<GlobalOption> getGlobalOptions();

	/**
	 * Returns the value of the '<em><b>Cls Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.ClassOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cls Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cls Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_ClsOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassOption> getClsOptions();

	/**
	 * Returns the value of the '<em><b>Attr Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.AttributeOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attr Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attr Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_AttrOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeOption> getAttrOptions();

	/**
	 * Returns the value of the '<em><b>Ref Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.AbstractReferenceOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_RefOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractReferenceOption> getRefOptions();

	/**
	 * Returns the value of the '<em><b>Filter Options</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.config.ReferenceFilterOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Options</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Options</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_FilterOptions()
	 * @model containment="true"
	 * @generated
	 */
	EList<ReferenceFilterOption> getFilterOptions();

	/**
	 * Returns the value of the '<em><b>Bound Solver</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound Solver</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound Solver</em>' attribute.
	 * @see #setBoundSolver(boolean)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_BoundSolver()
	 * @model
	 * @generated
	 */
	boolean isBoundSolver();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#isBoundSolver <em>Bound Solver</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound Solver</em>' attribute.
	 * @see #isBoundSolver()
	 * @generated
	 */
	void setBoundSolver(boolean value);

	/**
	 * Returns the value of the '<em><b>Metamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel</em>' reference.
	 * @see #setMetamodel(EPackage)
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getOptionModel_Metamodel()
	 * @model
	 * @generated
	 */
	EPackage getMetamodel();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.config.OptionModel#getMetamodel <em>Metamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metamodel</em>' reference.
	 * @see #getMetamodel()
	 * @generated
	 */
	void setMetamodel(EPackage value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model refRequired="true"
	 * @generated
	 */
	boolean isConstrained(EReference ref);

} // OptionModel
