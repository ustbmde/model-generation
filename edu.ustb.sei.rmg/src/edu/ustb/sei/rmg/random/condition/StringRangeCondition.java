package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class StringRangeCondition extends Condition<String> {
	public Condition<Character> charRange;
	public Condition<Integer> sizeRange;
	@Override
	public String getValue(RandomUtil r, Object... extra){
		int length = sizeRange.getValue(r);
		char[] buf = new char[length];
		for(int i=0;i<length;i++) {
			buf[i] = charRange.getValue(r);
		}
		return new String(buf);
	}
}
