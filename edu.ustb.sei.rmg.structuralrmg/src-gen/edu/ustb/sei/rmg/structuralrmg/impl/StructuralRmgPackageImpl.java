/**
 */
package edu.ustb.sei.rmg.structuralrmg.impl;

import edu.ustb.sei.rmg.structuralrmg.AttributeBinding;
import edu.ustb.sei.rmg.structuralrmg.BindingMapping;
import edu.ustb.sei.rmg.structuralrmg.Bound;
import edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.CompositionSort;
import edu.ustb.sei.rmg.structuralrmg.EndpointBinding;
import edu.ustb.sei.rmg.structuralrmg.NamedElement;
import edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateModel;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructuralRmgPackageImpl extends EPackageImpl implements StructuralRmgPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass templateModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass templateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass templateNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boundEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass templateEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveTemplateEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass chainTemplateEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveTemplateNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nestedTemplateNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bindingMappingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endpointBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum compositionSortEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StructuralRmgPackageImpl() {
		super(eNS_URI, StructuralRmgFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StructuralRmgPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StructuralRmgPackage init() {
		if (isInited) return (StructuralRmgPackage)EPackage.Registry.INSTANCE.getEPackage(StructuralRmgPackage.eNS_URI);

		// Obtain or create and register package
		StructuralRmgPackageImpl theStructuralRmgPackage = (StructuralRmgPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StructuralRmgPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StructuralRmgPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theStructuralRmgPackage.createPackageContents();

		// Initialize created meta-data
		theStructuralRmgPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStructuralRmgPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StructuralRmgPackage.eNS_URI, theStructuralRmgPackage);
		return theStructuralRmgPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemplateModel() {
		return templateModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateModel_Templates() {
		return (EReference)templateModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateModel_Packages() {
		return (EReference)templateModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemplate() {
		return templateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplate_Roles() {
		return (EReference)templateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplate_Nodes() {
		return (EReference)templateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplate_Edges() {
		return (EReference)templateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemplate_Abstract() {
		return (EAttribute)templateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplate_SuperTemplate() {
		return (EReference)templateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplate__GetAllRoles() {
		return templateEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplate__GetAllNodes() {
		return templateEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplate__GetAllEdges() {
		return templateEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplate__IsSuperOf__Template() {
		return templateEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemplateNode() {
		return templateNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateNode_Role() {
		return (EReference)templateNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateNode_Bound() {
		return (EReference)templateNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplateNode__IsSet() {
		return templateNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplateNode__GetMax() {
		return templateNodeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTemplateNode__GetMin() {
		return templateNodeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBound() {
		return boundEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBound_Min() {
		return (EAttribute)boundEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBound_Max() {
		return (EAttribute)boundEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemplateEdge() {
		return templateEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateEdge_Reference() {
		return (EReference)templateEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemplateEdge_Binding() {
		return (EReference)templateEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveTemplateEdge() {
		return primitiveTemplateEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveTemplateEdge_Source() {
		return (EReference)primitiveTemplateEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveTemplateEdge_Target() {
		return (EReference)primitiveTemplateEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrimitiveTemplateEdge_SourcePos() {
		return (EAttribute)primitiveTemplateEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrimitiveTemplateEdge_TargetPos() {
		return (EAttribute)primitiveTemplateEdgeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChainTemplateEdge() {
		return chainTemplateEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChainTemplateEdge_Node() {
		return (EReference)chainTemplateEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChainTemplateEdge_Round() {
		return (EAttribute)chainTemplateEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveTemplateNode() {
		return primitiveTemplateNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveTemplateNode_Type() {
		return (EReference)primitiveTemplateNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimitiveTemplateNode_Attributes() {
		return (EReference)primitiveTemplateNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeBinding() {
		return attributeBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeBinding_Attribute() {
		return (EReference)attributeBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNestedTemplateNode() {
		return nestedTemplateNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNestedTemplateNode_Templates() {
		return (EReference)nestedTemplateNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNestedTemplateNode_Sort() {
		return (EAttribute)nestedTemplateNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole() {
		return roleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBindingMapping() {
		return bindingMappingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBindingMapping_Roles() {
		return (EReference)bindingMappingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBindingMapping_Template() {
		return (EReference)bindingMappingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBindingMapping_Position() {
		return (EAttribute)bindingMappingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEndpointBinding() {
		return endpointBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEndpointBinding_Edge() {
		return (EReference)endpointBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEndpointBinding_SourceMappings() {
		return (EReference)endpointBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEndpointBinding_TargetMappings() {
		return (EReference)endpointBindingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCompositionSort() {
		return compositionSortEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralRmgFactory getStructuralRmgFactory() {
		return (StructuralRmgFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		templateModelEClass = createEClass(TEMPLATE_MODEL);
		createEReference(templateModelEClass, TEMPLATE_MODEL__TEMPLATES);
		createEReference(templateModelEClass, TEMPLATE_MODEL__PACKAGES);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		templateEClass = createEClass(TEMPLATE);
		createEReference(templateEClass, TEMPLATE__ROLES);
		createEReference(templateEClass, TEMPLATE__NODES);
		createEReference(templateEClass, TEMPLATE__EDGES);
		createEAttribute(templateEClass, TEMPLATE__ABSTRACT);
		createEReference(templateEClass, TEMPLATE__SUPER_TEMPLATE);
		createEOperation(templateEClass, TEMPLATE___GET_ALL_ROLES);
		createEOperation(templateEClass, TEMPLATE___GET_ALL_NODES);
		createEOperation(templateEClass, TEMPLATE___GET_ALL_EDGES);
		createEOperation(templateEClass, TEMPLATE___IS_SUPER_OF__TEMPLATE);

		templateNodeEClass = createEClass(TEMPLATE_NODE);
		createEReference(templateNodeEClass, TEMPLATE_NODE__ROLE);
		createEReference(templateNodeEClass, TEMPLATE_NODE__BOUND);
		createEOperation(templateNodeEClass, TEMPLATE_NODE___IS_SET);
		createEOperation(templateNodeEClass, TEMPLATE_NODE___GET_MAX);
		createEOperation(templateNodeEClass, TEMPLATE_NODE___GET_MIN);

		boundEClass = createEClass(BOUND);
		createEAttribute(boundEClass, BOUND__MIN);
		createEAttribute(boundEClass, BOUND__MAX);

		templateEdgeEClass = createEClass(TEMPLATE_EDGE);
		createEReference(templateEdgeEClass, TEMPLATE_EDGE__REFERENCE);
		createEReference(templateEdgeEClass, TEMPLATE_EDGE__BINDING);

		primitiveTemplateEdgeEClass = createEClass(PRIMITIVE_TEMPLATE_EDGE);
		createEReference(primitiveTemplateEdgeEClass, PRIMITIVE_TEMPLATE_EDGE__SOURCE);
		createEReference(primitiveTemplateEdgeEClass, PRIMITIVE_TEMPLATE_EDGE__TARGET);
		createEAttribute(primitiveTemplateEdgeEClass, PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS);
		createEAttribute(primitiveTemplateEdgeEClass, PRIMITIVE_TEMPLATE_EDGE__TARGET_POS);

		chainTemplateEdgeEClass = createEClass(CHAIN_TEMPLATE_EDGE);
		createEReference(chainTemplateEdgeEClass, CHAIN_TEMPLATE_EDGE__NODE);
		createEAttribute(chainTemplateEdgeEClass, CHAIN_TEMPLATE_EDGE__ROUND);

		primitiveTemplateNodeEClass = createEClass(PRIMITIVE_TEMPLATE_NODE);
		createEReference(primitiveTemplateNodeEClass, PRIMITIVE_TEMPLATE_NODE__TYPE);
		createEReference(primitiveTemplateNodeEClass, PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES);

		attributeBindingEClass = createEClass(ATTRIBUTE_BINDING);
		createEReference(attributeBindingEClass, ATTRIBUTE_BINDING__ATTRIBUTE);

		nestedTemplateNodeEClass = createEClass(NESTED_TEMPLATE_NODE);
		createEReference(nestedTemplateNodeEClass, NESTED_TEMPLATE_NODE__TEMPLATES);
		createEAttribute(nestedTemplateNodeEClass, NESTED_TEMPLATE_NODE__SORT);

		roleEClass = createEClass(ROLE);

		bindingMappingEClass = createEClass(BINDING_MAPPING);
		createEReference(bindingMappingEClass, BINDING_MAPPING__ROLES);
		createEReference(bindingMappingEClass, BINDING_MAPPING__TEMPLATE);
		createEAttribute(bindingMappingEClass, BINDING_MAPPING__POSITION);

		endpointBindingEClass = createEClass(ENDPOINT_BINDING);
		createEReference(endpointBindingEClass, ENDPOINT_BINDING__EDGE);
		createEReference(endpointBindingEClass, ENDPOINT_BINDING__SOURCE_MAPPINGS);
		createEReference(endpointBindingEClass, ENDPOINT_BINDING__TARGET_MAPPINGS);

		// Create enums
		compositionSortEEnum = createEEnum(COMPOSITION_SORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		templateModelEClass.getESuperTypes().add(this.getNamedElement());
		templateEClass.getESuperTypes().add(this.getNamedElement());
		templateNodeEClass.getESuperTypes().add(this.getNamedElement());
		templateEdgeEClass.getESuperTypes().add(this.getNamedElement());
		primitiveTemplateEdgeEClass.getESuperTypes().add(this.getTemplateEdge());
		chainTemplateEdgeEClass.getESuperTypes().add(this.getTemplateEdge());
		primitiveTemplateNodeEClass.getESuperTypes().add(this.getTemplateNode());
		attributeBindingEClass.getESuperTypes().add(this.getNamedElement());
		nestedTemplateNodeEClass.getESuperTypes().add(this.getTemplateNode());
		roleEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(templateModelEClass, TemplateModel.class, "TemplateModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTemplateModel_Templates(), this.getTemplate(), null, "templates", null, 0, -1, TemplateModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplateModel_Packages(), ecorePackage.getEPackage(), null, "packages", null, 0, -1, TemplateModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(templateEClass, Template.class, "Template", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTemplate_Roles(), this.getRole(), null, "roles", null, 0, -1, Template.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplate_Nodes(), this.getTemplateNode(), null, "nodes", null, 0, -1, Template.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplate_Edges(), this.getTemplateEdge(), null, "edges", null, 0, -1, Template.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemplate_Abstract(), ecorePackage.getEBoolean(), "abstract", "false", 1, 1, Template.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplate_SuperTemplate(), this.getTemplate(), null, "superTemplate", null, 0, 1, Template.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTemplate__GetAllRoles(), this.getRole(), "getAllRoles", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTemplate__GetAllNodes(), this.getTemplateNode(), "getAllNodes", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTemplate__GetAllEdges(), this.getTemplateEdge(), "getAllEdges", 0, -1, IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getTemplate__IsSuperOf__Template(), ecorePackage.getEBoolean(), "isSuperOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTemplate(), "t", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(templateNodeEClass, TemplateNode.class, "TemplateNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTemplateNode_Role(), this.getRole(), null, "role", null, 0, 1, TemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplateNode_Bound(), this.getBound(), null, "bound", null, 0, 1, TemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTemplateNode__IsSet(), ecorePackage.getEBoolean(), "isSet", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTemplateNode__GetMax(), ecorePackage.getEInt(), "getMax", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTemplateNode__GetMin(), ecorePackage.getEInt(), "getMin", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(boundEClass, Bound.class, "Bound", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBound_Min(), ecorePackage.getEInt(), "min", "0", 1, 1, Bound.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBound_Max(), ecorePackage.getEInt(), "max", "-1", 1, 1, Bound.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(templateEdgeEClass, TemplateEdge.class, "TemplateEdge", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTemplateEdge_Reference(), ecorePackage.getEReference(), null, "reference", null, 1, 1, TemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemplateEdge_Binding(), this.getEndpointBinding(), this.getEndpointBinding_Edge(), "binding", null, 0, 1, TemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveTemplateEdgeEClass, PrimitiveTemplateEdge.class, "PrimitiveTemplateEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimitiveTemplateEdge_Source(), this.getTemplateNode(), null, "source", null, 1, 1, PrimitiveTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimitiveTemplateEdge_Target(), this.getTemplateNode(), null, "target", null, 1, 1, PrimitiveTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrimitiveTemplateEdge_SourcePos(), ecorePackage.getEIntegerObject(), "sourcePos", null, 0, 1, PrimitiveTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrimitiveTemplateEdge_TargetPos(), ecorePackage.getEIntegerObject(), "targetPos", null, 0, 1, PrimitiveTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(chainTemplateEdgeEClass, ChainTemplateEdge.class, "ChainTemplateEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChainTemplateEdge_Node(), this.getTemplateNode(), null, "node", null, 1, 1, ChainTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChainTemplateEdge_Round(), ecorePackage.getEBoolean(), "round", null, 1, 1, ChainTemplateEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveTemplateNodeEClass, PrimitiveTemplateNode.class, "PrimitiveTemplateNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimitiveTemplateNode_Type(), ecorePackage.getEClass(), null, "type", null, 1, 1, PrimitiveTemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimitiveTemplateNode_Attributes(), this.getAttributeBinding(), null, "attributes", null, 0, -1, PrimitiveTemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeBindingEClass, AttributeBinding.class, "AttributeBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeBinding_Attribute(), ecorePackage.getEAttribute(), null, "attribute", null, 1, 1, AttributeBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nestedTemplateNodeEClass, NestedTemplateNode.class, "NestedTemplateNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNestedTemplateNode_Templates(), this.getTemplate(), null, "templates", null, 1, -1, NestedTemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNestedTemplateNode_Sort(), this.getCompositionSort(), "sort", null, 1, 1, NestedTemplateNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleEClass, Role.class, "Role", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bindingMappingEClass, BindingMapping.class, "BindingMapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBindingMapping_Roles(), this.getRole(), null, "roles", null, 1, -1, BindingMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBindingMapping_Template(), this.getTemplate(), null, "template", null, 1, 1, BindingMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBindingMapping_Position(), ecorePackage.getEIntegerObject(), "position", null, 0, 1, BindingMapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(endpointBindingEClass, EndpointBinding.class, "EndpointBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEndpointBinding_Edge(), this.getTemplateEdge(), this.getTemplateEdge_Binding(), "edge", null, 1, 1, EndpointBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEndpointBinding_SourceMappings(), this.getBindingMapping(), null, "sourceMappings", null, 0, -1, EndpointBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEndpointBinding_TargetMappings(), this.getBindingMapping(), null, "targetMappings", null, 0, -1, EndpointBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(compositionSortEEnum, CompositionSort.class, "CompositionSort");
		addEEnumLiteral(compositionSortEEnum, CompositionSort.NESTED);
		addEEnumLiteral(compositionSortEEnum, CompositionSort.SHARED);
		addEEnumLiteral(compositionSortEEnum, CompositionSort.REVERSED);

		// Create resource
		createResource(eNS_URI);
	}

} //StructuralRmgPackageImpl
