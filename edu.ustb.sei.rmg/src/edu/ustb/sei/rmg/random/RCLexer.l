
Options in effect for RCLexer.g:

    ACTION-BLOCK=("RCLexer.java","/.","./")

    AST-DIRECTORY=""  AST-TYPE="Ast"  NOATTRIBUTES  NOAUTOMATIC-AST  NOBACKTRACK
    BYTE  CONFLICTS  DAT-DIRECTORY=""  DAT-FILE="RCLexerdcl.data"  
    DCL-FILE="RCLexerdcl.java"  NODEBUG  DEF-FILE="RCLexerdef.java"  
    DIRECTORY-PREFIX=""  NOEDIT  NOERROR-MAPS  ESCAPE='$'  
    EXPORT-TERMINALS=("RCParsersym.java","TK_","")  EXTENDS-PARSETABLE  
    FACTORY="new "  FILE-PREFIX="RCLexer"  FILTER="RCKWLexer.g"  NOFIRST  
    NOFOLLOW  NOGLR  NOGOTO-DEFAULT  GRM-FILE="RCLexer.g"  
    IMP-FILE="RCLexerimp.java"  INCLUDE-DIRECTORY="./"  LALR=2  LEGACY  NOLIST
    MARGIN=4  MAX-CASES=1024  NAMES=OPTIMIZED  NONT-CHECK  OR_MARKER='|'  
    OUT-DIRECTORY=""  PACKAGE="edu.ustb.sei.rmg.random"  NOPARENT-SAVE  
    PARSETABLE-INTERFACES="lpg.runtime.ParseTable"  PREFIX="Char_"  PRIORITY  
    PROGRAMMING_LANGUAGE=JAVA  PRS-FILE="RCLexerprs.java"  NOQUIET  READ-REDUCE
    REMAP-TERMINALS  RULE_CLASSNAMES=SEQUENTIAL  NOSCOPES  NOSERIALIZE  
    NOSHIFT-DEFAULT  SINGLE-PRODUCTIONS  NOSOFT-KEYWORDS  NOSTATES  SUFFIX=""  
    SYM-FILE="RCLexersym.java"  TAB-FILE="RCLexer.t"  TABLE  
    TEMPLATE="./LexerTemplateF.gi"  TRACE=CONFLICTS  NOVARIABLES  NOVERBOSE  
    NOVISITOR  VISITOR-TYPE="Visitor"  WARNINGS  NOXREF  

RCLexer.g:377:9:380:10:6825:6889: Error: This action is associated with a single production
RCLexer.g:77:9:77:20:1011:1022: Informative: The terminal CtlCharNotWS is useless.
RCLexer.g:101:9:101:13:1501:1505: Informative: The terminal Acute is useless.


RCLexer.g:130:9:130:13:2100:2104: Informative: Grammar is  LALR(2).

Number of Terminals: 103
Number of Nonterminals: 32
Number of Productions: 232
Number of Single Productions: 148
Number of Items: 522
Number of States: 40
Number of look-ahead states: 1
Number of Shift actions: 340
Number of Goto actions: 41
Number of Shift/Reduce actions: 385
Number of Goto/Reduce actions: 38
Number of Reduce actions: 211
Number of Shift-Reduce conflicts: 0
Number of Reduce-Reduce conflicts: 0

Number of entries in base Action Table: 119
Additional space required for compaction of Action Table: 0.8%

Number of unique terminal states: 41
Number of Shift actions saved by merging: 0
Number of Conflict points saved by merging: 0
Number of Reduce actions saved by merging: 0
Number of Reduce saved by default: 204

Number of entries in Terminal Action Table: 774
Additional space required for compaction of Terminal Table: 11.1%

Actions in Compressed Tables:
     Number of Shifts: 340
     Number of Shift/Reduces: 385
     Number of Look-Ahead Shifts: 1
     Number of Gotos: 41
     Number of Goto/Reduces: 38
     Number of Reduces: 7
     Number of Defaults: 25

Parsing Tables storage:
    Storage required for BASE_CHECK: 232 Bytes
    Storage required for BASE_ACTION: 706 Bytes
    Storage required for TERM_CHECK: 877 Bytes
    Storage required for TERM_ACTION: 1722 Bytes
