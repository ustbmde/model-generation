/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage
 * @generated
 */
public interface StructuralRmgFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StructuralRmgFactory eINSTANCE = edu.ustb.sei.rmg.structuralrmg.impl.StructuralRmgFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Template Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Template Model</em>'.
	 * @generated
	 */
	TemplateModel createTemplateModel();

	/**
	 * Returns a new object of class '<em>Template</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Template</em>'.
	 * @generated
	 */
	Template createTemplate();

	/**
	 * Returns a new object of class '<em>Bound</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bound</em>'.
	 * @generated
	 */
	Bound createBound();

	/**
	 * Returns a new object of class '<em>Primitive Template Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primitive Template Edge</em>'.
	 * @generated
	 */
	PrimitiveTemplateEdge createPrimitiveTemplateEdge();

	/**
	 * Returns a new object of class '<em>Chain Template Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Chain Template Edge</em>'.
	 * @generated
	 */
	ChainTemplateEdge createChainTemplateEdge();

	/**
	 * Returns a new object of class '<em>Primitive Template Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primitive Template Node</em>'.
	 * @generated
	 */
	PrimitiveTemplateNode createPrimitiveTemplateNode();

	/**
	 * Returns a new object of class '<em>Attribute Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Binding</em>'.
	 * @generated
	 */
	AttributeBinding createAttributeBinding();

	/**
	 * Returns a new object of class '<em>Nested Template Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nested Template Node</em>'.
	 * @generated
	 */
	NestedTemplateNode createNestedTemplateNode();

	/**
	 * Returns a new object of class '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role</em>'.
	 * @generated
	 */
	Role createRole();

	/**
	 * Returns a new object of class '<em>Binding Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binding Mapping</em>'.
	 * @generated
	 */
	BindingMapping createBindingMapping();

	/**
	 * Returns a new object of class '<em>Endpoint Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Endpoint Binding</em>'.
	 * @generated
	 */
	EndpointBinding createEndpointBinding();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StructuralRmgPackage getStructuralRmgPackage();

} //StructuralRmgFactory
