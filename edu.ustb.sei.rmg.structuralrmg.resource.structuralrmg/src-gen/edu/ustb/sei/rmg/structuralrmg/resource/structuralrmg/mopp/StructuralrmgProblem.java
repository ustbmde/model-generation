/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgProblem implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgProblem {
	
	private String message;
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType type;
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity severity;
	private java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> quickFixes;
	
	public StructuralrmgProblem(String message, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType type, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix>emptySet());
	}
	
	public StructuralrmgProblem(String message, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType type, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity severity, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public StructuralrmgProblem(String message, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType type, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity severity, java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType getType() {
		return type;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
