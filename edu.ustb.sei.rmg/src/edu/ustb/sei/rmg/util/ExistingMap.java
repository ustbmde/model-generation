package edu.ustb.sei.rmg.util;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class ExistingMap<K> implements Collection<K> {
	private static final long serialVersionUID = 7094259112833674939L;
	
	private TransparantLinkedList<K> list;
	private HashMap<K,TransparantLinkedListNode<K>> set;
	
	public ExistingMap(List<K> source) {
		super();
		
		list = new TransparantLinkedList<K>();
		set = new HashMap<K,TransparantLinkedListNode<K>>();
		
		for(K o : source) {
			add(o);
		}
	}
	
	public ExistingMap() {
		list = new TransparantLinkedList<K>();
		set = new HashMap<K,TransparantLinkedListNode<K>>();
	}

	@SuppressWarnings("unchecked")
	public ExistingMap(List<EObject> src, EClass typeFilter) {
		list = new TransparantLinkedList<K>();
		set = new HashMap<K,TransparantLinkedListNode<K>>(src.size());
		
		for(EObject o : src) {
			if(typeFilter.isSuperTypeOf(o.eClass()))
				add((K)o);
		}
	}

	@Override
	public boolean add(K e) {
		if(set.get(e)!=null) return false;
		TransparantLinkedListNode<K> node = new TransparantLinkedListNode<K>(e);
		set.put(e, node);
		list.add(node);
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends K> c) {
		throw new UnsupportedOperationException("Existing Map: addAll");
	}

	@Override
	public void clear() {
		set.clear();
		list.clear();
	}

	@Override
	public boolean contains(Object o) {
		return set.get(o)!=null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException("Existing Map: containsAll");
	}

	@Override
	public boolean isEmpty() {
		return set.isEmpty();
	}

	@Override
	public Iterator<K> iterator() {
		return list.getIterator();
	}

	@Override
	public boolean remove(Object o) {
		TransparantLinkedListNode<K> node = set.get(o);
		if(node==null) return false;
		set.remove(o);
		list.remove(node);
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("Existing Map: removeAll");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Existing Map: retainAll");
	}

	@Override
	public int size() {
		return set.size();
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException("Existing Map: toArray");
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException("Existing Map: toArray");
	}
	
	

}


class TransparantLinkedList<T> {
	private TransparantLinkedListNode<T> head;
	private TransparantLinkedListNode<T> tail;
	
	public TransparantLinkedList() {
		head = null;
		tail = null;
	}
	
	public void clear() {
		head = tail = null;
	}
	
	public void add(TransparantLinkedListNode<T> node) {
		if(head == tail && tail == null) {
			head = node;
			tail = node;
		} else {
			tail.addAfter(node);
			tail = node;
		}
	}
	
	public void remove(TransparantLinkedListNode<T> node) {
		node.remove();
		if(head == node) head = node.getNext();
		if(tail == node) tail = node.getPrev();
	}
	
	public TransparantLinkedListIterator<T> getIterator() {
		return new TransparantLinkedListIterator<T>(head);
	}
}
class TransparantLinkedListNode<T> {
	private TransparantLinkedListNode<T> prev;
	public TransparantLinkedListNode<T> getPrev() {
		return prev;
	}

	public TransparantLinkedListNode<T> getNext() {
		return next;
	}

	public T getValue() {
		return value;
	}

	private TransparantLinkedListNode<T> next;
	
	private T value;
	
	public TransparantLinkedListNode(T value) {
		this.value = value;
		prev = null;
		next = null;
	}
	
	public int hashCode() {
		return value.hashCode();
	}
	
	public boolean equals(Object v) {
		if(v instanceof TransparantLinkedListNode<?>) {
			return value.equals(((TransparantLinkedListNode<?>) v).value);
		}
		return false;
	}
	
	public void addAfter(TransparantLinkedListNode<T> newNode) {
		newNode.prev = this;
		newNode.next = this.next;
		this.next = newNode;
	}
	
	public void remove() {
		if(next!=null) {
			next.prev = this.prev;
		}
		if(prev!=null) {
			prev.next = this.next;
		}
	}
	

}

class TransparantLinkedListIterator<T> implements Iterator<T> {
	private TransparantLinkedListNode<T> head;

	public TransparantLinkedListIterator(TransparantLinkedListNode<T> head) {
		super();
		this.head = head;
	}

	@Override
	public boolean hasNext() {
		return head!=null;
	}

	@Override
	public T next() {
		TransparantLinkedListNode<T> cur = head;
		head = head.getNext();
		return cur.getValue();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Existing Map: toArray");
	}
	
}