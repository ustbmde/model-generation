package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class CompositeLongFunction extends Condition<Long> {
	public DistributionFunction<?> function;
	public RangeCondition<Long> range;

	@Override
	public Long getValue(RandomUtil r, Object... extra) {
		// TODO Auto-generated method stub
		while(true) {
			if(function instanceof IntegerDistributionFunction) {
				long v = ((LongDistributionFunction)function).getValue(r);
				if(isIn(v))
					return v;
			} else {
				double v = ((RealDistributionFunction)function).getValue(r);
				long round = Math.round(v);
				if(isIn(round))
					return round;
			}
			
		}
	}
	
	private boolean isIn(long v) {
		boolean gl = true, lu = true;
		if(range.lower!=null) gl = (v>=range.lower);
		
		if(range.upper!=null) lu = (v<=range.upper);
		
		return gl&&lu;
	}

}
