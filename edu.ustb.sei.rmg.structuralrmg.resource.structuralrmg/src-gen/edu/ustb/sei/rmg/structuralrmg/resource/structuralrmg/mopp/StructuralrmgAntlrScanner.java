/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgAntlrScanner implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextScanner {
	
	private org.antlr.runtime3_4_0.Lexer antlrLexer;
	
	public StructuralrmgAntlrScanner(org.antlr.runtime3_4_0.Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final org.antlr.runtime3_4_0.Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextToken result = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new org.antlr.runtime3_4_0.ANTLRStringStream(text));
	}
	
}
