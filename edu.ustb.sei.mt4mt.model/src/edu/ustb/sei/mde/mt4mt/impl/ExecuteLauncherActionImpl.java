/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.ExecuteLauncherAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;
import edu.ustb.sei.mt4mt.base.actions.LaunchExecutor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execute Launcher Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl#getLauncher <em>Launcher</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.ExecuteLauncherActionImpl#getMaxWait <em>Max Wait</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecuteLauncherActionImpl extends TestActionImpl implements ExecuteLauncherAction {
	/**
	 * The default value of the '{@link #getLauncher() <em>Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLauncher()
	 * @generated
	 * @ordered
	 */
	protected static final String LAUNCHER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLauncher() <em>Launcher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLauncher()
	 * @generated
	 * @ordered
	 */
	protected String launcher = LAUNCHER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxWait() <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxWait()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_WAIT_EDEFAULT = 10000L;

	/**
	 * The cached value of the '{@link #getMaxWait() <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxWait()
	 * @generated
	 * @ordered
	 */
	protected long maxWait = MAX_WAIT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecuteLauncherActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.EXECUTE_LAUNCHER_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLauncher() {
		return launcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLauncher(String newLauncher) {
		String oldLauncher = launcher;
		launcher = newLauncher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.EXECUTE_LAUNCHER_ACTION__LAUNCHER, oldLauncher, launcher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaxWait() {
		return maxWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxWait(long newMaxWait) {
		long oldMaxWait = maxWait;
		maxWait = newMaxWait;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.EXECUTE_LAUNCHER_ACTION__MAX_WAIT, oldMaxWait, maxWait));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__LAUNCHER:
				return getLauncher();
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__MAX_WAIT:
				return getMaxWait();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__LAUNCHER:
				setLauncher((String)newValue);
				return;
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__MAX_WAIT:
				setMaxWait((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__LAUNCHER:
				setLauncher(LAUNCHER_EDEFAULT);
				return;
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__MAX_WAIT:
				setMaxWait(MAX_WAIT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__LAUNCHER:
				return LAUNCHER_EDEFAULT == null ? launcher != null : !LAUNCHER_EDEFAULT.equals(launcher);
			case MT4MTPackage.EXECUTE_LAUNCHER_ACTION__MAX_WAIT:
				return maxWait != MAX_WAIT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (launcher: ");
		result.append(launcher);
		result.append(", maxWait: ");
		result.append(maxWait);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		LaunchExecutor exe = (LaunchExecutor) edu.ustb.sei.mt4mt.base.Actions.launch(launcher);
		exe.setMaxWait(maxWait);
		return exe;
	}

} //ExecuteLauncherActionImpl
