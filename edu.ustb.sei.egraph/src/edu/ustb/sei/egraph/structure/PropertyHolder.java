package edu.ustb.sei.egraph.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class PropertyHolder<AL> {

	private ConcurrentHashMap<AL,Object> properties;
	
	public PropertyHolder() {
		properties = new ConcurrentHashMap<AL, Object>();
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(AL key) {
		return (T)properties.get(key);
	}
	
	public void put(AL key, Object value) {
		properties.put(key, value);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void add(AL key, Object value) {
		Collection values = get(key);
		// NOTE! values is not supposed to be thread-safe
		if(values == null) {
			values = new ArrayList();
			put(key,values);
		}
		values.add(value);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addAll(AL key, Collection<?> vals) {
		if(vals==null) return;
		
		Collection values = get(key);
		// NOTE! values is not supposed to be thread-safe
		if(values == null) {
			values = new ArrayList();
			put(key,values);
		}
		values.addAll(vals);
	}
	
	public void remove(AL key) {
		properties.remove(key);
	}
	
	/**
	 * not thread safe
	 * @return
	 */
	public Set<Entry<AL,Object>> allProperties() {
		return this.properties.entrySet();
	}
}
