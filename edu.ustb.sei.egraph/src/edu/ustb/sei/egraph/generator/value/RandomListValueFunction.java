package edu.ustb.sei.egraph.generator.value;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomListValueFunction<T> extends RandomValueFunction<List<T>> {
	
	public RandomListValueFunction(AbstractRandomFunction<T> elementFunction,AbstractRandomFunction<Integer> lengthFunction) {
		super();
		this.elementFunction = elementFunction;
		this.lengthFunction = lengthFunction;
	}

	private AbstractRandomFunction<T> elementFunction;
	private AbstractRandomFunction<Integer> lengthFunction;

	@Override
	public List<T> getValue(RandomService t) {
		int length = lengthFunction.getValue(t).getValue(t);
		if(length<0) length=0;
		List<T> list = new ArrayList<T>(length);
		for(int i=0;i<length;i++)
			list.add(elementFunction.getValue(t).getValue(t));
		return list;
	}

	@Override
	public Set<String> providedScopes() {
		return elementFunction.providedScopes();
	}
	
	@Override
	public Set<String> requiredScopes() {
		return elementFunction.requiredScopes();
	}
}
