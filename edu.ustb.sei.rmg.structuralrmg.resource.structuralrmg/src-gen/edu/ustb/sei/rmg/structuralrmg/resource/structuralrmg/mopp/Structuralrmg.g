grammar Structuralrmg;

options {
	superClass = StructuralrmgANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;
}

@members{
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> expectedElements = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgProblem() {
					public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity getSeverity() {
						return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity.ERROR;
					}
					public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType getType() {
						return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement terminal = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFollowSetProvider.TERMINALS[terminalID];
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] containmentFeatures = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFollowSetProvider.LINKS[ids[i]];
		}
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace containmentTrace = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElement = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new StructuralrmgParser(new org.antlr.runtime3_4_0.CommonTokenStream(new StructuralrmgLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new StructuralrmgParser(new org.antlr.runtime3_4_0.CommonTokenStream(new StructuralrmgLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public StructuralrmgParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((StructuralrmgLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((StructuralrmgLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.TemplateModel.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Template.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_Template();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Role.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_Role();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Bound.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_Bound();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.AttributeBinding.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.EndpointBinding.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();
			}
			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.BindingMapping.class) {
				return parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping();
			}
		}
		throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>>();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParseResult parseResult = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> newFollowSet = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 109;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]> newFollowerPair : newFollowers) {
							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace containmentTrace = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace(null, newFollowerPair.getRight());
							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal newFollowTerminal = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[0]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel returns [edu.ustb.sei.rmg.structuralrmg.TemplateModel element = null]
@init{
}
:
	a0 = 'model' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[1]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[3]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[4]);
	}
	
	(
		(
			a2 = 'import' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[5]);
			}
			
			(
				a3 = URI				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("URI");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.emf.ecore.EPackage proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEPackage();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateModelPackagesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[6]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[7]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[8]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[9]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[10]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[11]);
	}
	
	(
		(
			(
				a4_0 = parse_edu_ustb_sei_rmg_structuralrmg_Template				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__TEMPLATES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_4_0_0_0, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[12]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[13]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[15]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_Template returns [edu.ustb.sei.rmg.structuralrmg.Template element = null]
@init{
}
:
	(
		(
			a0 = 'abstract' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_0, true, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
				// set value of boolean attribute
				Object value = true;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
				completedElement(value, false);
			}
		)?	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[16]);
	}
	
	a3 = 'template' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[17]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
				startIncompleteElement(element);
				// initialize boolean attribute
				{
					Object value = false;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
				}
			}
			if (a4 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[18]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[19]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[20]);
	}
	
	(
		(
			a5 = '(' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[21]);
			}
			
			(
				a6_0 = parse_edu_ustb_sei_rmg_structuralrmg_Role				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						startIncompleteElement(element);
						// initialize boolean attribute
						{
							Object value = false;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
						}
					}
					if (a6_0 != null) {
						if (a6_0 != null) {
							Object value = a6_0;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_1, a6_0, true);
						copyLocalizationInfos(a6_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[22]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[23]);
			}
			
			(
				(
					a7 = ',' {
						if (element == null) {
							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
							startIncompleteElement(element);
							// initialize boolean attribute
							{
								Object value = false;
								element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
							}
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[24]);
					}
					
					(
						a8_0 = parse_edu_ustb_sei_rmg_structuralrmg_Role						{
							if (terminateParsing) {
								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
							}
							if (element == null) {
								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
								startIncompleteElement(element);
								// initialize boolean attribute
								{
									Object value = false;
									element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
								}
							}
							if (a8_0 != null) {
								if (a8_0 != null) {
									Object value = a8_0;
									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_2_0_0_1, a8_0, true);
								copyLocalizationInfos(a8_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[25]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[26]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[27]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[28]);
			}
			
			a9 = ')' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_3, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[29]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[30]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[32]);
	}
	
	(
		(
			a10 = 'extends' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[33]);
			}
			
			(
				a11 = TEXT				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						startIncompleteElement(element);
						// initialize boolean attribute
						{
							Object value = false;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
						}
					}
					if (a11 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.Template, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateSuperTemplateReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[34]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[35]);
	}
	
	a12 = '{' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[36]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[37]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[38]);
	}
	
	(
		(
			a13 = 'node' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[39]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[40]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[41]);
			}
			
			(
				a14_0 = parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						startIncompleteElement(element);
						// initialize boolean attribute
						{
							Object value = false;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
						}
					}
					if (a14_0 != null) {
						if (a14_0 != null) {
							Object value = a14_0;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NODES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_1, a14_0, true);
						copyLocalizationInfos(a14_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[42]);
			}
			
			a15 = ';' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[43]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[44]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[45]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[46]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[47]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[48]);
	}
	
	(
		(
			a16 = 'edge' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[49]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[50]);
			}
			
			(
				a17_0 = parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						startIncompleteElement(element);
						// initialize boolean attribute
						{
							Object value = false;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
						}
					}
					if (a17_0 != null) {
						if (a17_0 != null) {
							Object value = a17_0;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__EDGES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_1, a17_0, true);
						copyLocalizationInfos(a17_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[51]);
			}
			
			a18 = ';' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[52]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[53]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[54]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[55]);
	}
	
	a19 = '}' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[56]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[57]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_Role returns [edu.ustb.sei.rmg.structuralrmg.Role element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_2_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[58]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[59]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_Bound returns [edu.ustb.sei.rmg.structuralrmg.Bound element = null]
@init{
}
:
	a0 = 'set' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[60]);
	}
	
	a1 = '[' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[61]);
	}
	
	(
		a2 = NUMBER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[62]);
	}
	
	a3 = '..' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[63]);
	}
	
	(
		a4 = NUMBER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[64]);
	}
	
	a5 = ']' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[65]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[66]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode returns [edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element = null]
@init{
}
:
	(
		(
			a0_0 = parse_edu_ustb_sei_rmg_structuralrmg_Bound			{
				if (terminateParsing) {
					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__BOUND), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[67]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[68]);
	}
	
	a2 = ':' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[69]);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateNodeTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[70]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[71]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[72]);
	}
	
	(
		(
			a4 = '[' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[73]);
			}
			
			(
				a5_0 = parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_1, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[74]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[75]);
			}
			
			(
				(
					a6 = ',' {
						if (element == null) {
							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[76]);
					}
					
					(
						a7_0 = parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding						{
							if (terminateParsing) {
								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
							}
							if (element == null) {
								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
								startIncompleteElement(element);
							}
							if (a7_0 != null) {
								if (a7_0 != null) {
									Object value = a7_0;
									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_2_0_0_1, a7_0, true);
								copyLocalizationInfos(a7_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[77]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[78]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[79]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[80]);
			}
			
			a8 = ']' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_3, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[81]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[82]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[83]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[84]);
	}
	
	(
		(
			a9 = 'plays' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[85]);
			}
			
			(
				a10 = TEXT				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
						startIncompleteElement(element);
					}
					if (a10 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateNode, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[86]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[87]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding returns [edu.ustb.sei.rmg.structuralrmg.AttributeBinding element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[88]);
	}
	
	a1 = ':' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[89]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EAttribute proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAttributeBindingAttributeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[91]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode returns [edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element = null]
@init{
}
:
	(
		(
			a0_0 = parse_edu_ustb_sei_rmg_structuralrmg_Bound			{
				if (terminateParsing) {
					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
					startIncompleteElement(element);
					// initialize enumeration attribute
					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__BOUND), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[92]);
	}
	
	(
		(
			a1 = 'shared' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
					startIncompleteElement(element);
					// initialize enumeration attribute
					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
				// set value of enumeration attribute
				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.SHARED_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				completedElement(value, false);
			}
			|			a2 = 'reversed' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
					startIncompleteElement(element);
					// initialize enumeration attribute
					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
				// set value of enumeration attribute
				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.REVERSED_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				completedElement(value, false);
			}
		)?	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[93]);
	}
	
	(
		a5 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
				startIncompleteElement(element);
				// initialize enumeration attribute
				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
			}
			if (a5 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[94]);
	}
	
	a6 = '@' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
			startIncompleteElement(element);
			// initialize enumeration attribute
			Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
			element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[95]);
	}
	
	(
		a7 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
				startIncompleteElement(element);
				// initialize enumeration attribute
				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
			}
			if (a7 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES, value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[96]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[97]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[98]);
	}
	
	(
		(
			a8 = ',' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
					startIncompleteElement(element);
					// initialize enumeration attribute
					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[99]);
			}
			
			(
				a9 = TEXT				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
						startIncompleteElement(element);
						// initialize enumeration attribute
						Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
					}
					if (a9 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a9).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[100]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[101]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[102]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[103]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[104]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[105]);
	}
	
	(
		(
			a10 = 'plays' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
					startIncompleteElement(element);
					// initialize enumeration attribute
					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[106]);
			}
			
			(
				a11 = TEXT				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
						startIncompleteElement(element);
						// initialize enumeration attribute
						Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
					}
					if (a11 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateNode, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[107]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[108]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[109]);
	}
	
	a1 = ':' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[110]);
	}
	
	(
		a2 = PATH		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EReference proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[111]);
	}
	
	a3 = '(' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[112]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeSourceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[113]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[114]);
	}
	
	(
		(
			a5 = '[' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[115]);
			}
			
			(
				a6 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
						startIncompleteElement(element);
					}
					if (a6 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[116]);
			}
			
			a7 = ']' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[117]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[118]);
	}
	
	a8 = ',' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[119]);
	}
	
	(
		a9 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
				startIncompleteElement(element);
			}
			if (a9 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a9).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_7, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[120]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[121]);
	}
	
	(
		(
			a10 = '[' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[122]);
			}
			
			(
				a11 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
						startIncompleteElement(element);
					}
					if (a11 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[123]);
			}
			
			a12 = ']' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[124]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[125]);
	}
	
	a13 = ')' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[126]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[127]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[128]);
	}
	
	(
		(
			a14_0 = parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding			{
				if (terminateParsing) {
					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
					startIncompleteElement(element);
				}
				if (a14_0 != null) {
					if (a14_0 != null) {
						Object value = a14_0;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__BINDING), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_10, a14_0, true);
					copyLocalizationInfos(a14_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[129]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
				startIncompleteElement(element);
				// initialize boolean attribute
				{
					Object value = false;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
				}
			}
			if (a0 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[130]);
	}
	
	a1 = ':' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[131]);
	}
	
	(
		a2 = PATH		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
				startIncompleteElement(element);
				// initialize boolean attribute
				{
					Object value = false;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
				}
			}
			if (a2 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EReference proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[132]);
	}
	
	a3 = 'in' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
			startIncompleteElement(element);
			// initialize boolean attribute
			{
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
			}
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[133]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[134]);
	}
	
	(
		(
			a4 = 'loop' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
					}
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_4, true, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
				// set value of boolean attribute
				Object value = true;
				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
				completedElement(value, false);
			}
		)?	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[135]);
	}
	
	(
		a7 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
				startIncompleteElement(element);
				// initialize boolean attribute
				{
					Object value = false;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
				}
			}
			if (a7 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getChainTemplateEdgeNodeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_5, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[136]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[137]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[138]);
	}
	
	(
		(
			a8_0 = parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding			{
				if (terminateParsing) {
					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
					startIncompleteElement(element);
					// initialize boolean attribute
					{
						Object value = false;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
					}
				}
				if (a8_0 != null) {
					if (a8_0 != null) {
						Object value = a8_0;
						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__BINDING), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_6, a8_0, true);
					copyLocalizationInfos(a8_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[139]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding returns [edu.ustb.sei.rmg.structuralrmg.EndpointBinding element = null]
@init{
}
:
	(
		(
			a0 = 'for' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[140]);
			}
			
			a1 = 'source' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[141]);
			}
			
			(
				(
					a2_0 = parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping					{
						if (terminateParsing) {
							throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
						}
						if (element == null) {
							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
							startIncompleteElement(element);
						}
						if (a2_0 != null) {
							if (a2_0 != null) {
								Object value = a2_0;
								addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS, value);
								completedElement(value, true);
							}
							collectHiddenTokens(element);
							retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_2, a2_0, true);
							copyLocalizationInfos(a2_0, element);
						}
					}
				)
				
			)+			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[142]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[143]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[144]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[145]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[146]);
	}
	
	(
		(
			a3 = 'for' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[147]);
			}
			
			a4 = 'target' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[148]);
			}
			
			(
				(
					a5_0 = parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping					{
						if (terminateParsing) {
							throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
						}
						if (element == null) {
							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
							startIncompleteElement(element);
						}
						if (a5_0 != null) {
							if (a5_0 != null) {
								Object value = a5_0;
								addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS, value);
								completedElement(value, true);
							}
							collectHiddenTokens(element);
							retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_2, a5_0, true);
							copyLocalizationInfos(a5_0, element);
						}
					}
				)
				
			)+			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[149]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[150]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[151]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping returns [edu.ustb.sei.rmg.structuralrmg.BindingMapping element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingTemplateReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_0, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[152]);
	}
	
	a1 = '=>' {
		if (element == null) {
			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[153]);
	}
	
	(
		(
			(
				a2 = TEXT				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
						startIncompleteElement(element);
					}
					if (a2 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_0, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[154]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[155]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[156]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[157]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[158]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[159]);
			}
			
			(
				(
					a3 = ',' {
						if (element == null) {
							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[160]);
					}
					
					(
						a4 = TEXT						
						{
							if (terminateParsing) {
								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
							}
							if (element == null) {
								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
								startIncompleteElement(element);
							}
							if (a4 != null) {
								edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
								tokenResolver.setOptions(getOptions());
								edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
								}
								String resolved = (String) resolvedObject;
								edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
								collectHiddenTokens(element);
								registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), resolved, proxy);
								if (proxy != null) {
									Object value = proxy;
									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_1, proxy, true);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[161]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[162]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[163]);
						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[164]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[165]);
						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[166]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[167]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[168]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[169]);
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[170]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[171]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[172]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[173]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[174]);
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[175]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[176]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[177]);
	}
	
	(
		(
			a5 = '[' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[178]);
			}
			
			(
				a6 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
						startIncompleteElement(element);
					}
					if (a6 != null) {
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[179]);
			}
			
			a7 = ']' {
				if (element == null) {
					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[180]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[181]);
				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[182]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[183]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[184]);
		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[185]);
	}
	
;

parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode returns [edu.ustb.sei.rmg.structuralrmg.TemplateNode element = null]
:
	c0 = parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.TemplateEdge element = null]
:
	c0 = parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

TEXT:
	(('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-')*)
;
LINEBREAK:
	(('\r\n'|'\r'|'\n'))
	{ _channel = 99; }
;
WHITESPACE:
	((' '|'\t'|'\f'))
	{ _channel = 99; }
;
URI:
	('<'('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-'|':'|'/'|'\\'|'\.')+'>')
;
PATH:
	(('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-')*'::'('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'0'..'9'|'_'|'-')*)
;
NUMBER:
	('-'?('0'..'9')+)
;
ML_COMMENT:
	( '/*'.*'*/')
	{ _channel = 99; }
;

