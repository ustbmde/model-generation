/**
 */
package edu.ustb.sei.rmg.structuralrmg.util;

import edu.ustb.sei.rmg.structuralrmg.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage
 * @generated
 */
public class StructuralRmgSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StructuralRmgPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralRmgSwitch() {
		if (modelPackage == null) {
			modelPackage = StructuralRmgPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StructuralRmgPackage.TEMPLATE_MODEL: {
				TemplateModel templateModel = (TemplateModel)theEObject;
				T result = caseTemplateModel(templateModel);
				if (result == null) result = caseNamedElement(templateModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.TEMPLATE: {
				Template template = (Template)theEObject;
				T result = caseTemplate(template);
				if (result == null) result = caseNamedElement(template);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.TEMPLATE_NODE: {
				TemplateNode templateNode = (TemplateNode)theEObject;
				T result = caseTemplateNode(templateNode);
				if (result == null) result = caseNamedElement(templateNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.BOUND: {
				Bound bound = (Bound)theEObject;
				T result = caseBound(bound);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.TEMPLATE_EDGE: {
				TemplateEdge templateEdge = (TemplateEdge)theEObject;
				T result = caseTemplateEdge(templateEdge);
				if (result == null) result = caseNamedElement(templateEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE: {
				PrimitiveTemplateEdge primitiveTemplateEdge = (PrimitiveTemplateEdge)theEObject;
				T result = casePrimitiveTemplateEdge(primitiveTemplateEdge);
				if (result == null) result = caseTemplateEdge(primitiveTemplateEdge);
				if (result == null) result = caseNamedElement(primitiveTemplateEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.CHAIN_TEMPLATE_EDGE: {
				ChainTemplateEdge chainTemplateEdge = (ChainTemplateEdge)theEObject;
				T result = caseChainTemplateEdge(chainTemplateEdge);
				if (result == null) result = caseTemplateEdge(chainTemplateEdge);
				if (result == null) result = caseNamedElement(chainTemplateEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE: {
				PrimitiveTemplateNode primitiveTemplateNode = (PrimitiveTemplateNode)theEObject;
				T result = casePrimitiveTemplateNode(primitiveTemplateNode);
				if (result == null) result = caseTemplateNode(primitiveTemplateNode);
				if (result == null) result = caseNamedElement(primitiveTemplateNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.ATTRIBUTE_BINDING: {
				AttributeBinding attributeBinding = (AttributeBinding)theEObject;
				T result = caseAttributeBinding(attributeBinding);
				if (result == null) result = caseNamedElement(attributeBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.NESTED_TEMPLATE_NODE: {
				NestedTemplateNode nestedTemplateNode = (NestedTemplateNode)theEObject;
				T result = caseNestedTemplateNode(nestedTemplateNode);
				if (result == null) result = caseTemplateNode(nestedTemplateNode);
				if (result == null) result = caseNamedElement(nestedTemplateNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.ROLE: {
				Role role = (Role)theEObject;
				T result = caseRole(role);
				if (result == null) result = caseNamedElement(role);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.BINDING_MAPPING: {
				BindingMapping bindingMapping = (BindingMapping)theEObject;
				T result = caseBindingMapping(bindingMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StructuralRmgPackage.ENDPOINT_BINDING: {
				EndpointBinding endpointBinding = (EndpointBinding)theEObject;
				T result = caseEndpointBinding(endpointBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTemplateModel(TemplateModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTemplate(Template object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTemplateNode(TemplateNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBound(Bound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTemplateEdge(TemplateEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Template Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Template Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitiveTemplateEdge(PrimitiveTemplateEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Chain Template Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Chain Template Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChainTemplateEdge(ChainTemplateEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Template Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Template Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitiveTemplateNode(PrimitiveTemplateNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeBinding(AttributeBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nested Template Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nested Template Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNestedTemplateNode(NestedTemplateNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRole(Role object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binding Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBindingMapping(BindingMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Endpoint Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Endpoint Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndpointBinding(EndpointBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //StructuralRmgSwitch
