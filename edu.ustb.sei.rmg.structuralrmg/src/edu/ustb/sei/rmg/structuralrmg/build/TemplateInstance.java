package edu.ustb.sei.rmg.structuralrmg.build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.structuralrmg.BindingMapping;
import edu.ustb.sei.rmg.structuralrmg.EndpointBinding;
import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;

class TemplateInstance {
	private EObject element;
	
	public TemplateInstance(EObject element, Template template) {
		this(element,template,false);
	}
	
	public TemplateInstance(EObject element, Template template, boolean concurrency) {
		super();
		this.element = element;
		this.template = template;
		nodeInstanceMap = concurrency ? new ConcurrentHashMap<TemplateNode,Object>() : new HashMap<TemplateNode,Object>();
	}

	public void putNodeInstance(TemplateNode n, Object i) {
		nodeInstanceMap.put(n, i);
	}

	private Template template;
	private Map<TemplateNode, Object> nodeInstanceMap;
	
	public Object getNodeInstance(TemplateNode node) {
		return nodeInstanceMap.get(node);
	}
	
	public List<Object> getRoleInstance(EList<Role> eList, Integer pos) {
		List<Object> res = new ArrayList<Object>();
		
		for(Role r : eList) {//role order
			boolean flag = false;
			for(TemplateNode node : template.getAllNodes()) {
				if(flag) break; // node order
				
				if(r==node.getRole()) {
					Object o = getNodeInstance(node);
					if(o==null) continue;
					if(o instanceof List) {
						if(((List) o).size()>0) {
							List<?> list = (List<?>)o;
							int id = 0;
							
							if(pos==null) ProduceModel.random.nextInt(list.size());
							else {
								if(pos<0 || pos>=list.size()) id = list.size()-1;
								else id = pos;
							}
							res.add(list.get(id));
							flag = true;
						} else {
							flag = false;
						}
						
					} else {
						res.add(o);
						flag = true;
					}
				}
			}
			if(flag) break;
		}
//		for(TemplateNode node : template.getNodes()) {
//			if(eList.contains(node.getRole())) {
//				Object o = getNodeInstance(node);
//				
//				if(o instanceof List) {
//					List<?> list = (List<?>)o;
//					int id = 0;
//					
//					if(pos==null) ProduceModel.random.nextInt(list.size());
//					else {
//						if(pos==-1 || pos>=list.size()) id = list.size()-1;
//						else id = pos;
//					}
//					res.add(list.get(id));
//				} else {
//					res.add(o);
//				}
//			}
//		}
		return res;
	}
	
	public List<Object> getObject(Template t, TemplateEdge e,boolean source) {
		List<Object> olist = getRole(t,e,this,source);
		List<Object> rlist = new ArrayList<Object>();
		
		if(olist==null) return rlist;
		
		for(Object o : olist) {
			if(o instanceof EObject) {
				rlist.add(o);
			}
			else if(o instanceof TemplateInstance) {
				rlist.addAll(((TemplateInstance) o).getObject(t, e, source));
			} 		
		}
		return rlist;
		
	}
	
	static List<Object> getRole(Template host, TemplateEdge e, TemplateInstance ti, boolean source) {
//		for(EndpointBinding b : host.getBindings()) {
		EndpointBinding b = e.getBinding();
			if(b==null) return null;
			if(source) {
				for(BindingMapping m : b.getSourceMappings()) {
					if(m.getTemplate().isSuperOf(ti.template))
						return ti.getRoleInstance(m.getRoles(),m.getPosition());
				}
			} else {
				for(BindingMapping m : b.getTargetMappings()) {
					if(m.getTemplate().isSuperOf(ti.template))
						return ti.getRoleInstance(m.getRoles(),m.getPosition());
				}
			}
//		}
		return null;
	}
}