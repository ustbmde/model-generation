package edu.ustb.sei.rmg.random.condition;

import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.random.RandomUtil;

public class StringConcatCondition extends Condition<String> {
	public List<Condition<String>> list;

	@Override
	public String getValue(RandomUtil r, Object... extra){
		try {
			StringBuilder builder = new StringBuilder();
			for(Condition<String> f : list) {
				String string = f.getValue(r, extra).toString();
				builder.append(string);
			}
			return builder.toString();
		} catch (ValueLazyGenerationRequest le) {
			throw new ValueLazyGenerationRequest(le, this, (EObject)extra[0], (EAttribute)extra[1]);
		} catch(FeatureConditionError fe) {
			throw new FeatureConditionError(fe,this);
		}
	}
}
