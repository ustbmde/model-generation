package edu.ustb.sei.rmg.random.condition;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.random.RandomUtil;
import edu.ustb.sei.rmg.util.Pair;

public class ConditionList<T> extends Condition<T> {
	public List<Condition<T>> list = new ArrayList<Condition<T>>();
	public List<InevitableCondition<T>> inevitableConditionList = new ArrayList<InevitableCondition<T>>();
	private Pair<Condition<T>,Double>[] pairArray = null;
	
	@SuppressWarnings("unchecked")
	public boolean recover() {
		pairArray = (Pair<Condition<T>, Double>[]) new Pair<?,?>[list.size()];
		int i = 0;
		double sum = 0.0;
		int unSpecified = 0;
		for(Condition<T> f : list) {
			pairArray[i] = new Pair<Condition<T>, Double>(f,f.possibility);
			
			if(f.possibility==null) {
				unSpecified++;
			} else {
				sum += f.possibility;
			}
			i++;
		}
		
		if(unSpecified != 0) {
			double rem = 1.0 - sum;
			if(rem<0) 
				return false;
			double avg = rem/unSpecified;
			for(Pair<Condition<T>, Double> p : pairArray) {
				if(p.second==null)
					p.second = avg;
			}
		}
		
		return true;
	}


	@Override
	public T getValue(RandomUtil r, Object... extra) {
		boolean inevitable = false;
		int[] pair = null;
		int size = inevitableConditionList.size();
		if(extra.length>=4) {
			pair = (int[])extra[3];
			
			if(pair[0]<0||size==0)
				inevitable = false;
			else {
				
				double p = (double)(size-pair[1])/(double)pair[0];
				double randomDouble = r.randomDouble();
				inevitable = randomDouble<p;

				System.out.println(extra[0]);
				System.out.println(pair[0]+","+pair[1]);
				System.out.println(p+","+randomDouble);
			}			
		}
		
		if(inevitable) {
			Condition<T> t = inevitableConditionList.get(pair[1]);
			pair[1]++;
			System.out.println("inevitable");
			return t.getValue(r, extra);
		} else {
			if(pairArray==null) {
				recover();
			}
			
			Condition<T> c = r.randomDiscrete(pairArray);
			try {
				return c.getValue(r, extra);
			} catch (FeatureConditionError ce) {
				if(list.size()==1) {
					throw new FeatureConditionError(ce, this);
				} else {
					System.out.println("try another branch!!");
					return getValue(r,extra); // try other branches
				}
			}		
		}
	}


	@Override
	public Object[] getGenerationBound() throws BoundSpecException {
		if(list.size()==1)
			return list.get(0).getGenerationBound();
		throw new BoundSpecException(this);
	}
	
	
}
