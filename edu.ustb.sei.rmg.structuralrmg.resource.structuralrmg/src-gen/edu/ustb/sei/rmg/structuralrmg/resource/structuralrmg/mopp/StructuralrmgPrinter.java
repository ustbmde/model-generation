/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgPrinter implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextPrinter {
	
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource;
	
	private java.util.Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public StructuralrmgPrinter(java.io.OutputStream outputStream, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.TemplateModel) {
			print_edu_ustb_sei_rmg_structuralrmg_TemplateModel((edu.ustb.sei.rmg.structuralrmg.TemplateModel) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.Template) {
			print_edu_ustb_sei_rmg_structuralrmg_Template((edu.ustb.sei.rmg.structuralrmg.Template) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.Role) {
			print_edu_ustb_sei_rmg_structuralrmg_Role((edu.ustb.sei.rmg.structuralrmg.Role) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.Bound) {
			print_edu_ustb_sei_rmg_structuralrmg_Bound((edu.ustb.sei.rmg.structuralrmg.Bound) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode) {
			print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode((edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.AttributeBinding) {
			print_edu_ustb_sei_rmg_structuralrmg_AttributeBinding((edu.ustb.sei.rmg.structuralrmg.AttributeBinding) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode) {
			print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode((edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge) {
			print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge((edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge) {
			print_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge((edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.EndpointBinding) {
			print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding((edu.ustb.sei.rmg.structuralrmg.EndpointBinding) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.rmg.structuralrmg.BindingMapping) {
			print_edu_ustb_sei_rmg_structuralrmg_BindingMapping((edu.ustb.sei.rmg.structuralrmg.BindingMapping) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgReferenceResolverSwitch getReferenceResolverSwitch() {
		return (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgReferenceResolverSwitch) new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgProblem(errorMessage, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType.PRINT_PROBLEM, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.OutputStreamWriter(new java.io.BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_TemplateModel(edu.ustb.sei.rmg.structuralrmg.TemplateModel element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__TEMPLATES));
		printCountingMap.put("templates", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES));
		printCountingMap.put("packages", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("model");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_TemplateModel_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_TemplateModel_1(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_TemplateModel_0(edu.ustb.sei.rmg.structuralrmg.TemplateModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("import");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("packages");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("URI");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateModelPackagesReferenceResolver().deResolve((org.eclipse.emf.ecore.EPackage) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES), element));
				out.print(" ");
			}
			printCountingMap.put("packages", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_TemplateModel_1(edu.ustb.sei.rmg.structuralrmg.TemplateModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("templates");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__TEMPLATES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("templates", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES));
		printCountingMap.put("roles", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NODES));
		printCountingMap.put("nodes", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__EDGES));
		printCountingMap.put("edges", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT));
		printCountingMap.put("abstract", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE));
		printCountingMap.put("superTemplate", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("abstract");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT));
			if (o != null) {
			}
			printCountingMap.put("abstract", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("template");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_Template_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_Template_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_Template_2(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_Template_3(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template_0(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("roles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("roles", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_Template_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template_0_0(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("roles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("roles", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template_1(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("extends");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("superTemplate");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateSuperTemplateReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Template) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), element));
				out.print(" ");
			}
			printCountingMap.put("superTemplate", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template_2(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("node");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("nodes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NODES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("nodes", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Template_3(edu.ustb.sei.rmg.structuralrmg.Template element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("edge");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("edges");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__EDGES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("edges", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Role(edu.ustb.sei.rmg.structuralrmg.Role element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_Bound(edu.ustb.sei.rmg.structuralrmg.Bound element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN));
		printCountingMap.put("min", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX));
		printCountingMap.put("max", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("set");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("min");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN), element));
				out.print(" ");
			}
			printCountingMap.put("min", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("max");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX), element));
				out.print(" ");
			}
			printCountingMap.put("max", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(5);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE));
		printCountingMap.put("role", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__BOUND));
		printCountingMap.put("bound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES));
		printCountingMap.put("attributes", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("bound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__BOUND));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("bound", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateNodeTypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClass) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_0(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("attributes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("attributes", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_0_0(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("attributes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("attributes", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_1(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("plays");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("role");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), element));
				out.print(" ");
			}
			printCountingMap.put("role", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_AttributeBinding(edu.ustb.sei.rmg.structuralrmg.AttributeBinding element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE));
		printCountingMap.put("attribute", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("attribute");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAttributeBindingAttributeReferenceResolver().deResolve((org.eclipse.emf.ecore.EAttribute) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), element));
				out.print(" ");
			}
			printCountingMap.put("attribute", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode(edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(5);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE));
		printCountingMap.put("role", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__BOUND));
		printCountingMap.put("bound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES));
		printCountingMap.put("templates", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT));
		printCountingMap.put("sort", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("bound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__BOUND));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("bound", count - 1);
		}
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("sort");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT));
			if (o != null) {
			}
			printCountingMap.put("sort", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("templates");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Template) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), element));
				out.print(" ");
			}
			printCountingMap.put("templates", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_0(edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("templates");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Template) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), element));
				out.print(" ");
			}
			printCountingMap.put("templates", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_1(edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("plays");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("role");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), element));
				out.print(" ");
			}
			printCountingMap.put("role", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(7);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE));
		printCountingMap.put("reference", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__BINDING));
		printCountingMap.put("binding", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE));
		printCountingMap.put("source", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET));
		printCountingMap.put("target", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS));
		printCountingMap.put("sourcePos", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS));
		printCountingMap.put("targetPos", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("reference");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("PATH");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver().deResolve((org.eclipse.emf.ecore.EReference) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), element));
				out.print(" ");
			}
			printCountingMap.put("reference", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("source");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeSourceReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.TemplateNode) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), element));
				out.print(" ");
			}
			printCountingMap.put("source", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("target");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeTargetReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.TemplateNode) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), element));
				out.print(" ");
			}
			printCountingMap.put("target", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("binding");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__BINDING));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("binding", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_0(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("sourcePos");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS), element));
				out.print(" ");
			}
			printCountingMap.put("sourcePos", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_1(edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("targetPos");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS), element));
				out.print(" ");
			}
			printCountingMap.put("targetPos", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge(edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(5);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE));
		printCountingMap.put("reference", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__BINDING));
		printCountingMap.put("binding", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE));
		printCountingMap.put("node", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND));
		printCountingMap.put("round", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("reference");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("PATH");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver().deResolve((org.eclipse.emf.ecore.EReference) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), element));
				out.print(" ");
			}
			printCountingMap.put("reference", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("in");
		out.print(" ");
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("round");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND));
			if (o != null) {
			}
			printCountingMap.put("round", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("node");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getChainTemplateEdgeNodeReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.TemplateNode) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), element));
				out.print(" ");
			}
			printCountingMap.put("node", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("binding");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__BINDING));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("binding", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding(edu.ustb.sei.rmg.structuralrmg.EndpointBinding element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__EDGE));
		printCountingMap.put("edge", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS));
		printCountingMap.put("sourceMappings", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS));
		printCountingMap.put("targetMappings", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_0(edu.ustb.sei.rmg.structuralrmg.EndpointBinding element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("for");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("source");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceMappings");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceMappings", 0);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_1(edu.ustb.sei.rmg.structuralrmg.EndpointBinding element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("for");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("target");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("targetMappings");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("targetMappings", 0);
		}
	}
	
	
	public void print_edu_ustb_sei_rmg_structuralrmg_BindingMapping(edu.ustb.sei.rmg.structuralrmg.BindingMapping element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES));
		printCountingMap.put("roles", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE));
		printCountingMap.put("template", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION));
		printCountingMap.put("position", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("template");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingTemplateReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Template) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), element));
				out.print(" ");
			}
			printCountingMap.put("template", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("=>");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_0(element, localtab, out, printCountingMap);
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_0(edu.ustb.sei.rmg.structuralrmg.BindingMapping element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("roles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), element));
				out.print(" ");
			}
			printCountingMap.put("roles", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_0_0(edu.ustb.sei.rmg.structuralrmg.BindingMapping element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("roles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver().deResolve((edu.ustb.sei.rmg.structuralrmg.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES)), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), element));
				out.print(" ");
			}
			printCountingMap.put("roles", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_rmg_structuralrmg_BindingMapping_1(edu.ustb.sei.rmg.structuralrmg.BindingMapping element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("position");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION));
			if (o != null) {
				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION), element));
				out.print(" ");
			}
			printCountingMap.put("position", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
}
