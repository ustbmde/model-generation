package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class LongDistributionFunction extends DistributionFunction<Long> {

	@Override
	public Long getValue(RandomUtil r, Object... extra) {
		switch(name){
		case "geom":
			return r.geometric(parameters[0]);
		case "pois":
			return r.poisson(parameters[0]);
		}
		return 0L;
	}

}
