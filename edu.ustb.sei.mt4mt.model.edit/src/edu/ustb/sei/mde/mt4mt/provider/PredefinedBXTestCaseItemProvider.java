/**
 */
package edu.ustb.sei.mde.mt4mt.provider;


import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mt4mt.PredefinedBXTestCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PredefinedBXTestCaseItemProvider extends PredefinedTestCaseItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PredefinedBXTestCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFwdLauncherPropertyDescriptor(object);
			addFwdSourcePropertyDescriptor(object);
			addFwdViewPropertyDescriptor(object);
			addBwdLauncherPropertyDescriptor(object);
			addBwdSourcePropertyDescriptor(object);
			addBwdViewPropertyDescriptor(object);
			addBwdUpdatedSourcePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Fwd Launcher feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFwdLauncherPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_fwdLauncher_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_fwdLauncher_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Fwd Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFwdSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_fwdSource_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_fwdSource_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__FWD_SOURCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Fwd View feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFwdViewPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_fwdView_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_fwdView_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__FWD_VIEW,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bwd Launcher feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBwdLauncherPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_bwdLauncher_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_bwdLauncher_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bwd Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBwdSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_bwdSource_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_bwdSource_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__BWD_SOURCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bwd View feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBwdViewPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_bwdView_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_bwdView_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__BWD_VIEW,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bwd Updated Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBwdUpdatedSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PredefinedBXTestCase_bwdUpdatedSource_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PredefinedBXTestCase_bwdUpdatedSource_feature", "_UI_PredefinedBXTestCase_type"),
				 MT4MTPackage.Literals.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		PredefinedBXTestCase predefinedBXTestCase = (PredefinedBXTestCase)object;
		return getString("_UI_PredefinedBXTestCase_type") + " " + predefinedBXTestCase.getId();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(PredefinedBXTestCase.class)) {
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_LAUNCHER:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_SOURCE:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__FWD_VIEW:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_LAUNCHER:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_SOURCE:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_VIEW:
			case MT4MTPackage.PREDEFINED_BX_TEST_CASE__BWD_UPDATED_SOURCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
