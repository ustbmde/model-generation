// $ANTLR 3.4

	package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class StructuralrmgParser extends StructuralrmgANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "LINEBREAK", "ML_COMMENT", "NUMBER", "PATH", "TEXT", "URI", "WHITESPACE", "'('", "')'", "','", "'..'", "':'", "';'", "'=>'", "'@'", "'['", "']'", "'abstract'", "'edge'", "'extends'", "'for'", "'import'", "'in'", "'loop'", "'model'", "'node'", "'plays'", "'reversed'", "'set'", "'shared'", "'source'", "'target'", "'template'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int LINEBREAK=4;
    public static final int ML_COMMENT=5;
    public static final int NUMBER=6;
    public static final int PATH=7;
    public static final int TEXT=8;
    public static final int URI=9;
    public static final int WHITESPACE=10;

    // delegates
    public StructuralrmgANTLRParserBase[] getDelegates() {
        return new StructuralrmgANTLRParserBase[] {};
    }

    // delegators


    public StructuralrmgParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public StructuralrmgParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(45 + 1);
         

    }

    public String[] getTokenNames() { return StructuralrmgParser.tokenNames; }
    public String getGrammarFileName() { return "Structuralrmg.g"; }


    	private edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> expectedElements = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
    			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgProblem() {
    					public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity getSeverity() {
    						return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemSeverity.ERROR;
    					}
    					public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType getType() {
    						return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement terminal = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFollowSetProvider.TERMINALS[terminalID];
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[] containmentFeatures = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgFollowSetProvider.LINKS[ids[i]];
    		}
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace containmentTrace = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElement = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
    			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
    				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
    			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
    				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>() {
    			public boolean execute(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
    				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new StructuralrmgParser(new org.antlr.runtime3_4_0.CommonTokenStream(new StructuralrmgLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new StructuralrmgParser(new org.antlr.runtime3_4_0.CommonTokenStream(new StructuralrmgLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public StructuralrmgParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((StructuralrmgLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((StructuralrmgLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.TemplateModel.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Template.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_Template();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Role.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_Role();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.Bound.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_Bound();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.AttributeBinding.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.EndpointBinding.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.rmg.structuralrmg.BindingMapping.class) {
    				return parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping();
    			}
    		}
    		throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>>();
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParseResult parseResult = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
    		java.util.List<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal> newFollowSet = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 109;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.util.StructuralrmgPair<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContainedFeature[]> newFollowerPair : newFollowers) {
    							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace containmentTrace = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgContainmentTrace(null, newFollowerPair.getRight());
    							edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal newFollowTerminal = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Structuralrmg.g:529:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        edu.ustb.sei.rmg.structuralrmg.TemplateModel c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Structuralrmg.g:530:2: ( (c0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel ) EOF )
            // Structuralrmg.g:531:2: (c0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[0]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Structuralrmg.g:536:2: (c0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel )
            // Structuralrmg.g:537:3: c0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel
            {
            pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel_in_start82);
            c0=parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel"
    // Structuralrmg.g:545:1: parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel returns [edu.ustb.sei.rmg.structuralrmg.TemplateModel element = null] : a0= 'model' (a1= TEXT ) ( (a2= 'import' (a3= URI ) ) )* ( ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) ) )* ;
    public final edu.ustb.sei.rmg.structuralrmg.TemplateModel parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.TemplateModel element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        edu.ustb.sei.rmg.structuralrmg.Template a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Structuralrmg.g:548:2: (a0= 'model' (a1= TEXT ) ( (a2= 'import' (a3= URI ) ) )* ( ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) ) )* )
            // Structuralrmg.g:549:2: a0= 'model' (a1= TEXT ) ( (a2= 'import' (a3= URI ) ) )* ( ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) ) )*
            {
            a0=(Token)match(input,28,FOLLOW_28_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[1]);
            	}

            // Structuralrmg.g:563:2: (a1= TEXT )
            // Structuralrmg.g:564:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel133); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[3]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[4]);
            	}

            // Structuralrmg.g:601:2: ( (a2= 'import' (a3= URI ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==25) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Structuralrmg.g:602:3: (a2= 'import' (a3= URI ) )
            	    {
            	    // Structuralrmg.g:602:3: (a2= 'import' (a3= URI ) )
            	    // Structuralrmg.g:603:4: a2= 'import' (a3= URI )
            	    {
            	    a2=(Token)match(input,25,FOLLOW_25_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel163); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[5]);
            	    			}

            	    // Structuralrmg.g:617:4: (a3= URI )
            	    // Structuralrmg.g:618:5: a3= URI
            	    {
            	    a3=(Token)match(input,URI,FOLLOW_URI_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel189); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3 != null) {
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("URI");
            	    						tokenResolver.setOptions(getOptions());
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            	    						}
            	    						String resolved = (String) resolvedObject;
            	    						org.eclipse.emf.ecore.EPackage proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEPackage();
            	    						collectHiddenTokens(element);
            	    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateModel, org.eclipse.emf.ecore.EPackage>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateModelPackagesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES), resolved, proxy);
            	    						if (proxy != null) {
            	    							Object value = proxy;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__PACKAGES, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_3_0_0_1, proxy, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[6]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[7]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[8]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[9]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[10]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[11]);
            	}

            // Structuralrmg.g:668:2: ( ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==21||LA2_0==36) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Structuralrmg.g:669:3: ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) )
            	    {
            	    // Structuralrmg.g:669:3: ( (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template ) )
            	    // Structuralrmg.g:670:4: (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template )
            	    {
            	    // Structuralrmg.g:670:4: (a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template )
            	    // Structuralrmg.g:671:5: a4_0= parse_edu_ustb_sei_rmg_structuralrmg_Template
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Template_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel250);
            	    a4_0=parse_edu_ustb_sei_rmg_structuralrmg_Template();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplateModel();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4_0 != null) {
            	    						if (a4_0 != null) {
            	    							Object value = a4_0;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE_MODEL__TEMPLATES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_0_0_0_4_0_0_0, a4_0, true);
            	    						copyLocalizationInfos(a4_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[12]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[13]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[15]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_Template"
    // Structuralrmg.g:707:1: parse_edu_ustb_sei_rmg_structuralrmg_Template returns [edu.ustb.sei.rmg.structuralrmg.Template element = null] : ( (a0= 'abstract' )? ) a3= 'template' (a4= TEXT ) ( (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' ) )? ( (a10= 'extends' (a11= TEXT ) ) )? a12= '{' ( (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' ) )* ( (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' ) )* a19= '}' ;
    public final edu.ustb.sei.rmg.structuralrmg.Template parse_edu_ustb_sei_rmg_structuralrmg_Template() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.Template element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_Template_StartIndex = input.index();

        Token a0=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a7=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a15=null;
        Token a16=null;
        Token a18=null;
        Token a19=null;
        edu.ustb.sei.rmg.structuralrmg.Role a6_0 =null;

        edu.ustb.sei.rmg.structuralrmg.Role a8_0 =null;

        edu.ustb.sei.rmg.structuralrmg.TemplateNode a14_0 =null;

        edu.ustb.sei.rmg.structuralrmg.TemplateEdge a17_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Structuralrmg.g:710:2: ( ( (a0= 'abstract' )? ) a3= 'template' (a4= TEXT ) ( (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' ) )? ( (a10= 'extends' (a11= TEXT ) ) )? a12= '{' ( (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' ) )* ( (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' ) )* a19= '}' )
            // Structuralrmg.g:711:2: ( (a0= 'abstract' )? ) a3= 'template' (a4= TEXT ) ( (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' ) )? ( (a10= 'extends' (a11= TEXT ) ) )? a12= '{' ( (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' ) )* ( (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' ) )* a19= '}'
            {
            // Structuralrmg.g:711:2: ( (a0= 'abstract' )? )
            // Structuralrmg.g:712:3: (a0= 'abstract' )?
            {
            // Structuralrmg.g:712:3: (a0= 'abstract' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==21) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // Structuralrmg.g:713:4: a0= 'abstract'
                    {
                    a0=(Token)match(input,21,FOLLOW_21_in_parse_edu_ustb_sei_rmg_structuralrmg_Template315); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_0, true, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    				// set value of boolean attribute
                    				Object value = true;
                    				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[16]);
            	}

            a3=(Token)match(input,36,FOLLOW_36_in_parse_edu_ustb_sei_rmg_structuralrmg_Template336); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[17]);
            	}

            // Structuralrmg.g:756:2: (a4= TEXT )
            // Structuralrmg.g:757:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Template354); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            				startIncompleteElement(element);
            				// initialize boolean attribute
            				{
            					Object value = false;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            				}
            			}
            			if (a4 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[18]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[19]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[20]);
            	}

            // Structuralrmg.g:799:2: ( (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==11) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // Structuralrmg.g:800:3: (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' )
                    {
                    // Structuralrmg.g:800:3: (a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')' )
                    // Structuralrmg.g:801:4: a5= '(' (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )* a9= ')'
                    {
                    a5=(Token)match(input,11,FOLLOW_11_in_parse_edu_ustb_sei_rmg_structuralrmg_Template384); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[21]);
                    			}

                    // Structuralrmg.g:820:4: (a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role )
                    // Structuralrmg.g:821:5: a6_0= parse_edu_ustb_sei_rmg_structuralrmg_Role
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Role_in_parse_edu_ustb_sei_rmg_structuralrmg_Template410);
                    a6_0=parse_edu_ustb_sei_rmg_structuralrmg_Role();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    						startIncompleteElement(element);
                    						// initialize boolean attribute
                    						{
                    							Object value = false;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    						}
                    					}
                    					if (a6_0 != null) {
                    						if (a6_0 != null) {
                    							Object value = a6_0;
                    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES, value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_1, a6_0, true);
                    						copyLocalizationInfos(a6_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[22]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[23]);
                    			}

                    // Structuralrmg.g:852:4: ( (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==13) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // Structuralrmg.g:853:5: (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) )
                    	    {
                    	    // Structuralrmg.g:853:5: (a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role ) )
                    	    // Structuralrmg.g:854:6: a7= ',' (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role )
                    	    {
                    	    a7=(Token)match(input,13,FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_Template451); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    	    							startIncompleteElement(element);
                    	    							// initialize boolean attribute
                    	    							{
                    	    								Object value = false;
                    	    								element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    	    							}
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_2_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[24]);
                    	    					}

                    	    // Structuralrmg.g:873:6: (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role )
                    	    // Structuralrmg.g:874:7: a8_0= parse_edu_ustb_sei_rmg_structuralrmg_Role
                    	    {
                    	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Role_in_parse_edu_ustb_sei_rmg_structuralrmg_Template485);
                    	    a8_0=parse_edu_ustb_sei_rmg_structuralrmg_Role();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    	    								startIncompleteElement(element);
                    	    								// initialize boolean attribute
                    	    								{
                    	    									Object value = false;
                    	    									element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    	    								}
                    	    							}
                    	    							if (a8_0 != null) {
                    	    								if (a8_0 != null) {
                    	    									Object value = a8_0;
                    	    									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ROLES, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_2_0_0_1, a8_0, true);
                    	    								copyLocalizationInfos(a8_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[25]);
                    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[26]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[27]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[28]);
                    			}

                    a9=(Token)match(input,12,FOLLOW_12_in_parse_edu_ustb_sei_rmg_structuralrmg_Template546); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_3_0_0_3, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[29]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[30]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[32]);
            	}

            // Structuralrmg.g:941:2: ( (a10= 'extends' (a11= TEXT ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==23) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // Structuralrmg.g:942:3: (a10= 'extends' (a11= TEXT ) )
                    {
                    // Structuralrmg.g:942:3: (a10= 'extends' (a11= TEXT ) )
                    // Structuralrmg.g:943:4: a10= 'extends' (a11= TEXT )
                    {
                    a10=(Token)match(input,23,FOLLOW_23_in_parse_edu_ustb_sei_rmg_structuralrmg_Template588); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[33]);
                    			}

                    // Structuralrmg.g:962:4: (a11= TEXT )
                    // Structuralrmg.g:963:5: a11= TEXT
                    {
                    a11=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Template614); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    						startIncompleteElement(element);
                    						// initialize boolean attribute
                    						{
                    							Object value = false;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
                    						}
                    					}
                    					if (a11 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.Template, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateSuperTemplateReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__SUPER_TEMPLATE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_4_0_0_1, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[34]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[35]);
            	}

            a12=(Token)match(input,37,FOLLOW_37_in_parse_edu_ustb_sei_rmg_structuralrmg_Template660); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[36]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[37]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[38]);
            	}

            // Structuralrmg.g:1035:2: ( (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==29) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // Structuralrmg.g:1036:3: (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' )
            	    {
            	    // Structuralrmg.g:1036:3: (a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';' )
            	    // Structuralrmg.g:1037:4: a13= 'node' (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode ) a15= ';'
            	    {
            	    a13=(Token)match(input,29,FOLLOW_29_in_parse_edu_ustb_sei_rmg_structuralrmg_Template683); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    					startIncompleteElement(element);
            	    					// initialize boolean attribute
            	    					{
            	    						Object value = false;
            	    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    					}
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[39]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[40]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[41]);
            	    			}

            	    // Structuralrmg.g:1058:4: (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode )
            	    // Structuralrmg.g:1059:5: a14_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_Template709);
            	    a14_0=parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    						startIncompleteElement(element);
            	    						// initialize boolean attribute
            	    						{
            	    							Object value = false;
            	    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    						}
            	    					}
            	    					if (a14_0 != null) {
            	    						if (a14_0 != null) {
            	    							Object value = a14_0;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__NODES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_1, a14_0, true);
            	    						copyLocalizationInfos(a14_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[42]);
            	    			}

            	    a15=(Token)match(input,16,FOLLOW_16_in_parse_edu_ustb_sei_rmg_structuralrmg_Template737); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    					startIncompleteElement(element);
            	    					// initialize boolean attribute
            	    					{
            	    						Object value = false;
            	    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    					}
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_7_0_0_2, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[43]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[44]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[45]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[46]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[47]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[48]);
            	}

            // Structuralrmg.g:1119:2: ( (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==22) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Structuralrmg.g:1120:3: (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' )
            	    {
            	    // Structuralrmg.g:1120:3: (a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';' )
            	    // Structuralrmg.g:1121:4: a16= 'edge' (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge ) a18= ';'
            	    {
            	    a16=(Token)match(input,22,FOLLOW_22_in_parse_edu_ustb_sei_rmg_structuralrmg_Template779); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    					startIncompleteElement(element);
            	    					// initialize boolean attribute
            	    					{
            	    						Object value = false;
            	    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    					}
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a16, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[49]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplate(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[50]);
            	    			}

            	    // Structuralrmg.g:1141:4: (a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge )
            	    // Structuralrmg.g:1142:5: a17_0= parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_Template805);
            	    a17_0=parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    						startIncompleteElement(element);
            	    						// initialize boolean attribute
            	    						{
            	    							Object value = false;
            	    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    						}
            	    					}
            	    					if (a17_0 != null) {
            	    						if (a17_0 != null) {
            	    							Object value = a17_0;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__EDGES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_1, a17_0, true);
            	    						copyLocalizationInfos(a17_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[51]);
            	    			}

            	    a18=(Token)match(input,16,FOLLOW_16_in_parse_edu_ustb_sei_rmg_structuralrmg_Template833); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    					startIncompleteElement(element);
            	    					// initialize boolean attribute
            	    					{
            	    						Object value = false;
            	    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            	    					}
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_8_0_0_2, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a18, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[52]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[53]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[54]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[55]);
            	}

            a19=(Token)match(input,38,FOLLOW_38_in_parse_edu_ustb_sei_rmg_structuralrmg_Template866); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.TEMPLATE__ABSTRACT), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_1_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a19, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[56]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getTemplateModel(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[57]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_edu_ustb_sei_rmg_structuralrmg_Template_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_Template"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_Role"
    // Structuralrmg.g:1222:1: parse_edu_ustb_sei_rmg_structuralrmg_Role returns [edu.ustb.sei.rmg.structuralrmg.Role element = null] : (a0= TEXT ) ;
    public final edu.ustb.sei.rmg.structuralrmg.Role parse_edu_ustb_sei_rmg_structuralrmg_Role() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.Role element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_Role_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Structuralrmg.g:1225:2: ( (a0= TEXT ) )
            // Structuralrmg.g:1226:2: (a0= TEXT )
            {
            // Structuralrmg.g:1226:2: (a0= TEXT )
            // Structuralrmg.g:1227:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Role899); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ROLE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_2_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[58]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[59]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_edu_ustb_sei_rmg_structuralrmg_Role_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_Role"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_Bound"
    // Structuralrmg.g:1265:1: parse_edu_ustb_sei_rmg_structuralrmg_Bound returns [edu.ustb.sei.rmg.structuralrmg.Bound element = null] : a0= 'set' a1= '[' (a2= NUMBER ) a3= '..' (a4= NUMBER ) a5= ']' ;
    public final edu.ustb.sei.rmg.structuralrmg.Bound parse_edu_ustb_sei_rmg_structuralrmg_Bound() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.Bound element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_Bound_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Structuralrmg.g:1268:2: (a0= 'set' a1= '[' (a2= NUMBER ) a3= '..' (a4= NUMBER ) a5= ']' )
            // Structuralrmg.g:1269:2: a0= 'set' a1= '[' (a2= NUMBER ) a3= '..' (a4= NUMBER ) a5= ']'
            {
            a0=(Token)match(input,32,FOLLOW_32_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound935); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[60]);
            	}

            a1=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound949); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[61]);
            	}

            // Structuralrmg.g:1297:2: (a2= NUMBER )
            // Structuralrmg.g:1298:3: a2= NUMBER
            {
            a2=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound967); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MIN), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[62]);
            	}

            a3=(Token)match(input,14,FOLLOW_14_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound988); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[63]);
            	}

            // Structuralrmg.g:1347:2: (a4= NUMBER )
            // Structuralrmg.g:1348:3: a4= NUMBER
            {
            a4=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound1006); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BOUND__MAX), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[64]);
            	}

            a5=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound1027); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBound();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_3_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[65]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[66]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_edu_ustb_sei_rmg_structuralrmg_Bound_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_Bound"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode"
    // Structuralrmg.g:1400:1: parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode returns [edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element = null] : ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? (a1= TEXT ) a2= ':' (a3= TEXT ) ( (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' ) )? ( (a9= 'plays' (a10= TEXT ) ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        edu.ustb.sei.rmg.structuralrmg.Bound a0_0 =null;

        edu.ustb.sei.rmg.structuralrmg.AttributeBinding a5_0 =null;

        edu.ustb.sei.rmg.structuralrmg.AttributeBinding a7_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Structuralrmg.g:1403:2: ( ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? (a1= TEXT ) a2= ':' (a3= TEXT ) ( (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' ) )? ( (a9= 'plays' (a10= TEXT ) ) )? )
            // Structuralrmg.g:1404:2: ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? (a1= TEXT ) a2= ':' (a3= TEXT ) ( (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' ) )? ( (a9= 'plays' (a10= TEXT ) ) )?
            {
            // Structuralrmg.g:1404:2: ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==32) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // Structuralrmg.g:1405:3: (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound )
                    {
                    // Structuralrmg.g:1405:3: (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound )
                    // Structuralrmg.g:1406:4: a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Bound_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1065);
                    a0_0=parse_edu_ustb_sei_rmg_structuralrmg_Bound();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    					startIncompleteElement(element);
                    				}
                    				if (a0_0 != null) {
                    					if (a0_0 != null) {
                    						Object value = a0_0;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__BOUND), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_0, a0_0, true);
                    					copyLocalizationInfos(a0_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[67]);
            	}

            // Structuralrmg.g:1432:2: (a1= TEXT )
            // Structuralrmg.g:1433:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1095); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[68]);
            	}

            a2=(Token)match(input,15,FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1116); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[69]);
            	}

            // Structuralrmg.g:1482:2: (a3= TEXT )
            // Structuralrmg.g:1483:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1134); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateNodeTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_3, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[70]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[71]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[72]);
            	}

            // Structuralrmg.g:1524:2: ( (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // Structuralrmg.g:1525:3: (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' )
                    {
                    // Structuralrmg.g:1525:3: (a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']' )
                    // Structuralrmg.g:1526:4: a4= '[' (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )* a8= ']'
                    {
                    a4=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1164); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[73]);
                    			}

                    // Structuralrmg.g:1540:4: (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding )
                    // Structuralrmg.g:1541:5: a5_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1190);
                    a5_0=parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    						startIncompleteElement(element);
                    					}
                    					if (a5_0 != null) {
                    						if (a5_0 != null) {
                    							Object value = a5_0;
                    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES, value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_1, a5_0, true);
                    						copyLocalizationInfos(a5_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[74]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[75]);
                    			}

                    // Structuralrmg.g:1567:4: ( (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==13) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // Structuralrmg.g:1568:5: (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) )
                    	    {
                    	    // Structuralrmg.g:1568:5: (a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding ) )
                    	    // Structuralrmg.g:1569:6: a6= ',' (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding )
                    	    {
                    	    a6=(Token)match(input,13,FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1231); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_2_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateNode(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[76]);
                    	    					}

                    	    // Structuralrmg.g:1583:6: (a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding )
                    	    // Structuralrmg.g:1584:7: a7_0= parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding
                    	    {
                    	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1265);
                    	    a7_0=parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a7_0 != null) {
                    	    								if (a7_0 != null) {
                    	    									Object value = a7_0;
                    	    									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ATTRIBUTES, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_2_0_0_1, a7_0, true);
                    	    								copyLocalizationInfos(a7_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[77]);
                    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[78]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[79]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[80]);
                    			}

                    a8=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1326); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_4_0_0_3, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[81]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[82]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[83]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[84]);
            	}

            // Structuralrmg.g:1641:2: ( (a9= 'plays' (a10= TEXT ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==30) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // Structuralrmg.g:1642:3: (a9= 'plays' (a10= TEXT ) )
                    {
                    // Structuralrmg.g:1642:3: (a9= 'plays' (a10= TEXT ) )
                    // Structuralrmg.g:1643:4: a9= 'plays' (a10= TEXT )
                    {
                    a9=(Token)match(input,30,FOLLOW_30_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1368); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[85]);
                    			}

                    // Structuralrmg.g:1657:4: (a10= TEXT )
                    // Structuralrmg.g:1658:5: a10= TEXT
                    {
                    a10=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1394); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
                    						startIncompleteElement(element);
                    					}
                    					if (a10 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateNode, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_NODE__ROLE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_4_0_0_5_0_0_1, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[86]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[87]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding"
    // Structuralrmg.g:1706:1: parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding returns [edu.ustb.sei.rmg.structuralrmg.AttributeBinding element = null] : (a0= TEXT ) a1= ':' (a2= TEXT ) ;
    public final edu.ustb.sei.rmg.structuralrmg.AttributeBinding parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.AttributeBinding element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Structuralrmg.g:1709:2: ( (a0= TEXT ) a1= ':' (a2= TEXT ) )
            // Structuralrmg.g:1710:2: (a0= TEXT ) a1= ':' (a2= TEXT )
            {
            // Structuralrmg.g:1710:2: (a0= TEXT )
            // Structuralrmg.g:1711:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1459); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[88]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1480); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[89]);
            	}

            // Structuralrmg.g:1760:2: (a2= TEXT )
            // Structuralrmg.g:1761:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1498); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createAttributeBinding();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EAttribute proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.AttributeBinding, org.eclipse.emf.ecore.EAttribute>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAttributeBindingAttributeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ATTRIBUTE_BINDING__ATTRIBUTE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_5_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[91]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode"
    // Structuralrmg.g:1803:1: parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode returns [edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element = null] : ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? ( (a1= 'shared' |a2= 'reversed' )? ) (a5= TEXT ) a6= '@' (a7= TEXT ) ( (a8= ',' (a9= TEXT ) ) )* ( (a10= 'plays' (a11= TEXT ) ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        edu.ustb.sei.rmg.structuralrmg.Bound a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Structuralrmg.g:1806:2: ( ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? ( (a1= 'shared' |a2= 'reversed' )? ) (a5= TEXT ) a6= '@' (a7= TEXT ) ( (a8= ',' (a9= TEXT ) ) )* ( (a10= 'plays' (a11= TEXT ) ) )? )
            // Structuralrmg.g:1807:2: ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )? ( (a1= 'shared' |a2= 'reversed' )? ) (a5= TEXT ) a6= '@' (a7= TEXT ) ( (a8= ',' (a9= TEXT ) ) )* ( (a10= 'plays' (a11= TEXT ) ) )?
            {
            // Structuralrmg.g:1807:2: ( (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==32) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // Structuralrmg.g:1808:3: (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound )
                    {
                    // Structuralrmg.g:1808:3: (a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound )
                    // Structuralrmg.g:1809:4: a0_0= parse_edu_ustb_sei_rmg_structuralrmg_Bound
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Bound_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1543);
                    a0_0=parse_edu_ustb_sei_rmg_structuralrmg_Bound();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
                    					startIncompleteElement(element);
                    					// initialize enumeration attribute
                    					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
                    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				}
                    				if (a0_0 != null) {
                    					if (a0_0 != null) {
                    						Object value = a0_0;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__BOUND), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_0, a0_0, true);
                    					copyLocalizationInfos(a0_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[92]);
            	}

            // Structuralrmg.g:1838:2: ( (a1= 'shared' |a2= 'reversed' )? )
            // Structuralrmg.g:1839:3: (a1= 'shared' |a2= 'reversed' )?
            {
            // Structuralrmg.g:1839:3: (a1= 'shared' |a2= 'reversed' )?
            int alt14=3;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==33) ) {
                alt14=1;
            }
            else if ( (LA14_0==31) ) {
                alt14=2;
            }
            switch (alt14) {
                case 1 :
                    // Structuralrmg.g:1840:4: a1= 'shared'
                    {
                    a1=(Token)match(input,33,FOLLOW_33_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1578); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
                    					startIncompleteElement(element);
                    					// initialize enumeration attribute
                    					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
                    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    				// set value of enumeration attribute
                    				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.SHARED_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Structuralrmg.g:1856:8: a2= 'reversed'
                    {
                    a2=(Token)match(input,31,FOLLOW_31_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1593); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
                    					startIncompleteElement(element);
                    					// initialize enumeration attribute
                    					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
                    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    				// set value of enumeration attribute
                    				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.REVERSED_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[93]);
            	}

            // Structuralrmg.g:1878:2: (a5= TEXT )
            // Structuralrmg.g:1879:3: a5= TEXT
            {
            a5=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1618); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
            				startIncompleteElement(element);
            				// initialize enumeration attribute
            				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
            			}
            			if (a5 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[94]);
            	}

            a6=(Token)match(input,18,FOLLOW_18_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1639); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
            			startIncompleteElement(element);
            			// initialize enumeration attribute
            			Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
            			element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[95]);
            	}

            // Structuralrmg.g:1934:2: (a7= TEXT )
            // Structuralrmg.g:1935:3: a7= TEXT
            {
            a7=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1657); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
            				startIncompleteElement(element);
            				// initialize enumeration attribute
            				Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
            			}
            			if (a7 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES, value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[96]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[97]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[98]);
            	}

            // Structuralrmg.g:1979:2: ( (a8= ',' (a9= TEXT ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==13) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // Structuralrmg.g:1980:3: (a8= ',' (a9= TEXT ) )
            	    {
            	    // Structuralrmg.g:1980:3: (a8= ',' (a9= TEXT ) )
            	    // Structuralrmg.g:1981:4: a8= ',' (a9= TEXT )
            	    {
            	    a8=(Token)match(input,13,FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1687); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
            	    					startIncompleteElement(element);
            	    					// initialize enumeration attribute
            	    					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
            	    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[99]);
            	    			}

            	    // Structuralrmg.g:1998:4: (a9= TEXT )
            	    // Structuralrmg.g:1999:5: a9= TEXT
            	    {
            	    a9=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1713); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
            	    						startIncompleteElement(element);
            	    						// initialize enumeration attribute
            	    						Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
            	    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
            	    					}
            	    					if (a9 != null) {
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	    						tokenResolver.setOptions(getOptions());
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a9).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStopIndex());
            	    						}
            	    						String resolved = (String) resolvedObject;
            	    						edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            	    						collectHiddenTokens(element);
            	    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getNestedTemplateNodeTemplatesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES), resolved, proxy);
            	    						if (proxy != null) {
            	    							Object value = proxy;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__TEMPLATES, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_5_0_0_1, proxy, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, element);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, proxy);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[100]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[101]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[102]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[103]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[104]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[105]);
            	}

            // Structuralrmg.g:2052:2: ( (a10= 'plays' (a11= TEXT ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // Structuralrmg.g:2053:3: (a10= 'plays' (a11= TEXT ) )
                    {
                    // Structuralrmg.g:2053:3: (a10= 'plays' (a11= TEXT ) )
                    // Structuralrmg.g:2054:4: a10= 'plays' (a11= TEXT )
                    {
                    a10=(Token)match(input,30,FOLLOW_30_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1768); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
                    					startIncompleteElement(element);
                    					// initialize enumeration attribute
                    					Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
                    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[106]);
                    			}

                    // Structuralrmg.g:2071:4: (a11= TEXT )
                    // Structuralrmg.g:2072:5: a11= TEXT
                    {
                    a11=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1794); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createNestedTemplateNode();
                    						startIncompleteElement(element);
                    						// initialize enumeration attribute
                    						Object value = edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getCompositionSort().getEEnumLiteral(edu.ustb.sei.rmg.structuralrmg.CompositionSort.NESTED_VALUE).getInstance();
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__SORT), value);
                    					}
                    					if (a11 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateNode, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateNodeRoleReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.NESTED_TEMPLATE_NODE__ROLE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_6_0_0_6_0_0_1, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[107]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[108]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge"
    // Structuralrmg.g:2123:1: parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element = null] : (a0= TEXT ) a1= ':' (a2= PATH ) a3= '(' (a4= TEXT ) ( (a5= '[' (a6= NUMBER ) a7= ']' ) )? a8= ',' (a9= TEXT ) ( (a10= '[' (a11= NUMBER ) a12= ']' ) )? a13= ')' ( (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        edu.ustb.sei.rmg.structuralrmg.EndpointBinding a14_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Structuralrmg.g:2126:2: ( (a0= TEXT ) a1= ':' (a2= PATH ) a3= '(' (a4= TEXT ) ( (a5= '[' (a6= NUMBER ) a7= ']' ) )? a8= ',' (a9= TEXT ) ( (a10= '[' (a11= NUMBER ) a12= ']' ) )? a13= ')' ( (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )? )
            // Structuralrmg.g:2127:2: (a0= TEXT ) a1= ':' (a2= PATH ) a3= '(' (a4= TEXT ) ( (a5= '[' (a6= NUMBER ) a7= ']' ) )? a8= ',' (a9= TEXT ) ( (a10= '[' (a11= NUMBER ) a12= ']' ) )? a13= ')' ( (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )?
            {
            // Structuralrmg.g:2127:2: (a0= TEXT )
            // Structuralrmg.g:2128:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1859); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[109]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1880); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[110]);
            	}

            // Structuralrmg.g:2177:2: (a2= PATH )
            // Structuralrmg.g:2178:3: a2= PATH
            {
            a2=(Token)match(input,PATH,FOLLOW_PATH_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1898); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EReference proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__REFERENCE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[111]);
            	}

            a3=(Token)match(input,11,FOLLOW_11_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1919); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[112]);
            	}

            // Structuralrmg.g:2231:2: (a4= TEXT )
            // Structuralrmg.g:2232:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1937); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeSourceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[113]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[114]);
            	}

            // Structuralrmg.g:2272:2: ( (a5= '[' (a6= NUMBER ) a7= ']' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==19) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // Structuralrmg.g:2273:3: (a5= '[' (a6= NUMBER ) a7= ']' )
                    {
                    // Structuralrmg.g:2273:3: (a5= '[' (a6= NUMBER ) a7= ']' )
                    // Structuralrmg.g:2274:4: a5= '[' (a6= NUMBER ) a7= ']'
                    {
                    a5=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1967); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[115]);
                    			}

                    // Structuralrmg.g:2288:4: (a6= NUMBER )
                    // Structuralrmg.g:2289:5: a6= NUMBER
                    {
                    a6=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1993); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    						startIncompleteElement(element);
                    					}
                    					if (a6 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__SOURCE_POS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[116]);
                    			}

                    a7=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2026); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_5_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[117]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[118]);
            	}

            a8=(Token)match(input,13,FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2059); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[119]);
            	}

            // Structuralrmg.g:2359:2: (a9= TEXT )
            // Structuralrmg.g:2360:3: a9= TEXT
            {
            a9=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2077); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            				startIncompleteElement(element);
            			}
            			if (a9 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a9.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a9).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a9).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPrimitiveTemplateEdgeTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_7, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a9, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[120]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[121]);
            	}

            // Structuralrmg.g:2400:2: ( (a10= '[' (a11= NUMBER ) a12= ']' ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==19) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // Structuralrmg.g:2401:3: (a10= '[' (a11= NUMBER ) a12= ']' )
                    {
                    // Structuralrmg.g:2401:3: (a10= '[' (a11= NUMBER ) a12= ']' )
                    // Structuralrmg.g:2402:4: a10= '[' (a11= NUMBER ) a12= ']'
                    {
                    a10=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2107); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[122]);
                    			}

                    // Structuralrmg.g:2416:4: (a11= NUMBER )
                    // Structuralrmg.g:2417:5: a11= NUMBER
                    {
                    a11=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2133); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    						startIncompleteElement(element);
                    					}
                    					if (a11 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__TARGET_POS), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[123]);
                    			}

                    a12=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2166); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_8_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[124]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[125]);
            	}

            a13=(Token)match(input,12,FOLLOW_12_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2199); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[126]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getPrimitiveTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[127]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[128]);
            	}

            // Structuralrmg.g:2489:2: ( (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )?
            int alt19=2;
            switch ( input.LA(1) ) {
                case 24:
                    {
                    alt19=1;
                    }
                    break;
                case 16:
                    {
                    int LA19_2 = input.LA(2);

                    if ( (synpred20_Structuralrmg()) ) {
                        alt19=1;
                    }
                    }
                    break;
                case EOF:
                    {
                    int LA19_3 = input.LA(2);

                    if ( (synpred20_Structuralrmg()) ) {
                        alt19=1;
                    }
                    }
                    break;
            }

            switch (alt19) {
                case 1 :
                    // Structuralrmg.g:2490:3: (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
                    {
                    // Structuralrmg.g:2490:3: (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
                    // Structuralrmg.g:2491:4: a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2222);
                    a14_0=parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateEdge();
                    					startIncompleteElement(element);
                    				}
                    				if (a14_0 != null) {
                    					if (a14_0 != null) {
                    						Object value = a14_0;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.PRIMITIVE_TEMPLATE_EDGE__BINDING), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_7_0_0_10, a14_0, true);
                    					copyLocalizationInfos(a14_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[129]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge"
    // Structuralrmg.g:2519:1: parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge element = null] : (a0= TEXT ) a1= ':' (a2= PATH ) a3= 'in' ( (a4= 'loop' )? ) (a7= TEXT ) ( (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a7=null;
        edu.ustb.sei.rmg.structuralrmg.EndpointBinding a8_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Structuralrmg.g:2522:2: ( (a0= TEXT ) a1= ':' (a2= PATH ) a3= 'in' ( (a4= 'loop' )? ) (a7= TEXT ) ( (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )? )
            // Structuralrmg.g:2523:2: (a0= TEXT ) a1= ':' (a2= PATH ) a3= 'in' ( (a4= 'loop' )? ) (a7= TEXT ) ( (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )?
            {
            // Structuralrmg.g:2523:2: (a0= TEXT )
            // Structuralrmg.g:2524:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2267); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
            				startIncompleteElement(element);
            				// initialize boolean attribute
            				{
            					Object value = false;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
            				}
            			}
            			if (a0 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[130]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2288); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[131]);
            	}

            // Structuralrmg.g:2583:2: (a2= PATH )
            // Structuralrmg.g:2584:3: a2= PATH
            {
            a2=(Token)match(input,PATH,FOLLOW_PATH_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2306); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
            				startIncompleteElement(element);
            				// initialize boolean attribute
            				{
            					Object value = false;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
            				}
            			}
            			if (a2 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("PATH");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EReference proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.TemplateEdge, org.eclipse.emf.ecore.EReference>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTemplateEdgeReferenceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__REFERENCE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[132]);
            	}

            a3=(Token)match(input,26,FOLLOW_26_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2327); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
            			startIncompleteElement(element);
            			// initialize boolean attribute
            			{
            				Object value = false;
            				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
            			}
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[133]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[134]);
            	}

            // Structuralrmg.g:2648:2: ( (a4= 'loop' )? )
            // Structuralrmg.g:2649:3: (a4= 'loop' )?
            {
            // Structuralrmg.g:2649:3: (a4= 'loop' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==27) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // Structuralrmg.g:2650:4: a4= 'loop'
                    {
                    a4=(Token)match(input,27,FOLLOW_27_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2350); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
                    					}
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_4, true, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    				// set value of boolean attribute
                    				Object value = true;
                    				element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[135]);
            	}

            // Structuralrmg.g:2674:2: (a7= TEXT )
            // Structuralrmg.g:2675:3: a7= TEXT
            {
            a7=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2375); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
            				startIncompleteElement(element);
            				// initialize boolean attribute
            				{
            					Object value = false;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
            				}
            			}
            			if (a7 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				edu.ustb.sei.rmg.structuralrmg.TemplateNode proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createPrimitiveTemplateNode();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge, edu.ustb.sei.rmg.structuralrmg.TemplateNode>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getChainTemplateEdgeNodeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__NODE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_5, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[136]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getChainTemplateEdge(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[137]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[138]);
            	}

            // Structuralrmg.g:2721:2: ( (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==24) ) {
                alt21=1;
            }
            else if ( (LA21_0==16) ) {
                int LA21_2 = input.LA(2);

                if ( (synpred22_Structuralrmg()) ) {
                    alt21=1;
                }
            }
            switch (alt21) {
                case 1 :
                    // Structuralrmg.g:2722:3: (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
                    {
                    // Structuralrmg.g:2722:3: (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
                    // Structuralrmg.g:2723:4: a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2405);
                    a8_0=parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createChainTemplateEdge();
                    					startIncompleteElement(element);
                    					// initialize boolean attribute
                    					{
                    						Object value = false;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__ROUND), value);
                    					}
                    				}
                    				if (a8_0 != null) {
                    					if (a8_0 != null) {
                    						Object value = a8_0;
                    						element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.CHAIN_TEMPLATE_EDGE__BINDING), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_8_0_0_6, a8_0, true);
                    					copyLocalizationInfos(a8_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[139]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding"
    // Structuralrmg.g:2756:1: parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding returns [edu.ustb.sei.rmg.structuralrmg.EndpointBinding element = null] : ( (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )? ( (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.EndpointBinding parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.EndpointBinding element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        Token a4=null;
        edu.ustb.sei.rmg.structuralrmg.BindingMapping a2_0 =null;

        edu.ustb.sei.rmg.structuralrmg.BindingMapping a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Structuralrmg.g:2759:2: ( ( (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )? ( (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )? )
            // Structuralrmg.g:2760:2: ( (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )? ( (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )?
            {
            // Structuralrmg.g:2760:2: ( (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==24) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==34) ) {
                    alt23=1;
                }
            }
            switch (alt23) {
                case 1 :
                    // Structuralrmg.g:2761:3: (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ )
                    {
                    // Structuralrmg.g:2761:3: (a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ )
                    // Structuralrmg.g:2762:4: a0= 'for' a1= 'source' ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+
                    {
                    a0=(Token)match(input,24,FOLLOW_24_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2455); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[140]);
                    			}

                    a1=(Token)match(input,34,FOLLOW_34_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2475); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[141]);
                    			}

                    // Structuralrmg.g:2790:4: ( (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+
                    int cnt22=0;
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==TEXT) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // Structuralrmg.g:2791:5: (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping )
                    	    {
                    	    // Structuralrmg.g:2791:5: (a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping )
                    	    // Structuralrmg.g:2792:6: a2_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping
                    	    {
                    	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2508);
                    	    a2_0=parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (terminateParsing) {
                    	    							throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    	    						}
                    	    						if (element == null) {
                    	    							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						if (a2_0 != null) {
                    	    							if (a2_0 != null) {
                    	    								Object value = a2_0;
                    	    								addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__SOURCE_MAPPINGS, value);
                    	    								completedElement(value, true);
                    	    							}
                    	    							collectHiddenTokens(element);
                    	    							retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_0_0_0_2, a2_0, true);
                    	    							copyLocalizationInfos(a2_0, element);
                    	    						}
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt22 >= 1 ) break loop22;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(22, input);
                                throw eee;
                        }
                        cnt22++;
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[142]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[143]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[144]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[145]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[146]);
            	}

            // Structuralrmg.g:2828:2: ( (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==24) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // Structuralrmg.g:2829:3: (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ )
                    {
                    // Structuralrmg.g:2829:3: (a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+ )
                    // Structuralrmg.g:2830:4: a3= 'for' a4= 'target' ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+
                    {
                    a3=(Token)match(input,24,FOLLOW_24_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2570); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[147]);
                    			}

                    a4=(Token)match(input,35,FOLLOW_35_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2590); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[148]);
                    			}

                    // Structuralrmg.g:2858:4: ( (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping ) )+
                    int cnt24=0;
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==TEXT) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // Structuralrmg.g:2859:5: (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping )
                    	    {
                    	    // Structuralrmg.g:2859:5: (a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping )
                    	    // Structuralrmg.g:2860:6: a5_0= parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping
                    	    {
                    	    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2623);
                    	    a5_0=parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (terminateParsing) {
                    	    							throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    	    						}
                    	    						if (element == null) {
                    	    							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createEndpointBinding();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						if (a5_0 != null) {
                    	    							if (a5_0 != null) {
                    	    								Object value = a5_0;
                    	    								addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.ENDPOINT_BINDING__TARGET_MAPPINGS, value);
                    	    								completedElement(value, true);
                    	    							}
                    	    							collectHiddenTokens(element);
                    	    							retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_9_0_0_1_0_0_2, a5_0, true);
                    	    							copyLocalizationInfos(a5_0, element);
                    	    						}
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt24 >= 1 ) break loop24;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(24, input);
                                throw eee;
                        }
                        cnt24++;
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[149]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[150]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[151]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping"
    // Structuralrmg.g:2896:1: parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping returns [edu.ustb.sei.rmg.structuralrmg.BindingMapping element = null] : (a0= TEXT ) a1= '=>' ( ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* ) )+ ( (a5= '[' (a6= NUMBER ) a7= ']' ) )? ;
    public final edu.ustb.sei.rmg.structuralrmg.BindingMapping parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.BindingMapping element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Structuralrmg.g:2899:2: ( (a0= TEXT ) a1= '=>' ( ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* ) )+ ( (a5= '[' (a6= NUMBER ) a7= ']' ) )? )
            // Structuralrmg.g:2900:2: (a0= TEXT ) a1= '=>' ( ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* ) )+ ( (a5= '[' (a6= NUMBER ) a7= ']' ) )?
            {
            // Structuralrmg.g:2900:2: (a0= TEXT )
            // Structuralrmg.g:2901:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2695); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				edu.ustb.sei.rmg.structuralrmg.Template proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createTemplate();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Template>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingTemplateReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__TEMPLATE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_0, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[152]);
            	}

            a1=(Token)match(input,17,FOLLOW_17_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2716); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[153]);
            	}

            // Structuralrmg.g:2954:2: ( ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* ) )+
            int cnt27=0;
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==TEXT) ) {
                    int LA27_2 = input.LA(2);

                    if ( (LA27_2==EOF||LA27_2==TEXT||LA27_2==13||LA27_2==16||LA27_2==19||LA27_2==24) ) {
                        alt27=1;
                    }


                }


                switch (alt27) {
            	case 1 :
            	    // Structuralrmg.g:2955:3: ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* )
            	    {
            	    // Structuralrmg.g:2955:3: ( (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )* )
            	    // Structuralrmg.g:2956:4: (a2= TEXT ) ( (a3= ',' (a4= TEXT ) ) )*
            	    {
            	    // Structuralrmg.g:2956:4: (a2= TEXT )
            	    // Structuralrmg.g:2957:5: a2= TEXT
            	    {
            	    a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2745); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a2 != null) {
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	    						tokenResolver.setOptions(getOptions());
            	    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            	    						}
            	    						String resolved = (String) resolvedObject;
            	    						edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
            	    						collectHiddenTokens(element);
            	    						registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), resolved, proxy);
            	    						if (proxy != null) {
            	    							Object value = proxy;
            	    							addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_0, proxy, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[154]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[155]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[156]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[157]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[158]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[159]);
            	    			}

            	    // Structuralrmg.g:3001:4: ( (a3= ',' (a4= TEXT ) ) )*
            	    loop26:
            	    do {
            	        int alt26=2;
            	        int LA26_0 = input.LA(1);

            	        if ( (LA26_0==13) ) {
            	            alt26=1;
            	        }


            	        switch (alt26) {
            	    	case 1 :
            	    	    // Structuralrmg.g:3002:5: (a3= ',' (a4= TEXT ) )
            	    	    {
            	    	    // Structuralrmg.g:3002:5: (a3= ',' (a4= TEXT ) )
            	    	    // Structuralrmg.g:3003:6: a3= ',' (a4= TEXT )
            	    	    {
            	    	    a3=(Token)match(input,13,FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2791); if (state.failed) return element;

            	    	    if ( state.backtracking==0 ) {
            	    	    						if (element == null) {
            	    	    							element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
            	    	    							startIncompleteElement(element);
            	    	    						}
            	    	    						collectHiddenTokens(element);
            	    	    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_0, null, true);
            	    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	    	    					}

            	    	    if ( state.backtracking==0 ) {
            	    	    						// expected elements (follow set)
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[160]);
            	    	    					}

            	    	    // Structuralrmg.g:3017:6: (a4= TEXT )
            	    	    // Structuralrmg.g:3018:7: a4= TEXT
            	    	    {
            	    	    a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2825); if (state.failed) return element;

            	    	    if ( state.backtracking==0 ) {
            	    	    							if (terminateParsing) {
            	    	    								throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
            	    	    							}
            	    	    							if (element == null) {
            	    	    								element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
            	    	    								startIncompleteElement(element);
            	    	    							}
            	    	    							if (a4 != null) {
            	    	    								edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	    	    								tokenResolver.setOptions(getOptions());
            	    	    								edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
            	    	    								tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), result);
            	    	    								Object resolvedObject = result.getResolvedToken();
            	    	    								if (resolvedObject == null) {
            	    	    									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	    	    								}
            	    	    								String resolved = (String) resolvedObject;
            	    	    								edu.ustb.sei.rmg.structuralrmg.Role proxy = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createRole();
            	    	    								collectHiddenTokens(element);
            	    	    								registerContextDependentProxy(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgContextDependentURIFragmentFactory<edu.ustb.sei.rmg.structuralrmg.BindingMapping, edu.ustb.sei.rmg.structuralrmg.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getBindingMappingRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES), resolved, proxy);
            	    	    								if (proxy != null) {
            	    	    									Object value = proxy;
            	    	    									addObjectToList(element, edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__ROLES, value);
            	    	    									completedElement(value, false);
            	    	    								}
            	    	    								collectHiddenTokens(element);
            	    	    								retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_2_0_0_1_0_0_1, proxy, true);
            	    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            	    	    							}
            	    	    						}

            	    	    }


            	    	    if ( state.backtracking==0 ) {
            	    	    						// expected elements (follow set)
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[161]);
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[162]);
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[163]);
            	    	    						addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[164]);
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[165]);
            	    	    						addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[166]);
            	    	    					}

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop26;
            	        }
            	    } while (true);


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[167]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[168]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[169]);
            	    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[170]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[171]);
            	    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[172]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt27 >= 1 ) break loop27;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(27, input);
                        throw eee;
                }
                cnt27++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[173]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[174]);
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[175]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[176]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[177]);
            	}

            // Structuralrmg.g:3085:2: ( (a5= '[' (a6= NUMBER ) a7= ']' ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==19) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // Structuralrmg.g:3086:3: (a5= '[' (a6= NUMBER ) a7= ']' )
                    {
                    // Structuralrmg.g:3086:3: (a5= '[' (a6= NUMBER ) a7= ']' )
                    // Structuralrmg.g:3087:4: a5= '[' (a6= NUMBER ) a7= ']'
                    {
                    a5=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2915); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[178]);
                    			}

                    // Structuralrmg.g:3101:4: (a6= NUMBER )
                    // Structuralrmg.g:3102:5: a6= NUMBER
                    {
                    a6=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2941); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
                    						startIncompleteElement(element);
                    					}
                    					if (a6 != null) {
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.BINDING_MAPPING__POSITION), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[179]);
                    			}

                    a7=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2974); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.rmg.structuralrmg.StructuralRmgFactory.eINSTANCE.createBindingMapping();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgGrammarInformationProvider.STRUCTURALRMG_10_0_0_3_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[180]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[181]);
                    				addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[182]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage.eINSTANCE.getEndpointBinding(), edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[183]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[184]);
            		addExpectedElement(null, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgExpectationConstants.EXPECTATIONS[185]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode"
    // Structuralrmg.g:3164:1: parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode returns [edu.ustb.sei.rmg.structuralrmg.TemplateNode element = null] : (c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode |c1= parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode );
    public final edu.ustb.sei.rmg.structuralrmg.TemplateNode parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.TemplateNode element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode_StartIndex = input.index();

        edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateNode c0 =null;

        edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Structuralrmg.g:3165:2: (c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode |c1= parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode )
            int alt29=2;
            switch ( input.LA(1) ) {
            case 32:
                {
                int LA29_1 = input.LA(2);

                if ( (LA29_1==19) ) {
                    int LA29_4 = input.LA(3);

                    if ( (LA29_4==NUMBER) ) {
                        int LA29_6 = input.LA(4);

                        if ( (LA29_6==14) ) {
                            int LA29_7 = input.LA(5);

                            if ( (LA29_7==NUMBER) ) {
                                int LA29_8 = input.LA(6);

                                if ( (LA29_8==20) ) {
                                    int LA29_9 = input.LA(7);

                                    if ( (LA29_9==TEXT) ) {
                                        int LA29_2 = input.LA(8);

                                        if ( (LA29_2==15) ) {
                                            alt29=1;
                                        }
                                        else if ( (LA29_2==18) ) {
                                            alt29=2;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 29, 2, input);

                                            throw nvae;

                                        }
                                    }
                                    else if ( (LA29_9==31||LA29_9==33) ) {
                                        alt29=2;
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 29, 9, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 29, 8, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 29, 7, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 29, 6, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 29, 4, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 1, input);

                    throw nvae;

                }
                }
                break;
            case TEXT:
                {
                int LA29_2 = input.LA(2);

                if ( (LA29_2==15) ) {
                    alt29=1;
                }
                else if ( (LA29_2==18) ) {
                    alt29=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 2, input);

                    throw nvae;

                }
                }
                break;
            case 31:
            case 33:
                {
                alt29=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;

            }

            switch (alt29) {
                case 1 :
                    // Structuralrmg.g:3166:2: c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode3018);
                    c0=parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Structuralrmg.g:3167:4: c1= parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode3028);
                    c1=parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode"



    // $ANTLR start "parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge"
    // Structuralrmg.g:3171:1: parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge returns [edu.ustb.sei.rmg.structuralrmg.TemplateEdge element = null] : (c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge |c1= parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge );
    public final edu.ustb.sei.rmg.structuralrmg.TemplateEdge parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.TemplateEdge element =  null;

        int parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge_StartIndex = input.index();

        edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge c0 =null;

        edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Structuralrmg.g:3172:2: (c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge |c1= parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==TEXT) ) {
                int LA30_1 = input.LA(2);

                if ( (LA30_1==15) ) {
                    int LA30_2 = input.LA(3);

                    if ( (LA30_2==PATH) ) {
                        int LA30_3 = input.LA(4);

                        if ( (LA30_3==11) ) {
                            alt30=1;
                        }
                        else if ( (LA30_3==26) ) {
                            alt30=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 30, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 2, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }
            switch (alt30) {
                case 1 :
                    // Structuralrmg.g:3173:2: c0= parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge3049);
                    c0=parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Structuralrmg.g:3174:4: c1= parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge3059);
                    c1=parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge"

    // $ANTLR start synpred20_Structuralrmg
    public final void synpred20_Structuralrmg_fragment() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.EndpointBinding a14_0 =null;


        // Structuralrmg.g:2490:3: ( (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )
        // Structuralrmg.g:2490:3: (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
        {
        // Structuralrmg.g:2490:3: (a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
        // Structuralrmg.g:2491:4: a14_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding
        {
        pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_synpred20_Structuralrmg2222);
        a14_0=parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();

        state._fsp--;
        if (state.failed) return ;

        }


        }

    }
    // $ANTLR end synpred20_Structuralrmg

    // $ANTLR start synpred22_Structuralrmg
    public final void synpred22_Structuralrmg_fragment() throws RecognitionException {
        edu.ustb.sei.rmg.structuralrmg.EndpointBinding a8_0 =null;


        // Structuralrmg.g:2722:3: ( (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding ) )
        // Structuralrmg.g:2722:3: (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
        {
        // Structuralrmg.g:2722:3: (a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding )
        // Structuralrmg.g:2723:4: a8_0= parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding
        {
        pushFollow(FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_synpred22_Structuralrmg2405);
        a8_0=parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding();

        state._fsp--;
        if (state.failed) return ;

        }


        }

    }
    // $ANTLR end synpred22_Structuralrmg

    // Delegated rules

    public final boolean synpred22_Structuralrmg() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred22_Structuralrmg_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred20_Structuralrmg() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred20_Structuralrmg_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel115 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel133 = new BitSet(new long[]{0x0000001002200002L});
    public static final BitSet FOLLOW_25_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel163 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_URI_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel189 = new BitSet(new long[]{0x0000001002200002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Template_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateModel250 = new BitSet(new long[]{0x0000001000200002L});
    public static final BitSet FOLLOW_21_in_parse_edu_ustb_sei_rmg_structuralrmg_Template315 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_parse_edu_ustb_sei_rmg_structuralrmg_Template336 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Template354 = new BitSet(new long[]{0x0000002000800800L});
    public static final BitSet FOLLOW_11_in_parse_edu_ustb_sei_rmg_structuralrmg_Template384 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Role_in_parse_edu_ustb_sei_rmg_structuralrmg_Template410 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_Template451 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Role_in_parse_edu_ustb_sei_rmg_structuralrmg_Template485 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_parse_edu_ustb_sei_rmg_structuralrmg_Template546 = new BitSet(new long[]{0x0000002000800000L});
    public static final BitSet FOLLOW_23_in_parse_edu_ustb_sei_rmg_structuralrmg_Template588 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Template614 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_parse_edu_ustb_sei_rmg_structuralrmg_Template660 = new BitSet(new long[]{0x0000004020400000L});
    public static final BitSet FOLLOW_29_in_parse_edu_ustb_sei_rmg_structuralrmg_Template683 = new BitSet(new long[]{0x0000000380000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_Template709 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_edu_ustb_sei_rmg_structuralrmg_Template737 = new BitSet(new long[]{0x0000004020400000L});
    public static final BitSet FOLLOW_22_in_parse_edu_ustb_sei_rmg_structuralrmg_Template779 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_Template805 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_edu_ustb_sei_rmg_structuralrmg_Template833 = new BitSet(new long[]{0x0000004000400000L});
    public static final BitSet FOLLOW_38_in_parse_edu_ustb_sei_rmg_structuralrmg_Template866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_Role899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound935 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound949 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound967 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound988 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound1006 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_Bound1027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Bound_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1065 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1095 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1116 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1134 = new BitSet(new long[]{0x0000000040080002L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1164 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1190 = new BitSet(new long[]{0x0000000000102000L});
    public static final BitSet FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1231 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1265 = new BitSet(new long[]{0x0000000000102000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1326 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1368 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode1394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1459 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1480 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_AttributeBinding1498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_Bound_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1543 = new BitSet(new long[]{0x0000000280000100L});
    public static final BitSet FOLLOW_33_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1578 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_31_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1593 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1618 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1639 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1657 = new BitSet(new long[]{0x0000000040002002L});
    public static final BitSet FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1687 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1713 = new BitSet(new long[]{0x0000000040002002L});
    public static final BitSet FOLLOW_30_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1768 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode1794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1859 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1880 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_PATH_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1898 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1919 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1937 = new BitSet(new long[]{0x0000000000082000L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1967 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge1993 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2026 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2059 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2077 = new BitSet(new long[]{0x0000000000081000L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2107 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2133 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2166 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2199 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge2222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2267 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2288 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_PATH_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2306 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2327 = new BitSet(new long[]{0x0000000008000100L});
    public static final BitSet FOLLOW_27_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2350 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2375 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge2405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2455 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2475 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2508 = new BitSet(new long[]{0x0000000001000102L});
    public static final BitSet FOLLOW_24_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2570 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2590 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping_in_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding2623 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2695 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2716 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2745 = new BitSet(new long[]{0x0000000000082102L});
    public static final BitSet FOLLOW_13_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2791 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2825 = new BitSet(new long[]{0x0000000000082102L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2915 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_NUMBER_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2941 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_rmg_structuralrmg_BindingMapping2974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode3018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_NestedTemplateNode_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateNode3028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_PrimitiveTemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge3049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_ChainTemplateEdge_in_parse_edu_ustb_sei_rmg_structuralrmg_TemplateEdge3059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_synpred20_Structuralrmg2222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_rmg_structuralrmg_EndpointBinding_in_synpred22_Structuralrmg2405 = new BitSet(new long[]{0x0000000000000002L});

}