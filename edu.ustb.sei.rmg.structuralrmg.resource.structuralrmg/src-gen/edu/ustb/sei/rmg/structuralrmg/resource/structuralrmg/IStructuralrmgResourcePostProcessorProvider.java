/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IStructuralrmgResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourcePostProcessor getResourcePostProcessor();
	
}
