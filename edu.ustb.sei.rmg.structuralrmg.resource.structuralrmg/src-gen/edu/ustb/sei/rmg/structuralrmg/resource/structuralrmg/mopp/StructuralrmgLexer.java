// $ANTLR 3.4

	package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class StructuralrmgLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int LINEBREAK=4;
    public static final int ML_COMMENT=5;
    public static final int NUMBER=6;
    public static final int PATH=7;
    public static final int TEXT=8;
    public static final int URI=9;
    public static final int WHITESPACE=10;

    	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
    	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
    	
    	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public StructuralrmgLexer() {} 
    public StructuralrmgLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public StructuralrmgLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Structuralrmg.g"; }

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:15:7: ( '(' )
            // Structuralrmg.g:15:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:16:7: ( ')' )
            // Structuralrmg.g:16:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:17:7: ( ',' )
            // Structuralrmg.g:17:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:18:7: ( '..' )
            // Structuralrmg.g:18:9: '..'
            {
            match(".."); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:19:7: ( ':' )
            // Structuralrmg.g:19:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:20:7: ( ';' )
            // Structuralrmg.g:20:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:21:7: ( '=>' )
            // Structuralrmg.g:21:9: '=>'
            {
            match("=>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:22:7: ( '@' )
            // Structuralrmg.g:22:9: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:23:7: ( '[' )
            // Structuralrmg.g:23:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:24:7: ( ']' )
            // Structuralrmg.g:24:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:25:7: ( 'abstract' )
            // Structuralrmg.g:25:9: 'abstract'
            {
            match("abstract"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:26:7: ( 'edge' )
            // Structuralrmg.g:26:9: 'edge'
            {
            match("edge"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:27:7: ( 'extends' )
            // Structuralrmg.g:27:9: 'extends'
            {
            match("extends"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:28:7: ( 'for' )
            // Structuralrmg.g:28:9: 'for'
            {
            match("for"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:29:7: ( 'import' )
            // Structuralrmg.g:29:9: 'import'
            {
            match("import"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:30:7: ( 'in' )
            // Structuralrmg.g:30:9: 'in'
            {
            match("in"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:31:7: ( 'loop' )
            // Structuralrmg.g:31:9: 'loop'
            {
            match("loop"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:32:7: ( 'model' )
            // Structuralrmg.g:32:9: 'model'
            {
            match("model"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:33:7: ( 'node' )
            // Structuralrmg.g:33:9: 'node'
            {
            match("node"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:34:7: ( 'plays' )
            // Structuralrmg.g:34:9: 'plays'
            {
            match("plays"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:35:7: ( 'reversed' )
            // Structuralrmg.g:35:9: 'reversed'
            {
            match("reversed"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:36:7: ( 'set' )
            // Structuralrmg.g:36:9: 'set'
            {
            match("set"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:37:7: ( 'shared' )
            // Structuralrmg.g:37:9: 'shared'
            {
            match("shared"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:38:7: ( 'source' )
            // Structuralrmg.g:38:9: 'source'
            {
            match("source"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:39:7: ( 'target' )
            // Structuralrmg.g:39:9: 'target'
            {
            match("target"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:40:7: ( 'template' )
            // Structuralrmg.g:40:9: 'template'
            {
            match("template"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:41:7: ( '{' )
            // Structuralrmg.g:41:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:42:7: ( '}' )
            // Structuralrmg.g:42:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3178:5: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* ) )
            // Structuralrmg.g:3179:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* )
            {
            // Structuralrmg.g:3179:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* )
            // Structuralrmg.g:3179:3: ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // Structuralrmg.g:3179:26: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='-'||(LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Structuralrmg.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3181:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Structuralrmg.g:3182:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Structuralrmg.g:3182:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Structuralrmg.g:3182:3: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Structuralrmg.g:3182:3: ( '\\r\\n' | '\\r' | '\\n' )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='\r') ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='\n') ) {
                    alt2=1;
                }
                else {
                    alt2=2;
                }
            }
            else if ( (LA2_0=='\n') ) {
                alt2=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // Structuralrmg.g:3182:4: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Structuralrmg.g:3182:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Structuralrmg.g:3182:16: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3185:11: ( ( ( ' ' | '\\t' | '\\f' ) ) )
            // Structuralrmg.g:3186:2: ( ( ' ' | '\\t' | '\\f' ) )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "URI"
    public final void mURI() throws RecognitionException {
        try {
            int _type = URI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3189:4: ( ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' | ':' | '/' | '\\\\' | '\\.' )+ '>' ) )
            // Structuralrmg.g:3190:2: ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' | ':' | '/' | '\\\\' | '\\.' )+ '>' )
            {
            // Structuralrmg.g:3190:2: ( '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' | ':' | '/' | '\\\\' | '\\.' )+ '>' )
            // Structuralrmg.g:3190:3: '<' ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' | ':' | '/' | '\\\\' | '\\.' )+ '>'
            {
            match('<'); 

            // Structuralrmg.g:3190:6: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' | ':' | '/' | '\\\\' | '\\.' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '-' && LA3_0 <= ':')||(LA3_0 >= 'A' && LA3_0 <= 'Z')||LA3_0=='\\'||LA3_0=='_'||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Structuralrmg.g:
            	    {
            	    if ( (input.LA(1) >= '-' && input.LA(1) <= ':')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='\\'||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            match('>'); 

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "URI"

    // $ANTLR start "PATH"
    public final void mPATH() throws RecognitionException {
        try {
            int _type = PATH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3192:5: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* '::' ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* ) )
            // Structuralrmg.g:3193:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* '::' ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* )
            {
            // Structuralrmg.g:3193:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* '::' ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* )
            // Structuralrmg.g:3193:3: ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )* '::' ( 'A' .. 'Z' | 'a' .. 'z' | '_' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // Structuralrmg.g:3193:26: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='-'||(LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||LA4_0=='_'||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Structuralrmg.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            match("::"); 



            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // Structuralrmg.g:3193:90: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='-'||(LA5_0 >= '0' && LA5_0 <= '9')||(LA5_0 >= 'A' && LA5_0 <= 'Z')||LA5_0=='_'||(LA5_0 >= 'a' && LA5_0 <= 'z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Structuralrmg.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PATH"

    // $ANTLR start "NUMBER"
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3195:7: ( ( ( '-' )? ( '0' .. '9' )+ ) )
            // Structuralrmg.g:3196:2: ( ( '-' )? ( '0' .. '9' )+ )
            {
            // Structuralrmg.g:3196:2: ( ( '-' )? ( '0' .. '9' )+ )
            // Structuralrmg.g:3196:3: ( '-' )? ( '0' .. '9' )+
            {
            // Structuralrmg.g:3196:3: ( '-' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='-') ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // Structuralrmg.g:3196:3: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Structuralrmg.g:3196:7: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0 >= '0' && LA7_0 <= '9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // Structuralrmg.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Structuralrmg.g:3198:11: ( ( '/*' ( . )* '*/' ) )
            // Structuralrmg.g:3199:2: ( '/*' ( . )* '*/' )
            {
            // Structuralrmg.g:3199:2: ( '/*' ( . )* '*/' )
            // Structuralrmg.g:3199:4: '/*' ( . )* '*/'
            {
            match("/*"); 



            // Structuralrmg.g:3199:8: ( . )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='*') ) {
                    int LA8_1 = input.LA(2);

                    if ( (LA8_1=='/') ) {
                        alt8=2;
                    }
                    else if ( ((LA8_1 >= '\u0000' && LA8_1 <= '.')||(LA8_1 >= '0' && LA8_1 <= '\uFFFF')) ) {
                        alt8=1;
                    }


                }
                else if ( ((LA8_0 >= '\u0000' && LA8_0 <= ')')||(LA8_0 >= '+' && LA8_0 <= '\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Structuralrmg.g:3199:8: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            match("*/"); 



            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    public void mTokens() throws RecognitionException {
        // Structuralrmg.g:1:8: ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | TEXT | LINEBREAK | WHITESPACE | URI | PATH | NUMBER | ML_COMMENT )
        int alt9=35;
        alt9 = dfa9.predict(input);
        switch (alt9) {
            case 1 :
                // Structuralrmg.g:1:10: T__11
                {
                mT__11(); 


                }
                break;
            case 2 :
                // Structuralrmg.g:1:16: T__12
                {
                mT__12(); 


                }
                break;
            case 3 :
                // Structuralrmg.g:1:22: T__13
                {
                mT__13(); 


                }
                break;
            case 4 :
                // Structuralrmg.g:1:28: T__14
                {
                mT__14(); 


                }
                break;
            case 5 :
                // Structuralrmg.g:1:34: T__15
                {
                mT__15(); 


                }
                break;
            case 6 :
                // Structuralrmg.g:1:40: T__16
                {
                mT__16(); 


                }
                break;
            case 7 :
                // Structuralrmg.g:1:46: T__17
                {
                mT__17(); 


                }
                break;
            case 8 :
                // Structuralrmg.g:1:52: T__18
                {
                mT__18(); 


                }
                break;
            case 9 :
                // Structuralrmg.g:1:58: T__19
                {
                mT__19(); 


                }
                break;
            case 10 :
                // Structuralrmg.g:1:64: T__20
                {
                mT__20(); 


                }
                break;
            case 11 :
                // Structuralrmg.g:1:70: T__21
                {
                mT__21(); 


                }
                break;
            case 12 :
                // Structuralrmg.g:1:76: T__22
                {
                mT__22(); 


                }
                break;
            case 13 :
                // Structuralrmg.g:1:82: T__23
                {
                mT__23(); 


                }
                break;
            case 14 :
                // Structuralrmg.g:1:88: T__24
                {
                mT__24(); 


                }
                break;
            case 15 :
                // Structuralrmg.g:1:94: T__25
                {
                mT__25(); 


                }
                break;
            case 16 :
                // Structuralrmg.g:1:100: T__26
                {
                mT__26(); 


                }
                break;
            case 17 :
                // Structuralrmg.g:1:106: T__27
                {
                mT__27(); 


                }
                break;
            case 18 :
                // Structuralrmg.g:1:112: T__28
                {
                mT__28(); 


                }
                break;
            case 19 :
                // Structuralrmg.g:1:118: T__29
                {
                mT__29(); 


                }
                break;
            case 20 :
                // Structuralrmg.g:1:124: T__30
                {
                mT__30(); 


                }
                break;
            case 21 :
                // Structuralrmg.g:1:130: T__31
                {
                mT__31(); 


                }
                break;
            case 22 :
                // Structuralrmg.g:1:136: T__32
                {
                mT__32(); 


                }
                break;
            case 23 :
                // Structuralrmg.g:1:142: T__33
                {
                mT__33(); 


                }
                break;
            case 24 :
                // Structuralrmg.g:1:148: T__34
                {
                mT__34(); 


                }
                break;
            case 25 :
                // Structuralrmg.g:1:154: T__35
                {
                mT__35(); 


                }
                break;
            case 26 :
                // Structuralrmg.g:1:160: T__36
                {
                mT__36(); 


                }
                break;
            case 27 :
                // Structuralrmg.g:1:166: T__37
                {
                mT__37(); 


                }
                break;
            case 28 :
                // Structuralrmg.g:1:172: T__38
                {
                mT__38(); 


                }
                break;
            case 29 :
                // Structuralrmg.g:1:178: TEXT
                {
                mTEXT(); 


                }
                break;
            case 30 :
                // Structuralrmg.g:1:183: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;
            case 31 :
                // Structuralrmg.g:1:193: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 32 :
                // Structuralrmg.g:1:204: URI
                {
                mURI(); 


                }
                break;
            case 33 :
                // Structuralrmg.g:1:208: PATH
                {
                mPATH(); 


                }
                break;
            case 34 :
                // Structuralrmg.g:1:213: NUMBER
                {
                mNUMBER(); 


                }
                break;
            case 35 :
                // Structuralrmg.g:1:220: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;

        }

    }


    protected DFA9 dfa9 = new DFA9(this);
    static final String DFA9_eotS =
        "\13\uffff\13\40\2\uffff\1\40\5\uffff\2\40\2\uffff\4\40\1\66\15\40"+
        "\1\104\1\40\1\uffff\5\40\1\113\5\40\1\121\1\40\1\uffff\1\40\1\124"+
        "\1\40\1\126\2\40\1\uffff\5\40\1\uffff\2\40\1\uffff\1\140\1\uffff"+
        "\1\141\7\40\1\151\2\uffff\1\40\1\153\1\154\1\155\2\40\1\160\1\uffff"+
        "\1\40\3\uffff\1\40\1\163\1\uffff\1\164\1\165\3\uffff";
    static final String DFA9_eofS =
        "\166\uffff";
    static final String DFA9_minS =
        "\1\11\12\uffff\13\55\2\uffff\1\55\5\uffff\2\55\2\uffff\24\55\1\uffff"+
        "\15\55\1\uffff\6\55\1\uffff\5\55\1\uffff\2\55\1\uffff\1\55\1\uffff"+
        "\11\55\2\uffff\7\55\1\uffff\1\55\3\uffff\2\55\1\uffff\2\55\3\uffff";
    static final String DFA9_maxS =
        "\1\175\12\uffff\13\172\2\uffff\1\172\5\uffff\2\172\2\uffff\24\172"+
        "\1\uffff\15\172\1\uffff\6\172\1\uffff\5\172\1\uffff\2\172\1\uffff"+
        "\1\172\1\uffff\11\172\2\uffff\7\172\1\uffff\1\172\3\uffff\2\172"+
        "\1\uffff\2\172\3\uffff";
    static final String DFA9_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\13\uffff\1\33"+
        "\1\34\1\uffff\1\36\1\37\1\40\1\42\1\43\2\uffff\1\35\1\41\24\uffff"+
        "\1\20\15\uffff\1\16\6\uffff\1\26\5\uffff\1\14\2\uffff\1\21\1\uffff"+
        "\1\23\11\uffff\1\22\1\24\7\uffff\1\17\1\uffff\1\27\1\30\1\31\2\uffff"+
        "\1\15\2\uffff\1\13\1\25\1\32";
    static final String DFA9_specialS =
        "\166\uffff}>";
    static final String[] DFA9_transitionS = {
            "\1\32\1\31\1\uffff\1\32\1\31\22\uffff\1\32\7\uffff\1\1\1\2\2"+
            "\uffff\1\3\1\34\1\4\1\35\12\34\1\5\1\6\1\33\1\7\2\uffff\1\10"+
            "\32\30\1\11\1\uffff\1\12\1\uffff\1\30\1\uffff\1\13\3\30\1\14"+
            "\1\15\2\30\1\16\2\30\1\17\1\20\1\21\1\30\1\22\1\30\1\23\1\24"+
            "\1\25\6\30\1\26\1\uffff\1\27",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\37\1\36\30\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\42\23\37\1\43\2\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\44\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\14\37\1\45\1\46\14\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\47\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\50\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\51\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\13\37\1\52\16\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\53\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\54\2\37\1\55\6\37\1\56\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\57\3\37\1\60\25\37",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\22\37\1\61\7\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\6\37\1\62\23\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\63\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\64\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\17\37\1\65\12\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\67\13\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\70\26\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\71\26\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\72\31\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\25\37\1\73\4\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\74\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\75\31\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\24\37\1\76\5\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\77\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\14\37\1\100\15\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\101\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\102\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\103\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\16\37\1\105\13\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\17\37\1\106\12\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\107\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\110\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\30\37\1\111\1\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\112\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\114\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\115\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\6\37\1\116\23\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\17\37\1\117\12\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\120\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\15\37\1\122\14\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\123\10\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\13\37\1\125\16\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\22\37\1\127\7\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\21\37\1\130\10\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\131\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\2\37\1\132\27\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\133\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\13\37\1\134\16\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\135\31\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\136\26\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\137\6\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\22\37\1\142\7\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\143\26\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\144\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\145\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\1\146\31\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\2\37\1\147\27\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\22\37\1\150\7\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\152\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\156\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\23\37\1\157\6\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\3\37\1\161\26\37",
            "",
            "",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\4\37\1\162\25\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\2\uffff\12\37\1\41\6\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            ""
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | TEXT | LINEBREAK | WHITESPACE | URI | PATH | NUMBER | ML_COMMENT );";
        }
    }
 

}