/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.AssertOCLInvariantAction;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assert OCL Invariant Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl#getModelKey <em>Model Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl#getInvariant <em>Invariant</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertOCLInvariantActionImpl#getImportContextVariables <em>Import Context Variables</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssertOCLInvariantActionImpl extends TestActionImpl implements AssertOCLInvariantAction {
	/**
	 * The default value of the '{@link #getModelKey() <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelKey()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelKey() <em>Model Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelKey()
	 * @generated
	 * @ordered
	 */
	protected String modelKey = MODEL_KEY_EDEFAULT;

	/**
	 * The default value of the '{@link #getInvariant() <em>Invariant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInvariant()
	 * @generated
	 * @ordered
	 */
	protected static final String INVARIANT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInvariant() <em>Invariant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInvariant()
	 * @generated
	 * @ordered
	 */
	protected String invariant = INVARIANT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImportContextVariables() <em>Import Context Variables</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportContextVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<String> importContextVariables;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertOCLInvariantActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.ASSERT_OCL_INVARIANT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelKey() {
		return modelKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelKey(String newModelKey) {
		String oldModelKey = modelKey;
		modelKey = newModelKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY, oldModelKey, modelKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInvariant() {
		return invariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInvariant(String newInvariant) {
		String oldInvariant = invariant;
		invariant = newInvariant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__INVARIANT, oldInvariant, invariant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getImportContextVariables() {
		if (importContextVariables == null) {
			importContextVariables = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES);
		}
		return importContextVariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY:
				return getModelKey();
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__INVARIANT:
				return getInvariant();
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES:
				return getImportContextVariables();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY:
				setModelKey((String)newValue);
				return;
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__INVARIANT:
				setInvariant((String)newValue);
				return;
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES:
				getImportContextVariables().clear();
				getImportContextVariables().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY:
				setModelKey(MODEL_KEY_EDEFAULT);
				return;
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__INVARIANT:
				setInvariant(INVARIANT_EDEFAULT);
				return;
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES:
				getImportContextVariables().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__MODEL_KEY:
				return MODEL_KEY_EDEFAULT == null ? modelKey != null : !MODEL_KEY_EDEFAULT.equals(modelKey);
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__INVARIANT:
				return INVARIANT_EDEFAULT == null ? invariant != null : !INVARIANT_EDEFAULT.equals(invariant);
			case MT4MTPackage.ASSERT_OCL_INVARIANT_ACTION__IMPORT_CONTEXT_VARIABLES:
				return importContextVariables != null && !importContextVariables.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (modelKey: ");
		result.append(modelKey);
		result.append(", invariant: ");
		result.append(invariant);
		result.append(", importContextVariables: ");
		result.append(importContextVariables);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		return edu.ustb.sei.mt4mt.base.Actions.assertOCL(invariant, modelKey,this.getImportContextVariables().toArray(new String[0]));
	}

} //AssertOCLInvariantActionImpl
