package edu.ustb.sei.egraph.generator.util;

import java.io.OutputStream;
import java.util.logging.*;

public class LogUtil {
	private Logger logger;
	public LogUtil(OutputStream stream) {
		logger = Logger.getLogger("Model Generator");
		logger.addHandler(new StreamHandler(stream, new java.util.logging.SimpleFormatter()));
	}
}
