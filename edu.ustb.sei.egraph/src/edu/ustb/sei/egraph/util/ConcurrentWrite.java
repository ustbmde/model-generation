package edu.ustb.sei.egraph.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.FIELD,ElementType.LOCAL_VARIABLE,ElementType.PARAMETER})
public @interface ConcurrentWrite {
	boolean read() default true;
}
