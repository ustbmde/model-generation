package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class RandomStringValueInRangeFunction extends RandomValueInRangeFunction<String> {
	public RandomStringValueInRangeFunction(AbstractRandomFunction<Character> charRange,
			AbstractRandomFunction<Integer> lenRange) {
		super();
		this.charRange = charRange;
		this.lenRange = lenRange;
	}

	private AbstractRandomFunction<Character> charRange;
	private AbstractRandomFunction<Integer> lenRange;

	@Override
	public String getValue(RandomService t) {
		int length = Math.abs(lenRange.getValue(t).getValue(t));
		char[] buf = new char[length];
		
		for(int i = 0; i < length ; i ++) {
			char ch = charRange.getValue(t).getValue(t);
			buf[i] = ch;
		}
		
		return String.valueOf(buf);
	}

}
