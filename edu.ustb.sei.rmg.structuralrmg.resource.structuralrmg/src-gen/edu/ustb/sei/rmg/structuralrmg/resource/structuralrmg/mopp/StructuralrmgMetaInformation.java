/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgMetaInformation implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgMetaInformation {
	
	public String getSyntaxName() {
		return "structuralrmg";
	}
	
	public String getURI() {
		return "http://www.ustb.edu.cn/sei/mde/structuralrmg";
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextScanner createLexer() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAntlrScanner(new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgLexer());
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParser().createInstance(inputStream, encoding);
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextPrinter createPrinter(java.io.OutputStream outputStream, edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource resource) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgReferenceResolverSwitch getReferenceResolverSwitch() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgReferenceResolverSwitch();
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenResolverFactory getTokenResolverFactory() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "edu.ustb.sei.rmg.structuralrmg/metamodel/structuralrmg.cs";
	}
	
	public String[] getTokenNames() {
		return edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgParser.tokenNames;
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTokenStyle getDefaultTokenStyle(String tokenName) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgBracketPair> getBracketPairs() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResourceFactory();
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgNewFileContentProvider getNewFileContentProvider() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.launchConfigurationType";
	}
	
	public edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgNameProvider createNameProvider() {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.analysis.StructuralrmgDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAntlrTokenHelper tokenHelper = new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
