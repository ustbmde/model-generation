/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgResourceFactory implements org.eclipse.emf.ecore.resource.Resource.Factory {
	
	public StructuralrmgResourceFactory() {
		super();
	}
	
	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {
		return new edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource(uri);
	}
	
}
