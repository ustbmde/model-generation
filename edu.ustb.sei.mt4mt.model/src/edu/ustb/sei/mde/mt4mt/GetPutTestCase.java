/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Get Put Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getGetPutTestCase()
 * @model
 * @generated
 */
public interface GetPutTestCase extends PredefinedBXTestCase {
} // GetPutTestCase
