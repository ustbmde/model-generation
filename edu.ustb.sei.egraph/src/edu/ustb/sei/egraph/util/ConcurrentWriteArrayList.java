package edu.ustb.sei.egraph.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * concurrent write.
 *  after finishing writing, the list becomes immutable
 * @author hexiao
 *
 * @param <E>
 */
public class ConcurrentWriteArrayList<E> extends ArrayList<E> {
	public ConcurrentWriteArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	private static final long serialVersionUID = -197854316712662825L;
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	
	@Override
	public boolean add(E e) {
		boolean ret = false;
		
		lock.writeLock().lock();
		ret = super.add(e);
		lock.writeLock().unlock();
		
		return ret;
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean ret = false;
		
		lock.writeLock().lock();
		try{
			ret = super.addAll(c);
		}catch(Exception e) {
			ret = false;
		}
		lock.writeLock().unlock();
		
		return ret;
	}
	
	@Override
	public void add(int index, E element) {
		lock.writeLock().lock();
		try {
			super.add(index, element);
		} catch(Exception e) {
		}
		lock.writeLock().unlock();
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		boolean ret = false;
		
		lock.writeLock().lock();
		try {
			ret = super.addAll(index, c);
		} catch(Exception e) {
			ret = false;
		}
		lock.writeLock().unlock();
		
		return ret;
	}
	

	
	@Override
	public E set(int index, E element) {
		E e= null;
		lock.writeLock().lock();
		try {
			e = super.set(index, element);
		} catch(Exception ee){}
		lock.writeLock().unlock();
		return e;
	}
	

}
