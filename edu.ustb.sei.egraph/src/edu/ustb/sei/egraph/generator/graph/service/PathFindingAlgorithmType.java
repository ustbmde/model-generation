package edu.ustb.sei.egraph.generator.graph.service;

import java.util.Set;

import edu.ustb.sei.egraph.structure.ENode;

public enum PathFindingAlgorithmType {
	srcToTar,
	tarToSrc
}

@FunctionalInterface
interface PathFindingAlgorithm<NT,ET> {
	boolean findPath(ENode<NT,ET,?> s, ENode<NT,ET,?> t, Set<ENode<NT,ET,?>> visited);
}

