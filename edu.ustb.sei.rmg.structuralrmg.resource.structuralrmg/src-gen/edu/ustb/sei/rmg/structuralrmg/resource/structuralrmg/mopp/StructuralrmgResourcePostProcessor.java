/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgResourcePostProcessor implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourcePostProcessor {
	
	public void process(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource resource) {
		// Set the overrideResourcePostProcessor option to false to customize resource
		// post processing.
	}
	
	public void terminate() {
		// To signal termination to the process() method, setting a boolean field is
		// recommended. Depending on the value of this field process() can stop its
		// computation. However, this is only required for computation intensive
		// post-processors.
	}
	
}
