%options escape=$
%options la=2
%options fp=RCLexer,prefix=Char_
%options single-productions
%options noserialize
%options filter=RCKWLexer.g
%options package=edu.ustb.sei.rmg.random
%options template=./LexerTemplateF.gi
%options export_terminals=("RCParsersym.java", "TK_")
%options include_directory="./"

%Import
	LexerBasicMapF.gi
%End

%Define
	$action_class /.$file_prefix./  -- Deprecated.
	$eof_token /.$_EOF_TOKEN./

 	
	$kw_lexer_class /.RCKWLexer./
 	 	
	$copyright_contributions /.*./

%End

%Globals
    /.
    import java.io.Reader;
    ./
%End


%Export

	IDENTIFIER
	INTEGER_LITERAL
	LONG_LITERAL
	REAL_LITERAL
	CHAR_LITERAL
	STRING_LITERAL
	DATE_LITERAL
	
	PLUS
	MINUS
	MULTIPLY
	DIVIDE

	GREATER
	LESS
	EQUAL
	GREATER_EQUAL
	LESS_EQUAL
	NOT_EQUAL

	LPAREN
	RPAREN
	LBRACE
	RBRACE
	LBRACKET
	RBRACKET

	ARROW
	BAR
	COMMA
	COLON
	COLONCOLON
	SEMICOLON
	DOT
	DOTDOT
	PERCENT
	AT_SYMBOL
	DOLLAR_SYMBOL
%End

%Terminals
	CtlCharNotWS

	LF   CR   HT   FF

	a b c d e f g h i j k l m n o p q r s t u v w x y z
	_

	A B C D E F G H I J K L M N O P Q R S T U V W X Y Z

	0 1 2 3 4 5 6 7 8 9

	AfterASCII
	Space        ::= ' '
	LF           ::= NewLine
	CR           ::= Return
	HT           ::= HorizontalTab
	FF           ::= FormFeed
	DoubleQuote  ::= '"'
	SingleQuote  ::= "'"
	Percent      ::= '%'
	VerticalBar  ::= '|'
	Exclamation  ::= '!'
	AtSign       ::= '@'
	BackQuote    ::= '`'
	Acute        ::= '?��????'
	Tilde        ::= '~'
	Sharp        ::= '#'
	DollarSign   ::= '$'
	Ampersand    ::= '&'
	Caret        ::= '^'
	Colon        ::= ':'
	SemiColon    ::= ';'
	BackSlash    ::= '\'
	LeftBrace    ::= '{'
	RightBrace   ::= '}'
	LeftBracket  ::= '['
	RightBracket ::= ']'
	QuestionMark ::= '?'
	Comma        ::= ','
	Dot          ::= '.'
	LessThan     ::= '<'
	GreaterThan  ::= '>'
	Plus         ::= '+'
	Minus        ::= '-'
	Slash        ::= '/'
	Star         ::= '*'
	LeftParen    ::= '('
	RightParen   ::= ')'
	Equal        ::= '='

%End

%Start
	Token
%End

%Rules

	---------------------  Rules for Scanned Tokens --------------------------------
	-- The lexer creates an array list of tokens which is defined in the PrsStream class.
	-- A token has three attributes: a start offset, an end offset and a kind.
	-- 
	-- Only rules that produce complete tokens have actions to create token objects.
	-- When making a token, calls to the methods, $getToken(1) and $getRightSpan(), 
	-- provide the offsets (i.e. the span) of a rule's right hand side (rhs) and thus of the token.
	-- For a rule of the form A ::= A1 A2 ... An, the start offset of the rhs of A is given by
	-- $getToken(1) or by $getLeftSpan() and the end offset by $getRightSpan().
	--  
	-- Regarding rules for parsing in general, note that for a rhs symbol Ai, the 
	-- method $getToken(i) returns the location of the leftmost character derived from Ai.  
	-- The method $getLeftSpan(i) returns the same location unless Ai produces %empty in which case
	-- it returns the location of the last character derived before reducing Ai to %empty. 
	-- The method $getRightSpan(i) returns the location of the rightmost character derived from Ai 
	-- unless Ai produces %empty in which case it returns the location of the last character 
	-- derived before reducing Ai to %empty.
	--------------------------------------------------------------------------------
	Token ::= Identifier
		/.$BeginAction
					checkForKeyWord();
		  $EndAction
		./


	Token ::= IntegerLiteral
		/.$NoAction
		./
	
	Token ::= LongLiteral
		/.$NoAction
		./
		
	Token ::= DateLiteral
		/.$NoAction
		./
	
	Token ::= IntegerLiteral DotToken
		/.$NoAction
		./

	Token ::= IntegerLiteral DotDotToken
		/.$NoAction
		./

	Token ::= RealLiteral
		/.$BeginAction
					makeToken($_REAL_LITERAL);
		  $EndAction
		./

	Token ::= DoubleQuote SLNotDQOpt DoubleQuote
		/.$BeginAction
				makeToken($_STRING_LITERAL);
		  $EndAction
		./
	
	Token ::= SingleQuote NotSQ SingleQuote
		/.$BeginAction
				makeToken($_CHAR_LITERAL);
		  $EndAction
		./

	Token ::= WS -- White Space is scanned but not added to output vector
		/.$BeginAction
					skipToken();
		  $EndAction
		./

	Token ::= '+'
		/.$BeginAction
					makeToken($_PLUS);
		  $EndAction
		./
	
	Token ::= '$'
		/.$BeginAction
					makeToken($_DOLLAR_SYMBOL);
		  $EndAction
		./
	
		
	Token ::= '-'
		/.$BeginAction
					makeToken($_MINUS);
		  $EndAction
		./

	Token ::= '@'
		/.$BeginAction
					makeToken($_AT_SYMBOL);
		  $EndAction
		./

		
	Token ::= '%'
		/.$BeginAction
			makeToken($_PERCENT);
		  $EndAction
		./

	Token ::= '*'
		/.$BeginAction
					makeToken($_MULTIPLY);
		  $EndAction
		./

	Token ::= '/'
		/.$BeginAction
					makeToken($_DIVIDE);
		  $EndAction
		./

	Token ::= '('
		/.$BeginAction
					makeToken($_LPAREN);
		  $EndAction
		./

	Token ::= ')'
		/.$BeginAction
					makeToken($_RPAREN);
		  $EndAction
		./

	Token ::= '>'
		/.$BeginAction
					makeToken($_GREATER);
		  $EndAction
		./
		
	Token ::= '<'
		/.$BeginAction
					makeToken($_LESS);
		  $EndAction
		./

	Token ::= '='
		/.$BeginAction
					makeToken($_EQUAL);
		  $EndAction
		./

	Token ::= '>' '='
		/.$BeginAction
					makeToken($_GREATER_EQUAL);
		  $EndAction
		./

	Token ::= '<' '='
		/.$BeginAction
					makeToken($_LESS_EQUAL);
		  $EndAction
		./

	Token ::= '<' '>'
		/.$BeginAction
					makeToken($_NOT_EQUAL);
		  $EndAction
		./

	Token ::= '['
		/.$BeginAction
					makeToken($_LBRACKET);
		  $EndAction
		./

	Token ::= ']'
		/.$BeginAction
					makeToken($_RBRACKET);
		  $EndAction
		./

	Token ::= '{'
		/.$BeginAction
					makeToken($_LBRACE);
		  $EndAction
		./

	Token ::= '}'
		/.$BeginAction
					makeToken($_RBRACE);
		  $EndAction
		./

	Token ::= '-' '>'
		/.$BeginAction
					makeToken($_ARROW);
		  $EndAction
		./

	Token ::= '|'
		/.$BeginAction
					makeToken($_BAR);
		  $EndAction
		./

	Token ::= ','
		/.$BeginAction
					makeToken($_COMMA);
		  $EndAction
		./

	Token ::= ':'
		/.$BeginAction
					makeToken($_COLON);
		  $EndAction
		./

	Token ::= ':' ':'
		/.$BeginAction
					makeToken($_COLONCOLON);
		  $EndAction
		./

	Token ::= ';'
		/.$BeginAction
					makeToken($_SEMICOLON);
		  $EndAction
		./

	Token ::= DotToken
		/.$NoAction
		./

	DotToken ::= '.'
		/.$BeginAction
					makeToken($_DOT);
		  $EndAction
		./

	Token ::= DotDotToken
		/.$NoAction
		./

	DotDotToken ::= '.' '.'
		/.$BeginAction
					makeToken($_DOTDOT);
		  $EndAction
		./
	
	DateLiteral -> Year '-' Month '-' Day
	/.$BeginAction
			makeToken($_DATE_LITERAL);
	  $EndAction
	./
	
	Year -> Integer
	
	Month -> '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' 
	       | '0' '1' | '0' '2' | '0' '3' | '0' '4' | '0' '5' | '0' '6' | '0' '7' | '0' '8' | '0' '9' 
	       | '1' '0' | '1' '1' | '1' '2'
	
	Day -> '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' 
	     | '0' '1' | '0' '2' | '0' '3' | '0' '4' | '0' '5' | '0' '6' | '0' '7' | '0' '8' | '0' '9' 
	     | '1' Digit | '2' Digit 
	     | '3' '0' | '3' '1'

    IntegerLiteral ::= Integer
		/.$BeginAction
					makeToken($_INTEGER_LITERAL);
		  $EndAction
		./
	
	LongLiteral ::= Long
	/.$BeginAction
			makeToken($_LONG_LITERAL);
	  $EndAction
	./

    RealLiteral -> Decimal
                 | Decimal Exponent
                 | Integer Exponent
                 
    Long -> Integer 'L' | Integer 'l'

    Integer -> Digit
             | Integer Digit

    Decimal -> Integer '.' Integer

    Exponent -> LetterEe Integer
              | LetterEe '-' Integer
              | LetterEe '+' Integer

    WSChar -> Space
            | LF
            | CR
            | HT
            | FF

    Letter -> LowerCaseLetter
            | UpperCaseLetter
            | _
            | AfterASCII

    LowerCaseLetter -> a | b | c | d | e | f | g | h | i | j | k | l | m |
                       n | o | p | q | r | s | t | u | v | w | x | y | z

    UpperCaseLetter -> A | B | C | D | E | F | G | H | I | J | K | L | M |
                       N | O | P | Q | R | S | T | U | V | W | X | Y | Z

    Digit -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9

    LetterEe -> 'E'
              | 'e'

    WS -> WSChar
        | WS WSChar

    Identifier -> Letter
                | Identifier Letter
                | Identifier Digit
                | Identifier DollarSign
	
	SLNotDQOpt -> %empty
	            | SLNotDQ
	            
	SLNotDQ -> NotDQ
	         | SLNotDQ NotDQ
	
    NotDQ -> Letter
           | Digit
           | SpecialNotDQ
           | Space
           | BackslashEscapedSymbol

    SpecialNotDQ -> SpecialNotSQNotDQ | "'"
    
    SpecialNotSQNotDQ -> '+' | '-' | '/' | '(' | ')' | '*' | '!' | '@' | '`' | '~' |
                         '%' | '&' | '^' | ':' | ';' | '|' | '{' | '}' |
                         '[' | ']' | '?' | ',' | '.' | '<' | '>' | '=' | '#' | DollarSign

    BackslashEscapedSymbol -> '\' EscapedSymbols

	EscapedSymbols -> NotSQNotDQ | '"' | "'" | '\'

    NotSQNotDQ -> Letter
           | Digit
           | SpecialNotSQNotDQ
           | Space

	NotSQ -> Letter
           | Digit
           | SpecialNotSQ
           | Space
           | BackslashEscapedSymbol
    
    SpecialNotSQ -> SpecialNotSQNotDQ | '"'
%End