package edu.ustb.sei.mt4mt.base.updates;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

abstract public class ChangeFeatureUpdate implements IUpdate {
	protected ChangeFeatureUpdate(String hostClass, boolean typeOf, double possibility, String featureName) {
		super();
		this.typeOf = typeOf;
		this.hostClass = hostClass;
		this.possibility = possibility;
		this.featureName = featureName;
	}

	protected boolean typeOf;
	protected String hostClass;
	protected double possibility;
	protected String featureName;


	@Override
	public boolean apply(EObject o, EClass oClass, EPackage oPackage, RandomUtil util, Set<EObject> createdObjects) {
		if(checkTypeValidity(oClass) && createdObjects.contains(o)==false && checkObjectValidity(o, createdObjects)) {
			EStructuralFeature cf = IUpdate.searchFeature(oClass, featureName);
			return doChange(o, cf, util);
		}
		return false;
	}

	protected boolean checkObjectValidity(EObject o, Set<EObject> createdObjects) {
		return true;
	}

	protected boolean doChange(EObject o, EStructuralFeature cf, RandomUtil util) {
		if(shouldChange(o, util)) {
			if(cf.isMany()) {
				List<Object> list = (List<Object>) o.eGet(cf);
				for(int i = 0; i< list.size() ; i ++ ) {
					if(util.decision(this.changeRate())) {
						Object v = list.get(i);
						Object nv = calculateValue(v,util);
						list.set(i, nv);
					}
				}
			} else {
				o.eSet(cf, calculateValue(o.eGet(cf),util));
			}
			return true;
		}
		return false;
	}

	abstract protected Object calculateValue(Object v, RandomUtil util);
	protected double changeRate() {
		return 1.0;
	}

	protected boolean shouldChange(EObject o, RandomUtil util) {
		return util.decision(possibility);
	}

	protected boolean checkTypeValidity(EClass oClass) {
		return this.typeOf ? IUpdate.isTypeOf(oClass, hostClass):  IUpdate.isKindOf(oClass, this.hostClass);
	}

}
