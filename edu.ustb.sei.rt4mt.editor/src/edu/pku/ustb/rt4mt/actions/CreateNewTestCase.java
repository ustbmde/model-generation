package edu.pku.ustb.rt4mt.actions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import edu.pku.ustb.rt4mt.InputParameterConfiguration;
import edu.pku.ustb.rt4mt.RegressionTestModel;
import edu.pku.ustb.rt4mt.Rt4mtFactory;
import edu.pku.ustb.rt4mt.TestCase;
import edu.pku.ustb.rt4mt.TestData;
import edu.pku.ustb.rt4mt.TestExecution;
import edu.pku.ustb.rt4mt.TestRound;
import edu.pku.ustb.rt4mt.TestRoundGroup;
import edu.ustb.sei.rmg.Generator;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.OptionModel;

public class CreateNewTestCase extends Action implements
		ISelectionChangedListener {
	protected ISelectionProvider selectionProvider;
	public ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	public void setSelectionProvider(ISelectionProvider selectionProvider) {
		this.selectionProvider = selectionProvider;
	}

	protected TestRound testRound;
	protected EditingDomain domain;
	protected RegressionTestModel testModel;
	
	public EditingDomain getDomain() {
		return domain;
	}

	public void setDomain(EditingDomain domain) {
		this.domain = domain;
	}

	public CreateNewTestCase() {
		super("Create Test Case");
		setDescription("Create a New Test Case");
	}
	
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		// TODO Auto-generated method stub
		selectionProvider = event.getSelectionProvider();
	    if (event.getSelection() instanceof IStructuredSelection)
	    {
	      setEnabled(updateSelection((IStructuredSelection)event.getSelection()));
	    }
	    else
	    {
	      setEnabled(false);
	    }
	}

	public boolean updateSelection(IStructuredSelection selection) {
		if(selection.size()==1) {
			Object se = selection.getFirstElement();
			if(se instanceof TestRound) {
				testRound = (TestRound) se;
				testModel = (RegressionTestModel) testRound.eContainer().eContainer();
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		//domain.getResourceSet().getPackageRegistry().put(ConfigPackage.eNS_URI, ConfigPackage.eINSTANCE);
		TestCase tc = Rt4mtFactory.eINSTANCE.createTestCase();
		int id = 0;
		for(TestCase c : testModel.getTestCaseGroup().getTestCases()) {
			if(id<c.getId())
				id = c.getId();
		}
		
		
		EList<InputParameterConfiguration> inputs = testModel.getInput().getParameters();
		for(InputParameterConfiguration input : inputs) {
			Generator g = new Generator();			
			OptionModel config = g.loadConfig(input.getRmgModelURI());
			g.setAutoNumber(true);
			g.loadMetamodel(config.getMetamodel());
			g.doConfig(config);
			g.randomGenerateModel();
			String str = g.saveModel(testModel.getInputBase()+"/input.xmi");
			
			if(str!=null) {
				tc.setId(id+1);
				TestData td = Rt4mtFactory.eINSTANCE.createTestData();
				td.setName(input.getName());
				td.setModelURI(str);
				tc.getInputs().add(td);
				testModel.getTestCaseGroup().getTestCases().add(tc);
				TestExecution te = Rt4mtFactory.eINSTANCE.createTestExecution();
				te.setTestCase(tc);
				te.setPassed(false);
				testRound.getTestExecutions().add(te);
				testModel.eResource().setModified(true);
			}
		}
	}
}
