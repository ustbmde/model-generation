/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateModel#getTemplates <em>Templates</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateModel#getPackages <em>Packages</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateModel()
 * @model
 * @generated
 */
public interface TemplateModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Templates</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.rmg.structuralrmg.Template}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Templates</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Templates</em>' containment reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateModel_Templates()
	 * @model containment="true"
	 * @generated
	 */
	EList<Template> getTemplates();

	/**
	 * Returns the value of the '<em><b>Packages</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packages</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packages</em>' reference list.
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateModel_Packages()
	 * @model
	 * @generated
	 */
	EList<EPackage> getPackages();

} // TemplateModel
