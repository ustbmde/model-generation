/**
 */
package edu.ustb.sei.mde.mt4mt.provider;


import edu.ustb.sei.mde.mt4mt.MT4MTFactory;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mde.mt4mt.WrapperTestCase;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mt4mt.WrapperTestCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class WrapperTestCaseItemProvider extends TestCaseItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WrapperTestCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRepeatPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Repeat feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRepeatPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_WrapperTestCase_repeat_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_WrapperTestCase_repeat_feature", "_UI_WrapperTestCase_type"),
				 MT4MTPackage.Literals.WRAPPER_TEST_CASE__REPEAT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST);
			childrenFeatures.add(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST);
			childrenFeatures.add(MT4MTPackage.Literals.WRAPPER_TEST_CASE__CORE_TEST);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns WrapperTestCase.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/WrapperTestCase"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		WrapperTestCase wrapperTestCase = (WrapperTestCase)object;
		String coreName = wrapperTestCase.getCoreTest()==null ? "" : wrapperTestCase.getCoreTest().eClass().getName();
		return "wrapper test" + " " + wrapperTestCase.getId() +" ("+coreName+")";
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(WrapperTestCase.class)) {
			case MT4MTPackage.WRAPPER_TEST_CASE__REPEAT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case MT4MTPackage.WRAPPER_TEST_CASE__BEFORE_TEST:
			case MT4MTPackage.WRAPPER_TEST_CASE__AFTER_TEST:
			case MT4MTPackage.WRAPPER_TEST_CASE__CORE_TEST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createLoadModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createSaveModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createExecuteLauncherAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createEvaluateOCLQueryAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createCopyModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createCompareModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createAssertComparisonResultAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createAssertOCLInvariantAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createUserAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createSetVariableAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createUpdateModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createClearAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST,
				 MT4MTFactory.eINSTANCE.createExternalAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createLoadModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createSaveModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createExecuteLauncherAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createEvaluateOCLQueryAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createCopyModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createCompareModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createAssertComparisonResultAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createAssertOCLInvariantAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createUserAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createSetVariableAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createUpdateModelAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createClearAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST,
				 MT4MTFactory.eINSTANCE.createExternalAction()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__CORE_TEST,
				 MT4MTFactory.eINSTANCE.createCustomizableTestCase()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__CORE_TEST,
				 MT4MTFactory.eINSTANCE.createGetPutTestCase()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__CORE_TEST,
				 MT4MTFactory.eINSTANCE.createPutGetTestCase()));

		newChildDescriptors.add
			(createChildParameter
				(MT4MTPackage.Literals.WRAPPER_TEST_CASE__CORE_TEST,
				 MT4MTFactory.eINSTANCE.createWrapperTestCase()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == MT4MTPackage.Literals.WRAPPER_TEST_CASE__BEFORE_TEST ||
			childFeature == MT4MTPackage.Literals.WRAPPER_TEST_CASE__AFTER_TEST;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
