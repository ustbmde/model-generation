package edu.ustb.sei.rmg;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.rmg.random.RandomUtil;

public class SrcCandidateIterator implements Iterator<EObject> {
	private List<EObject> src;
	private EObject tar;
	private EClass typeFilter;
	private ForbiddenMap forbiddenMap;
	private boolean start;
	
	public SrcCandidateIterator(List<EObject> src, EObject tar,
			EClass typeFilter, ForbiddenMap forbiddenMap, RandomUtil random) {
		super();
		this.src = src;
		this.tar = tar;
		this.typeFilter = typeFilter;
		this.forbiddenMap = forbiddenMap;
		startPos = random.randomUIntU(src.size());
		curPos = startPos;
		start = false;
	}

	private int startPos;
	private int curPos;

	private boolean checkCondition() {
		EObject obj = src.get(curPos);
		if(typeFilter.isInstance(obj)==false) return false;
		if(forbiddenMap.isForbidden(obj, tar)) return false;
		return true;
	}
	
	@Override
	public boolean hasNext() {
		if(start && curPos==startPos) return false;
		while(checkCondition()==false) {
			moveCur();
			if(curPos==startPos) return false;//visited all
		}
		return true;
	}

	protected void moveCur() {
		curPos++;
		if(curPos>=src.size()) curPos = 0;//visited end
	}
	
	

	@Override
	public EObject next() {
		EObject obj = src.get(curPos);
		moveCur();
		start = true;
		return obj;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
