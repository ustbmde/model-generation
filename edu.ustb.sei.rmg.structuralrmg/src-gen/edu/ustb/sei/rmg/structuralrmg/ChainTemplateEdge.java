/**
 */
package edu.ustb.sei.rmg.structuralrmg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chain Template Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#getNode <em>Node</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#isRound <em>Round</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getChainTemplateEdge()
 * @model
 * @generated
 */
public interface ChainTemplateEdge extends TemplateEdge {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' reference.
	 * @see #setNode(TemplateNode)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getChainTemplateEdge_Node()
	 * @model required="true"
	 * @generated
	 */
	TemplateNode getNode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#getNode <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node</em>' reference.
	 * @see #getNode()
	 * @generated
	 */
	void setNode(TemplateNode value);

	/**
	 * Returns the value of the '<em><b>Round</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round</em>' attribute.
	 * @see #setRound(boolean)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getChainTemplateEdge_Round()
	 * @model required="true"
	 * @generated
	 */
	boolean isRound();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge#isRound <em>Round</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round</em>' attribute.
	 * @see #isRound()
	 * @generated
	 */
	void setRound(boolean value);

} // ChainTemplateEdge
