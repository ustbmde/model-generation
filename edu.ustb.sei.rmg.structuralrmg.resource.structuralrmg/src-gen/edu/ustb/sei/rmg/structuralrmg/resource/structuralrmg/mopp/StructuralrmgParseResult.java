/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp;

public class StructuralrmgParseResult implements edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>> commands = new java.util.ArrayList<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>>();
	
	public StructuralrmgParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgCommand<edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
