package edu.ustb.sei.rmg.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OrderedTripleMap<F,S,T,V> {
	private ConcurrentHashMap<F,ConcurrentHashMap<S,ConcurrentHashMap<T,V>>> map = new ConcurrentHashMap<F,ConcurrentHashMap<S,ConcurrentHashMap<T,V>>>();
	private Map<S,ConcurrentHashMap<T,V>> get(F f) {
		ConcurrentHashMap<S,ConcurrentHashMap<T,V>> sMap = map.get(f);
		
		if(sMap==null){
			synchronized(f){
				sMap = map.get(f);
				if(sMap==null){
					sMap = new ConcurrentHashMap<S,ConcurrentHashMap<T,V>>();
					map.put(f, sMap);
				}
			}
		}
		
		return sMap;
	}
	private Map<T,V> get(F f, S s) {
		Map<S, ConcurrentHashMap<T, V>> sMap = get(f);
		ConcurrentHashMap<T,V> tMap = sMap.get(s);
		if(tMap==null){
			synchronized(s){
				tMap = sMap.get(s);
				if(tMap==null){
					tMap = new ConcurrentHashMap<T,V>();
					sMap.put(s, tMap);
				}
			}
		}
		
		return tMap;
	}
	
	public V get(F f, S s, T t) {
		return get(f,s).get(t);
	}
	
	public void put(F f,S s,T t,V v){
		get(f,s).put(t,v);
	}
}
