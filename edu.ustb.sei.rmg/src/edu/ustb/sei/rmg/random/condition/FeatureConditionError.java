package edu.ustb.sei.rmg.random.condition;

public class FeatureConditionError extends RuntimeException {
	private Condition<?> requester;

	public Condition<?> getRequester() {
		return requester;
	}

	public void setRequester(Condition<?> requester) {
		this.requester = requester;
	}
	
	public FeatureConditionError(Condition<?> requester) {
		super();
		this.requester = requester;
	}

	public FeatureConditionError(FeatureConditionError parent, Condition<?> requester) {
		super(parent);
		this.requester = requester;
	}
}
