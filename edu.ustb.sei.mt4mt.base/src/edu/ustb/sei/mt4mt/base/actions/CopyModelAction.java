package edu.ustb.sei.mt4mt.base.actions;

import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

import edu.ustb.sei.mt4mt.base.Context;
import edu.ustb.sei.mt4mt.base.IAction;

public class CopyModelAction implements IAction {
	
	private String sourceModel;
	private String targetModel;

	public CopyModelAction(String sourceModel, String targetModel) {
		super();
		this.sourceModel = sourceModel;
		this.targetModel = targetModel;
	}
	
	protected static URIConverter converter = new ExtensibleURIConverterImpl();
	protected static byte[] buffer = new byte[8192];
	
	@Override
	public void run(Context context) {
		try {
			URI fromURI = URI.createURI(sourceModel, true);
			URI toURI = URI.createURI(targetModel, true);
			
			forChannel(fromURI, toURI);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	public static boolean forChannel(URI ifu,URI ofu) throws Exception{
		InputStream  f1 =  converter.createInputStream(ifu);
		OutputStream  f2 = converter.createOutputStream(ofu);
		
		while(true){
			int len = f1.read(buffer);
			if(len<=0) break;
			f2.write(buffer, 0, len);
		}
		f1.close();
		f2.close();
		return true;
	}

}
