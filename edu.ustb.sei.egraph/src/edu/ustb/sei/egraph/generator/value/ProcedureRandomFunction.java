package edu.ustb.sei.egraph.generator.value;

import java.util.HashSet;
import java.util.Set;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class ProcedureRandomFunction<T> extends AbstractRandomFunction<T> {
	
	public ProcedureRandomFunction(AbstractRandomFunction<T> headFunction, AbstractRandomFunction<T> defaultFunction) {
		super();
		this.headFunction = headFunction;
		this.defaultFunction = defaultFunction;
	}

	private AbstractRandomFunction<T> headFunction;
	private AbstractRandomFunction<T> defaultFunction;

	@Override
	public RandomValueFunction<T> getValue(RandomService t) {
		if(t.decideOnProcedure(this)) {
			return headFunction.getValue(t);
		} else
			return defaultFunction.getValue(t);
	}
	
	private Set<String> provided = null;
	@Override
	public Set<String> providedScopes() {
		if(provided==null) {
			provided = new HashSet<String>();
			provided.addAll(headFunction.providedScopes());
			provided.addAll(defaultFunction.providedScopes());
		}
		return provided;
	}
	
	private Set<String> required = null;
	@Override
	public Set<String> requiredScopes() {
		if(required==null) {
			required = new HashSet<String>();
			required.addAll(headFunction.requiredScopes());
			required.addAll(defaultFunction.requiredScopes());
		}
		return required;
	}

}
