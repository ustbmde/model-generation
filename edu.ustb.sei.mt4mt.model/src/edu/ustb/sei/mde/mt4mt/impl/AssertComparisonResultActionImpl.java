/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.AssertComparisonResultAction;
import edu.ustb.sei.mde.mt4mt.DiffKind;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;
import edu.ustb.sei.mt4mt.base.IAction;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assert Comparison Result Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl#getDiffKey <em>Diff Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl#getDifferentCount <em>Different Count</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AssertComparisonResultActionImpl#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssertComparisonResultActionImpl extends TestActionImpl implements AssertComparisonResultAction {
	/**
	 * The default value of the '{@link #getDiffKey() <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiffKey()
	 * @generated
	 * @ordered
	 */
	protected static final String DIFF_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDiffKey() <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiffKey()
	 * @generated
	 * @ordered
	 */
	protected String diffKey = DIFF_KEY_EDEFAULT;

	/**
	 * The default value of the '{@link #getDifferentCount() <em>Different Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDifferentCount()
	 * @generated
	 * @ordered
	 */
	protected static final int DIFFERENT_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDifferentCount() <em>Different Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDifferentCount()
	 * @generated
	 * @ordered
	 */
	protected int differentCount = DIFFERENT_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final DiffKind KIND_EDEFAULT = DiffKind.EQUAL;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected DiffKind kind = KIND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertComparisonResultActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.ASSERT_COMPARISON_RESULT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDiffKey() {
		return diffKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiffKey(String newDiffKey) {
		String oldDiffKey = diffKey;
		diffKey = newDiffKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY, oldDiffKey, diffKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDifferentCount() {
		return differentCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDifferentCount(int newDifferentCount) {
		int oldDifferentCount = differentCount;
		differentCount = newDifferentCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT, oldDifferentCount, differentCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiffKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(DiffKind newKind) {
		DiffKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY:
				return getDiffKey();
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT:
				return getDifferentCount();
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__KIND:
				return getKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY:
				setDiffKey((String)newValue);
				return;
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT:
				setDifferentCount((Integer)newValue);
				return;
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__KIND:
				setKind((DiffKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY:
				setDiffKey(DIFF_KEY_EDEFAULT);
				return;
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT:
				setDifferentCount(DIFFERENT_COUNT_EDEFAULT);
				return;
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__KIND:
				setKind(KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFF_KEY:
				return DIFF_KEY_EDEFAULT == null ? diffKey != null : !DIFF_KEY_EDEFAULT.equals(diffKey);
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__DIFFERENT_COUNT:
				return differentCount != DIFFERENT_COUNT_EDEFAULT;
			case MT4MTPackage.ASSERT_COMPARISON_RESULT_ACTION__KIND:
				return kind != KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (diffKey: ");
		result.append(diffKey);
		result.append(", differentCount: ");
		result.append(differentCount);
		result.append(", kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public IAction build() {
		return edu.ustb.sei.mt4mt.base.Actions.assertComparison(diffKey, differentCount, kind.getValue());
	}

} //AssertComparisonResultActionImpl
