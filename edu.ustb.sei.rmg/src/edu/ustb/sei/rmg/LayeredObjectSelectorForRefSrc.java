package edu.ustb.sei.rmg;


import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.rmg.util.Pair;

class LayeredObjectSelectorForRefSrc extends AbstractLayeredObjectSelector {
	public LayeredObjectSelectorForRefSrc(Generator host, EList<EObject> allObjs,
			EList<EReference> refs,
			boolean required, boolean unique){
		super(host,allObjs,refs, required, unique);
		currentClass = LayeredObjectSelectorForRefSrc.class;
		
		partition();
	}
	
	
	// Ѱ��src.[ref]=tar���ݵĶ���, ���Ҳ��ƻ�forbidenMap
	// forbidenMap����Ϊnull
//	public EList<EObject> pickObjectWithConditions(EObject tar, EReference ref,
//			ForbiddenMap forbiddenMap, int size) {
//		
//		EList<EObject> result = new UniqueEList<EObject>(size);
//		EList<EObject> availableObjs = new BasicEList<EObject>();
//		
//		
//		if(first.size()!=0){
//			for(EObject o : first) {
//				if(forbiddenMap==null||forbiddenMap.isForbidden(o, tar)==false) {
//					if(ref.getEContainingClass().isSuperTypeOf(o.eClass())){
//						EList<EReference> srcRefs = getRefsBySrc(o.eClass());
//						int id = srcRefs.indexOf(ref);
//						int[] b = this.getLowerBound(o.eClass());
//						int[] d = getDegree(o);
//						if(d[id+1]<b[id+1])
//							availableObjs.add(o);
//					}
//				}
//				//else reject by forbidden map
//			}
//			if(availableObjs.size()!=0){
//				size = availableObjs.size()>size ? size : availableObjs.size();
//				
//				while(result.size()<size) {
//					int id = host.randomUIntU(availableObjs.size());
//					result.add(availableObjs.get(id));
//					availableObjs.remove(id);
//				}
//				
//				return result;
//			}
//		} 
//		
//		if(second.size()!=0) {
//			for(EObject o : second) {
//				if(forbiddenMap==null||forbiddenMap.isForbidden(o,tar)==false) {
//					if(ref.getEContainingClass().isSuperTypeOf(o.eClass())) {
//						EList<EReference> srcRefs = getRefsBySrc(o.eClass());
//						int id = srcRefs.indexOf(ref);
//						int[] ub = this.getUpperBound(o.eClass());
//						int[] d = getDegree(o);
//						
//						if(d[id+1]<ub[id+1]||ub[id+1]==-1)
//							availableObjs.add(o);
//					}
//				}
//			}
//			if(availableObjs.size()!=0){
//				size = availableObjs.size()>size ? size : availableObjs.size();
//				
//				while(result.size()<size) {
//					int id = host.randomUIntU(availableObjs.size());
//					result.add(availableObjs.get(id));
//					availableObjs.remove(id);
//				}
//				
//				return result;
//			}
//		}
//		
//		return null;
//	}
	
//	protected EList<EReference> getRefsBySrc(EClass c) {
//		EList<EReference> result = refsOfObj.get(currentClass, allRefs, c);
//		if(result!=null) return result;
//		result = new UniqueEList<EReference>();
//		refsOfObj.put(currentClass, allRefs,c,result);
//		for(EReference r : allRefs) {
//			if(getReferenceClass(r).isSuperTypeOf(c))
//				result.add(r);
//		}
//		
//		return result;
//	}
	
	@Override
	public EObject pick() {
		int ri = host.random.randomUInt();
		
		if(first.size()!=0){
			int id = ri % first.size();
			return first.get(id);
		} else if(second.size()!=0) {
			int id = ri % second.size();
			return second.get(id);
		}
		
		return null;
	}
	
//	public Pair<EObject, EReference> pickObjectAndReference(int scope) throws Exception {
//		int ri = host.randomUInt();
//		
//		if(((scope&SEARCH_SCOPE_FIRST)!=0) 
//				&& first.size()!=0){//��firstLayer������Ԫ��, ����ѡ��������Ԫ��
//			int id = ri % first.size();
//			EObject o = first.get(id);
//			EList<EReference> refs = getRefs(o.eClass());
//			int[] degree = getDegree(o);
//			int[] bounds = ((scope&SEARCH_SCOPE_SECOND)!=0) ? 
//					getLowerBound(o.eClass()) : getUpperBound(o.eClass());
//			int availableCount = 0;
//			
//			boolean special = getClassCase(o, refs);
//			if(!special) {
//				//������ͨ������м���
//				for(int i=1;i<degree.length;i++)
//					if(degree[i]<bounds[i]&&unhandledObjs.get(o)==null) availableCount++;
//			} else {
//				//���޶���0, һ����˵������ë����
//				//�������Ψһ�Ŀ��ܾ���required=true, d[0]=0  b[i]=0, �������һ��������
//				//require=falseʱ, ����ͬ��ͨ���
//				//require=trueʱ, �������е����޶���0, b[0]Ҳ����1
//				//firstSize�е�Ԫ��һ����d[0]<b[0]
//				for(int i=1;i<degree.length;i++)
//					if(bounds[i]==0&&unhandledObjs.get(o)==null) availableCount++;
//			}
//			
//			int test = host.randomUIntU(availableCount);
//			
//			
//			for(int i=1;i<degree.length;i++) {
//				if((degree[i]<bounds[i]|| (special&&bounds[i]==0))
//						&&unhandledObjs.get(o)==null) {
//					test--;
//					if(test==0)
//						return new Pair<EObject, EReference>(o,refs.get(i-1));
//				}
//			}
//			throw new Exception("FirstLayer�д���һ���Դ���");
//		}
//		
//		if(((scope&SEARCH_SCOPE_SECOND)!=0)
//				&& second.size()!=0) {//��firstLayer������Ԫ��, ��secondLayer��ѡ��
//			int id = ri % second.size();
//			EObject o = second.get(id);
//			EList<EReference> refs = getRefs(o.eClass());
//			int[] d = getDegree(o);
//			int[] b = getUpperBound(o.eClass());
//			int availableCount = 0;
//			
//			for(int i=1;i<d.length;i++)
//				if((d[i]<b[i]||b[i]==-1)&&unhandledObjs.get(o)==null) availableCount++;
//			int test = host.randomUIntU(availableCount);
//			
//			
//			for(int i=1;i<d.length;i++)
//				if((d[i]<b[i]||b[i]==-1)&&unhandledObjs.get(o)==null) {
//					test--;
//					if(test==0)
//						return new Pair<EObject, EReference>(o,refs.get(i-1));
//				}
//			throw new Exception("SecondLayer�д���һ���Դ���");
//		} 
//
//		if((scope&SEARCH_SCOPE_THIRD)!=0){ //������, ��thridLayer��ѡ��, ���������ƻ�ģ�͵ĽṹԼ��
//			if(((scope&SEARCH_SCOPE_THIRD)!=0)) { 
//				EObject o = third.get(host.randomUIntU(third.size()));
//				EList<EReference> refs = getRefs(o.eClass());
//				EReference r = refs.get(host.randomUIntU(refs.size()));
//				
//				return new Pair<EObject, EReference>(o,r);
//			} else {
//				return null;//�Ҳ���Ԫ����!
//			}
//		}
//		return null;
//	}
	
//	@Override
//	protected EList<EReference> getRefs(EClass c) {
//		return this.getRefsBySrc(c);
//	}


	@Override
	protected int getRefLowerBound(EReference r) {
		int l = r.getLowerBound();
		if(this.required && l==0) l = 1;
		return l;
	}
	
	
	protected int getRefUpperBound(EReference r) {
		int u = r.getUpperBound();
		if(unique && (u>1 || u<0)) u = 1;
		return u;
	}


	@Override
	public EClass getReferenceClass(EReference r) {
		return r.getEContainingClass();
	}


	@Override
	protected Iterator<EObject> checkForbiddenConditionForPickObject(EObject opposite, EClass typeFilter,
			List<EObject> candidate, ForbiddenMap forbiddenMap) {
		
		if(forbiddenMap == null)
			return candidate.iterator();
		
//		return forbiddenMap.collectNonForbiddenObjects(candidate, opposite, typeFilter);
		return new SrcCandidateIterator(candidate, opposite, typeFilter, forbiddenMap, host.random);
		
		//return forbiddenMap==null||forbiddenMap.isForbidden(candidate, opposite)==false;
	}
}