package edu.ustb.sei.egraph.util;

import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

@Target({ElementType.PARAMETER,ElementType.METHOD})
public @interface Nullable {
	boolean value() default true;
}
