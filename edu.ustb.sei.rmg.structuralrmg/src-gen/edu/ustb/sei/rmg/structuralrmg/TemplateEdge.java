/**
 */
package edu.ustb.sei.rmg.structuralrmg;

import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getReference <em>Reference</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding <em>Binding</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateEdge()
 * @model abstract="true"
 * @generated
 */
public interface TemplateEdge extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(EReference)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateEdge_Reference()
	 * @model required="true"
	 * @generated
	 */
	EReference getReference();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(EReference value);

	/**
	 * Returns the value of the '<em><b>Binding</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' containment reference.
	 * @see #setBinding(EndpointBinding)
	 * @see edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage#getTemplateEdge_Binding()
	 * @see edu.ustb.sei.rmg.structuralrmg.EndpointBinding#getEdge
	 * @model opposite="edge" containment="true"
	 * @generated
	 */
	EndpointBinding getBinding();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.rmg.structuralrmg.TemplateEdge#getBinding <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding</em>' containment reference.
	 * @see #getBinding()
	 * @generated
	 */
	void setBinding(EndpointBinding value);

} // TemplateEdge
