/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug;

public interface IStructuralrmgDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.debug.StructuralrmgDebugMessage message);
}
