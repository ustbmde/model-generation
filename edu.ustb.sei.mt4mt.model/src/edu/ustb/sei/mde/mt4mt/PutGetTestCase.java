/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Put Get Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getPutGetTestCase()
 * @model
 * @generated
 */
public interface PutGetTestCase extends PredefinedBXTestCase {
} // PutGetTestCase
