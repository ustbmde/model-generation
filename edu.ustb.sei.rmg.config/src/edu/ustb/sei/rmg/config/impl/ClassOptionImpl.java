/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.AttributeOption;
import edu.ustb.sei.rmg.config.ClassOption;
import edu.ustb.sei.rmg.config.ConfigPackage;

import edu.ustb.sei.rmg.config.ReferenceGroupOption;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ClassOptionImpl#getClassRef <em>Class Ref</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ClassOptionImpl#getAttributeOptions <em>Attribute Options</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ClassOptionImpl#getReferenceOptions <em>Reference Options</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassOptionImpl extends ScalableConditionImpl implements ClassOption {
	/**
	 * The cached value of the '{@link #getClassRef() <em>Class Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassRef()
	 * @generated
	 * @ordered
	 */
	protected EClass classRef;

	/**
	 * The cached value of the '{@link #getAttributeOptions() <em>Attribute Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeOption> attributeOptions;

	/**
	 * The cached value of the '{@link #getReferenceOptions() <em>Reference Options</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceGroupOption> referenceOptions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.CLASS_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassRef() {
		if (classRef != null && classRef.eIsProxy()) {
			InternalEObject oldClassRef = (InternalEObject)classRef;
			classRef = (EClass)eResolveProxy(oldClassRef);
			if (classRef != oldClassRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.CLASS_OPTION__CLASS_REF, oldClassRef, classRef));
			}
		}
		return classRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetClassRef() {
		return classRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassRef(EClass newClassRef) {
		EClass oldClassRef = classRef;
		classRef = newClassRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.CLASS_OPTION__CLASS_REF, oldClassRef, classRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeOption> getAttributeOptions() {
		if (attributeOptions == null) {
			attributeOptions = new EObjectContainmentEList<AttributeOption>(AttributeOption.class, this, ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS);
		}
		return attributeOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceGroupOption> getReferenceOptions() {
		if (referenceOptions == null) {
			referenceOptions = new EObjectContainmentEList<ReferenceGroupOption>(ReferenceGroupOption.class, this, ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS);
		}
		return referenceOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS:
				return ((InternalEList<?>)getAttributeOptions()).basicRemove(otherEnd, msgs);
			case ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS:
				return ((InternalEList<?>)getReferenceOptions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.CLASS_OPTION__CLASS_REF:
				if (resolve) return getClassRef();
				return basicGetClassRef();
			case ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS:
				return getAttributeOptions();
			case ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS:
				return getReferenceOptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.CLASS_OPTION__CLASS_REF:
				setClassRef((EClass)newValue);
				return;
			case ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS:
				getAttributeOptions().clear();
				getAttributeOptions().addAll((Collection<? extends AttributeOption>)newValue);
				return;
			case ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS:
				getReferenceOptions().clear();
				getReferenceOptions().addAll((Collection<? extends ReferenceGroupOption>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.CLASS_OPTION__CLASS_REF:
				setClassRef((EClass)null);
				return;
			case ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS:
				getAttributeOptions().clear();
				return;
			case ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS:
				getReferenceOptions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.CLASS_OPTION__CLASS_REF:
				return classRef != null;
			case ConfigPackage.CLASS_OPTION__ATTRIBUTE_OPTIONS:
				return attributeOptions != null && !attributeOptions.isEmpty();
			case ConfigPackage.CLASS_OPTION__REFERENCE_OPTIONS:
				return referenceOptions != null && !referenceOptions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClassOptionImpl
