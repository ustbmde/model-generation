package edu.ustb.sei.rmg.random;

public class RCParserprs implements lpg.runtime.ParseTable, RCParsersym {
    public final static int ERROR_SYMBOL = 49;
    public final int getErrorSymbol() { return ERROR_SYMBOL; }

    public final static int SCOPE_UBOUND = 16;
    public final int getScopeUbound() { return SCOPE_UBOUND; }

    public final static int SCOPE_SIZE = 17;
    public final int getScopeSize() { return SCOPE_SIZE; }

    public final static int MAX_NAME_LENGTH = 36;
    public final int getMaxNameLength() { return MAX_NAME_LENGTH; }

    public final static int NUM_STATES = 167;
    public final int getNumStates() { return NUM_STATES; }

    public final static int NT_OFFSET = 49;
    public final int getNtOffset() { return NT_OFFSET; }

    public final static int LA_STATE_OFFSET = 1130;
    public final int getLaStateOffset() { return LA_STATE_OFFSET; }

    public final static int MAX_LA = 1;
    public final int getMaxLa() { return MAX_LA; }

    public final static int NUM_RULES = 157;
    public final int getNumRules() { return NUM_RULES; }

    public final static int NUM_NONTERMINALS = 73;
    public final int getNumNonterminals() { return NUM_NONTERMINALS; }

    public final static int NUM_SYMBOLS = 122;
    public final int getNumSymbols() { return NUM_SYMBOLS; }

    public final static int SEGMENT_SIZE = 8192;
    public final int getSegmentSize() { return SEGMENT_SIZE; }

    public final static int START_STATE = 158;
    public final int getStartState() { return START_STATE; }

    public final static int IDENTIFIER_SYMBOL = 24;
    public final int getIdentifier_SYMBOL() { return IDENTIFIER_SYMBOL; }

    public final static int EOFT_SYMBOL = 37;
    public final int getEoftSymbol() { return EOFT_SYMBOL; }

    public final static int EOLT_SYMBOL = 37;
    public final int getEoltSymbol() { return EOLT_SYMBOL; }

    public final static int ACCEPT_ACTION = 972;
    public final int getAcceptAction() { return ACCEPT_ACTION; }

    public final static int ERROR_ACTION = 973;
    public final int getErrorAction() { return ERROR_ACTION; }

    public final static boolean BACKTRACK = false;
    public final boolean getBacktrack() { return BACKTRACK; }

    public final int getStartSymbol() { return lhs(0); }
    public final boolean isValidForParser() { return RCParsersym.isValidForParser; }


    public interface IsNullable {
        public final static byte isNullable[] = {0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,1,
            0,0
        };
    };
    public final static byte isNullable[] = IsNullable.isNullable;
    public final boolean isNullable(int index) { return isNullable[index] != 0; }

    public interface ProsthesesIndex {
        public final static byte prosthesesIndex[] = {0,
            18,27,39,14,50,20,23,47,49,73,
            33,36,38,19,21,22,28,56,57,42,
            44,53,55,70,72,35,37,15,34,41,
            43,31,32,40,46,48,52,54,60,62,
            65,67,69,71,30,45,51,68,29,59,
            61,64,66,58,63,26,2,3,4,5,
            6,7,8,9,10,11,12,13,16,17,
            24,25,1
        };
    };
    public final static byte prosthesesIndex[] = ProsthesesIndex.prosthesesIndex;
    public final int prosthesesIndex(int index) { return prosthesesIndex[index]; }

    public interface IsKeyword {
        public final static byte isKeyword[] = {0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,1,1,1,0,0,0,0,0,
            0,1,1,0,1,1,1,0,0,0,
            0,0,1,1,1,1,0,0,0,0,
            0,0,0,0,0,0,0,1,0
        };
    };
    public final static byte isKeyword[] = IsKeyword.isKeyword;
    public final boolean isKeyword(int index) { return isKeyword[index] != 0; }

    public interface BaseCheck {
        public final static short baseCheck[] = {0,
            1,1,1,1,1,1,1,1,1,1,
            1,5,1,1,1,1,1,1,1,1,
            1,1,0,1,1,3,1,1,6,6,
            1,1,1,4,1,3,3,1,3,3,
            1,1,1,1,3,5,5,5,2,1,
            3,4,1,4,1,3,3,1,3,3,
            1,1,1,3,5,5,5,2,1,4,
            1,4,1,3,3,1,3,3,1,1,
            1,1,3,5,2,1,1,1,3,3,
            1,3,3,1,1,1,3,5,1,1,
            4,1,3,3,1,3,3,1,1,3,
            1,3,1,3,1,1,2,3,1,1,
            3,3,1,3,3,1,1,3,1,1,
            1,1,3,3,1,3,3,1,1,3,
            1,1,4,1,3,3,1,3,3,1,
            1,1,3,5,5,5,1,-1,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,-2,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,-3,0,0,0,
            0,0,0,-4,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,-88,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,-5,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,-6,0,0,
            0,0,0,0,-7,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,-30,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,-8,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,-9,0,
            0,0,0,0,0,-54,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,-10,0,0,0,0,0,0,
            0,0,0,0,-12,0,0,0,0,0,
            0,0,0,0,-18,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,-11,0,0,0,0,0,0,-38,
            -13,0,0,-61,0,0,0,0,0,0,
            0,0,0,-31,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,-20,
            0,0,0,-14,0,-93,0,0,-156,0,
            0,0,0,0,0,0,-53,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,-114,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,-101,0,
            -128,0,-35,-15,0,0,0,-134,0,0,
            0,0,-122,-89,-16,0,0,0,0,0,
            0,0,-91,0,0,0,-106,0,0,0,
            0,0,0,-63,0,0,0,0,0,0,
            0,0,0,-115,-36,0,-46,0,0,0,
            0,0,0,0,-137,0,0,0,0,0,
            0,0,-98,0,0,-17,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,-109,0,0,0,0,-116,0,0,
            0,-95,0,0,0,0,-19,-40,-110,0,
            -21,-64,0,0,0,0,0,0,0,0,
            0,0,0,0,0,-43,0,-100,0,0,
            0,0,0,0,0,-97,-65,0,0,0,
            0,0,0,0,-22,0,0,0,0,0,
            0,0,0,0,-99,0,0,0,-123,-23,
            0,0,0,0,-108,0,0,0,0,0,
            0,0,-105,0,0,0,0,0,0,0,
            0,0,-157,-24,0,0,0,0,0,0,
            0,0,0,0,-41,0,-166,-119,0,0,
            0,0,0,0,0,0,-44,0,-25,0,
            0,0,0,0,-167,-26,0,-107,0,0,
            0,0,0,-104,-165,0,0,0,0,0,
            0,0,0,0,0,0,0,0,-125,-127,
            -27,0,0,0,-129,-102,0,0,0,0,
            0,-28,0,-37,0,0,-39,0,0,-67,
            0,0,-117,0,0,0,-69,0,0,-87,
            -42,0,-126,0,0,-45,0,0,0,0,
            0,0,-121,-29,0,0,0,0,-164,0,
            0,0,-32,0,0,0,0,0,0,0,
            0,0,-33,0,-111,0,0,-34,0,0,
            0,0,0,-130,0,0,0,0,-55,0,
            -47,0,0,-142,0,0,0,0,0,-120,
            -48,0,0,-66,0,0,0,-68,0,0,
            -70,0,-103,-112,-113,0,0,0,0,-133,
            0,-49,-50,0,0,0,-138,0,-135,0,
            0,0,-51,-139,0,0,0,-52,0,-162,
            0,-118,0,-140,0,-56,0,0,-161,-163,
            0,-57,0,-58,-59,-60,-62,-71,-72,-73,
            -74,-75,0,0,-76,-77,-78,-79,-80,-81,
            -82,-83,-84,-85,-86,-90,-92,-94,-96,-124,
            -131,-132,-136,-141,-143,-144,-145,-146,-147,-148,
            -149,-150,-151,-152,-153,-154,-155,-158,-159,-160,
            0
        };
    };
    public final static short baseCheck[] = BaseCheck.baseCheck;
    public final int baseCheck(int index) { return baseCheck[index]; }
    public final static short rhs[] = baseCheck;
    public final int rhs(int index) { return rhs[index]; };

    public interface BaseAction {
        public final static char baseAction[] = {
            57,57,57,58,58,58,58,58,58,58,
            58,58,59,68,68,28,14,14,16,16,
            7,7,7,71,71,72,72,56,56,69,
            70,49,49,60,60,45,45,32,45,45,
            33,32,17,17,17,17,11,11,11,2,
            2,15,15,62,62,29,29,26,29,29,
            27,26,12,12,12,13,13,13,3,3,
            6,61,61,34,34,30,34,34,31,30,
            20,20,20,20,21,1,1,65,46,46,
            35,46,46,36,35,8,8,8,9,5,
            64,64,47,47,37,47,47,38,37,22,
            23,23,18,18,18,18,19,4,4,66,
            54,54,50,54,54,51,50,39,39,40,
            40,67,55,55,52,55,55,53,52,41,
            41,42,63,63,48,48,43,48,48,44,
            43,24,24,24,25,25,25,10,1,82,
            43,63,685,96,433,285,749,95,152,42,
            441,62,278,328,257,214,111,113,384,80,
            647,700,565,151,55,58,170,554,73,76,
            35,38,484,88,91,102,105,781,127,824,
            139,144,147,349,756,724,616,244,120,123,
            132,135,792,833,4,186,207,2,3,4,
            5,6,7,8,9,10,11,13,14,1,
            82,43,63,685,96,433,285,749,95,152,
            42,441,62,278,328,257,214,111,113,384,
            80,647,700,565,151,55,58,102,554,73,
            76,35,38,484,88,91,102,105,781,127,
            824,139,144,147,349,756,724,616,111,120,
            123,132,135,792,833,155,951,207,2,3,
            4,5,6,7,8,9,10,11,13,14,
            37,82,43,63,685,96,433,285,749,95,
            152,42,441,62,278,328,257,214,111,113,
            384,80,647,700,565,151,55,58,241,929,
            73,76,35,38,930,88,91,102,105,781,
            127,824,139,144,147,928,931,932,937,72,
            120,123,132,135,935,936,115,82,43,63,
            685,96,433,285,749,95,152,42,441,62,
            278,328,257,214,111,113,384,80,647,700,
            565,151,55,58,195,929,73,76,35,38,
            930,88,91,102,105,308,85,49,68,144,
            147,928,931,932,937,91,82,43,63,685,
            96,433,285,903,95,152,42,441,62,278,
            328,257,214,111,113,384,80,647,700,565,
            151,55,58,401,871,73,76,35,38,892,
            344,223,102,105,284,85,49,68,144,147,
            861,51,893,908,64,82,43,63,685,96,
            433,285,945,95,152,42,943,62,278,328,
            257,944,111,113,942,80,941,700,938,151,
            249,924,926,925,333,922,236,85,49,371,
            916,49,68,940,127,939,139,139,82,43,
            63,685,96,433,285,903,95,152,42,943,
            62,278,328,257,944,111,113,942,80,941,
            700,938,151,139,82,43,63,685,96,433,
            285,945,95,152,42,943,62,278,328,257,
            944,111,113,942,80,941,700,938,151,201,
            82,187,63,438,416,81,285,946,236,27,
            28,441,62,263,399,329,952,685,96,384,
            80,749,95,197,85,55,58,84,953,73,
            76,111,113,930,287,647,700,916,685,96,
            940,127,903,95,12,197,15,311,88,91,
            102,105,110,113,958,201,82,152,63,931,
            932,81,285,158,26,43,417,441,62,16,
            285,565,151,37,42,384,80,278,328,257,
            214,55,58,406,929,73,76,956,58,930,
            145,148,403,959,926,35,38,410,158,406,
            43,397,252,956,16,285,685,96,928,42,
            903,95,278,328,257,214,233,82,197,924,
            111,113,81,285,647,700,399,177,49,43,
            36,39,34,16,285,267,384,80,42,103,
            106,278,328,257,112,177,74,43,54,287,
            423,16,285,685,96,337,42,903,95,278,
            328,257,944,217,82,152,63,111,113,81,
            285,941,700,287,424,943,62,685,96,938,
            151,945,95,942,80,197,15,177,294,43,
            964,111,113,16,285,941,700,278,42,126,
            63,278,328,257,30,177,425,43,311,441,
            62,16,285,75,302,508,42,63,152,278,
            328,257,29,56,59,965,441,62,77,318,
            110,429,565,151,96,186,17,950,95,925,
            55,58,430,954,168,27,28,168,27,28,
            318,144,147,197,960,96,937,110,950,95,
            186,197,15,341,88,91,197,15,96,781,
            127,968,95,58,432,931,824,139,922,302,
            120,123,63,434,935,89,92,132,135,57,
            936,441,62,156,146,406,781,127,435,956,
            25,824,139,25,227,56,59,121,124,197,
            15,80,133,136,28,947,948,63,949,948,
            324,193,967,966,197,15,943,62,197,15,
            965,197,15,273,406,406,68,104,956,956,
            197,15,266,353,72,939,139,354,85,351,
            68,49,90,436,359,924,122,925,362,134,
            331,85,53,68,369,962,31,68,12,399,
            273,967,439,966,441,443,444,446,225,364,
            376,378,382,101,143,387,388,392,374,440,
            447,449,452,453,454,394,134,455,462,463,
            467,469,474,475,411,477,476,479,480,481,
            484,486,487,489,501,502,503,505,496,488,
            506,973,973
        };
    };
    public final static char baseAction[] = BaseAction.baseAction;
    public final int baseAction(int index) { return baseAction[index]; }
    public final static char lhs[] = baseAction;
    public final int lhs(int index) { return lhs[index]; };

    public interface TermCheck {
        public final static byte termCheck[] = {0,
            0,1,2,0,4,5,6,7,8,9,
            10,0,12,13,14,15,0,1,18,16,
            4,10,22,23,24,25,26,0,1,2,
            0,4,5,33,34,35,0,1,2,28,
            4,5,6,7,8,9,10,17,12,13,
            14,15,0,1,18,0,4,0,22,23,
            24,25,26,0,1,2,9,4,5,6,
            7,0,9,10,3,12,13,14,15,0,
            28,18,3,0,1,22,23,24,25,26,
            0,1,2,10,4,5,6,7,8,9,
            10,0,12,13,14,15,27,0,18,0,
            0,2,22,23,0,1,2,8,4,5,
            6,7,8,9,10,0,12,13,14,15,
            29,21,18,0,25,26,22,23,0,1,
            2,16,4,5,6,7,21,9,10,16,
            12,13,14,15,0,0,18,0,1,2,
            22,23,5,6,31,8,0,0,1,0,
            13,14,15,6,7,0,0,1,2,22,
            23,5,6,29,29,0,0,2,2,13,
            14,15,0,8,0,3,0,1,22,23,
            0,1,2,7,4,5,37,7,8,24,
            16,25,26,13,14,15,0,1,2,27,
            4,5,0,7,0,0,0,3,2,13,
            14,15,0,1,2,0,1,5,16,7,
            0,6,7,0,20,13,14,15,0,1,
            24,0,4,2,6,7,5,9,10,8,
            9,21,0,12,2,0,0,5,3,18,
            8,9,0,1,12,32,4,0,1,2,
            18,4,5,0,1,8,0,4,2,6,
            7,5,27,0,1,9,0,31,12,6,
            17,0,1,2,18,4,5,0,1,8,
            0,4,2,6,7,5,0,0,8,2,
            10,28,5,0,1,8,9,4,0,6,
            0,1,0,10,4,3,0,7,2,0,
            0,5,2,0,16,5,10,17,5,9,
            0,1,0,0,1,3,6,4,0,1,
            7,0,4,0,3,7,3,17,0,1,
            0,1,4,0,4,0,6,0,3,27,
            3,0,0,20,3,17,0,0,27,3,
            3,0,19,0,3,20,0,20,0,1,
            0,20,0,1,6,0,20,20,6,0,
            0,20,19,3,21,0,0,12,3,3,
            24,21,0,0,0,16,3,3,0,0,
            20,0,3,0,0,0,3,0,0,0,
            0,36,0,0,16,0,0,16,0,12,
            28,0,0,0,0,17,21,17,19,17,
            17,0,0,29,3,19,0,19,0,24,
            19,19,19,0,0,0,0,3,0,0,
            0,0,16,0,30,0,0,0,0,11,
            11,11,30,17,11,0,11,11,30,11,
            0,0,0,30,0,0,31,0,21,0,
            0,11,11,11,19,11,0,10,0,0,
            0,0,0,0,0,0,21,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0
        };
    };
    public final static byte termCheck[] = TermCheck.termCheck;
    public final int termCheck(int index) { return termCheck[index]; }

    public interface TermAction {
        public final static char termAction[] = {0,
            973,395,300,41,1042,480,1023,1059,454,1072,
            1130,973,651,993,994,995,973,914,1089,595,
            1042,1130,991,992,1114,1102,1103,973,883,764,
            973,1042,786,848,843,405,973,395,300,957,
            1042,480,1023,1059,454,1072,1130,594,651,993,
            994,995,973,883,1089,973,1042,973,991,992,
            1114,1102,1103,973,395,300,1072,1042,480,1023,
            1059,33,1072,1130,648,651,993,994,995,973,
            961,1089,648,973,955,991,992,1114,1102,1103,
            973,395,356,1130,1042,480,1023,1059,497,1072,
            1130,973,651,993,994,995,633,973,1089,973,
            973,780,991,992,973,395,356,551,1042,480,
            1023,1059,523,1072,1130,94,651,993,994,995,
            794,440,1089,973,1102,1103,991,992,973,395,
            356,874,1042,480,1023,1059,677,1072,1130,890,
            651,993,994,995,973,973,1089,973,676,613,
            991,992,642,1023,927,695,973,23,486,973,
            993,994,995,1023,1059,973,973,676,613,991,
            992,642,1023,797,229,973,973,785,780,993,
            994,995,973,854,79,747,973,573,991,992,
            973,897,605,1059,1042,904,972,1059,713,1114,
            735,1102,1103,993,994,995,973,897,605,649,
            1042,904,61,1059,973,973,973,648,785,993,
            994,995,973,573,549,973,486,668,811,1059,
            44,1023,1059,1,1018,993,994,995,973,444,
            1114,973,1042,563,1023,1059,823,1072,1130,699,
            1072,17,973,651,563,973,115,823,666,1089,
            723,1072,973,883,651,553,1042,973,883,764,
            1089,1042,786,973,395,713,973,1042,563,1023,
            1059,823,845,973,676,1072,973,927,651,1023,
            870,973,883,764,1089,1042,786,973,395,864,
            973,1042,758,1023,1059,577,973,973,705,779,
            1130,963,823,973,489,813,1072,1042,150,1023,
            973,897,71,1130,1042,666,973,1059,758,973,
            973,577,779,973,816,823,1130,920,642,1072,
            973,676,973,973,897,652,1023,1042,973,910,
            1059,973,1042,973,597,1059,747,919,973,883,
            973,489,1042,973,1042,973,1023,973,666,884,
            800,973,973,1037,652,920,973,973,885,807,
            810,973,1122,973,597,1056,973,1070,973,676,
            81,1087,973,899,1023,973,1101,1113,1023,108,
            973,1126,1066,829,677,53,142,651,747,597,
            1090,16,109,100,87,859,652,800,126,119,
            1037,138,807,131,973,973,810,973,973,973,
            973,1004,973,973,878,973,973,881,973,651,
            584,973,973,973,973,638,677,803,1110,912,
            738,24,973,564,558,1098,94,1080,973,1091,
            1051,1033,1013,973,973,32,973,747,973,973,
            973,973,874,973,1025,973,973,973,973,1128,
            1127,1071,1043,765,1057,973,1039,1038,969,1020,
            973,973,973,970,973,973,927,973,737,973,
            973,1019,1129,1040,1066,1021,973,1130,973,973,
            973,973,973,973,973,973,755
        };
    };
    public final static char termAction[] = TermAction.termAction;
    public final int termAction(int index) { return termAction[index]; }

    public interface Asb {
        public final static char asb[] = {0,
            1,168,112,108,24,26,24,28,111,108,
            29,39,108,111,111,108,111,44,108,67,
            143,105,106,111,31,111,109,111,109,4,
            5,111,24,24,96,81,126,59,126,50,
            81,81,76,120,81,90,145,145,145,145,
            26,145,8,44,81,65,65,65,65,65,
            61,143,99,98,14,81,131,81,136,81,
            156,156,156,156,156,156,156,156,160,160,
            160,160,160,160,160,159,141,1,21,162,
            81,114,127,36,21,114,21,50,14,81,
            74,71,71,120,75,84,90,91,95,95,
            95,95,95,8,87,103,81,152,116,69,
            103,98,99,109,131,101,136,137,141,142,
            114,114,81,127,148,37,74,40,40,63,
            156,121,65,165,170,170,170,170,170,170,
            170,170,170,170,170,70,99,160,26,26,
            21,62,71,120,93,14,14
        };
    };
    public final static char asb[] = Asb.asb;
    public final int asb(int index) { return asb[index]; }

    public interface Asr {
        public final static byte asr[] = {0,
            33,34,35,8,24,25,26,10,9,18,
            12,7,4,5,22,23,13,14,15,2,
            1,6,0,29,0,21,0,28,19,27,
            32,20,37,16,21,30,3,0,6,1,
            7,4,0,10,9,18,12,7,4,8,
            2,13,14,15,22,23,1,6,5,0,
            6,7,1,4,17,0,9,7,10,6,
            1,4,0,8,4,2,13,14,15,5,
            1,7,0,10,1,0,10,28,0,8,
            5,2,10,0,36,12,0,8,18,12,
            5,2,9,0,31,19,28,27,20,16,
            3,37,32,30,0,1,6,28,0,8,
            5,1,4,2,0,30,1,7,6,0,
            8,2,9,5,0,8,25,26,2,0,
            8,2,24,0,27,3,0,1,6,17,
            0,1,4,28,0,3,20,0,21,19,
            0,16,31,0,32,30,31,37,0,11,
            0
        };
    };
    public final static byte asr[] = Asr.asr;
    public final int asr(int index) { return asr[index]; }

    public interface Nasb {
        public final static byte nasb[] = {0,
            1,11,11,11,11,11,11,11,11,11,
            11,12,11,11,11,11,11,3,11,18,
            11,11,11,11,11,11,11,11,11,1,
            1,11,11,11,31,37,57,16,57,44,
            37,37,28,53,37,39,11,11,11,11,
            11,11,3,3,37,11,11,11,11,11,
            12,11,50,49,8,37,59,37,62,37,
            11,11,11,11,11,11,11,11,11,11,
            11,11,11,11,11,11,65,1,25,11,
            14,11,24,11,25,11,25,8,8,14,
            27,22,22,55,27,35,40,40,31,31,
            31,31,31,3,35,42,14,22,25,20,
            42,50,50,11,51,51,63,63,66,66,
            11,11,37,33,25,11,27,13,13,22,
            11,55,11,11,11,11,11,11,11,11,
            11,11,11,11,11,21,50,11,11,11,
            25,13,22,54,35,8,8
        };
    };
    public final static byte nasb[] = Nasb.nasb;
    public final int nasb(int index) { return nasb[index]; }

    public interface Nasr {
        public final static byte nasr[] = {0,
            41,39,24,4,8,6,12,16,7,15,
            0,2,3,1,0,11,0,1,5,10,
            2,3,0,1,2,0,12,7,20,0,
            4,0,56,0,10,0,28,0,44,24,
            0,5,0,15,7,16,33,0,38,4,
            8,0,31,27,12,0,72,0,8,36,
            0,51,39,0,53,41,0
        };
    };
    public final static byte nasr[] = Nasr.nasr;
    public final int nasr(int index) { return nasr[index]; }

    public interface TerminalIndex {
        public final static byte terminalIndex[] = {0,
            3,14,20,33,16,29,30,7,31,34,
            17,27,35,39,40,21,25,32,6,15,
            26,37,38,49,41,42,46,2,12,13,
            24,11,43,44,45,47,48,1,4,5,
            8,9,10,18,19,22,23,36,50
        };
    };
    public final static byte terminalIndex[] = TerminalIndex.terminalIndex;
    public final int terminalIndex(int index) { return terminalIndex[index]; }

    public interface NonterminalIndex {
        public final static byte nonterminalIndex[] = {0,
            55,63,74,53,83,57,60,81,0,102,
            69,72,0,56,58,59,64,89,0,77,
            0,86,88,100,0,71,73,54,70,76,
            78,67,68,75,80,82,85,87,92,0,
            96,0,99,101,66,79,84,98,65,91,
            93,95,97,90,94,62,51,52,0,0,
            0,0,0,0,0,0,0,0,0,0,
            0,61,0
        };
    };
    public final static byte nonterminalIndex[] = NonterminalIndex.nonterminalIndex;
    public final int nonterminalIndex(int index) { return nonterminalIndex[index]; }

    public interface ScopePrefix {
        public final static byte scopePrefix[] = {
            53,1,6,11,14,17,20,23,26,29,
            32,35,38,41,44,47,50
        };
    };
    public final static byte scopePrefix[] = ScopePrefix.scopePrefix;
    public final int scopePrefix(int index) { return scopePrefix[index]; }

    public interface ScopeSuffix {
        public final static byte scopeSuffix[] = {
            57,4,9,4,9,4,9,4,9,4,
            9,4,9,4,9,4,9
        };
    };
    public final static byte scopeSuffix[] = ScopeSuffix.scopeSuffix;
    public final int scopeSuffix(int index) { return scopeSuffix[index]; }

    public interface ScopeLhs {
        public final static byte scopeLhs[] = {
            70,24,44,41,53,39,51,18,38,8,
            36,20,31,12,27,17,33
        };
    };
    public final static byte scopeLhs[] = ScopeLhs.scopeLhs;
    public final int scopeLhs(int index) { return scopeLhs[index]; }

    public interface ScopeLa {
        public final static byte scopeLa[] = {
            30,20,19,20,19,20,19,20,19,20,
            19,20,19,20,19,20,19
        };
    };
    public final static byte scopeLa[] = ScopeLa.scopeLa;
    public final int scopeLa(int index) { return scopeLa[index]; }

    public interface ScopeStateSet {
        public final static byte scopeStateSet[] = {
            39,42,46,1,3,9,11,28,34,25,
            17,70,61,54,59,83,90
        };
    };
    public final static byte scopeStateSet[] = ScopeStateSet.scopeStateSet;
    public final int scopeStateSet(int index) { return scopeStateSet[index]; }

    public interface ScopeRhs {
        public final static byte scopeRhs[] = {0,
            97,2,0,20,0,73,8,0,19,0,
            104,2,0,90,8,0,103,2,0,88,
            8,0,96,2,0,71,8,0,95,2,
            0,57,8,0,83,2,0,69,8,0,
            78,2,0,61,8,0,94,2,0,66,
            8,0,106,29,34,0,30,21,66,0
        };
    };
    public final static byte scopeRhs[] = ScopeRhs.scopeRhs;
    public final int scopeRhs(int index) { return scopeRhs[index]; }

    public interface ScopeState {
        public final static char scopeState[] = {0,
            854,454,785,229,810,300,158,0,551,454,
            780,229,807,300,158,0,779,563,229,800,
            356,300,158,0,813,779,800,723,699,523,
            584,497,454,563,652,356,300,405,229,158,
            0,523,705,497,454,758,229,356,597,300,
            405,158,0,864,523,713,497,454,829,764,
            605,549,229,356,747,300,405,158,0,605,
            523,713,549,229,356,497,666,454,300,405,
            158,0,755,737,523,695,677,497,454,613,
            229,356,648,300,405,158,0
        };
    };
    public final static char scopeState[] = ScopeState.scopeState;
    public final int scopeState(int index) { return scopeState[index]; }

    public interface InSymb {
        public final static byte inSymb[] = {0,
            0,106,107,66,65,63,56,64,94,69,
            55,1,61,83,78,73,97,35,71,5,
            12,53,72,96,57,95,88,103,90,2,
            8,104,34,33,32,16,29,21,29,3,
            16,16,3,3,16,3,94,78,83,96,
            57,97,8,2,16,59,54,50,52,51,
            1,31,28,3,21,16,3,16,3,16,
            94,78,83,95,96,103,104,97,73,90,
            88,71,69,61,66,57,3,29,29,53,
            1,120,1,121,5,120,1,2,8,5,
            2,5,1,2,8,5,2,8,27,27,
            27,27,27,8,17,17,17,17,17,17,
            5,2,8,57,2,8,2,8,2,8,
            106,51,16,3,1,78,2,1,5,1,
            78,8,1,53,28,59,54,50,28,52,
            28,51,59,52,51,1,8,57,30,30,
            17,1,17,3,17,21,21
        };
    };
    public final static byte inSymb[] = InSymb.inSymb;
    public final int inSymb(int index) { return inSymb[index]; }

    public interface Name {
        public final static String name[] = {
            "",
            "%",
            "+",
            "-",
            "*",
            "/",
            ">",
            "<",
            "=",
            ">=",
            "<=",
            "<>",
            "(",
            ")",
            "{",
            "}",
            "[",
            "]",
            "->",
            "|",
            ",",
            ":",
            "::",
            ";",
            ".",
            "..",
            "@",
            "$",
            "$empty",
            "INTEGER_LITERAL",
            "REAL_LITERAL",
            "CHAR_LITERAL",
            "STRING_LITERAL",
            "LONG_LITERAL",
            "DATE_LITERAL",
            "norm",
            "bern",
            "geom",
            "pois",
            "pare",
            "cauc",
            "true",
            "false",
            "separate",
            "list",
            "unique",
            "for",
            "all",
            "EOF_TOKEN",
            "IDENTIFIER",
            "ERROR_TOKEN",
            "goal",
            "EqualConditionGoal",
            "FeaturePath",
            "Possibility",
            "RealLiteral",
            "DistFunc",
            "RealDistFunc",
            "IntegerDistFunc",
            "IDFuncName",
            "RDFuncName",
            "Parameters",
            "Parameter",
            "IntegerLiteral",
            "AtomicIntegerConditionWithoutD" +
            "P",
            "Scope",
            "IntegerConditionList",
            "AtomicIntegerCondition",
            "AtomicInevitableIntegerConditi" +
            "on",
            "IntegerRange",
            "LongConditionList",
            "AtomicLongCondition",
            "AtomicLongConditionWithoutDP",
            "AtomicInevitableLongCondition",
            "LongLiteral",
            "RealConditionList",
            "AtomicRealCondition",
            "AtomicRealConditionWithoutDP",
            "AtomicInevitableRealCondition",
            "CharConditionList",
            "AtomicCharCondition",
            "AtomicCharConditionWithoutDP",
            "AtomicInevitableCharCondition",
            "CharLiteral",
            "StringConditionList",
            "AtomicStringCondition",
            "AtomicStringConditionWithoutDP",
            "AtomicInevitableStringConditio" +
            "n",
            "AtomicStringConcatConditionWit" +
            "houtDP",
            "AtomicStringSimpleConditionWit" +
            "houtDP",
            "BooleanConditionList",
            "AtomicBooleanCondition",
            "AtomicBooleanConditionWithoutD" +
            "P",
            "AtomicInevitableBooleanConditi" +
            "on",
            "EnumConditionList",
            "AtomicEnumCondition",
            "AtomicEnumConditionWithoutDP",
            "AtomicInevitableEnumCondition",
            "DateConditionList",
            "AtomicDateCondition",
            "AtomicDateConditionWithoutDP",
            "AtomicInevitableDateCondition",
            "DateLiteral"
        };
    };
    public final static String name[] = Name.name;
    public final String name(int index) { return name[index]; }

    public final int originalState(int state) {
        return -baseCheck[state];
    }
    public final int asi(int state) {
        return asb[originalState(state)];
    }
    public final int nasi(int state) {
        return nasb[originalState(state)];
    }
    public final int inSymbol(int state) {
        return inSymb[originalState(state)];
    }

    /**
     * assert(! goto_default);
     */
    public final int ntAction(int state, int sym) {
        return baseAction[state + sym];
    }

    /**
     * assert(! shift_default);
     */
    public final int tAction(int state, int sym) {
        int i = baseAction[state],
            k = i + sym;
        return termAction[termCheck[k] == sym ? k : i];
    }
    public final int lookAhead(int la_state, int sym) {
        int k = la_state + sym;
        return termAction[termCheck[k] == sym ? k : la_state];
    }
}
