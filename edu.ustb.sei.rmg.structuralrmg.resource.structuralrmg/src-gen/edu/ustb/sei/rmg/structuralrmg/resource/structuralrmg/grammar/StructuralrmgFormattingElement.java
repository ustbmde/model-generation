/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar;

public abstract class StructuralrmgFormattingElement extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgSyntaxElement {
	
	public StructuralrmgFormattingElement(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.grammar.StructuralrmgCardinality cardinality) {
		super(cardinality, null);
	}
	
}
