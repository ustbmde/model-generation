/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

public interface IStructuralrmgInterpreterListener {
	
	public void handleInterpreteObject(org.eclipse.emf.ecore.EObject element);
}
