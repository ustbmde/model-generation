package edu.ustb.sei.egraph.generator.util;

import java.util.Arrays;

/**
 * The degree table is defined for a node
 * @author hexiao
 *
 */
public class DegreeTable {
	
	public DegreeTable(int length) {
		table = new int[length];
		Arrays.fill(table, 0);
		sum = 0;
	}
	
	// for fast access
	public int sum;
	public int[] table;
	
	public void increase(int position) {
		table[position]++;
		sum++;
	}
	
	public void decrease(int position) {
		table[position]--;
		sum--;
	}
	
	public void adjust(int from, int to) {
		table[from]--;
		table[to]++;
	}
}
