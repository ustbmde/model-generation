/**
 */
package edu.ustb.sei.rmg.config.impl;

import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.ReferenceOption;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.impl.ReferenceOptionImpl#getReferenceRef <em>Reference Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceOptionImpl extends AbstractReferenceOptionImpl implements ReferenceOption {
	/**
	 * The cached value of the '{@link #getReferenceRef() <em>Reference Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceRef()
	 * @generated
	 * @ordered
	 */
	protected EReference referenceRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.REFERENCE_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceRef() {
		if (referenceRef != null && referenceRef.eIsProxy()) {
			InternalEObject oldReferenceRef = (InternalEObject)referenceRef;
			referenceRef = (EReference)eResolveProxy(oldReferenceRef);
			if (referenceRef != oldReferenceRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.REFERENCE_OPTION__REFERENCE_REF, oldReferenceRef, referenceRef));
			}
		}
		return referenceRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetReferenceRef() {
		return referenceRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceRef(EReference newReferenceRef) {
		EReference oldReferenceRef = referenceRef;
		referenceRef = newReferenceRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.REFERENCE_OPTION__REFERENCE_REF, oldReferenceRef, referenceRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_OPTION__REFERENCE_REF:
				if (resolve) return getReferenceRef();
				return basicGetReferenceRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_OPTION__REFERENCE_REF:
				setReferenceRef((EReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_OPTION__REFERENCE_REF:
				setReferenceRef((EReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.REFERENCE_OPTION__REFERENCE_REF:
				return referenceRef != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferenceOptionImpl
