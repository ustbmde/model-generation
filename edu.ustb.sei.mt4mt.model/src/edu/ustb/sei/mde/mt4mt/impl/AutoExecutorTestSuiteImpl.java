/**
 */
package edu.ustb.sei.mde.mt4mt.impl;

import edu.ustb.sei.mde.mt4mt.AutoExecutorTestSuite;
import edu.ustb.sei.mde.mt4mt.MT4MTPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Auto Executor Test Suite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl#getExecutors <em>Executors</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl#getExecutables <em>Executables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.impl.AutoExecutorTestSuiteImpl#getExecutorAttributeName <em>Executor Attribute Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AutoExecutorTestSuiteImpl extends TestSuiteImpl implements AutoExecutorTestSuite {
	/**
	 * The cached value of the '{@link #getExecutors() <em>Executors</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutors()
	 * @generated
	 * @ordered
	 */
	protected EList<String> executors;

	/**
	 * The cached value of the '{@link #getExecutables() <em>Executables</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutables()
	 * @generated
	 * @ordered
	 */
	protected EList<String> executables;

	/**
	 * The default value of the '{@link #getExecutorAttributeName() <em>Executor Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutorAttributeName()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTOR_ATTRIBUTE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutorAttributeName() <em>Executor Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutorAttributeName()
	 * @generated
	 * @ordered
	 */
	protected String executorAttributeName = EXECUTOR_ATTRIBUTE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AutoExecutorTestSuiteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MT4MTPackage.Literals.AUTO_EXECUTOR_TEST_SUITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getExecutors() {
		if (executors == null) {
			executors = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTORS);
		}
		return executors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getExecutables() {
		if (executables == null) {
			executables = new EDataTypeUniqueEList<String>(String.class, this, MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES);
		}
		return executables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutorAttributeName() {
		return executorAttributeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutorAttributeName(String newExecutorAttributeName) {
		String oldExecutorAttributeName = executorAttributeName;
		executorAttributeName = newExecutorAttributeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME, oldExecutorAttributeName, executorAttributeName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTORS:
				return getExecutors();
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES:
				return getExecutables();
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME:
				return getExecutorAttributeName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTORS:
				getExecutors().clear();
				getExecutors().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES:
				getExecutables().clear();
				getExecutables().addAll((Collection<? extends String>)newValue);
				return;
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME:
				setExecutorAttributeName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTORS:
				getExecutors().clear();
				return;
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES:
				getExecutables().clear();
				return;
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME:
				setExecutorAttributeName(EXECUTOR_ATTRIBUTE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTORS:
				return executors != null && !executors.isEmpty();
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTABLES:
				return executables != null && !executables.isEmpty();
			case MT4MTPackage.AUTO_EXECUTOR_TEST_SUITE__EXECUTOR_ATTRIBUTE_NAME:
				return EXECUTOR_ATTRIBUTE_NAME_EDEFAULT == null ? executorAttributeName != null : !EXECUTOR_ATTRIBUTE_NAME_EDEFAULT.equals(executorAttributeName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executors: ");
		result.append(executors);
		result.append(", executables: ");
		result.append(executables);
		result.append(", executorAttributeName: ");
		result.append(executorAttributeName);
		result.append(')');
		return result.toString();
	}

} //AutoExecutorTestSuiteImpl
