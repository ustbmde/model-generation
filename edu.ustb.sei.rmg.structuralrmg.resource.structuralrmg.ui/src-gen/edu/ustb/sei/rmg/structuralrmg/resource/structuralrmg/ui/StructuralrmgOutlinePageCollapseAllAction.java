/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

public class StructuralrmgOutlinePageCollapseAllAction extends edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.AbstractStructuralrmgOutlinePageAction {
	
	public StructuralrmgOutlinePageCollapseAllAction(edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui.StructuralrmgOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
