/**
 */
package edu.ustb.sei.mde.mt4mt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compare Model Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getLeftURI <em>Left URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getRightURI <em>Right URI</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getDiffKey <em>Diff Key</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCompareModelAction()
 * @model
 * @generated
 */
public interface CompareModelAction extends TestAction {
	/**
	 * Returns the value of the '<em><b>Left URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left URI</em>' attribute.
	 * @see #setLeftURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCompareModelAction_LeftURI()
	 * @model
	 * @generated
	 */
	String getLeftURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getLeftURI <em>Left URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left URI</em>' attribute.
	 * @see #getLeftURI()
	 * @generated
	 */
	void setLeftURI(String value);

	/**
	 * Returns the value of the '<em><b>Right URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right URI</em>' attribute.
	 * @see #setRightURI(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCompareModelAction_RightURI()
	 * @model
	 * @generated
	 */
	String getRightURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getRightURI <em>Right URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right URI</em>' attribute.
	 * @see #getRightURI()
	 * @generated
	 */
	void setRightURI(String value);

	/**
	 * Returns the value of the '<em><b>Diff Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diff Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diff Key</em>' attribute.
	 * @see #setDiffKey(String)
	 * @see edu.ustb.sei.mde.mt4mt.MT4MTPackage#getCompareModelAction_DiffKey()
	 * @model
	 * @generated
	 */
	String getDiffKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mt4mt.CompareModelAction#getDiffKey <em>Diff Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diff Key</em>' attribute.
	 * @see #getDiffKey()
	 * @generated
	 */
	void setDiffKey(String value);

} // CompareModelAction
