package edu.ustb.sei.mt4mt.base.test;

import java.io.PrintStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

public class AutoExecutorTestRunner extends TestRunner {
	private String[] executors;
	private String[] executables;
	private String executorAttributeName;

	public AutoExecutorTestRunner(PrintStream out, IFile logFile, String title, String[] executors, String[] executables, String executorAttributeName) {
		super(out, logFile, title);
		this.executables = executables;
		this.executors = executors;
		this.executorAttributeName = executorAttributeName;
	}

	public AutoExecutorTestRunner(PrintStream out, String title, String[] executors, String[] executables, String executorAttributeName) {
		super(out, title);
		this.executables = executables;
		this.executors = executors;
		this.executorAttributeName = executorAttributeName;
	}
	
	@Override
	public int getTaskCount() {
		return executables.length * super.getTaskCount();
	}
	
	@Override
	public void run(IProgressMonitor monitor) {
		for(String exe : this.executables) {
			println("Switch to new executable "+exe);
			System.out.println("Executing "+exe);
			if(monitor!=null) {
				monitor.setTaskName("Testing "+exe);
				if(monitor.isCanceled())
					return;
			}
			if(setExecutors(exe))
				super.run(monitor);
			println("");
			System.out.println("End of "+exe+"\n");
		}
	}
	
	private boolean setExecutors(String executable) {
		for(String executor : executors) {
			try {
				ILaunchConfiguration launchConfig = getLaunchConfiguration(executor);
				ILaunchConfigurationWorkingCopy copy = launchConfig.getWorkingCopy();
				copy.setAttribute(this.executorAttributeName, executable);
				copy.doSave();
			} catch(Exception e) {
				println("fatal error: cannot switch to new executable");
				return false;
			}
		}
		return true;
	}
	
	protected ILaunchConfiguration getLaunchConfiguration(String name) {
		ILaunchConfiguration[] launchConfigurations;
		try {
			launchConfigurations = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations();
			for(ILaunchConfiguration o : launchConfigurations) {
				if(name.equals(o.getName()))
					return o;
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
