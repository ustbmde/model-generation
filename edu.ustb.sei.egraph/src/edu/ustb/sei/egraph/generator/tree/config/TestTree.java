package edu.ustb.sei.egraph.generator.tree.config;

public class TestTree {

	public static void main(String[] args) {
		String config = "test:Class<deep>{a[1..5]=random([0..5]), b=test(), c=[not self.parent->size()>0.0]}";
		
		TreeConfigUtil.SINGLETON.parse(config);
	}

}
