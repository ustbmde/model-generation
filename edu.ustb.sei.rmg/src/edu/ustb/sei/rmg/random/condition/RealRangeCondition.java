package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class RealRangeCondition extends RangeCondition<Double> {

	@Override
	protected Double randomLU(RandomUtil r) {
		return r.randomDouble(lower, upper);
	}

	@Override
	protected Double randomL(RandomUtil r) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Double randomU(RandomUtil r) {
		// TODO Auto-generated method stub
		return null;
	}

}
