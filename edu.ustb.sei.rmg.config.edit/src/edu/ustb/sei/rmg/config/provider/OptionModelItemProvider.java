/**
 */
package edu.ustb.sei.rmg.config.provider;


import edu.ustb.sei.rmg.config.ConfigFactory;
import edu.ustb.sei.rmg.config.ConfigPackage;
import edu.ustb.sei.rmg.config.OptionModel;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.rmg.config.OptionModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OptionModelItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptionModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMetamodelURIPropertyDescriptor(object);
			addOutputFileURIPropertyDescriptor(object);
			addRootClassPropertyDescriptor(object);
			addAutoNumberPropertyDescriptor(object);
			addUniqueRootPropertyDescriptor(object);
			addEnableConcurrencyPropertyDescriptor(object);
			addBoundSolverPropertyDescriptor(object);
			addMetamodelPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Metamodel URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMetamodelURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_metamodelURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_metamodelURI_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__METAMODEL_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Output File URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputFileURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_outputFileURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_outputFileURI_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__OUTPUT_FILE_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Root Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_rootClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_rootClass_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__ROOT_CLASS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Auto Number feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAutoNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_autoNumber_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_autoNumber_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__AUTO_NUMBER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Unique Root feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUniqueRootPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_uniqueRoot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_uniqueRoot_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__UNIQUE_ROOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enable Concurrency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnableConcurrencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_enableConcurrency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_enableConcurrency_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__ENABLE_CONCURRENCY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bound Solver feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBoundSolverPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_boundSolver_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_boundSolver_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__BOUND_SOLVER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Metamodel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMetamodelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_OptionModel_metamodel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_OptionModel_metamodel_feature", "_UI_OptionModel_type"),
				 ConfigPackage.Literals.OPTION_MODEL__METAMODEL,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConfigPackage.Literals.OPTION_MODEL__GLOBAL_OPTIONS);
			childrenFeatures.add(ConfigPackage.Literals.OPTION_MODEL__CLS_OPTIONS);
			childrenFeatures.add(ConfigPackage.Literals.OPTION_MODEL__ATTR_OPTIONS);
			childrenFeatures.add(ConfigPackage.Literals.OPTION_MODEL__REF_OPTIONS);
			childrenFeatures.add(ConfigPackage.Literals.OPTION_MODEL__FILTER_OPTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns OptionModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/OptionModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((OptionModel)object).getMetamodelURI();
		return label == null || label.length() == 0 ?
			getString("_UI_OptionModel_type") :
			getString("_UI_OptionModel_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(OptionModel.class)) {
			case ConfigPackage.OPTION_MODEL__METAMODEL_URI:
			case ConfigPackage.OPTION_MODEL__OUTPUT_FILE_URI:
			case ConfigPackage.OPTION_MODEL__AUTO_NUMBER:
			case ConfigPackage.OPTION_MODEL__UNIQUE_ROOT:
			case ConfigPackage.OPTION_MODEL__ENABLE_CONCURRENCY:
			case ConfigPackage.OPTION_MODEL__BOUND_SOLVER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConfigPackage.OPTION_MODEL__GLOBAL_OPTIONS:
			case ConfigPackage.OPTION_MODEL__CLS_OPTIONS:
			case ConfigPackage.OPTION_MODEL__ATTR_OPTIONS:
			case ConfigPackage.OPTION_MODEL__REF_OPTIONS:
			case ConfigPackage.OPTION_MODEL__FILTER_OPTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__GLOBAL_OPTIONS,
				 ConfigFactory.eINSTANCE.createGlobalOption()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__CLS_OPTIONS,
				 ConfigFactory.eINSTANCE.createClassOption()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__ATTR_OPTIONS,
				 ConfigFactory.eINSTANCE.createAttributeOption()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__REF_OPTIONS,
				 ConfigFactory.eINSTANCE.createReferenceOption()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__REF_OPTIONS,
				 ConfigFactory.eINSTANCE.createReferenceGroupOption()));

		newChildDescriptors.add
			(createChildParameter
				(ConfigPackage.Literals.OPTION_MODEL__FILTER_OPTIONS,
				 ConfigFactory.eINSTANCE.createReferenceFilterOption()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RmgconfigEditPlugin.INSTANCE;
	}

}
