package edu.ustb.sei.egraph.generator.graph.service;

import java.util.List;

import edu.ustb.sei.commonutil.util.PairHashMap;
import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.ENode;

public enum CriticalPathCollectingAlgorithmType {
	srcToTar,
	tarToSrc
}

@FunctionalInterface
interface CriticalPathCollectingAlgorithm<NT,ET> {
	List<EEdge<NT,ET,?>> collectPaths(ENode<NT,ET,?> s, ENode<NT,ET,?> t, PairHashMap<ENode<NT,ET,?>, ENode<NT,ET,?>, List<EEdge<NT,ET,?>>> info);
}