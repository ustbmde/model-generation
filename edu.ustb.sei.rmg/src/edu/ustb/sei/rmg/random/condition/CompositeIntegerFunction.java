package edu.ustb.sei.rmg.random.condition;

import edu.ustb.sei.rmg.random.RandomUtil;

public class CompositeIntegerFunction extends Condition<Integer> {
	public DistributionFunction<?> function;
	public RangeCondition<Integer> range;

	@Override
	public Integer getValue(RandomUtil r, Object... extra) {
		// TODO Auto-generated method stub
		while(true) {
			if(function instanceof IntegerDistributionFunction) {
				int v = ((IntegerDistributionFunction)function).getValue(r);
				if(isIn(v))
					return v;
			} else {
				double v = ((RealDistributionFunction)function).getValue(r);
				long round = Math.round(v);
				if(isIn((int)round))
					return (int)round;
			}
			
		}
	}
	
	private boolean isIn(int v) {
		boolean gl = true, lu = true;
		if(range.lower!=null) gl = (v>=range.lower);
		
		if(range.upper!=null) lu = (v<=range.upper);
		
		return gl&&lu;
	}

}
