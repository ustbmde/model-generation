package edu.ustb.sei.egraph.generator.value;

import edu.ustb.sei.egraph.generator.graph.service.RandomService;

public class ConstantValueFunction<T> extends RandomValueFunction<T> {
	public ConstantValueFunction(T value) {
		super();
		this.value = value;
	}

	private T value;

	@Override
	public T getValue(RandomService t) {
		return value;
	}

	public String toString() {
		return value.toString();
	}
}
