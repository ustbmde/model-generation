package edu.ustb.sei.egraph.generator.graph.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import edu.ustb.sei.egraph.generator.graph.config.EdgeSetConfig;
import edu.ustb.sei.egraph.structure.EEdge;
import edu.ustb.sei.egraph.structure.EGraph;
import edu.ustb.sei.egraph.structure.ENode;

/*
 * anchor is source
 */
public class TargetNodeSelectionService<NT,ET> extends NodeSelectionService<NT, ET> {

	public TargetNodeSelectionService(GraphGenerationService<NT,ET,?,?> generationService, EdgeSetConfig constraint, List<ENode<NT, ET, ?>> allNodes,
			List<ET> allReferences) {
		super(generationService, constraint, allNodes, allReferences);
	}

	@Override
	public NT getReferenceClass(ET e) {
		return (NT) this.typeModel.getTarget(e);
	}

	@Override
	public int getLowerBound(ET e) {
		return this.typeModel.getSourceLowerBound(e);
	}

	@Override
	public int getUpperBound(ET e) {
		return this.typeModel.getSourceUpperBound(e);
	}

	@Override
	public boolean isRequiredInConstraint() {
		return this.config.isSourceRequired();
	}

	@Override
	public boolean isUniqueInConstraint() {
		return  this.config.isSourceUnique();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Iterator<ENode<NT, ET, ?>> checkForbiddenConditionForPickObject(ENode<NT, ET, ?> anchor, NT typeFilter,
			List<ENode<NT, ET, ?>> candidate, ReachabilityService<NT, ET> reachability) {
		return new ConditionalIterator(anchor, candidate, true, typeFilter, reachability, typeModel);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public boolean addEdge(ENode sn, ENode tn, ET et) {
		EEdge e = this.typeModel.createEdge(et,tn,sn);
		this.generationService.getHostGraph().addEdge(e);
		return true;
	}
	
	@Override
	public boolean isFull(ENode<NT, ET, ?> node, ET et) {
		NT st = this.getReferenceClass(et);
		NT nt = node.getType();
		if(this.typeModel.isSuperNodeType(nt, st)) {
			EGraph<NT,ET,?,?> graph = this.generationService.getHostGraph();
			Collection<?> edges = graph.getIncomings(node, et);
			int ub = this.getUpperBound(et);
			return edges.size() >= ub;
		} else 
			return true;
	}
	
	@Override
	public boolean isEqualToOrLowerThanNeeded(ENode<NT, ET, ?> node, ET et) {
		NT st = this.getReferenceClass(et);
		NT nt = node.getType();
		if(this.typeModel.isSuperNodeType(nt, st)) {
			EGraph<NT,ET,?,?> graph = this.generationService.getHostGraph();
			Collection<?> edges = graph.getIncomings(node, et);
			int ub = this.getLowerBound(et);
			return edges.size() <= ub;
		} else 
			return false;
	}
}