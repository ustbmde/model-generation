/**
 */
package edu.ustb.sei.rmg.config;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Group Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceGroupOption#getReferencesRef <em>References Ref</em>}</li>
 *   <li>{@link edu.ustb.sei.rmg.config.ReferenceGroupOption#getTypeFilter <em>Type Filter</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceGroupOption()
 * @model
 * @generated
 */
public interface ReferenceGroupOption extends AbstractReferenceOption {
	/**
	 * Returns the value of the '<em><b>References Ref</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ref</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ref</em>' reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceGroupOption_ReferencesRef()
	 * @model
	 * @generated
	 */
	EList<EReference> getReferencesRef();

	/**
	 * Returns the value of the '<em><b>Type Filter</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Filter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Filter</em>' reference list.
	 * @see edu.ustb.sei.rmg.config.ConfigPackage#getReferenceGroupOption_TypeFilter()
	 * @model
	 * @generated
	 */
	EList<EClass> getTypeFilter();

} // ReferenceGroupOption
