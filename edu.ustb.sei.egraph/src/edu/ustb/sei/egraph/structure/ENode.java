package edu.ustb.sei.egraph.structure;

import java.util.Map.Entry;

import edu.ustb.sei.egraph.generator.util.QualifiedName;

public class ENode<NT,ET,AT> extends PropertyHolder<AT> {
	public ENode(NT type) {
		super();
		this.type = type;
	}
	
	int internalID = -1;
	
	public void setInternalID(int id) {
		this.internalID = id;
	}
	
	private EGraph<NT,ET,AT,?> graph;

	private NT type;

	public NT getType() {
		return type;
	}

	public void setType(NT label) {
		this.type = label;
	}

	public EGraph<NT, ET, AT, ?> getGraph() {
		return graph;
	}

	public void setGraph(EGraph<NT, ET, AT, ?> graph) {
		this.graph = graph;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.internalID);
		builder.append(", ");
		try {
			QualifiedName typeName = this.graph.getTypeModel().desolveNodeType(type);
			builder.append(typeName.name);
		}catch(Exception e) {
			builder.append(type.toString());
		}
		
		builder.append("{");
		
		for(Entry<AT,Object> pair : this.allProperties()) {
			try {
				QualifiedName attrName = this.graph.getTypeModel().desolveNodeAttribute(pair.getKey());
				builder.append(attrName.name);
			}catch(Exception e) {
				builder.append(pair.getKey().toString());
			}
			builder.append(":\"");
			builder.append(pair.getValue());
			builder.append("\",");
		}
		builder.append("}");
		
		return builder.toString();
	}
}
