package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.rmg.structuralrmg.ChainTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.EndpointBinding;
import edu.ustb.sei.rmg.structuralrmg.NestedTemplateNode;
import edu.ustb.sei.rmg.structuralrmg.PrimitiveTemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.Role;
import edu.ustb.sei.rmg.structuralrmg.StructuralRmgPackage;
import edu.ustb.sei.rmg.structuralrmg.Template;
import edu.ustb.sei.rmg.structuralrmg.TemplateEdge;
import edu.ustb.sei.rmg.structuralrmg.TemplateNode;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptionProvider;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgOptions;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourcePostProcessor;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.IStructuralrmgResourcePostProcessorProvider;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.StructuralrmgEProblemType;
import edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgResource;

public class TemplateChecker implements IStructuralrmgOptionProvider, 
IStructuralrmgResourcePostProcessorProvider, 
IStructuralrmgResourcePostProcessor {

	public TemplateChecker() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map<?, ?> getOptions() {
		// TODO Auto-generated method stub
		return Collections.singletonMap( 
				IStructuralrmgOptions.RESOURCE_POSTPROCESSOR_PROVIDER , 
				this 
				);
	}
	
	private boolean flag = true;

	@Override
	public void process(StructuralrmgResource resource) {
		// TODO Auto-generated method stub
		flag = true;
		
		TreeIterator<EObject> it = resource.getAllContents();
		
		while(it.hasNext()&&flag) {
			EObject o = it.next();
			
			if(o instanceof Template) {
				try{
					checkVariableName((Template)o,resource);
					checkMissingBinding((Template)o,resource);
					checkUnusedRole((Template) o,resource);
				}catch(Exception e) {}
			}
		}
	}
	
	private void checkUnusedRole(Template o, StructuralrmgResource resource) {
		if(o.isAbstract()) return;
		
		for(Role r : o.getAllRoles()) {
			boolean used = false;
			for(TemplateNode n : o.getAllNodes()) {
				if(n.getRole()==r) {
					used = true;
					break;
				}
			}
			if(used==false) {
				resource.addWarning("unsed role", StructuralrmgEProblemType.ANALYSIS_PROBLEM, r);
			}
		}
	}

	private void checkMissingBinding(Template o, StructuralrmgResource resource) {
		// TODO Auto-generated method stub
		for(TemplateEdge e : o.getEdges()) {
			if(e instanceof PrimitiveTemplateEdge) {
				if(((PrimitiveTemplateEdge)e).getSource()!=null && ((PrimitiveTemplateEdge)e).getSource() instanceof NestedTemplateNode) {
					EndpointBinding eb = e.getBinding();
//					for(EndpointBinding b : o.getBindings()) {
//						if(b.getEdge()==e) {
//							eb=b;
//							break;
//						}
//					}
					
					if(eb==null) {
						resource.addError("binding is required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, e);
					} else 
						if(eb==null || eb.getSourceMappings()==null ||eb.getSourceMappings().size()==0) {
							resource.addError("source mappings are required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, eb);
						}
				}
				
				if(((PrimitiveTemplateEdge)e).getTarget()!=null && ((PrimitiveTemplateEdge)e).getTarget() instanceof NestedTemplateNode) {
					EndpointBinding eb = e.getBinding();
//					for(EndpointBinding b : o.getBindings()) {
//						if(b.getEdge()==e) {
//							eb=b;
//							break;
//						}
//					}
					
					if(eb==null) {
						resource.addError("binding is required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, e);
					} else 
						if(eb==null || eb.getTargetMappings()==null ||eb.getTargetMappings().size()==0) {
							resource.addError("target mappings are required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, eb);
						}
				}
			} else if(e instanceof ChainTemplateEdge) {
				if(((ChainTemplateEdge)e).getNode()!=null && ((ChainTemplateEdge)e).getNode() instanceof NestedTemplateNode) {
					EndpointBinding eb = e.getBinding();
//					for(EndpointBinding b : o.getBindings()) {
//						if(b.getEdge()==e) {
//							eb=b;
//							break;
//						}
//					}
					
					if(eb==null) {
						resource.addError("binding is required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, e);
					} else 
						if(eb==null || eb.getSourceMappings()==null || eb.getSourceMappings().size()==0) {
							resource.addError("source mappings are required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, eb);
						}
						if(eb==null || eb.getTargetMappings()==null ||eb.getTargetMappings().size()==0) {
							resource.addError("target mappings are required", StructuralrmgEProblemType.ANALYSIS_PROBLEM, eb);
						}
				}
			}
		}
	}

	private void checkVariableName(Template o, StructuralrmgResource resource) {
		// TODO Auto-generated method stub
		HashSet<String> names = new HashSet<String>();
		for(TemplateNode n : o.getAllNodes()) {
			if(names.contains(n.getName())) {
				resource.addError("the node name has been used", StructuralrmgEProblemType.ANALYSIS_PROBLEM, o);
			} else {
				names.add(n.getName());
			}
		}
		
		
		names.clear();
		for(TemplateEdge n : o.getAllEdges()) {
			if(names.contains(n.getName())) {
				resource.addError("the edge name has been used", StructuralrmgEProblemType.ANALYSIS_PROBLEM, o);
			} else {
				names.add(n.getName());
			}
		}
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		flag=false;
	}

	@Override
	public IStructuralrmgResourcePostProcessor getResourcePostProcessor() {
		return this;
	}

}
