/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.ui;

/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg.mopp.StructuralrmgAntlrTok
 * enHelper.
 */
public class StructuralrmgAntlrTokenHelper {
	// This class is intentionally left empty.
}
