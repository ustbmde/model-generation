/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.rmg.structuralrmg.resource.structuralrmg;

public enum StructuralrmgEProblemSeverity {
	WARNING, ERROR;
}
